function listarAlertas() {
    switchDivsResultados('agendadas');
}

function iniAlertas() {
    alertas();
    setInterval("alertas()", 300000);
}

function alertas(){
    $("#cont_info_alertas").html(".");
    $.post("modulos/gescom/bandeja.php?cmd=cargarAlertas", {}, function(data){
        var num_agendas = $("#txt_cont_alertas").attr("value");
        var arrRetornaCadenaAgendadas = data.split("-agendas|");

        retornaNumAgendadas = arrRetornaCadenaAgendadas[0];
        retornaListaAgendadas = arrRetornaCadenaAgendadas[1];

        $("#cont_info_alertas").empty();
        $("#cont_info_alertas").html(retornaNumAgendadas);

        if(retornaNumAgendadas>0){
            if(num_agendas=='0'){
                document.getElementById('txt_cont_alertas').value=retornaNumAgendadas;
                $("#cont_alertas").show("slide", {
                    direction: "down"
                }, 1000);
            }
            $("#cont_resultado_agendadas").empty();
            $("#cont_resultado_agendadas").html(retornaListaAgendadas);
        } else {
            if(num_agendas!='0'){
                document.getElementById('txt_cont_alertas').value=retornaNumAgendadas;
                $("#cont_alertas").hide("slide", {
                    direction: "down"
                }, 1000);
            }
        }
    });
}

function switchDivsResultados(contenedor) {
    if(contenedor=='main'){
        controlesOn();
        
        var tipo = $("#cmb_tipo").attr("value");
        
        if(tipo == '')
            controlesOff();
        
        if(tipo != '')
            $("#cmb_abc").attr("disabled",'');

        $("#cmb_tipo").attr("disabled",'');
        $("#cont_resultado_main").css("display","block");
        $("#cont_resultado_agendadas").css("display","none");
        $("#cont_resultado_ultimos_movimientos").css("display","none");
    }
    if(contenedor=='agendadas'){
        controlesOff();
        $("#cmb_tipo").attr("disabled",'disable');
        $("#cont_resultado_main").css("display","none");
        $("#cont_resultado_agendadas").css("display","block");
        $("#cont_resultado_ultimos_movimientos").css("display","none");
    }

    if(contenedor=='movimientos'){
        controlesOff();
        $("#cmb_tipo").attr("disabled",'disable');
        $("#cont_resultado_main").css("display","none");
        $("#cont_resultado_agendadas").css("display","none");
        $("#cont_resultado_ultimos_movimientos").css("display","block");
    }
}

function iniUltimosMovimientos() {
    ultimosMovimientos();
    setInterval("ultimosMovimientos()", 300000);    
}

function ultimosMovimientos(){    
    $("#cont_info_movs").html(".");
    $.post("modulos/gescom/bandeja.php?cmd=nuevosMovimientos", {}, function(data){
        var num_movs = $("#txt_cont_movs").attr("value");
        var arrRetornaCadenaMovimientos = data.split("-movs|");

        retornaNumMovimientos = arrRetornaCadenaMovimientos[0];
        retornaListaMovimientos = arrRetornaCadenaMovimientos[1];

        $("#cont_info_movs").empty();
        $("#cont_info_movs").html(retornaNumMovimientos);

        if(retornaNumMovimientos>0){
            if(num_movs=='0'){
                document.getElementById('txt_cont_movs').value=retornaNumMovimientos;
                $("#cont_movs").show("slide", {
                    direction: "down"
                }, 1000);
            }
            $("#cont_resultado_ultimos_movimientos").empty();
            $("#cont_resultado_ultimos_movimientos").html(retornaListaMovimientos);
        } else {
            if(num_movs!='0'){
                document.getElementById('txt_cont_movs').value=retornaNumMovimientos;
                $("#cont_movs").hide("slide", {
                    direction: "down"
                }, 1000);
            }
        }
    });
}

function listarUltimosMovs() {
    //alert("Estamos trabajando en eso... un poquito de paciencia!");
    switchDivsResultados('movimientos');
}

function filtro(accion) {
    $("#cont_filtros").toggle();
}

function centrarElemento(objeto) {
    $('#'+objeto).css({
        position:'absolute',
        left: ($(window).width() - $('#'+objeto).outerWidth())/2,
        top: ($(window).height() - $('#'+objeto).outerHeight())/2
    });
}

function verMovimientos(idgestion) {
    $("#cont_actu_detalle").css("display","none");
    $("#cont_actu_movimientos").css("display","block");
    $("#cont_actu_movimientos").html("Cargando...");
    $.post("modulos/gescom/bandeja.php?cmd=cargarMovimientos", {
        idgestion: idgestion
    },
    function(data){
        $("#cont_actu_movimientos").empty();
        $("#cont_actu_movimientos").html(data);
    });
}

function cerrarMovimientos() {
    $("#cont_actu_detalle").css("display","block");
    $("#cont_actu_movimientos").css("display","none");
}

function detalleActuacion(codigo, tipo, gestion, tabla) {
    loader('start'); 
    $("#childModal").html('');
    $("#childModal").css("background-color","#FFFFFF");
    $.post("modulos/gescom/bandeja.php?cmd=cargarActuacion", {
        codigo: codigo,
        tipo: tipo,
        gestion: gestion,
        tabla: tabla
    },
    function(data){
        loader('end');
        $("#childModal").html(data);
    });
    
    $("#childModal").dialog({
        modal:true,
        width:'80%',
        hide: 'slide',
        title: 'DETALLE',
        position:'top'
    });
}

function detalleActuacionAseg(codigo, tipo, gestion) {
    loader('start'); 
    $("#childModal").html('');
    $("#childModal").css("background-color","#FFFFFF");
    $.post("modulos/gescom/bandeja.aseg.php?cmd=cargarActuacion", {
        codigo: codigo, 
        tipo: tipo,
        gestion: gestion
    },
    function(data){
        loader('end');
        $("#childModal").html(data);       
    });

    $("#childModal").dialog({
        modal:true,
        width:'80%',
        hide: 'slide',
        title: 'DETALLE',
        position:'top'
    });
}

function detalleActuacionCanal(codigo, tipo, gestion, tabla) {
    loader('start'); 
    $("#childModal").html('');
    $("#childModal").css("background-color","#FFFFFF");
    $.post("modulos/gescom/bandeja.canal.php?cmd=cargarDetalleCanal", {
        codigo: codigo, 
        tipo: tipo,
        gestion: gestion,
        tabla: tabla
    },
    function(data){
        loader('end');
        $("#childModal").html(data);       
    });

    $("#childModal").dialog({
        modal:true,
        width:'80%',
        hide: 'slide',
        title: 'DETALLE',
        position:'top'
    });
}

function registrarPedido(id_area, id, tipo, identificador, fecha_agenda, origen , tabla , bolsa ,sub_bolsa) {
    loader('start');
    $("#childModal").html('');
    $("#childModal").css("background-color","#FFFFFF");

    $("#childModal").html('Cargando...');
    $.post("modulos/gescom/consulta.php?cmd=verFormGestion", {
        id_area: id_area, id_pedido: id, tipo: tipo, id_gestion_actuacion: identificador, fecha_agenda: fecha_agenda, origen: origen, tabla: tabla, bolsa : bolsa, sub_bolsa:sub_bolsa
    }, function(data){
        loader('end');
        $("#childModal").html(data);
    });
    
    $("#childModal").dialog({
        modal: true,
        width: '90%',
        hide: 'slide',
        title: 'GESTIONAR',
        position:'top'
    });
}

function actualizarEstado(id_gestion_actuacion, peticion) {
    loader('start');
    $("#childModal").html('');
    $("#childModal").css("background-color","#FFFFFF");

    $.post("modulos/gescom/bandeja.pai.php?cmd=verFormActualizarEstado", {
        id_gestion_actuacion: id_gestion_actuacion, peticion: peticion
    }, function(data){
        loader('end');
        $("#childModal").html(data);
    });

    $("#childModal").dialog({
        modal: true,
        width: '35%',
        hide: 'slide',
        title: 'ACTUALIZAR ESTADO',
        position:'middle'
    });
}

function actualizarEmpresa(peticion) {
    loader('start');
    $("#childModal").html('');
    $("#childModal").css("background-color","#FFFFFF");

    $.post("modulos/gescom/bandeja.pai.php?cmd=verFormActualizarEmpresa", {
        peticion: peticion
    }, function(data){
        loader('end');
        $("#childModal").html(data);
    });

    $("#childModal").dialog({
        modal: true,
        width: '35%',
        hide: 'slide',
        title: 'ACTUALIZAR EMPRESA',
        position:'middle'
    });
}

function movimientosActuacion(id_actuacion) {
    $("#fondoOpaco").css("display","block");
    $("#cont_ga_"+id_actuacion).css("position","absolute");
    $("#cont_ga_"+id_actuacion).css("z-index","10");
    $("#cont_ga_"+id_actuacion).css("top","160px");
    $("#cont_ga_"+id_actuacion).show(300);
    $.post("modulos/gescom/bandeja.php?cmd=cargarMovimientos", {
        id_actuacion: id_actuacion
    },
    function(data){

        $("#cont_ga_"+id_actuacion).empty();
        $("#cont_ga_"+id_actuacion).html(data);
    });
}

function loader(estado) {
    if(estado=='start') {
        $(".bgLoader").css("display","block");
    }
    if(estado=='end') {
        $(".bgLoader").css("display","none");
    }
}

function imprimirBoleta(tipo_actuacion, negocio, gestionado, codigo, idgestion_actuacion) {
    var flag_gestion = gestionado;
    var nro_actuacion = codigo;
    var flag_tipo_actuacion;

    tipo_actuacion = tipo_actuacion.toUpperCase();
    negocio = negocio.toUpperCase();

    if (tipo_actuacion=="AVERIA")
    {
        flag_tipo_actuacion = "AVE";
    }
    else if (tipo_actuacion=="RUTINA")
    {
        flag_tipo_actuacion = "RUT";
    }
    else if (tipo_actuacion=="PROVISION")
    {
        flag_tipo_actuacion = "PROV";
    }
    
    //limpiar boleta
    $("#childModal").html("");

    var pag = "modulos/gescom/vistas/principal/boleta_eecc.php?flag_gestion="+flag_gestion+"&flag_tipo_actuacion="+flag_tipo_actuacion;
    pag = pag+"&nro_actuacion="+nro_actuacion+"&idgestion_actuacion="+idgestion_actuacion;

    $.get("modulos/gescom/vistas/principal/boleta_eecc.php",{
        flag_gestion:flag_gestion,
        flag_tipo_actuacion:flag_tipo_actuacion,
        nro_actuacion:nro_actuacion,
        idgestion_actuacion:idgestion_actuacion
    },function(data){
        //alert(data);
        $("#childModal").html(data);
    });
    
    $("#childModal").dialog({
        modal : true,
        width : '720px',
        hide : 'slide',
        position : 'top',
        title : "Imprimir boleta"
    });
}

/*
function imprimirSeleccion() {
    if(getChkBlock('div','content') > 0) {
        var seleccion = getCheckedBoxFrom('div', 'content', 1);
        $.post("modulos/gescom/bandeja.php?cmd=printSeleccion", {
            seleccion: seleccion
        },
        function(data){
            window.open("modulos/gescom/vistas/principal/boleta_eecc_masivo.php");
        });
    } else {
        alert("Debe seleccionar una o más actuaciones a imprimir!");
    }
}*/

function ini(vista) {
    var strVista='';
    if(vista){
        strVista = '.'+vista;
    }

    switchDivsResultados('main');
    loader('start');
    var tipo = $("#cmb_tipo").attr("value");
    var negocio = $("#cmb_negocio").attr("value");
    var abc = $("#cmb_abc").attr("value");
    var cmb_gest= $("#cmb_gest").attr("value");
    var campo = $("#campo").attr("value");
    var valor = $('#valor').attr("value").replace(/^\s+|\s+$/g, "");
    var cmb_user_aseg= $("#cmb_user_aseg").attr("value");

    var flag_liquidadas = '';
    if($("#ckh_liquidadas").attr('checked')){
        flag_liquidadas = '1';
    }
   
    $.post("modulos/gescom/bandeja"+strVista+".php?cmd=listarActuacionesIni", {
        tipo:tipo,
        negocio:negocio,
        abc:abc,
        campo:campo,
        valor:valor,
        cmb_gest:cmb_gest,
	iduser_aseg:cmb_user_aseg,
        flag_liquidadas:flag_liquidadas
    }, function(data){
        $("#cont_resultado_main").empty();
        $("#cont_resultado_main").html(data);
        loader('end');
        controlesOn();
    });
}

function iniPAI() {


    switchDivsResultados('main');
    loader('start');
    var operacion = $("#cmb_tipo_operacion").attr("value");
    var motivo = $("#cmb_motivo").attr("value");

    var campo = $("#campo").attr("value");
    var valor = $('#valor').attr("value").replace(/^\s+|\s+$/g, "");

    var flag_liquidadas = '';
    if($("#ckh_liquidadas").attr('checked')){
        flag_liquidadas = '1';
    }

    var flag_sin_empresa = '';
    if($("#ckh_sin_empresa").attr('checked')){
        flag_sin_empresa = '1';
    }
    
    var flag_observacion_call = '';
    if($("#ckh_observacion").attr('checked')){
    	flag_observacion_call = '1';
    }
    
    var flag_envio_canal = '';
    if($("#ckh_canal").attr('checked')){
    	flag_envio_canal = '1';
    }
	var flag_agenda_canal = '';
    if($("#ckh_agenda_canal").attr('checked')){
    	flag_agenda_canal = '1';
    }

    $.post("modulos/gescom/bandeja.pai.php?cmd=listarActuacionesIni", {
        operacion:operacion,
        motivo:motivo,
        campo:campo,
        valor:valor,
        flag_liquidadas:flag_liquidadas,
        flag_sin_empresa:flag_sin_empresa,
        flag_observacion_call:flag_observacion_call,
        flag_envio_canal:flag_envio_canal,
        flag_agenda_canal:flag_agenda_canal
    }, function(data){
        $("#cont_resultado_main").empty();
        $("#cont_resultado_main").html(data);
        loader('end');
        controlesOn();
    });
}

function iniCANAL(){


    switchDivsResultados('main');
    loader('start');

	var flag_envio_pai = '';
    if($("#ckh_gestionadas").attr('checked')){
		flag_envio_pai = '1';
    }

    $.post("modulos/gescom/bandeja.canal.php?cmd=listarPeticionesOperador", {
	    flag_envio_pai:flag_envio_pai
    }, function(data){
        $("#cont_resultado_main").empty();
        $("#cont_resultado_main").html(data);
		controlesOn();
        loader('end');
        
    });
}

function ordenar(orden, direccion,vista) {
    $("#orden").attr("value",orden);
    $("#direccion").attr("value",direccion);
    buscar(vista);
}

function buscar_anterior(vista) {
    var strVista='';
    if(vista){
        strVista = '.'+vista;
    }
    switchDivsResultados('main');
    loader('start');
    var tipo = $("#cmb_tipo").attr("value");
    var negocio = $("#cmb_negocio").attr("value");
    var abc = $("#cmb_abc").attr("value");
    var campo = $("#campo").attr("value");
    var valor = $('#valor').attr("value").replace(/^\s+|\s+$/g, "");
    var flag_liquidadas = '';
    if($("#ckh_liquidadas").attr('checked')){
        flag_liquidadas = '1';
    }
    var orden = $("#orden").attr("value");
    var direccion = $("#direccion").attr("value");
    var empresa = $("#cmb_empresa").attr("value");
    var estado = $("#cmb_estado").attr("value");   
    var zonal = $("#cmb_zonal").attr("value");
    var mdf = $("#cmb_mdf").attr("value");
    var flag_gestionado = '';
    if($("#ckh_gestionada").attr('checked')){
        flag_gestionado = '1';
    }
    var flag_criticos = '';
    if($("#ckh_criticos").attr('checked')){
        flag_criticos = '1';
    }
    var nro_reiteradas = $("#cmb_reiteradas").attr("value");
    var segmento = $("#cmb_segmento").attr("value");

    $("#pag_actual").attr("value",'1');
    var pagina = $("#pag_actual").attr("value");
    
    controlesOff();
    $.post("modulos/gescom/bandeja"+strVista+".php?cmd=filtrarActuaciones", {
        tipo:tipo,
        negocio:negocio,
        abc:abc,
        campo:campo,
        valor:valor,
        orden:orden,
        direccion:direccion,
        pagina:pagina,
        empresa:empresa,
        estado:estado,
        zonal:zonal,
        mdf:mdf,
        flag_gestionado:flag_gestionado,
        flag_liquidadas:flag_liquidadas,
        flag_criticos:flag_criticos,
        nro_reiteradas:nro_reiteradas,
        segmento:segmento
    }, function(data){
        $("#tb_resultado").empty();
        $("#tb_resultado").html(data);
        controlesOn();
        loader('end');
    });
}

function buscar(vista) {
    var strVista='';
    if(vista){
        strVista = '.'+vista;
    }
    switchDivsResultados('main');
    loader('start');
    var tipo    = $("#cmb_tipo").attr("value");
    var negocio = $("#cmb_negocio").attr("value");
    var abc     = $("#cmb_abc").attr("value");
    var campo   = $("#campo").attr("value");
    var valor   = $('#valor').attr("value").replace(/^\s+|\s+$/g, "");
    var flag_liquidadas = '';
    if($("#ckh_liquidadas").attr('checked')){
        flag_liquidadas = '1';
    }
    var orden   = $("#orden").attr("value");
    var direccion = $("#direccion").attr("value");
    var empresa = $("#cmb_empresa").attr("value");
    var estado  = $("#cmb_estado").attr("value");
    var zonal   = $("#cmb_zonal").attr("value");
    var mdf     = $("#cmb_mdf").attr("value");
    var lista_mdf = getCheckedBoxFrom("div", "div_list_mdfs", "1");
    var lista_segmento = getCheckedBoxFrom("div", "div_list_segmento", "1");
    
    var flag_gestionado = '';
    if($("#ckh_gestionada").attr('checked')){
        flag_gestionado = '1';
    }
    var flag_criticos = '';
    if($("#ckh_criticos").attr('checked')){
        flag_criticos = '1';
    }
    var flag_visita = '';
    if($("#ckh_visita").attr('checked')){
        flag_visita = '2';
    }
    var nro_reiteradas = $("#cmb_reiteradas").attr("value");
    var segmento = $("#cmb_segmento").attr("value");

    var flag_con_speedy = "";
    if($("#ckh_con_speedy").attr("checked")){
        flag_con_speedy = "1";
    }
	
	// Se agrega el flag para CRM - Carlos Zegarra - 15/09/2011 
	var flag_crm = "";
    if($("#ckh_crm").attr("checked")){
        flag_crm = "1";
    }
    
    var segm_prioridad = $("#cmb_seg_prioridad").attr("value");

    $("#pag_actual").attr("value",'1');
    var pagina = $("#pag_actual").attr("value");

    controlesOff();
    $.post("modulos/gescom/bandeja"+strVista+".php?cmd=filtrarActuaciones", {
        tipo:tipo,
        negocio:negocio,
        abc:abc,
        campo:campo,
        valor:valor,
        orden:orden,
        direccion:direccion,
        pagina:pagina,
        empresa:empresa,
        estado:estado,
        zonal:zonal,
        mdf:mdf,
        lista_mdf:lista_mdf,
        flag_gestionado:flag_gestionado,
        flag_liquidadas:flag_liquidadas,
        flag_criticos:flag_criticos,
        nro_reiteradas:nro_reiteradas,
        segmento:segmento,
        lista_segmento:lista_segmento,
        flag_con_speedy:flag_con_speedy,
        flag_visita:flag_visita,
        segm_prioridad:segm_prioridad,
		flag_crm:flag_crm
    }, function(data){
        $("#tb_resultado").empty();
        $("#tb_resultado").html(data);
        controlesOn();
        loader('end');
    });
}

function buscarPAI() {

    switchDivsResultados('main');
    loader('start');
    var operacion = $("#cmb_tipo_operacion").attr("value");
    var motivo = $("#cmb_motivo").attr("value");
    var campo = $("#campo").attr("value");
    var valor = $('#valor').attr("value").replace(/^\s+|\s+$/g, "");
    var flag_liquidadas = '';
    if($("#ckh_liquidadas").attr('checked')){
        flag_liquidadas = '1';
    }
    var flag_sin_empresa = '';
    if($("#ckh_sin_empresa").attr('checked')){
        flag_sin_empresa = '1';
    }
    var flag_observacion_call = '';
    if($("#ckh_observacion").attr('checked')){
    	flag_observacion_call = '1';
    }
    var flag_envio_canal = '';
    if($("#ckh_canal").attr('checked')){
    	flag_envio_canal = '1';
    }
	var flag_agenda_canal = '';
    if($("#ckh_agenda_canal").attr('checked')){
    	flag_agenda_canal = '1';
    }
	
    var orden = $("#orden").attr("value");
    var direccion = $("#direccion").attr("value");
    var empresa = $("#cmb_empresa").attr("value");
    var estado = $("#cmb_estado").attr("value");
    var zonal = $("#cmb_zonal").attr("value");
    var operador = $("#cmb_operador").attr("value");
    var prioridad = $("#cmb_prioridad").attr("value");
    var mdf = $("#cmb_mdf").attr("value");
    var segmento = $("#cmb_segmento").attr("value");
    var producto = $("#cmb_producto").attr("value");

    $("#pag_actual").attr("value",'1');
    var pagina = $("#pag_actual").attr("value");

    controlesOff();
    $.post("modulos/gescom/bandeja.pai.php?cmd=filtrarActuaciones", {
        operacion:operacion,
        motivo:motivo,
        campo:campo,
        valor:valor,
        orden:orden,
        direccion:direccion,
        pagina:pagina,
        empresa:empresa,
        estado:estado,
        zonal:zonal,
        mdf:mdf,
        flag_liquidadas:flag_liquidadas,
        operador:operador,
        prioridad:prioridad,
        segmento:segmento,
        producto:producto,
        flag_sin_empresa:flag_sin_empresa,
        flag_observacion_call:flag_observacion_call,
        flag_envio_canal:flag_envio_canal,
        flag_agenda_canal:flag_agenda_canal
    }, function(data){
        $("#tb_resultado").empty();
        $("#tb_resultado").html(data);
        controlesOn();
        loader('end');
    });
}

function controlesOff() {
    $("#cmb_abc").attr("disabled",'disable');
    $("#cmb_negocio").attr("disabled",'disable');
    $("#campo").attr("disabled",'disable');
    $("#valor").attr("disabled",'disable');
    $("#btn_buscar_ini").attr("disabled",'disable');
    $("#btn_limpiar").attr("disabled",'disable');
    $("#cmb_gest").attr("disabled",'disable');
}

function controlesOn() {
    $("#cmb_abc").attr("disabled",'');
    $("#cmb_negocio").attr("disabled",'');
    $("#campo").attr("disabled",'');
    $("#valor").attr("disabled",'');
    $("#btn_buscar_ini").attr("disabled",'');
    $("#btn_limpiar").attr("disabled",'');
    $("#cmb_gest").attr("disabled",'');
}

function masMovimiento(idmovimiento) {
    $("#info_mov_"+idmovimiento).css("display","block");
    $("#btn_mas_"+idmovimiento).css("display","none");
    $("#btn_menos_"+idmovimiento).css("display","block");
}

function masMovimientoCerrar(idmovimiento) {
    $("#info_mov_"+idmovimiento).css("display","none");
    $("#btn_mas_"+idmovimiento).css("display","block");
    $("#btn_menos_"+idmovimiento).css("display","none");
}

function descargaReporte(){
    /*
     *  Todo el codigo para generar el reporte
    */   
    window.open("modulos/gescom/vistas/principal/rpt_descarga_tai.php",'mywin','left=20,top=20,width=700,height=400,toolbar=no,directories=no,menubar=no,status=no,resizable=1');
}

function asignarOperadorAseg() {
    var chx = getCheckedBoxFrom("div", "content", "1")
    if (chx == "") {
        alert("Debe seleccionar al menos una actuacion");
    } else {
        $.post("modulos/gescom/bandeja.aseg.php?cmd=vistaAsignarOperadorAseg", { strActu : chx }, function(data) {
            $("#childModal").html(data);
        });

        $("#childModal").dialog({
            modal : true,
            width : '500px',
            hide : 'slide',
            title : 'Asignar Operador - Aseguramiento',
            position : 'top'
        });
    }
}

function asignarOperador() {
    var chx = getCheckedBoxFrom("div", "content", "1")

    if (chx == "") {
        alert("Debe seleccionar al menos una actuacion");
    } else {
        $.post("modulos/gescom/bandeja.psi.php?cmd=vistaAsignarOperador", { strActu : chx }, function(data) {
            $("#childModal").html(data);
        });
        $("#childModal").dialog({
            modal : true,
            width : '500px',
            hide : 'slide',
            title : 'Asignar Operador',
            position : 'top'
        });
    }
}

function asignarOperadorPai() {
    var chx = getCheckedBoxFrom("div", "content", "1")

    if (chx == "") {
        alert("Debe seleccionar al menos una peticion");
    } else {
        $.post("modulos/gescom/bandeja.pai.php?cmd=vistaAsignarOperador", { strPeticiones : chx }, function(data) {
            $("#childModal").html(data);
        });
        $("#childModal").dialog({
            modal : true,
            width : '500px',
            hide : 'slide',
            title : 'Asignar Operador',
            position : 'top'
        });
    }
}

function selecOperadorActu(idoperador){
    $('#usuario_select').attr("value",idoperador);
}

function descargaBandejaTAI2(){
    /*
     *  Todo el codigo para generar el reporte
     */
    flag_liquidadas = '0';
    flag_gestionado = '0';

    var tipo = $("#cmb_tipo").attr("value");

    if($("#ckh_liquidadas").attr('checked')) {
        flag_liquidadas = '1';
    }

    if($("#ckh_gestionada").attr('checked')){
        flag_gestionado = '1';
    }
    
    if(tipo=='Averia' && (flag_liquidadas=='1' || flag_gestionado=='1')){
        var parametros= $("#form_content").serialize();
        window.top.location="modulos/gescom/bandeja.php?cmd=reporteBAndejaTAI2&"+parametros;
    } else {
        alert('Descarga permitida unicamente para Averias gestionadas');
    }            
}


function descargaBandejaTAI2_new()
{
    window.top.location="modulos/gescom/reporteador/rpt.gestionados.tai2.php";   
}


function descargaBandeja_gestionadas(contrata)
{
    var reporte = 'tdp';
    if(contrata=='')  reporte = 'tdp';
    if(contrata=='1') reporte = 'tdp';
    if(contrata=='3') reporte = 'itete';
    if(contrata=='4') reporte = 'lari';
    if(contrata=='5') reporte = 'cobra';
    if(contrata=='6') reporte = 'calatel';

    //window.top.location="modulos/gescom/vistas/usp_rpt/rpt.bandeja.tai2.php";
    window.top.location="../webunif_reportes/gestionadas_eecc/rpt_gestionados_"+reporte+".zip";
    

}

function descargaBandejaPSI()
{
    window.top.location="modulos/gescom/reporteador/rpt.gestionados.psi.php";

}


function descargar(nom_archivo) {
    if(nom_archivo != '')
        window.top.location="modulos/gescom/consulta.php?cmd=descargar&nom_archivo="+nom_archivo;
    else
        alert('No se puede iniciar la descarga porque el archivo no existe');
}

function iniOperador(vista) {

    loader('start');

    if(vista=='pai'){
        $.post("modulos/gescom/bandeja.pai.php?cmd=listarPeticionesOperador", { }, function(data){
            $("#cont_resultado_main").empty();
            $("#cont_resultado_main").html(data);
            loader('end');
        });
    }
    if(vista=='psi'){

        $.post("modulos/gescom/bandeja.psi.php?cmd=listarActuacionesOperador", { }, function(data){

            $("#cont_resultado_main").empty();
            $("#cont_resultado_main").html(data);
            loader('end');
        
        });
    }
    if(vista=='canal'){
    $.post("modulos/gescom/bandeja.canal.php?cmd=listarPeticionesOperador", { }, function(data){
        $("#cont_resultado_main").empty();
        $("#cont_resultado_main").html(data);
        loader('end');       
    });
    }

}

function mostrarActualizacion(){
    $.post("modulos/gescom/actualizaciones.php", {cmd: "mostrarActualizacion" }, function(data){
        $("#cont_actualizacion_data").empty();
        $("#cont_actualizacion_data").html(data);
        
    });
}

function refrescaAsignacionPSI(){

    $("#resultado_asignacion_auto").hide("slow");
       loader('start');
       $.post("modulos/gescom/bandeja.psi.php?cmd=listarActuacionesOperadorNew", { }, function(data){

            $("#resultado_asignacion_auto").empty();
            $("#resultado_asignacion_auto").html(data);
            loader('end');
            $("#resultado_asignacion_auto").show("slow");

            $.post("modulos/gescom/bandeja.psi.php?cmd=listarActuacionesOperadorAsignadas", { }, function(data){
                $("#cont_actuaciones_operador").empty();
                $("#cont_actuaciones_operador").html("Cargando actuaciones...");
                $("#cont_actuaciones_operador").html(data);
                
            });

        });
        
}

function verificaActuacionEnBandeja(idarea, codigo, tipo_pedido, idgestion, fecha_agenda, origen_bandeja, tabla_origen){
    $.post("modulos/gescom/bandeja.psi.php?cmd=verificaActuacionEnBandeja", {
                idactuacion:idgestion
            }, function(data){
                if(data==1){
                    registrarPedido(idarea, codigo, tipo_pedido, idgestion, fecha_agenda, origen_bandeja, tabla_origen);
                } else {
                    alert("El pedido fue liquidado. Se le est\xE1 asignando autom\xE1ticamente un nuevo pedido!");
                    iniOperador("psi");
                    return false;
                }
            });
}



function iniCANAL_ADM(){

	
    switchDivsResultados('main');
    loader('start');
	
	var flag_envio_pai = '';
    if($("#ckh_gestionadas_adm").attr('checked')){
		flag_envio_pai = '1';
    }
	
	var idcanal = $("#canal").attr("value");
	var idzonal = $("#zonal_canal").attr("value");
	var idgrupo = $("#grupo").attr("value");
	var idpto_vta = $("#slt_motivo").attr("value");
	
    $.post('modulos/gescom/bandeja.canal.php?cmd=listarPeticionesAdministrador', {
	    idcanal:idcanal,
		idzonal:idzonal,
		idgrupo:idgrupo,
		idpto_vta:idpto_vta,
		flag_envio_pai:flag_envio_pai
    }, function(data){
        $("#cont_resultado_main").empty();
        $("#cont_resultado_main").html(data);
		controlesOn();
        loader('end');
        
    });
}


