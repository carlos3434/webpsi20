function iniActualizacionData() {
    mostrarActualizacionData();
}

function mostrarActualizacionData(){
    /*
    $.post("modulos/gescom/actualizaciones.php", {cmd: "mostrarActualizacion" }, function(data){
        $("#cont_actualizacion_data").empty();
        $("#cont_actualizacion_data").html(data);

    });
    */

   $("#cont_info_actualizacion_data").html("");
    $.post("modulos/gescom/actualizaciones.php", {cmd: "mostrarActualizacion" }, function(data){
        $("#cont_info_actualizacion_data").empty();
        $("#cont_info_actualizacion_data").html(data);
        $("#cont_actualizacion_data").show("slide", {
                    direction: "down"
                }, 1000);

    });
}
//Para limpiar Alertas
function clearAndHiddenActualizacionData(){
   $("#cont_info_actualizacion_data").empty(); 
   $("#cont_actualizacion_data").css("display","none");
   
   $("#cont_info_actualizacion_data_Linea2").empty(); 
   $("#cont_actualizacion_data_Linea2").css("display","none");
}

function mostrarActualizacionData_Linea2(){

    $("#cont_info_actualizacion_data_Linea2").html("");
    $.post("modulos/gescom/actualizaciones.php", {cmd: "mostrarActualizacionLinea2" }, function(data){
        $("#cont_info_actualizacion_data_Linea2").empty();
        $("#cont_info_actualizacion_data_Linea2").append(data);
        $("#cont_actualizacion_data_Linea2").show("slide", {
                    direction: "down"
                }, 1000);

    });
}
