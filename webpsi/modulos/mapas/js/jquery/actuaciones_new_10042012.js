function buscar_new() {
    
    controlesOff_new();
    loader('start');

    var tipo    = $("#cmb_tipo").attr("value");
    var negocio = $("#cmb_negocio").attr("value");
    var abc     = $("#cmb_abc").attr("value");
    var campo   = $("#campo").attr("value");
    var valor   = $('#valor').attr("value").replace(/^\s+|\s+$/g, "");
    var flag_liquidadas = '';
    if($("#ckh_liquidadas").attr('checked')){
        flag_liquidadas = '1';
    }
    var orden   = $("#orden").attr("value");
    var direccion = $("#direccion").attr("value");
    var empresa = $("#cmb_empresa").attr("value");
    var estado  = $("#cmb_estado").attr("value");
    var zonal   = $("#cmb_zonal").attr("value");
    var mdf     = $("#cmb_mdf").attr("value");
    var lista_mdf = getCheckedBoxFrom("div", "div_list_mdfs", "1");
    var lista_segmento = getCheckedBoxFrom("div", "div_list_segmento", "1");
    //alert(lista_segmento);
    var flag_gestionado = '';
    if($("#ckh_gestionada").attr('checked')){
        flag_gestionado = '1';
    }
    var flag_criticos = '';
    if($("#ckh_criticos").attr('checked')){
        flag_criticos = '1';
    }
    var flag_visita = '';
    if($("#ckh_visita").attr('checked')){
        flag_visita = '2';
    }
    var nro_reiteradas = $("#cmb_reiteradas").attr("value");
    var segmento = $("#cmb_segmento").attr("value");

    var flag_con_speedy = "";
    if($("#ckh_con_speedy").attr("checked")){
        flag_con_speedy = "1";
    }
    var segm_prioridad = $("#cmb_seg_prioridad").attr("value");

    var flag_crm = "";
    if($("#ckh_crm").attr("checked")){
        flag_crm = "1";
    }
	var flag_movistar = "";
    if($("#ckh_movistar").attr("checked")){
        flag_movistar = "1";
    }
    
    var flag_rutina_off = "";
    if($("#chk_rutina_off").attr("checked")){
        flag_rutina_off = "1";
    }

    var tipo_gestion = $("#cmb_tipo_gestion").attr("value");    /* Agregado por Czegarraa el 15/02/2012 */
	
    $("#pag_actual").attr("value",'1');
    var pagina = $("#pag_actual").attr("value");

    $.post("modulos/gescom/bandeja.tai1_new.php?cmd=filtrarActuaciones", {
        tipo:tipo,
        negocio:negocio,
        abc:abc,
        campo:campo,
        valor:valor,
        orden:orden,
        direccion:direccion,
        pagina:pagina,
        empresa:empresa,
        estado:estado,
        zonal:zonal,
        mdf:mdf,
        lista_mdf:lista_mdf,
        flag_gestionado:flag_gestionado,
        flag_liquidadas:flag_liquidadas,
        flag_criticos:flag_criticos,
        nro_reiteradas:nro_reiteradas,
        segmento:segmento,
        lista_segmento:lista_segmento,
        flag_con_speedy:flag_con_speedy,
        flag_visita:flag_visita,
        segm_prioridad:segm_prioridad,
        flag_crm:flag_crm,
        flag_movistar:flag_movistar,
        flag_rutina_off:flag_rutina_off,
        tipo_gestion:tipo_gestion
    }, function(data){
        $("#cont_resultado_main").empty();
        $("#cont_resultado_main").html(data);
        controlesOn_new();
        loader('end');
    });
}

function ordenar_new(orden, direccion,vista) {
    $("#orden").attr("value",orden);
    $("#direccion").attr("value",direccion);
    buscar_new(vista);
}

function controlesOff_new() {
    
    $("#cmb_tipo").attr("disabled",'disable');
    $("#cmb_negocio").attr("disabled",'disable');
    $("#cmb_abc").attr("disabled",'disable');

    $("#campo").attr("disabled",'disable');
    $("#valor").attr("disabled",'disable');
    //$("#btn_buscar_ini").attr("disabled",'disable');
    $("#btn_limpiar").attr("disabled",'disable');

    $("#cmb_empresa").attr("disabled",'disable');
    $("#cmb_zonal").attr("disabled",'disable');
    $("#cmb_estado").attr("disabled",'disable');
    $("#cmb_reiteradas").attr("disabled",'disable');
    $("#btn_buscar_filtro").attr("disabled",'disable');
    $("#cmb_seg_prioridad").attr("disabled",'disable');
}

function controlesOn_new() {

    $("#cmb_tipo").attr("disabled",'');
    $("#cmb_negocio").attr("disabled",'');
    $("#cmb_abc").attr("disabled",'');

    $("#campo").attr("disabled",'');
    $("#valor").attr("disabled",'');
    //$("#btn_buscar_ini").attr("disabled",'');
    $("#btn_limpiar").attr("disabled",'');

    $("#cmb_empresa").attr("disabled",'');
    $("#cmb_zonal").attr("disabled",'');
    $("#cmb_estado").attr("disabled",'');
    $("#cmb_reiteradas").attr("disabled",'');
    $("#btn_buscar_filtro").attr("disabled",'');
    $("#cmb_seg_prioridad").attr("disabled",'');

    //$("#cmb_gest").attr("disabled",'');
}

function filtro_new(accion) {
    //$("#cont_filtros").toggle();
    
    if(accion=='close'){
    $("#filtro_contrae").css("display","none");
    $("#filtro_expande").css("display","block");
    $("#cont_filtros").hide("slide", {
        direction: "up"
        }, 500);
    }
    if(accion=='open'){
    $("#filtro_contrae").css("display","block");
    $("#filtro_expande").css("display","none");
    $("#cont_filtros").show("slide", {
        direction: "up"
        }, 500);
    }
}

function buscar_visor_l1() {
    
    loader('start');
    var zonal   = $("#cmb_zonal").attr("value");
    var empresa = $("#cmb_empresa").attr("value");
    var tipo    = $("#cmb_tipo").attr("value");
    var lista_mdf = getCheckedBoxFrom("div", "div_list_mdfs", "1");
    var abc     = $("#cmb_abc").attr("value");

    if(tipo==''){
        alert("Seleccione el Tipo de pedido!");
        $("#cmb_tipo").focus();
        loader('end');
        return;
    }

    $("#cmb_empresa").css("color","#000");
    $("#cmb_empresa").css("font-weight","normal");

    if(empresa==''){
        alert("Por favor, seleccione una Empresa!");
        loader('end');
        controlesOn_visor();
        $("#cmb_empresa").css("color","#FF0000");
        $("#cmb_empresa").css("font-weight","bold");
        $("#cmb_empresa").focus();
        return false;

    }
    //controlesOff_new();
    $.post("modulos/gescom/bandeja.tai1.visor.php?cmd=cargarVisor", {
        zonal:zonal,
        empresa:empresa,
        tipo:tipo,
        lista_mdf:lista_mdf,
        abc:abc
    }, function(data){
        $("#cont_resultado_main").empty();
        $("#cont_resultado_main").html(data);
        controlesOn_new();
        loader('end');
    });
}

function buscar_visor_l2() {
     
    loader('start');
    var zonal   = $("#cmb_zonal").attr("value");
    var empresa = $("#cmb_empresa").attr("value");
    var tipo    = $("#cmb_tipo").attr("value");
    var lista_mdf = getCheckedBoxFrom("div", "div_list_mdfs", "1");
    var abc     = $("#cmb_abc").attr("value");
    var area_prov = $("#cmb_area_prov").attr("value");
    var area_aver = $("#cmb_area_aver").attr("value");

    var crm = '';
    if( $("#cmb_crm").is(':checked') ) {
        crm = '1';
    }

    if(tipo==''){
        alert("Seleccione el Tipo de pedido!");
        $("#cmb_tipo").focus();
        loader('end');
        return;
    }

    $("#cmb_empresa").css("color","#000");
    $("#cmb_empresa").css("font-weight","normal");

    //editado 15-12-2011 por wsandoval
    /*
    if(empresa==''){
        alert("Por favor, seleccione una Empresa!");
        loader('end');
        controlesOn_visor();
        $("#cmb_empresa").css("color","#FF0000");
        $("#cmb_empresa").css("font-weight","bold");
        $("#cmb_empresa").focus();
        return false;

    }
    */
    //FIN editado 15-12-2011 por wsandoval
    
    //controlesOff_new();
    $.post("modulos/gescom/bandeja.tai1.visor_b2.php?cmd=cargarVisor", {
        zonal:zonal,
        empresa:empresa,
        tipo:tipo,
        lista_mdf:lista_mdf,
        abc:abc,
        area_prov:area_prov,
        area_aver:area_aver,
        crm:crm
    }, function(data){
        $("#cont_resultado_main").empty();
        $("#cont_resultado_main").html(data);
        controlesOn_new();
        loader('end');
    });
}

function cancelarLiquidacion(id_gestion_actuacion, tipo, origen , tabla) {
    loader('start');
    $("#childModal").html('');
    $("#childModal").css("background-color","#FFFFFF");

    $.post("modulos/gescom/consulta.php?cmd=verFormCancelLiquidacion", {
        id_gestion_actuacion: id_gestion_actuacion, tipo: tipo, origen: origen, tabla: tabla
    }, function(data){
        loader('end');
        $("#childModal").html(data);
    });

    $("#childModal").dialog({
        modal: true,
        width: '50%',
        hide: 'slide',
        title: 'Cancelar Liquidación',
        position:'top'
    });
}

function controlesOn_visor() {

    $("#cmb_tipo").attr("disabled",'');
    $("#btn_buscar_ini").attr("disabled",'');
    
    $("#cmb_empresa").attr("disabled",'');
    $("#cmb_zonal").attr("disabled",'');
    $("#cmb_estado").attr("disabled",'');
    $("#cmb_abc").attr("disabled",'');
    
    $("#cmb_crm").attr("disabled",'');
    
    if( $("#cmb_tipo").attr("value") == 'Provision' ) {
        $("#divAreaProv").show();
        $("#divAreaAver").hide();
    }else if( $("#cmb_tipo").attr("value") == 'Averia' ) {
        $("#divAreaProv").hide();
        $("#divAreaAver").show();
    }
    
}

function controlesOff_visor() {

    $("#cmb_tipo").attr("disabled",'disable');
    $("#btn_buscar_ini").attr("disabled",'disable');

    $("#cmb_empresa").attr("disabled",'disable');
    $("#cmb_zonal").attr("disabled",'disable');
    $("#cmb_estado").attr("disabled",'disable');
    $("#cmb_abc").attr("disabled",'disable');
    
    $("#cmb_crm").attr("disabled",'disable');
    
    $("#divAreaProv").hide();
    $("#divAreaAver").hide();

}

function listaVisorL1(tipo,empresa,zonal,abc,negocio,estado_gestion,grupo,rango, pagina){
    //alert("Estamos trabajando. Tenga un poquito de paciencia!");
    $("#cont_resultado_main").css("display",'none');
    $("#cont_grupo_negocio").css("display",'block');
    controlesOff_visor();
    var lista_mdf = getCheckedBoxFrom("div", "div_list_mdfs", "1");

    loader('start');
    $("#lista_grupo_negocio").html("Cargando...");
    $.post("modulos/gescom/bandeja.tai1.visor.php?cmd=listarGrupoPorNegocio", {
        tipo:tipo,
        empresa:empresa,
        zonal:zonal,
        negocio:negocio,
        estado_gestion:estado_gestion,
        grupo:grupo,
        rango:rango,
        pagina:pagina,
        lista_mdf:lista_mdf,
        abc:abc

    }, function(data){
        $("#lista_grupo_negocio").empty();
        $("#lista_grupo_negocio").html("Cargando...");
        $("#lista_grupo_negocio").html(data);
        //controlesOn_new();
        loader('end');
    });

}

function listaVisorL2(tipo,empresa,zonal,abc,negocio,estado_gestion,grupo,rango, pagina, area_identificada, area, crm){
    //alert("Estamos trabajando. Tenga un poquito de paciencia!");
    $("#cont_resultado_main").css("display",'none');
    $("#cont_grupo_negocio").css("display",'block');
    controlesOff_visor();
    var lista_mdf = getCheckedBoxFrom("div", "div_list_mdfs", "1");

    loader('start');
    $("#lista_grupo_negocio").html("Cargando...");
    $.post("modulos/gescom/bandeja.tai1.visor_b2.php?cmd=listarGrupoPorNegocio", {
        tipo:tipo,
        empresa:empresa,
        zonal:zonal,
        negocio:negocio,
        estado_gestion:estado_gestion,
        grupo:grupo,
        rango:rango,
        pagina:pagina,
        lista_mdf:lista_mdf,
        abc:abc,
        area_identificada:area_identificada,
        area:area,
        crm:crm

    }, function(data){
        $("#lista_grupo_negocio").empty();
        $("#lista_grupo_negocio").html("Cargando...");
        $("#lista_grupo_negocio").html(data);
        //controlesOn_new();
        loader('end');
    });

}

function returnVisorL1(){
    $("#cont_resultado_main").css("display",'block');
    $("#cont_grupo_negocio").css("display",'none');
    controlesOn_visor();
}

function iniCambiosAgenda() {
    cambiosAgenda();
    //setInterval("cambiosAgenda()", 1200000);
    setInterval("cambiosAgenda()", 600000);
}

function cambiosAgenda(){
    //$("#cont_info_alertas_reagenda").html(".");
    $.post("modulos/gescom/bandeja.tai1_new.php?cmd=alertaReagendas", {}, function(data){
        //alert(data);
        var num_reagendas = $("#txt_cont_alertas_reagenda").attr("value");
        var arrRetornaCadenaAgendadas = data.split("-reagendas|");
        
        retornaNumReAgendadas = arrRetornaCadenaAgendadas[0];
        retornaListaReAgendadas = arrRetornaCadenaAgendadas[1];
        if(retornaNumReAgendadas>0){

            var arrReagendasTipo = retornaListaReAgendadas.split(",");
            var reagendaAveria = arrReagendasTipo[0];
            var totalReagendaAveria = reagendaAveria.split(".")[0];

            var reagendaProvision = arrReagendasTipo[1];
            var totalReagendaProvision = reagendaProvision.split(".")[0];

            var reagendaRutina = arrReagendasTipo[2];
            var totalReagendaRutina = reagendaRutina.split(".")[0];
            //alert(totalReagendaAveria+"-"+totalReagendaProvision+"-"+totalReagendaRutina);

            $("#cont_info_alertas_reagenda").empty();
            $("#cont_info_alertas_reagenda").html(retornaNumReAgendadas);

            strAlertaReagenda = "";

            if(totalReagendaAveria>0){
                strAlertaReagenda = strAlertaReagenda + "<div onclick=\"listarReagendas('Averia')\" title=\"Ver reagendamientos\" style=\"cursor:pointer; color:#666; display:block; width:70px; margin-top:3px; margin-left:3px;\">Averias: &nbsp;&nbsp;<span style=\"color:red; font-weight: bold; font-size: 13px;\" id=\"cont_info_alertas_reagenda_averia\">"+totalReagendaAveria+"</span></div>";
            }
            if(totalReagendaProvision>0){
                strAlertaReagenda = strAlertaReagenda + "<div onclick=\"listarReagendas('Provision')\" title=\"Ver reagendamientos\" style=\"cursor:pointer; color:#666; display:block; width:70px;  margin-top:3px; margin-left:3px;\">Provision: <span style=\"color:red; font-weight: bold; font-size: 13px;\" id=\"cont_info_alertas_reagenda_provision\">"+totalReagendaProvision+"</span></div>";
            }
            if(totalReagendaRutina>0){
                strAlertaReagenda = strAlertaReagenda + "<div onclick=\"listarReagendas('Rutina')\" title=\"Ver reagendamientos\" style=\"cursor:pointer; color:#666; display:block; width:70px;  margin-top:3px; margin-left:3px;\">Rutinas: <span style=\"color:red; font-weight: bold; font-size: 13px;\" id=\"cont_info_alertas_reagenda_rutina\">"+totalReagendaRutina+"</span></div>";
            }
            $("#cont_reagendas_tipo").html(strAlertaReagenda);

        }

        if(retornaNumReAgendadas>0){
            if(num_reagendas=='0'){
                document.getElementById('txt_cont_alertas_reagenda').value=retornaNumReAgendadas;
                $("#cont_alerta_reagendas").show("slide", {
                    direction: "down"
                }, 1000);
            }
        } else {
            if(num_reagendas!='0'){
                document.getElementById('txt_cont_alertas_reagenda').value=retornaNumReAgendadas;
                $("#cont_alerta_reagendas").hide("slide", {
                    direction: "down"
                }, 1000);
            }
        }
    });
}

function listarReagendas(tipo){
    //alert(tipo_pedido);
    
    $("#cont_resultado_main").css("display",'none');
    $("#cont_resultados_reagendas").css("display",'block');
    controlesOff_new();
    loader('start');
    $("#cont_resultados_reagendas").empty();
    $("#cont_resultados_reagendas").html("Cargando...");
    $.post("modulos/gescom/bandeja.tai1_new.php?cmd=listarReagendas", {
        tipo:tipo
    }, function(data){
        $("#cont_resultados_reagendas").empty();
        $("#cont_resultados_reagendas").html(data);
        loader('end');
        //controlesOn_new();
    });

}

function returnListarReagendas(){
    $("#cont_resultado_main").css("display",'block');
    $("#cont_resultados_reagendas").css("display",'none');
    controlesOn_new();

}
