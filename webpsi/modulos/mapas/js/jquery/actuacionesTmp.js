function listarAlertas() {
    switchDivsResultados('agendadas');
}

function iniAlertas() {
    alertas();
    setInterval("alertas()", 60000);
}

function alertas(){
    $("#cont_info_alertas").html(".");
    $.post("/webunificada/modulos/gescom/bandeja.php?cmd=cargarAlertas", {}, function(data){
        var num_agendas = $("#txt_cont_alertas").attr("value");
        var arrRetornaCadenaAgendadas = data.split("-agendas|");

        retornaNumAgendadas = arrRetornaCadenaAgendadas[0];
        retornaListaAgendadas = arrRetornaCadenaAgendadas[1];

        $("#cont_info_alertas").empty();
        $("#cont_info_alertas").html(retornaNumAgendadas);

        if(retornaNumAgendadas>0){
            if(num_agendas=='0'){
                document.getElementById('txt_cont_alertas').value=retornaNumAgendadas;
                $("#cont_alertas").show("slide", {
                    direction: "down"
                }, 1000);
            }
            $("#cont_resultado_agendadas").empty();
            $("#cont_resultado_agendadas").html(retornaListaAgendadas);
        } else {
            if(num_agendas!='0'){
                document.getElementById('txt_cont_alertas').value=retornaNumAgendadas;
                $("#cont_alertas").hide("slide", {
                    direction: "down"
                }, 1000);
            }
        }
    });
}

function switchDivsResultados(contenedor) {
    if(contenedor=='main'){
        controlesOn();
        
        var tipo = $("#cmb_tipo").attr("value");
        
        if(tipo == '')
            controlesOff();
        
        //var abc = $("#cmb_abc").attr("value");

        //if(abc == '')
            //controlesOff();

        if(tipo != '')
            $("#cmb_abc").attr("disabled",'');

        $("#cmb_tipo").attr("disabled",'');
        $("#cont_resultado_main").css("display","block");
        $("#cont_resultado_agendadas").css("display","none");
        $("#cont_resultado_ultimos_movimientos").css("display","none");
    }
    if(contenedor=='agendadas'){
        controlesOff();
        $("#cmb_tipo").attr("disabled",'disable');
        $("#cont_resultado_main").css("display","none");
        $("#cont_resultado_agendadas").css("display","block");
        $("#cont_resultado_ultimos_movimientos").css("display","none");
    }
    if(contenedor=='movimientos'){
        controlesOff();
        $("#cmb_tipo").attr("disabled",'disable');
        $("#cont_resultado_main").css("display","none");
        $("#cont_resultado_agendadas").css("display","none");
        $("#cont_resultado_ultimos_movimientos").css("display","block");

    }
}

function iniUltimosMovimientos() {
    ultimosMovimientos();
    setInterval("ultimosMovimientos()", 60000);
    
}

function ultimosMovimientos(){
    //alert("ultimos...");
    $("#cont_info_movs").html(".");
    $.post("/webunificada/modulos/gescom/bandeja.php?cmd=nuevosMovimientos", {}, function(data){
        //alert(data);

        //$("#cont_resultado_ultimos_movimientos").html(data); //return false;
        var num_movs = $("#txt_cont_movs").attr("value");
        var arrRetornaCadenaMovimientos = data.split("-movs|");

        retornaNumMovimientos = arrRetornaCadenaMovimientos[0];
        retornaListaMovimientos = arrRetornaCadenaMovimientos[1];

        $("#cont_info_movs").empty();
        $("#cont_info_movs").html(retornaNumMovimientos);

        if(retornaNumMovimientos>0){
            if(num_movs=='0'){
                document.getElementById('txt_cont_movs').value=retornaNumMovimientos;
                $("#cont_movs").show("slide", {
                    direction: "down"
                }, 1000);
            }
            $("#cont_resultado_ultimos_movimientos").empty();
            $("#cont_resultado_ultimos_movimientos").html(retornaListaMovimientos);
        } else {
            if(num_movs!='0'){
                document.getElementById('txt_cont_movs').value=retornaNumMovimientos;
                $("#cont_movs").hide("slide", {
                    direction: "down"
                }, 1000);
            }
        }
    });
}

function listarUltimosMovs(){
    //alert("Estamos trabajando en eso... un poquito de paciencia!");
    switchDivsResultados('movimientos');
}

function filtro(accion)
{
    $("#cont_filtros").toggle();
}

function centrarElemento(objeto) {
    $('#'+objeto).css({
        position:'absolute',
        left: ($(window).width() - $('#'+objeto).outerWidth())/2,
        top: ($(window).height() - $('#'+objeto).outerHeight())/2
    });
}

function verMovimientos(idgestion) {
    $("#cont_actu_detalle").css("display","none");
    $("#cont_actu_movimientos").css("display","block");
    $("#cont_actu_movimientos").html("Cargando...");
    $.post("/webunificada/modulos/gescom/bandeja.php?cmd=cargarMovimientos", {
        idgestion: idgestion
    },
    function(data){
        $("#cont_actu_movimientos").empty();
        $("#cont_actu_movimientos").html(data);
    });
}

function cerrarMovimientos() {
    $("#cont_actu_detalle").css("display","block");
    $("#cont_actu_movimientos").css("display","none");
}

function detalleActuacion(codigo, tipo, gestion) {
    loader('start'); 
    $("#childModal").html('');
    $("#childModal").css("background-color","#FFFFFF");
    $.post("/webunificada/modulos/gescom/bandeja.php?cmd=cargarActuacion", {
        codigo: codigo, 
        tipo: tipo,
        gestion: gestion
    },
    function(data){
        loader('end');
        $("#childModal").html(data);       
    });

    $("#childModal").dialog({
        modal:true,
        width:'80%',
        hide: 'slide',
        title: 'DETALLE',
        position:'top'
    });
}

function registrarPedido(id, tipo, identificador, fecha_agenda, origen) {
    loader('start');
    $("#childModal").html('');
    $("#childModal").css("background-color","#FFFFFF");
    $.post("/webunificada/modulos/gescom/consulta.php?cmd=verFormGestion", {
        id_pedido: id,
        tipo: tipo,
        id_gestion_actuacion: identificador,
        fecha_agenda: fecha_agenda,
        origen: origen
    }, function(data){
        loader('end');
        $("#childModal").html(data);
    });
    $("#childModal").dialog({
        modal:true,
        width:'80%',
        hide: 'slide',
        title: 'GESTIONAR',
        position:'top'
    });
}

function movimientosActuacion(id_actuacion)
{
    $("#fondoOpaco").css("display","block");
    $("#cont_ga_"+id_actuacion).css("position","absolute");
    $("#cont_ga_"+id_actuacion).css("z-index","10");
    $("#cont_ga_"+id_actuacion).css("top","160px");
    $("#cont_ga_"+id_actuacion).show(300);
    $.post("/webunificada/modulos/gescom/bandeja.php?cmd=cargarMovimientos", {
        id_actuacion: id_actuacion
    },
    function(data){

        $("#cont_ga_"+id_actuacion).empty();
        $("#cont_ga_"+id_actuacion).html(data);
    });
}

function loader(estado)
{
    if(estado=='start')
    {
        $(".bgLoader").css("display","block");
    }
    if(estado=='end')
    {
        $(".bgLoader").css("display","none");
    }
}

function imprimirBoleta(tipo_actuacion, negocio,gestionado, codigo, idgestion_actuacion)
{
    var flag_gestion = gestionado;
    var nro_actuacion = codigo;
    var flag_tipo_actuacion;

    tipo_actuacion = tipo_actuacion.toUpperCase();
    negocio = negocio.toUpperCase();

    if (tipo_actuacion=="AVERIA")
    {
        flag_tipo_actuacion = "AVE";
    }
    else if (tipo_actuacion=="RUTINA")
    {
        flag_tipo_actuacion = "RUT";
    }
    else if (tipo_actuacion=="PROVISION")
    {
        flag_tipo_actuacion = "PROV";
    }

    //alert("tipo = "+flag_tipo_actuacion+", negocio="+negocio+", gestionado="+gestionado);
    var pag = "/webunificada/modulos/gescom/vistas/principal/boleta_eecc.php?flag_gestion="+flag_gestion+"&flag_tipo_actuacion="+flag_tipo_actuacion;
    pag = pag+"&nro_actuacion="+nro_actuacion+"&idgestion_actuacion="+idgestion_actuacion;
    //alert(pag);
    //window.open(pag);
    //$("#childModal").load("modulos/gescom/vistas/principal/boleta_eecc.php?flag_gestion="+flag_gestion+"&flag_tipo_actuacion="+flag_tipo_actuacion+"&nro_actuacion="+nro_actuacion+"&idgestion_actuacion="+idgestion_actuacion);
    
    $.get("modulos/gescom/vistas/principal/boleta_eecc.php", {
        flag_gestion:flag_gestion,
        flag_tipo_actuacion:flag_tipo_actuacion,
        nro_actuacion:nro_actuacion,
        idgestion_actuacion:idgestion_actuacion
    }, function(data) {
        //alert(data);
        $("#childModal").html(data);
    });
    
    
    $("#childModal").dialog({
        modal : true,
        width : '720px',
        hide : 'slide',
        position : 'top',
        title : "Imprimir boleta"
    });
	    
}

function imprimirSeleccion()
{
    if(getChkBlock('div','content')>0)
    {
        var seleccion = getCheckedBoxFrom('div', 'content', 1);
        $.post("/webunificada/modulos/gescom/bandeja.php?cmd=printSeleccion", {
            seleccion: seleccion
        },
        function(data){
            window.open("/webunificada/modulos/gescom/vistas/principal/boleta_eecc_masivo.php");
        });
    } else {
        alert("Debe seleccionar una o más actuaciones a imprimir!");
    }
}

function ini(vista)
{
    var strVista='';
    if(vista){
        strVista = '.'+vista;
    }

    switchDivsResultados('main');
    loader('start');
    var tipo    = $("#cmb_tipo").attr("value");
    var negocio = $("#cmb_negocio").attr("value");
    var abc     = $("#cmb_abc").attr("value");
    var campo   = $("#campo").attr("value");
    var valor   = $("#valor").attr("value");
    var flag_liquidadas = '';
    if($("#ckh_liquidadas").attr('checked')){
        flag_liquidadas = '1';
    }
    
    $.post("/webunificada/modulos/gescom/bandeja"+strVista+".php?cmd=listarActuacionesIni", {
        tipo:tipo,
        negocio:negocio,
        abc:abc,
        campo:campo,
        valor:valor,
        flag_liquidadas:flag_liquidadas
    }, function(data){
        $("#cont_resultado_main").empty();
        $("#cont_resultado_main").html(data);
        loader('end');
        controlesOn();
    });
}

function ordenar(orden, direccion,vista){
    $("#orden").attr("value",orden);
    $("#direccion").attr("value",direccion);
    //$("#pag_actual").attr("value",'1');
    buscar(vista);
}

function buscar(vista)
{

    var strVista='';
    if(vista){
        strVista = '.'+vista;
    }
    switchDivsResultados('main');
    loader('start');
    var tipo        = $("#cmb_tipo").attr("value");
    var negocio     = $("#cmb_negocio").attr("value");
    var abc         = $("#cmb_abc").attr("value");
    var campo       = $("#campo").attr("value");
    var valor       = $("#valor").attr("value");
    var flag_liquidadas = '';
    if($("#ckh_liquidadas").attr('checked')){
        flag_liquidadas = '1';
    }
    var orden       = $("#orden").attr("value");
    var direccion   = $("#direccion").attr("value");

    var empresa     = $("#cmb_empresa").attr("value");
    var estado      = $("#cmb_estado").attr("value");   
    var zonal       = $("#cmb_zonal").attr("value");
    var mdf         = $("#cmb_mdf").attr("value");
    var flag_gestionado = '';
    if($("#ckh_gestionada").attr('checked')){
        flag_gestionado = '1';
    }
    var nro_reiteradas  = $("#cmb_reiteradas").attr("value");
    var segmento        = $("#cmb_segmento").attr("value");

    $("#pag_actual").attr("value",'1');
    var pagina      = $("#pag_actual").attr("value");
    //alert(pagina);
    controlesOff();
    $.post("/webunificada/modulos/gescom/bandeja"+strVista+".php?cmd=filtrarActuaciones", {
        tipo:tipo,
        negocio:negocio,
        abc:abc,
        campo:campo,
        valor:valor,
        orden:orden,
        direccion:direccion,
        pagina:pagina,
        empresa:empresa,
        estado:estado,
        zonal:zonal,
        mdf:mdf,
        flag_gestionado:flag_gestionado,
        flag_liquidadas:flag_liquidadas,
        nro_reiteradas:nro_reiteradas,
        segmento:segmento
    }, function(data){
        $("#tb_resultado").empty();
        $("#tb_resultado").html(data);
        controlesOn();
        loader('end');
    });
}

function controlesOff()
{
    //$("#cmb_tipo").attr("disabled",'disable');
    $("#cmb_abc").attr("disabled",'disable');
    $("#cmb_negocio").attr("disabled",'disable');
    $("#campo").attr("disabled",'disable');
    $("#valor").attr("disabled",'disable');
    $("#btn_buscar_ini").attr("disabled",'disable');
    $("#btn_limpiar").attr("disabled",'disable');
//$("#cmb_estado").attr("disabled",'disable');
}

function controlesOn()
{
    //$("#cmb_tipo").attr("disabled",'');
    $("#cmb_abc").attr("disabled",'');
    $("#cmb_negocio").attr("disabled",'');
    $("#campo").attr("disabled",'');
    $("#valor").attr("disabled",'');
    $("#btn_buscar_ini").attr("disabled",'');
    $("#btn_limpiar").attr("disabled",'');
//$("#cmb_estado").attr("disabled",'');
}

function masMovimiento(idmovimiento)
{
    $("#info_mov_"+idmovimiento).css("display","block");
    $("#btn_mas_"+idmovimiento).css("display","none");
    $("#btn_menos_"+idmovimiento).css("display","block");
}

function masMovimientoCerrar(idmovimiento)
{
    $("#info_mov_"+idmovimiento).css("display","none");
    $("#btn_mas_"+idmovimiento).css("display","block");
    $("#btn_menos_"+idmovimiento).css("display","none");
}

function descargaReporte(){

    /*
     *  Todo el codigo para generar el reporte
     */
   // alert("Estamos trabajando en esto...");
    window.open("modulos/gescom/vistas/principal/rpt_descarga_tai.php",'mywin','left=20,top=20,width=700,height=400,toolbar=no,directories=no,menubar=no,status=no,resizable=1');
}