//var pr = null;
//var order_field = '';
//var order_type = '';
$(document).ready(function(){

    $("#cancelar_ajax").click(function(){
        D.pr.abort();
        loader("end");
    });
    
    $("#divReportes, #divReportesOpc").mouseover(function() {
    	$("#divReportesOpc").show();
    });
    $("#divReportes, #divReportesOpc").mouseout(function() {
    	$("#divReportesOpc").hide();
    });
    
    $('#chkOpcionesMdfNodo').click(function(event) {
		if($("#chkOpcionesMdfNodo").is(':checked')) { 
			$(".clsMdfNodo").attr('checked', 'checked');
		}else {  
			$(".clsMdfNodo").attr('checked', '');
		}
    });

    $("#btn_buscar").click(function() {
        D.order_field = '';
        D.order_type = '';
        $('#tb_resultado thead tr.headFilter a').removeClass("hd_orden_asc hd_orden_desc");
        
        //seteando pagina = 1, para iniciar la busqueda
        $("#pag_actual").val('1');
        
        //seteando a '' order_field y order_type
        $("#order_field").val('');
        $("#order_type").val('');
        
        D.buscarMdfNodo();
    });

    $("#campo_valor").keyup(function(e) {
        if(e.keyCode == 13) {
            D.order_field = '';
            D.order_type = '';
            $('#tb_resultado thead tr.headFilter a').removeClass("hd_orden_asc hd_orden_desc");
            
            //seteando pagina = 1, para iniciar la busqueda
            $("#pag_actual").val('1');
            
            //seteando a '' order_field y order_type
            $("#order_field").val('');
            $("#order_type").val('');
            
            D.buscarMdfNodo();
        }
    });
    
    $('#tb_resultado thead tr.headFilter a').click(function(event) {
        D.order_field  = $(this).attr("orderField");
        D.order_type   = $(this).attr("orderType");
        
        $("#order_field").val(D.order_field);
        $("#order_type").val(D.order_type);
        
        $(this).attr("orderType","desc");
        if( D.order_type == 'desc' ) {
            $(this).attr("orderType","asc");
        }
        
        D.buscarMdfNodo();
    });
    
    $('#tb_resultado thead tr.headFilter a').toggle(
        function() {
            $('#tb_resultado thead tr.headFilter a').removeClass("hd_orden_asc");
            $(this).addClass("hd_orden_desc"); 
        }, function () {
            $('#tb_resultado thead tr.headFilter a').removeClass("hd_orden_desc");
            $(this).addClass("hd_orden_asc");
        }
    );
    
    verCambiarEstadoAgendamiento = function() {
    	D.verCambiarEstadoAgendamiento();
    }
    
    verCambiarEstadoAgendamientoIndividual = function(params) {
    	D.verCambiarEstadoAgendamientoIndividual(params);
    }
    
    cambiarEstadoAgendamiento = function() {
    	D.cambiarEstadoAgendamiento();
    }
    
    verMdfNodoMovimientos = function(tipo, zonal, mdfNodo) {
    	D.verMdfNodoMovimientos(tipo, zonal, mdfNodo);
    }
    
    verRptMdfNodoPorEstado = function() {
    	D.verRptMdfNodoPorEstado();
    }
    
    descargarRptMdfNodoPorEstado = function() {
    	D.descargarRptMdfNodoPorEstado();
    }
    
    descargarRptMdfNodoPorEstadoPDF = function() {
    	D.descargarRptMdfNodoPorEstadoPDF();
    }
    
    verRptHistoricoMdfNodoPorEstado = function() {
    	D.verRptHistoricoMdfNodoPorEstado();
    }
    
    descargarRptHistoricoMdfNodoPorEstado = function() {
    	D.descargarRptHistoricoMdfNodoPorEstado();
    }
    
    descargarRptHistoricoMdfNodoPorEstadoPDF = function() {
    	D.descargarRptHistoricoMdfNodoPorEstadoPDF();
    }
    
    cierraDescargaMdfNodo = function() {
        $('ifrReporteMdfNodo').remove();
        $('#msgDescarga').hide();
    }

    cerrarVentana = function() {
        D.cerrarVentana();
    }
    

    var D = {

		pr : null,
		order_field : '',
		order_type : '',

		cerrarVentana: function() {
			$("#childModal").dialog('close');
		},
    		
		buscarMdfNodo: function() {
            var zonal        = $("#zonal").val();
            var campo_filtro = $.trim($("#campo_filtro").val());
            var campo_valor  = $.trim($("#campo_valor").val());
            var agendable    = $("#f_agendable").val();

            var page    = $("#pag_actual").val();
            var tl      = $("#tl_registros").val();
            var order_field = $("#order_field").val();
            var order_type = $("#order_type").val();

            $("#fl_mdf_nodo").attr('orderField', 'mdf_nodo.mdf');
            $("#fl_nombre").attr('orderField', 'mdf_nodo.nombre');
            $("#fl_fecha_insert").attr('orderField', 'mdf_nodo.fecha_insert');
            if ( campo_filtro == 'f_nodo' ) {
            	$("#fl_mdf_nodo").attr('orderField', 'mdf_nodo.nodo');
            	$("#fl_nombre").attr('orderField', 'mdf_nodo.desc_nodo');
                $("#fl_fecha_insert").attr('orderField', 'mdf_nodo.fecha_insert');
            }
            
            
            var data_content = { 
                pagina      : page, 
                total       : tl, 
                zonal       : zonal, 
                campo_filtro : campo_filtro,
                campo_valor : campo_valor,
                agendable   : agendable,
                order_field : order_field,
                order_type  : order_type
            };

            loader("start");
            D.pr = $.post("modulos/gescom/mantenimiento.agendamiento.php?cmd=filtroBusqueda", data_content, function(data){
            	
            	//oculto cabecera si tipo = mdf
            	if ( campo_filtro == 'f_nodo' ) {
            		$("#tb_resultado th:nth-child(7)").show();
            	} else {
            		$("#tb_resultado th:nth-child(7)").hide();
            	}
            	
            	$("#msgMdfNodo").hide();
                $("#tb_sup_tl_registros").show();
                $("#tb_pie_tl_registros").show();
                
                $("#tb_resultado").show();
                $("#tb_resultado tbody").empty();
                $("#tb_resultado tbody").append(data);
                loader("end");
            });
        },
        
        
        verCambiarEstadoAgendamiento: function() {		
			var chk_val = contar_check_temas(".clsMdfNodo");
			var arrMdfNodo = [];
			for( i in chk_val) {
				var mdfNodo = chk_val[i].split("|");
				arrMdfNodo.push(mdfNodo[0] + "|" + mdfNodo[1] + "|" + mdfNodo[2] + "|" + mdfNodo[3] + "|" + mdfNodo[4] + "|" + mdfNodo[5]);
			}
			
			if (chk_val.length > 0) {    
				loader('start');
				$("#childModal").html('Cargando...');
				$("#childModal").css("background-color","#FFFFFF");
				$.post("modulos/gescom/mantenimiento.agendamiento.php?cmd=verCambiarEstadoAgendamiento", {
					'arrMdfNodo' : arrMdfNodo
				},function(data){ 					
					loader('end');
					$("#childModal").html(data);
				});
				
				$("#childModal").dialog({modal:true, width:'600px', hide: 'slide', title: 'Cambio de estado de Agendamiento', position:'top'});

			}else{
				alert("Ningun Mdf/Nodo seleccionado para cambiar estado de agendamiento");
			}
		},
                
                
        verCambiarEstadoAgendamientoIndividual: function(params) {		
                    var arrMdfNodo = [];
                    var mdfNodo = params.split("|");
                    arrMdfNodo.push(mdfNodo[0] + "|" + mdfNodo[1] + "|" + mdfNodo[2] + "|" + mdfNodo[3] + "|" + mdfNodo[4] + "|" + mdfNodo[5]);

                    loader('start');
                    $("#childModal").html('Cargando...');
                    $("#childModal").css("background-color","#FFFFFF");
                    $.post("modulos/gescom/mantenimiento.agendamiento.php?cmd=verCambiarEstadoAgendamiento", {
                            'arrMdfNodo' : arrMdfNodo
                    },function(data){ 					
                            loader('end');
                            $("#childModal").html(data);
                    });

                    $("#childModal").dialog({modal:true, width:'600px', hide: 'slide', title: 'Cambio de estado de Agendamiento', position:'top'});
		},
        
        cambiarEstadoAgendamiento: function() {
        	
        	var motivo    = $.trim($("#motivo").val());
			var quienPide = $.trim($("#quien_pide").val());
			var agendable = $.trim($("#agendable").val());
			var tipo      = $.trim($("#tipo").val());
			
			if ( motivo == '' ) {
				alert('Debe ingresar el campo: motivo');
				$("#motivo").focus();
				return false;
			}
			if ( quienPide == '' ) {
				alert('Debe ingresar el campo: quien lo pide');
				$("#quien_pide").focus();
				return false;
			}
			if ( agendable == '' ) {
				alert('Debe ingresar el campo: es agendable');
				$("#agendable").focus();
				return false;
			}
			
        	
        	if ( !confirm("Seguro de registrar el cambio de estado de agendamiento?") ) {
            	return false;
            }
        	
        	var chk_val = contar_check_temas(".clsMdfNodoEstado");
			var arrMdfNodo = [];
			for( i in chk_val) {
				//var mdfNodo = chk_val[i].split("|");
				//arrMdfNodo.push(mdfNodo[0] + "|" + mdfNodo[1] + "|" + mdfNodo[2] + "|" + mdfNodo[3] + "|" + mdfNodo[4]);
				var mdfNodo = chk_val[i];
				arrMdfNodo.push(mdfNodo);
			}

			if (chk_val.length > 0) {    
				
				$("#msg").show();
	            $("input[id=btn_registrar]").attr('disabled','disabled');
				
				D.pr = $.post("modulos/gescom/mantenimiento.agendamiento.php?cmd=cambiarEstadoAgendamiento", {
					'arrMdfNodo' : arrMdfNodo,
					'tipo'		 : tipo,
					'motivo'	 : motivo,
					'quienPide'  : quienPide,
					'agendable'  : agendable
				}, function(data){
	                if(data['flag'] == '1') {
	                	$("#msg").hide();
	                	
	                	alert(data['mensaje']);
	                	
	                    //refresca la bandeja
	                	D.buscarMdfNodo();
	                	
	                	//cerramos la ventana
	                	D.cerrarVentana();
	                }else{
	                	$("#msg").hide();
	                    alert(data['mensaje']);
	                }
	                
	                $("input[id=btn_registrar]").attr('disabled','');
	                
	            },'json');
				
			}else{
				alert("Ningun Mdf/Nodo seleccionado para modificar el estado de agendamiento");
			}
        },
        
        verMdfNodoMovimientos: function(tipo, idMdfNodo) {
            loader('start');
            $("#childModal").html('Cargando...');
            $("#childModal").css("background-color","#FFFFFF");
            D.pr = $.post("modulos/gescom/mantenimiento.agendamiento.php?cmd=verMdfNodoMovimientos", {
            	tipo 	  : tipo, 
            	idMdfNodo : idMdfNodo
            },
            function(data){
                $("#childModal").html(data);
                loader('end');
            });

            $("#childModal").dialog({modal:true, width:'80%', hide: 'slide', title: 'Movimientos de cambios de Estado de Agendamiento', position:'top'});
        },
        
        verRptMdfNodoPorEstado: function() {
            var zonal        = $("#zonal").val();
            var campo_filtro = $.trim($("#campo_filtro").val());
            var campo_valor  = $.trim($("#campo_valor").val());
            var agendable    = $("#f_agendable").val();

            loader("start");
            $("#childModal").html('Cargando...');
            $("#childModal").css("background-color","#FFFFFF");
            D.pr = $.post("modulos/gescom/mantenimiento.agendamiento.php?cmd=verRptMdfNodoPorEstado", {
            	zonal        : zonal, 
                campo_filtro : campo_filtro,
                campo_valor  : campo_valor,
                agendable    : agendable
            }, 
            function(data){
                $("#childModal").html(data);
                loader('end');
            });

            $("#childModal").dialog({modal:true, width:'70%', hide: 'slide', title: 'Reporte de Mdf/Nodo por estado', position:'top'});
        },
        
        descargarRptMdfNodoPorEstado: function() {
        	if( confirm("Seguro de descargar el reporte en Excel?") ) {
        	
	            var zonal        = $("#zonal").val();
	            var campo_filtro = $.trim($("#campo_filtro").val());
	            var campo_valor  = $.trim($("#campo_valor").val());
	            var agendable    = $("#f_agendable").val();
	
	            var data_content = '';
	            data_content += '&zonal=' + zonal;
	            data_content += '&campo_filtro=' + campo_filtro;
	            data_content += '&campo_valor=' + campo_valor;
	            data_content += '&agendable=' + agendable;
	        	
	            $('#msgDescarga').show();
	            $('body').append('<iframe src="modulos/gescom/mantenimiento.agendamiento.php?cmd=descargarRptMdfNodoPorEstado' + data_content + '" id="ifrReporteMdfNodo" onload="parent.cierraDescargaMdfNodo();"></iframe>');

        	}
        },
        
        descargarRptMdfNodoPorEstadoPDF: function() {
        	if( confirm("Seguro de descargar el reporte en PDF?") ) {
                
                    var zonal        = $("#zonal").val();
	            var campo_filtro = $.trim($("#campo_filtro").val());
	            var campo_valor  = $.trim($("#campo_valor").val());
	            var agendable    = $("#f_agendable").val();
	            var data_content = '';
                    
	            data_content += '&zonal=' + zonal;
	            data_content += '&campo_filtro=' + campo_filtro;
	            data_content += '&campo_valor=' + campo_valor;
	            data_content += '&agendable=' + agendable;
                    
                    window.location.href = "modulos/gescom/reporteador/rpt.MDFNodoXEstado.php?" + data_content;
        	}
        },
        
        verRptHistoricoMdfNodoPorEstado: function() {
            var zonal        = $("#zonal").val();
            var campo_filtro = $.trim($("#campo_filtro").val());
            var campo_valor  = $.trim($("#campo_valor").val());
            var agendable    = $("#f_agendable").val();

            loader("start");
            $("#childModal").html('Cargando...');
            $("#childModal").css("background-color","#FFFFFF");
            D.pr = $.post("modulos/gescom/mantenimiento.agendamiento.php?cmd=verRptHistoricoMdfNodoPorEstado", {
            	zonal        : zonal, 
                campo_filtro : campo_filtro,
                campo_valor  : campo_valor,
                agendable    : agendable
            }, 
            function(data){
                $("#childModal").html(data);
                loader('end');
            });

            $("#childModal").dialog({modal:true, width:'70%', hide: 'slide', title: 'Reporte Historico de Mdf/Nodo por estado', position:'top'});
        },
        
        descargarRptHistoricoMdfNodoPorEstado: function() {
            
        	if( confirm("Seguro de descargar el reporte en Excel?") ) {
        	
	            var zonal        = $("#zonal").val();
	            var campo_filtro = $.trim($("#campo_filtro").val());
	            var campo_valor  = $.trim($("#campo_valor").val());
	            var agendable    = $("#f_agendable").val();
	
	            var data_content = '';
	            data_content += '&zonal=' + zonal;
	            data_content += '&campo_filtro=' + campo_filtro;
	            data_content += '&campo_valor=' + campo_valor;
	            data_content += '&agendable=' + agendable;
	        	
	            $('#msgDescarga').show();
	            $('body').append('<iframe src="modulos/gescom/mantenimiento.agendamiento.php?cmd=descargarRptHistoricoMdfNodoPorEstado' + data_content + '" id="ifrReporteMdfNodo" onload="parent.cierraDescargaMdfNodo();"></iframe>');

        	}
        },
        
        descargarRptHistoricoMdfNodoPorEstadoPDF: function() {
            
        	if( confirm("Seguro de descargar el reporte en PDF?") ) {
        	
	            var zonal        = $("#zonal").val();
	            var campo_filtro = $.trim($("#campo_filtro").val());
	            var campo_valor  = $.trim($("#campo_valor").val());
	            var agendable    = $("#f_agendable").val();
	
	            var data_content = '';
	            data_content += '&zonal=' + zonal;
	            data_content += '&campo_filtro=' + campo_filtro;
	            data_content += '&campo_valor=' + campo_valor;
	            data_content += '&agendable=' + agendable;
	        	
                    window.location.href = "modulos/gescom/reporteador/rpt.HistoricoMDFNodoXEstado.php?" + data_content;
        	}
        }
        
    };
});