function buscar_tdp() {
    
    switchDivsResultados('main');

    controlesOff_tdp();
    loader('start');

    var tipo    = $("#cmb_tipo").attr("value");
    var negocio = $("#cmb_negocio").attr("value");
    var abc     = $("#cmb_abc").attr("value");
    var campo   = $("#campo").attr("value");
    var valor   = $('#valor').attr("value").replace(/^\s+|\s+$/g, "");
    var flag_liquidadas = '';
    if($("#ckh_liquidadas").attr('checked')){
        flag_liquidadas = '1';
    }
    var orden   = $("#orden").attr("value");
    var direccion = $("#direccion").attr("value");
    var empresa = $("#cmb_empresa").attr("value");
    var estado  = $("#cmb_estado").attr("value");
    var zonal   = $("#cmb_zonal").attr("value");
    var mdf     = $("#cmb_mdf").attr("value");
    var lista_mdf = getCheckedBoxFrom("div", "div_list_mdfs", "1");
    var lista_segmento = getCheckedBoxFrom("div", "div_list_segmento", "1");
    //alert(lista_segmento);
    var flag_gestionado = '';
    if($("#ckh_gestionada").attr('checked')){
        flag_gestionado = '1';
    }
    var flag_criticos = '';
    if($("#ckh_criticos").attr('checked')){
        flag_criticos = '1';
    }
    var flag_visita = '';
    if($("#ckh_visita").attr('checked')){
        flag_visita = '2';
    }
    var nro_reiteradas = $("#cmb_reiteradas").attr("value");
    var segmento = $("#cmb_segmento").attr("value");

    var flag_con_speedy = "";
    if($("#ckh_con_speedy").attr("checked")){
        flag_con_speedy = "1";
    }
    var segm_prioridad = $("#cmb_seg_prioridad").attr("value");

    var flag_crm = "";
    if($("#ckh_crm").attr("checked")){
        flag_crm = "1";
    }
    var flag_movistar = "";
    if($("#ckh_movistar").attr("checked")){
        flag_movistar = "1";
    }

    var flag_rutina_off = "";
    if($("#chk_rutina_off").attr("checked")){
        flag_rutina_off = "1";
    }

    var tecnico     = $("#cmb_tecnico").attr("value");

    $("#pag_actual").attr("value",'1');
    var pagina = $("#pag_actual").attr("value");

    $.post("modulos/gescom/bandeja.tdp.php?cmd=filtrarActuaciones", {
        tipo:tipo,
        negocio:negocio,
        abc:abc,
        campo:campo,
        valor:valor,
        orden:orden,
        direccion:direccion,
        pagina:pagina,
        empresa:empresa,
        estado:estado,
        zonal:zonal,
        mdf:mdf,
        lista_mdf:lista_mdf,
        flag_gestionado:flag_gestionado,
        flag_liquidadas:flag_liquidadas,
        flag_criticos:flag_criticos,
        nro_reiteradas:nro_reiteradas,
        segmento:segmento,
        lista_segmento:lista_segmento,
        flag_con_speedy:flag_con_speedy,
        flag_visita:flag_visita,
        segm_prioridad:segm_prioridad,
        flag_crm:flag_crm,
        flag_movistar:flag_movistar,
        tecnico:tecnico,
        flag_rutina_off:flag_rutina_off
    }, function(data){
        $("#cont_resultado_main").empty();
        $("#cont_resultado_main").html(data);
        controlesOn_tdp();
        loader('end');
    });
}

function ordenar_tdp(orden, direccion) {
    $("#orden").attr("value",orden);
    $("#direccion").attr("value",direccion);
    buscar_tdp();
}

function reasignarContrata() {

        if($("#cmb_tipo").attr("value")!='Averia'){
            alert("ATENCION: Esta operacion esta permitida solamente para Averias!");
            return;
        }

	var chx = getCheckedBoxFrom("table", "tb_resultado", "1")

	if (chx == "") {
		alert("Debe seleccionar al menos una actuacion");
	} else {
                var random = (Math.random()*10);
		$.post("modulos/gescom/bandeja.tdp.php?cmd=vistaReasignarEmpresa&rdn="+random+"", {
                        strActu : chx
		}, function(data) {
			$("#childModal").html(data);
		});

		$("#childModal").dialog({
			modal : true,
			width : '500px',
			hide : 'slide',
			title : 'Re-Asignar Contrata',
			position : 'top'
		});
	}
}

function controlesOff_tdp() {

    $("#cmb_tipo").attr("disabled",'disable');
    $("#cmb_negocio").attr("disabled",'disable');
    $("#cmb_abc").attr("disabled",'disable');

    $("#campo").attr("disabled",'disable');
    $("#valor").attr("disabled",'disable');

    $("#cmb_empresa").attr("disabled",'disable');
    $("#cmb_zonal").attr("disabled",'disable');
    $("#cmb_estado").attr("disabled",'disable');
    $("#cmb_reiteradas").attr("disabled",'disable');
    $("#btn_buscar_filtro").attr("disabled",'disable');
    $("#cmb_seg_prioridad").attr("disabled",'disable');
}

function controlesOn_tdp() {

    $("#cmb_tipo").attr("disabled",'');
    $("#cmb_negocio").attr("disabled",'');
    $("#cmb_abc").attr("disabled",'');

    $("#campo").attr("disabled",'');
    $("#valor").attr("disabled",'');

    $("#cmb_empresa").attr("disabled",'');
    $("#cmb_zonal").attr("disabled",'');
    $("#cmb_estado").attr("disabled",'');
    $("#cmb_reiteradas").attr("disabled",'');
    $("#btn_buscar_filtro").attr("disabled",'');
    $("#cmb_seg_prioridad").attr("disabled",'');
}
