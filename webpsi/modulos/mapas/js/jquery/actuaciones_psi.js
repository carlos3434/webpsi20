var cronPsi;

function solicitarPedidoPSI(){
    stopVerificaPedidosLibres();
    loader('start');

    $.post("modulos/gescom/bandeja.psi_new.php?cmd=solicitarNuevoPedido", { }, function(data){

        $("#cont_new_asignada").empty();
        $("#cont_new_asignada").html(data);
        loader('end');

    });

}

function pedidoPendienteOperadorPSI(){
    loader('start');

    $.post("modulos/gescom/bandeja.psi_new.php?cmd=pedidoPendienteOperadorPSI", { }, function(data){

        $("#cont_new_asignada").empty();
        $("#cont_new_asignada").html(data);
        loader('end');

    });

}

function verificaActuacionEnBandejaNew(modulo,idarea, codigo, tipo_pedido, idgestion, fecha_agenda, origen_bandeja, tabla_origen){
    $.post("modulos/gescom/bandeja.psi_new.php?cmd=verificaActuacionEnBandeja", {
                idactuacion:idgestion
            }, function(data){
                if(data==1){
                    registrarPedido(idarea, codigo, tipo_pedido, idgestion, fecha_agenda, origen_bandeja, tabla_origen);
                } else {
                    //alert("El pedido fue liquidado. Se le est\xE1 asignando autom\xE1ticamente un nuevo pedido!");
                    alert("El pedido ha cambiado de Estado y saldr\xE1 de su bandeja. Disculpe las molestias!");
                    if(modulo=='pendiente'){
                        solicitarPedidoPSIPrev();
                        pedidoPendienteOperadorPSI();
                        listar_asignadas_operador();    
                    }
                    if(modulo=='historial'){
                        listar_asignadas_operador();    
                    }

                    
                    return false;
                }
            });
}

function solicitarPedidoPSIPrev(){
    //alert("Te va a asignar un nuevo pedido! acepta naa mas!");
    $("#cont_new_asignada").css('display','none');
    $("#gest_btn_asignar_2").css('display','block');
    //solicitarPedidoPSI();
    //solicitarPedidoPSI();
}

function ini_psi(vista) {
    buscar_psi(vista);
}

function buscar_psi(vista) {
    var strVista='';
    if(vista){
        strVista = '.'+vista;
    }
    switchDivsResultados('main');
    loader('start');
    var tipo    = $("#cmb_tipo").attr("value");
    var negocio = $("#cmb_negocio").attr("value");
    var abc     = $("#cmb_abc").attr("value");
    var campo   = $("#campo").attr("value");
    var valor   = $('#valor').attr("value").replace(/^\s+|\s+$/g, "");
    var flag_liquidadas = '';
    if($("#ckh_liquidadas").attr('checked')){
        flag_liquidadas = '1';
    }
    var orden   = $("#orden").attr("value");
    var direccion = $("#direccion").attr("value");
    var empresa = $("#cmb_empresa").attr("value");
    var estado  = $("#cmb_estado").attr("value");
    var zonal   = $("#cmb_zonal").attr("value");
    var mdf     = $("#cmb_mdf").attr("value");
    var lista_mdf = getCheckedBoxFrom("div", "div_list_mdfs", "1");
    var lista_segmento = getCheckedBoxFrom("div", "div_list_segmento", "1");
    //alert(lista_segmento);
    var flag_gestionado = '';
    if($("#ckh_gestionada").attr('checked')){
        flag_gestionado = '1';
    }
    var flag_criticos = '';
    if($("#ckh_criticos").attr('checked')){
        flag_criticos = '1';
    }
    var nro_reiteradas = $("#cmb_reiteradas").attr("value");
    var segmento = $("#cmb_segmento").attr("value");

    var flag_con_speedy = "";
    if($("#ckh_con_speedy").attr("checked")){
        flag_con_speedy = "1";
    }
    var flag_crm = '';
    if($("#ckh_crm").attr('checked')){
        flag_crm = '1';
    }


    $("#pag_actual").attr("value",'1');
    var pagina = $("#pag_actual").attr("value");

    controlesOff();
    $.post("modulos/gescom/bandeja.psi_new.php?cmd=filtrarActuaciones", {
        tipo:tipo,
        negocio:negocio,
        abc:abc,
        campo:campo,
        valor:valor,
        orden:orden,
        direccion:direccion,
        pagina:pagina,
        empresa:empresa,
        estado:estado,
        zonal:zonal,
        mdf:mdf,
        lista_mdf:lista_mdf,
        flag_gestionado:flag_gestionado,
        flag_liquidadas:flag_liquidadas,
        flag_criticos:flag_criticos,
        nro_reiteradas:nro_reiteradas,
        segmento:segmento,
        lista_segmento:lista_segmento,
        flag_con_speedy:flag_con_speedy,
        flag_crm:flag_crm
    }, function(data){
        $("#cont_resultado_main").empty();
        $("#cont_resultado_main").html(data);
        controlesOn();
        loader('end');
    });
}

function ordenar_psi(orden, direccion,vista) {
    $("#orden").attr("value",orden);
    $("#direccion").attr("value",direccion);
    buscar_psi(vista);
}

function asignarOperadorPsi() {
    var chx = getCheckedBoxFrom("div", "content", "1")

    if (chx == "") {
        alert("Debe seleccionar al menos una actuacion");
    } else {
        $.post("modulos/gescom/bandeja.psi_new.php?cmd=vistaAsignarOperador", { strActu : chx }, function(data) {
            $("#childModal").html("Cargando...");
            $("#childModal").html(data);
        });
        $("#childModal").dialog({
            modal : true,
            width : '500px',
            hide : 'slide',
            title : 'Asignar Operador',
            position : 'top'
        });
    }
}

function listar_asignadas_operador(){
    
    loader('start')

    var campo   = $("#campo").attr("value");
    var valor   = $("#valor").attr("value");
    var estado  = $("#cmb_estado").attr("value");
    var segmento_top  = $("#cmb_segmento_top").attr("value");
    
    $.post("modulos/gescom/bandeja.psi_new.php?cmd=listarActuacionesOperadorAsignadas", {
        campo:campo,
        valor:valor,
        estado:estado,
        segmento_top:segmento_top
        }, function(data){
        $("#cont_actuaciones_operador").empty();
        $("#cont_actuaciones_operador").html("Cargando actuaciones...");
        //$("#cont_actuaciones_operador").html("");
        $("#cont_actuaciones_operador").html(data);
        loader('end')
    });
}

function iniVerificaPedidosLibres() {
    verificaPedidosLibres();
    cronPsi = setInterval("verificaPedidosLibres()", 10000);
}

function verificaPedidosLibres(){
    $.post("modulos/gescom/bandeja.psi_new.php?cmd=verificaPedidosLibresOperador", { }, function(data){
        if(data=='0'){
            //alert('No hay pedidos para asignar aun!');
            cadena="";
        } else {

            $("#cont_new_asignada").css('display','none');
            $("#gest_btn_asignar").css('display','none');
            $("#gest_btn_asignar_2").css('display','none');
            //$("#gest_btn_auto_detecta").css('display','block');
            
            $("#gest_btn_auto_detecta").show("slide", {
                    direction: "down"
                }, 1000);
            stopVerificaPedidosLibres();
        }

        //$("#cont_new_asignada").empty();
        //$("#cont_new_asignada").html(data);
    });
}

function stopVerificaPedidosLibres(){
    //var refreshIntervalId = setInterval(fname, 10000);
    clearInterval(cronPsi);
}