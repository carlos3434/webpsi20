/**
 * scripts para la gestion de los canales
 * @author Ricardo De la Torre
 * 20-05-2011
 */


function descargar_canal(id_usuario){


    loader('start');
    $("#childModal").html('');
    $("#childModal").css("background-color","#FFFFFF");
    $.post("/webunificada/modulos/gescom/bandeja.canal.php?cmd=descargarArchivos", {
        id_usuario: id_usuario
    },
    function(data){
        loader('end');
        $("#childModal").html(data);
    });

    $("#childModal").dialog({
        modal : true,
        width : '500px',
        hide : 'slide',
        title : 'Descargar Archivos ',
        position : 'top'
    });
}

