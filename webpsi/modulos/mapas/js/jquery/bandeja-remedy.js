function descargar_archivo(nom_archivo) {
    if(nom_archivo != '')
        window.top.location="modulos/gescom/bandeja.remedy.php?cmd=descargar&nom_archivo="+nom_archivo;
    else
        alert('No se puede iniciar la descarga porque el archivo no existe');
}
function cargaMasiva() {
    $("#childModal").html('');
    $("#childModal").css("background-color","#FFFFFF");
    $("#cargando").show();        
    
    $.post("modulos/gescom/bandeja.remedy.php?cmd=vistaCargaMasiva", {  }, function(data) {
        $("#cargando").hide();
        $("#childModal").html(data);            
    });
    
    $("#childModal").dialog({
        modal: true,
        width: '50%',
        hide: 'slide',
        title: 'CARGA MASIVA',
        position:'center'
    });
}
function cargaArchivo() {
    $("#childModal").html('');
    $("#childModal").css("background-color","#FFFFFF");
    $("#cargando").show();

    $.post("modulos/gescom/bandeja.remedy.php?cmd=vistaCargaArchivo", {  }, function(data) {
        $("#cargando").hide();
        $("#childModal").html(data);
    });

    $("#childModal").dialog({
        modal: true,
        width: '50%',
        hide: 'slide',
        title: 'CARGA ARCHIVO REMEDY',
        position:'center'
    });
}
