function registrar_parametro(){

    var zonal   = $("#cmb_zonal").attr("value");
    var negocio = $("#cmb_negocio").attr("value");
    var lista_mdf       = getCheckedBoxFrom("div", "div_list_mdfs", "1");
    var lista_segmento  = getCheckedBoxFrom("div", "div_list_segmento", "1");
    var empresa = $("#cmb_empresa").attr("value");
    
    var flag_conf_tdp = flag_conf_contrata = flag_tipo_averia = flag_tipo_provision = flag_tipo_rutina = '0';
    var flag_tipo_otro1 = flag_tipo_otro2 = flag_neg_adsl = flag_neg_tba = flag_neg_catv = '0';
    var cond_asdl  = cond_tba_ = cond_catv = '2';
    
    if($("#chk_conf_tdp").attr('checked')) flag_conf_tdp = '1';
    if($("#chk_conf_contrata").attr('checked')) flag_conf_contrata = '1';
    if($("#chk_tipo_averia").attr('checked')) flag_tipo_averia = '1';
    if($("#chk_tipo_provision").attr('checked')) flag_tipo_provision = '1';
    if($("#chk_tipo_rutina").attr('checked')) flag_tipo_rutina = '1';
    if($("#chk_tipo_otro1").attr('checked')) flag_tipo_otro1 = '1';
    if($("#chk_tipo_otro2").attr('checked')) flag_tipo_otro2 = '1';
    if($("#chk_neg_adsl").attr('checked')) flag_neg_adsl = '1';
    if($("#chk_neg_tba").attr('checked')) flag_neg_tba = '1';
    if($("#chk_neg_catv").attr('checked')) flag_neg_catv = '1';

    cond_asdl   = $("#cmb_cond_adsl").attr("value");
    cond_tba    = $("#cmb_cond_tba").attr("value");
    cond_catv   = $("#cmb_cond_catv").attr("value");

    var opcion_save  = $("#opcion").attr("value");

    //alert(cond_asdl+""+cond_tba+""+cond_catv);
    loader("start");

    $.post("administracion_liqtec.php?cmd=registrar_parametro", {
        zonal:zonal,
        negocio:negocio,
        lista_mdf:lista_mdf,
        lista_segmento:lista_segmento,
        empresa:empresa,
        flag_conf_tdp:flag_conf_tdp,
        flag_conf_contrata:flag_conf_contrata,
        flag_tipo_averia:flag_tipo_averia,
        flag_tipo_provision:flag_tipo_provision,
        flag_tipo_rutina:flag_tipo_rutina,
        flag_tipo_otro1:flag_tipo_otro1,
        flag_tipo_otro2:flag_tipo_otro2,
        flag_neg_adsl:flag_neg_adsl,
        flag_neg_tba:flag_neg_tba,
        flag_neg_catv:flag_neg_catv,
        cond_asdl:cond_asdl,
        cond_tba:cond_tba,
        cond_catv:cond_catv

    }, function(data){
        loader("end");
        
        if(data=='registrado'){
            alert("El Parametro se ha registrado correctamente!");
            if(opcion_save=='1'){
                window.location='administracion_liqtec.php?cmd=list';
            } else {
                window.location='administracion_liqtec.php?cmd=add';
            }
            
        }
        if(data=='existe'){
            alert("ERROR: El Parametro ya se encuentra registrado!");
            //window.location='administracion_liqtec.php?cmd=list';
        }
        if(data=='error'){
            alert("ERROR: No se pudo registrar el parametro!");
            //window.location='administracion_liqtec.php?cmd=add';
        }
    });

    
}

function registrar_pref(){

    var negocio     = $("#cmb_negocio").attr("value");
    var tipo_pedido = $("#cmb_tipo_pedido").attr("value");
    var prefijo     = $("#txt_prefijo").attr("value");

    if(tipo_pedido=='') {
        alert("Seleccione el tipo de pedido!");
        $("#cmb_tipo_pedido").focus();
        return;
    }
    if(negocio=='') {
        alert("Seleccione el negocio!");
        $("#cmb_negocio").focus();
        return;
    }
    if(prefijo=='') {
        alert("Ingrese el nombre del Prefijo!");
        $("#txt_prefijo").focus();
        return;
    }

    var opcion_save  = $("#opcion").attr("value");

    //alert(cond_asdl+""+cond_tba+""+cond_catv);
    loader("start");

    $.post("administracion_liqtec.php?cmd=registrar_prefijo", {
        negocio:negocio,
        tipo_pedido:tipo_pedido,
        prefijo:prefijo

    }, function(data){
        loader("end");
        
        if(data=='registrado'){
            alert("El Prefijo se ha registrado correctamente!");
            if(opcion_save=='1'){
                window.location='administracion_liqtec.php?cmd=main_pref';
            } else {
                window.location='administracion_liqtec.php?cmd=add_pref';
            }

        }
        if(data=='existe'){
            alert("ERROR: El Prefijo ya se encuentra registrado!");
            
        }
        if(data=='error'){
            alert("ERROR: No se pudo registrar el Prefiji!");

        }
    });


}

/**
 * Realiza la busqueda de los parametros segun los filtros selecionados
 *
 * @method buscar_liq_param
 * @return void
 */
function buscar_liq_param() {

    var orden   = $("#orden").attr("value");
    var direccion = $("#direccion").attr("value");

    var zonal   = $("#cmb_zonal").attr("value");
    var negocio = $("#cmb_negocio").attr("value");
    var lista_mdf       = getCheckedBoxFrom("div", "div_list_mdfs", "1");
    var lista_segmento  = getCheckedBoxFrom("div", "div_list_segmento", "1");
    var empresa = $("#cmb_empresa").attr("value");

    var default_checks = '';
    if($("#chk_forzar_estados").attr('checked')){
        default_checks = '0';
    }
    var flag_conf_tdp = flag_conf_contrata = flag_tipo_averia = flag_tipo_provision = flag_tipo_rutina = default_checks;
    var flag_tipo_otro1 = flag_tipo_otro2 = flag_neg_adsl = flag_neg_tba = flag_neg_catv = default_checks;

    if($("#chk_conf_tdp").attr('checked')) flag_conf_tdp = '1';
    if($("#chk_conf_contrata").attr('checked')) flag_conf_contrata = '1';
    if($("#chk_tipo_averia").attr('checked')) flag_tipo_averia = '1';    
    if($("#chk_tipo_provision").attr('checked')) flag_tipo_provision = '1';
    if($("#chk_tipo_rutina").attr('checked')) flag_tipo_rutina = '1';   
    if($("#chk_tipo_otro1").attr('checked')) flag_tipo_otro1 = '1';
    if($("#chk_tipo_otro2").attr('checked')) flag_tipo_otro2 = '1';
    if($("#chk_neg_adsl").attr('checked')) flag_neg_adsl = '1';
    if($("#chk_neg_tba").attr('checked')) flag_neg_tba = '1';
    if($("#chk_neg_catv").attr('checked')) flag_neg_catv = '1';

    $("#pag_actual").attr("value",'1');
    var pagina = $("#pag_actual").attr("value");

    
    var tipo_seleccion = '';
    tipo_seleccion = $("input[name='opt_masivo']:checked").val();
    //alert(tipo_seleccion);
    if(!(tipo_seleccion=='seleccion' || tipo_seleccion=='filtro')) tipo_seleccion = 'filtro';

    loader("start");
    
    $.post("administracion_liqtec.php?cmd=filtrar", {
        zonal:zonal,
        negocio:negocio,
        lista_mdf:lista_mdf,
        lista_segmento:lista_segmento,
        empresa:empresa,
        flag_conf_tdp:flag_conf_tdp,
        flag_conf_contrata:flag_conf_contrata,
        flag_tipo_averia:flag_tipo_averia,
        flag_tipo_provision:flag_tipo_provision,
        flag_tipo_rutina:flag_tipo_rutina,
        flag_tipo_otro1:flag_tipo_otro1,
        flag_tipo_otro2:flag_tipo_otro2,
        flag_neg_adsl:flag_neg_adsl,
        flag_neg_tba:flag_neg_tba,
        flag_neg_catv:flag_neg_catv,
        pagina:pagina,
        orden:orden,
        direccion:direccion,
        tipo_seleccion:tipo_seleccion
        
    }, function(data){
        $("#cont_resultado_main").empty();
        $("#cont_resultado_main").html(data);
        loader("end");

    });
}

/**
 * Cambia el estado de un parametro segun el campo de configuracion
 *
 * @method actualizaEstado
 * @param String indice
 * @param String campo
 * @param Int new_estado
 * @return void
 */

 function buscar_pref_liq_param(){

    var prefijo     = $("#txt_pref").attr("value");
    var negocio     = $("#cmb_negocio").attr("value");
    var tipo_pedido = $("#cmb_tipo_pedido").attr("value");

    var orden       = $("#orden").attr("value");
    var direccion   = $("#direccion").attr("value");
    $("#pag_actual").attr("value",'1');
    var pagina = $("#pag_actual").attr("value");

    loader("start");

    $.post("administracion_liqtec.php?cmd=filtrar_pref", {
        prefijo:prefijo,
        negocio:negocio,
        tipo_pedido:tipo_pedido,
        pagina:pagina,
        orden:orden,
        direccion:direccion
    }, function(data){
        $("#cont_resultado_main").empty();
        $("#cont_resultado_main").html(data);
        loader("end");

    });
 }

function actualizaEstado(indice,campo,new_estado){

    loader('start');
    $.post("administracion_liqtec.php?cmd=cambiar_parametro", {
        indice:indice,
        campo:campo,
        new_estado:new_estado
    }, function(data){
        if(data=='1'){
            if(new_estado=='1'){
                cadena_html = "<a href=\"javascript:actualizaEstado('"+indice+"','"+campo+"','0')\" ><img src=\"img/estado_habilitado.png\"  alt=\"Deshabilitar\" title=\"Deshabilitar\"  /></a>";
            }
            if(new_estado=='0'){
                cadena_html = "<a href=\"javascript:actualizaEstado('"+indice+"','"+campo+"','1')\" ><img src=\"img/estado_deshabilitado_gris.png\"  alt=\"Habilitar\" title=\"Habilitar\"  /></a>";
            }
            $("#"+indice+"-"+campo).html(cadena_html);
        }
    });
    loader('end');

}

/**
 * habilita/deshabilita los checks del filtro de parametrizacion
 *
 * @method activaCheckFiltro
 * @return void
 */
function activaCheckFiltro(){

    var check = 'checked';
    if($("#chk_forzar_estados").attr('checked')){
        check = 'checked';
    } else {
        check = '';
    }
    
    $("#chk_conf_tdp").attr('checked',check);
    $("#chk_conf_contrata").attr('checked',check);
    $("#chk_tipo_averia").attr('checked',check);
    $("#chk_tipo_provision").attr('checked',check);
    $("#chk_tipo_rutina").attr('checked',check);
    $("#chk_tipo_otro1").attr('checked',check);
    $("#chk_tipo_otro2").attr('checked',check);
    $("#chk_neg_adsl").attr('checked',check);
    $("#chk_neg_tba").attr('checked',check);
    $("#chk_neg_catv").attr('checked',check);


}

/**
 * Cambia el estado masivamente de un parametro segun seleccion o filtrado
 *
 * @method actualizaEstadoMasivo
 * @param String campo
 * @param Int new_estado
 * @return void
 */
function actualizaEstadoMasivo(campo,new_estado){

    var tipo_seleccion = seleccion = zonal = negocio = lista_mdf = lista_segmento = empresa = '';
    tipo_seleccion  = $("input[name='opt_masivo']:checked").val();

    loader('start');

    if(tipo_seleccion=='seleccion'){
      seleccion  = getCheckedBoxFrom("table", "tb_resultado", "1");
      
      if(seleccion==''){
          loader('end');
          alert("Debes seleccionar uno o mas parametros!");
          return;
      }
      
    }

    if(tipo_seleccion=='filtro'){
        zonal   = $("#cmb_zonal").attr("value");
        negocio = $("#cmb_negocio").attr("value");
        lista_mdf       = getCheckedBoxFrom("div", "div_list_mdfs", "1");
        lista_segmento  = getCheckedBoxFrom("div", "div_list_segmento", "1");
        empresa = $("#cmb_empresa").attr("value");

    }
    loader('start');
    $.post("administracion_liqtec.php?cmd=cambiar_parametro_masivo", {
        campo:campo,
        new_estado:new_estado,
        tipo_seleccion:tipo_seleccion,
        seleccion:seleccion,
        zonal:zonal,
        negocio:negocio,
        lista_mdf:lista_mdf,
        lista_segmento:lista_segmento,
        empresa:empresa
    }, function(data){
        loader('end');
        if(data=='1'){
            buscar_liq_param();
            

        }
    });
    
}

/**
 * Habilita el Combo para el cambio de la condicion de un negocio
 *
 * @method optActualizaCondicional
 * @param String indice
 * @param String negocio
 * @return void
 */
function optActualizaCondicional(indice,negocio){
    
    $("#cmb_"+indice+"-"+negocio+"_test").css("display","block");
    $("#opt_"+indice+"-"+negocio+"_test").css("display","none");
    var indice_actual = $("#indice_selec").attr("value");
    var negocio_actual = $("#negocio_selec").attr("value");

    if(indice_actual==''){
        $("#indice_selec").attr("value",indice);
        $("#negocio_selec").attr("value",negocio);
    } else {
        $("#cmb_"+indice_actual+"-"+negocio_actual+"_test").css("display","none");
        $("#opt_"+indice_actual+"-"+negocio_actual+"_test").css("display","block");
        $("#indice_selec").attr("value",indice);
        $("#negocio_selec").attr("value",negocio);
    }

}

/**
 * Actualiza el cambio de estado de un combo de negocio seleccionado
 *
 * @method actualizaCondicional
 * @param String indice
 * @param String negocio
 * @param String valor
 * @return void
 */
function actualizaCondicional(indice,negocio,valor){

    loader('start');
    $.post("administracion_liqtec.php?cmd=cambiar_condicional_negocio", {
        indice:indice,
        negocio:negocio,
        valor:valor
    }, function(data){
        //alert(data);
        if(data==1){
            if(valor==0) desc_cond = "OK";
            if(valor==1) desc_cond = "C/P";
            if(valor==2) desc_cond = "S/P";

            $("#opt_"+indice+"-"+negocio+"_test").html(desc_cond);
            $("#opt_"+indice+"-"+negocio+"_test").css("display","block");
            $("#cmb_"+indice+"-"+negocio+"_test").css("display","none");

        }
    });
    loader('end');

}

/**
 * Check/No check de todas las casillas del listado de Parametros
 *
 * @method select_unselect_check_param
 * @return void
 */
function select_unselect_check_param(){

    if($("#chk_all").is(':checked')){
        $('.lista_chk').attr('checked',true);
    } else {
        $('.lista_chk').attr('checked',false);
    }

}

/**
 * Actualiza masivamente los parametros de verificacion de prueba por negocio
 *
 * @method actualizaCondicionalMasivo
 * @param String negocio_test
 * @param String new_estado
 * @return void
 */
function actualizaCondicionalMasivo(negocio_test,new_estado){

    if(new_estado=='') return;
    
    var tipo_seleccion = seleccion = zonal = negocio = lista_mdf = lista_segmento = empresa = '';
    tipo_seleccion  = $("input[name='opt_masivo']:checked").val();

    loader('start');

    if(tipo_seleccion=='seleccion'){
      seleccion  = getCheckedBoxFrom("table", "tb_resultado", "1");

      if(seleccion==''){
          loader('end');
          alert("Debes seleccionar uno o mas parametros!");
          return;
      }

    }

    if(tipo_seleccion=='filtro'){
        zonal   = $("#cmb_zonal").attr("value");
        negocio = $("#cmb_negocio").attr("value");
        lista_mdf       = getCheckedBoxFrom("div", "div_list_mdfs", "1");
        lista_segmento  = getCheckedBoxFrom("div", "div_list_segmento", "1");
        empresa = $("#cmb_empresa").attr("value");

    }
    campo = negocio_test+"_cond_test";
    //alert(campo);
    loader('start');
    $.post("administracion_liqtec.php?cmd=cambiar_parametro_masivo", {
        campo:campo,
        new_estado:new_estado,
        tipo_seleccion:tipo_seleccion,
        seleccion:seleccion,
        zonal:zonal,
        negocio:negocio,
        lista_mdf:lista_mdf,
        lista_segmento:lista_segmento,
        empresa:empresa
    }, function(data){
        
        loader('end');
        if(data=='1'){
            buscar_liq_param();
        } else {
            alert("Error al hacer los cambios! Por favor vuelva a intentarlo mas tarde.");
        }
    });

}
