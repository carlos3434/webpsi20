/**
 *
 * [Web Unificada] :: Scripts para la bandeja de PAI Comercial
 *
 * @package     /js/jquery
 * @name
 * @category	js
 * @author	Carlos Zegarra Adrianzen <czegarraa@gmd.com.pe>
 * @copyright	2011 Telefonica del Peru
 * @version     1.0 - 2011/11/04
 */

/* Variables iniciales */
var cronPai;

/**
* Solicita la asignación de un pedido para el operador PAI
*
* @method solicitarPedidoPAI
* @return void
*/
function solicitarPedidoPAI(){
    
    stopVerificaPedidosLibresPAI();
    loader('start');

    $.post("modulos/gescom/bandeja.pai.com.php?cmd=solicitarNuevoPedido", { }, function(data){
        
        $("#cont_new_asignada").empty();
        $("#cont_new_asignada").html(data);
        loader('end');

    });

}

/**
* Obtiene el pedido actual asignado al Operador
*
* @method pedidoPendienteOperadorPAI
* @return void
*/
function pedidoPendienteOperadorPAI(){
    loader('start');

    $.post("modulos/gescom/bandeja.pai.com.php?cmd=pedidoPendienteOperadorPAI", { }, function(data){

        $("#cont_new_asignada").empty();
        $("#cont_new_asignada").html(data);
        loader('end');

    });

}

/**
* Verifica si el pedido actual asignado al Operador se encuentra con un estado perteneciente al area PAI
*
* @method verificaActuacionEnBandejaPAI
* @return void
*/
function verificaActuacionEnBandejaPAI(modulo,idarea, codigo, tipo_pedido, idgestion, fecha_agenda, origen_bandeja, tabla_origen){
    
    $.post("modulos/gescom/bandeja.pai.com.php?cmd=verificaActuacionEnBandeja", {
                peticion:codigo
            }, function(data){
                
                if(data==1){
                    registrarPedido(idarea, codigo, tipo_pedido, idgestion, fecha_agenda, origen_bandeja, tabla_origen);
                    //solicitarPedidoPAIPrev(); // comentar esta linea si se descomenta la linea anterior
                } else {
                    //alert("El pedido fue liquidado. Se le est\xE1 asignando autom\xE1ticamente un nuevo pedido!");
                    alert("El pedido ha cambiado de Estado y saldr\xE1 de su bandeja. Disculpe las molestias!");
                    if(modulo=='pendiente'){
                        solicitarPedidoPAIPrev();
                        pedidoPendienteOperadorPAI();
                        listarAsignadasOperadorPAI();
                    }
                    if(modulo=='historial'){
                        listarAsignadasOperadorPAI();
                    }
                    return false;
                }
            });
}

/**
* Muestra el Div con el boton para Solicitar la asignacion de un pedido
*
* @method solicitarPedidoPAIPrev
* @return void
*/
function solicitarPedidoPAIPrev(){
    
    $("#cont_new_asignada").css('display','none');
    $("#gest_btn_asignar_2").css('display','block');
    listarAsignadasOperadorPAI();
    
}

/**
* Carga los pedidos gestionados por un Operador PAI que le fueron asignados
*
* @method listarAsignadasOperadorPai
* @return void
*/
function listarAsignadasOperadorPAI(){
    
    loader('start')

    var campo   = $("#campo").attr("value");
    var valor   = $("#valor").attr("value");
    var estado  = $("#cmb_estado").attr("value");
    var segmento_top  = $("#cmb_segmento_top").attr("value");
    var operacion   = $("#cmb_tipo_operacion").attr("value");
    
    $.post("modulos/gescom/bandeja.pai.com.php?cmd=listarActuacionesOperadorAsignadas", {
        campo:campo,
        valor:valor,
        estado:estado,
        segmento_top:segmento_top,
        operacion:operacion
        }, function(data){
        $("#cont_actuaciones_operador").empty();
        $("#cont_actuaciones_operador").html("Cargando actuaciones...");
        //$("#cont_actuaciones_operador").html("");
        $("#cont_actuaciones_operador").html(data);
        loader('end')
    });
}

/**
* Inicia el proceso recurrente de verificación de pedidos disponibles para ser asignados a un Operador PAI
*
* @method iniVerificaPedidosLibresPAI
* @return void
*/
function iniVerificaPedidosLibresPAI() {
    verificaPedidosLibresPAI();
    cronPai = setInterval("verificaPedidosLibresPAI()", 10000);
}

/**
* Verificaca la disponibilidad de pedidos para ser asignados a un Operador PAI
*
* @method verificaPedidosLibresPAI
* @return void
*/
function verificaPedidosLibresPAI(){
    $.post("modulos/gescom/bandeja.pai.com.php?cmd=verificaPedidosLibresOperador", { }, function(data){
        
        if(data=='0'){
            //alert('No hay pedidos para asignar aun!');
            cadena="";
        } else {

            $("#cont_new_asignada").css('display','none');
            $("#gest_btn_asignar").css('display','none');
            $("#gest_btn_asignar_2").css('display','none');
            //$("#gest_btn_auto_detecta").css('display','block');
            
            $("#gest_btn_auto_detecta").show("slide", {
                    direction: "down"
                }, 1000);
            stopVerificaPedidosLibresPAI();
        }

        //$("#cont_new_asignada").empty();
        //$("#cont_new_asignada").html(data);
    });
}

/**
* Detiene el proceso recurrente de verificación de pedidos disponibles para ser asignados a un Operador PAI
*
* @method stopVerificaPedidosLibresPAI
* @return void
*/
function stopVerificaPedidosLibresPAI(){
    clearInterval(cronPai);
}

/**
* Busca todos los pedidos para la bandeja del Supervisor PAI definido los parametros de su perfil
*
* @method buscarPAICom
* @return void
*/
function buscarPAICom() {

    loader('start');

    var operacion   = $("#cmb_tipo_operacion").attr("value");
    var motivo      = $("#cmb_motivo").attr("value");
    var campo       = $("#campo").attr("value");
    var valor       = $('#valor').attr("value").replace(/^\s+|\s+$/g, "");

    var flag_liquidadas = '';
    if($("#ckh_liquidadas").attr('checked')){
        flag_liquidadas = '1';
    }
    var flag_sin_empresa = '';
    if($("#ckh_sin_empresa").attr('checked')){
        flag_sin_empresa = '1';
    }
    var flag_observacion_call = '';
    if($("#ckh_observacion").attr('checked')){
    	flag_observacion_call = '1';
    }
    var flag_envio_canal = '';
    if($("#ckh_canal").attr('checked')){
    	flag_envio_canal = '1';
    }
    var flag_agenda_canal = '';
    if($("#ckh_agenda_canal").attr('checked')){
    	flag_agenda_canal = '1';
    }
    
    var orden       = $("#orden").attr("value");
    var direccion   = $("#direccion").attr("value");
    var empresa     = $("#cmb_empresa").attr("value");
    var estado      = $("#cmb_estado").attr("value");
    var zonal       = $("#cmb_zonal").attr("value");
    var operador    = $("#cmb_operador").attr("value");
    var prioridad   = $("#cmb_prioridad").attr("value");
    var producto = $("#cmb_producto").attr("value");


    var lista_mdf = getCheckedBoxFrom("div", "div_list_mdfs", "1");
    var lista_segmento = getCheckedBoxFrom("div", "div_list_segmento", "1");


    $("#pag_actual").attr("value",'1');
    var pagina = $("#pag_actual").attr("value");

    controlesOff();
    $.post("modulos/gescom/bandeja.pai.com.php?cmd=filtrarActuaciones", {
        operacion:operacion,
        motivo:motivo,
        campo:campo,
        valor:valor,
        orden:orden,
        direccion:direccion,
        pagina:pagina,
        empresa:empresa,
        estado:estado,
        zonal:zonal,
        flag_liquidadas:flag_liquidadas,
        operador:operador,
        prioridad:prioridad,
        mdfs:lista_mdf,
        segmentos:lista_segmento,
        producto:producto,
        flag_sin_empresa:flag_sin_empresa,
        flag_observacion_call:flag_observacion_call,
        flag_envio_canal:flag_envio_canal,
        flag_agenda_canal:flag_agenda_canal
    }, function(data){
        $("#cont_resultado_main").empty();
        $("#cont_resultado_main").html(data);
        controlesOn();
        loader('end');
    });
}

/**
* Ordena los pedidos ASC/DESC en la bandeja del Supervisor PAI
*
* @method ordenarPAICom
* @return void
*/
function ordenarPAICom(orden, direccion,vista) {
    $("#orden").attr("value",orden);
    $("#direccion").attr("value",direccion);
    buscarPAICom();
}

/**
* Muestra la pantalla con los pedidos seleccionados para ser asignados a un Operador PAI
*
* @method asignarOperadorPaiCom
* @return void
*/
function asignarOperadorPaiCom() {

    var chx = getCheckedBoxFrom("div", "content", "1")

    if (chx == "") {
        alert("Debe seleccionar al menos una peticion");
    } else {
        $.post("modulos/gescom/bandeja.pai.com.php?cmd=vistaAsignarOperador", { strPeticiones : chx }, function(data) {
            $("#childModal").html(data);
        });
        $("#childModal").dialog({
            modal : true,
            width : '500px',
            hide : 'slide',
            title : 'Asignar Operador PAI',
            position : 'top'
        });
    }
}

