function imprimirBoletasDespacho(){

	var chkVal = getCheckedBoxFrom("table", "tb_resultado", "1");
    var arrChkVal = chkVal.split(",");  
    var arrChk = new Array();
    var i=0;
    for(r=1;r<arrChkVal.length;r++){
        exp1 = arrChkVal[r].split("|");
        arrChk[i] = exp1[0]+"|"+exp1[1]+"|"+exp1[2]+"|"+exp1[3]+"|"+exp1[4]+"|"+exp1[7];
        i++;
    }
    
    if (arrChk.length>0){           
        $("#childModal").html("");
        var ventimp = window.open(' ', 'popimpr', 'left=0,top=0,width=50,height=30,toolbar=no,directories=no,menubar=no,status=no,resizable=1');
		//ventimp.document.write("Cargado impresion...");
        $.post("modulos/gescom/vistas/principal/boleta_eecc_masivo.php", {
            printAll : "Ok",
            seleccion : arrChk
        },function(data){            
            $("#childModal").html(data);
            var ficha = document.getElementById("printAllId");
  		    ventimp.document.write( ficha.innerHTML );
  		    ventimp.document.close();
  		    ventimp.print( );
  		    ventimp.close();
        });
        $("#childModal").dialog({
            modal : true,
            width : '750px',
            maxHeight : '500px',
            hide : 'slide',
            position : 'top',
            title : "Imprimir boleta (vista previa)"
        });
    }else{
        alert("Debe seleccionar una o más actuaciones a imprimir!");
    }
}

function imprimirBoletaInd(tipo_actuacion, negocio, gestionado, codigo, idgestion_actuacion) {
    var flag_gestion = gestionado;
    var nro_actuacion = codigo;
    var flag_tipo_actuacion;

    tipo_actuacion = tipo_actuacion.toUpperCase();
    negocio = negocio.toUpperCase();

    if (tipo_actuacion=="AVERIA")
    {
        flag_tipo_actuacion = "AVE";
    }
    else if (tipo_actuacion=="RUTINA")
    {
        flag_tipo_actuacion = "RUT";
    }
    else if (tipo_actuacion=="PROVISION")
    {
        flag_tipo_actuacion = "PROV";
    }
    
    //limpiar boleta
    $("#childModal").html("");
    var ventimp = window.open(' ', 'popimpr', 'left=0,top=0,width=50,height=30,toolbar=no,directories=no,menubar=no,status=no,resizable=1');
	
    $.get("modulos/gescom/vistas/principal/boleta_eecc.php",{
        flag_gestion:flag_gestion,
        flag_tipo_actuacion:flag_tipo_actuacion,
        nro_actuacion:nro_actuacion,
        idgestion_actuacion:idgestion_actuacion
    },function(data){
        //alert(data);
        $("#childModal").html(data);
        var ficha = document.getElementById("printAllId");
	    ventimp.document.write( ficha.innerHTML );
	    ventimp.document.close();
	    ventimp.print( );
	    ventimp.close();
    });
    
    $("#childModal").dialog({
        modal : true,
        width : '720px',
        hide : 'slide',
        position : 'top',
        title : "Imprimir boleta (vista previa)"
    });
}


function contador_imp_boleta_ind(){
	setTimeout(function(){
		$("div#printAllId").printArea();	
	},5000);    
}

function contador_imp_boleta_mas(){
    cont_img++;
    if(cont_aver==cont_img){
        $("div#printAllId").printArea();
    }
}