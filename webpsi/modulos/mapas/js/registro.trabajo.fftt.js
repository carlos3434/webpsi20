$(document).ready(function(){
    $("#cmb_tipoRegistro").change(function(){
        var tipo_registro = $(this).val(); 
        clearForm();
        if(tipo_registro==""){ 
            $("#cmb_tipoNegocio option:eq(0)").attr("selected", "selected");
            $("#cmb_tipoNegocio").attr("disabled","disabled");
            $(".filtroIndividual").addClass("hide");
            $(".filtroElemento").addClass("hide");
        }
        else{
            if(tipo_registro=='I'){
                $(".filtroElemento").addClass("hide");
            }
            else if(tipo_registro=='P'){
                $(".filtroIndividual").addClass("hide");
            }
            $("#cmb_tipoNegocio option:eq(0)").attr("selected", "selected");
            $("#cmb_tipoNegocio").attr("disabled","");
        }
    });
   
    $("#cmb_tipoNegocio").change(function(){
        var tipo_negocio    = $(this).val(); 
        var tipo_registro   = $("#cmb_tipoRegistro").val();
        clearForm();
        if(tipo_negocio==""){ 
            $(".filtroIndividual").addClass("hide");
            $(".filtroElemento").addClass("hide");
        }
        else if(tipo_registro=='I'){
            if(tipo_negocio=='STB' || tipo_negocio=='ADSL'){
                $(".txtCampo").val('');
                $(".filtroIndividual").addClass("hide");
                $(".nroTelefono").removeClass("hide");
                $("input[name=nroTelefono]").focus();
            }
            else if(tipo_negocio=='CATV'){
                $(".txtCampo").val('');
                $(".filtroIndividual").addClass("hide");
                $(".codCliente").removeClass("hide");
                $("input[name=codCliente]").focus();
            }
        }else if(tipo_registro=='P'){
            if(tipo_negocio=='STB' || tipo_negocio=='ADSL'){
//                $(".filtroElemento").addClass("hide");
                $(".filtroElemento").removeClass("hide");
                $("#cmb_elemento").attr("disabled","disabled");
            }
            else if(tipo_negocio=='CATV'){
                $(".filtroElemento").addClass("hide");
                $(".elemento").removeClass("hide");
                $("#cmb_elemento").attr("disabled","");
                
                getElemento_by_CATV();
            }
            
            //MODIFICACIONES DEL DIA 11/08/2011
//            $(".filtroElemento").removeClass("hide");
            $("#cmb_tipo_red option:eq(0)").attr("selected", "selected");
            $("select[name=cmb_elemento]").html("<option value=''>Seleccione</option>");
//            $("#cmb_elemento").attr("disabled","disabled");
        }        
    });
    
    $("#cmb_tipo_red").change(function(){
        var tipo_red = $(this).val(); 
        clearForm();
        $("select[name=cmb_elemento]").html("<option value=''>Seleccione</option>");
        if(tipo_red==""){ 
            $("#cmb_elemento").attr("disabled","disabled");
        }else{
            $("#cmb_elemento").attr("disabled","");
            var tipo_negocio = $("#cmb_tipoNegocio").val()
            //Ajax: Traer los elementos referentes al tipo de Negocio y Tipo de Red
            $.post("modulos/gescom/registro.trabajo.fftt.php?cmd=getElemento_by_Negocio_and_TipoRed", {
                tipo_negocio: tipo_negocio, 
                tipo_red: tipo_red,
                is_pex: '0'
            },
            function(data) {
                $.each(data, function(i, item) {
                    $("select[name=cmb_elemento]").append("<option value='"+data[i].value+"' class='"+data[i].hide+"'>"+data[i].text+"</option>")
                });
            },'json');
        }
    });    
    
    $("#cmb_elemento").change(function(){
        //si estoy en la bandeja ya no traigo el template del elemento de red
        if( $("#is_bandeja").val() == 1 ) {
            return false;
        }
        
        clearForm();
        var tipo_registro   = $("#cmb_tipoRegistro").val();
        var tipo_negocio    = $("#cmb_tipoNegocio").val();
        var tipo_red        = $("#cmb_tipo_red").val();
        var cod_elemento    = $("#cmb_elemento").val();
        var nom_elemento    = ($("#cmb_elemento option:selected").text());
      
        if(tipo_negocio==''){alert('Seleccione correctamente un tipo de Negocio.');return false;}
        if(tipo_red=='' && tipo_negocio!='CATV'){alert('Seleccione correctamente un tipo de Red.');return false;}
        if(cod_elemento==''){alert('Seleccione correctamente un elemento de Red.');return false;}
        loader('start');
        $.post("modulos/gescom/registro.trabajo.fftt.php?cmd=buscarElemento", {
            tipo_negocio    : tipo_negocio, 
            tipo_registro   : tipo_registro,
            tipo_red        : tipo_red,
            cod_elemento    : cod_elemento,
            nom_elemento    : nom_elemento
        },
        function(data) {
            loader('end');
            $("#frmRegistroPEX").html(data);
        }); 
//        return true;
    });
    
    getElemento_by_CATV = function(){
        var tipo_negocio = $("#cmb_tipoNegocio").val()
        //Ajax: Traer los elementos referentes al tipo de Negocio y Tipo de Red
        $.post("modulos/gescom/registro.trabajo.fftt.php?cmd=getElemento_by_Negocio_and_TipoRed", {
            tipo_negocio: tipo_negocio
        },
        function(data) {
            $.each(data, function(i, item) {
                $("select[name=cmb_elemento]").append("<option value='"+data[i].value+"' class='"+data[i].hide+"'>"+data[i].text+"</option>")
            });
        },'json');
    }
    
    $('input[name=buscarCliente]').click(function(){
        var tipo_registro   = $("#cmb_tipoRegistro").val();
        var tipo_negocio    = $("#cmb_tipoNegocio").val();
        var campo_busqueda;
        
        if(tipo_negocio=='STB' || tipo_negocio=='ADSL'){
            campo_busqueda= $.trim($("input[name=nroTelefono]").val());
            if(campo_busqueda==''){
                alert('Ingrese correctamente el Nro. Telefonico del Cliente.');
                $("input[name=nroTelefono]").focus();
                return false; 
            }          
        }else{
            campo_busqueda = $.trim($("input[name=codCliente]").val());            
            if(campo_busqueda==''){
                alert('Ingrese correctamente el Cod. del Cliente.');
                $("input[name=codCliente]").focus();
                return false; 
            }          
        }
        //PASA LAS VALIDACIONES MINIMAS REQUERIDAD
        clearForm();
        loader('start');
        $.post("modulos/innovaPTR/registro.pexBAK.php?cmd=buscarCliente", {
            tipo_negocio    : tipo_negocio, 
            tipo_registro   : tipo_registro,
            campo_busqueda  : campo_busqueda
        },
        function(data) {
            loader('end');
            $("#frmRegistroPEX").html(data);
        }); 
    });
    
    changeElemento=function(){
        var tipo_negocio    = $("input[name=negocio]").val();
        var cod_elemento    = $("select[name=codelementoptr]").val();
        
        if(cod_elemento == '19'){ 
            var primario = $.trim($('input[name=primario]').val());
            if(primario==''){$('input[name=primario]').attr('readonly', false).focus();}
        }else{ 
            $('input[name=primario]').attr('readonly', 'readonly').val($('input[name=tmp_primario]').val());}
        
        if(cod_elemento == '6' || cod_elemento == '12' || cod_elemento == '13' || cod_elemento == '18' || cod_elemento == '17' || cod_elemento == '16'){ 
            var direccioncliente = $.trim($('input[name=direccioncliente]').val());
            if(direccioncliente==''){$('input[name=direccioncliente]').attr('readonly', false).focus();}
        }else{ 
            $('input[name=direccioncliente]').attr('readonly', 'readonly').val($('input[name=tmp_direccioncliente]').val());}
        
        $("select[name=codcasuisticapex]").html("<option value=''>Seleccione</option>");
        $("td[title=codprioridadpex]").html("<input name='codprioridadpex' type='hidden' value=''/><span class='nomprioridadpex'></span>");
        if(cod_elemento != ''){
            $.post("modulos/innovaPTR/registro.pexBAK.php?cmd=getCasuistica_by_Elemento_and_Negocio", {
                tipo_negocio    : tipo_negocio,
                cod_elemento    : cod_elemento
            },
            function(data) {
                $.each(data, function(i, item) {
                    $("select[name=codcasuisticapex]").append("<option value='"+data[i].codcasuistica+"'>"+data[i].descripcion+"</option>")
                });
            },"json");
        }
    }
    
    changeCasuistica=function(){
        var tipo_negocio    = $("input[name=negocio]").val();
        var cod_elemento    = $("select[name=codelementoptr]").val();
        var cod_casuistica  = $("select[name=codcasuisticapex]").val();

        $("td[title=codprioridadpex]").html("<input name='codprioridadpex' type='hidden' value=''/><span class='nomprioridadpex'></span>");
        if(cod_casuistica!=''){
            $.post("modulos/innovaPTR/registro.pexBAK.php?cmd=getPrioridad_by_CodCasuistica", {
                tipo_negocio    : tipo_negocio,
                cod_elemento    : cod_elemento,
                cod_casuistica  : cod_casuistica
            },
            function(data) {
                if(data.tipo=='A'){
                    $("input[name=codprioridadpex]").val(data.valor);
                    $("span[class=nomprioridadpex]").html(data.text);
                }else if(data.tipo=='S'){
                    $("td[title=codprioridadpex]").append(data.valor);
                }
            },'json');
        }
    }   
    
    changeJefatura=function(){
        var cod_jefatura    = $("select[name=codjefatura]").val();
        var tipo_negocio    = $("input[name=negocio]").val();
              
        var cod_zonal   = $("select[name=codjefatura] option:selected").attr('title');
        var cod_eecc    = $("select[name=codjefatura] option:selected").attr('class');
        var nom_zonal   = $("select[name=codjefatura] option:selected").attr('lang');
            
        $("input[name=codzonal]").val(cod_zonal);
        $("input[name=nomzonal]").val(nom_zonal);
        $("input[name=codeecc]").val(cod_eecc);
        
        $("select[class=changeJefatura]").html("<option value=''>Seleccione</option>");
        $("input[name=direccioncliente]").attr('readonly','readonly').val('');

        
        if(cod_jefatura!=''){
            getDepartamento(cod_zonal);
            
            if(tipo_negocio=='STB' || tipo_negocio=='ADSL' || tipo_negocio == 'CATV'){      
                $.post("modulos/gescom/registro.trabajo.fftt.php?cmd=getMdfNodo_byZonal_Ecc_Tipo", {
                    tipo_negocio    : tipo_negocio,
                    cod_zonal       : cod_zonal,
                    cod_eecc        : cod_eecc,
                    cod_jefatura    : cod_jefatura
                },
                function(data) {
                    $.each(data, function(i, item) {
                        if(tipo_negocio=='STB' || tipo_negocio=='ADSL'){
                            $("select[name=mdf]").append("<option value='"+data[i].mdf_nodo+"'>"+data[i].mdf_nodo+"</option>")
                        }else{
                            $("select[name=nodo]").append("<option value='"+data[i].mdf_nodo+"'>"+data[i].mdf_nodo+"</option>")
                        }
                    });
                },"json");
            } 
        }    
    }
    
    getDepartamento = function(cod_zonal){
        $.post("modulos/gescom/registro.trabajo.fftt.php?cmd=getDepartamento_by_Zonal", {
            cod_zonal:cod_zonal
        },
        function(data) {
            for(i=0;i<data.length;i++){
                $("select[name=coddepartamento]").append("<option value='"+data[i].coddpto+"'>"+data[i].nombre+"</option>")
            }
        },"json");
    } 

    resetFrm = function(){
        if(confirm("\u00BFEst\u00E1 seguro de limpiar lo llenado de la PEX\u003F")){
            $("#frmRegistroPEX").html("");
        }
    }
 
    selectedPrioridad = function(){
        $("input[name=codprioridadpex]").val($("input[name=opcionprioridadpex]:checked").val());
    }   
    
    changeEntrenador = function(){
        var is_entrenador = $("select[name=is_entrenador]").val();
        
        $("input[name=no_cipinformante], input[name=no_nombreinformante]").val('');
//        $("span[name=noneEntrenador]").attr("class","hide");
        $("tr[name=noneEntrenador]").attr("class","hide");
        $("input[name=no_cipinformante]").focus();
        
        if(is_entrenador=='' || is_entrenador=='NO'){ 
//            $("span[title=optionBusqueda], span[name=type_search]").attr("class","hide"); 
            $("span[title=optionBusqueda], tr[name=type_search]").attr("class","hide"); 
            $("select[name=cmb_busqueda] option:eq(0)").attr("selected","selected"); 
            
            $(".tecnico_seleccionado").html('');
            
            if(is_entrenador=='NO') { 
//                $("span[name=noneEntrenador]").attr("class",""); 
                $("tr[name=noneEntrenador]").attr("class",""); 
                $("input[name=no_cipinformante]").focus();
            }
            return false; 
        }        
        $("span[title=optionBusqueda]").attr("class","");
//        $("span[name=noneEntrenador]").attr("class","hide");
        $("tr[name=noneEntrenador]").attr("class","hide");
    }
    
    setTypeSearch = function(){
        var type_search = $("select[name=cmb_busqueda]").val();
        
//        $("span[name=type_search]").attr("class","hide"); 
        $("tr[name=type_search]").attr("class","hide");
        
        if(type_search=='') {$(".tecnico_seleccionado").html('');return false;}
//        $("span[name=type_search]").attr("class","");
        $("tr[name=type_search]").attr("class","");
        $("input[name=txt_search]").val('').focus();
        
           
        $("input[name=txt_search]").autocomplete("modulos/innovaPTR/registro.pexBAK.php?cmd=get_Entrenador",{
            extraParams :{tipo_busqueda : function() {return $('select[name=cmb_busqueda]').val();}},
            minChars    : 5,
            cacheLength : 0,
            dataType    : "json",
            parse: function(data) {
                return $.map(data, function(row) {
                    return {
                        data: row,
                        value: row.cip,
                        result: function(){$('#observaciones').focus();return '';}
                    }
                });
            },
            formatItem: function(row) {
                return "CIP: "+row.cip+" - ["+row.nombres+"]"
            }
        }).result(function(e, item) {
            $(".tecnico_seleccionado").html('').append('<div id="'+item.cip+'" class="widget" style="width: 99%; "><input type="hidden" name="si_cipinformante" value="'+item.cip+'"/><input type="hidden" name="si_nombreinformante" value="'+item.nombres+'"/><div>CIP: '+item.cip+' - ['+item.nombres+']</div></div>');
	});
    }  
    
    agregarFoto = function(){
        var cantidad = $("#galeriaImagen tr").length+1;
        if(cantidad>3){return false;}
        var myRand = parseInt(Math.random()*99999);
        $('<tr id="foto'+myRand+'"><td></td><td><input name="imagen_pex[]" type="file" size="25" /> <img src="img/no_accept.png"><a onclick="quitarFoto(\'foto'+myRand+'\')" href="javascript:void(0)"> Quitar Foto</a></td></tr>').appendTo('#galeriaImagen');
    }
    
    quitarFoto = function(foto){
        $("#"+foto+"").remove();
    }
    
    limpiarFoto = function(){
        $("input[name=imagen_pex[]]").val('');
    }
 
    registrarTrabajo = function(){
        //var validacion = validateDepProDis();
        //if(validacion==false){return false;}
        
        validacion = validateFFTT();
        if(validacion==false){return false;}

        var codelemento     = $("select[name=codelementoptr], input[name=codelementoptr]").val();
        var codjefatura     = $("select[name=codjefatura], input[name=codjefatura]").val();
        var codzonal        = $("input[name=codzonal]").val();

        var codcasuistica   = $("select[name=codcasuisticapex]").val();
        var fecha_inicio_trabajo = $("#fecha_inicio_trabajo").val();
        var fecha_fin_trabajo = $("#fecha_fin_trabajo").val();
        
        //var prioridadpex    = $("input[name=codprioridadpex]").val();
        
        /*
        var cipinformante   = '';
        var nombreinformante= '';
        
        var is_entrenador   = $("select[name=is_entrenador], input[name=is_entrenador]").val();
        if(is_entrenador=='SI'){
            cipinformante   = $.trim($("input[name=si_cipinformante]").val());
            nombreinformante= $.trim($("input[name=si_nombreinformante]").val());
        }else if(is_entrenador=='NO'){
            cipinformante   = $.trim($("input[name=no_cipinformante]").val());
            nombreinformante= $.trim($("input[name=no_nombreinformante]").val());
        }
        */
      
        if(codjefatura == ""){alert('Seleccione correctamente la Jefatura.');return false;}
        if(codzonal == ""){alert('Seleccione correctamente la Zonal.');return false;}
       
        if(codelemento == ""){alert('Seleccione correctamente un Elemento de Red.');return false;}
        if(codcasuistica == ""){alert('Seleccione correctamente un Casuistica.');return false;}
        
        if(fecha_inicio_trabajo == ""){alert('Ingrese correctamente la fecha de inicio del trabajo.');return false;}
        if(fecha_fin_trabajo == ""){alert('Ingrese correctamente la fecha de fin del trabajo');return false;}
        //if(prioridadpex==""){alert('Seleccione correctamente un Prioridad.');return false;}
        //if(cipinformante==""){alert('Ingrese correctamente un CIP del Informante.');return false;}
        //if(nombreinformante==""){alert('Ingrese correctamente el nombre del Informante.');return false;}
        
        /*
         * Validacion Opcional para el Terminal y Armario 
         * Validacion de la direccion para un tipo de registro Por Elemento
         */
        var tipo_registro = $("input[name=codtiporegistropex]").val();
        if((codelemento =='5' || codelemento == '3' || codelemento =='8') && tipo_registro == 'P'){
            if($.trim($("input[name=direccioncliente]").val())==''){ alert('Ingrese la direccion correctamente!!'); return false; }
        }
        /*
         * Fin de la Validacion Opcional para el Terminal y Armario
         */
        
        
        if(confirm("Seguro de registrar el trabajo en planta y averias masivas?")) {
            $("input[name=btnGuardar]").attr('disabled','disabled');
            $("#box_loading").show();
            $('#frmRegistroPEX').submit();
        }
        
        
    }
  
    $('#frmRegistroPEX').ajaxForm({ 
        dataType    : 'json', 
        success     : processJson     
    });
    
    
    function processJson(data) { 
        $("input[name=btnGuardar]").attr('disabled','');
        alert(data.mensaje); 
        if(data.flag=='1'){
            reset_clear_frm();
        }else {
            $("#box_loading").hide();
        }

    }
});

function resetForm(){
    if(confirm("\u00BFEst\u00E1 seguro de limpiar lo llenado del formulario\u003F")){
        reset_clear_frm();
    }
}

function clearForm(){
    $("#frmRegistroPEX").html(' ');
}

function reset_clear_frm(){
    $("#cmb_tipoRegistro option:eq(0)").attr("selected", "selected");
    $("#cmb_tipoRegistro option:eq(0)").attr("selected", "selected");
    $("#cmb_tipoNegocio option:eq(0)").attr("selected", "selected");
    $("#cmb_tipoNegocio").attr("disabled","disabled");
    $(".filtroIndividual").addClass("hide");
    $(".filtroElemento").addClass("hide");
    clearForm();
}