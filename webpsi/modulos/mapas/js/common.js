function getCheckedBoxFrom(element, id, logic) {
	var strChk = ""
	var strChkInv = "";
	var allCheck = $(element + "#" + id + " INPUT[type='checkbox']")
	allCheck.each(function() {

		if (($(this).is(':checked')) && ($(this).attr('id')!='chk_all')) {
			strChk += ',' + $(this).attr('id')
		}else{ 
			strChkInv += ',' + $(this).attr('id')
		}

	});
	if(logic=="1"){
		return strChk
	}else{
		return strChkInv
	}
}

function getChkBlock(element, id) {
	var isChk = 0
	var allCheck = $(element + "#" + id + " INPUT[type='checkbox']")
	allCheck.each(function() {

		if ($(this).is(':checked')) {
			isChk++
		}

	});
	return isChk
}

function getKeyPress() {
    
    $(document).ready(function(){
            $(document).keypress(function(e){
             //if(e.keyCode==27){
                // alert("No preciones la tecla scape!!!");
             //}
             return e.keyCode;
           });
        });
    
}

function viewMiCuenta(page){
	$("#childModal").html("");
	$("#childModal").load(page + ".php");
	$("#childModal").dialog({
		modal : true,
		width : '750px',
		hide : 'slide',
                draggable: false,
		position : 'top',
		title : "Mi Cuenta",
                closeOnEscape: false,
                resizable: false
	});
}