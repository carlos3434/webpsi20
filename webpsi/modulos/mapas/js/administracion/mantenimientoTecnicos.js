/* 
 * Proyecto Web Unificada TDP
 * A.P. Omar Calderon Evangelista ocalderon@gmd.com.pe
 */
$(document).ready(function(){
    infoTecnicos=function(idtecnico){
        loader('start');
        $("#childModal").html('');
        $("#childModal").css("background-color","#FFFFFF");
        $.post("administracion_eecc.php?cmd=viewInfoTecnico", {
            idtecnico : idtecnico
        },
        function(data){
            loader('end');
            $("#childModal").html(data);
        });
        $("#childModal").dialog({
            modal:true, 
            width:'720px', 
            hide: 'slide', 
            title: 'Informaci&oacute;n del Tecnico', 
            position:'top'
        });    
    }
   
    change_state=function(idtecnico, estado, nom_tecnico){
        if(confirm("Esta seguro de cambiar el estado del Tecnico: "+nom_tecnico)){
            loader('start');
            var estado_inicial = estado;
            var nvo_estado = 0;
            $.post("administracion_eecc.php?cmd=UpdateStateTecnico",{
                idtecnico: idtecnico,
                estado  : estado_inicial
            }, function(data){
                loader('end');
                if(data==1){
                    if(estado_inicial==0){
                        nvo_estado = 1;
                        $('#alink_state_'+idtecnico).removeAttr('href');
                        $('#alink_state_'+idtecnico).attr('href','javascript:change_state(\''+idtecnico+'\',\''+nvo_estado+'\',\''+nom_tecnico+'\')');
                        $('#img_state_'+idtecnico).attr('src','img/estado_deshabilitado.png');
                        $('#img_state_'+idtecnico).attr('alt','Cambiar estado a Habilitado');
                        $('#img_state_'+idtecnico).attr('title','Cambiar estado a Habilitado');
                    }else if(estado_inicial==1){
                        nvo_estado = 0;
                        $('#alink_state_'+idtecnico).removeAttr('href');
                        $('#alink_state_'+idtecnico).attr('href','javascript:change_state(\''+idtecnico+'\',\''+nvo_estado+'\',\''+nom_tecnico+'\')');
                        $('#img_state_'+idtecnico).attr('src','img/estado_habilitado.png');
                        $('#img_state_'+idtecnico).attr('alt','Cambiar estado a Deshabilitado');
                        $('#img_state_'+idtecnico).attr('title','Cambiar estado a Deshabilitado');
                    }
                }
            });
        }
    }
    
    addTecnicosACelulas=function(idcelula,nomcelula,idempresa){
        loader('start');
        $("#childModal").html('');
        $("#childModal").css("background-color","#FFFFFF");
        
        
        $.post("administracion_eecc.php?cmd=AddTecnicosACelulas",{
            idcelula    : idcelula,
            nomcelula   : nomcelula,
            idempresa   : idempresa
        },function(data){
            loader('end');
            $("#childModal").html(data);
        });
        $("#childModal").dialog({
            modal: true, 
            width:'880px', 
            hide: 'slide', 
            title: 'Asignacion de Tecnicos a Celulas', 
            position:'top'
        });    
    }
    
    formAddCelulasASupervisores=function(idcelula,nomcelula,idempresa){
        loader('start');
        $("#childModal").html('');
        $("#childModal").css("background-color","#FFFFFF");
        $.post("administracion_eecc.php?cmd=formAddCelulasASupervisores",{
            idcelula    : idcelula,
            nomcelula   : nomcelula,
            idempresa   : idempresa
        },function(data){
            loader('end');
            $("#childModal").html(data);
        });
        $("#childModal").dialog({
            modal: true, 
            width:'880px', 
            hide: 'slide', 
            title: 'Asignacion de Supervisores a Celulas', 
            position:'top'
        });
    }
    formAddCelulasACoordinadores=function(idcelula,nomcelula,idempresa){
        loader('start');
        $("#childModal").html('');
        $("#childModal").css("background-color","#FFFFFF");
        $.post("administracion_eecc.php?cmd=formAddCelulasACoordinadores",{
            idcelula    : idcelula,
            nomcelula   : nomcelula,
            idempresa   : idempresa
        },function(data){
            loader('end');
            $("#childModal").html(data);
        });
        $("#childModal").dialog({
            modal: true, 
            width:'600px', 
            hide: 'slide', 
            title: 'Asignacion de Coordinadores a Celulas', 
            position:'top'
        });    
    }

    editArea=function(id_area){    
        var emisor=$('#emisor').attr('value');
        loader('start');
        $("#childModal").html('');
        $("#childModal").css("background-color","#FFFFFF");
        $.post("administracion.php?cmd=editArea", {
            id_area: id_area,
            emisor:emisor
        },
        function(data){
            loader('end');
            $("#childModal").html(data);
        });
        $("#childModal").dialog({
            modal:true, 
            width:'500px', 
            hide: 'slide', 
            title: 'Editar Area', 
            position:'top'
        });
    }   
});


function valida_letras(e){
    //document.oncontextmenu = function(){return false} // anula el boton derecho del mouse
    tecla = (document.all) ? e.keyCode : e.which;
    if (tecla==17 || e.ctrlKey){
        return false;
    }

    if ((tecla==13)  || (tecla==8) || (tecla==0) || (tecla==32)) return true;
    //patron =/[^A-Za-z\sA-Za-z]/;
    patron =/^[a-zA-Z ������������]+$/;
    te = String.fromCharCode(tecla);
    if ( patron.test(te) == true){
        return true;
    }else{
        return false; //solo letras
    }
} 
function valida_numeros(e){
    //document.oncontextmenu = function(){return false}
    tecla = (document.all) ? e.keyCode : e.which;
    if (tecla==17){
        return false;
    }
    if ((tecla==13)  || (tecla==8) || (tecla==0) || (tecla==32) || (tecla==67) || (tecla==99) || (tecla==86) || (tecla==118)) {
        return true;
    }
    patron =/[0-9*#\s]/;
    te = String.fromCharCode(tecla); // 5
    if ( patron.test(te) == true){
        return true;
    }else{
        return false; //solo letras
    }
} 