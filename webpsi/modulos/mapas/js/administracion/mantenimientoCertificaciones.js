var Certificaciones = function(){};
Certificaciones.prototype.initCertificaciones = function(){
	//controlesOff();  funcion  general que desactiva todos los campos de un formulario momentaneamente.
	//controlesOn();   funcion  general que activa todos los campos de un formulario.
	this.escondeElementos();
	var _self = this;
	//vinculos a los modulos
	$.each($('a.options'),function(){ // para los enlaces Speedy , Básica ,Cable , Trio
		$(this).click(function(){
			$('a.options').css({
				"color":"#0086C3",
				"font-size" : 14
			});
			$(this).css({
				"color":"red",
				"font-size" : 15
			});
			_selfLink = $(this);
			_self.escondeElementos();
            $("#msg_producto").html('<img src="img/loading.gif" alt="" border="0" />').show();
			controlesOff(); 
			var primer_valor = $("#cmb_tipo option:first");
			$('#activateAveria').val('').hide();
			$('#cmb_reporte').html("<option value=''>" + primer_valor.html() + "</option>");
			$.ajax({
				type: "POST",
				dataType : "json",
				cache:false,
				url: "modulos/certifica/bandeja.certifica.php?cmd=mostrarTipo",
				data: {
					producto:$(this).attr("producto")
				},
				success: function(data) {
					if(typeof data =="null"){
						alert("No hay datos disponibles para esta consulta");
						return false;
					};
					var cad = "<option value=''>" + primer_valor.html() + "</option>";
					$("#msg_producto").hide();
					controlesOn();
					$("#filtro_reporte").show();
                    $("#producto").val(_selfLink.attr("producto"));
					var totalArray = data.length - 1;
					for(i = 0; i<=totalArray; i++){
						cad +="<option value='" + data[i].id + "'>" + data[i].nombre + "</option>";
					};
					$("#cmb_tipo").html(cad);
					var url_load = "";
					var base_path = "modulos/certifica/vistas/"
					switch(_selfLink.attr("producto")){
						case "speedy":
							url_load = "speedy/reportes/speedy.filtros.php";
						break;
						case "basica":
							url_load = "basica/reportes/basica.filtros.php";
						break;
						case "cable":
							url_load = "catv/reportes/catv.filtros.php";
						break;
						case "trio":
							url_load = "trio/reportes/trio.filtros.php";
						break;
						case "documentos":
							url_load = "documentos/reportes/documentos.filtros.php";
						break;
						
					};
					$('#cont_filtros_texto').load(base_path + url_load,function(){
						//$('select').css("cursor","pointer");
					});
				},
				error : function(){
					alert("<p style='color:red'>Parece que el servidor esta tardando en responder, por favor int&eacute;ntelo en unos minutos</p>");
					$("#msg_producto").hide();
					$("#filtro_reporte").hide();
					controlesOn();
				}
			});
		});
	});
	
	$("#cmb_tipo").change(function() {
		var tipo = $(this).val();
		var producto = $("#producto").val();
		if(tipo == ''){
			alert("<span style='color:red'>Por favor seleccione un tipo</span>");
			return false;
		};
		if(tipo == 2){
			$('#activateAveria').val('').show();
		}else{
			$('#activateAveria').val('').hide();
		};
		
		$("#filtro_busqueda").hide();
		$("#cont_resultado_main").hide();
		$("#msg_accion").show();
		$('#msg_generar').hide();
		 document.getElementById("formReporte").reset();
		controlesOff(); 
		var primer_valor = $("#cmb_tipo option:first");
		$.ajax({
				type: "POST",
				dataType : "json",
				url: "modulos/certifica/bandeja.certifica.php?cmd=mostrarTipoReporte",
				data: {
					producto : producto,
					tipo : tipo
				},
				success: function(data) {					
					var cad = "<option value=''>" + primer_valor.html() + "</option>";				
					$("#msg_producto").hide();
					controlesOn();
					$("#tipo").val(tipo);				
                    $("#msg_accion").hide();
					var totalArray = data.length - 1;
						for(i = 0; i<=totalArray; i++){
							cad +="<option value='" + data[i].id + "'>" + data[i].nombre + "</option>";
						};
					$("#cmb_reporte").html(cad);
				},
				error : function(){
					alert("<p style='color:red'>Parece que el servidor esta tardando en responder, por favor int&eacute;ntelo en unos minutos</p>");
					$("#msg_producto").hide();
					$("#filtro_reporte").hide();
					controlesOn();
				}
		});
	});
	$("#cmb_reporte").change(function() {
		 var producto = $("#producto").val();
         var tipo = $("#tipo").val();
         var reporte = $(this).val();
		 if(reporte == ''){
			alert("<span style='color:red'>Por favor seleccione un tipo de reporte</span>");
			return false;
		 };
         $("#filtro_busqueda").hide();
         $("#cont_resultado_main").hide();
		 $("#msg_accion").show();
		 $('#msg_generar').hide();
		 document.getElementById("formReporte").reset();
         controlesOff(); 
		 $.ajax({
			type: "POST",
			dataType : "html",
			cache:false,
			url: "modulos/certifica/bandeja.certifica.php?cmd=mostrarFiltro",
			data: {
				producto : producto,
				tipo : tipo,
				reporte: reporte
			},
			success: function(data) {	
				$("#reporte").val(reporte);
				controlesOn();
				$("#msg_accion").hide();
				
				var base_path = "modulos/certifica/vistas/"
				
				if(producto == "speedy"){
					if(tipo ==1 && reporte == 2){
						var url_load = base_path+"speedy/reportes/cd.filtros.php";
						$("#cont_filtros_texto").load(url_load,function(){
							$("#filtro_busqueda").show();
							//$('select').css("cursor","pointer");
						});
					}else if(tipo ==2 && reporte == 1){
						var url_load = base_path+"speedy/reportes/averias.filtros.php";
						$("#cont_filtros_texto").load(url_load,function(){
							$("#filtro_busqueda").show();
							//$('select').css("cursor","pointer");
						});
					}else{
						$("#filtro_busqueda").show();   
					}
				};
				if(producto =="basica"){
					if(tipo ==2 && reporte == 1){
						var url_load = base_path+"basica/reportes/troncales.filtros.php";
						$("#cont_filtros_texto").load(url_load,function(){
							$("#filtro_busqueda").show();
							//$('select').css("cursor","pointer");
						});
					}else{
						$("#filtro_busqueda").show();   
					}
				}else if(producto =="cable"){
					if(tipo ==2 && reporte == 1){
						var url_load = base_path+"catv/reportes/dth.filtros.php";
						$("#cont_filtros_texto").load(url_load,function(){
							$("#filtro_busqueda").show();
							//$('select').css("cursor","pointer");
						});
					}else if(tipo ==1 && reporte == 1){
						var url_load = base_path+"catv/reportes/catv.filtros.php";
						$("#cont_filtros_texto").load(url_load,function(){
							$("#filtro_busqueda").show();
							//$('select').css("cursor","pointer");
						});
						
					}else{
						$("#filtro_busqueda").show();   
					}
				}else if(producto =="documentos"){
					$("#filtro_busqueda").show();
					$('#btn_generar').trigger("click") //para que muestre todos;
				}else{
					$("#filtro_busqueda").show();   
				}
                
			},
			error : function(){
				alert("<p style='color:red'>Parece que el servidor esta tardando en responder, por favor int&eacute;ntelo en unos minutos</p>");
				$("#msg_producto").hide();
				$("#filtro_reporte").show();
				controlesOn();
			}
		 });
    });

};
Certificaciones.prototype.preparaSelects = function(){
	var _object = this;
	//Acciones de selects :
	//accion en select para acciones en seleccionados:
	var selectsAcciones = $('select.accionesSelect');
	$.each(selectsAcciones,function(){
		$(this).change(function(){
			var valor = $(this).val();
			if(valor==''){
				alert("<p style='color:red'>Por favor seleccione una acci&oacute;n v&aacute;lida</p>");
			}else{
				if($(this).attr("accion")=="seleccionados"){
					_object.actualizaGrupal($(this),"seleccionados",valor);
				}else if ($(this).attr("accion")=="todos"){
					_object.actualizaGrupal($(this),"todos",valor);
				};
			};
		});
	});
	
	
	$('#cont_resultado_main a.doExcel').click(function(e){
        e.stopPropagation();
		$('#modalJqueryTDPTip').html("Generando archivo por favor espere...");
		$('body').append("<iframe name='iframeExcel' id='iframeExcel' style='display:none'  / >");
		$('#formReporte').submit();
		$('#iframeExcel').load(function(){
			 setTimeout(function(){
				//$('#iframeExcel').remove();
			 },800);
		});
		
    });
	$('#cont_resultado_main').tips({
		selector : 'a.doExcel',
		tipClass : 'classCoord'
	});

};

Certificaciones.prototype.activaGrid = function(){
	var _object = this;
	//$('input[type="checkbox"] ,select').css("cursor","pointer");
	var elements = $('#tb_resultado tbody tr.menuDerecho td');
    $.each(elements,function(){
		var itemActual = $(this);
		var fila = itemActual.parent();
		var select = fila.find("select.selectFormCambiaEstado");
		var valorAnterior = select.val();
        $(this).menuDerecho({
				items : [	
							{
								text : 'Seleccionar / No seleccionar',
								accion : function(){
									fila.find("td").toggleClass("seleccionado");
									var check = fila.find("input[type='checkbox']");
									(check.is(":checked"))?check.attr("checked",""):check.attr("checked","checked");
								},
								icono :'img/accept.png'
							},
							{
								text : 'Ver / Ocultar Baremos',
								accion : function(){
									var fila = itemActual.parent();
									var baremo = $('#baremo_' + fila.attr("id_fila"));
									if(baremo.is(":visible")){
										baremo.hide(); 
									}else{
										var contenidoBaremo = $.trim($("#dt"+fila.attr("id_fila")).html());
										if(contenidoBaremo==''){
											$("#dt"+fila.attr("id_fila")).html("<p style='text-align=center'><h3>Cargando baremos...</h3></p>");
											var tipo = $("#tipo").val();
											var reporte = $("#reporte").val();
											$.post("modulos/certifica/bandeja.certifica.php?cmd=mostrarDetBaremos",
												{ 
													id: fila.attr("id_fila"),
													date : fila.attr("date_in_system"),
													tipo:tipo,
													reporte:reporte,
													producto : fila.attr("tipo")
												},
												function(data){
													$("#dt"+fila.attr("id_fila")).html(data).show();
												}
											); 
										};
										baremo.show(); 
									};
										 
								},
								icono : "img/edit_agenda.png"
							},
							{
								text : 'Marcar como <span>Pendiente</span>',
								accion : function(){
									var valor = "0";
									_object.actualizaEstado(fila,valor,select,valorAnterior);
								},
								icono :'img/btn_estado_gestion_24.png'
							},
							{
								text : 'Marcar como <span>Aprobada</span>',
								accion : function(){
									var valor = "1";
									_object.actualizaEstado(fila,valor,select,valorAnterior);
								},
								icono :'img/btn_estado_gestion_24.png'
							},
							{
								text : 'Marcar como <span>Observada</span>',
								accion : function(){
									var valor = "2";
									_object.actualizaEstado(fila,valor,select,valorAnterior);
								},
								icono :'img/btn_estado_gestion_24.png'
							}
                        ],
                        highlightClass:'selected'
        });
    });
			
	var inputs = $('#tb_resultado tbody input[type="checkbox"]');
	
	inputs.click(function(){
		$('#tb_resultado input.all').attr("checked",'');
        if($(this).is(":checked")){
			$(this).parent().parent().find("td").addClass("seleccionado").end().end().end().attr("checked","checked");
        }else{
			$(this).parent().parent().find("td").removeClass("seleccionado").end().end().end().attr("checked","");
        };
    });		
	
	//opcion para seleccionar o no selecionar todos los checks
	$('#tb_resultado input.all').click(function(){
        if($(this).is(":checked")){
			$.each(inputs,function(){
               $(this).parent().parent().find("td").addClass("seleccionado").end().end().end().attr("checked","checked");
            });
        }else{
            $.each(inputs,function(){
                $(this).parent().parent().find("td").removeClass("seleccionado").end().end().end().attr("checked","");;
            });
        };
    });
	
	
	//funciones para los links a Baremos cuando ya se carga la paginacion 
	var linksBaremos  = $('a.verBaremo');
	$.each(linksBaremos,function(){
		var fila = $(this);
		fila.click(function(){
			var baremo = $("#baremo_"+$(this).attr("id_fila"));
			if(baremo.is(":visible")){
				 baremo.hide(); 
			}else{
				var contenidoBaremo = $.trim($("#dt"+fila.attr("id_fila")).html());
				if(contenidoBaremo==''){
					$("#dt"+fila.attr("id_fila")).html("<p style='text-align=center'><h3>Cargando baremos...</h3></p>");
					var tipo = $("#tipo").val();
					var reporte = $("#reporte").val();
					$.post("modulos/certifica/bandeja.certifica.php?cmd=mostrarDetBaremos",
						{ 
						  id: fila.parent().parent().attr("id_fila"), //obtenemos los datos de la fila TR
						  date : fila.parent().parent().attr("date_in_system"), //obtenemos los datos de la fila TR
						  tipo : tipo,
						  reporte :  reporte,
						  producto : fila.parent().parent().attr("tipo")
						}, 
						function(data){
							$("#dt"+fila.attr("id_fila")).html(data).show();
						}
					); 
				};
				baremo.show(); 
			};
		});
	});
	
	//Acciones
	//Cambiar cada registro de estado : Pendiente / Aprobado / Observado  esto es por cada select al final de la fila
	var selectsEstado = $('table#tb_resultado select.selectFormCambiaEstado');
	$.each(selectsEstado,function(){
		var select = $(this);
		select.change(function(){
			var fila =  $(this).parent().parent(); //vamos a la fila TR para obtener los datos necesarios para el cambio de estado;
			var valor = $(this).val();
			var msg = "";
			_object.actualizaEstado(fila,valor,select);
		});
	});
	
	//Cambiar la contrata de los diferentes  registros listados
	var selectsContrata = $('table#tb_resultado select.selectContratas');
	$.each(selectsContrata,function(){
		var select = $(this);
		select.change(function(){
			var fila =  $(this).parent().parent(); //vamos a la fila TR para obtener los datos necesarios para el cambio de estado;
			var contrata = $(this).find("option:selected").html();
			var codigo_contrata = $(this).val();
			_object.actualizaContrata(fila,contrata,codigo_contrata,select);
		});
	});
	
	//cambiar las zonales de cada registro listado
	var selectsZonal = $('table#tb_resultado select.selectZonales');
	$.each(selectsZonal,function(){
		var select = $(this);
		select.change(function(){
			var fila =  $(this).parent().parent(); //vamos a la fila TR para obtener los datos necesarios para el cambio de estado;
			var codigo_zonal = $(this).val();
			_object.actualizaZonal(fila,codigo_zonal,select);
		});
	});
	
	
	/*
	
	var indextheadActivo = '';
	
	var theads = $('#tb_resultado thead th a');
	var findThead = false;
	$.each(theads,function(index){
		if($(this).hasClass("columnAsc") || $(this).hasClass("columnDesc")){
			indextheadActivo = index
			findThead = true;
		};
	});
	
	if(findThead){
	  $("#tb_resultado td:nth-child("+((indextheadActivo) + 3)+")").addClass("seleccionado");
	}
	*/
};

Certificaciones.prototype.prepareColumnsOrder = function (){
	var _object = this;
	//para los ordenamientos
	var linksOrder = $('#tb_resultado thead th a.orderBy');
	
	$.each(linksOrder,function(index){
		var _link = $(this);
		
		var _column = _link.attr("orderBy");
		var _direccion ='';
		var _producto = $('#tb_resultado tbody tr:first').attr("tipo");
		var _tipo = $('#tipo').val();
		var _reporte = $('#reporte').val();
		
		
		_link.toggle(function(){
			$('a.orderBy').removeClass("columnDesc columnAsc");
			_link.attr("orderType","desc").removeClass("columnDesc").addClass("columnAsc");
			_pagina = $('.jPag-current').html();
			_direccion = "desc";
			loader("start");
			$.ajax({
				type: "POST",
				cache:false,
				dataType: "html",
				url: "modulos/certifica/bandeja.certifica.php?cmd=orderPor&producto="+_producto+"&direccion="+_direccion+"&columna="+_column+"&tipo="+_tipo+"&reporte="+_reporte+"&pagina="+_pagina,
				data: $('#formReporte').serialize(),
				success: function(dataAjax) {	
					$("#resultadosPaginados").html(dataAjax);
					_object.activaGrid();  
					_object.preparaSelects();  
					$("#orderby").val(_column);
                    $("#direction").val(_direccion);
					//$("#tb_resultado td:nth-child("+((index) + 2)+")").addClass("seleccionado");
					loader("end");
				},
				error : function(){
					alert("<p style='color:red'>Parece que el servidor esta tardando en responder, por favor int&eacute;ntelo en unos minutos</p>");					
					select.val(valorAnterior);
				}
			});
			
		},function(){
			$('a.orderBy').removeClass("columnDesc columnAsc");
			_link.attr("orderType","asc").removeClass("columnAsc").addClass("columnDesc");			
			_direccion = "asc";
			_pagina = $('.jPag-current').html();
			loader("start");
			$.ajax({
				type: "POST",
				cache:false,
				dataType: "html",
				url: "modulos/certifica/bandeja.certifica.php?cmd=orderPor&producto="+_producto+"&direccion="+_direccion+"&columna="+_column+"&tipo="+_tipo+"&reporte="+_reporte+"&pagina="+_pagina,
				data: $('#formReporte').serialize(),
				success: function(data) {	
					$("#resultadosPaginados").html(data);
					_object.activaGrid();  
					_object.preparaSelects();   
					loader("end");
					$("#orderby").val(_column);
                    $("#direction").val(_direccion);
					//$("#tb_resultado td:nth-child("+((index) + 2)+")").addClass("seleccionado");
				},
				error : function(){
					alert("<p style='color:red'>Parece que el servidor esta tardando en responder, por favor int&eacute;ntelo en unos minutos</p>");
					loader("end");
					select.val(valorAnterior);
				}
			});
			
		});
		
			
	});
	
	
	$('#tb_resultado thead th').tips({
		selector : 'a.orderBy',
		tipClass : 'classCoord'
	});
	
};

Certificaciones.prototype.actualizaEstado = function (fila,estado,select){

	var tipo_consulta = $("#tipo").val();
    var reporte = $("#reporte").val();
	
	switch(estado){
		case '0':msg = "<br>Esta seguro que desea cambia a PENDIENTE este registro?";break;
		case '1':msg = "<br>Esta seguro que desea cambia a APROBADO este registro?";break;
		case '2':msg = "<br>Esta seguro que desea cambia a OBSERVADO este registro?";break;
	};
	var valorAnterior = select.attr("estado_actual");
	$('body').append("<div id='modalJqueryTDPPrompt' align='center'><p>"+msg+"</p></div>");
	$('#modalJqueryTDPPrompt').createPrompt({
		ok :function(){
			controlesOff();
			$.ajax({
				type: "POST",
				dataType: "html",
				cache:false,
				url: "modulos/certifica/bandeja.certifica.php?cmd=cambiaEstadoRegistro",
				data: {
					id: fila.attr("id_fila"),
					estado : estado,
					date : fila.attr("date_in_system"),
					tipo : fila.attr("tipo"),
					tipo_consulta : tipo_consulta,
					reporte : reporte
				},
				success: function(data) {	
					controlesOn();
					valorAnterior = select.attr("estado_actual",estado);
					select.val(estado);
				},
				error : function(){
					alert("<p style='color:red'>Parece que el servidor esta tardando en responder, por favor int&eacute;ntelo en unos minutos</p>");
					controlesOn();
					select.val(valorAnterior);
				}
			});
		},
		cancel : function(){
			select.val(valorAnterior);
		}
	});
};

Certificaciones.prototype.actualizaContrata = function (fila,contrata,codigo_contrata,select){	
	var tipo_consulta = $("#tipo").val();
    var reporte = $("#reporte").val();
	
	var msg = "<br>Desea cambiar de contrata al registro seleccionado?";
	var valorAnterior = select.attr("contrata_actual");
	$('body').append("<div id='modalJqueryTDPPrompt' align='center'><p>"+msg+"</p></div>");
	$('#modalJqueryTDPPrompt').createPrompt({
		ok :function(){
			controlesOff();
			$.ajax({
				type: "POST",
				dataType: "html",
				cache:false,
				url: "modulos/certifica/bandeja.certifica.php?cmd=cambiaContrata",
				data: {
					id: fila.attr("id_fila"),
					contrata : $.trim(contrata),
					date : fila.attr("date_in_system"),
					tipo : fila.attr("tipo"),
					codigo_contrata : codigo_contrata,
					tipo_consulta : tipo_consulta,
					reporte: reporte
				},
				success: function(data) {	
					controlesOn();
					valorAnterior = select.attr("contrata_actual",codigo_contrata);
					select.val(codigo_contrata);
				},
				error : function(){
					alert("<p style='color:red'>Parece que el servidor esta tardando en responder, por favor int&eacute;ntelo en unos minutos</p>");
					controlesOn();
					select.val(valorAnterior);
				}
			});
		},
		cancel : function(){
			select.val(valorAnterior);
		}
	});
};

Certificaciones.prototype.actualizaZonal = function(fila,codigo_zonal,select){
	var tipo_consulta = $("#tipo").val();
    var reporte = $("#reporte").val();
	
	var msg = "<br>Desea cambiar de zonal al registro seleccionado?";
	var valorAnterior = select.attr("zona_actual");
	$('body').append("<div id='modalJqueryTDPPrompt' align='center'><p>"+msg+"</p></div>");
	$('#modalJqueryTDPPrompt').createPrompt({
		ok :function(){
			controlesOff();
			$.ajax({
				type: "POST",
				dataType: "html",
				cache:false,
				url: "modulos/certifica/bandeja.certifica.php?cmd=cambiaZonal",
				data: {
					id: fila.attr("id_fila"),
					date : fila.attr("date_in_system"),
					tipo : fila.attr("tipo"),
					codigo_zonal : codigo_zonal,
					tipo_consulta : tipo_consulta,
					reporte : reporte
				},
				success: function(data) {	
					controlesOn();
					valorAnterior = select.attr("zona_actual",codigo_zonal);
					select.val(codigo_zonal);
				},
				error : function(){
					alert("<p style='color:red'>Parece que el servidor esta tardando en responder, por favor int&eacute;ntelo en unos minutos</p>");
					controlesOn();
					select.val(valorAnterior);
				}
			});
		},
		cancel : function(){
			select.val(valorAnterior);
		}
	});
};

Certificaciones.prototype.actualizaGrupal = function (select,cantidad,estado){
	var tipo_consulta = $("#tipo").val();
    var reporte = $("#reporte").val();
	
	var msg = "";
	var ids = '';
	var checks = '';
	switch(cantidad){
		case "seleccionados":
			checks = $("#tb_resultado tbody tr").find("input[type='checkbox']:checked");
			var data = new Array();
			$.each(checks,function(index,value){
				data.push($(this).val());
			});
			var cadena_ids = data.join(",");
			if(checks.length<=0){
				alert("<span style='color:red'>Debes de elegir al menos un registro para poder cambiar de estado</span>");
				select.val('');
				return false;
			}else{
				ids = cadena_ids;
			};
			switch(estado){
				case "0":
					msg ="Esta acci&oacute;n actualizar&aacute; todas las ordenes seleccionadas a PENDIENTES<br>";
					msg +="Desea continuar?";
				break;
				case "1":
					msg ="Esta acci&oacute;n actualizar&aacute; todas las ordenes seleccionadas a APROBADAS<br>";
					msg +="Desea continuar?";
				break;
				case "2":
					msg ="Esta acci&oacute;n actualizar&aacute; todas las ordenes seleccionadas a OBSERVADAS<br>";
					msg +="Desea continuar?";
				break;
			};
			
		break;
		case "todos":			
			switch(estado){
				case "0":
					msg ="Esta acci&oacute;n actualizar&aacute; todas las ordenes pendientes<br>";
					msg += "como aprobadas y no se podr&aacute;n realizar cambios<br>";
					msg +="Desea continuar?";
				break;
				case "2":
					msg ="Esta acci&oacute;n actualizar&aacute; todas las ordenes observadas<br>";
					msg +="como aprobadas y no se podr&aacute;n realizar cambios<br>";
					msg +="Desea continuar?";
				break;
			};
		break;
	};
	var date_in_system = $("#tb_resultado tbody tr:first").attr("date_in_system"); //obtenemos la fecha desde la primera fila de la tabla
	$('body').append("<div id='modalJqueryTDPPrompt' align='center'><p>"+msg+"</p></div>");
	$('#modalJqueryTDPPrompt').createPrompt({
		ok :function(){
			controlesOff();
			$.ajax({
				type: "POST",
				dataType: "html",
				cache:false,
				url: "modulos/certifica/bandeja.certifica.php?cmd=cambiaEstadoGrupo",
				data: {
					ids: ids,
					date : date_in_system,
					tipo : select.attr("tipo"),
					estado : estado,
					tipo_update : cantidad,
					tipo_consulta : tipo_consulta,
					reporte : reporte
				},
				success: function(data) {	
					controlesOn();
					if(cantidad=="todos"){
						$.each(checks,function(){
							$(this).parent().parent().find("select.selectFormCambiaEstado").val("1").attr("estado_actual","1");
						});
						$('select.selectFormCambiaEstado').val(1).attr("estado_actual",1);
					};
					if(cantidad=="seleccionados"){
						$.each(checks,function(){
							$(this).parent().parent().find("select.selectFormCambiaEstado").val(estado).attr("estado_actual",estado);
						});
					};
					select.val('');
				},
				error : function(){
					alert("<p style='color:red'>Parece que el servidor esta tardando en responder, por favor int&eacute;ntelo en unos minutos</p>");
					controlesOn();
					select.val('');
				}
			});
		},
		cancel : function(){
			select.val('');
		}
	});
};

Certificaciones.prototype.escondeElementos = function(){
	$("#filtro_reporte").hide();
    $("#filtro_busqueda").hide();
    $("#cont_resultado_main").hide();
	$('#cmb_tipo').val('');
	$('#cmb_reporte').val('');
};

Certificaciones.prototype.deleteDocument = function(anchor){

	$('body').append("<div id='modalJqueryTDPPrompt' align='center'><p><br>Realmente desea eliminar el documento seleccionado?</p></div>");
	
	$('#modalJqueryTDPPrompt').createPrompt({
		ok :function(){
			controlesOff();
			$.ajax({
				type: "POST",
				dataType: "html",
				cache:false,
				url: "modulos/certifica/bandeja.certifica.php?cmd=deleteDocument",
				data: {
					id: anchor.attr("id_fila"),
					file :anchor.attr("file")
				},
				success: function(data) {	
					controlesOn();
					$('#btn_generar').trigger("click");
				},
				error : function(){
					alert("<p style='color:red'>Parece que el servidor esta tardando en responder, por favor int&eacute;ntelo en unos minutos</p>");
					controlesOn();
				}
			});
		},cancel : function(){}
	});
};


/**
* Eliminacion de documentos masiva	
*/
Certificaciones.prototype.deleteDocumentMassive = function(){

	var checks = $('#tb_resultado tbody').getChecks({type : "string"});
    var files = $('#tb_resultado tbody').getChecks({type : "string",extract:'file'});
	
	if(checks == ''){
		alert("<span style='color:red'>Debes de elegir al menos un documento para poder eliminarlo</span>");
		return false;
	};
	
	$('body').append("<div id='modalJqueryTDPPrompt' align='center'><p><br>Realmente desea eliminar los documentos seleccionados?</p></div>");
		
	$('#modalJqueryTDPPrompt').createPrompt({
		ok :function(){
			controlesOff();
			$.ajax({
				type: 'POST',
				cache:false,
				url: "modulos/certifica/bandeja.certifica.php?cmd=deleteDocumentMassive",
				data: {
					ids :checks,
					files : files
				},
				success: function() {
					$('#btn_generar').trigger("click");
					controlesOn();
				},
				error : function(){	
				  alert("<br><span style='color:red'>Ha ocurrido un error en la eliminacion de archivos por favor int&eacute;ntelo en unos minutos</span>");
				}
			});
		},cancel : function(){}
	});
};

var certificaciones = new Certificaciones();

$('#cont_resultado_main').load("modulos/certifica/vistas/main/bienvenida.reportes.php",function(){
	$('#cont_resultado_main').show();
});

var subidaOk = function (){
	$('#msgCargaDocumento').html("Archivo subido correctamente.<a href='javascript:;' class='addNewDocument' onclick='resetUploadDocument();'>A&ntilde;adir otro documento</a>");
	$('#btn_generar').trigger("click");
	$('#cmb_mes').focus();
};
var resetUploadDocument = function (){
	document.getElementById("formAddDocumento").reset();
	$('#msgCargaDocumento').html("");
};
var errorSubida = function (){
	$('#msgCargaDocumento').html("Hubo un error al subir el archivo. por favor intentelo nuevamente.");
};

