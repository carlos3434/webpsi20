var _self = '';
Conciliacion = function (){
	_self = this;
};


Conciliacion.prototype.preparaFormulario = function (){
    
    var theForm = $('#form_documentos_conciliacion');
	
    theForm.submit(function(e){
		e.preventDefault(e);	
		
		Cargando("inicio");
		$('#pagina').val('');
        $.post(theForm.attr("action"),theForm.serialize(),
			function(data){
		
				Cargando("fin");
				$('#resultados_conciliacion').html(data);
    
				_self.preparaConciliaciones();
        });   
                    
    });
		
	var radioTipo = $('#form_documentos_conciliacion input[name="tipo"]');
	
	radioTipo.click(function(){
		if($(this).val() == 0){
			$('#selectContrata').hide().find("select").val("");
			$('#selectZonal').show().find("select").val("");
		}else{
			$('#selectContrata').show().find("select").val("");
			$('#selectZonal').hide().find("select").val("");
		};
	});
	
	
};

Conciliacion.prototype.preparaConciliaciones = function(){

	_self.activaGrid();  
	$('a.eliminar').click(function(){
		_self.deleteDocument($(this));
    });
	
	$('#tb_resultado').tips({
		selector : 'a.descargar,a.eliminar,a.editar'
    });
   
	$('a.editar').click(function(){
		tdp.cargaModal("Edicion de Documentos","modulos/conci/bandeja.conciliacion.php?cmd=mostrarFormularioEditar&id="+$(this).attr("id_fila"));
    });
	
};

/**
* Eliminacion de documentos masiva	
*/
Conciliacion.prototype.deleteDocumentMassive = function(){

	var checks = $('#tb_resultado tbody').getChecks({type : "string"});
    var files = $('#tb_resultado tbody').getChecks({type : "string",extract:'file'});
	
	if(checks == ''){
		alert("<span style='color:red'>Debes de elegir al menos un documento para poder eliminarlo</span>");
		return false;
	};
	
	$('body').append("<div id='modalJqueryTDPPrompt' align='center'><p><br>Realmente desea eliminar los documentos seleccionados?</p></div>");
		
	$('#modalJqueryTDPPrompt').createPrompt({
		ok :function(){
			controlesOff();
			$.ajax({
				type: 'POST',
				cache:false,
				url: "modulos/conci/bandeja.conciliacion.php?cmd=deleteDocumentMassive",
				data: {
					ids :checks,
					files : files
				},
				success: function() {
					$('#form_documentos_conciliacion').trigger("submit");
					controlesOn();
				},
				error : function(){	
				  alert("<br><span style='color:red'>Ha ocurrido un error en la eliminacion de archivos por favor int&eacute;ntelo en unos minutos</span>");
				}
			});
		},cancel : function(){}
	});
};

Conciliacion.prototype.deleteDocument = function(anchor){

	$('body').append("<div id='modalJqueryTDPPrompt' align='center'><p><br>Realmente desea eliminar el documento seleccionado?</p></div>");
	
	$('#modalJqueryTDPPrompt').createPrompt({
		ok :function(){
			controlesOff();
			$.ajax({
				type: "POST",
				dataType: "html",
				cache:false,
				url: "modulos/conci/bandeja.conciliacion.php?cmd=deleteDocument",
				data: {
					id: anchor.attr("id_fila"),
					file :anchor.attr("file")
				},
				success: function(data) {	
					controlesOn();
					$('#form_documentos_conciliacion').trigger("submit");
				},
				error : function(){
					alert("<p style='color:red'>Parece que el servidor esta tardando en responder, por favor int&eacute;ntelo en unos minutos</p>");
					controlesOn();
				}
			});
		},cancel : function(){}
	});
};


Conciliacion.prototype.activaGrid = function(){
	var _object = this;
	//$('input[type="checkbox"] ,select').css("cursor","pointer");
	var elements = $('#tb_resultado tbody tr.menuDerecho td');
    $.each(elements,function(){
		var itemActual = $(this);
		var fila = itemActual.parent();
		var select = fila.find("select.selectFormCambiaEstado");
		var valorAnterior = select.val();
        $(this).menuDerecho({
				items : [	
							{
								text : 'Seleccionar / No seleccionar',
								accion : function(){
									fila.find("td").toggleClass("seleccionado");
									var check = fila.find("input[type='checkbox']");
									(check.is(":checked"))?check.attr("checked",""):check.attr("checked","checked");
								},
								icono :'img/accept.png'
							},
							{
								text : 'Ver / Ocultar Baremos',
								accion : function(){
									var fila = itemActual.parent();
									var baremo = $('#baremo_' + fila.attr("id_fila"));
									if(baremo.is(":visible")){
										baremo.hide(); 
									}else{
										var contenidoBaremo = $.trim($("#dt"+fila.attr("id_fila")).html());
										if(contenidoBaremo==''){
											$("#dt"+fila.attr("id_fila")).html("<p style='text-align=center'><h3>Cargando baremos...</h3></p>");
											var tipo = $("#tipo").val();
											var reporte = $("#reporte").val();
											$.post("modulos/certifica/bandeja.certifica.php?cmd=mostrarDetBaremos",
												{ 
													id: fila.attr("id_fila"),
													date : fila.attr("date_in_system"),
													tipo:tipo,
													reporte:reporte,
													producto : fila.attr("tipo")
												},
												function(data){
													$("#dt"+fila.attr("id_fila")).html(data).show();
												}
											); 
										};
										baremo.show(); 
									};
										 
								},
								icono : "img/edit_agenda.png"
							},
							{
								text : 'Marcar como <span>Pendiente</span>',
								accion : function(){
									var valor = "0";
									_object.actualizaEstado(fila,valor,select,valorAnterior);
								},
								icono :'img/btn_estado_gestion_24.png'
							},
							{
								text : 'Marcar como <span>Aprobada</span>',
								accion : function(){
									var valor = "1";
									_object.actualizaEstado(fila,valor,select,valorAnterior);
								},
								icono :'img/btn_estado_gestion_24.png'
							},
							{
								text : 'Marcar como <span>Observada</span>',
								accion : function(){
									var valor = "2";
									_object.actualizaEstado(fila,valor,select,valorAnterior);
								},
								icono :'img/btn_estado_gestion_24.png'
							}
                        ],
                        highlightClass:'selected'
        });
    });
			
	var inputs = $('#tb_resultado tbody input[type="checkbox"]');
	
	inputs.click(function(){
		$('#tb_resultado input.all').attr("checked",'');
        if($(this).is(":checked")){
			$(this).parent().parent().find("td").addClass("seleccionado").end().end().end().attr("checked","checked");
        }else{
			$(this).parent().parent().find("td").removeClass("seleccionado").end().end().end().attr("checked","");
        };
    });		
	
	//opcion para seleccionar o no selecionar todos los checks
	$('#tb_resultado input.all').click(function(){
        if($(this).is(":checked")){
			$.each(inputs,function(){
               $(this).parent().parent().find("td").addClass("seleccionado").end().end().end().attr("checked","checked");
            });
        }else{
            $.each(inputs,function(){
                $(this).parent().parent().find("td").removeClass("seleccionado").end().end().end().attr("checked","");;
            });
        };
    });
	
	
	//funciones para los links a Baremos cuando ya se carga la paginacion 
	var linksBaremos  = $('a.verBaremo');
	$.each(linksBaremos,function(){
		var fila = $(this);
		fila.click(function(){
			var baremo = $("#baremo_"+$(this).attr("id_fila"));
			if(baremo.is(":visible")){
				 baremo.hide(); 
			}else{
				var contenidoBaremo = $.trim($("#dt"+fila.attr("id_fila")).html());
				if(contenidoBaremo==''){
					$("#dt"+fila.attr("id_fila")).html("<p style='text-align=center'><h3>Cargando baremos...</h3></p>");
					var tipo = $("#tipo").val();
					var reporte = $("#reporte").val();
					$.post("modulos/certifica/bandeja.certifica.php?cmd=mostrarDetBaremos",
						{ 
						  id: fila.parent().parent().attr("id_fila"), //obtenemos los datos de la fila TR
						  date : fila.parent().parent().attr("date_in_system"), //obtenemos los datos de la fila TR
						  tipo : tipo,
						  reporte :  reporte,
						  producto : fila.parent().parent().attr("tipo")
						}, 
						function(data){
							$("#dt"+fila.attr("id_fila")).html(data).show();
						}
					); 
				};
				baremo.show(); 
			};
		});
	});
	
	//Acciones
	//Cambiar cada registro de estado : Pendiente / Aprobado / Observado  esto es por cada select al final de la fila
	var selectsEstado = $('table#tb_resultado select.selectFormCambiaEstado');
	$.each(selectsEstado,function(){
		var select = $(this);
		select.change(function(){
			var fila =  $(this).parent().parent(); //vamos a la fila TR para obtener los datos necesarios para el cambio de estado;
			var valor = $(this).val();
			var msg = "";
			_object.actualizaEstado(fila,valor,select);
		});
	});
	
	//Cambiar la contrata de los diferentes  registros listados
	var selectsContrata = $('table#tb_resultado select.selectContratas');
	$.each(selectsContrata,function(){
		var select = $(this);
		select.change(function(){
			var fila =  $(this).parent().parent(); //vamos a la fila TR para obtener los datos necesarios para el cambio de estado;
			var contrata = $(this).find("option:selected").html();
			var codigo_contrata = $(this).val();
			_object.actualizaContrata(fila,contrata,codigo_contrata,select);
		});
	});
	
	//cambiar las zonales de cada registro listado
	var selectsZonal = $('table#tb_resultado select.selectZonales');
	$.each(selectsZonal,function(){
		var select = $(this);
		select.change(function(){
			var fila =  $(this).parent().parent(); //vamos a la fila TR para obtener los datos necesarios para el cambio de estado;
			var codigo_zonal = $(this).val();
			_object.actualizaZonal(fila,codigo_zonal,select);
		});
	});
};


conciliacion = new Conciliacion();

var msgOkCierreDocumentos = function(){
	$("#msgCargaDocumento").html("<span style='color:green;font-weight:bold'>Mes cerrado correctamente</span>");
};
var msgMesCerrado = function(fecha){
	$("#msgCargaDocumento").html("<span style='color:red;font-weight:bold'>No se pueden agregar mas archivos  en la fecha : "+fecha+"</span>");
};
var subidaBaseOk = function(){
	$("#msgCargaDocumento").html("<span style='color:green;font-weight:bold'>Documento Base Grabado Correctamente</span>");
};
var subidaDocumentoOk = function(){
	$("#msgCargaDocumento").html("<span style='color:green;font-weight:bold'>Documento Grabado Correctamente</span>");
	$('#form_documentos_conciliacion').submit();
};
var errorSubida = function (){
	$('#msgCargaDocumento').html("Hubo un error al subir el archivo. por favor intentelo nuevamente.");
};