/* 
 * Proyecto Web Unificada TDP
 * Planificacion y Solu
 * Autor Gonzalo Chacaltana Buleje <gchacaltana@gmd.com.pe>
 */
$(document).ready(function(){
   $(".trigger").mouseover(function(){
		$(".panel").toggle("fast");
		$(this).toggleClass("active");
		return false;
    });
    $('.panel').mouseleave(function(){
       $(".panel").toggle("fast");
       $('.trigger').toggleClass("active");
    });
    //Fin Menu

   change_state=function(idpoligono_tipo,estado,nom_poligono){
        //true para desabilitar
        if(confirm("Esta seguro de cambiar el estado de la Ruta/poligono : "+nom_poligono)){
            loader('start');
    var emisor=$('#emisor').attr('value');
    var respuesta=0;
    var estado_inicial=estado;
    $.post("administracion_poligono.php?cmd=execUpdateStatePoligonoTipo", {idpoligono_tipo: idpoligono_tipo, state: estado, emisor:emisor}, function(data){
          loader('end');
          respuesta=data;

          if(respuesta==1){
              if(estado_inicial==1){
                  var nvo_estado=0;
                  $('#alink_state_'+idpoligono_tipo).attr('href','javascript:change_state(\''+idpoligono_tipo+'\',\''+nvo_estado+'\',\''+nom_poligono+'\')');
                  $('#img_state_'+idpoligono_tipo).attr('src','img/estado_deshabilitado.png');
                  $('#img_state_'+idpoligono_tipo).attr('alt','Cambiar estado a Habilitado');
                  $('#img_state_'+idpoligono_tipo).attr('title','Cambiar estado a Habilitado');
              }
              else{
                   nvo_estado=1;
                  $('#alink_state_'+idpoligono_tipo).attr('href','javascript:change_state(\''+idpoligono_tipo+'\',\''+nvo_estado+'\',\''+nom_poligono+'\')');
                  $('#img_state_'+idpoligono_tipo).attr('src','img/estado_habilitado.png');
                  $('#img_state_'+idpoligono_tipo).attr('alt','Cambiar estado a Deshabilitado');
                  $('#img_state_'+idpoligono_tipo).attr('title','Cambiar estado a Deshabilitado');
              }

          }
    });
        }
   }

   editPoligonoTipo=function(idpoligono_tipo){
    
    var emisor=$('#emisor').attr('value');
    loader('start');
    $("#childModal").html('Cargando...');
    $("#childModal").css("background-color","#FFFFFF");
    $.post("administracion_poligono.php?cmd=editPoligonoTipo", {
        idpoligono_tipo: idpoligono_tipo,
        emisor:emisor
    },
    function(data){
        loader('end');
        $("#childModal").html(data);
    });

    $("#childModal").dialog({modal:true, width:'500px', hide: 'slide', title: 'Editar Tipo Ruta/poligono', position:'top'});

   }
   

   view_ruta_poligono_mapa=function(idpoligono_tipo, tipo){
        var emisor=$('#emisor').attr('value');
        loader('start');
    $("#childModal").html('Cargando...');
    $("#childModal").css("background-color","#FFFFFF");
    $.post("administracion_poligono.php?cmd=viewRutaPoligonoMapa", {
        idpoligono_tipo	: idpoligono_tipo,
		tipo			: tipo,
        emisor			: emisor
    },
    function(data){
        loader('end');
        $("#childModal").html(data);
    });

    $("#childModal").dialog({modal:true, width:'800px', hide: 'slide', title: 'Visualizar Rutas/poligonos', position:'top'});

   }
   
   
   view_poligonos=function(idpoligono_tipo, tipo){
        var emisor=$('#emisor').attr('value');
        loader('start');
		$("#childModal").html('Cargando..');
		$("#childModal").css("background-color","#FFFFFF");
		$.post("administracion_poligono.php?cmd=viewPoligonos", {
			idpoligono_tipo	: idpoligono_tipo,
			tipo			: tipo,
			emisor			: emisor
		},
		function(data){
			loader('end');
			$("#childModal").html(data);
		});

		$("#childModal").dialog({modal:true, width:'520px', hide: 'slide', title: 'Listado de Rutas/poligonos', position:'top'});

   }
   
   $('#btn_buscar').click(function(){
       
	   
	   
	   
   });

   $('#img_down_pdf').click(function(){
       alert("Muy pronto..");
   });

   $('#img_down_xls').click(function(){
       alert("Muy pronto..");
   });

});

