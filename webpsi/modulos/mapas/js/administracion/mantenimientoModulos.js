/* 
 * Sistema Web Unificada TDP
 * A.P. Gonzalo Chacaltana Buleje gchacaltana@gmd.com.pe
 * GMD S.A
 */
$(document).ready(function(){
   $(".trigger").mouseover(function(){
		$(".panel").toggle("fast");
		$(this).toggleClass("active");
		return false;
    });
    $('.panel').mouseleave(function(){
       $(".panel").toggle("fast");
       $('.trigger').toggleClass("active");
    });
    //Fin Menu
    
    infoModulo=function(idmodulo){
    var emisor=$('#emisor').attr('value');
    loader('start');
    $("#childModal").html('');
    $("#childModal").css("background-color","#FFFFFF");
    $.post("administracion.php?cmd=viewInfoModulo", {
        idmodulo: idmodulo,
        emisor:emisor
    },
    function(data){
        loader('end');
        $("#childModal").html(data);
    });

    $("#childModal").dialog({modal:true, width:'720px', hide: 'slide', title: 'Informaci&oacute;n del Modulo', position:'top'});
    
    }
    
    change_state=function(id_modulo,estado,nom_modulo){
        //true para desabilitar
        if(confirm("Esta seguro de cambiar el estado del Modulo : "+nom_modulo)){
            loader('start');
    var emisor=$('#emisor').attr('value');
    var respuesta=0;
    var estado_inicial=estado;
    $.post("administracion.php?cmd=execUpdateStateModulo", {id_modulo: id_modulo, state: estado, emisor:emisor}, function(data){
          loader('end');
          respuesta=data;

          if(respuesta==1){
              if(estado_inicial==1){
                  var nvo_estado=0;
                  $('#alink_state_'+id_modulo).attr('href','javascript:change_state(\''+id_modulo+'\',\''+nvo_estado+'\',\''+nom_modulo+'\')');
                  $('#img_state_'+id_modulo).attr('src','img/estado_deshabilitado.png');
                  $('#img_state_'+id_modulo).attr('alt','Cambiar estado a Habilitado');
                  $('#img_state_'+id_modulo).attr('title','Cambiar estado a Habilitado');
              }
              else{
                   nvo_estado=1;
                  $('#alink_state_'+id_modulo).attr('href','javascript:change_state(\''+id_modulo+'\',\''+nvo_estado+'\',\''+nom_modulo+'\')');
                  $('#img_state_'+id_modulo).attr('src','img/estado_habilitado.png');
                  $('#img_state_'+id_modulo).attr('alt','Cambiar estado a Deshabilitado');
                  $('#img_state_'+id_modulo).attr('title','Cambiar estado a Deshabilitado');
              }

          }
    });
        }
   }
   
   editModulo=function(id_modulo){
    
    var emisor=$('#emisor').attr('value');
    loader('start');
    $("#childModal").html('');
    $("#childModal").css("background-color","#FFFFFF");
    $.post("administracion.php?cmd=editModulo", {
        id_modulo: id_modulo,
        emisor:emisor
    },
    function(data){
        loader('end');
        $("#childModal").html(data);
    });

    $("#childModal").dialog({modal:true, width:'700px', hide: 'slide', title: 'Editar M&oacute;dulo', position:'top'});

   }
    
});

