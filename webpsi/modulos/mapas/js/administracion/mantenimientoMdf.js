$(document).ready(function(){
var emisor=$('#emisor').val();
$(".trigger").mouseover(function(){
		$(".panel").toggle("fast");
		$(this).toggleClass("active");
		return false;
    });
    $('.panel').mouseleave(function(){
       $(".panel").toggle("fast");
       $('.trigger').toggleClass("active");
    });
    //Fin Menu
    
$('#zonal').change(function(){
   var zonal=$('#zonal').attr('value');
   
   
   if(zonal!=0){
    $('#ciudad').css('display','none');
    $('#msg').html('<img src="img/loading.gif" alt="" border="0" />');
    
    $.post("administracion.php?cmd=viewCiudades_x_Zonal", {
        zonal: zonal,
        emisor:emisor
    },
    function(data){
        $('#msg').empty();
        $('#ciudad').empty();
        $('#ciudad').css('display','block');
        $('#ciudad').append(data);
    });

   }else{
       alert("Seleccione una Zonal");
   }
   

});

//Buscar por Zonal y Ciudad

$('#btn_buscar_x_zonal_ciudad').click(function(){
    filtrarBusquedaZonalCiudad();
});

$('#ciudad').change(function(){
   filtrarBusquedaZonalCiudad();
});

filtrarBusquedaZonalCiudad=function(){
  var filtro_zonal=$('#zonal').attr('value');
  var filtro_ciudad=$('#ciudad').attr('value');

  if(filtro_zonal==0){alert("Seleccione una Zonal.");}else{

      if(filtro_ciudad==0){
          filtro_ciudad='';
      }
      
      $('#tipo_busqueda').attr('value','filtro_zonal_ciudad');
      $('#filtro_busqueda_zonal').attr('value',filtro_zonal);
      $('#filtro_busqueda_ciudad').attr('value',filtro_ciudad);
      loader("start");
      $.post("administracion.php?cmd=listarBusquedaPorFiltros", {
        filtro_zonal: filtro_zonal,
        filtro_ciudad:filtro_ciudad,
        emisor:emisor
        },
        function(data){
            loader("end");
            $("#tb_resultado").empty();
            $("#tb_resultado").append(data);
        });

  }

}

$('#btn_buscar_x_mdf').click(function(){
    filtrarBusquedaMdf();
});

$('#buscom_mdf').keypress(function(e) {
      if(e.which == 13) {
       filtrarBusquedaMdf();
      }
});

$('#btnVerTodos').click(function(){
   location="administracion.php?cmd=listMdf";
});

filtrarBusquedaMdf=function(){
    var cadenaMdf=$('#buscom_mdf').val();
    if (cadenaMdf=='') {
        alert("Ingrese el Mdf a buscar.");$('#buscom_mdf').focus();
    } else {
        if (cadenaMdf.length<2) {
            alert("El dato ingresado debe contener 4 caracteres.");$('#buscom_mdf').focus();
        } else {
            $('#tipo_busqueda').attr('value','filtro_cadena');
            $('#filtro_cadena').attr('value',cadenaMdf);
            loader("start");
            $.post("administracion.php?cmd=listarBusquedaPorMdf", {
            cadenaMdf: cadenaMdf,
            emisor:emisor
            },
            function(data){
            loader("end");
            $("#tb_resultado").empty();
            $("#tb_resultado").append(data);
            });
            
        }
    }
}

//Ordenamiento

$('.link_hd').click(function(){
    
    var valor=$(this).attr('rel');
    var total=$('#tl_registros').val();
    var direction_order=$('#direction_order_'+valor).attr('value');
    
    var tipo_busqueda=$('#tipo_busqueda').val();
    var filtro_zonal=$('#filtro_busqueda_zonal').val();
    var filtro_ciudad=$('#filtro_busqueda_ciudad').val();
    var filtro_cadena=$('#filtro_busqueda_mdf').val();

    if(direction_order=="none"){
        $('#direction_order_'+valor).attr('value','asc');
        $('#img_order_'+valor).attr('src','img/desc.gif');
        $(this).attr('title','Ordenar en forma ascendente.');
    }

        if(direction_order=="asc"){
        
        $('#rel_order_actual').attr('value',valor);
        $('#direction_order_actual').attr('value','asc');
        loader("start");
        $.post("administracion.php?cmd=listarMdfOrdenado", {
        valor_campo: valor,
        direction:'asc',
        total:total,
        emisor:emisor,
        tipo_busqueda:tipo_busqueda,
        filtro_zonal:filtro_zonal,
        filtro_ciudad:filtro_ciudad,
        filtro_cadena:filtro_cadena
        },
        function(data){
            loader("end");
            $("#tb_resultado").empty();
            $("#tb_resultado").append(data);

        });

        $('#img_order_'+valor).attr('src','img/asc.gif');
        $('#direction_order_'+valor).attr('value','desc');
        $(this).attr('title','Ordenar en forma descendente.');
    }
    
    if(direction_order=="desc"){

        $('#rel_order_actual').attr('value',valor);
        $('#direction_order_actual').attr('value','desc');
        loader("start");
        $.post("administracion.php?cmd=listarMdfOrdenado", {
        valor_campo: valor,
        direction:'desc',
        total:total,
        emisor:emisor,
        tipo_busqueda:tipo_busqueda,
        filtro_zonal:filtro_zonal,
        filtro_ciudad:filtro_ciudad,
        filtro_cadena:filtro_cadena
        },
        function(data){
            loader("end");
            $("#tb_resultado").empty();
            $("#tb_resultado").append(data);
        });

        $('#img_order_'+valor).attr('src','img/desc.gif');
        $('#direction_order_'+valor).attr('value','asc');
        $(this).attr('title','Ordenar en forma ascendente.');
    }
});

infoMdf=function(){
    alert("Estamos trabajando..");
}

editMdf=function(){
    alert("Estamos trabajando..");
}

zonaSegura=function(mdf){
    var emisor=$('#emisor').attr('value');
    loader('start');
    $("#childModal").html('');
    $("#childModal").css("background-color","#FFFFFF");
    $.post("administracion.php?cmd=zonaSeguraXmdf", {
        mdf: mdf,
        emisor:emisor
    },
    function(data){
        loader('end');
        $("#childModal").html(data);
    });

    $("#childModal").dialog({modal:true, width:'720px', hide: 'slide', title: 'Zona Segura', position:'top'});
}

editAgendableMdf = function(mdf,zonal){
    
    var emisor=$('#emisor').attr('value');
    loader('start');
    $("#childModal").html('');
    $("#childModal").css("background-color","#FFFFFF");
    $.post("administracion.php?cmd=modalUpAgendableMdf", {
        mdf: mdf,
        zonal:zonal,
        emisor:emisor
    },
    function(data){
        loader('end');
        $("#childModal").html(data);
    });

    $("#childModal").dialog({modal:true, width:'720px', hide: 'slide', title: 'Permisos de agendamiento MDF', position:'top'});
    
}

});

