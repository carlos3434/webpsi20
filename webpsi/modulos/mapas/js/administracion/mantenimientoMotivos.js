/* 
 * Proyecto Web Unificada TDP
 * GMD S.A
 * A.P. Gonzalo Chacaltana Buleje <gchacaltana@gmd.com.pe>
 */
$(document).ready(function(){
   $(".trigger").mouseover(function(){
		$(".panel").toggle("fast");
		$(this).toggleClass("active");
		return false;
    });
    $('.panel').mouseleave(function(){
       $(".panel").toggle("fast");
       $('.trigger').toggleClass("active");
    });
    //Fin Menu

   infoMotivo=function(idmotivo){
    var emisor=$('#emisor').attr('value');
    loader('start');
    $("#childModal").html('');
    $("#childModal").css("background-color","#FFFFFF");
    $.post("administracion.php?cmd=viewInfoMotivo", {
        idmotivo: idmotivo,
        emisor:emisor
    },
    function(data){
        loader('end');
        $("#childModal").html(data);
    });
   $("#childModal").dialog({modal:true, width:'720px', hide: 'slide', title: 'Informaci&oacute;n del Motivo', position:'top'});
   }
   
   change_state=function(id_motivo,estado,nom_motivo){
        //true para desabilitar
        if(confirm("Esta seguro de cambiar el estado del Motivo : "+nom_motivo)){
            loader('start');
    var emisor=$('#emisor').attr('value');
    var respuesta=0;
    var estado_inicial=estado;
    $.post("administracion.php?cmd=execUpdateStateMotivo", {id_motivo: id_motivo, state: estado, emisor:emisor}, function(data){
          loader('end');
          respuesta=data;

          if(respuesta==1){
              if(estado_inicial==1){
                  var nvo_estado=0;
                  $('#alink_state_'+id_motivo).attr('href','javascript:change_state(\''+id_motivo+'\',\''+nvo_estado+'\',\''+nom_motivo+'\')');
                  $('#img_state_'+id_motivo).attr('src','img/estado_deshabilitado.png');
                  $('#img_state_'+id_motivo).attr('alt','Cambiar estado a Habilitado');
                  $('#img_state_'+id_motivo).attr('title','Cambiar estado a Habilitado');
              }
              else{
                   nvo_estado=1;
                  $('#alink_state_'+id_motivo).attr('href','javascript:change_state(\''+id_motivo+'\',\''+nvo_estado+'\',\''+nom_motivo+'\')');
                  $('#img_state_'+id_motivo).attr('src','img/estado_habilitado.png');
                  $('#img_state_'+id_motivo).attr('alt','Cambiar estado a Deshabilitado');
                  $('#img_state_'+id_motivo).attr('title','Cambiar estado a Deshabilitado');
              }

          }
    });
    }
   }

   editMotivo=function(id_motivo){
    
    var emisor=$('#emisor').attr('value');
    loader('start');
    $("#childModal").html('');
    $("#childModal").css("background-color","#FFFFFF");
    $.post("administracion.php?cmd=editMotivo", {
        id_motivo: id_motivo,
        emisor:emisor
    },
    function(data){
        loader('end');
        $("#childModal").html(data);
    });

    $("#childModal").dialog({modal:true, width:'560px', hide: 'slide', title: 'Editar Motivo', position:'top'});

   }

});

