$(document).ready(function(){
var emisor=$('#emisor').val();
$(".trigger").mouseover(function(){
		$(".panel").toggle("fast");
		$(this).toggleClass("active");
		return false;
    });
    $('.panel').mouseleave(function(){
       $(".panel").toggle("fast");
       $('.trigger').toggleClass("active");
    });
    //Fin Menu


$('#btnAll').click(function(){
   location="administracion.php?cmd=listCATV";
});

$('#btnFiltrar').click(function(){
   
   var arreglo_oficinas=new Array();
   var arreglo_jefaturas=new Array();
   arreglo_oficinas=load_array('multiselect_oficina[]');
   arreglo_jefaturas=load_array('multiselect_jefatura[]');
   $("#tb_resultado").hide();
   $("#box_resultado").show();
   $.post("administracion.php?cmd=listarCATVFiltrado", {
   arreglo_oficinas: arreglo_oficinas,
   arreglo_jefaturas:arreglo_jefaturas,
   emisor:emisor
},function(data){
$("#tb_resultado").empty();
$("#box_resultado").hide();
$("#tb_resultado").css('display','block');
$("#tb_resultado").append(data);

}); 
   
});


//Ordenamiento

$('.link_hd').click(function(){
    
    var valor=$(this).attr('rel');
    var total=$('#tl_registros').val();
    var direction_order=$('#direction_order_'+valor).attr('value');
    
    var arreglo_oficinas=new Array();
    var arreglo_jefaturas=new Array();
    arreglo_oficinas=load_array('multiselect_oficina[]');
    arreglo_jefaturas=load_array('multiselect_jefatura[]');

    if(direction_order=="none"){
        $('#direction_order_'+valor).attr('value','asc');
        $('#img_order_'+valor).attr('src','img/desc.gif');
        $(this).attr('title','Ordenar en forma ascendente.');
    }
        if(direction_order=="asc"){
        
        $('#rel_order_actual').attr('value',valor);
        $('#direction_order_actual').attr('value','asc');
        loader("start");
        $.post("administracion.php?cmd=listarCATVOrdenado", {
        valor_campo: valor,
        direction:'asc',
        total:total,
        emisor:emisor,
        arreglo_oficinas:arreglo_oficinas,
        arreglo_jefaturas:arreglo_jefaturas
        },
        function(data){
            loader("end");
            $("#tb_resultado").empty();
            $("#tb_resultado").append(data);
        });

        $('#img_order_'+valor).attr('src','img/asc.gif');
        $('#direction_order_'+valor).attr('value','desc');
        $(this).attr('title','Ordenar en forma descendente.');
    }
    
    if(direction_order=="desc"){

        $('#rel_order_actual').attr('value',valor);
        $('#direction_order_actual').attr('value','desc');
        loader("start");
        $.post("administracion.php?cmd=listarCATVOrdenado", {
        valor_campo: valor,
        direction:'desc',
        total:total,
        emisor:emisor,
        arreglo_oficinas:arreglo_oficinas,
        arreglo_jefaturas:arreglo_jefaturas
        },
        function(data){
            loader("end");
            $("#tb_resultado").empty();
            $("#tb_resultado").append(data);
        });

        $('#img_order_'+valor).attr('src','img/desc.gif');
        $('#direction_order_'+valor).attr('value','asc');
        $(this).attr('title','Ordenar en forma ascendente.');
    }
});

load_array=function(element){
        var grupo=new Array();
        $("input[name='"+element+"']:checked").each(function(){grupo.push($(this).val());});
        return grupo;
}
    
infoCatv=function(nodo){
    alert("Estamos trabajando..");
}

editCatv=function(nodo){
    alert("Estamos trabajando..");
}

zonaSegura=function(nodo){
    var emisor=$('#emisor').attr('value');
    loader('start');
    $("#childModal").html('');
    $("#childModal").css("background-color","#FFFFFF");
    $.post("administracion.php?cmd=zonaSeguraXcatv", {
        nodo: nodo,
        emisor:emisor
    },
    function(data){
        loader('end');
        $("#childModal").html(data);
    });

    $("#childModal").dialog({modal:true, width:'720px', hide: 'slide', title: 'Zona Segura', position:'top'});
}


});


