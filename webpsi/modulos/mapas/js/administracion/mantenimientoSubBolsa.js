/*
 * Proyecto Web Unificada TDP
 */
$(document).ready(function(){
    $(".trigger").mouseover(function(){
        $(".panel").toggle("fast");
        $(this).toggleClass("active");
        return false;
    });
    $('.panel').mouseleave(function(){
        $(".panel").toggle("fast");
        $('.trigger').toggleClass("active");
    });
    //Fin Menu



    change_state=function(idsub_bolsa,estado,nom_subbolsa){
//        alert("idsubbolsa: "+idsub_bolsa+" estado: "+estado+" nom: "+nom_subbolsa);
        if(confirm("Esta seguro de cambiar el estado de la Sub Bolsa : "+nom_subbolsa)){
            loader('start');

            var respuesta=0;
            var estado_inicial=estado;

            $.post("administracion.php?cmd=execUpdateStateSubBolsa", {
                idsub_bolsa: idsub_bolsa,
                state: estado,
                emisor:nom_subbolsa
            }, function(data){
                loader('end');
                respuesta=data;

                if(respuesta==1){
                    if(estado_inicial==1){
                        var nvo_estado=0;
                        $('#alink_state_'+idsub_bolsa).attr('href','javascript:change_state(\''+idsub_bolsa+'\',\''+nvo_estado+'\',\''+nom_subbolsa+'\')');
                        $('#img_state_'+idsub_bolsa).attr('src','img/estado_deshabilitado.png');
                        $('#img_state_'+idsub_bolsa).attr('alt','Cambiar estado a Habilitado');
                        $('#img_state_'+idsub_bolsa).attr('title','Cambiar estado a Habilitado');
                    }
                    else{
                        nvo_estado=1;
                        $('#alink_state_'+idsub_bolsa).attr('href','javascript:change_state(\''+idsub_bolsa+'\',\''+nvo_estado+'\',\''+nom_subbolsa+'\')');
                        $('#img_state_'+idsub_bolsa).attr('src','img/estado_habilitado.png');
                        $('#img_state_'+idsub_bolsa).attr('alt','Cambiar estado a Deshabilitado');
                        $('#img_state_'+idsub_bolsa).attr('title','Cambiar estado a Deshabilitado');
                    }

                }
            });
        }
    }

    editSubBolsa=function(idsub_bolsa){

        var emisor=$('#emisor').attr('value');
        loader('start');
        $("#childModal").html('');
        $("#childModal").css("background-color","#FFFFFF");
        $.post("administracion.php?cmd=editSubBolsa", {
            idsub_bolsa: idsub_bolsa
        },
        function(data){
            loader('end');
            $("#childModal").html(data);
        });

        $("#childModal").dialog({
            modal:true,
            width:'500px',
            hide: 'slide',
            title: 'Editar Sub Bolsa',
            position:'top'
        });

    }

    buscaSubBolsa=function(){
        $("#desc").val();
        loader('start');
        $.post("administracion.php?cmd=buscaSubBolsa", {
            valor:$("#desc").val()
        },function(data){
            loader('end');
            $("#tb_resultado").empty();
            $("#tb_resultado").append(data);

        });


    }

    $('#img_down_pdf').click(function(){
        alert("Muy pronto..");
    });

    $('#img_down_xls').click(function(){
        alert("Muy pronto..");
    });

});

