/* 
 * Sistema Web Unificada TDP
 * A.P. Gonzalo Chacaltana Buleje gchacaltana@gmd.com.pe
 * GMD S.A
 */
$(document).ready(function(){
   $(".trigger").mouseover(function(){
		$(".panel").toggle("fast");
		$(this).toggleClass("active");
		return false;
    });
    $('.panel').mouseleave(function(){
       $(".panel").toggle("fast");
       $('.trigger').toggleClass("active");
    });
    
    
    //Fin Menu
    
    
    validacion_form=function(){
                    var emisor=$('#emisor').attr('value');
                    var descripcion=$('#descripcion').attr('value');
                    var url=$('#url').attr('value');
                    var detalle=$('#detalle').attr('value');
                    var in_menu=$('#in_menu').attr('value');
                    var modulo=$('#modulo').attr('value');
                    var sub_modulo=$('#sub_modulo').attr('value');
                    var opcion=$('#opcion').attr('value');
                    var go_process=false;
                    if(descripcion.length<2){
                        alert("Ingrese una descripcion mayor a 2 caracteres");
                        $('#descripcion').focus();
                    }else{
                        if(url.length<3){
                            alert("Ingrese una URL mayor a 3 caracteres");
                            $('#url').focus();
                        }else{
                            if(detalle.length<2){
                                alert("Ingrese un Detalle mayor a 2 caracteres");
                                $('#detalle').focus();
                            }else{
                                if(in_menu=="none"){
                                    alert("Seleccione una opcion para el submodulo.");
                                }else{
                                    if(in_menu==0){
                                        if(sub_modulo==0){
                                            alert("Seleccione el Submodulo asociado.");
                                        }else{
                                            go_process=true;
                                        }
                                    }
                                    if(in_menu==1){
                                        if(modulo==0){
                                            alert("Seleccione el Modulo que pertenece el submodulo.");
                                        }else{
                                            go_process=true;
                                        }
                                    }
                                    
                                    if(go_process==true){
                                        
                                        $('#loading').show();
                                        $.post("administracion.php?cmd=regSubModulo", {
                                        descripcion: descripcion,
                                        url:url,
                                        detalle:detalle,
                                        in_menu:in_menu,
                                        modulo:modulo,
                                        sub_modulo:sub_modulo,
                                        emisor:emisor
                                        },
                                        function(data){
                                   
                                        $('#loading').css('display','none');
                                        //$('#box_result').css('display','block');
                                        //$('#box_result').html(data);
                                        
                                        if(data==1){
                                           $('#box_result').css({
                                                    'border':'1px solid #0292E9',
                                                    'background':'#E8F2F6',
                                                    'height':'30px',
                                                    'color':'#048FE4'
                                                });
                                            $('#box_result').html('Datos Guardados.<img src="img/check.png" alt="" />');
                                            
                                            $('#box_result').show();
                                            
                                            if(opcion==0){
                                             setTimeout("clearBox()",1300);
                                             window.location="administracion.php?cmd=verFormSubModuloInsert";
                                             }else{
                                             window.location="administracion.php?cmd=listSubModulo";
                                             }
                                            
                                        }else{
                                            $('#box_result').css({
                                           'width':'350px',
                                           'border':'1px solid #CD0A0A',
                                           'background':'#FEF2EE',
                                           'color':'#CD0A0A'
                                            });
                                            $('#box_result').html('Ocurrio un error, contactese con el Administrador del Sistema. <img src="img/aviso_small.png" alt="" />');
                                            $('#box_result').show();
                                            setTimeout("clearBox()",2200);
                                            
                                        }
                                        
                                        });
                                        
                                        
                                    }
                            }
                        }
                    }
                }
                
                }
            clearBox=function(){
                $('#box_result').hide();
                $('#box_result').html('');
                }
    
    
    infoSubModulo=function(idsubmodulo){
    var emisor=$('#emisor').attr('value');
    loader('start');
    $("#childModal").html('');
    $("#childModal").css("background-color","#FFFFFF");
    $.post("administracion.php?cmd=viewInfoSubModulo", {
        idsubmodulo: idsubmodulo,
        emisor:emisor
    },
    function(data){
        loader('end');
        $("#childModal").html(data);
    });

    $("#childModal").dialog({modal:true, width:'720px', hide: 'slide', title: 'Informaci&oacute;n del Sub Modulo', position:'top'});
    
    }
    
    //wsandoval agregado 17/07/2012
    accesoSubModulo=function(idsubmodulo){
        var emisor=$('#emisor').attr('value');
        loader('start');
        $("#childModal").html('');
        $("#childModal").css("background-color","#FFFFFF");
        $.post("administracion.php?cmd=accesoSubModulo", {
            idsubmodulo: idsubmodulo,
            emisor:emisor
        },
        function(data){
            loader('end');
            $("#childModal").html(data);
        });

        $("#childModal").dialog({modal:true, width:'400px', hide: 'slide', title: 'Asignar/Restringir Sub Modulo', position:'top'});
        
    }
    //fin wsandoval agregado 17/07/2012
    
    change_state=function(id_submodulo,estado,nom_submodulo){
        //true para desabilitar
        if(confirm("Esta seguro de cambiar el estado del Sub Modulo : "+nom_submodulo)){
            loader('start');
    var emisor=$('#emisor').attr('value');
    var respuesta=0;
    var estado_inicial=estado;
    $.post("administracion.php?cmd=execUpdateStateSubModulo", {id_submodulo: id_submodulo, state: estado, emisor:emisor}, function(data){
          loader('end');
          respuesta=data;

          if(respuesta==1){
              if(estado_inicial==1){
                  var nvo_estado=0;
                  $('#alink_state_'+id_submodulo).attr('href','javascript:change_state(\''+id_submodulo+'\',\''+nvo_estado+'\',\''+nom_submodulo+'\')');
                  $('#img_state_'+id_submodulo).attr('src','img/estado_deshabilitado.png');
                  $('#img_state_'+id_submodulo).attr('alt','Cambiar estado a Habilitado');
                  $('#img_state_'+id_submodulo).attr('title','Cambiar estado a Habilitado');
              }
              else{
                   nvo_estado=1;
                  $('#alink_state_'+id_submodulo).attr('href','javascript:change_state(\''+id_submodulo+'\',\''+nvo_estado+'\',\''+nom_submodulo+'\')');
                  $('#img_state_'+id_submodulo).attr('src','img/estado_habilitado.png');
                  $('#img_state_'+id_submodulo).attr('alt','Cambiar estado a Deshabilitado');
                  $('#img_state_'+id_submodulo).attr('title','Cambiar estado a Deshabilitado');
              }

          }
    });
        }
   }
   
   editSubModulo=function(id_submodulo){
    var emisor=$('#emisor').attr('value');
    loader('start');
    $("#childModal").html('');
    $("#childModal").css("background-color","#FFFFFF");
    $.post("administracion.php?cmd=editSubModulo", {
        id_submodulo: id_submodulo,
        emisor:emisor
    },
    function(data){
        loader('end');
        $("#childModal").html(data);
    });

    $("#childModal").dialog({modal:true, width:'700px', hide: 'slide', title: 'Editar Sub M&oacute;dulo', position:'top'});

   }
   
    
});