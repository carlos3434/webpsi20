/*
 * Proyecto Sistema Web Unificada TDP
 * A.P. Gonzalo Chacaltana Buleje gchacaltana@gmd.com.pe
 */
$(document).ready(function(){
   $(".trigger").mouseover(function(){
		$(".panel").toggle("fast");
		$(this).toggleClass("active");
		return false;
    });
    $('.panel').mouseleave(function(){
       $(".panel").toggle("fast");
       $('.trigger').toggleClass("active");
    });
    //Fin Menu

    $('#operadores').click(function(){
       var operador=$(this).attr('value');
       var emisor=$('#emisor').val();
       $('#data_horario').html('Cargando datos.<img src="img/loading.gif" alt="" />');
       
       $.post("administracion.php?cmd=viewHorariosXoperador", {
        idusuario: operador,
        emisor:emisor
        },
        function(data){
            $('#data_horario').empty();
            $('#data_horario').append(data);

            $('.save_horarios').click(function(){

                var dia=$(this).attr('id');
                var operacion=$(this).attr('title');

                if(operacion=="Grabar Cambios"){

                var operador=$('#operador_name').attr('value');
                //Turno mañana
                var hh_ini_man=$('#turno_man_ini_'+dia).val();
                var hh_fin_man=$('#turno_man_fin_'+dia).attr('value');
                //Turno tarde
                var hh_ini_tar=$('#turno_tar_ini_'+dia).attr('value');
                var hh_fin_tar=$('#turno_tar_fin_'+dia).attr('value');
                //Turno noche
                var hh_ini_noc=$('#turno_noc_ini_'+dia).attr('value');
                var hh_fin_noc=$('#turno_noc_fin_'+dia).attr('value');

                var stateProcess=true;


                if(hh_ini_man=="00:0" && hh_fin_man=="00:0" && hh_ini_tar=="00:0" && hh_fin_tar=="00:0" && hh_ini_noc=="00:0" && hh_fin_noc=="00:0"){
                    alert("Seleccione los horarios de trabajo del dia "+dia+ "\n\ del operador "+operador);
                }else{

                    if((hh_ini_man!="00:0" && hh_fin_man=="00:0") || (hh_ini_man=="00:0" && hh_fin_man!="00:0")){
                     stateProcess=false;
                     alert("Selecione correctamente los horarios de trabajo del turno de la mañana.");
                    }else{
                        if(hh_ini_man!="00:0" && hh_ini_man!="00:0"){$('#turno_manana_'+dia).attr('value',1);}
                    }

                    if((hh_ini_tar!="00:0" && hh_fin_tar=="00:0") || (hh_ini_tar=="00:0" && hh_fin_tar!="00:0")){
                     stateProcess=false;
                     alert("Selecione correctamente los horarios de trabajo del turno de la tarde.");
                    }else{
                        if(hh_ini_tar!="00:0" && hh_ini_tar!="00:0"){$('#turno_tarde_'+dia).attr('value',1);}
                    }

                    if((hh_ini_noc!="00:0" && hh_fin_noc=="00:0") || (hh_ini_noc=="00:0" && hh_fin_noc!="00:0")){
                     stateProcess=false;
                     alert("Selecione correctamente los horarios de trabajo del turno de la noche.");
                    }else{
                        if(hh_ini_noc!="00:0" && hh_ini_noc!="00:0"){$('#turno_noche_'+dia).attr('value',1);}
                    }

                    if(stateProcess==true){
                        var tipo_mensaje_alerta="";
                        var estado_horario=$('#estado').val();
                        if(estado_horario=="-1"){ tipo_mensaje_alerta="registrar";}else{ tipo_mensaje_alerta="actualizar";}
                        
                        if(confirm("Esta seguro de "+ tipo_mensaje_alerta +" el horario de trabajo del dia "+dia+" \n\ para el operador "+operador)){
                            var turno_man=$('#turno_manana_'+dia).val();
                            var turno_tar=$('#turno_tarde_'+dia).val();
                            var turno_noc=$('#turno_noche_'+dia).val();


                            var total_hh_turno_man=0;
                            var total_hh_turno_tar=0;
                            var total_hh_turno_noc=0;
                            var total_hh_dia="00:00";

                            var total_turno_manana=$('#hh_tl_tur_man').attr('value');
                            var total_turno_tarde=$('#hh_tl_tur_tar').attr('value');
                            var total_turno_noche=$('#hh_tl_tur_noc').attr('value');

                            var total_hh_semana=$('#horas_totales_semana').attr('value');

                            var go_process=false;

                            if(turno_man==1){
                                total_hh_turno_man=restar_horas(hh_fin_man,hh_ini_man);

                                if(total_hh_turno_man!=false){
                                    go_process=true;
                                    $('#acumulado_turno_man_'+dia).attr('value',total_hh_turno_man);
                                    total_hh_dia=sumar_horas(total_hh_dia,total_hh_turno_man);
                                    total_turno_manana=sumar_horas(total_turno_manana,total_hh_turno_man);
                                    total_hh_semana=sumar_horas(total_hh_semana,total_hh_turno_man);
                                    
                                    if(estado_horario!="-1"){
                                        $('#se_modifico_turno_man_'+dia).attr('value',1);
                                    }
                                    
                                }else{
                                    alert("Los horarios del turno de la mañana son incorrectos.");
                                }

                            }

                            if(turno_tar==1){
                                total_hh_turno_tar=restar_horas(hh_fin_tar,hh_ini_tar);
                                if(total_hh_turno_tar!=false){
                                go_process=true;
                                $('#acumulado_turno_tar_'+dia).attr('value',total_hh_turno_tar);
                                total_hh_dia=sumar_horas(total_hh_dia,total_hh_turno_tar);
                                total_turno_tarde=sumar_horas(total_turno_tarde,total_hh_turno_tar);
                                total_hh_semana=sumar_horas(total_hh_semana,total_hh_turno_tar);
                                if(estado_horario!="-1"){
                                        $('#se_modifico_turno_tar_'+dia).attr('value',1);
                                    }
                                }else{
                                alert("Los horarios del turno de la tarde son incorrectos.");
                                }

                            }

                            if(turno_noc==1){
                                total_hh_turno_noc=restar_horas(hh_fin_noc,hh_ini_noc);
                                if(total_hh_turno_noc!=false){
                                go_process=true;
                                $('#acumulado_turno_noc_'+dia).attr('value',total_hh_turno_noc);
                                total_hh_dia=sumar_horas(total_hh_dia,total_hh_turno_noc);
                                total_turno_noche=sumar_horas(total_turno_noche,total_hh_turno_noc);
                                total_hh_semana=sumar_horas(total_hh_semana,total_hh_turno_noc);
                                if(estado_horario!="-1"){
                                        $('#se_modifico_turno_noc_'+dia).attr('value',1);
                                    }
                                }else{
                                alert("Los horarios del turno de la noche son incorrectos.");
                                }

                            }

                            if(go_process==true){
                                
                                //Activamos campo oculto que valida si el dia a sido ingresado un horario
                                $('#set_data_dia_'+dia).attr('value',1);
                                
                                if(estado_horario!='-1'){
                                    //Activamos campo oculto que valida si el dia a sido actualizado un horario 
                                    $('#row_have_data_update_'+dia).attr('value',1);
                                }
                                
                                //Cargamos y mostramos las horas acumuladas
                                $('#box_hh_tl_'+dia).empty();
                                $('#hh_tl_'+dia).attr('value',total_hh_dia);
                                $('#box_hh_tl_'+dia).html(total_hh_dia);

                                $('#box_hh_tl_tur_man').empty();
                                $('#box_hh_tl_tur_man').html(total_turno_manana);
                                $('#hh_tl_tur_man').attr('value',total_turno_manana);

                                $('#box_hh_tl_tur_tar').empty();
                                $('#box_hh_tl_tur_tar').html(total_turno_tarde);
                                $('#hh_tl_tur_tar').attr('value',total_turno_tarde);

                                $('#box_hh_tl_tur_noc').empty();
                                $('#box_hh_tl_tur_noc').html(total_turno_noche);
                                $('#hh_tl_tur_noc').attr('value',total_turno_noche);

                                $('#box_horas_totales_semana').empty();
                                $('#box_horas_totales_semana').html(total_hh_semana);
                                $('#horas_totales_semana').attr('value',total_hh_semana);

                                //Mostrar Horarios sin editar
                                //Turno Mañana
                                $('#box_turno_man_ini_'+dia).empty();
                                $('#box_turno_man_ini_'+dia).html(hh_ini_man);
                                $('#turno_man_ini_'+dia).css('display','none');

                                $('#box_turno_man_fin_'+dia).empty();
                                $('#box_turno_man_fin_'+dia).html(hh_fin_man);
                                $('#turno_man_fin_'+dia).css('display','none');

                                //Turno Mañana
                                $('#box_turno_tar_ini_'+dia).empty();
                                $('#box_turno_tar_ini_'+dia).html(hh_ini_tar);
                                $('#turno_tar_ini_'+dia).css('display','none');

                                $('#box_turno_tar_fin_'+dia).empty();
                                $('#box_turno_tar_fin_'+dia).html(hh_fin_tar);
                                $('#turno_tar_fin_'+dia).css('display','none');

                                //Turno Noche
                                $('#box_turno_noc_ini_'+dia).empty();
                                $('#box_turno_noc_ini_'+dia).html(hh_ini_noc);
                                $('#turno_noc_ini_'+dia).css('display','none');

                                $('#box_turno_noc_fin_'+dia).empty();
                                $('#box_turno_noc_fin_'+dia).html(hh_fin_noc);
                                $('#turno_noc_fin_'+dia).css('display','none');

                                $(this).attr('src','img/pencil.png');
                                $(this).attr('alt','Editar Horario');
                                $(this).attr('title','Editar Horario');
                                
                            }



                          }

                    }


                }


                }else{
                    if(operacion=="Editar Horario"){
                                var nuevo_o_data_horario=$('#estado').attr('value');
                                var img_id=$(this).attr('id');
                                
                                if(nuevo_o_data_horario=='-1'){
                                 editarHorario(img_id,dia,0);
                                }else{

                                    var dia_seleccion=$('#fecha_dia_'+dia).attr('value');
                                    
                                    var date_today_server;
                                    var hora_today_server;
                                    $.post("administracion.php?cmd=bringDateTodayServer", {
                                    emisor:emisor
                                    },
                                    function(data){
                                        date_today_server=data;
                                        if(dia_seleccion==date_today_server){
                                                    $.post("administracion.php?cmd=bringHourTodayServer", {
                                                    emisor:emisor
                                                    },
                                                    function(data){
                                                    hora_today_server=data;
                                                    if(hora_today_server>=8 && hora_today_server<13){
                                                        editarHorario(img_id,dia,1);
                                                    }else{
                                                        if(hora_today_server>=13 && hora_today_server<18){
                                                         editarHorario(img_id,dia,2);
                                                        }else{
                                                            if(hora_today_server>=18){
                                                                editarHorario(img_id,dia,3);
                                                            }
                                                        }
                                                            
                                                    }
                                                    
                                                    });
                                        }else{
                                           editarHorario(img_id,dia,0);
                                        }
                                        
                                    });                                    
                                    //alert(date_today_server);
  
                                    
                                }
                    }
                }

                
                

            });
            
            editarHorario=function(obj,dia,denegado){
                var valida_estado_horario_actual=$('#estado').val();//Estado del horario (Nuevo, incompleto o completo);
                   //Mostrar Horarios para editar
                                //Turno Mañana
                                //alert(obj);
                                if(denegado!=1 && denegado!=2 && denegado!=3){
                                $('#box_turno_man_ini_'+dia).empty();
                                $('#turno_man_ini_'+dia).css('display','block');
                                $('#turno_man_ini_'+dia).attr('disabled','');
                                
                                $('#box_turno_man_fin_'+dia).empty();
                                $('#turno_man_fin_'+dia).css('display','block');
                                $('#turno_man_fin_'+dia).attr('disabled','');
                                }
                                //Turno Mañana
                                if(denegado!=2 && denegado!=3){
                                $('#box_turno_tar_ini_'+dia).empty();
                                $('#turno_tar_ini_'+dia).css('display','block');
                                $('#turno_tar_ini_'+dia).attr('disabled','');

                                $('#box_turno_tar_fin_'+dia).empty();
                                $('#turno_tar_fin_'+dia).css('display','block');
                                $('#turno_tar_fin_'+dia).attr('disabled','');
                                }
                                //Turno Noche
                                if(denegado!=3){
                                $('#box_turno_noc_ini_'+dia).empty();
                                $('#turno_noc_ini_'+dia).css('display','block');
                                $('#turno_noc_ini_'+dia).attr('disabled','');
                                
                                $('#box_turno_noc_fin_'+dia).empty();
                                $('#turno_noc_fin_'+dia).css('display','block');
                                $('#turno_noc_fin_'+dia).attr('disabled','');
                                }
                                
                                //Desactivamos campo oculto que valida si el dia a sido ingresado un horario
                                
                                $('#set_data_dia_'+dia).attr('value',0);
                                if(valida_estado_horario_actual!="-1"){
                                //Desactivamos campo oculto que valida si el dia a sido actualizado un horario
                                $('#row_have_data_update_'+dia).attr('value',0);  
                                }
                               
                                if(valida_estado_horario_actual=="-1"){
                                procesarSumaHorario();
                                }
                                else{
                                procesarSumaHorario_Actualizar();
                                }

                                //Cambiar img boton
                                $('#'+obj).attr('src','img/save_button.png');
                                $('#'+obj).attr('alt','Grabar Cambios');
                                $('#'+obj).attr('title','Grabar Cambios');
            }

            $('#btnGuardarHorario').click(function(){
                 var estado_horario=$('#estado').val();
                 var mensaje_alert="";
                 var i;
                 var go_save_process=false;
                 var set_data_ingreso_dia;
                 var dias=new Array();
                 dias[1]="lunes";
                 dias[2]="martes";
                 dias[3]="miercoles";
                 dias[4]="jueves";
                 dias[5]="viernes";
                 dias[6]="sabado";
                 dias[7]="domingo";
                 
                 for(i=1;i<dias.length;i++){
                     set_data_ingreso_dia=$('#set_data_dia_'+dias[i]).attr('value');
                     if(set_data_ingreso_dia==1){go_save_process=true;}                  
                 }  
                
                if(go_save_process==true){
                    
                var operador=$('#operador_name').attr('value');
                if(estado_horario=="-1"){ mensaje_alert="guardar";}else{ mensaje_alert="actualizar";}
                if(confirm("¿ Esta seguro de "+ mensaje_alert +" el horario de trabajo semanal\n\ del operador "+operador+" ?")){
                        
                    var operador_id=$('#operador_id').attr('value');
                    var semana=$('#semana').attr('value');
                    
                    var data_horario={}
                    
                    data_horario['id_usuario']=operador_id;
                    data_horario['semana']=semana;
                    
                        
                    for(i=1;i<dias.length;i++){
                        var turno_manana_dia=$('#turno_manana_'+dias[i]).val();
                        var turno_tarde_dia=$('#turno_tarde_'+dias[i]).val();
                        var turno_noche_dia=$('#turno_noche_'+dias[i]).val();
                        var fecha_dia=$('#fecha_dia_'+dias[i]).attr('value');
                        
                        if(estado_horario!="-1"){
                            
                                var is_update_turno_manana=$('#update_turno_man_'+dias[i]).val();
                                var is_update_turno_tarde=$('#update_turno_tar_'+dias[i]).val();
                                var is_update_turno_noche=$('#update_turno_noc_'+dias[i]).val();
                                
                                var is_modifico_turno_manana=$('#se_modifico_turno_man_'+dias[i]).val();
                                var is_modifico_turno_tarde=$('#se_modifico_turno_tar_'+dias[i]).val();
                                var is_modifico_turno_noche=$('#se_modifico_turno_noc_'+dias[i]).val();
                                
                                var id_horario_detalle_manana=$('#id_horario_detalle_man_'+dias[i]).val();
                                var id_horario_detalle_tarde=$('#id_horario_detalle_tar_'+dias[i]).val();
                                var id_horario_detalle_noche=$('#id_horario_detalle_noc_'+dias[i]).val();
                                
                                //Almacenamos en Array los turnos que son para actualizar
                                data_horario['is_update_turno_man_'+dias[i]]=is_update_turno_manana;
                                data_horario['is_update_turno_tar_'+dias[i]]=is_update_turno_tarde;
                                data_horario['is_update_turno_noc_'+dias[i]]=is_update_turno_noche;
                                
                                //Almacenamos en Array los turnos que fueron actualizados
                                data_horario['is_modifico_turno_man_'+dias[i]]=is_modifico_turno_manana;
                                data_horario['is_modifico_turno_tar_'+dias[i]]=is_modifico_turno_tarde;
                                data_horario['is_modifico_turno_noc_'+dias[i]]=is_modifico_turno_noche; 
                                
                                //Almacenamos en Array los id de los horarios detalle 
                                data_horario['id_horario_detalle_turno_man_'+dias[i]]=id_horario_detalle_manana;
                                data_horario['id_horario_detalle_turno_tar_'+dias[i]]=id_horario_detalle_tarde;
                                data_horario['id_horario_detalle_turno_noc_'+dias[i]]=id_horario_detalle_noche;
                                      
                        }
                        
                        var turno;
                        var hh_ini_dia;
                        var hh_fin_dia;
                        var hh_acu_dia;

                        //falta pasar dia y fecha
                        if(turno_manana_dia==1){
                            turno="MANANA";
                            hh_ini_dia=$('#turno_man_ini_'+dias[i]).attr('value');
                            hh_fin_dia=$('#turno_man_fin_'+dias[i]).attr('value');
                            hh_acu_dia=$('#acumulado_turno_man_'+dias[i]).attr('value');
                            //Llenar Array de posibles valores Turno Mañana
                            data_horario['turno_man_'+dias[i]+'_ok']=1;
                            data_horario["turno_man_"+dias[i]]=turno;
                            data_horario["hh_ini_man_"+dias[i]]=hh_ini_dia;
                            data_horario["hh_fin_man_"+dias[i]]=hh_fin_dia;
                            data_horario["total_hh_acumulado_man_"+dias[i]]=hh_acu_dia;
                            
                       }

                        if(turno_tarde_dia==1){
                            turno="TARDE";
                            hh_ini_dia=$('#turno_tar_ini_'+dias[i]).attr('value');
                            hh_fin_dia=$('#turno_tar_fin_'+dias[i]).attr('value');
                            hh_acu_dia=$('#acumulado_turno_tar_'+dias[i]).attr('value');

                            //Llenar Array de posibles valores Turno Tarde
                            data_horario["turno_tar_"+dias[i]+"_ok"]=1;
                            data_horario["turno_tar_"+dias[i]]=turno;
                            data_horario["hh_ini_tar_"+dias[i]]=hh_ini_dia;
                            data_horario["hh_fin_tar_"+dias[i]]=hh_fin_dia;
                            data_horario["total_hh_acumulado_tar_"+dias[i]]=hh_acu_dia;
                         }

                        if(turno_noche_dia==1){
                            turno="NOCHE";
                            hh_ini_dia=$('#turno_noc_ini_'+dias[i]).attr('value');
                            hh_fin_dia=$('#turno_noc_fin_'+dias[i]).attr('value');
                            hh_acu_dia=$('#acumulado_turno_noc_'+dias[i]).attr('value');
                            
                            //Llenar Array de posibles valores
                            data_horario["turno_noc_"+dias[i]+"_ok"]=1;
                            data_horario["turno_noc_"+dias[i]]=turno;
                            data_horario["hh_ini_noc_"+dias[i]]=hh_ini_dia;
                            data_horario["hh_fin_noc_"+dias[i]]=hh_fin_dia;
                            data_horario["total_hh_acumulado_noc_"+dias[i]]=hh_acu_dia;

                        }

                        if(turno_manana_dia==1 || turno_tarde_dia==1 || turno_noche_dia==1){
                            data_horario["fecha_dia_"+dias[i]]=fecha_dia;
                        }                        
                    }
                    var cmd="";
                    if(estado_horario=="-1"){ cmd="save_horarios";}else{ cmd="update_horarios";}
                    
                    $('#loader').html('Procesando.<img src="img/loading.gif" alt="" />');
                    $.ajax({
                    type:   "POST",
                    url:    "administracion.php?cmd="+cmd,
                    data:   data_horario,
                    success: function(datos) {
                        $('#loader').empty();
                        //$('#box_resultado').css('display','block');
                        //$('#box_resultado').html(datos);                       
                        if(datos==1){                            
                            $('#box_resultado').css({
                                                    'border':'1px solid #0292E9',
                                                    'background':'#E8F2F6',
                                                    'height':'30px',
                                                    'color':'#048FE4'
                            });
                            $("#box_resultado").empty();
                            $("#box_resultado").css('display','block');
                            if(estado_horario=='-1'){
                                $("#box_resultado").html('Horario Registrado. <img src="img/check.png" alt="" />');
                            }else{
                                $("#box_resultado").html('Horario Actualizado. <img src="img/check.png" alt="" />');
                            }

                            setTimeout("clearMsg()",1000);
                            
                        }else{
                            
                            $('#box_resultado').css({
                                           'border':'1px solid #CD0A0A',
                                           'background':'#FEF2EE',
                                           'color':'#CD0A0A'
                            });
                            $("#box_resultado").empty();
                            $("#box_resultado").css('display','block');
                            $("#box_resultado").html('Ocurrio un error<br />, contacte con el Administrador del Sistema. <img src="img/ico_critico.png" alt="" />');
                            setTimeout("clearMsg()",2500);
                        }
                       
                        
                        }
                     });
                    
                }
                    
                }else{
                    var mensaje_alert_not="";
                    if(estado_horario=="-1"){ mensaje_alert_not="ingresar";}else{ mensaje_alert_not="actualizar";}
                    alert("Debe "+ mensaje_alert_not +" por lo menos un horario de algun dia de la semana actual.");
                }
                    
            });
            
        });
        
       

    });
    
function procesarSumaHorario(){
    var i,set_data_ingreso_dia;
    var hh_tl_dia;
    var hh_total_semana='00:00';
    var total_turno_manana='00:00';
    var total_turno_tarde='00:00';
    var total_turno_noche='00:00';
    var dias=new Array();
    dias[1]="lunes";
    dias[2]="martes";
    dias[3]="miercoles";
    dias[4]="jueves";
    dias[5]="viernes";
    dias[6]="sabado";
    dias[7]="domingo";
                 
    for(i=1;i<dias.length;i++){
      set_data_ingreso_dia=$('#set_data_dia_'+dias[i]).attr('value');
      if(set_data_ingreso_dia==1){
          var turno_man=$('#turno_manana_'+dias[i]).val();
          var turno_tar=$('#turno_tarde_'+dias[i]).val();
          var turno_noc=$('#turno_noche_'+dias[i]).val();
          
          var hh_acumulada_manana=$('#acumulado_turno_man_'+dias[i]).attr('value');
          var hh_acumulada_tarde=$('#acumulado_turno_tar_'+dias[i]).attr('value');
          var hh_acumulada_noche=$('#acumulado_turno_noc_'+dias[i]).attr('value');
          
          hh_tl_dia=$('#hh_tl_'+dias[i]).attr('value');
          hh_total_semana=sumar_horas(hh_total_semana,hh_tl_dia);
          
          
          if(turno_man==1){
           total_turno_manana=sumar_horas(total_turno_manana,hh_acumulada_manana);
          }
          if(turno_tar==1){
           total_turno_tarde=sumar_horas(total_turno_tarde,hh_acumulada_tarde);
          }
          if(turno_noc==1){
          total_turno_noche=sumar_horas(total_turno_noche,hh_acumulada_noche);
          }         
      }                  
    }
    //Mostrar nuevos valores totales

                                $('#box_hh_tl_tur_man').empty();
                                $('#box_hh_tl_tur_man').html(total_turno_manana);
                                $('#hh_tl_tur_man').attr('value',total_turno_manana);

                                $('#box_hh_tl_tur_tar').empty();
                                $('#box_hh_tl_tur_tar').html(total_turno_tarde);
                                $('#hh_tl_tur_tar').attr('value',total_turno_tarde);

                                $('#box_hh_tl_tur_noc').empty();
                                $('#box_hh_tl_tur_noc').html(total_turno_noche);
                                $('#hh_tl_tur_noc').attr('value',total_turno_noche);

                                $('#box_horas_totales_semana').empty();
                                $('#box_horas_totales_semana').html(hh_total_semana);
                                $('#horas_totales_semana').attr('value',hh_total_semana);
    
}

function procesarSumaHorario_Actualizar(){
    var i;
    var hh_tl_dia;
    var hh_total_semana='00:00';
    var total_turno_manana='00:00';
    var total_turno_tarde='00:00';
    var total_turno_noche='00:00';
    
    var dias=new Array();
    dias[1]="lunes";
    dias[2]="martes";
    dias[3]="miercoles";
    dias[4]="jueves";
    dias[5]="viernes";
    dias[6]="sabado";
    dias[7]="domingo";
                 
    for(i=1;i<dias.length;i++){
      var dia_change_data_horario=$('#row_have_data_update_'+dias[i]).attr('value');
      
      if(dia_change_data_horario==1){
          
      var valida_update_manana=$('#update_turno_man_'+dias[i]).attr('value');
      var valida_update_tarde=$('#update_turno_tar_'+dias[i]).attr('value');
      var valida_update_noche=$('#update_turno_noc_'+dias[i]).attr('value');
      
      var hh_acumulada_manana=$('#acumulado_turno_man_'+dias[i]).attr('value');
      var hh_acumulada_tarde=$('#acumulado_turno_tar_'+dias[i]).attr('value');
      var hh_acumulada_noche=$('#acumulado_turno_noc_'+dias[i]).attr('value');
      
      hh_tl_dia=$('#hh_tl_'+dias[i]).attr('value');
      
      hh_total_semana=sumar_horas(hh_total_semana,hh_tl_dia); 
      
      if(valida_update_manana==1){
         total_turno_manana=sumar_horas(total_turno_manana,hh_acumulada_manana);       
      }else{
         total_turno_manana=sumar_horas(total_turno_manana,hh_acumulada_manana);
      }
                                    
      if(valida_update_tarde==1){
         total_turno_tarde=sumar_horas(total_turno_tarde,hh_acumulada_tarde);
      }else{
         total_turno_tarde=sumar_horas(total_turno_tarde,hh_acumulada_tarde);
      }
                                    
      if(valida_update_noche==1){
         total_turno_noche=sumar_horas(total_turno_noche,hh_acumulada_noche);
      }else{
        total_turno_tarde=sumar_horas(total_turno_tarde,hh_acumulada_noche);
       }
      
      }
      
    }
    //alert(hh_total_semana);
                                //Mostrar nuevos valores totales

                                $('#box_hh_tl_tur_man').empty();
                                $('#box_hh_tl_tur_man').html(total_turno_manana);
                                $('#hh_tl_tur_man').attr('value',total_turno_manana);

                                $('#box_hh_tl_tur_tar').empty();
                                $('#box_hh_tl_tur_tar').html(total_turno_tarde);
                                $('#hh_tl_tur_tar').attr('value',total_turno_tarde);

                                $('#box_hh_tl_tur_noc').empty();
                                $('#box_hh_tl_tur_noc').html(total_turno_noche);
                                $('#hh_tl_tur_noc').attr('value',total_turno_noche);

                                $('#box_horas_totales_semana').empty();
                                $('#box_horas_totales_semana').html(hh_total_semana);
                                $('#horas_totales_semana').attr('value',hh_total_semana);

   
}

function restar_horas(v1,v2)
{
horas1=v1.split(":"); /*Mediante la función split separamos el string por ":" y lo convertimos en array. */ 
horas2=v2.split(":");
horatotale=new Array();
for(a=0;a<3;a++) /*bucle para tratar la hora, los minutos y los segundos*/
{
horas1[a]=(isNaN(parseInt(horas1[a])))?0:parseInt(horas1[a]) /*si horas1[a] es NaN lo convertimos a 0, sino convertimos el valor en entero*/
horas2[a]=(isNaN(parseInt(horas2[a])))?0:parseInt(horas2[a])
if(horas1[a]<horas2[a]){return false;}else{
horatotale[a]=(horas1[a]-horas2[a]);  
}
}
horatotal=new Date()  /*Instanciamos horatotal con la clase Date de javascript para manipular las horas*/
horatotal.setHours(horatotale[0]); /* En horatotal insertamos las horas, minutos y segundos calculados en el bucle*/ 
horatotal.setMinutes(horatotale[1]);
horatotal.setSeconds(horatotale[2]);
return horatotal.getHours()+":"+horatotal.getMinutes();
/*Devolvemos el valor calculado en el formato hh:mm:ss*/
}


        sumar_horas=function(v1,v2)
{

horas1=v1.split(":");
horas2=v2.split(":");
horatotale=new Array();
for(a=0;a<3;a++) 
{
horas1[a]=(isNaN(parseInt(horas1[a])))?0:parseInt(horas1[a])
horas2[a]=(isNaN(parseInt(horas2[a])))?0:parseInt(horas2[a])
horatotale[a]=(horas1[a]+horas2[a]);
}

horatotal=new Date()  /*Instanciamos horatotal con la clase Date de javascript para manipular las horas*/
horatotal.setHours(horatotale[0]); /* En horatotal insertamos las horas, minutos y segundos calculados en el bucle*/
horatotal.setMinutes(horatotale[1]);
horatotal.setSeconds(horatotale[2]);

//return horatotal.getHours()+":"+horatotal.getMinutes(); // muestra horas de 00:00 a 24:00
return horatotale[0]+":"+horatotale[1];//muestra la suma original asi pase las 24 horas
}

clearMsg=function(){$('#box_resultado').css('display','none');
window.location="administracion.php?cmd=verFormHorarioPsiInsert";
}


});
