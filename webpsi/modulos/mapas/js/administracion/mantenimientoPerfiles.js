/* 
 * Sistema Web Unificada TDP
 * A.P. Gonzalo Chacaltana Buleje gchacaltana@gmd.com.pe
 * GMD S.A
 */
$(document).ready(function(){
   $(".trigger").mouseover(function(){
		$(".panel").toggle("fast");
		$(this).toggleClass("active");
		return false;
    });
    $('.panel').mouseleave(function(){
       $(".panel").toggle("fast");
       $('.trigger').toggleClass("active");
    });
    //Fin Menu
    
    infoEmpresa=function(idempresa){
    var emisor=$('#emisor').attr('value');
    loader('start');
    $("#childModal").html('');
    $("#childModal").css("background-color","#FFFFFF");
    $.post("administracion.php?cmd=viewInfoEmpresa", {
        idempresa: idempresa,
        emisor:emisor
    },
    function(data){
        loader('end');
        $("#childModal").html(data);
    });

    $("#childModal").dialog({modal:true, width:'720px', hide: 'slide', title: 'Informaci&oacute;n de la Empresa', position:'middle'});
    
    }
    
    change_state=function(id_empresa,estado,nom_empresa){
        //true para desabilitar
        if(confirm("Esta seguro de cambiar el estado de la Empresa : "+nom_empresa)){
            loader('start');
    var emisor=$('#emisor').attr('value');
    var respuesta=0;
    var estado_inicial=estado;
    $.post("administracion.php?cmd=execUpdateStateEmpresa", {id_empresa: id_empresa, state: estado, emisor:emisor}, function(data){
          loader('end');
          respuesta=data;

          if(respuesta==1){
              if(estado_inicial==1){
                  var nvo_estado=0;
                  $('#alink_state_'+id_empresa).attr('href','javascript:change_state(\''+id_empresa+'\',\''+nvo_estado+'\',\''+nom_empresa+'\')');
                  $('#img_state_'+id_empresa).attr('src','img/estado_deshabilitado.png');
                  $('#img_state_'+id_empresa).attr('alt','Cambiar estado a Habilitado');
                  $('#img_state_'+id_empresa).attr('title','Cambiar estado a Habilitado');
              }
              else{
                   nvo_estado=1;
                  $('#alink_state_'+id_empresa).attr('href','javascript:change_state(\''+id_empresa+'\',\''+nvo_estado+'\',\''+nom_empresa+'\')');
                  $('#img_state_'+id_empresa).attr('src','img/estado_habilitado.png');
                  $('#img_state_'+id_empresa).attr('alt','Cambiar estado a Deshabilitado');
                  $('#img_state_'+id_empresa).attr('title','Cambiar estado a Deshabilitado');
              }

          }
    });
        }
   }
 
   editEmpresa=function(id_empresa){
    
    var emisor=$('#emisor').attr('value');
    loader('start');
    $("#childModal").html('');
    $("#childModal").css("background-color","#FFFFFF");
    $.post("administracion.php?cmd=editEmpresa", {
        id_empresa: id_empresa,
        emisor:emisor
    },
    function(data){
        loader('end');
        $("#childModal").html(data);
    });

    $("#childModal").dialog({modal:true, width:'700px', hide: 'slide', title: 'Editar Empresa', position:'top'});

   }
   
    
});
