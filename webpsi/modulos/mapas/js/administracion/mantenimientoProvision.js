/* 
 * Sistema Web Unificada
 * Planificacion y Soluciones Informaticas - Telefonica
 * GMD S.A
 * Autor: Gonzalo Chacaltana Buleje <gchacaltana@gmd.com.pe>
 * Mantenimiento de Provision - Ingreso Manual
 */
$(document).ready(function(){
$("#txt_numero_pedido").attr("value",'');
$("#txt_numero_pedido").focus();
$(".trigger").mouseover(function(){
$(".panel").toggle("fast");
$(this).toggleClass("active");
return false;
});
$('.panel').mouseleave(function(){
$(".panel").toggle("fast");
$('.trigger').toggleClass("active");
});
//Fin Menu
$('#txt_numero_pedido').keyup(function(e) { if(e.which == 13) { buscarNumberPedido_Provision();}});
$('#btn_buscar').click(function(){
   buscarNumberPedido_Provision();
});
                 
validarNumeroPedido =function(e){
//Valida no Crtl
var tecla = (document.all) ? e.keyCode : e.which;
if(tecla==17 || e.ctrlKey){return false;}
else{
if (tecla==8) return true;
var patron =/\w/;//valida solo letras y numeros
var te = String.fromCharCode(tecla);
return patron.test(te);
}
}
validarNumber=function(e){
tecla = (document.all) ? e.keyCode : e.which;
if(tecla==17 || e.ctrlKey){return false;}
else{
if (tecla==8) return true;
var patron =/\d/;//valida solo numeros
var te = String.fromCharCode(tecla);
return patron.test(te);
}
}

});
function buscarNumberPedido_Provision()
{
    var numero_provision = $.trim($("#txt_numero_pedido").val());
    if(numero_provision==''){
        show_alert('Sistema Web Unificada','Ingrese el n&uacute;mero de Pedido a buscar.','Por favor');
        $('#txt_numero_pedido').focus();
        
    }else{
       var envio = "pedido="+numero_provision;
    //alert(envio);
	$('#box_loading').css('visibility','visible');
 	$.ajax({
	    type: "POST",
	    url: "administracion.php?cmd=chekingInfoProvision",
	    data: envio,
	    success: function(html){
               var response = jQuery.trim(html);
               $('#box_loading').css('visibility','hidden');
	        if (response=='Gestion_Actuaciones') // Se encontro en Gestion Actuaciones
	        {
                    show_alert('Sistema Web Unificada','El registro con el numero de pedido ingresado ya existe en la Tabla Gestion Actuaciones.','Provision existe');
	            $('#txt_numero_pedido').focus();
                    $("#div_res").html("");
	            return false;
	        }
	        else
	        {
                    if(response=='tmp_gaudi'){
                    show_alert('Sistema Web Unificada','El registro con el numero de pedido ingresado ya existe en la Tabla Tmp Gaudi.','Provision existe');
	            $('#txt_numero_pedido').focus();
                    $("#div_res").html("");
	            return false;
                    }else{
                        if(response=='no_exist'){
                            show_alert('Sistema Web Unificada','El registro con el numero de pedido no existe en la Base de Datos.','Provision no existe');
                            $('#txt_numero_pedido').focus();
                            $("#div_res").html("");
                            return false;
                        }else{
                            $("#div_res").empty();
                            $("#div_res").html(response);
                            
                            $('#datepicker').datetimepicker({
                            dateFormat: 'yyyy/mm/dd HH:MM'
                    
                            });
                            $("#btn_registrar_provision").click(function(){
                                registrar_provision_manual();
                            });
                            $("#txt_telefono").focus();
                            
                        return true;
                        }
                    }
	             
	        }
 		}
	}); 
    }

}
function show_alert(title,message,titulo_message){
        $('#dialog-modal').attr('title','<span style="font-size:11px;">'+title+'</span>');
        $('#dialog-modal').html('<span style="font-size:11px;"><b style="color:#CC0800;">'+titulo_message+':</b><br /><br />'+message);
        $( "#dialog-modal" ).dialog({modal: true});
}

function change_empresas(obj){
    var id_empresa=$('#'+obj.id).attr('value');
    if(id_empresa==0){
        alert("Seleccione una empresa.");
        return;
    }else{
        $.ajax({
	    type: "POST",
	    url: "administracion.php?cmd=selectZonalesByEmpresa_ProvisionManualEnter",
	    data: "idempresa="+id_empresa,
	    success: function(html) {
               var response = jQuery.trim(html);
               $('#cmb_zonales').empty();
               $('#cmb_zonales').attr('disabled',false);
               $('#cmb_zonales').append(response);
            }
        });
    }
}

function change_zonales(obj){
    var zonal=$('#'+obj.id).attr('value');
    var id_empresa=$('#cmb_empresas').attr('value');
    if(id_empresa==0){
        alert("Seleccione una Empresa");
        return;
    }else{
        if(zonal==0){
        alert("Seleccione una Zonal.");
        return;
    }else{
        $.ajax({
	    type: "POST",
	    url: "administracion.php?cmd=selectMDFNODOByEmpresa_ZonalProvisionManualEnter",
	    data: "idempresa="+id_empresa+"&zonal="+zonal,
	    success: function(html) {
               var response = jQuery.trim(html);
               $('#cmb_mdf_nodo').empty();
               $('#cmb_mdf_nodo').attr('disabled',false);
               $('#cmb_mdf_nodo').append(response);
            }
        });
    }
    }
    
}

function registrar_provision_manual(){
    var cliente=$.trim($('#txt_nombre').val());
    var inscripcion=$.trim($('#txt_inscripcion').val());
    var negocio=$.trim($('#cmb_negocios').val());
    var telefono=$.trim($('#txt_telefono').val());
    var zonal=$('#cmb_zonales').val();
    var empresa=$('#cmb_empresas').val();
    var mdf_nodo=$('#cmb_mdf_nodo').val();
    var segmentos=$('#cmb_segmentos').val();
    var fechareg=$.trim($('#datepicker').val());
    var numero_pedido=$.trim($('#numero_pedido').val());
    var abc_masivo=$.trim($('#cmb_abc_masivo').val());
    var ot=$.trim($('#txt_ot').val());
    var distrito=$.trim($('#txt_distrito').val());
    var origen=$.trim($('#origen').val());
    var comentario=$.trim($('#txt_comentario').val());
    var direccion=$.trim($('#txt_direccion').val());
    
    if(telefono.length<1){
        show_alert('Sistema Web Unificada','Ingrese el n&uacute;mero de Tel&eacute;fono del Cliente.','Por favor');
        $('#txt_telefono').focus();
    }else{
        if(telefono.length>0 &&(!/^([0-9])*[.]?[0-9]*$/.test(telefono))){
            show_alert('Sistema Web Unificada','El n&uacute;mero de Tel&eacute;fono debe ser un dato num&eacute;rico.','Error');
            $('#txt_telefono').focus();
        }else{
            if(cliente.length<1){
                show_alert('Sistema Web Unificada','Ingrese el nombre completo del Cliente.','Por favor');
                $('#txt_nombre').focus();
            }else{
                if(inscripcion.length<1){
                    show_alert('Sistema Web Unificada','Ingrese el c&oacute;digo de inscripcion del Cliente.','Por favor');
                    $('#txt_inscripcion').focus();
                }else{
                    if(inscripcion.length>0 &&(!/^([0-9])*[.]?[0-9]*$/.test(inscripcion))){
                        show_alert('Sistema Web Unificada','El n&uacute;mero de Inscripci&oacute;n debe ser un dato num&eacute;rico.','Error');
                        $('#txt_inscripcion').focus();
                    }else{
                        if(negocio==0){
                            show_alert('Sistema Web Unificada','Seleccione un Negocio.','Por favor');
                            $('#cmb_negocios').focus();
                        }else{
                                if(empresa==0){
                                        show_alert('Sistema Web Unificada','Seleccione una Empresa Contratista.','Por favor');
                                        $('#cmb_empresas').focus();
                                    }else{
                                        if(zonal==0){
                                            show_alert('Sistema Web Unificada','Seleccione una Zonal.','Por favor');
                                            $('#cmb_zonales').focus();
                                            }else{
                                        if(mdf_nodo==0){
                                            show_alert('Sistema Web Unificada','Seleccione un MDF o Nodo.','Por favor');
                                            $('#cmb_mdf_nodo').focus();
                                        }else{
                                            if(segmentos==0){
                                                show_alert('Sistema Web Unificada','Seleccione un Segmento.','Por favor');
                                                $('#cmb_segmentos').focus();
                                            }else{
                                                if(fechareg.length<10){
                                                    show_alert('Sistema Web Unificada','Seleccione una Fecha.','Por favor');
                                                    $('#datepicker').focus();
                                                }else{
                                                    if(abc_masivo==0){
                                                        show_alert('Sistema Web Unificada','Seleccione una opcion de ABC o Masivo.','Por favor');
                                                        $('#cmb_abc_masivo').focus();
                                                    }else{
                                                        if(ot.length<1){
                                                            show_alert('Sistema Web Unificada','Ingrese la orden de trabajo.','Por favor');
                                                            $('#txt_ot').focus();
                                                        }else{
                                                            if(distrito.length<1){
                                                                show_alert('Sistema Web Unificada','Ingrese el nombre del distrito.','Por favor');
                                                                $('#txt_distrito').focus();
                                                            }else{
                                                                if(direccion.length<1){
                                                                    show_alert('Sistema Web Unificada','Ingrese la direccion del Cliente.','Por favor');
                                                                    $('#txt_direccion').focus();
                                                                }else{

                                                            var data_provision={}
                                                            data_provision['numero_pedido']=numero_pedido;
                                                            data_provision['telefono_cliente']=telefono;
                                                            data_provision['inscripcion_cliente']=inscripcion;
                                                            data_provision['nombre_cliente']=cliente;
                                                            data_provision['direccion_cliente']=direccion;
                                                            data_provision['negocio']=negocio;
                                                            data_provision['zonal']=zonal;
                                                            data_provision['empresa_eecc']=empresa;
                                                            data_provision['mdf_nodo']=mdf_nodo;
                                                            data_provision['codigo_segmento']=segmentos;
                                                            data_provision['fecha_registro']=fechareg;
                                                            data_provision['abc_masivo']=abc_masivo;
                                                            data_provision['order_trabajo']=ot;
                                                            data_provision['descripcion_distrito']=distrito;
                                                            data_provision['fuente_origen']=origen;
                                                            data_provision['comentario']=comentario;
   
                                                                $.post("administracion.php?cmd=SavingProvisionManual",{"process":"proccess_saving_provision","data":data_provision},function(html){
                                                                    var response = $.trim(html);
                                                                    //alert(response);
                                                                    if (response=="1")
                                                                    {
                                                                        show_alert('Sistema Web Unificada','Se registro correctamente el registro de Provisi&oacute;n.','Proceso Completo');
                                                                        setTimeout('window.location="administracion.php?cmd=InsertManualProvision"',2000);
                                                                    }
                                                                    else if (response=="2")
                                                                    {
                                                                        show_alert('Sistema Web Unificada','La Provision ya a sido registrada en Gaudi.','Provisi&oacute; ya existe');
                                                                        setTimeout('window.location="administracion.php?cmd=InsertManualProvision"',2000);
                                                                    }else{
                                                                        show_alert('Sistema Web Unificada','No se pudo registrar la Provisi&oacute;n, contactese con el Administrador del Sistema.','Ocurri&oacute; un Error');
                                                                        return;
                                                                    }
                                                                    
                                                                });
                                                                    
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            
                        }
                    }
                }
            }
        }
    }
}