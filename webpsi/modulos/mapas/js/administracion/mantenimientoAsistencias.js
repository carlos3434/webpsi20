var AsistenciaTecnico = function(){};
AsistenciaTecnico.prototype.init = function(){};
AsistenciaTecnico.prototype.activaGrid = function (){
    var _self = this;
    var elements = $('#paginadoResultados tr.menuDerecho td');
    /*
    $.each(elements,function(){
		var itemActual = $(this);
		$(this).menuDerecho({
			items : [	
						{
							text : 'Seleccionar / No seleccionar',
							accion : function(){
								
							},
							icono :'img/accept.png'
						},
						{
							text : 'Seleccionar / No seleccionar',
							accion : function(){
								
							},
							icono :'img/accept.png'
						}
			]
		});
	});
	*/
    _self.preparaData();
	
	
	
	
	
    $('a.ver_mas').click(function(){
	
        $('#__window__tardanza__').hide();
        //$(this).html("[-]");
		
        var idtecnico = $(this).attr("id_tecnico");
		
        var string_date = $('#date').val().split("_");	
        var fecha_inicio = string_date[0].split("-");
        var mes = fecha_inicio[1];
        var anio = fecha_inicio[0];
		
		
        $.ajax({
            type: "POST",
            dataType: "html",
            cache:false,
            //url: "modulos/gesrec/bandeja.asistencia.php?cmd=verCalendario",
            url: "modulos/gesrec/bandeja.asistencia.php?cmd=verCalendarioTecnico",
            data: {
                //tecnico : idtecnico
                tecnico : idtecnico,
                mes : mes,
                anio : anio
            },
            success: function(data) {
                //$('#detalle_'+idtecnico).html("<td colspan='12'>"+data+"</td>").show();
				
                $('body').append("<div id='modalJqueryTDPReport' align='left' title='Reportes de asistencia' style='display:none'></div>");
	
                $('#modalJqueryTDPReport').html("<div id='_resultados_reportes_'>"+data+"</div>")
                .dialog({
                    width : 900,
                    height : 500,
                    modal : true,
                    hide : 'slide',
                    close :function(){
                        $(this).remove();
                    }
                }).show();
				
            },
            error : function(){
                alert("<p style='color:red'>Parece que el servidor esta tardando en responder, por favor int&eacute;ntelo en unos minutos</p>");
                controlesOn();			
            }
        });

		
    }		//,function(){
    //$('#__window__tardanza__').hide();
    //$(this).html("[+]");
    //$('#detalle_'+$(this).attr("id_tecnico")).hide();
    //}
    );
	
    $('tr.menuDerecho').tips({
        selector : 'a.ver_mas , a.asistente,a.tardanza,a.falta,span.asistente,span.tardanza,img.cumple,a.editarTecnico,a.vacaciones_ver,a.vacaciones',
        tipClass : 'classCoordDetalles'
    });
	
	
    /*
	$('tr.menuDerecho')
    .hover(
		function(){
			$(this).find("td").addClass("seleccionado");
	    },
        function(){
			$(this).find("td").removeClass("seleccionado");
	    }
	);
	*/
	

    for(var i=1;i<=7;i++){
        var dia_mes_anio = $('#paginadoResultados tr:first').find('td:eq('+(i+5)+')').attr("fecha");		
        var dia = dia_mes_anio.split("-");
        $('#span_dia'+i).html(dia[2]);
    };
	
	
	
    $('a.vacaciones').click(function(){
        var idtecnico = $(this).attr("id_tecnico");
        var fecha = $(this).attr("fecha");
        $('body').append("<div id='modalJqueryTDPVacaciones' align='left' title='Reportes de asistencia'></div>");	
        $('#modalJqueryTDPVacaciones').dialog({
            width : 400,
            height : 360,
            modal : true,
            hide : 'slide',
            resizable:false,
            close :function(){
                $(this).remove();
            }
        });
		
        $.ajax({
            type: "POST",
            dataType: "html",
            cache:false,
            url: "modulos/gesrec/bandeja.asistencia.php?cmd=asignarVacaciones",
            data: {
                tecnico : idtecnico,
                fecha : fecha
            },
            success: function(data) {
                $('#modalJqueryTDPVacaciones').html(data);
            },
            error : function(){
                alert("<p style='color:red'>Parece que el servidor esta tardando en responder, por favor int&eacute;ntelo en unos minutos</p>");
                controlesOn();			
            }
        });
		
    });
	
    $('a.vacaciones_ver').click(function(){
        var idtecnico = $(this).attr("id_tecnico");
        var fecha = $(this).attr("fecha");
		
        $('body').append("<div id='modalJqueryTDPVacaciones' align='left' title='Reportes de asistencia'></div>");	
        $('#modalJqueryTDPVacaciones').dialog({
            width : 450,
            height : 330,
            modal : true,
            hide : 'slide',
            resizable:false,
            close :function(){
                $(this).remove();
            }
        });
		
        $.ajax({
            type: "POST",
            dataType: "html",
            cache:false,
            url: "modulos/gesrec/bandeja.asistencia.php?cmd=verVacaciones",
            data: {
                tecnico : idtecnico,
                fecha : fecha
            },
            success: function(data) {
                $('#modalJqueryTDPVacaciones').html(data);
            },
            error : function(){
                alert("<p style='color:red'>Parece que el servidor esta tardando en responder, por favor int&eacute;ntelo en unos minutos</p>");
                controlesOn();			
            }
        });
		
    });
	
	
	
	
    $('a.editarTecnico').click(function(){
	
        var idtecnico = $(this).attr("id_tecnico");
		
        $('body').append("<div id='modalJqueryTDPEditarTecnico' align='left' title='Edici&oacute;n de t&eacute;cnicos'></div>");	
		
        $('#modalJqueryTDPEditarTecnico').dialog({
            width : 730,
            height : 330,
            modal : true,
            hide : 'slide',
            resizable:false,
            close :function(){
                $(this).remove();
            }
        });
		
        $.ajax({
            type: "POST",
            dataType: "html",
            cache:false,
            url: "modulos/gesrec/bandeja.asistencia.php?cmd=mostrarEdicionTecnico",
            data: {
                tecnico : idtecnico
            },
            success: function(data) {
                $('#modalJqueryTDPEditarTecnico').html(data);
            },
            error : function(){
                alert("<p style='color:red'>Parece que el servidor esta tardando en responder, por favor int&eacute;ntelo en unos minutos</p>");
                controlesOn();			
            }
        });
		
		
    });
	
};

AsistenciaTecnico.prototype.sendData = function (fecha_inicio,fecha_fin,tecnico,celula,cip,empresa,desde){

    loader("start");

    $.ajax({
        type: "POST",
        dataType: "html",
        cache:false,
        url: "modulos/gesrec/bandeja.asistencia.php?cmd=mostrarResultados",
        data: {
            empresa : empresa,
            fecha_inicio:fecha_inicio,
            fecha_fin:fecha_fin,
            tecnico : tecnico,
            celula : celula,
            cip : cip
        },
        success: function(data) {
            if($('#__window__tardanza__')){
                $('#__window__tardanza__').remove();
            };
            controlesOn();
            loader("end");
            $('#grid_resultados').html(data);
			
            if(desde=="link"){
                $('#empresa').val(empresa);
				
                var primer_valor = $("#celula option:first");
				
                var cad = "<option value=''>" + primer_valor.html() + "</option>";
					
                $.post("modulos/gesrec/bandeja.asistencia.php?cmd=cargaCelulas",{
                    valor : $("#empresa").val()
                },function(data){
                    var totalArray = data.length - 1;
                    for(i = 0; i<=totalArray; i++){
                        cad +="<option id ='data_" + data[i].id + "' value='" + data[i].id + "'>" + data[i].nombre + "</option>";
                    };
                    $("#celula").html(cad).val(celula);
                },"json");
            };
			
			
            asistenciaTecnico.activaGrid();
        },
        error : function(){
            alert("<p style='color:red'>Parece que el servidor esta tardando en responder, por favor int&eacute;ntelo en unos minutos</p>");
            controlesOn();			
        }
    });
};

AsistenciaTecnico.prototype.preparaData = function(){
	
    
    var elements = $('#paginadoResultados li a.tardanza , #paginadoResultados li a.falta');
	
    var link_actual = '';
	
	
    $.each(elements,function(){
        $(this).click(function(){
		
            if(link_actual!=''){
                link_actual.removeClass("selected_estado_asistencia selected_estado_ausente selected_estado_tardanza");
            };
            if($(this).hasClass("asistente")){
                $(this).addClass("selected_estado_asistencia");
            };
            if($(this).hasClass("falta")){
                $(this).addClass("selected_estado_ausente");
            };
            if($(this).hasClass("tardanza")){
                $(this).addClass("selected_estado_tardanza");
            };
            link_actual = $(this);
        });
			
    });

    $('body').append("<div id='__window__tardanza__' class='ventana_tardanza' style='display:none' />");
    var fieldset = "<fieldset><legend>Marcando Asistencia</legend>"+
    "<form id='__form__tardanza'>"+
    "<div id='input_hora'>Hora de ingreso :"+
    "<select name='__hora__' id='__hora__'>";
    for(var i=1;i<=24;i++){
        if(i<12){
            fieldset += "<option value='"+i+"'>"+i+" am</option>";
        }else{
            if(i===12){
                fieldset += "<option value='"+i+"'>"+i+" pm</option>";
            }else if(i==24){
                fieldset += "<option value='"+i+"'>12 am</option>";
            }else{
                fieldset += "<option value='"+i+"'>"+(i-12)+" pm</option>";
            };
        };
    };
    fieldset  +=	"</select>"+
    "<select name='__minutos__' id='__minutos__'>"
    for(var i=0;i<=59;i++){
						
        if(i<10){
            fieldset += "<option value='"+i+"'>0"+i+"</option>";
        }else{
            fieldset += "<option value='"+i+"'>"+i+"</option>";
        };
    };
    fieldset  +=	+"</select>"+
    "<input type='hidden' name='__hora_entrada__' id='__hora_entrada__'/></div>"+
    "Observaciones:<textarea name='__observaciones__' id='__observaciones__'></textarea>"+
    "<div align='center'>"+
    "<label>"+
    "<input type='checkbox' name= 'permiso' id='_permiso_'/>"+
    "Solicit&oacute; permiso</label>"+
    "</div>"+
    "<div style='' id='__msg__horario__'>"+
    "<input type ='hidden' value ='' id='_id_link_' />"+
    "<input type ='hidden' value ='' id='_estado_original_' />"+
    "<input type ='submit' value ='Grabar' />"+
    "<input type ='button' value ='Cancelar' />"+
    "</div>"+
    "</form>"+
    "</fieldset>";
	
    $('#__window__tardanza__').append(fieldset);
	
    $('#__form__tardanza').attr("autocomplete","off");
	
    var elements = $('#paginadoResultados li a.tardanza , #paginadoResultados li a.falta');
	
	
    $('#__form__tardanza').submit(function(e){
	
        e.preventDefault();
		
        var link_actual_elegido = $('#_id_link_').val();
        var hora_creada = $('#__hora__').val()+":"+$('#__minutos__').val();
        var estado = $('#'+link_actual_elegido).attr("estado");
        var id_actual = $('#'+link_actual_elegido).attr("id_actual");
        var dia = $('#'+link_actual_elegido).attr("dia");
        var parent_link = $('#'+link_actual_elegido).parent().parent().parent().parent();
        var id_tecnico = parent_link.attr("id_tecnico");
        var fecha = parent_link.attr("fecha");
        var observaciones = $('#__observaciones__').val();
        var permiso = ($('#_permiso_').is(':checked'))?1:0;
		
        controlesOff();
		
        $.ajax({
            type: "POST",
            dataType: "json",
            cache:false,
            url: "modulos/gesrec/bandeja.asistencia.php?cmd=grabaHora",
            data: {
                id_tecnico : id_tecnico,
                hora :hora_creada,
                id_actual : id_actual,
                fecha : fecha,
                observaciones : observaciones,
                estado : estado,
                permiso : permiso
            },
            success: function(data) {
			
                controlesOn();
                $('#_estado_original_').val('');
                switch(estado){
					
                    case '1':
                        $('#a_'+dia+'_'+id_tecnico).removeClass("selected_estado_asistencia selected_estado_ausente selected_estado_tardanza")
                        .attr("id_actual",data.id);
                        $('#f_'+dia+'_'+id_tecnico).removeClass("selected_estado_asistencia selected_estado_ausente selected_estado_tardanza")
                        .attr("id_actual",data.id);
                        break;
					
                    case '2':
                        $('#a_'+dia+'_'+id_tecnico).removeClass("selected_estado_asistencia selected_estado_ausente selected_estado_tardanza")
                        .attr("id_actual",data.id);
                        $('#t_'+dia+'_'+id_tecnico).removeClass("selected_estado_asistencia selected_estado_ausente selected_estado_tardanza")
                        .attr("id_actual",data.id);
                        break;
                };
				
                link_actual = $('#a_'+dia+'_'+id_tecnico);
	
                $('#__msg__horario__').respuestaOK({
                    success: function(){
                        $('#__window__tardanza__').hide();
                        document.getElementById('__form__tardanza').reset();
                    }
                });		
                
                $('#envio_filtro').trigger("click");
                
            },
            error : function(){
                alert("<p style='color:red'>Parece que el servidor esta tardando en responder, por favor int&eacute;ntelo en unos minutos</p>");
                controlesOn();			
            }
        });
        
    });
	
    $('#__form__tardanza input[type="button"]').click(function(e){
	
        $('#__window__tardanza__').hide();	
		
        var element_actual = $(".clicked");
		
        switch(element_actual.attr("estado")){
            case '1':
                if(element_actual.attr("estado") != $('#_estado_original_').val()){
				
                    $('#'+element_actual.attr("id")).removeClass("selected_estado_tardanza");
                };
                break;
            case '2':
                if(element_actual.attr("estado") != $('#_estado_original_').val()){
                    $('#'+element_actual.attr("id")).removeClass("selected_estado_ausente");
                };
                break;
        };
		
        element_actual.removeClass("clicked");
    });
	
	
    var altoVentana = $('window').height();
    var anchoVentana = $('window').width();
    var anchoForm = $('#__window__tardanza__').width(); 
    var altoForm = $('#__window__tardanza__').height(); 
	
    $.each(elements,function(){
	
        $(this).click(function(e){
		
            var estado = $(this).attr("estado");
			
            if($(this).attr("estado")==2){
                $('#input_hora').hide();
            }else{
                $('#input_hora').show();
            };
			
            $('#__window__tardanza__')
            .css({
                left: (e.pageX + anchoForm > anchoVentana)?e.pageX - (anchoForm + 45):e.pageX + 10,
                top: (e.pageY + altoForm > altoVentana)?e.pageY - (altoForm + 40):e.pageY + 10
            });
			
			
            $('#__hora_entrada__').val($(this).attr("hora_entrada"));
            $('#_id_link_').val($(this).attr("id"));
			
            var parent_link = $(this).parent().parent().parent().parent();
            var id_tecnico = parent_link.attr("id_tecnico");		
            var fecha = parent_link.attr("fecha");
			
            if(estado !=0){
                $(this).addClass("clicked");
                $.ajax({
                    type: "POST",
                    dataType: "json",
                    cache:false,
                    url: "modulos/gesrec/bandeja.asistencia.php?cmd=consultarDataAsistencia",
                    data: {
                        id_tecnico : id_tecnico,
                        fecha : fecha
                    },
                    success: function(data) {
					
                        $('#_estado_original_').val(data.estado);
						
                        if(estado == data.estado){
                            $('#__observaciones__').val(data.observaciones);
                            $('#__hora__').val(data.hora);
                            $('#__minutos__').val(data.minutos);
							
                            if(data.permiso==1){
                                $('#_permiso_').attr("checked","checked");
                            }else{
                                $('#_permiso_').attr("checked","");
                            };
							
                        }else{
                            document.getElementById('__form__tardanza').reset();
                        };
                        $('#__window__tardanza__').show();
                    },
                    error : function(){
                        alert("<p style='color:red'>Parece que el servidor esta tardando en responder, por favor int&eacute;ntelo en unos minutos</p>");
                        controlesOn();			
                    }
                });
            }else{
                document.getElementById('__form__tardanza').reset();			
                $('#__window__tardanza__').show();
            };
	
        });
		
    });
	
    var dias_asistentes = $('a.asistente');
	
    $.each(dias_asistentes,function(){
		
        $(this).click(function(){
		
            $(this).addClass("selected_estado_asistencia");
            var _link = $(this);
            var estado = $(this).attr("estado");
            var id_actual = $(this).attr("id_actual");
            var dia = $(this).attr("dia");
            var parent_link = $(this).parent().parent().parent().parent();
            var id_tecnico = parent_link.attr("id_tecnico");		
            var fecha = parent_link.attr("fecha");
			
            $.ajax({
                type: "POST",
                dataType: "json",
                cache:false,
                url: "modulos/gesrec/bandeja.asistencia.php?cmd=grabaHora",
                data: {
                    id_tecnico : id_tecnico,
                    hora :'',
                    id_actual : id_actual,
                    fecha : fecha,
                    observaciones : '',
                    estado : estado
                },
                success: function(data) {
                    controlesOn();
                    _link.attr("id_actual",data.id);
                    $('#t_'+dia+'_'+id_tecnico).attr("id_actual",data.id)
                    .removeClass("selected_estado_asistencia selected_estado_ausente selected_estado_tardanza")
					
                    $('#f_'+dia+'_'+id_tecnico).attr("id_actual",data.id)
                    .removeClass("selected_estado_asistencia selected_estado_ausente selected_estado_tardanza")
					
                },
                error : function(){
                    alert("<p style='color:red'>Parece que el servidor esta tardando en responder, por favor int&eacute;ntelo en unos minutos</p>");
                    controlesOn();			
                }
            });
        });
    });
	
	
	
};

AsistenciaTecnico.prototype.saveHorario = function(){

    var hora_inicio = $("#rango_horarios").slider("values",0);
    var hora_fin = $("#rango_horarios").slider("values",1);

    $.ajax({
        type: "POST",
        dataType: "json",
        cache:false,
        url: "modulos/gesrec/bandeja.asistencia.php?cmd=grabaHorario",
        data: {
            hora_inicio : hora_inicio,
            hora_fin : hora_fin
        },
        success: function(data) {	
            $('#msghorario').respuestaOK({
                success: function (){
                    $('#modalJqueryTDP').dialog("close");
                    $('#envio_filtro').trigger("click");
                }
            });
        },
        error : function(){
            alert("<p style='color:red'>Parece que el servidor esta tardando en responder, por favor int&eacute;ntelo en unos minutos</p>");
            controlesOn();			
        }
    });

};

AsistenciaTecnico.prototype.loadReports = function(){

    $('body').append("<div id='modalJqueryTDPReport' align='left' title='Reportes de asistencia'></div>");
	
    $('#modalJqueryTDPReport').dialog({
        width : 900,
        height : 550,
        modal : true,
        close :function(){
            $(this).remove();
        }
    }).load("modulos/gesrec/bandeja.asistencia.php?cmd=cargarReportes");
	
};

AsistenciaTecnico.prototype.changeEscene = function(element){
    
    $("#_resultados_reportes_").hide()
    
    var $element = $("#_resultados_reportes_");
    $.ajax({
        type: "POST",
        dataType: "html",
        cache:false,
        url: "modulos/gesrec/bandeja.asistencia.php?cmd=verCalendarioTecnico",
        data: {
            tecnico : element.attr("id_tecnico"),
            mes : $('#mes_reporte').val(),
            anio : $('#anio_reporte').val()
        },
        success: function(data) {	
            $element.html(data).show();//;.fadeIn("fast");
        },
        error : function(){
            alert("<p style='color:red'>Parece que el servidor esta tardando en responder, por favor int&eacute;ntelo en unos minutos</p>");
            controlesOn();			
        }
    });

	
};



var asistenciaTecnico = new AsistenciaTecnico();
