/*
 * Telefonica del Peru.
 * Planificacion y Soluciones Informaticas STC
 * GMD S.A.
 * @autor: Gonzalo Chacaltana Buleje <gchacaltana@gmd.com.pe>
 * JS Util - Funciones para uso comun del Modulo de Administracion
 **/
function verificaCheck(group){
   var cantidad=0;
   var tl=group.length;

   if(tl>0){
   for (var x=0; x < group.length; x++) {
        if (group[x].checked) {
        cantidad++;
        }
   }
   }else{
       if(group.checked){cantidad++}
   }
    return cantidad;
}
function load_array(element){
    var grupo=new Array();
    $("input[name='"+element+"']:checked").each(function(){grupo.push($(this).val());});
    return grupo;
}
function validarNumero(e){
var tecla = (document.all) ? e.keyCode : e.which;
if(tecla==17 || e.ctrlKey){return false;}
else{
if (tecla==8) return true;
var patron =/\d/;//valida solo numeros
var te = String.fromCharCode(tecla);
return patron.test(te);
}
}
function validarTextoNumero(e){
var tecla = (document.all) ? e.keyCode : e.which;
if(tecla==17 || e.ctrlKey){return false;}
else{
if (tecla==8) return true;
var patron =/\w/;//valida solo letras y numeros
var te = String.fromCharCode(tecla);
return patron.test(te);
}
}
function clearDataDiv(div){
        $('#'+div).empty();
        $('#'+div).css('display','none');
}
function closeModal(){
    $("#childModal").dialog("close");
}
