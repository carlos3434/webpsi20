var titleBar = document.title;

var CertificacionesProcesos = function() {};

CertificacionesProcesos.prototype.init = function() {
	var _self = this;
	$.each($('a.options'), function() {// para los enlaces Speedy , B�sica ,Cable , Trio
		$(this).click(function() {
			$("#msg_producto").html('<img src="img/loading.gif" alt="" border="0" />').show();
			$('a.options').css({
				"color" : "#0086C3",
				"font-size" : 14
			});
			$(this).css({
				"color" : "red",
				"font-size" : 15
			});
			_selfLink = $(this);

			$.ajax({
				type : "POST",
				dataType : "html",
				cache : false,
				url : "modulos/certifica/bandeja.procesos.php?cmd=cargaInicio",
				data : {
					producto : _selfLink.attr("producto")
				},
				success : function(data) {
					$('#cont_resultado_main').html(data);
					$("#msg_producto").hide();
				},
				//timeout : 5000,
				error : function() {
					alert("<p style='color:red'>Parece que el servidor esta tardando en responder, por favor int&eacute;ntelo en unos minutos</p>");
					$("#msg_producto").hide();
				}
			});

		});
	});
};

CertificacionesProcesos.prototype.addFieldFile = function(elementBefore) {

	var elements = '';
	elements = '<tr>';
	elements += '<td class="tdLabel">';
	elements += 'Subir archivo :';
	elements += '</td>';
	elements += '<td>';
	elements += '<input type="file" size="50" name="file[]" />';
	elements += '<input type ="button" value="[X]" title="Click para Eliminar" class="deleteFieldFile" onclick="$(this).parent().parent().remove();" />';
	elements += '</td>';
	elements += '</tr>';
	$('#actionButtons').before(elements);
	//agregamos los elementos
	converButtons();
	//convertimos los botones
};

CertificacionesProcesos.prototype.uploadFile = function(url, cancel) {
	$('body').append("<div id='modalJqueryProgress' align='center'></div>");

	$('#modalJqueryProgress').createProgressBar({
		url : url,
		detalle : true,
		cancel : cancel
	});

};
cp = new CertificacionesProcesos();


$('#cont_resultado_main').load("modulos/certifica/vistas/main/bienvenida.procesos.php", function() {
	$('#cont_resultado_main').show();
});
var converButtons = function() {
	$("input[type='submit']").addClass("ui-button ui-widget ui-state-active ui-corner-all ui-button-text-only");
	$("input[type='button']").addClass("ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only");
	$("form").attr("autocomplete", "off")
};
var subidaOk = function() {

};
var resetUploadDocument = function() {

};
var errorSubida = function() {

};
var writeLog = function(msg, percent) {

	var element = $('#_detalleUpload_');
	element.html(msg);
	element.animate({
		scrollTop : element[0].scrollHeight
	});

	//element.css({
	//scrollTop : element.get(0).scrollHeight - element.height()
	//});
	var percent_actual = $('#progress').progressbar("value");
	$('#progress').progressbar("value", (percent_actual + percent));

	var total_percent = percent_actual + percent;

	if(total_percent <= 100) {
		document.title =   parseInt(percent_actual + percent) + "% procesando archivos..." + titleBar;
	}else{
		document.title = titleBar;
	};
	
};

var activarBotonProcesoAverias = function() {

	//alert('<div align="center">Certificaciones Procesadas</div>');
	setTimeout(function() {
		//		$('#btnSubmitAverias').attr("disabled","");
		//		$("#modalJqueryProgress").dialog("close");

		$('#listadoFiles ul').remove();
		$('#listadoFiles').html("<div style='padding:15px' align ='center'><h3>Proceso terminado.<br>No hay archivos a procesar</h3></div>");

		$('#formLoad input[type="submit"]').val("Archivos procesados").attr("disabled", "disabled").css({
			cursor : 'default'
		});

		$('#formLoad input[type="checkbox"] , #formLoad select').attr("disabled", "disabled").parent().css("cursor", "default");

	}, 1000);
};
var desactivarControles = activarBotonProcesoAverias;