/*
 * Proyecto Web Unificada TDP
 */
$(document).ready(function(){
    $(".trigger").mouseover(function(){
        $(".panel").toggle("fast");
        $(this).toggleClass("active");
        return false;
    });
    $('.panel').mouseleave(function(){
        $(".panel").toggle("fast");
        $('.trigger').toggleClass("active");
    });
    //Fin Menu



    change_state=function(id_bolsa,estado,nom_bolsa){

        if(confirm("Esta seguro de cambiar el estado de la Bolsa : "+nom_bolsa)){
            loader('start');

            var respuesta=0;
            var estado_inicial=estado;

            $.post("administracion.php?cmd=execUpdateStateBolsa", {
                id_bolsa: id_bolsa,
                state: estado,
                emisor:nom_bolsa
            }, function(data){
                loader('end');
                respuesta=data;

                if(respuesta==1){
                    if(estado_inicial==1){
                        var nvo_estado=0;
                        $('#alink_state_'+id_bolsa).attr('href','javascript:change_state(\''+id_bolsa+'\',\''+nvo_estado+'\',\''+nom_bolsa+'\')');
                        $('#img_state_'+id_bolsa).attr('src','img/estado_deshabilitado.png');
                        $('#img_state_'+id_bolsa).attr('alt','Cambiar estado a Habilitado');
                        $('#img_state_'+id_bolsa).attr('title','Cambiar estado a Habilitado');
                    }
                    else{
                        nvo_estado=1;
                        $('#alink_state_'+id_bolsa).attr('href','javascript:change_state(\''+id_bolsa+'\',\''+nvo_estado+'\',\''+nom_bolsa+'\')');
                        $('#img_state_'+id_bolsa).attr('src','img/estado_habilitado.png');
                        $('#img_state_'+id_bolsa).attr('alt','Cambiar estado a Deshabilitado');
                        $('#img_state_'+id_bolsa).attr('title','Cambiar estado a Deshabilitado');
                    }

                }
            });
        }
    }

    editBolsa=function(id_bolsa){

        var emisor=$('#emisor').attr('value');
        loader('start');
        $("#childModal").html('');
        $("#childModal").css("background-color","#FFFFFF");
        $.post("administracion.php?cmd=editBolsa", {
            id_bolsa: id_bolsa
        },
        function(data){
            loader('end');
            $("#childModal").html(data);
        });

        $("#childModal").dialog({
            modal:true,
            width:'500px',
            hide: 'slide',
            title: 'Editar Bolsa',
            position:'top'
        });

    }

    buscaBolsa=function(){
        $("#desc").val();
        loader('start');
        $.post("administracion.php?cmd=buscaBolsa", {
            valor:$("#desc").val()
        },function(data){
            loader('end');
            $("#tb_resultado").empty();
            $("#tb_resultado").append(data);

        });
        

    }

    $('#img_down_pdf').click(function(){
        alert("Muy pronto..");
    });

    $('#img_down_xls').click(function(){
        alert("Muy pronto..");
    });

});


