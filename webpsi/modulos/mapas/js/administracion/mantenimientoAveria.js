/* 
 * Sistema Web Unificada
 * Planificacion y Soluciones Informaticas - Telefonica
 * GMD S.A
 * Autor: Gonzalo Chacaltana Buleje <gchacaltana@gmd.com.pe>
 * Mantenimiento de Averia - Ingreso Manual
 */
$(document).ready(function(){
$("#txt_numero_averia").attr("value",'');
$("#txt_numero_averia").focus();
$('#btn_registrar').click(function(){
       registrar_averia_pendiente();
});
$(".trigger").mouseover(function(){
$(".panel").toggle("fast");
$(this).toggleClass("active");
return false;
});
$('.panel').mouseleave(function(){
$(".panel").toggle("fast");
$('.trigger').toggleClass("active");
});
//Fin Menu
$('#txt_numero_averia').keyup(function(e) { if(e.which == 13) { buscarNumero_Averia();}});
$('#btn_buscar').click(function(){
   buscarNumero_Averia();
});
                 
validarNumeroAveria =function(e){
//Valida no Crtl
var tecla = (document.all) ? e.keyCode : e.which;
if(tecla==17 || e.ctrlKey){return false;}
else{
if (tecla==8) return true;
var patron =/\w/;//valida solo letras y numeros
var te = String.fromCharCode(tecla);
return patron.test(te);
}
}
validarNumber=function(e){
tecla = (document.all) ? e.keyCode : e.which;
if(tecla==17 || e.ctrlKey){return false;}
else{
if (tecla==8) return true;
var patron =/\d/;//valida solo numeros
var te = String.fromCharCode(tecla);
return patron.test(te);
}
}

});
function buscarNumero_Averia()
{
    var numero_averia = $.trim($("#txt_numero_averia").val());
    if(numero_averia==''){
        show_alert('Sistema Web Unificada','Ingrese el n&uacute;mero de Pedido a buscar.','Por favor');
        $('#txt_numero_pedido').focus();
        
    }else{
       var envio = "numero_averia="+numero_averia;
    //alert(envio);
	$('#box_loading').css('visibility','visible');
 	$.ajax({
	    type: "POST",
	    url: "administracion.php?cmd=chekingInfoAveria",
	    data: envio,
	    success: function(html){
               var response = jQuery.trim(html);
               $('#box_loading').css('visibility','hidden');
                    if(response==2){
                        show_alert('Sistema Web Unificada','El registro con el numero de averia ingresado ya existe.','Aver&iacute;a existe');
                        $('#txt_numero_averia').focus();
                        $("#div_res").empty();
                        return false;
                    }else{
                        
                         $("#div_res").empty();
                         $("#div_res").html(response);
                         $('#datepicker').datetimepicker({
                            dateFormat: 'yyyy/mm/dd HH:MM'

                        });
                        $('#tb_btn').css('visibility','visible');
                        $('#txt_telefono').focus();
                         return true;
                        
                    }
	       
 		}
	}); 
    }

}
function show_alert(title,message,titulo_message){
        $('#dialog-modal').attr('title','<span style="font-size:11px;">'+title+'</span>');
        $('#dialog-modal').html('<span style="font-size:11px;"><b style="color:#CC0800;">'+titulo_message+':</b><br /><br />'+message);
        $( "#dialog-modal" ).dialog({modal: true});
}

function change_zonal(obj){
    var zonal=$('#'+obj.id).attr('value');
    if(zonal==0){
        alert("Seleccione una Zonal");
    }else{
       var pagina = "administracion.php?cmd=viewEmpresasByZonales_Averia";
       var envio = "zonal="+zonal;
        $.ajax({
	    type: "POST",
	    url: pagina,
	    data: envio,
	    success: function(html) {
       
	        $("#cmb_empresas").empty();
                $("#cmb_empresas").attr('disabled',false);
                $("#cmb_empresas").append(html);
                
 		}
	});
       
        $.ajax({
	    type: "POST",
	    url: "administracion.php?cmd=viewCiudadesByZonales_Averia",
	    data: "zonal="+zonal,
	    success: function(html) {
       
	        $("#cmb_ciudad").empty();
                $("#cmb_ciudad").attr('disabled',false);
                $("#cmb_ciudad").append(html);
                
 		}
	});
    }
}

function change_empresas(obj){
    var empresa=$('#'+obj.id).attr('value');
    var zonal=$('#cmb_zonales').attr('value');
    if(empresa==0){
        alert("Seleccione una Empresa");
    }else{
       var pagina = "administracion.php?cmd=viewMdfNodoByEmpresa";
       var envio = "idempresa="+empresa+"&zonal="+zonal;
        $.ajax({
	    type: "POST",
	    url: pagina,
	    data: envio,
	    success: function(html) {
       
	        $("#cmb_mdf_nodo").empty();
                $("#cmb_mdf_nodo").attr('disabled',false);
                $("#cmb_mdf_nodo").append(html);
                
 		}
	});
        
         $.ajax({
	    type: "POST",
	    url: "administracion.php?cmd=viewJefaturaGerenciaByEmpresaZonal_Averias",
	    data: "idempresa="+empresa+"&zonal="+zonal,
	    success: function(html) {
                var elementos=html.split('|');
	        $('#txt_gerencia').attr('value',elementos[0]);
                $('#txt_jefatura').attr('value',elementos[1]);
                
 		}
	});
        
    }
}

function change_negocios(obj){
    var negocio=$('#'+obj.id).attr('value');
    if(negocio==0){
        alert("Seleccione un Negocio");
    }else{
       var pagina = "administracion.php?cmd=viewClaseByNegocio_Averia";
       var envio = "negocio="+negocio;
        $.ajax({
	    type: "POST",
	    url: pagina,
	    data: envio,
	    success: function(html) { 
                $("#cmb_clase").empty();
                $("#cmb_clase").attr('disabled',false);
                $("#cmb_clase").append(html);
                if(negocio=='CATV'){
                    $('#txt_servicio').attr('disabled',false);
                    $('#txt_servicio').focus();
                }else{
                    $('#txt_servicio').empty();
                    $('#txt_servicio').attr('disabled',true);
                    
                }

 		}
	});
        
        $.ajax({
	    type: "POST",
	    url: "administracion.php?cmd=viewDescAveriaByNegocio_Averia",
	    data: "negocio="+negocio,
	    success: function(html) { 
                $("#cmb_des_averia").empty();
                $("#cmb_des_averia").attr('disabled',false);
                $("#cmb_des_averia").append(html);

 		}
	});
        
    }
}

function registrar_averia_pendiente(){
    var cliente=$.trim($('#txt_nombre').val());
    var inscripcion=$.trim($('#txt_inscripcion').val());
    var negocio=$.trim($('#cmb_negocios').val());
    var telefono=$.trim($('#txt_telefono').val());
    var zonal=$('#cmb_zonales').val();
    var empresa=$('#cmb_empresas').val();
    var area=$('#cmb_estados').val();
    var mdf_nodo=$('#cmb_mdf_nodo').val();
    var segmentos=$('#cmb_segmentos').val();
    var fechareg=$.trim($('#datepicker').val());
    var reiteradas=$.trim($('#txt_reiteradas').val());
    var averia=$.trim($('#averia').val());
    
    if(telefono.length<1){
        show_alert('Sistema Web Unificada','Ingrese el n&uacute;mero de Tel&eacute;fono del Cliente.','Por favor');
        $('#txt_telefono').focus();
    }else{
        if(telefono.length>0 &&(!/^([0-9])*[.]?[0-9]*$/.test(telefono))){
            show_alert('Sistema Web Unificada','El n&uacute;mero de Tel&eacute;fono debe ser un dato num&eacute;rico.','Error');
            $('#txt_telefono').focus();
        }else{
            if(cliente.length<1){
                show_alert('Sistema Web Unificada','Ingrese el nombre completo del Cliente.','Por favor');
                $('#txt_nombre').focus();
            }else{
                if(inscripcion.length<1){
                    show_alert('Sistema Web Unificada','Ingrese el c&oacute;digo de inscripcion del Cliente.','Por favor');
                    $('#txt_inscripcion').focus();
                }else{
                    if(inscripcion.length>0 &&(!/^([0-9])*[.]?[0-9]*$/.test(inscripcion))){
                        show_alert('Sistema Web Unificada','El n&uacute;mero de Inscripci&oacute;n debe ser un dato num&eacute;rico.','Error');
                        $('#txt_inscripcion').focus();
                    }else{
                        if(negocio==0){
                            show_alert('Sistema Web Unificada','Seleccione un Negocio.','Por favor');
                            $('#cmb_negocios').focus();
                        }else{
                            if(area==0){
                                show_alert('Sistema Web Unificada','Seleccione un Area.','Por favor');
                                $('#cmb_areas').focus();
                            }else{
                                if(zonal==0){
                                show_alert('Sistema Web Unificada','Seleccione una Zonal.','Por favor');
                                $('#cmb_zonales').focus();
                                }else{
                                    if(empresa==0){
                                        show_alert('Sistema Web Unificada','Seleccione una Empresa Contratista.','Por favor');
                                        $('#cmb_empresas').focus();
                                    }else{
                                        if(mdf_nodo==0){
                                            show_alert('Sistema Web Unificada','Seleccione un MDF o Nodo.','Por favor');
                                            $('#cmb_mdf_nodo').focus();
                                        }else{
                                            if(segmentos==0){
                                                show_alert('Sistema Web Unificada','Seleccione un Segmento.','Por favor');
                                                $('#cmb_segmentos').focus();
                                            }else{
                                                if(fechareg.length<10){
                                                    show_alert('Sistema Web Unificada','Seleccione una Fecha.','Por favor');
                                                    $('#datepicker').focus();
                                                }else{
                                                    if(reiteradas.length<1){
                                                        show_alert('Sistema Web Unificada','Ingrese el numero de reiteradas.','Por favor');
                                                        $('#txt_reiteradas').focus();
                                                    }else{
                                                        if(reiteradas.length>0 &&(!/^([0-9])*[.]?[0-9]*$/.test(reiteradas))){
                                                            show_alert('Sistema Web Unificada','El n&uacute;mero de Reiteradas debe ser un dato num&eacute;rico.','Error');
                                                            $('#txt_reiteradas').focus();
                                                        }else{
                                                            var descripcion_empresa=$('#cmb_empresas option:selected').text();
                                                            var descripcion_segmento=$('#cmb_segmentos option:selected').text();
                                                            var servicio=$('#txt_servicio').val();
                                                            var clase=$('#cmb_clase').val();
                                                            var cod_ciudad=$('#cmb_ciudad').val();
                                                            var id_des_averia=$('#cmb_des_averia').val();
                                                            var descripcion_averia=$('#cmb_des_averia option:selected').text();
                                                            var gerencia=$('#txt_gerencia').val();
                                                            var jefatura=$('#txt_jefatura').val();
                                                            var direccion=$('#txt_direccion').val();
                                                            var bolsa=$('#txt_bolsa').val();
                                                            var subbolsa=$('#txt_subbolsa').val();
                                                            
                                                            var data_averia_pendiente={}
                                                            data_averia_pendiente['process']='saving_averia';
                                                            data_averia_pendiente['numero_averia']=averia;
                                                            data_averia_pendiente['telefono_cliente']=telefono;
                                                            data_averia_pendiente['inscripcion_cliente']=inscripcion;
                                                            data_averia_pendiente['nombre_cliente']=cliente;
                                                            data_averia_pendiente['direccion_cliente']=direccion;
                                                            data_averia_pendiente['negocio']=negocio;
                                                            data_averia_pendiente['servicio']=servicio;
                                                            data_averia_pendiente['clase']=clase;
                                                            data_averia_pendiente['area']=area;
                                                            data_averia_pendiente['zonal']=zonal;
                                                            data_averia_pendiente['empresa_eecc']=empresa;
                                                            data_averia_pendiente['desc_empresa_eecc']=descripcion_empresa;
                                                            data_averia_pendiente['mdf_nodo']=mdf_nodo;
                                                            data_averia_pendiente['codigo_ciudad']=cod_ciudad;
                                                            data_averia_pendiente['codigo_segmento']=segmentos;
                                                            data_averia_pendiente['descripcion_segmento']=descripcion_segmento;
                                                            data_averia_pendiente['fecha_registro']=fechareg;
                                                            data_averia_pendiente['id_descripcion_averia']=id_des_averia;
                                                            data_averia_pendiente['descripcion_averia']=descripcion_averia;
                                                            data_averia_pendiente['numero_reiteradas']=reiteradas;
                                                            data_averia_pendiente['gerencia']=gerencia;
                                                            data_averia_pendiente['jefatura']=jefatura;
                                                            data_averia_pendiente['bolsa']=bolsa;
                                                            data_averia_pendiente['subbolsa']=subbolsa;
                                                            data_averia_pendiente['estado_gaudi']=area;
                                                                $.post("administracion.php?cmd=SaveManualAveriaPendiente",{"process":"proccess_saving","data":data_averia_pendiente},function(html){
                                                                    var response = jQuery.trim(html);
                                                                    var resp = response.split("|");
                                                                    //alert(resp[0]);
                                                                    if (resp[0]=="0")
                                                                    {
                                                                        
                                                                        show_alert('Sistema Web Unificada','No se pudo registrar la Aver&iacute;a, contactese con el Administrador del Sistema.','Ocurri&oacute; un Error');
                                                                        return;
                                                                    }
                                                                    else if (resp[0]=="1")
                                                                    {
                                                                        show_alert('Sistema Web Unificada','Se registro correctamente la Aver&iacute;a Pendiente.','Proceso Completo');
                                                                        setTimeout('window.location="administracion.php?cmd=InsertManualAveria"',2000);
                                                                    }
                                                                    else if (resp[0]=="2")
                                                                    {
                                                                        show_alert('Sistema Web Unificada','La Aver&iacute;a Pendiente ya a sido registrada.','Averia ya existe');
                                                                        setTimeout('window.location="ingreso_averias.php"',2000);
                                                                    }
                                                                        
                                                                });
                                                            
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}