/* 
 * Sistema Web Unificada
 * Planificacion y Soluciones Informaticas STC Telefonica
 * @autor: Gonzalo Chacaltana Buleje <gchacaltana@gmd.com.pe>
 * TDP Optimizacion de Procesos
 * GMD S.A
 * Fecha de Actualizacion : 21/08/2011
 */
$(document).ready(function(){
    //Menu
   $(".trigger").mouseover(function(){
		$(".panel").toggle("fast");
		$(this).toggleClass("active");
		return false;
    });
    $('.panel').mouseleave(function(){
       $(".panel").toggle("fast");
       $('.trigger').toggleClass("active");
    });
    //Fin Menu

    $('#empresa').change(function(){
       var idtipopedido=$('#tipo').val();
       var idempresa=$('#empresa').val();
       var emisor=$('#emisor').val();
       if(idtipopedido==0){alert("Seleccione un Tipo de Pedido.");}else{
       $('#box_zonal').empty();
       $('#box_zonal').html('Cargando.<img src="img/loading.gif" alt="" />');
       
       $.post("administracion.php?cmd=loadZonalesxEmpresa", {
        idempresa:idempresa,
        emisor:emisor
    },
    function(data){
        $('#box_zonal').empty();
        $('#box_zonal').html(data);
            $('#zonal').change(function(){
                var zonal=$('#zonal').val();
                $('.img_horario').css('display','none');
                $('#box_agendamiento').html('Cargando Agenda.<img src="img/loading.gif" alt="" />');
                $.post("administracion.php?cmd=loadAgendaTurnos", {
                idempresa:idempresa,
                emisor:emisor,
                idtipopedido:idtipopedido,
                zonal:zonal
                },
                function(data){
                    $('#box_agendamiento').empty();
                    //$('#tr_buttons').css('visibility','visible');
                    $('#box_agendamiento').append(data);
                    //Para actualizar
                            $('.textlink').click(function(){
                            var turno=$(this).attr('rel');
                            $(this).css('display','none');
                            $('#op_change_'+turno).css('visibility','visible');
                            $('#box_turno_'+turno).css('display','inline');
                            $('#capacidad_'+turno).attr('value');
                            $('#capacidad_'+turno).focus();
                            });
                            
                            $('.textlink_eecc').click(function(){
                            var turno=$(this).attr('rel');
                            $(this).css('display','none');
                            $('#op_change_eecc_'+turno).css('visibility','visible');
                            $('#box_turno_eecc_'+turno).css('display','inline');
                            $('#capacidad_eecc_'+turno).attr('value');
                            $('#capacidad_eecc_'+turno).focus();
                            });
                            
                            

                            $('.ipt_hor_up').blur(function(){
                            var valor=$(this).attr('value');
                            if(valor==''){
                            $(this).attr('value','0');
                            }
                            sumarCupos();
                            });
                            $('.ipt_hor_up_eecc').blur(function(){
                                var valor=$(this).attr('value');
                                if(valor==''){
                                $(this).attr('value','0');
                                }
                                sumarCupos_eecc();
                            });

                           $(document).keyup(function(e) {
                            if (e.keyCode == 27) {
                                $('.box_op_img').css('visibility','hidden');
                                $('.box_content_valor').css('display','none');
                                $('.textlink').css('display','inline');
                                $('.textlink_eecc').css('display','inline');
                            }
                            });

                            $('.img_op_grabar').click(function(){
                             var turno=$(this).attr('name');
                             var nvo_valor=$('#capacidad_'+turno).attr('value');
                             if(nvo_valor<=150 && nvo_valor>=0){
                             var ant_valor=$('#ant_valor_'+turno).val();
                             var emisor=$('#emisor').attr('value');
                             var id_agenda=$('#id_agenda_'+turno).attr('value');
                             var fecha_dia=$('#fecha_dia_'+turno).attr('value');
                             var valor_eecc=$('#capacidad_eecc_'+turno).attr('value');
                             //alert(fecha_dia);
                             var respuesta;
                                $.post("administracion.php?cmd=updateCuposAgendaXturno", {
                                id_agenda:id_agenda,
                                emisor:emisor,
                                nvocapacidad:nvo_valor,
                                antcapacidad:ant_valor,
                                fecha_dia:fecha_dia
                                },
                                function(data){
                                   respuesta=data;
                                   //$('#box_mensaje_ok').css('display','block');
                                   //$('#box_mensaje_ok').html(respuesta);
                                   
                                   if(respuesta==1){
                                        $('#box_mensaje_ok').css({
                                           'border':'1px solid #CD0A0A',
                                           'background':'#FEF2EE',
                                           'color':'#CD0A0A'
                                        });
                                        $('#box_mensaje_ok').css('display','block');
                                        $('#box_mensaje_ok').html('Imposible actualizar capacidad de cupos.<img src="img/aviso_small.png" alt="" /><br />Los cupos agendados es igual al valor ingresado.');
                                        $('#capacidad_'+turno).focus();
                                        setTimeout("clearMsg()",2000);
                                    }else{
                                        if(respuesta==2){
                                            $('#box_mensaje_ok').css({
                                           'border':'1px solid #CD0A0A',
                                           'background':'#FEF2EE',
                                           'color':'#CD0A0A'
                                        });
                                        $('#box_mensaje_ok').css('display','block');
                                        $('#box_mensaje_ok').html('Imposible actualizar capacidad de cupos.<img src="img/aviso_small.png" alt="" /><br />Los cupos agendados es menor al valor ingresado.');
                                        $('#capacidad_'+turno).focus();
                                        setTimeout("clearMsg()",2000);
                                        
                                        }else{
                                            if(respuesta==3){
                                                if(valor_eecc>nvo_valor){
                                                    var nuevo_valor_eecc=parseInt(nvo_valor/2);
                                                    $.post("administracion.php?cmd=updateCuposAgendaXturno_eecc", {
                                                        id_agenda:id_agenda,
                                                        emisor:emisor,
                                                        nvocapacidad:nuevo_valor_eecc,
                                                        antcapacidad:valor_eecc,
                                                        fecha_dia:fecha_dia
                                                    },function(data){
                                                        $('#box_mensaje_ok').css({
                                                            'border':'1px solid #0292E9',
                                                            'background':'#E8F2F6',
                                                            'height':'30px',
                                                            'color':'#048FE4'
                                                        });
                                                        $('#box_mensaje_ok').css('display','block');
                                                        $('#box_turno_'+turno).css('display','none');
                                                        $('#a_link_'+turno).css('display','inline');
                                                        $('#span_'+turno).html(nvo_valor);
                                                        $('#span_eecc_'+turno).html(nuevo_valor_eecc);
                                                        $('#op_change_'+turno).css('visibility','hidden');
                                                        $('#box_mensaje_ok').html('Cupos Actualizados.<img src="img/accept.png" alt="" />');
                                                        setTimeout("clearMsg()",2000);
                                                        
                                                    });
                                                }else{
                                                    $('#box_mensaje_ok').css({
                                                        'border':'1px solid #0292E9',
                                                        'background':'#E8F2F6',
                                                        'height':'30px',
                                                        'color':'#048FE4'
                                                    });
                                                    $('#box_mensaje_ok').css('display','block');
                                                    $('#box_turno_'+turno).css('display','none');
                                                    $('#a_link_'+turno).css('display','inline');
                                                    $('#span_'+turno).html(nvo_valor);
                                                    $('#op_change_'+turno).css('visibility','hidden');
                                                    $('#box_mensaje_ok').html('Cupos Guardados.<img src="img/accept.png" alt="" />');
                                                    setTimeout("clearMsg()",2000);
                                                }
                                                
                                            }
                                        }
                                    }
                                    

                                });

                                clearMsg=function(){
                                    $('#box_mensaje_ok').css('display','none');
                                }
                            }else{
                                alert("La cantidad debe estar en el rango de 50 a 150 cupos.");
                                $('#capacidad_'+turno).focus();
                            }
                             
                            });
                            
                            $('.img_op_grabar_eecc').click(function(){
                              
                             var turno_eecc=$(this).attr('name');
                             var valor_tl_turno=$('#capacidad_'+turno_eecc).attr('value');
                             var nvo_valor_eecc=$('#capacidad_eecc_'+turno_eecc).attr('value');
                             
                             if(nvo_valor_eecc<=valor_tl_turno){
                             
                             var ant_valor_eecc=$('#ant_valor_eecc_'+turno_eecc).val();
                             var emisor=$('#emisor').attr('value');
                             var id_agenda=$('#id_agenda_'+turno_eecc).attr('value');
                             var fecha_dia=$('#fecha_dia_'+turno_eecc).attr('value');
                             
                             var respuesta;
                                $.post("administracion.php?cmd=updateCuposAgendaXturno_eecc", {
                                id_agenda:id_agenda,
                                emisor:emisor,
                                nvocapacidad:nvo_valor_eecc,
                                antcapacidad:ant_valor_eecc,
                                fecha_dia:fecha_dia
                                },
                                function(data){
                                   respuesta=data;
                                   
                                   if(respuesta==1){
                                        $('#box_mensaje_ok').css({
                                           'border':'1px solid #CD0A0A',
                                           'background':'#FEF2EE',
                                           'color':'#CD0A0A'
                                        });
                                        $('#box_mensaje_ok').css('display','block');
                                        $('#box_mensaje_ok').html('Imposible actualizar capacidad de cupos.<img src="img/aviso_small.png" alt="" /><br />Los cupos agendados es igual al valor ingresado.');
                                        $('#capacidad_eecc_'+turno_eecc).focus();
                                        setTimeout("clearMsg()",2200);
                                    }else{
                                        if(respuesta==2){
                                            $('#box_mensaje_ok').css({
                                           'border':'1px solid #CD0A0A',
                                           'background':'#FEF2EE',
                                           'color':'#CD0A0A'
                                        });
                                        $('#box_mensaje_ok').css('display','block');
                                        $('#box_mensaje_ok').html('Imposible actualizar capacidad de cupos.<img src="img/aviso_small.png" alt="" /><br />Los cupos agendados es menor al valor ingresado.');
                                        $('#capacidad_eecc_'+turno_eecc).focus();
                                        setTimeout("clearMsg()",2200);
                                        
                                        }else{
                                            if(respuesta==3){
                                                $('#box_mensaje_ok').css({
                                                    'border':'1px solid #0292E9',
                                                    'background':'#E8F2F6',
                                                    'height':'30px',
                                                    'color':'#048FE4'
                                                });
                                                $('#box_mensaje_ok').css('display','block');
                                                $('#box_turno_eecc_'+turno_eecc).css('display','none');
                                                $('#a_link_eecc_'+turno_eecc).css('display','inline');
                                                $('#span_eecc_'+turno_eecc).html(nvo_valor_eecc);
                                                $('#op_change_eecc_'+turno_eecc).css('visibility','hidden');
                                                $('#box_mensaje_ok').html('Cupos Guardados.<img src="img/accept.png" alt="" />');
                                                setTimeout("clearMsg()",2000);
                                            }
                                        }
                                    }
                                    

                                });

                                clearMsg=function(){
                                    $('#box_mensaje_ok').css('display','none');
                                }
                             
                             
                             }else{
                                 alert("La capacidad de cupos de la Empresa Contratista no debe ser mayor al total de cupos del Turno");
                                 $('#capacidad_eecc_'+turno_eecc).focus();
                             }
                             
                            });


                });
            
            });
    });
    
    }
    });

    $('#zonal_adm').change(function(){
                var idempresa=$('#empresa').val();
                var idtipopedido=$('#tipo').val();
                var zonal=$('#zonal_adm').val();
                var emisor=$('#emisor').val();
                $('.img_horario').css('display','none');
                //alert(idempresa);
                $('#box_agendamiento').html('Cargando Agenda.<img src="img/loading.gif" alt="" />');
                $.post("administracion.php?cmd=loadAgendaTurnos", {
                idempresa:idempresa,
                emisor:emisor,
                idtipopedido:idtipopedido,
                zonal:zonal
                },
                function(data){
                    $('#box_agendamiento').empty();
                    //$('#tr_buttons').css('visibility','visible');
                    $('#box_agendamiento').append(data);
                    //Para actualizar
                            $('.textlink').click(function(){
                            var turno=$(this).attr('rel');
                            $(this).css('display','none');
                            $('#op_change_'+turno).css('visibility','visible');
                            $('#box_turno_'+turno).css('display','inline');
                            $('#capacidad_'+turno).attr('value');
                            $('#capacidad_'+turno).focus();
                            });

                            $('.ipt_hor_up').blur(function(){
                            var valor=$(this).attr('value');
                            if(valor==''){
                            $(this).attr('value','0');
                            }
                            sumarCupos();
                            });

                           $(document).keyup(function(e) {
                            if (e.keyCode == 27) {
                                $('.box_op_img').css('visibility','hidden');
                                $('.box_content_valor').css('display','none');
                                $('.textlink').css('display','inline');
                            }
                            });

                            $('.img_op_grabar').click(function(){
                             var turno=$(this).attr('name');
                             var nvo_valor=$('#capacidad_'+turno).attr('value');
                             if(nvo_valor<=150 && nvo_valor>=0){
                             var ant_valor=$('#ant_valor_'+turno).val();
                             var emisor=$('#emisor').attr('value');
                             var id_agenda=$('#id_agenda_'+turno).attr('value');
                             var fecha_dia=$('#fecha_dia_'+turno).attr('value');
                             var respuesta;
                                $.post("administracion.php?cmd=updateCuposAgendaXturno", {
                                id_agenda:id_agenda,
                                emisor:emisor,
                                nvocapacidad:nvo_valor,
                                antcapacidad:ant_valor,
                                fecha_dia:fecha_dia
                                },
                                function(data){
                                   respuesta=data;

                                   if(respuesta==1){
                                        $('#box_mensaje_ok').css({
                                           'border':'1px solid #CD0A0A',
                                           'background':'#FEF2EE',
                                           'color':'#CD0A0A'
                                        });
                                        $('#box_mensaje_ok').css('display','block');
                                        $('#box_mensaje_ok').html('Imposible actualizar capacidad de cupos.<img src="img/aviso_small.png" alt="" /><br />Los cupos agendados es igual al valor ingresado.');
                                        $('#capacidad_'+turno).focus();
                                        setTimeout("clearMsg()",2000);
                                    }else{
                                        if(respuesta==2){
                                            $('#box_mensaje_ok').css({
                                           'border':'1px solid #CD0A0A',
                                           'background':'#FEF2EE',
                                           'color':'#CD0A0A'
                                        });
                                        $('#box_mensaje_ok').css('display','block');
                                        $('#box_mensaje_ok').html('Imposible actualizar capacidad de cupos.<img src="img/aviso_small.png" alt="" /><br />Los cupos agendados es menor al valor ingresado.');
                                        $('#capacidad_'+turno).focus();
                                        setTimeout("clearMsg()",2000);

                                        }else{
                                            if(respuesta==3){
                                                $('#box_mensaje_ok').css({
                                                    'border':'1px solid #0292E9',
                                                    'background':'#E8F2F6',
                                                    'height':'30px',
                                                    'color':'#048FE4'
                                                });
                                                $('#box_mensaje_ok').css('display','block');
                                                $('#box_turno_'+turno).css('display','none');
                                                $('#a_link_'+turno).css('display','inline');
                                                $('#span_'+turno).html(nvo_valor);
                                                $('#op_change_'+turno).css('visibility','hidden');
                                                $('#box_mensaje_ok').html('Cupos Guardados.<img src="img/accept.png" alt="" />');
                                                setTimeout("clearMsg()",2000);
                                            }
                                        }
                                    }
                               
                                
                                });

                                clearMsg=function(){
                                    $('#box_mensaje_ok').css('display','none');
                                }
                                
                               }else{
                                alert("La cantidad debe estar en el rango de 50 a 150 cupos.");
                                $('#capacidad_'+turno).focus();
                                }
                            });


                });

            });

    

    //Para registrar
    $('.imglink').click(function(){
       var turno=$(this).attr('rel');
       $(this).css('display','none');
       $('#box_turno_'+turno).css('display','block');
       $('#capacidad_'+turno).attr('value','');
       $('#capacidad_'+turno).focus();
    });


    $('.ipt_hor').blur(function(){
        var valor=$(this).attr('value');
        if(valor==''){
            $(this).attr('value','0');
        }
        sumarCupos();
    });

    sumarCupos=function(){
        var tl_lunes=0;
       var tl_martes=0;
       var tl_miercoles=0;
       var tl_jueves=0;
       var tl_viernes=0;
       var tl_sabado=0;
       var tl_domingo=0;
       var row;

       for(row=1;row<=7;row++){
           var valor_lunes=$('#capacidad_1_'+row).attr('value');
           var valor_martes=$('#capacidad_2_'+row).attr('value');
           var valor_miercoles=$('#capacidad_3_'+row).attr('value');
           var valor_jueves=$('#capacidad_4_'+row).attr('value');
           var valor_viernes=$('#capacidad_5_'+row).attr('value');
           var valor_sabado=$('#capacidad_6_'+row).attr('value');
           var valor_domingo=$('#capacidad_7_'+row).attr('value');
           if(valor_lunes>0){
               tl_lunes=tl_lunes+parseInt(valor_lunes);
           }
           if(valor_martes>0){
               tl_martes=tl_martes+parseInt(valor_martes);
           }
            if(valor_miercoles>0){
               tl_miercoles=tl_miercoles+parseInt(valor_miercoles);
           }
           if(valor_jueves>0){
               tl_jueves=tl_jueves+parseInt(valor_jueves);
           }
           if(valor_viernes>0){
               tl_viernes=tl_viernes+parseInt(valor_viernes);
           }
           if(valor_sabado>0){
               tl_sabado=tl_sabado+parseInt(valor_sabado);
           }
           if(valor_domingo>0){
               tl_domingo=tl_domingo+parseInt(valor_domingo);
           }

       }

       if(parseInt(tl_lunes)>0){
           if(parseInt(tl_lunes)==1){
               $('#tl_lunes').html(tl_lunes);
           }else{
               $('#tl_lunes').html(tl_lunes);
           }
          
       }
       if(parseInt(tl_martes)>0){
          if(parseInt(tl_martes)==1){
              $('#tl_martes').html(tl_martes);
          }else{
              $('#tl_martes').html(tl_martes);
          }
       }
       if(parseInt(tl_miercoles)>0){
          if(parseInt(tl_miercoles)==1){
              $('#tl_miercoles').html(tl_miercoles);
          }else{
              $('#tl_miercoles').html(tl_miercoles);
          }
       }
       if(parseInt(tl_jueves)>0){
          if(parseInt(tl_jueves)==1){
              $('#tl_jueves').html(tl_jueves);
          }else{
              $('#tl_jueves').html(tl_jueves);
          }
       }
       if(parseInt(tl_viernes)>0){
          if(parseInt(tl_viernes)==1){
              $('#tl_viernes').html(tl_viernes);
          }else{
              $('#tl_viernes').html(tl_viernes);
          }
       }
       if(parseInt(tl_sabado)>0){
          if(parseInt(tl_sabado)==1){
              $('#tl_sabado').html(tl_sabado);
          }else{
              $('#tl_sabado').html(tl_sabado);
          }
       }
       if(parseInt(tl_domingo)>0){
          if(parseInt(tl_domingo)==1){
              $('#tl_domingo').html(tl_domingo);
          }else{
              $('#tl_domingo').html(tl_domingo);
          }
       }
    }
    
    sumarCupos_eecc=function(){
       var tl_lunes=0;
       var tl_martes=0;
       var tl_miercoles=0;
       var tl_jueves=0;
       var tl_viernes=0;
       var tl_sabado=0;
       var tl_domingo=0;
       var row;

       for(row=1;row<=7;row++){
           var valor_lunes=$('#capacidad_eecc_1_'+row).attr('value');
           var valor_martes=$('#capacidad_eecc_2_'+row).attr('value');
           var valor_miercoles=$('#capacidad_eecc_3_'+row).attr('value');
           var valor_jueves=$('#capacidad_eecc_4_'+row).attr('value');
           var valor_viernes=$('#capacidad_eecc_5_'+row).attr('value');
           var valor_sabado=$('#capacidad_eecc_6_'+row).attr('value');
           var valor_domingo=$('#capacidad_eecc_7_'+row).attr('value');
           if(valor_lunes>0){
               tl_lunes=tl_lunes+parseInt(valor_lunes);
           }
           if(valor_martes>0){
               tl_martes=tl_martes+parseInt(valor_martes);
           }
            if(valor_miercoles>0){
               tl_miercoles=tl_miercoles+parseInt(valor_miercoles);
           }
           if(valor_jueves>0){
               tl_jueves=tl_jueves+parseInt(valor_jueves);
           }
           if(valor_viernes>0){
               tl_viernes=tl_viernes+parseInt(valor_viernes);
           }
           if(valor_sabado>0){
               tl_sabado=tl_sabado+parseInt(valor_sabado);
           }
           if(valor_domingo>0){
               tl_domingo=tl_domingo+parseInt(valor_domingo);
           }

       }

       if(parseInt(tl_lunes)>0){
           if(parseInt(tl_lunes)==1){
               $('#tl_lunes_eecc').html(tl_lunes);
           }else{
               $('#tl_lunes_eecc').html(tl_lunes);
           }
          
       }
       if(parseInt(tl_martes)>0){
          if(parseInt(tl_martes)==1){
              $('#tl_martes_eecc').html(tl_martes);
          }else{
              $('#tl_martes_eecc').html(tl_martes);
          }
       }
       if(parseInt(tl_miercoles)>0){
          if(parseInt(tl_miercoles)==1){
              $('#tl_miercoles_eecc').html(tl_miercoles);
          }else{
              $('#tl_miercoles_eecc').html(tl_miercoles);
          }
       }
       if(parseInt(tl_jueves)>0){
          if(parseInt(tl_jueves)==1){
              $('#tl_jueves_eec').html(tl_jueves);
          }else{
              $('#tl_jueves_eecc').html(tl_jueves);
          }
       }
       if(parseInt(tl_viernes)>0){
          if(parseInt(tl_viernes)==1){
              $('#tl_viernes_eecc').html(tl_viernes);
          }else{
              $('#tl_viernes_eecc').html(tl_viernes);
          }
       }
       if(parseInt(tl_sabado)>0){
          if(parseInt(tl_sabado)==1){
              $('#tl_sabado_eecc').html(tl_sabado);
          }else{
              $('#tl_sabado_eecc').html(tl_sabado);
          }
       }
       if(parseInt(tl_domingo)>0){
          if(parseInt(tl_domingo)==1){
              $('#tl_domingo_eecc').html(tl_domingo);
          }else{
              $('#tl_domingo_eecc').html(tl_domingo);
          }
       }
    }

    $('#btn_deshacer').click(function(){
        $('#frm_agenda')[0].reset();
        limpiarHorario();
    });

    $('#btnLimpiarHorario').click(function(){
        limpiarHorario();
    });

    limpiarHorario=function(){
       var row;
       for(row=1;row<=7;row++){
        $('#box_turno_1_'+row).css('display','none');
        $('#box_turno_2_'+row).css('display','none');
        $('#box_turno_3_'+row).css('display','none');
        $('#box_turno_4_'+row).css('display','none');
        $('#box_turno_5_'+row).css('display','none');
        $('#box_turno_6_'+row).css('display','none');
        $('#box_turno_7_'+row).css('display','none');
       }
       $('.imglink').css('display','block');
       $('#tl_lunes').empty();
       $('#tl_martes').empty();
       $('#tl_miercoles').empty();
       $('#tl_jueves').empty();
       $('#tl_viernes').empty();
       $('#tl_sabado').empty();
       $('#tl_domingo').empty();
    }
validarNumber=function(e){
tecla = (document.all) ? e.keyCode : e.which;
if (tecla==8) return true;
patron =/\d/;//valida solo numeros
te = String.fromCharCode(tecla);
return patron.test(te);
}

});

