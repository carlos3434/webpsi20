/** 
 * Sistema Web Unificada TDP
 * @author Gonzalo Chacaltana Buleje <gchacaltana@gmd.com.pe>
 * GMD S.A
 * @file mantenimientoComunicados.js
 */
$(document).ready(function(){
   $(".trigger").mouseover(function(){
		$(".panel").toggle("fast");
		$(this).toggleClass("active");
		return false;
    });
    $('.panel').mouseleave(function(){
       $(".panel").toggle("fast");
       $('.trigger').toggleClass("active");
    });
    //Fin Menu
    
    //Metodo de registrar comunicado
	
    $('#btn_guardar_agregar').click(function(){
    	try{
    	var title = $.trim($('#titulo').attr("value"));
    	var EndDate = $.trim($('#fecha_fin').attr("value"));
    	var descripcion = $('#contenido').attr("value");
    	var file = $('#file').val();
    	var chk_saludo = "0";
    	var exe_process = false;
    	var allow_format = new Array('pdf','zip','rar');
    	var ext=$('#file').val().split('.').pop().toLowerCase();
    	//validamos
    	if(title.length<5){
    		alert("Ingrese un t&iacute;tulo mas completo para el comunicado");
                $('#titulo').focus();
    		return false;
    	}else{
    		if(descripcion.length<10){
    			alert("Por favor\nIngrese un contenido mas completo para el comunicado.");
    			$('#contenido').focus();
        		return false;
    		}else{
    			if(EndDate.length<10){
    				alert("Por favor\nSeleccione una fecha limite de visualizacion del comunicado.");
        			$('#fecha_fin').focus();
            		return false;
    			}else{
    				//Validamos archivo a cargar.
    				if(file!=''){
    					if(jQuery.inArray(ext, allow_format)==-1){
        					alert("Archivo incorrecto\nSolo esta permitido subir archivos pdf, winzip y winrar.");
        					return false;
        				}else{
        					//verificar si presentamos saludo personal al usuario
        	            	if($('#chk_saludo_personal').is(':checked')){
        	            		chk_saludo="1";
        	            	}
        	            	//verificamos permisos de publicacion
        	            	if($('#vis_all').is(':checked')){
        	            		exe_process=true;
        	            	}else{
        	            		exe_process=false;
        	            	}
        				}
    				}else{
    					//verificar si presentamos saludo personal al usuario
    	            	if($('#chk_saludo_personal').is(':checked')){
    	            		chk_saludo="1";
    	            	}
    	            	//verificamos permisos de publicacion
    	            	if($('#vis_all').is(':checked')){
    	            		exe_process=true;
    	            	}else{
    	            		exe_process=false;
    	            	}
    				}
    			}
    		}
    	}
    	//validamos permisos de visualizacion de comunicado
    	if(exe_process==false){
            var n_empresas=verificaCheck(document.getElementsByName('multiselect_slt_empresa[]'));
            if(n_empresas>0){
            	var n_zonales=verificaCheck(document.getElementsByName('multiselect_slt_zonales[]'));
            	if(n_zonales>0){
            		var n_areas=verificaCheck(document.getElementsByName('multiselect_slt_areas[]'));
            		if(n_areas>0){
            			exe_process=true;
            		}else{
            			alert("Por favor\nSeleccione la(s) area(s) para publicar el comunicado.");
                    	return false;
            		}
            	}else{
            		alert("Por favor\nSeleccione la(s) zonal(es) que desea publicar el comunicado.");
                	return false;
            	}
            }else{
            	alert("Por favor\nSeleccione la(s) empresa(s) que desea publicar el comunicado.");
            	return false;
            }
    	}
    	//sending data
    	if(exe_process==true){
    		if(confirm('Esta seguro de publicar el comunicado ingresado.')){
    			$("#dvloading").empty();
        		$(this).css("display","none");
        		$("#dvloading").html('Procesando <img src="img/loading.gif" alt="" title="" />');
        		$("#dvloading").css("display","block");
    			$('form#frmAdd_comunicado').submit();
                        $('#ifr_load_file_comunicado').load(function(){
                           $("#dvloading").empty();
                           //$("#dvloading").html("Comunicado registrado");
                           setTimeout(window.location="administracion.php?cmd=addComunicados",3500);
                        });
                        //setTimeout(window.location="administracion.php?cmd=addComunicados",2300);
    		}else{
    			return false;
    		}
    	}
    	}catch(exception){
    		alert("Ocurrio un error : "+exception.description());
    	}
    });
    
    
    infoComunicado=function(id){
    var emisor=$('#emisor').attr('value');
    loader('start');
    $("#childModal").html('');
    $("#childModal").css("background-color","#FFFFFF");
    $.post("administracion.php?cmd=viewInfoComunicado", {
        id: id,
        emisor:emisor
    },
    function(data){
        loader('end');
        $("#childModal").html(data);
    });

    $("#childModal").dialog({modal:true, width:'720px', hide: 'slide', title: 'Informaci&oacute;n del Comunicado', position:'top'});
    
    }
    
    change_state=function(id,estado,titulo){
        //true para desabilitar
        if(confirm("Esta seguro de cambiar el estado del comunicado de titulo : \n"+titulo)){
            loader('start');
            var emisor=$('#emisor').attr('value');
            var respuesta=0;
            var estado_inicial=estado;
            $.post("administracion.php?cmd=execUpdateStateComunicado", {id: id, state: estado, emisor:emisor}, function(data){
                  loader('end');
                  respuesta=data;
                  if(respuesta==1){
                      if(estado_inicial==1){
                          var nvo_estado=0;
                          $('#alink_state_'+id).attr('href','javascript:change_state(\''+id+'\',\''+nvo_estado+'\',\''+titulo+'\')');
                          $('#img_state_'+id).attr('src','img/estado_deshabilitado.png');
                          $('#img_state_'+id).attr('alt','Cambiar estado a Habilitado');
                          $('#img_state_'+id).attr('title','Cambiar estado a Habilitado');
                          return false;
                      }
                      else{
                          nvo_estado=1;
                          $('#alink_state_'+id).attr('href','javascript:change_state(\''+id+'\',\''+nvo_estado+'\',\''+titulo+'\')');
                          $('#img_state_'+id).attr('src','img/estado_habilitado.png');
                          $('#img_state_'+id).attr('alt','Cambiar estado a Deshabilitado');
                          $('#img_state_'+id).attr('title','Cambiar estado a Deshabilitado');
                          return false;
                      }
                  }
            });
        }
   }
 
   editComunicado=function(id){
    
    var emisor=$('#emisor').attr('value');
    loader('start');
    $("#childModal").html('');
    $("#childModal").css("background-color","#FFFFFF");
    $.post("administracion.php?cmd=editComunicado", {
        id: id,
        emisor:emisor
    },
    function(data){
        loader('end');
        $("#childModal").html(data);
    });

    $("#childModal").dialog({modal:true, width:'900px', hide: 'slide', title: 'Editar Comunicado', position:'top'});

   }
   show_alert=function(title,message,titulo_message){
        $('#dialog-modal').attr('title','<span style="font-size:11px;">'+title+'</span>');
        $('#dialog-modal').html('<span style="font-size:11px;"><b style="color:#CC0800;">'+titulo_message+':</b><br /><br />'+message);
        $( "#dialog-modal" ).dialog({modal: true});
    }
});
/**
* Funcion para visualizar mensajes
*/
