/* 
 * Proyecto Web Unificada TDP
 * Planificacion y Solu
 * Autor Gonzalo Chacaltana Buleje <gchacaltana@gmd.com.pe>
 */
$(document).ready(function(){
   $(".trigger").mouseover(function(){
		$(".panel").toggle("fast");
		$(this).toggleClass("active");
		return false;
    });
    $('.panel').mouseleave(function(){
       $(".panel").toggle("fast");
       $('.trigger').toggleClass("active");
    });
    //Fin Menu

   infoArea=function(idarea){
    var emisor=$('#emisor').attr('value');
    loader('start');
    $("#childModal").html('');
    $("#childModal").css("background-color","#FFFFFF");
    $.post("administracion.php?cmd=viewInfoArea", {
        idarea: idarea,
        emisor:emisor
    },
    function(data){
        loader('end');
        $("#childModal").html(data);
    });

    $("#childModal").dialog({modal:true, width:'720px', hide: 'slide', title: 'Informaci&oacute;n del Area', position:'top'});
    
   }
   
   change_state=function(id_area,estado,nom_area){
        //true para desabilitar
        if(confirm("Esta seguro de cambiar el estado del Area : "+nom_area)){
            loader('start');
    var emisor=$('#emisor').attr('value');
    var respuesta=0;
    var estado_inicial=estado;
    $.post("administracion.php?cmd=execUpdateStateArea", {id_area: id_area, state: estado, emisor:emisor}, function(data){
          loader('end');
          respuesta=data;

          if(respuesta==1){
              if(estado_inicial==1){
                  var nvo_estado=0;
                  $('#alink_state_'+id_area).attr('href','javascript:change_state(\''+id_area+'\',\''+nvo_estado+'\',\''+nom_area+'\')');
                  $('#img_state_'+id_area).attr('src','img/estado_deshabilitado.png');
                  $('#img_state_'+id_area).attr('alt','Cambiar estado a Habilitado');
                  $('#img_state_'+id_area).attr('title','Cambiar estado a Habilitado');
              }
              else{
                   nvo_estado=1;
                  $('#alink_state_'+id_area).attr('href','javascript:change_state(\''+id_area+'\',\''+nvo_estado+'\',\''+nom_area+'\')');
                  $('#img_state_'+id_area).attr('src','img/estado_habilitado.png');
                  $('#img_state_'+id_area).attr('alt','Cambiar estado a Deshabilitado');
                  $('#img_state_'+id_area).attr('title','Cambiar estado a Deshabilitado');
              }

          }
    });
        }
   }
   
   change_state_area_motivo = function(id_area_motivo,estado){
        //true para desabilitar
        if(confirm("Esta seguro de cambiar el estado del Area Motivo")){
            loader('start');
    var emisor=$('#emisor').attr('value');
    var respuesta=0;
    var estado_inicial=estado;
    $.post("administracion.php?cmd=execUpdateStateAreaMotivo", {id_area_motivo: id_area_motivo, state: estado, emisor:emisor}, function(data){
          loader('end');
          respuesta=data;

          if(respuesta==1){
              if(estado_inicial==1){
                  var nvo_estado=0;
                  $('#alink_state_'+id_area_motivo).attr('href','javascript:change_state_area_motivo(\''+id_area_motivo+'\',\''+nvo_estado+'\')');
                  $('#img_state_'+id_area_motivo).attr('src','img/estado_deshabilitado.png');
                  $('#img_state_'+id_area_motivo).attr('alt','Cambiar estado a Habilitado');
                  $('#img_state_'+id_area_motivo).attr('title','Cambiar estado a Habilitado');
              }
              else{
                   nvo_estado=1;
                  $('#alink_state_'+id_area_motivo).attr('href','javascript:change_state_area_motivo(\''+id_area_motivo+'\',\''+nvo_estado+'\')');
                  $('#img_state_'+id_area_motivo).attr('src','img/estado_habilitado.png');
                  $('#img_state_'+id_area_motivo).attr('alt','Cambiar estado a Deshabilitado');
                  $('#img_state_'+id_area_motivo).attr('title','Cambiar estado a Deshabilitado');
              }

          }
    });
        }
   }

   editArea=function(id_area){
    
    var emisor=$('#emisor').attr('value');
    loader('start');
    $("#childModal").html('');
    $("#childModal").css("background-color","#FFFFFF");
    $.post("administracion.php?cmd=editArea", {
        id_area: id_area,
        emisor:emisor
    },
    function(data){
        loader('end');
        $("#childModal").html(data);
    });

    $("#childModal").dialog({modal:true, width:'500px', hide: 'slide', title: 'Editar Area', position:'top'});

   }
   
   view_add_motivos=function(id_area){
    var emisor=$('#emisor').attr('value');
    loader('start');
    $("#childModal").html('');
    $("#childModal").css("background-color","#FFFFFF");
    $.post("administracion.php?cmd=viewAddMotivoArea", {
        id_area: id_area,
        emisor:emisor
    },
    function(data){
        loader('end');
        $("#childModal").html(data);
    });

    $("#childModal").dialog({modal:true, width:'800px', hide: 'slide', title: 'Agregar y Visualizar Motivos', position:'top'});

   }
   view_add_estados=function(id_area){
        var emisor=$('#emisor').attr('value');
        loader('start');
    $("#childModal").html('');
    $("#childModal").css("background-color","#FFFFFF");
    $.post("administracion.php?cmd=viewAddEstadoArea", {
        id_area: id_area,
        emisor:emisor
    },
    function(data){
        loader('end');
        $("#childModal").html(data);
    });

    $("#childModal").dialog({modal:true, width:'600px', hide: 'slide', title: 'Agregar y Visualizar Estados de Gesti&oacute;n', position:'top'});

   }

   $('#img_down_pdf').click(function(){
       alert("Muy pronto..");
   });

   $('#img_down_xls').click(function(){
       alert("Muy pronto..");
   });

});

