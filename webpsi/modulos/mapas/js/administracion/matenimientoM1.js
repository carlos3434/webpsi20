var M1 = function(){};

jQuery.extend(jQuery.validator.messages, {
    required: "Campo Requerido"
});



M1.prototype.preparaEdicion = function(){
    var links = $('a.editar');
    var _self = this;
    $.each(links,function(){
        $(this).click(function(ev){
            ev.preventDefault();
            tdp.cargaModalRegulable("Revisi&oacute;n de Pedido","modulos/m1/bandeja.m1.php?cmd=verPedido&editar=1&pedido="+$(this).attr("id_pedido"),950);
        });
    });	
    var links = $('a.ver');
    $.each(links,function(){
        $(this).click(function(ev){
            ev.preventDefault();
            tdp.cargaModalRegulable("Revisi&oacute;n de Pedido","modulos/m1/bandeja.m1.php?cmd=verPedido&resultado=1&pedido="+$(this).attr("id_pedido"),900);
        });
    });
    var links = $('a.editar_pedido');
    $.each(links,function(){
        $(this).click(function(ev){
            ev.preventDefault();
            tdp.cargaModalRegulable("Edici&oacute;n de Pedido","modulos/m1/bandeja.m1.php?cmd=verPedido&resultado=1&pedido="+$(this).attr("id_pedido"),900);
        });
    });
    var links_historial = $('a.historial');
    $.each(links_historial,function(){
        $(this).click(function(ev){
            ev.preventDefault();
            tdp.cargaModalRegulable("Historial de Pedido","modulos/m1/bandeja.m1.php?cmd=verHistorial&id_pedido="+$(this).attr("id_pedido")+"&pagina=",950);
        });
    });
    var links = $('a.eliminar_pedido');
    $.each(links,function(){
        var anchor = $(this);
        $(this).click(function(ev){
            ev.preventDefault();
            $('body').append("<div id='modalJqueryTDPPrompt' align='center'><p><br>Realmente desea eliminar el pedido <b>"+$(this).attr("codigo_pedido")+"</b> ?</p></div>");
            $('#modalJqueryTDPPrompt').createPrompt({
                ok :function(){
                    Cargando("inicio");
                    $.ajax({
                        type: "POST",
                        dataType: "html",
                        cache:false,
                        url: "modulos/m1/bandeja.m1.php?cmd=eliminaPedido",
                        data: {
                            id: anchor.attr("id_pedido")
                        },
                        success: function(data) {	
                            Cargando("fin");
                            _self.cargarGrid();
                        },
                        error : function(){
                            alert("<p style='color:red'>Parece que el servidor esta tardando en responder, por favor int&eacute;ntelo en unos minutos</p>");
                            Cargando("fin");
                        }
                    });
                },
                cancel : function(){
                    Cargando("fin");
                }
            });
        });
    });
};

M1.prototype.preparaEdicionHistorial = function (){
    var links_historial = $('a.historial_detalle');
    $.each(links_historial,function(){
        $(this).click(function(ev){
            ev.preventDefault();
            $.post("modulos/m1/bandeja.m1.php?cmd=verDetalleHistorial",{
                id_historial_pedido : $(this).attr("id_h_pedido")
            },function(data){
                $('#div_vista_historial').html(data);
            });
        });
    });
};

M1.prototype.preparaEdicionFiltros = function(){
    var links = $('a.editarFiltro');
    var _self = this;
    $.each(links,function(){
        $(this).click(function(ev){
            ev.preventDefault();
            tdp.cargaModalRegulable("Acciones en Filtros","modulos/m1/bandeja.m1.php?cmd=verFiltro&id="+$(this).attr("id_filtro")+"&tipo="+$(this).attr("tipo"),500);
        });
    });
    var links = $('a.eliminaFiltro');
    var _self = this;
    $.each(links,function(){
        var anchor = $(this);
        $(this).click(function(ev){
            $('body').append("<div id='modalJqueryTDPPrompt' align='center'><p><br>Realmente desea eliminar el filtro seleccionado?</p></div>");
            $('#modalJqueryTDPPrompt').createPrompt({
                ok :function(){
                    Cargando("inicio");
                    $.ajax({
                        type: "POST",
                        dataType: "json",
                        cache:false,
                        url: "modulos/m1/bandeja.m1.php?cmd=eliminaFiltro",
                        data: {
                            id: anchor.attr("id_filtro"),
                            tipo : anchor.attr("tipo")
                        },
                        success: function(data) {	
                            Cargando("fin");
                            _self.cargaGridFiltros(anchor.attr("tipo"));
                        },
                        error : function(){
                            alert("<p style='color:red'>Parece que el servidor esta tardando en responder, por favor int&eacute;ntelo en unos minutos</p>");
                            Cargando("fin");
                        }
                    });
                },
                cancel : function(){
                    Cargando("fin");
                }
            });
        });
    });
};

M1.prototype.cargarGrid = function(){
    Cargando("inicio");
    $.ajax({
        url: "modulos/m1/bandeja.m1.php?cmd=listar",
        cache:false,
        type: "POST",
        data :{
            pagina :''
        },
        success: function(data) {
            $('#grid').html(data);
            $('#filtro_estado').val("");
            Cargando("fin");
        },
        error : function(){	
            alert("<p style='color:red'>Parece que el servidor esta tardando en responder, por favor int&eacute;ntelo en unos minutos</p>");
            Cargando("fin");
        }
    });
};

M1.prototype.cargarGridDevueltos = function(){
    Cargando("inicio");
    $.ajax({
        url: "modulos/m1/bandeja.m1.php?cmd=listar",
        cache:false,
        type: "POST",
        data :{
            pagina :'',
            devueltos : 1
        },
        success: function(data) {
            $('#grid').html(data);
            $('#filtro_estado').val("");
            Cargando("fin");
        },
        error : function(){	
            alert("<p style='color:red'>Parece que el servidor esta tardando en responder, por favor int&eacute;ntelo en unos minutos</p>");
            Cargando("fin");
        }
    });
};

M1.prototype.cargaGridFiltros = function(tipo){
    Cargando("inicio");
    $.ajax({
        url: "modulos/m1/bandeja.m1.php?cmd=listarFiltros",
        cache:false,
        type: "POST",
        data :{
            pagina :'',
            tipo : tipo
        },
        success: function(data) {
            $('#grid').html(data);
            $('#filtro_estado').val("");
            Cargando("fin");
        },
        error : function(){	
            alert("<p style='color:red'>Parece que el servidor esta tardando en responder, por favor int&eacute;ntelo en unos minutos</p>");
            Cargando("fin");
        }
    }); 
};

M1.prototype.preparaFormularioNuevoPedido = function(){
    
    $('#codigo_cms_cliente').keyup(function(){
        $("#codigo_cms_cliente_valida").hide();
    });
    
    $('#telefono_cliente').keyup(function(){
        $("#telefono_cliente_valida").hide();
    });
    
    $('#form_registro_m1').validate({        
        rules: {
            email: {
                required: true,
                email: true
            },
            required : {
                required: true
            }
        },
        messages: {
            email: {
                email: "Debe escribir un email v&aacute;lido"
            },
            required:{
                required : "*"
            }
        },
        submitHandler : function(form) {
            
            var estado_linea = $('#tratamiento_linea').find("option:selected").val();  
            var estado_cable = $('#tratamiento_cable').find("option:selected").val();  
            var estado_validacion = true;
            
            if(estado_linea != "1" && estado_cable != "1" ){
                
                if($("#codigo_cms_cliente").val() == ""){
                    $("#codigo_cms_cliente_valida").show();
                    estado_validacion = false;
                }else{
                    estado_validacion = true;
                };
                
                
                if($("#telefono_cliente").val() == ""){
                    $("#telefono_cliente_valida").show(); 
                    estado_validacion = false;
                }else{
                    estado_validacion = true;
                };
               
            }else{
                $("#codigo_cms_cliente_valida").hide();
                $("#telefono_cliente_valida").hide(); 
                estado_validacion = true;
            };
           
            if(estado_validacion){
                
                //$('#btn_envio').attr("disabled","disabled").val("Grabando registro...").addClass("ui-state-disabled");
                $.post($(form).attr("action"),
                    $(form).serialize(),
                    function(rpta){
                        var msg = "";
                        if(rpta.estado == 1){
                            msg = "<div style='color:green;text-align:center'>Grabado con &eacute;xito <br>C&oacute;digo generado para el pedido : <b>"+rpta.codigo_generado+"</b><div>";
                        
                            $("#codigo_cms_cliente_valida").hide();
                            $("#telefono_cliente_valida").hide(); 
                        
                        }else{
                            msg = "<div style='color:red;text-align:center'><br />"+rpta.mensaje+"<br /></div>";  
                        }
                        Emergente('form_m1_registro',msg);
                        // $('#btn_envio').attr("disabled","").val("Registrar Pedido").removeClass("ui-state-disabled");
                        backM1.cargarGrid();
                        var idForm = $(form).attr("id");
                        if(rpta.estado == "1"){
                            document.getElementById(idForm).reset();
                        };
                    },"json"); 
            };  
        },
        invalidHandler:function(){
            
            var estado_linea = $('#tratamiento_linea').find("option:selected").val();  
            var estado_cable = $('#tratamiento_cable').find("option:selected").val();  
            
            if(estado_linea != "1" ){
                $("#codigo_cms_cliente_valida").show();
                $("#telefono_cliente_valida").show(); 
            }else{
                $("#codigo_cms_cliente_valida").hide();
                $("#telefono_cliente_valida").hide(); 
            };
            
            if(estado_cable != "1" ){
                $("#codigo_cms_cliente_valida").show();
                $("#telefono_cliente_valida").show(); 
            }else{
                $("#codigo_cms_cliente_valida").hide();
                $("#telefono_cliente_valida").hide(); 
            };
        }
    });
    
    $('#form_registro_m1 td').attr("align","left");
    $('.fecha').datetimepicker();
   
};

M1.prototype.preparaFormularioEdicionPedido = function (){
    
    $('#form_registro_m1').validate({        
        submitHandler : function(form) {
           
            var estado_linea = $('#tratamiento_linea').find("option:selected").val();  
            var estado_cable = $('#tratamiento_cable').find("option:selected").val();  
            var estado_validacion = true;
            
            if(estado_linea != "1" && estado_cable != "1" ){
                
                if($("#codigo_cms_cliente").val() == ""){
                    $("#codigo_cms_cliente_valida").show();
                    estado_validacion = false;
                }else{
                    estado_validacion = true;
                }
                
                
                if($("#telefono_cliente").val() == ""){
                    $("#telefono_cliente_valida").show(); 
                    estado_validacion = false;
                }else{
                    estado_validacion = true;
                }; 
                
            }else{
                $("#codigo_cms_cliente_valida").hide();
                $("#telefono_cliente_valida").hide(); 
                estado_validacion = true;
            };
          
            if(estado_validacion){
            
                $('#btn_envio').attr("disabled","disabled").val("Grabando cambios...").addClass("ui-state-disabled");
                $.post($(form).attr("action"),
                    $(form).serialize(),
                    function(rpta){
                        var msg = "<div style='color:red;text-align:center'>Grabado con &eacute;xito<div>";
                        Emergente('form_m1_registro',msg);
                        $('#btn_envio').attr("disabled","").val("Modificar Pedido").removeClass("ui-state-disabled");
                        backM1.cargarGrid();
                    },"json");
            };
        },
        invalidHandler : function(){
            var estado_linea = $('#tratamiento_linea').find("option:selected").val();  
            var estado_cable = $('#tratamiento_cable').find("option:selected").val();  
            
            if(estado_linea != "1" ){
                $("#codigo_cms_cliente_valida").show();
                $("#telefono_cliente_valida").show(); 
            }else{
                $("#codigo_cms_cliente_valida").hide();
                $("#telefono_cliente_valida").hide(); 
            };
            
            if(estado_cable != "1" ){
                $("#codigo_cms_cliente_valida").show();
                $("#telefono_cliente_valida").show(); 
            }else{
                $("#codigo_cms_cliente_valida").hide();
                $("#telefono_cliente_valida").hide(); 
            };
        }
    });
    $('#form_registro_m1 td').attr("align","left");
    $('.fecha').datetimepicker({
        maxDate : '0d',
        dateFormat: 'dd/mm/yy'
    });
   
};

M1.prototype.preparaFiltroProducto = function (){

    $('#tipo_pedido_movistar').change(function(){
        var tipo_producto = $(this).find("option:selected").attr("id");
        $.ajax({
            url: "modulos/m1/bandeja.m1.php?cmd=listarFiltrosPorProducto",
            cache:false,
            type: "POST",
            dataType: "json",
            data :{
                tipo : tipo_producto
            },
            success: function(data) {
                
                var canales = data.canales_venta;
                var productos = data.productos;
                var renta_mensual = data.renta_mensual;
                var tipos = data.tipos;
                
                var cbo_canal = $('#canal_venta');
                var cbo_producto = $('#producto_mu');
                var cbo_renta_mensual = $("#renta_mensual_mu");
                var cbo_tipo_pedido = $('#tipo_pedido_mu');
                
                var option1 = cbo_canal.find('option:first');
                var cad = "<option value=''>" + option1.html() + "</option>";
                
                cbo_canal.html(cad);
                cbo_producto.html(cad);
                cbo_renta_mensual.html(cad);
                cbo_tipo_pedido.html(cad);
                
                for(var x_canal in canales){
                    cbo_canal.append("<option value='"+canales[x_canal].id+"'>"+canales[x_canal].nombre+"</option>");
                };
                
                for(var x_producto in productos){
                    cbo_producto.append("<option value='"+productos[x_producto].id+"'>"+productos[x_producto].nombre+"</option>");
                };
                
                for(var x_renta in renta_mensual){
                    cbo_renta_mensual.append("<option value='"+renta_mensual[x_renta].id+"'>"+renta_mensual[x_renta].nombre+"</option>");
                };
                
                for(var x_tipo in tipos){
                    cbo_tipo_pedido.append("<option value='"+tipos[x_tipo].id+"'>"+tipos[x_tipo].nombre+"</option>");
                };
              
            },
            error : function(){	
                alert("<p style='color:red'>Parece que el servidor esta tardando en responder, por favor int&eacute;ntelo en unos minutos</p>");
            }
        }); 
    });
};

var backM1 = new M1();