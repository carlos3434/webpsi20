/*
 * Proyecto Web Unificada TDP
 */
$(document).ready(function(){
    $(".trigger").mouseover(function(){
        $(".panel").toggle("fast");
        $(this).toggleClass("active");
        return false;
    });
    $('.panel').mouseleave(function(){
        $(".panel").toggle("fast");
        $('.trigger').toggleClass("active");
    });
    //Fin Menu





    editPrioridad=function(id_prioridad){

        var emisor=$('#emisor').attr('value');
        loader('start');
        $("#childModal").html('');
        $("#childModal").css("background-color","#FFFFFF");
        $.post("administracion.php?cmd=editPrioridad", {
            id_prioridad: id_prioridad
        },
        function(data){
            loader('end');
            $("#childModal").html(data);
        });

        $("#childModal").dialog({
            modal:true,
            width:'500px',
            hide: 'slide',
            title: 'Editar Prioridades',
            position:'top'
        });

    }


    $('#img_down_pdf').click(function(){
        alert("Muy pronto..");
    });

    $('#img_down_xls').click(function(){
        alert("Muy pronto..");
    });

});


