/*
 * Telefonica del Peru.
 * Planificacion y Soluciones Informaticas STC
 * Proyecto Web Unificada - STC TDP
 * @autor: Gonzalo J. Chacaltana Buleje. - <gchacaltana@gmd.com.pe>
 * GMD S.A
 **/
$(document).ready(function(){
        
       showModalChecks=function(div_chks,btn_div){
        var ventanaActiva=$('#ventanaActiva').attr('value');
        if(ventanaActiva==0){
        ventanaActiva=$('#ventanaActiva').attr('value','1');
   
            $('#'+div_chks).fadeIn(1500);
            $('#'+btn_div).focus();
        }else{alert("Ya existe una ventana de seleccion activa.");}
        }

       verificaCheckModal=function(chk_name,div_chks,slt,msg1,msg2){
       var group = document.getElementsByName(chk_name);
       var cantidad=verificaCheck(group);

       if(cantidad>0){
           $('#'+div_chks).fadeOut(800);
           $('#'+slt).html('<option value="1">'+cantidad+' '+msg1+'</option>');
           $('#ventanaActiva').attr('value',0);
           if(slt=="select_area"){
               //Verifica si existe funcionalidades para las areas seleccionadas - spu
               checkingFunctionXarea();
           }
       }else{
           alert(msg2);
       }
       }

       marcarDesmarcarChks=function(obj,chk_class){
        if(obj.is(':checked')){
            $('.'+chk_class).attr('checked',true);
        }else{
            $('.'+chk_class).attr('checked',false);
        }
       }
       
       cerrarVentana=function(div_chks){
           $('#'+div_chks).fadeOut(900);
           $('#ventanaActiva').attr('value',0);
       }


        //Funciones para Seleccionar Empresas
        $('#select_empresa').click(function(){showModalChecks('show_select_empresas','btn_listo_empresas');});

        $('#btn_listo_empresas').click(function(){
        verificaCheckModal('empresas[]','show_select_empresas','select_empresa',' item(s) seleccionado(s).','Debe seleccionar una Empresa.');
        });

        $('#chk_empresas_all').change(function(){marcarDesmarcarChks($(this),'chk_empresas');});

        //Funciones para Seleccionar Areas del usuario
        $('#select_area').click(function(){showModalChecks('show_select_areas','btn_listo_areas');});

        $('#btn_listo_areas').click(function(){
        verificaCheckModal('areas[]','show_select_areas','select_area',' item(s) seleccionado(s).','Debe seleccionar un Area.');
        });

        $('#chk_areas_all').click(function(){marcarDesmarcarChks($(this),'chk_areas');});


        //Funciones para Seleccionar zonales del usuario
        $('#select_zonal').click(function(){showModalChecks('show_select_zonales','btn_listo_zonales');});

        $('#btn_listo_zonales').click(function(){
        verificaCheckModal('grupo_zonal[]','show_select_zonales','select_zonal',' item(s) seleccionado(s).','Debe seleccionar una Zonal.');
        });

        $('#chk_zonales_all').change(function(){marcarDesmarcarChks($(this),'chk_zonales');});
        $('#btnCerrarZonal').click(function(){cerrarVentana('show_select_zonales');});


        //Funciones para Seleccionar Tipos de Pedidos del usuario
        $('#select_pedido').click(function(){showModalChecks('show_select_tipopedidos','btn_listo_tipopedidos');});

        $('#btn_listo_tipopedidos').click(function(){
        verificaCheckModal('tipopedidos[]','show_select_tipopedidos','select_pedido',' item(s) seleccionado(s).','Debe seleccionar un Tipo de Pedido.');
        });

        $('#chk_tipopedidos_all').change(function(){marcarDesmarcarChks($(this),'chk_tipopedidos');});
        $('#btnCerrarTipoPedido').click(function(){cerrarVentana('show_select_tipopedidos');});

        //Funciones para Seleccionar Tipos de Negocios del usuario
        $('#select_negocio').click(function(){showModalChecks('show_select_tiponegocios','btn_listo_tiponegocios');});

        $('#btn_listo_tiponegocios').click(function(){
        verificaCheckModal('tiponegocios[]','show_select_tiponegocios','select_negocio',' item(s) seleccionado(s).','Debe seleccionar un Tipo de Negocio.');
        });

        $('#chk_tiponegocios_all').change(function(){marcarDesmarcarChks($(this),'chk_tiponegocios');});
        $('#btnCerrarTipoNegocios').click(function(){cerrarVentana('show_select_tiponegocios');});

        //Funciones para Seleccionar Segmentos del usuario
        $('#select_segmento').click(function(){showModalChecks('show_select_segmentos','btn_listo_segmentos');});

        $('#btn_listo_segmentos').click(function(){
        verificaCheckModal('segmentos[]','show_select_segmentos','select_segmento',' item(s) seleccionado(s).','Debe seleccionar un Segmento.');
        });

        $('#chk_segmentos_all').change(function(){marcarDesmarcarChks($(this),'chk_segmentos');});

        $('#btnCerrarSegmentos').click(function(){cerrarVentana('show_select_segmentos');});
        
        //Funciones para Seleccionar productos del usuario - ASEG
        $('#select_producto').click(function(){showModalChecks('show_select_productos','btn_listo_productos');});

        $('#btn_listo_productos').click(function(){
        verificaCheckModal('productos[]','show_select_productos','select_producto',' item(s) seleccionado(s).','Debe seleccionar una Producto.');
        });

        $('#chk_productos_all').change(function(){marcarDesmarcarChks($(this),'chk_productos');});
        $('#btnCerrarProducto').click(function(){cerrarVentana('show_select_productos');});
        
        //Funciones para Seleccionar bolsa del usuario - ASEG
        $('#select_bolsa').click(function(){showModalChecks('show_select_bolsas','btn_listo_bolsas');});

        $('#btn_listo_bolsas').click(function(){
        verificaCheckModal('bolsas[]','show_select_bolsas','select_bolsa',' item(s) seleccionado(s).','Debe seleccionar una Bolsa.');
        });

        $('#chk_bolsas_all').change(function(){marcarDesmarcarChks($(this),'chk_bolsas');});
        $('#btnCerrarBolsa').click(function(){cerrarVentana('show_select_bolsas');});
        
        //Funciones para Seleccionar prioridades del usuario - PAI
        $('#select_prioridad').click(function(){showModalChecks('show_select_prioridades','btn_listo_prioridades');});

        $('#btn_listo_prioridades').click(function(){
        verificaCheckModal('prioridades[]','show_select_prioridades','select_prioridad',' item(s) seleccionado(s).','Debe seleccionar una Prioridad.');
        });

        $('#chk_prioridades_all').change(function(){marcarDesmarcarChks($(this),'chk_prioridades');});
        $('#btnCerrarPrioridad').click(function(){cerrarVentana('show_select_prioridades');});
        
        
        //Funciones para Seleccionar tipo de operaciones del usuario - PAI
        $('#select_tipooperacion').click(function(){showModalChecks('show_select_tipooperaciones','btn_listo_tipooperaciones');});

        $('#btn_listo_tipooperaciones').click(function(){
        verificaCheckModal('tipooperaciones[]','show_select_tipooperaciones','select_tipooperacion',' item(s) seleccionado(s).','Debe seleccionar un Tipo de Operacion.');
        });

        $('#chk_tipooperaciones_all').change(function(){marcarDesmarcarChks($(this),'chk_tipooperaciones');});
        $('#btnCerrarTipooperacion').click(function(){cerrarVentana('show_select_tipooperaciones');});
        
        
        //Funciones para Seleccionar los motivo devoluciones del usuario - PAI
        $('#select_motivodevolucion').click(function(){showModalChecks('show_select_motivodevoluciones','btn_listo_motivodevoluciones');});

        $('#btn_listo_motivodevoluciones').click(function(){
        verificaCheckModal('motivodevoluciones[]','show_select_motivodevoluciones','select_motivodevolucion',' item(s) seleccionado(s).','Debe seleccionar un Motivo de Devolucion.');
        });

        $('#chk_motivodevoluciones_all').change(function(){marcarDesmarcarChks($(this),'chk_motivodevoluciones');});
        $('#btnCerrarMotivodevolucion').click(function(){cerrarVentana('show_select_motivodevoluciones');});
        
        //Funciones para Seleccionar los canales del usuario - PAI Canales
        $('#select_canal').click(function(){showModalChecks('show_select_canales','btn_listo_canales');});

        $('#btn_listo_canales').click(function(){
        verificaCheckModal('canales[]','show_select_canales','select_canal',' item(s) seleccionado(s).','Debe seleccionar un Canal.');
        });

        $('#chk_canales_all').change(function(){marcarDesmarcarChks($(this),'chk_canales');});
        $('#btnCerrarCanal').click(function(){cerrarVentana('show_select_canales');});

        //Casos especiales  - Nuevo usuario
        //MDFs segun la zonal seleccionada
        array_value_mdf=new Array();
        $('#select_mdf').click(function(){
            var emisor=$('#emisor').val();
            var zonal=$('#select_zonal').val();
            var mdfLoaded=$('#mdfLoaded').val();
            var empresa=$('#select_empresa').val();
            if(empresa==0){alert("Debe seleccionar una empresa");}else{
            if(zonal==0){alert("Debe seleccionar una Zonal.");}else{
                var zonales=new Array();
                zonales=load_array('grupo_zonal[]');

                var empresas=new Array();
                empresas=load_array('empresas[]');
                $('#box_mdfs_selection').html('Cargando datos.<img src="img/loading.gif" alt="" />');
                $.post("administracion.php?cmd=viewMdfxZonalxEmpresa", {zonales: zonales,emisor:emisor,array_mdfs:array_value_mdf,mdfLoaded:mdfLoaded,empresas:empresas}, function(data){
                       $("#box_mdfs_selection").empty();
                       $("#box_mdfs_selection").append(data);

                       //Funciones para Seleccionar MDFs
                        showModalChecks('show_select_mdfs','btn_listo_mdfs');

                        $('#btn_listo_mdfs').click(function(){
                        array_value_mdf=load_array('mdfs[]');
                        verificaCheckModal('mdfs[]','show_select_mdfs','select_mdf',' mdf(s) seleccionada(s).','Debe seleccionar un MDF.');
                        $('#mdfLoaded').attr('value','1');
                        
                    });

                         $('#chk_mdfs_all').change(function(){marcarDesmarcarChks($(this),'chk_mdfs');});
                         $('#btnCerrarMdfs').click(function(){cerrarVentana('show_select_mdfs');});

                });
            }
            }
        });
        //Motivos segun el area seleccionada
        array_value_motivos=new Array();
        $('#select_motivo').click(function(){
            var emisor=$('#emisor').val();
            var area=$('#select_area').val();
            var motivoLoaded=$('#motivosLoaded').val();
            if(area==0){alert("Debe seleccionar un Area.");}else{
                var areas=new Array();
                areas=load_array('areas[]');
                $('#box_motivos_selection').html('Cargando datos.<img src="img/loading.gif" alt="" />');
                $.post("administracion.php?cmd=viewMotivosxArea", {areas: areas,emisor:emisor,array_motivos:array_value_motivos,motivoLoaded:motivoLoaded}, function(data){
                       $("#box_motivos_selection").empty();
                       $("#box_motivos_selection").append(data);

                       //Funciones para Seleccionar Motivos
                        showModalChecks('show_select_motivos','btn_listo_motivos');

                        $('#btn_listo_motivos').click(function(){
                        array_value_motivos=load_array('motivos[]');
                        verificaCheckModal('motivos[]','show_select_motivos','select_motivo',' motivo(s) seleccionado(s).','Debe seleccionar un Motivo.');
                        $('#motivosLoaded').attr('value','1');

                    });

                         $('#chk_motivos_all').change(function(){marcarDesmarcarChks($(this),'chk_motivos');});
                         $('#btnCerrarMotivos').click(function(){cerrarVentana('show_select_motivos');});

                });
            }
        });
        
        $('#btn_load_subbolsa').click(function(){
            var emisor=$('#emisor').val();
            var group = document.getElementsByName('multiselect_slt_bolsa[]');
            var cantidad=verificaCheck(group);
       
            if(cantidad==0){alert("Debe seleccionar una Bolsa.");}
            else{

                var bolsas=new Array();
                bolsas=load_array('multiselect_slt_bolsa[]');
                $('#box_subbolsa_data').empty();
                $('#box_subbolsa_data').html('Cargando datos.<img src="img/loading.gif" alt="" />');
                $.post("administracion.php?cmd=viewSubbolsa_x_Bolsa", {bolsas: bolsas,emisor:emisor},
                function(data){
                    $('#box_subbolsa_data').empty();
                    $('#box_subbolsa_data').append(data);
                });
                
            }
           
           
        });
        
        $('#btn_load_puntoventa').click(function(){
            var emisor=$('#emisor').val();
            var group = document.getElementsByName('multiselect_slt_canal[]');
            var cantidad=verificaCheck(group);
       
            if(cantidad==0){alert("Debe seleccionar un Canal.");}
            else{

                var canales=new Array();
                canales=load_array('multiselect_slt_canal[]');
                $('#box_puntoventa_data').empty();
                $('#box_puntoventa_data').html('Cargando datos.<img src="img/loading.gif" alt="" />');
                $.post("administracion.php?cmd=viewPuntoventa_x_Canal", {canales: canales,emisor:emisor},
                function(data){
                    $('#box_puntoventa_data').empty();
                    $('#box_puntoventa_data').append(data);
                });
                
            }
           
           
        });
        
        //SubBolsas segun las bolsas seleccionadas
        array_value_subbolsa=new Array();
        $('#select_subbolsa').click(function(){
            var emisor=$('#emisor').val();
            var bolsa=$('#select_bolsa').val();
            var subbolsaLoaded=$('#subbolsaLoaded').val();
            if(bolsa==0){alert("Debe seleccionar una bolsa.");}else{
                var bolsas=new Array();
                bolsas=load_array('bolsas[]');
                
                $('#box_subbolsas_selection').html('Cargando datos.<img src="img/loading.gif" alt="" />');
                $.post("administracion.php?cmd=viewSubbolsaxZonalxBolsa", {bolsas: bolsas,emisor:emisor,array_subbolsas:array_value_subbolsa,subbolsaLoaded:subbolsaLoaded}, function(data){
                       $("#box_subbolsas_selection").empty();
                       $("#box_subbolsas_selection").append(data);

                       //Funciones para Seleccionar Subbolsas
                        showModalChecks('show_select_subbolsas','btn_listo_subbolsas');

                        $('#btn_listo_subbolsas').click(function(){
                        array_value_subbolsa=load_array('subbolsas[]');
                        verificaCheckModal('subbolsas[]','show_select_subbolsas','select_subbolsa',' item(s) seleccionado(s).','Debe seleccionar una Sub Bolsa.');
                        $('#subbolsaLoaded').attr('value','1');
                        
                    });

                         $('#chk_subbolsas_all').change(function(){marcarDesmarcarChks($(this),'chk_subbolsas');});
                         $('#btnCerrarSubbolsas').click(function(){cerrarVentana('show_select_subbolsas');});

                });
            }
            
        });
        
        //Puntos de Venta segun los canales seleccionados
        array_value_puntoventa=new Array();
        $('#select_puntoventa').click(function(){
            var emisor=$('#emisor').val();
            var canal=$('#select_canal').val();
            var puntoventaLoaded=$('#puntoventaLoaded').val();
            if(canal==0){alert("Debe seleccionar un canal.");}else{
                var puntoventas=new Array();
                puntoventas=load_array('puntoventas[]');
                
                $('#box_puntoventas_selection').html('Cargando datos.<img src="img/loading.gif" alt="" />');
                $.post("administracion.php?cmd=viewPuntoventaxCanal", {puntoventas: puntoventas,emisor:emisor,array_puntoventas:array_value_puntoventa,puntoventaLoaded:puntoventaLoaded}, function(data){
                       $("#box_puntoventas_selection").empty();
                       $("#box_puntoventas_selection").append(data);

                       //Funciones para Seleccionar Subbolsas
                        showModalChecks('show_select_puntoventas','btn_listo_puntoventas');

                        $('#btn_listo_puntoventas').click(function(){
                        array_value_puntoventa=load_array('puntoventas[]');
                        verificaCheckModal('puntoventas[]','show_select_puntoventas','select_puntoventa',' item(s) seleccionado(s).','Debe seleccionar un Punto de Venta.');
                        $('#puntoventaLoaded').attr('value','1');
                        
                    });

                         $('#chk_puntoventas_all').change(function(){marcarDesmarcarChks($(this),'chk_puntoventas');});
                         $('#btnCerrarPuntoventas').click(function(){cerrarVentana('show_select_puntoventas');});

                });
            }
            
        });
        
         $('.chk_modulos').change(function(){
                    var chk_name="chk_mod"+$(this).attr('value');
                    select_check($(this),chk_name);
         });

          checkingSelect=function(obj,id){
            var idchk=$('#'+obj.id);
            if(idchk.attr('checked')==true){
                $('#lblcheck'+id).css({
                    'font-weight':'bold',
                    'color':'#3465CF'
                });

                }else{
                $('#lblcheck'+id).css({
                    'font-weight':'100',
                    'color':'#00587B'
                });
                }
          }

         select_check=function(obj,chk_class){
                    if(obj.attr('checked')==true){
                            $('.'+chk_class).attr('checked',true);
                    }else{
                            $('.'+chk_class).attr('checked',false);
                         }
                }

         showDescripcionSubmodulo=function(titulo,descripcion){
             $('#box_descripcion').css('display','block');
             $('#tit_descripcion').html(titulo);
             $('#descripcion_texto').html(descripcion);
         }

         $('.row_submodulo').mouseout(function(){
            $('#box_descripcion').css('display','none');
            $('#tit_descripcion').html("");
            $('#descripcion_texto').html("");
         });
         
         checkingFunctionXarea=function(){
             var array_areas=new Array();
             array_areas=load_array('areas[]');
             var aseg_exist=false;//Atributos aseg. ventas
             var pai_exist=false;//Atributos pai
             var pai_canales_exist=false;//Atributos pai canales
             var i;
             for(i=0;i<array_areas.length;i++){
                 if(array_areas[i]==19){
                     aseg_exist=true;
                 }
                 if(array_areas[i]==4){
                     pai_exist=true;
                 }
                 if(array_areas[i]==21){
                     pai_canales_exist=true;
                 }
             }
             
             if(aseg_exist==true){
                 $('#func_add_area_aseg').attr('value',1);
                 $('#section_area_aseg').css('display','block');
             }
             else{
                 $('#func_add_area_aseg').attr('value',0);
                 $('#section_area_aseg').css('display','none');
             }
             
             if(pai_exist==true){
                 $('#func_add_area_pai').attr('value',1);
                 $('#section_area_pai').css('display','block');
             }
             else{
                 $('#func_add_area_pai').attr('value',0);
                 $('#section_area_pai').css('display','none');
             }
             
             if(pai_canales_exist==true){
                 $('#func_add_area_pai_canales').attr('value',1);
                 $('#section_area_pai_canales').css('display','block');
             }
             else{
                 $('#func_add_area_pai_canales').attr('value',0);
                 $('#section_area_pai_canales').css('display','none');
             }
             
             
             
         }
        
        $(".btn_toogle").click(function(){
           var valor=$(this).attr("for");
           $("#div_box_grupoapp_"+valor).toggle("slow");
        });

validarData=function(e){
var tecla = (document.all) ? e.keyCode : e.which;
if(tecla==17 || e.ctrlKey){return false;}
else{
if (tecla==8) return true;
var patron =/\w/;//valida solo letras y numeros
var te = String.fromCharCode(tecla);
return patron.test(te);
}
}

validarNumber=function(e){
var tecla = (document.all) ? e.keyCode : e.which;
if(tecla==17 || e.ctrlKey){return false;}
else{
if (tecla==8) return true;
var patron =/\d/;//valida solo numeros
var te = String.fromCharCode(tecla);
return patron.test(te);
}
}

                $("#opcion").attr("value", "1");

               verificaCheck=function(group){
               //var state = false;
               var cantidad=0;
               var tl=group.length;

               if(tl>0){
               for (var x=0; x < group.length; x++) {
                    if (group[x].checked) {
                    cantidad++;
                    }
               }
               }else{
                   if(group.checked){cantidad++}
               }

                return cantidad;
               }
load_array=function(element){
        var grupo=new Array();
        $("input[name='"+element+"']:checked").each(function(){grupo.push($(this).val());});
        return grupo;
}
    
                $('#btnVerSubmodulos').click(function(){
                var group = document.getElementsByName('grupo_app[]');
                var cantidad=verificaCheck(group);
                if(cantidad>0){
                 var grupoapp=new Array();
                 grupoapp=load_array('grupo_app[]');
                 var emisor=$('#emisor').val();
                 loader('start');
                    $("#childModal").html('');
                    $("#childModal").css("background-color","#FFFFFF");
                    $.post("administracion.php?cmd=viewSubmodulosXgrupoApp", {
                    grupo_app: grupoapp,
                    emisor: emisor
                    },
                    function(data){
                    loader('end');
                    $("#childModal").html(data);
                });

                 $("#childModal").dialog({modal:true, width:'500px', hide: 'slide', title: 'Submodulos', position:'top'});
                }else{
                    alert("Debe seleccionar un Grupo de Aplicacion.");
                }

             });

            

             $("#btn_guardar_agregar").click(function() {
                    $("#opcion").attr("value", "0");
                    pre_validacion();

            });
            
             $("#btn_grabar").click(function(){
                $("#opcion").attr("value", "1");
                pre_validacion();
            });

            pre_validacion=function(){
                    var user_creator=$('#user_creator').val();
                    var login=$('#login').val();
                    var empresa_del_usuario;
                    var area_del_usuario;
                    if(user_creator==1){
                    empresa_del_usuario=$('#empresa_usuario').attr('value');
                    area_del_usuario=$('#area_usuario').attr('value');
                    }

                    var emisor=$('#emisor').attr('value');
                    var nombres=$('#nombre').val();
                    var apepat=$('#apaterno').val();
                    var password=$('#password').val();
                    
                    var perfil=$('#perfil').val();
                    var opcion=$('#opcion').val();
                    var zonal_del_usuario=$("#zonal_usuario").attr("value");
                    
                    var apemat=$('#amaterno').val();
                    var fecha_nac=$('#fechanacimiento').val();
                    var dni=$('#dni').val();
                    var cip=$('#cip').val();
                    var celular=$('#celular').val();
                    var rpm=$('#rpm').val();
                    var correo=$('#correo').val();

                    //area para adm
                    var area_adm=$('#select_area_adm').val();

                    //Set data  funcionalidades adicionales
                    var func_aseg=$('#func_add_area_aseg').attr('value');
                    var func_pai=$('#func_add_area_pai').attr('value');
                    var func_pai_canales=$('#func_add_area_pai_canales').attr('value');
                    
                    var go_process=false;
                    if(user_creator==1){
                                if(empresa_del_usuario==0){
                                    alert("Seleccione la empresa del usuario.");
                                }else{
                                    if(area_del_usuario==0){
                                        alert("Seleccione el area de pertenencia del usuario.");
                                    }else{
                                        go_process=true;
                                    }
                                }
                            }else{
                                go_process=true;
                            }
                    if(go_process==true){
                    
                        if(nombres.length<3){alert("El nombre del usuario debe contener mas de 2 caracteres.");$('#nombre').focus();}else{
                        if(dni.length!=8){alert("El dni del usuario debe contener 8 caracteres.");$('#dni').focus();}else{ 
                            if(apepat.length<2){alert("El apellido paterno del usuario debe contener mas de 1 caracter.");$('apaterno').focus();}else{
                                if(login.length<4){alert("El login del usuario debe contener mas de 3 caracteres.");$('#login').focus();}else{
                                    if(password.length<6){alert("El password del usuario debe contener mas de 5 caracteres.");$('password').focus();}else{
                                                if(perfil==0){alert("Seleccione el tipo de Perfil del usuario.");$('#perfil').focus();}else{
                                                             var go_saving;
                                                               if(correo.length>0){
                                                                   go_saving=false;
                                                                    if(/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(correo))
                                                                        {
                                                                            go_saving=true;
                                                                        }
                                                                        else{
                                                                                alert("Correo Electronico ingresado es incorrecto");
                                                                                $('#correo').focus();
                                                                             }
                                                               }else{
                                                                   go_saving=true;
                                                               }
                                                                    
                                                                        if(go_saving==true){
                                                                        var group = document.getElementsByName('submodulos[]');
                                                                        var cantidad_permisos=verificaCheck(group);
                                                                        if(cantidad_permisos<1){alert("Seleccione los permisos para el usuario.");}else{
                                                                        //Declarando arrays
                                                                        var array_empresas=new Array();
                                                                        var array_areas=new Array();
                                                                        //var array_grupoapp=new Array();
                                                                        var array_zonales=new Array();
                                                                        var array_tipopedidos=new Array();
                                                                        var array_tiponegocios=new Array();
                                                                        var array_segmentos=new Array();
                                                                        var array_submodulos=new Array();
                                                                        var array_productos=new Array();
                                                                        var array_prioridades=new Array();
                                                                        var array_tipooperaciones=new Array();
                                                                        var array_motivodevoluciones=new Array();
                                                                        
                                                                        var array_mdfs=new Array();
                                                                        var array_nodos=new Array();
                                                                        var array_motivos=new Array();
                                                                        var array_capas=new Array();
                                                                        var array_subbolsas=new Array();
                                                                        var array_punto_venta=new Array();
                                                                        
                                                                        //ASEG
                                                                        if(func_aseg==1){
                                                                          array_productos=load_array('multiselect_slt_producto[]');
                                                                          array_subbolsas=load_array('multiselect_slt_subbolsa[]');
                                                                        }
                                                                        //PAI
                                                                        if(func_pai==1){
                                                                           array_prioridades=load_array('multiselect_slt_prioridad[]');
                                                                           array_tipooperaciones=load_array('multiselect_slt_tipooperacion[]');
                                                                           array_motivodevoluciones=load_array('multiselect_slt_motivodevolucion[]');
                                                                        }
                                                                      
                                                                        array_punto_venta=load_array('multiselect_slt_puntoventa[]');
                                                                        //capturando el array de los checkboxes
                                                                        array_empresas=load_array('multiselect_slt_empresa[]');
                                                                        array_areas=load_array('multiselect_select_area[]');
                                                                        //array_grupoapp=load_array('grupo_app[]');
                                                                        array_zonales=load_array('multiselect_slt_zonal[]');
                                                                        array_tipopedidos=load_array('multiselect_slt_pedido[]');
                                                                        array_tiponegocios=load_array('multiselect_slt_negocio[]');
                                                                        array_segmentos=load_array('multiselect_slt_segmento[]');
                                                                        array_mdfs=load_array('multiselect_slt_mdf[]');
                                                                        array_motivos=load_array('multiselect_slt_motivo[]');
                                                                        array_nodos=load_array('multiselect_slt_nodocatv[]');
                                                                        array_submodulos=load_array('submodulos[]');
               
                                                                        array_capas=load_array('multiselect_slt_capa[]'); 
                                                                        
                                                                        
                                                                        $("#box_message_loading").css("display","block");
                                                                        var respuesta;
                                                                        if(user_creator==1){
                                                                            $.post("administracion.php?cmd=regUsuario", {zonal_del_usuario:zonal_del_usuario,idarea_usuario:area_del_usuario,idempresa_usuario:empresa_del_usuario,nombres: nombres, apaterno: apepat, amaterno: apemat,dni:dni,cip:cip,fechaNacimiento:fecha_nac,celular:celular,rpm:rpm,correo:correo,login:login,password:password,idperfil:perfil, empresas: array_empresas, areas: array_areas, grupozonal: array_zonales, tipopedidos: array_tipopedidos, tiponegocios: array_tiponegocios,segmentos:array_segmentos,mdfs:array_mdfs,motivos:array_motivos, val_emisor:emisor,opcion:opcion, submodulos:array_submodulos,user_creator:user_creator,area_adm:area_adm,array_productos:array_productos,array_subbolsas:array_subbolsas,array_prioridades:array_prioridades,array_tipooperaciones:array_tipooperaciones,array_motivodevoluciones:array_motivodevoluciones,array_puntoventas:array_punto_venta,array_nodos:array_nodos,array_capas:array_capas}, function(data){
                                                                            $("#box_message_loading").empty();
                                                                            respuesta=data;
                                                                            if(respuesta==1){
                                                                                $('#box_message_loading').css({
                                                                                   'border':'1px solid #CD0A0A',
                                                                                   'background':'#FEF2EE',
                                                                                   'color':'#CD0A0A'
                                                                                });
                                                                                $('#box_message_loading').css('display','block');
                                                                                $('#box_message_loading').html('El Login ya existe. <img src="img/aviso_small.png" alt="" />');
                                                                                $('#login').focus();
                                                                                setTimeout("clearDataUser()",3000);
                                                                            }else{
                                                                                if(respuesta==2){
                                                                                    $('#box_message_loading').css({
                                                                                           'border':'1px solid #FFB100',
                                                                                           'background':'#FEFBBA',
                                                                                           'color':'#1C85DA'
                                                                                        });
                                                                                    $('#box_message_loading').css('display','block');
                                                                                    
                                                                                    $('#box_message_loading').html('El usuario fue registrado.<img src="img/accept.png" alt="" />');
                                                                                    if(opcion==0){
                                                                                        setTimeout("clearDataUser()",3000);
                                                                                        window.location="administracion.php?cmd=verFormUsuarioInsert";
                                                                                    }else{
                                                                                        setTimeout("clearDataUser()",2000);
                                                                                        window.location="administracion.php?cmd=listUsuario";
                                                                                    }

                                                                                }else{
                                                                                    if(respuesta==3){
                                                                                        $('#box_message_loading').css({
                                                                                           'border':'1px solid #CD0A0A',
                                                                                           'background':'#FEF2EE',
                                                                                           'color':'#CD0A0A'
                                                                                        });
                                                                                        $('#box_message_loading').css('display','block');

                                                                                        $('#box_message_loading').html('El DNI ya existe. <img src="img/aviso_small.png" alt="" />');
                                                                                        $('#dni').focus();
                                                                                        setTimeout("clearDataUser()",3000);
                                                                                    }else{
                                                                                        if(respuesta==4){
                                                                                            $('#box_message_loading').css({
                                                                                           'border':'1px solid #CD0A0A',
                                                                                           'background':'#FEF2EE',
                                                                                           'color':'#CD0A0A'
                                                                                        });
                                                                                        $('#box_message_loading').css('display','block');
                                                                                            $('#box_message_loading').html('El Correo Electronico ya existe. <img src="img/aviso_small.png" alt="" />');
                                                                                            $('#correo').focus();
                                                                                            setTimeout("clearDataUser()",3000);
                                                                                        }else{
                                                                                            $('#box_message_loading').css({
                                                                                           'border':'1px solid #CD0A0A',
                                                                                           'background':'#FEF2EE',
                                                                                           'color':'#CD0A0A'
                                                                                            });
                                                                                            $('#box_message_loading').css('display','block');
                                                                                            $('#box_message_loading').html('Ocurrio un error, contactese con el Administrador');
                                                                                        }
                                                                                    }
                                                                                    
                                                                                }
                                                                            }
                                                                         
                                                                        });
                                                                        }else{
                                                                            if(user_creator==2){
                                                                                
                                                                            $.post("administracion.php?cmd=regUsuarioByAdm", {zonal_del_usuario:zonal_del_usuario,nombres: nombres, apaterno: apepat, amaterno: apemat,dni:dni,cip:cip,fechaNacimiento:fecha_nac,celular:celular,rpm:rpm,correo:correo,login:login,password:password,idperfil:perfil, empresas: array_empresas, areas: array_areas, grupozonal: array_zonales, tipopedidos: array_tipopedidos, tiponegocios: array_tiponegocios,segmentos:array_segmentos,mdfs:array_mdfs,motivos:array_motivos, val_emisor:emisor,opcion:opcion, submodulos:array_submodulos,user_creator:user_creator,area_adm:area_adm,array_productos:array_productos,array_subbolsas:array_subbolsas,array_prioridades:array_prioridades,array_tipooperaciones:array_tipooperaciones,array_motivodevoluciones:array_motivodevoluciones,array_puntoventas:array_punto_venta,array_nodos:array_nodos,array_capas:array_capas}, function(data){
                                                                            $("#box_message_loading").empty();
                                                                            respuesta=data;
                                                                            //respuesta_texto=data;
                                                                            //$('#msg').html(respuesta);
                                                                           
                                                                            if(respuesta==1){
                                                                                $('#box_message_loading').css({
                                                                                   'border':'1px solid #CD0A0A',
                                                                                   'background':'#FEF2EE',
                                                                                   'color':'#CD0A0A'
                                                                                });
                                                                                $('#box_message_loading').css('display','block');
                                                                                $('#box_message_loading').html('El Login ya existe. <img src="img/aviso_small.png" alt="" />');
                                                                                $('#login').focus();
                                                                                setTimeout("clearDataUser()",3000);
                                                                            }else{
                                                                                if(respuesta==2){
                                                                                    $('#box_message_loading').css({
                                                                                           'border':'1px solid #FFB100',
                                                                                           'background':'#FEFBBA',
                                                                                           'color':'#1C85DA'
                                                                                        });
                                                                                    $('#box_message_loading').css('display','block');
                                                                                    
                                                                                    $('#box_message_loading').html('El usuario fue registrado.<img src="img/accept.png" alt="" />');
                                                                                    if(opcion==0){
                                                                                        setTimeout("clearDataUser()",3000);
                                                                                        window.location="administracion.php?cmd=verFormUsuarioInsert";
                                                                                    }else{
                                                                                        setTimeout("clearDataUser()",2000);
                                                                                        window.location="administracion.php?cmd=listUsuario";
                                                                                    }

                                                                                }else{
                                                                                    if(respuesta==3){
                                                                                        $('#box_message_loading').css({
                                                                                           'border':'1px solid #CD0A0A',
                                                                                           'background':'#FEF2EE',
                                                                                           'color':'#CD0A0A'
                                                                                        });
                                                                                        $('#box_message_loading').css('display','block');

                                                                                        $('#box_message_loading').html('El DNI ya existe. <img src="img/aviso_small.png" alt="" />');
                                                                                        $('#dni').focus();
                                                                                        setTimeout("clearDataUser()",3000);
                                                                                    }else{
                                                                                        if(respuesta==4){
                                                                                            $('#box_message_loading').css({
                                                                                           'border':'1px solid #CD0A0A',
                                                                                           'background':'#FEF2EE',
                                                                                           'color':'#CD0A0A'
                                                                                        });
                                                                                        $('#box_message_loading').css('display','block');
                                                                                            $('#box_message_loading').html('El Correo Electronico ya existe. <img src="img/aviso_small.png" alt="" />');
                                                                                            $('#correo').focus();
                                                                                            setTimeout("clearDataUser()",3000);
                                                                                        }else{
                                                                                            $('#box_message_loading').css({
                                                                                           'border':'1px solid #CD0A0A',
                                                                                           'background':'#FEF2EE',
                                                                                           'color':'#CD0A0A'
                                                                                            });
                                                                                            $('#box_message_loading').css('display','block');
                                                                                            $('#box_message_loading').html('Ocurrio un error, contactese con el Administrador');
                                                                                        }
                                                                                    }
                                                                                    
                                                                                }
                                                                            }
                                                                           
                                                                            
                                                                            //$("#msg").append(data);
                                                                        });
                                                                                
                                                                            }
                                                                        }
                                                                        
                                                                        
                                                                      }
                                                                     }
                                                                    }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                        
                    
                    
                 }

            //aqui termina validacion

            }

    clearDataUser=function(){
        $('#box_message_loading').empty();
        $('#box_message_loading').css('display','none');
    }

    clearCbos=function(){
        $('#select_area').empty();
        $('#select_area').html('<option value="0" selected>Seleccione Area</option>');

        $('#select_zonal').empty();
        $('#select_zonal').html('<option value="0" selected>Seleccione Zonal</option>');

        $('#select_pedido').empty();
        $('#select_pedido').html('<option value="0" selected>Seleccione Tipo de Pedido</option>');

        $('#select_negocio').empty();
        $('#select_negocio').html('<option value="0" selected>Seleccione Tipo de Negocio</option>');

        $('#select_segmento').empty();
        $('#select_segmento').html('<option value="0" selected>Seleccione Segmento</option>');
        
        $('#select_mdf').empty();
        $('#select_mdf').html('<option value="0" selected>Seleccione MDF</option>');

        $('#select_motivo').empty();
        $('#select_motivo').html('<option value="0" selected>Seleccione Motivo</option>');

        $('.row_submodulo').css({
            'font-weight':'100',
            'color':'#00587B'
        });

    }

    $(".trigger").mouseover(function(){
		$(".panel").toggle("fast");
		$(this).toggleClass("active");
		return false;
    });
    $('.panel').mouseleave(function(){
       $(".panel").toggle("fast");
       $('.trigger').toggleClass("active");
    });

//Ordenamiento

$('.link_hd').click(function(){
    
    var valor=$(this).attr('rel');
    var emisor=$('#emisor').attr('value');
    var total=$('#tl_registros').val();
    var direction_order=$('#direction_order_'+valor).attr('value');
    var pagina_actual=$('#pag_actual').attr('value');
    var filtro_empresa=$('#filtro_busqueda_empresa').val();
    var filtro_area=$('#filtro_busqueda_area').val();

    if(direction_order=="none"){
        $('#direction_order_'+valor).attr('value','asc');
        $('#img_order_'+valor).attr('src','img/desc.gif');
        $(this).attr('title','Ordenar en forma ascendente.');
    }

        if(direction_order=="asc"){
        
        $('#rel_order_actual').attr('value',valor);
        $('#direction_order_actual').attr('value','asc');
        loader("start");
        $.post("administracion.php?cmd=listarUsuarioOrdenado", {
        valor_campo: valor,
        direction:'asc',
        total:total,
        emisor:emisor,
        filtro_empresa:filtro_empresa,
        filtro_area:filtro_area
        },
        function(data){
            loader("end");
            $("#tb_resultado").empty();
            $("#tb_resultado").append(data);
        });

        $('#img_order_'+valor).attr('src','img/asc.gif');
        $('#direction_order_'+valor).attr('value','desc');
        $(this).attr('title','Ordenar en forma descendente.');
    }
    
    if(direction_order=="desc"){

        $('#rel_order_actual').attr('value',valor);
        $('#direction_order_actual').attr('value','desc');
        loader("start");
        $.post("administracion.php?cmd=listarUsuarioOrdenado", {
        valor_campo: valor,
        direction:'desc',
        total:total,
        emisor:emisor,
        filtro_empresa:filtro_empresa,
        filtro_area:filtro_area
        },
        function(data){
            loader("end");
            $("#tb_resultado").empty();
            $("#tb_resultado").append(data);
        });

        $('#img_order_'+valor).attr('src','img/desc.gif');
        $('#direction_order_'+valor).attr('value','asc');
        $(this).attr('title','Ordenar en forma ascendente.');
    }
});

});

/* FUNCIONES PARA EL LISTADO DE USUARIOS */
function editUser(codigo)
{
    loader('start');
    $("#childModal").html('');
    $("#childModal").css("background-color","#FFFFFF");
    $.post("administracion.php?cmd=editarUsuario", {
        idusuario: codigo
    },
    function(data){
        loader('end');
        $("#childModal").html(data);
    });

    $("#childModal").dialog({modal:true, width:'900px', hide: 'slide', title: 'Editar Usuario', position:'top',resizable: false,draggable: false});
    
}
function change_state(idusuario,estado,nom_user){
    //true para desabilitar
    if(confirm("Esta seguro de cambiar el estado de : "+nom_user)){
    loader('start');
    var emisor=$('#emisor').attr('value');
    var respuesta=0;
    var estado_inicial=estado;
    $.post("administracion.php?cmd=execUpdateStateUsuario", {id_usuario: idusuario, state: estado, val_emisor:emisor}, function(data){
          loader('end');
          respuesta=data;

          if(respuesta==1){
              if(estado_inicial==1){
                  var nvo_estado=0;
                  $('#alink_state_'+idusuario).attr('href','javascript:change_state(\''+idusuario+'\',\''+nvo_estado+'\',\''+nom_user+'\')');
                  $('#img_state_'+idusuario).attr('src','img/estado_deshabilitado.png');
                  $('#img_state_'+idusuario).attr('alt','Cambiar estado a Habilitado');
              }
              else{
                  var nvo_estado=1;
                  $('#alink_state_'+idusuario).attr('href','javascript:change_state(\''+idusuario+'\',\''+nvo_estado+'\',\''+nom_user+'\')');
                  $('#img_state_'+idusuario).attr('src','img/estado_habilitado.png');
                  $('#img_state_'+idusuario).attr('alt','Cambiar estado a Deshabilitado');
              }

          }
    });
    }
}

function change_password(idusuario){
    var emisor=$('#emisor').attr('value');
    loader('start');
    $("#childModal").html('');
    $("#childModal").css("background-color","#FFFFFF");
    $.post("administracion.php?cmd=generarPassUsuario", {
        idusuario: idusuario,
        emisor:emisor
    },
    function(data){
        loader('end');
        $("#childModal").html(data);
    });

    $("#childModal").dialog({modal:true, width:'500px', hide: 'slide', title: 'Generar Contrase&ntilde;a', position:'middle',resizable: false,draggable: false});
}

function infoUser(idusuario){
   
    var emisor=$('#emisor').attr('value');
    loader('start');
    $("#childModal").html('');
    $("#childModal").css("background-color","#FFFFFF");
    $.post("administracion.php?cmd=viewInfoUsuario", {
        idusuario: idusuario,
        emisor:emisor
    },
    function(data){
        loader('end');
        $("#childModal").html(data);
    });

    $("#childModal").dialog({modal:true, width:'900px', hide: 'slide', title: 'Informaci&oacute;n del Usuario', position:'top',resizable: false,draggable: false});
}
function randompass(length){
var letras = new Array();
letras[1]="a";
letras[2]="b";
letras[3]="c";
letras[4]="d";
letras[5]="e";
letras[6]="f";
letras[7]="g";
letras[8]="h";
letras[9]="i";
letras[10]="j";
letras[11]="k";
letras[12]="l";
letras[13]="m";
letras[14]="n";
letras[15]="o";
letras[16]="p";
letras[17]="q";
letras[18]="r";
letras[19]="s";
letras[20]="t";
letras[21]="u";
letras[22]="w";
var pass = "";
var numero = 0;
for(var i=1;i<length;i++){
    numero = Math.floor(Math.random()*(22-1))+1;
    if(i==1){
        pass+=numero;
    }else{
        pass+=letras[numero];
    }        
}
numero = Math.floor(Math.random()*(22-1))+1;
pass+=numero;
return pass;
}
function CloneUser(idusuario){
    var emisor=$('#emisor').attr('value');
    loader('start');
    $("#childModal").html('');
    $("#childModal").css("background-color","#FFFFFF");
    $.post("administracion.php?cmd=CloneInfoUsuario", {
        idusuario: idusuario,
        emisor:emisor
    },
    function(data){
        loader('end');
        $("#childModal").html(data);
        $('#pass').attr('value',randompass(6));
    });

    $("#childModal").dialog({modal:true, width:'900px', hide: 'slide', title: 'Heredar Atributos del Usuario', position:'top',resizable: false,draggable: false});
    
   
}


