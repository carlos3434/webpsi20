/* 
 * Proyecto Web Unificada TDP
 * A.P. Omar Calderon Evangelista ocalderon@gmd.com.pe
 */
$(document).ready(function(){
    infoCelulas=function(idcelula){
        loader('start');
        $("#childModal").html('');
        $("#childModal").css("background-color","#FFFFFF");
        $.post("administracion_eecc.php?cmd=viewInfoCelula", {
            idcelula: idcelula        
        },
        function(data){
            loader('end');
            $("#childModal").html(data);
        });
        $("#childModal").dialog({
            modal:true, 
            width:'720px', 
            hide: 'slide', 
            title: 'Informaci&oacute;n de la C&eacutelula;', 
            position:'top'
        });    
    }
   
    change_state=function(idcelula, estado, nom_celula){
        if(confirm("Esta seguro de cambiar el estado de la Celula: "+nom_celula)){
            loader('start');
            var estado_inicial=estado;
            $.post("administracion_eecc.php?cmd=UpdateStateCelula",{
                idcelula: idcelula,
                estado  : estado
            }, function(data){
                loader('end');
                if(data==1){
                    //alert(estado_inicial);
                    if(estado_inicial==1){
                        var nvo_estado=0;
                        $('#alink_state_'+idcelula).attr('href','javascript:change_state(\''+idcelula+'\',\''+nvo_estado+'\',\''+nom_celula+'\')');
                        $('#img_state_'+idcelula).attr('src','img/estado_deshabilitado.png');
                        $('#img_state_'+idcelula).attr('alt','Cambiar estado a Habilitado');
                        $('#img_state_'+idcelula).attr('title','Cambiar estado a Habilitado');
                    }else{
                        nvo_estado=1;
                        $('#alink_state_'+idcelula).attr('href','javascript:change_state(\''+idcelula+'\',\''+nvo_estado+'\',\''+nom_celula+'\')');
                        $('#img_state_'+idcelula).attr('src','img/estado_habilitado.png');
                        $('#img_state_'+idcelula).attr('alt','Cambiar estado a Deshabilitado');
                        $('#img_state_'+idcelula).attr('title','Cambiar estado a Deshabilitado');
                    }
                }
            });
        }
    }
    
    addTecnicosACelulas=function(idcelula,nomcelula,idempresa,abv_zonal){
        
        loader('start');
        $("#childModal").html('');
        $("#childModal").css("background-color","#FFFFFF");
        
        
        $.post("administracion_eecc.php?cmd=AddTecnicosACelulas",{
            idcelula    : idcelula,
            nomcelula   : nomcelula,
            idempresa   : idempresa,
            abv_zonal   : abv_zonal
        },function(data){
            loader('end');
            $("#childModal").html(data);
        });
        $("#childModal").dialog({
            modal: true, 
            width:'880px', 
            hide: 'slide', 
            title: 'Asignacion de Tecnicos a Celulas', 
            position:'top'
        });    
    }
    
    
    formAddCelulasASupervisores=function(idcelula,nomcelula,idempresa,idzonal){
        loader('start');
        $("#childModal").html('');
        $("#childModal").css("background-color","#FFFFFF");
        $.post("administracion_eecc.php?cmd=formAddCelulasASupervisores",{
            idcelula    : idcelula,
            nomcelula   : nomcelula,
            idempresa   : idempresa,
            idzonal     : idzonal
        },function(data){
            loader('end');
            $("#childModal").html(data);
        });
        $("#childModal").dialog({
            modal: true, 
            width:'880px', 
            hide: 'slide', 
            title: 'Asignacion de Supervisores a Celulas', 
            position:'top'
        });
    }
    formAddCelulasACoordinadores=function(idcelula,nomcelula,idempresa,idzonal){
        loader('start');
        $("#childModal").html('');
        $("#childModal").css("background-color","#FFFFFF");
        $.post("administracion_eecc.php?cmd=formAddCelulasACoordinadores",{
            idcelula    : idcelula,
            nomcelula   : nomcelula,
            idempresa   : idempresa,
            idzonal     : idzonal
        },function(data){
            loader('end');
            $("#childModal").html(data);
        });
        $("#childModal").dialog({
            modal: true, 
            width:'600px', 
            hide: 'slide', 
            title: 'Asignacion de Coordinadores a Celulas', 
            position:'top'
        });    
    }
    
    //Agregado wsandoval 01/02/2011
    formEditarPoligonoCelula = function(idcelula, nomcelula){
        loader('start');
        $("#childModal").html('Cargando...');
        $("#childModal").css("background-color","#FFFFFF");
        $.post("administracion_eecc.php?cmd=vistaEditarPoligonoCelula",{
            idcelula    : idcelula,
            nomcelula   : nomcelula
        },function(data){
            loader('end');
            $("#childModal").html(data);
        });
        $("#childModal").dialog({
            modal: true, 
            width:'80%', 
            hide: 'slide', 
            title: 'CREAR/EDITAR POLIGONOS PARA LA CELULA: ' + nomcelula, 
            position:'top',
            close:function(){
                return false;
            }
        });    
    }

    editArea=function(id_area){    
        var emisor=$('#emisor').attr('value');
        loader('start');
        $("#childModal").html('');
        $("#childModal").css("background-color","#FFFFFF");
        $.post("administracion.php?cmd=editArea", {
            id_area: id_area,
            emisor:emisor
        },
        function(data){
            loader('end');
            $("#childModal").html(data);
        });
        $("#childModal").dialog({
            modal:true, 
            width:'500px', 
            hide: 'slide', 
            title: 'Editar Area', 
            position:'top'
        });
    }

        
});

