/* 
 * Sistema Web Unificada TDP
 * Planificacion y Soluciones Informaticas STC Telefonica
 * GMD S.A.
 * Autor: Gonzalo Chacaltana Buleje <gchacaltana@gmd.com.pe>
 */
$(document).ready(function(){
   $(".trigger").mouseover(function(){
		$(".panel").toggle("fast");
		$(this).toggleClass("active");
		return false;
    });
    $('.panel').mouseleave(function(){
       $(".panel").toggle("fast");
       $('.trigger').toggleClass("active");
    });
    //Fin Menu
    
    infoEstadoGestion=function(idestado_gestion){
    var emisor=$('#emisor').attr('value');
    loader('start');
    $("#childModal").html('');
    $("#childModal").css("background-color","#FFFFFF");
    $.post("administracion.php?cmd=viewInfoEstadoGestion", {
        idestado_gestion: idestado_gestion,
        emisor:emisor
    },
    function(data){
        loader('end');
        $("#childModal").html(data);
    });

    $("#childModal").dialog({modal:true, width:'520px', hide: 'slide', title: 'Informaci&oacute;n del Estado Gesti&oacute;n', position:'middle'});
    
  }
   
   editEstadoGestion=function(idestado_gestion){
    
    var emisor=$('#emisor').attr('value');
    loader('start');
    $("#childModal").html('');
    $("#childModal").css("background-color","#FFFFFF");
    $.post("administracion.php?cmd=editEstadoGestion", {
        idestado_gestion: idestado_gestion,
        emisor:emisor
    },
    function(data){
        loader('end');
        $("#childModal").html(data);
    });

    $("#childModal").dialog({modal:true, width:'700px', hide: 'slide', title: 'Editar Estado Gesti&oacute;n', position:'top'});

   }
   
    
});
