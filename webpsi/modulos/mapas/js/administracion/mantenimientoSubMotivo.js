/* 
 * Proyecto Web Unificada TDP
 * GMD S.A
 * A.P. Gonzalo Chacaltana Buleje <gchacaltana@gmd.com.pe>
 */
$(document).ready(function(){
   $(".trigger").mouseover(function(){
		$(".panel").toggle("fast");
		$(this).toggleClass("active");
		return false;
    });
    $('.panel').mouseleave(function(){
       $(".panel").toggle("fast");
       $('.trigger').toggleClass("active");
    });
    //Fin Menu

   infoSubMotivo=function(idSubMotivo){
    var emisor=$('#emisor').attr('value');
    loader('start');
    $("#childModal").html('');
    $("#childModal").css("background-color","#FFFFFF");
    $.post("administracion.php?cmd=viewInfoSubMotivo", {
        idSubMotivo: idSubMotivo,
        emisor:emisor
    },
    function(data){
        loader('end');
        $("#childModal").html(data);
    });
   $("#childModal").dialog({modal:true, width:'720px', hide: 'slide', title: 'Informaci&oacute;n del SubMotivo', position:'top'});
   }
   

   change_state=function(id_SubMotivo,estado,nom_SubMotivo){
        //true para desabilitar
        if(confirm("Esta seguro de cambiar el estado del SubMotivo : "+nom_SubMotivo)){
            loader('start');
    var emisor=$('#emisor').attr('value');
    var respuesta=0;
    var estado_inicial=estado;
    $.post("administracion.php?cmd=execUpdateStateSubMotivo", {id_SubMotivo: id_SubMotivo, state: estado, emisor:emisor}, function(data){
          loader('end');
          respuesta=data;

          if(respuesta==1){
              if(estado_inicial==1){
                  var nvo_estado=0;
                  $('#alink_state_'+id_SubMotivo).attr('href','javascript:change_state(\''+id_SubMotivo+'\',\''+nvo_estado+'\',\''+nom_SubMotivo+'\')');
                  $('#img_state_'+id_SubMotivo).attr('src','img/estado_deshabilitado.png');
                  $('#img_state_'+id_SubMotivo).attr('alt','Cambiar estado a Habilitado');
                  $('#img_state_'+id_SubMotivo).attr('title','Cambiar estado a Habilitado');
              }
              else{
                   nvo_estado=1;
                  $('#alink_state_'+id_SubMotivo).attr('href','javascript:change_state(\''+id_SubMotivo+'\',\''+nvo_estado+'\',\''+nom_SubMotivo+'\')');
                  $('#img_state_'+id_SubMotivo).attr('src','img/estado_habilitado.png');
                  $('#img_state_'+id_SubMotivo).attr('alt','Cambiar estado a Deshabilitado');
                  $('#img_state_'+id_SubMotivo).attr('title','Cambiar estado a Deshabilitado');
              }

          }
    });
    }
   }

   editSubMotivo=function(id_SubMotivo){
    
    var emisor=$('#emisor').attr('value');
    loader('start');
    $("#childModal").html('');
    $("#childModal").css("background-color","#FFFFFF");
    $.post("administracion.php?cmd=editSubMotivo", {
        id_SubMotivo: id_SubMotivo,
        emisor:emisor
    },
    function(data){
        loader('end');
        $("#childModal").html(data);
    });

    $("#childModal").dialog({modal:true, width:'560px', hide: 'slide', title: 'Editar SubMotivo', position:'top'});

   }

});

