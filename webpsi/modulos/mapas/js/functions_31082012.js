$(document).ready(function(){

        
    provinciasUbigeo =  function() {
        
        var coddpto = $("#departamento").attr('value');
        
        $("select[id=provincia]").html("<option value=''>Seleccione</option>");
        $("select[id=distrito]").html("<option value=''>Seleccione</option>");

        data_content = {
            'coddpto'   : coddpto
        };
        $.ajax({
            type:   "POST",
            url:    "modulos/maps/util/functions.php?cmd=provinciasUbigeo",
            data:   data_content,
            dataType: "json",
            success: function(data) {
                $.each(data, function(i, item) {
                    $("select[id=provincia]").append("<option value='"+data[i]['codProv']+"'>"+data[i]['nombre']+"</option>");
                });
            }
        }); 
    }
    
    distritosUbigeo =  function() {
        
        var coddpto = $("#departamento").attr('value');
        var codprov = $("#provincia").attr('value');
        
        $("select[id=distrito]").html("<option value=''>Seleccione</option>");

        data_content = {
            'coddpto'   : coddpto,
            'codprov'   : codprov
        };
        $.ajax({
            type:   "POST",
            url:    "modulos/maps/util/functions.php?cmd=distritosUbigeo",
            data:   data_content,
            dataType: "json",
            success: function(data) {
                $.each(data, function(i, item) {
                    $("select[id=distrito]").append("<option value='"+data[i]['codDist']+"'>"+data[i]['nombre']+"</option>");
                });
            }
        }); 
    }
        
    obtenerMdfsZonalUsuario = function() {
        $("select[id=mdf]").html("<option value=''>Seleccione</option>");
        $("#campo_valor").attr('value','');
        
        var zonal = $("#zonal").attr('value');
        if(zonal == "") {
            return false;
        }

        data_content = {
            'zonal' : zonal
        };
        $.ajax({
            type:   "POST",
            url:    "modulos/maps/util/functions.php?cmd=obtenerMdfsZonalUsuario",
            data:   data_content,
            dataType: "json",
            success: function(data) {
                $.each(data, function(i, item) {
                    $("select[id=mdf]").append("<option value='"+data[i]['mdf']+"'>"+ data[i]['mdf'] + ' - ' +data[i]['nombre']+"</option>");
                });
            }
        });
    }
    
});