var pr = null;
var order_field = '';
var order_type = '';
$(document).ready(function(){

    $("#cancelar_ajax").click(function(){
        pr.abort();
        loader("end");
    });

    $("#btn_buscar").click(function() {
        order_field = '';
        order_type = '';
        $('#tb_resultado thead tr.headFilter a').removeClass("hd_orden_asc hd_orden_desc");
        
        D.BuscarEdificios();
    });

    $("#campo_valor").keyup(function(e) {
        if(e.keyCode == 13) {
            order_field = '';
            order_type = '';
            $('#tb_resultado thead tr.headFilter a').removeClass("hd_orden_asc hd_orden_desc");
            
            D.BuscarEdificios();
        }
    });
    
    $('#tb_resultado thead tr.headFilter a').click(function(event) {
        order_field  = $(this).attr("orderField");
        order_type   = $(this).attr("orderType");
        
        $(this).attr("orderType","desc");
        if( order_type == 'desc' ) {
            $(this).attr("orderType","asc");
        }
        
        D.BuscarEdificios();
    });
    
    $('#tb_resultado thead tr.headFilter a').toggle(
        function() {
            $('#tb_resultado thead tr.headFilter a').removeClass("hd_orden_asc");
            $(this).addClass("hd_orden_desc"); 
        }, function () {
            $('#tb_resultado thead tr.headFilter a').removeClass("hd_orden_desc");
            $(this).addClass("hd_orden_asc");
        }
    );
    
    viewCambiarEstado = function(idedificio) {
        $('.spn_estado').show();
        $('.chg_estado').hide();

        $('#spn_estado_' + idedificio).hide();
        $('#chg_estado_' + idedificio).show();
    }
    
    //Cambiar estado del edificio
    cambiarEstado = function(idedificio) {
        var estado = $("#chg_estado_" + idedificio).val();
        
        D.CambiarEstado(idedificio, estado);
    }
    
    fotosEdificio = function(idEdificio) {
        D.FotosEdificio(idEdificio);
    }
    
    listadoFotosEdificio = function(idEdificio) {
        D.ListadoFotosEdificio(idEdificio);
    }
    
    verFotoDetalle = function(idarmarios_foto) {
        D.VerFotoDetalle(idarmarios_foto);
    }
    
    buscarEdificios = function() {
        D.BuscarEdificios();
    }
    
    detalleEdificio = function(idedificio) {
        D.DetalleEdificio(idedificio);
    }
    
    detalleEdificioParent = function(idedificio) {
        D.DetalleEdificioParent(idedificio);
    }
    
    editarEdificio = function(idedificio) {
        D.EditarEdificio(idedificio);
    }
    
    editarEdificioParent = function(idedificio) {
        D.EditarEdificioParent(idedificio);
    }

    nuevoEdificio = function() {
        D.NuevoEdificio();
    }
    
    updateDatosEdificio = function() {
        D.UpdateDatosEdificio();
    }
    
    insertDatosEdificio = function() {
        D.InsertDatosEdificio();
    }
    
    descargarEdificios = function() {
        if( confirm("Seguro de descargar el reporte?") ) {
            $('#msgDescarga').show();
            $('body').append('<iframe src="modulos/maps/bandeja.control.edificio.rpt.php" id="ifrReporteEdificio" onload="parent.cierraDescargaEdificio();"></iframe>');
        }
    }

    cierraDescargaEdificio = function() {
        $('ifrReporteEdificio').remove();
        $('#msgDescarga').hide();
    }
    
    verMas = function(mtgespkarm) {
        $("#verMas_" + mtgespkarm).hide();
        $("#ocultarMas_" + mtgespkarm).show();
        $("#mas_" + mtgespkarm).show();
    }
    ocultarMas = function(mtgespkarm) {
        $("#verMas_" + mtgespkarm).show();
        $("#ocultarMas_" + mtgespkarm).hide();
        $("#mas_" + mtgespkarm).hide();
    }

    cerrarVentana = function() {
        $("#childModal").dialog('close');
    }
    
    obtenerZonalesPorRegionUsuario = function() {
    	$("select[id=zonal]").html("<option value=''>Seleccione</option>");
        $("select[id=mdf]").html("<option value=''>Seleccione</option>");
        $("select[id=departamento]").html("<option value=''>Seleccione</option>");
        $("select[id=provincia]").html("<option value=''>Seleccione</option>");
        $("select[id=distrito]").html("<option value=''>Seleccione</option>");
        $("#campo_valor").attr('value','');
        
        var region = $("#region").attr('value');
        if(region == "") {
            return false;
        }

        data_content = {
            'region' : region
        };
        $.ajax({
            type:   "POST",
            url:    "modulos/maps/bandeja.control.edificio.php?cmd=obtenerZonalesPorRegionUsuario",
            data:   data_content,
            dataType: "json",
            success: function(data) {
            	var arrZonal = data['arrZonal'];
            	if ( arrZonal.length > 0 ) {
            		$.each(arrZonal, function(i, item) {
                        $("select[id=zonal]").append("<option value='"+arrZonal[i]['zonal']+"'>"+ arrZonal[i]['zonal'] + ' - ' +arrZonal[i]['nombre']+"</option>");
                    });
            	}
            }
        });
        
    }
    
    listarZonalesPorRegionUsuario = function() {
    	$("select[id=cmb_zonal]").html("<option value=''>Seleccione</option>");
        $("select[id=cmb_ura]").html("<option value=''>Seleccione</option>");
        //$("select[id=departamento]").html("<option value=''>Seleccione</option>");
        //$("select[id=provincia]").html("<option value=''>Seleccione</option>");
        //$("select[id=distrito]").html("<option value=''>Seleccione</option>");
        
        var region = $("#cmb_region").attr('value');
        if(region == "") {
            return false;
        }

        data_content = {
            'region' : region
        };
        $.ajax({
            type:   "POST",
            url:    "modulos/maps/bandeja.control.edificio.php?cmd=obtenerZonalesPorRegionUsuario",
            data:   data_content,
            dataType: "json",
            success: function(data) {
            	var arrZonal = data['arrZonal'];
            	if ( arrZonal.length > 0 ) {
            		$.each(arrZonal, function(i, item) {
                        $("select[id=cmb_zonal]").append("<option value='"+arrZonal[i]['zonal']+"'>"+ arrZonal[i]['zonal'] + ' - ' +arrZonal[i]['nombre']+"</option>");
                    });
            	}
            }
        });
    }

    var D = {

        BuscarEdificios: function() {
            var gestionObra = $("#gestionObra").val();
            var empresa		= $("#empresa").val();
            var region      = $("#region").val();
            var zonal       = $("#zonal").val();
            var mdf         = $("#mdf").val();
            var campo_valor = $.trim($("#campo_valor").val());

            var departamento    = $("#departamento").val();
            var provincia       = $("#provincia").val();
            var distrito        = $("#distrito").val();

            var page    = $("#pag_actual").val();
            var tl      = $("#tl_registros").val();
            
            if ( region == '' ) {
            	alert("Debe seleccionar una region");
            	return false;
            }
            
            var data_content = { 
                pagina      : page, 
                total       : tl, 
                gestionObra : gestionObra,
                empresa     : empresa,
                region		: region,
                zonal       : zonal, 
                mdf         : mdf, 
                departamento: departamento,
                provincia   : provincia,
                distrito    : distrito,
                campo_valor : campo_valor,
                order_field : order_field,
                order_type  : order_type
            };

            loader("start");
            pr = $.post("modulos/maps/bandeja.control.edificio.php?cmd=filtroBusqueda", data_content, function(data){
            	$("#msgEdificios").hide();
                $("#tb_sup_tl_registros").show();
                $("#tb_pie_tl_registros").show();
                
                $("#tb_resultado").show();
                $("#tb_resultado tbody").empty();
                $("#tb_resultado tbody").append(data);
                loader("end");
            });
        },
        
        CambiarEstado: function(idedificio, estado) {
            var data_content = {
                idedificio  : idedificio,
                estado      : estado
            }
            
            pr = $.post("modulos/maps/bandeja.control.edificio.php?cmd=cambiarEstado", data_content, function(data){
                if(data['flag'] == '1'){
                    //refresca la bandeja
                    buscarEdificios();
                }else{
                    alert(data['mensaje']);
                }
            },'json');
        },
        
        FotosEdificio: function(idEdificio) {
            loader('start');
            $("#childModal").html('Cargando...');
            $("#childModal").css("background-color","#FFFFFF");
            pr = $.post("modulos/maps/bandeja.control.edificio.php?cmd=fotosEdificio", {
            	idEdificio : idEdificio
            },
            function(data){
                $("#childModal").html(data);
                loader('end');
            });

            $("#childModal").dialog({modal:true, width:'75%', hide: 'slide', title: 'FOTOS DEL EDIFICIO', position:'top'});

        },
        
        ListadoFotosEdificio: function(idEdificio) {
            $("#loading_fotos_edificio").show();
            $.post("modulos/maps/bandeja.control.edificio.php?cmd=listadoFotosEdificio", {
                idEdificio : idEdificio
            },
            function(data){
                $("#tb_lista_fotos tbody").empty();
                $("#tb_lista_fotos tbody").append(data);
                $("#loading_fotos_edificio").hide();
            });
        },
        
        VerFotoDetalle: function(idedificios_foto) {
            $("#tabsFotosEdificio").tabs("enable", 2);
            $("#tabsFotosEdificio").tabs('select',2);
            
            $("#divFotosEdificio").html('Cargando fotos...');
            $.post("modulos/maps/bandeja.control.edificio.php?cmd=verFotoDetalle", {
            	idedificios_foto: idedificios_foto
            },
            function(data){
                $("#divFotosEdificio").html(data);
            });
        },
        
        DetalleEdificio: function(idedificio) {
            loader('start');
            $("#childModal").html('Cargando...');
            $("#childModal").css("background-color","#FFFFFF");
            pr = $.post("modulos/maps/bandeja.control.edificio.php?cmd=detalleEdificio", {
                idedificio: idedificio
            },
            function(data){
                $("#childModal").html(data);
                loader('end');
            });

            $("#childModal").dialog({modal:true, width:'75%', hide: 'slide', title: 'INFORMACIÓN DEL EDIFICIO', position:'top'});

        },
        
        DetalleEdificioParent: function(idedificio) {
            loader('start');
            parent.$("#childModal").html('Cargando...');
            parent.$("#childModal").css("background-color","#FFFFFF");
            pr = $.post("../bandeja.control.edificio.php?cmd=detalleEdificio", {
                idedificio: idedificio
            },
            function(data){
                parent.$("#childModal").html(data);
                loader('end');
            });

            parent.$("#childModal").dialog({modal:true, width:'75%', hide: 'slide', title: 'INFORMACIÓN DEL EDIFICIO', position:'top'});

        },
        
        EditarEdificio: function(idedificio) {
            loader('start');
            $("#childModal").html('Cargando...');
            $("#childModal").css("background-color","#FFFFFF");
            pr = $.post("modulos/maps/bandeja.control.edificio.php?cmd=editarEdificio", {
                idedificio: idedificio
            },
            function(data){
                $("#childModal").html(data);
                loader('end');
            });

            $("#childModal").dialog({modal:true, width:'75%', hide: 'slide', title: 'EDITAR INFORMACION DEL EDIFICIO', position:'top'});

        },
        
        EditarEdificioParent: function(idedificio) {
            loader('start');
            parent.$("#childModal").html('Cargando...');
            parent.$("#childModal").css("background-color","#FFFFFF");
            pr = $.post("../bandeja.control.edificio.php?cmd=editarEdificio", {
                idedificio: idedificio,
                iframe     : 1
            },
            function(data){
                parent.$("#childModal").html(data);
                loader('end');
            });

            parent.$("#childModal").dialog({modal:true, width:'75%', hide: 'slide', title: 'EDITAR INFORMACION DEL EDIFICIO', position:'top'});

        },
        
        NuevoEdificio: function() {
            loader('start');
            $("#childModal").html('Cargando...');
            $("#childModal").css("background-color","#FFFFFF");
            pr = $.post("modulos/maps/bandeja.control.edificio.php?cmd=nuevoEdificio", {},
            function(data){
                $("#childModal").html(data);
                loader('end');
            });

            $("#childModal").dialog({modal:true, width:'75%', hide: 'slide', title: 'INGRESAR INFORMACION DEL EDIFICIO', position:'top'});

        },
        
        UpdateDatosEdificio: function() {
        	alert("hola");
        	return false;
            var data_content = {
        	item                        : $("#item").val(),
        	item_etapa                  : $("#item_etapa").val(),
	    	fecha_registro_proyecto     : $("#fecha_registro_proyecto").val(),
            zonal                       : $("#cmb_zonal").val(),
	    	ura                         : $("#cmb_ura").val(),
	    	sector                      : $("#sector").val(),
	    	mza_tdp                     : $("#mza_tdp").val(),
	    	idtipo_via                  : $("#idtipo_via").val(),
	    	direccion_obra              : $("#direccion_obra").val(),
	    	numero                      : $("#numero").val(),
	    	mza                         : $("#mza").val(),
	    	lote                        : $("#lote").val(),
	    	idtipo_cchh                 : $("#idtipo_cchh").val(),
	    	cchh                        : $("#cchh").val(),
	    	distrito                    : $("#cmb_departamento").val() + $("#cmb_provincia").val() + $("#cmb_distrito").val(),
	    	nombre_constructora         : $("#nombre_constructora").val(),
	    	nombre_proyecto             : $("#nombre_proyecto").val(),
	    	idtipo_proyecto             : $("#idtipo_proyecto").val(),
	    	persona_contacto            : $("#persona_contacto").val(),
	    	pagina_web                  : $("#pagina_web").val(),
	    	email                       : $("#email").val(),
	    	nro_blocks                  : $("#nro_blocks").val(),
	    	nro_pisos_1                 : $("#nro_pisos_1").val(),
	    	nro_dptos_1                 : $("#nro_dptos_1").val(),
	    	nro_pisos_2                 : $("#nro_pisos_2").val(),
	    	nro_dptos_2                 : $("#nro_dptos_2").val(),
	    	nro_pisos_3                 : $("#nro_pisos_3").val(),
	    	nro_dptos_3                 : $("#nro_dptos_3").val(),
	    	nro_pisos_4                 : $("#nro_pisos_4").val(),
	    	nro_dptos_4                 : $("#nro_dptos_4").val(),
	    	nro_pisos_5                 : $("#nro_pisos_5").val(),
	    	nro_dptos_5                 : $("#nro_dptos_5").val(),
	    	idtipo_infraestructura      : $("#idtipo_infraestructura").val(),
	    	nro_dptos_habitados         : $("#nro_dptos_habitados").val(),
	    	avance                      : $("#avance").val(),
	    	fecha_termino_construccion  : $("#fecha_termino_construccion").val(),
	    	montante                    : $("#montante").val(),
	    	montante_fecha              : $("#montante_fecha").val(),
	    	montante_obs                : $("#montante_obs").val(),
	    	ducteria_interna            : $("#ducteria_interna").val(),
	    	ducteria_interna_fecha      : $("#ducteria_interna_fecha").val(),
	    	ducteria_interna_obs        : $("#ducteria_interna_obs").val(),
	    	punto_energia               : $("#punto_energia").val(),
	    	punto_energia_fecha         : $("#punto_energia_fecha").val(),
	    	punto_energia_obs           : $("#punto_energia_obs").val(),
	    	fecha_seguimiento           : $("#fecha_seguimiento").val(),
	    	observacion                 : $("#observacion").val(),
	    	idedificio                  : $("#idedificio").val()
            };

            if( data_content['zonal'] == '' ) {
                alert("Ingrese el campo Zonal");
                return false;
            }
            if( data_content['ura'] == '' ) {
                alert("Ingrese el campo URA");
                return false;
            }
            
            
            $("#msg").show();
            $("input[name=btn_grabar]").attr('disabled','disabled');
            pr = $.post("modulos/maps/bandeja.control.edificio.php?cmd=updateDatosEdificio", data_content, function(data){
                
                if(data['flag'] == '1'){
                    alert(data['mensaje']);

                    // si la ventana proviene de la bandeja de edificios
                    if( $("#iframe").val() == 'bandeja' ) {
                        //refresca la bandeja
                        buscarEdificios();
                    }
                    
                    //cerramos la ventana
                    cerrarVentana();

                }else{
                    alert(data['mensaje']);
                }
                
                $("#msg").hide();
                $("input[name=btn_grabar]").attr('disabled','');
            },'json');

        },
        
        InsertDatosEdificio: function() {
            var data_content = {
        	estado                      : $("#cmb_estado").val(),
	    	fecha_registro_proyecto     : $("#fecha_registro_proyecto").val(),
                zonal                       : $("#cmb_zonal").val(),
	    	ura                         : $("#cmb_ura").val(),
	    	sector                      : $("#sector").val(),
	    	mza_tdp                     : $("#mza_tdp").val(),
	    	idtipo_via                  : $("#idtipo_via").val(),
	    	direccion_obra              : $("#direccion_obra").val(),
	    	numero                      : $("#numero").val(),
	    	mza                         : $("#mza").val(),
	    	lote                        : $("#lote").val(),
	    	idtipo_cchh                 : $("#idtipo_cchh").val(),
	    	cchh                        : $("#cchh").val(),
	    	distrito                    : $("#cmb_departamento").val() + $("#cmb_provincia").val() + $("#cmb_distrito").val(),
	    	nombre_constructora         : $("#nombre_constructora").val(),
	    	nombre_proyecto             : $("#nombre_proyecto").val(),
	    	idtipo_proyecto             : $("#idtipo_proyecto").val(),
	    	persona_contacto            : $("#persona_contacto").val(),
	    	pagina_web                  : $("#pagina_web").val(),
	    	email                       : $("#email").val(),
	    	nro_blocks                  : $("#nro_blocks").val(),
	    	nro_pisos_1                 : $("#nro_pisos_1").val(),
	    	nro_dptos_1                 : $("#nro_dptos_1").val(),
	    	nro_pisos_2                 : $("#nro_pisos_2").val(),
	    	nro_dptos_2                 : $("#nro_dptos_2").val(),
	    	nro_pisos_3                 : $("#nro_pisos_3").val(),
	    	nro_dptos_3                 : $("#nro_dptos_3").val(),
	    	nro_pisos_4                 : $("#nro_pisos_4").val(),
	    	nro_dptos_4                 : $("#nro_dptos_4").val(),
	    	nro_pisos_5                 : $("#nro_pisos_5").val(),
	    	nro_dptos_5                 : $("#nro_dptos_5").val(),
	    	idtipo_infraestructura      : $("#idtipo_infraestructura").val(),
	    	nro_dptos_habitados         : $("#nro_dptos_habitados").val(),
	    	avance                      : $("#avance").val(),
	    	fecha_termino_construccion  : $("#fecha_termino_construccion").val(),
	    	montante                    : $("#montante").val(),
	    	montante_fecha              : $("#montante_fecha").val(),
	    	montante_obs                : $("#montante_obs").val(),
	    	ducteria_interna            : $("#ducteria_interna").val(),
	    	ducteria_interna_fecha      : $("#ducteria_interna_fecha").val(),
	    	ducteria_interna_obs        : $("#ducteria_interna_obs").val(),
	    	punto_energia               : $("#punto_energia").val(),
	    	punto_energia_fecha         : $("#punto_energia_fecha").val(),
	    	punto_energia_obs           : $("#punto_energia_obs").val(),
	    	fecha_seguimiento           : $("#fecha_seguimiento").val(),
	    	observacion                 : $("#observacion").val()
            };
            
            if( data_content['estado'] == '' ) {
                alert("Ingrese el campo Estado");
                return false;
            }
            if( data_content['zonal'] == '' ) {
                alert("Ingrese el campo Zonal");
                return false;
            }
            if( data_content['ura'] == '' ) {
                alert("Ingrese el campo URA");
                return false;
            }
            
            
            $("#msg").show();
            $("input[name=btn_grabar]").attr('disabled','disabled');
            pr = $.post("modulos/maps/bandeja.control.edificio.php?cmd=insertDatosEdificio", data_content, function(data){
                
                if(data['flag'] == '1'){
                    alert(data['mensaje']);

                    //refresca la bandeja
                    buscarEdificios();
                    
                    //cerramos la ventana
                    cerrarVentana();

                }else{
                    alert(data['mensaje']);
                }
                
                $("#msg").hide();
                $("input[name=btn_grabar]").attr('disabled','');
            },'json');

        }
        
    };
});