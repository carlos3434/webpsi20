function load_array(element) {
    var grupo = new Array();
    $("input[name='"+element+"']:checked").each(function(){
        grupo.push($(this).val());
    });
    return grupo;
}

function detalleVisor(tipo, flag_critico, area_averia, contrata, zonal, cumplimiento, nro_agendamientos, filtro_agend) {
    var area_averia = new Array();
    area_averia = load_array('areas[]');

    $("#childModal").html('');
    $("#childModal").css("background-color","#FFFFFF");
    $.post("modulos/reportes/detalle_visor_agendamiento.php", {
        action		: 'verDetalle',
        tipo		: tipo,
        flag_critico: flag_critico,
        area_averia : area_averia,
        contrata	: contrata,
        zonal		: zonal,
        cumplimiento: cumplimiento,
        nro_agendamientos: nro_agendamientos,
        filtro_agend: filtro_agend
    },
    function(data){
        $("#childModal").html(data);
    }
    );

    $("#childModal").dialog({
        modal:true, 
        width:'800px', 
        hide: 'slide', 
        title: 'Actuaciones Agendadas', 
        position:'top'
    });
}

function detalleVisorDescarga(tipo, flag_critico, area_averia, contrata, zonal, cumplimiento, nro_agendamientos, filtro_agend){
    
    var area_averia = new Array();
    area_averia = load_array('areas[]');

    
    /*$("#childModal").html('');
    $("#childModal").css("background-color","#FFFFFF");*/
    
    window.top.location = 'modulos/reportes/visor_agendamientos_descarga.php?tipo='+tipo+'&flag_critico=' + flag_critico + '&area_averia=' + area_averia
                            + '&contrata=' + contrata+ '&zonal=' + zonal+ '&cumplimiento=' + cumplimiento
                            + '&nro_agendamientos=' + nro_agendamientos+ '&filtro_agend=' + filtro_agend;
                        
                        
                        
    /*$.post("modulos/reportes/visor_agendamientos_descarga.php", {
            action		: 'verDetalle',
            tipo		: tipo,
            flag_critico: flag_critico,
            area_averia : area_averia,
            contrata	: contrata,
            zonal		: zonal,
            cumplimiento: cumplimiento,
            nro_agendamientos: nro_agendamientos,
            filtro_agend: filtro_agend
        },
        function(data){
            $("#childModal").html(data);
        }
    );

    $("#childModal").dialog({
        modal:true, 
        width:'800px', 
        hide: 'slide', 
        title: 'Actuaciones Agendadas', 
        position:'top'
    });*/
    
    
    
    

    /*window.top.location = 'modulos/reportes/visor_agendamientos_descarga.php?tipo=' + tipo+
                            '&flag_critico=' + flag_critico + '&area_averia=' + contrata +'&zonal=' + zonal +
                            '&cumplimiento=' + cumplimiento +'&nro_agendamientos=' + nro_agendamientos +'&filtro_agend=' + filtro_agend;*/


    
}



function detalleAgendReagend(tipo, flag_critico, area_averia, filtro_agend, reagendamiento, idarea) {
    var area_averia = new Array();
    area_averia = load_array('areas[]');
	
    $("#childModal").html('');
    $("#childModal").css("background-color","#FFFFFF");
    $.post("modulos/reportes/detalle_visor_agendamiento.php", {
        action		: 'verDetalleAgendReagend',
        tipo		: tipo,
        flag_critico: flag_critico,
        area_averia : area_averia,
        filtro_agend: filtro_agend,
        reagendamiento: reagendamiento,
        idarea		: idarea
    },
    function(data){
        $("#childModal").html(data);
    }
    );

    $("#childModal").dialog({
        modal:true, 
        width:'800px', 
        hide: 'slide', 
        title: 'Actuaciones Agendadas', 
        position:'top'
    });
    
}

function Buscar() {

    var flag_critico = $("#flag_critico").val();
    //var area_averia  = $("#area_averia").val();
	
    var area_averia = new Array();
    area_averia = load_array('areas[]');

    if( area_averia.length > 0 ) {
        $("#area_averia").html('<option value="">' + area_averia.length + ' area(s) seleccionadas</option>');
    }else {
        $("#area_averia").html('<option value="">Todos</option>');
    }
    $("#divAreaAveria").hide();
	
    var nro_agendamientos = "";
    if($("#agend-1").is(':checked')) { 
        nro_agendamientos += "&nro_agendamientos1=ok";
    }
    if($("#agend-2").is(':checked')) { 
        nro_agendamientos += "&nro_agendamientos2=ok";
    }
    if($("#agend-3").is(':checked')) { 
        nro_agendamientos += "&nro_agendamientos3=ok";
    }

    $("#fondoLoad").show();
	
    //data_content = "action=buscar&tipo=" + tipo + "&flag_critico=" + flag_critico;
    data_content = "action=buscar&flag_critico=" + flag_critico + "&area_averia=" + area_averia + nro_agendamientos;
    $.ajax({
        type:   "POST",
        //url:    "modulos/reportes/visor_agendamiento.php",
        url:    "modulos/reportes/actuaciones.agendadas.update.php",
        data:   data_content,
        success: function(datos) {
            $("#divResultVisor").html(datos);
            $("#fondoLoad").hide();
        }
    });
}

function BuscarContadorAgendas() {
    var flag_critico = $("#flag_critico").val();
    //var area_averia  = $("#area_averia").val();
	
    var area_averia = new Array();
    area_averia = load_array('areas[]');

    if( area_averia.length > 0 ) {
        $("#area_averia").html('<option value="">' + area_averia.length + ' area(s) seleccionadas</option>');
    }else {
        $("#area_averia").html('<option value="">Todos</option>');
    }
    $("#divAreaAveria").hide();
	
    var nro_agendamientos = "";
    if($("#agend-1").is(':checked')) { 
        nro_agendamientos += "&nro_agendamientos1=ok";
    }
    if($("#agend-2").is(':checked')) { 
        nro_agendamientos += "&nro_agendamientos2=ok";
    }
    if($("#agend-3").is(':checked')) { 
        nro_agendamientos += "&nro_agendamientos3=ok";
    }

    $("#fondoLoad").show();

    //data_content = "action=buscar&area_averia=" + area_averia;
    data_content = "action=buscar&flag_critico=" + flag_critico + "&area_averia=" + area_averia + nro_agendamientos;
    $.ajax({
        type:   "POST",
        url:    "modulos/reportes/contador.agendas.php",
        data:   data_content,
        success: function(datos) {
            $("#divResultVisor").html(datos);
            $("#fondoLoad").hide();
        }
    });
}

function BuscarAgendamientosReagendamientos() {
    var flag_critico = $("#flag_critico").val();
    //var area_averia  = $("#area_averia").val();
	
    var area_averia = new Array();
    area_averia = load_array('areas[]');

    if( area_averia.length > 0 ) {
        $("#area_averia").html('<option value="">' + area_averia.length + ' area(s) seleccionadas</option>');
    }else {
        $("#area_averia").html('<option value="">Todos</option>');
    }
    $("#divAreaAveria").hide();
	
    var nro_agendamientos = "";
    if($("#agend-1").is(':checked')) { 
        nro_agendamientos += "&nro_agendamientos1=ok";
    }
    if($("#agend-2").is(':checked')) { 
        nro_agendamientos += "&nro_agendamientos2=ok";
    }
    if($("#agend-3").is(':checked')) { 
        nro_agendamientos += "&nro_agendamientos3=ok";
    }

    $("#fondoLoad").show();

    //data_content = "action=buscar&area_averia=" + area_averia;
    data_content = "action=buscar&flag_critico=" + flag_critico + "&area_averia=" + area_averia + nro_agendamientos;
    $.ajax({
        type:   "POST",
        url:    "modulos/reportes/agendamientos.reagendamientos.php",
        data:   data_content,
        success: function(datos) {
            $("#divResultVisor").html(datos);
            $("#fondoLoad").hide();
        }
    });
}




