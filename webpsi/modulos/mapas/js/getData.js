getElementoByTipoRed = function(){
    var tipo_red  = $("select[name=cmb_tipo_red]").val();
    $("select[name=cmb_elemento]").html("<option value=''>Seleccione</option>");
    
    $("input[name=tipored]").val(tipo_red);
    
    if(tipo_red==""){ 
        $("#cmb_elemento").attr("disabled","disabled");
    }else{
        $("#cmb_elemento").attr("disabled","");
        var tipo_planta  = $("input[name=codplantaptr]").val();
        //Ajax: Traer los elementos referentes al tipo de Negocio y Tipo de Red
        $.post("modulos/gescom/registro.trabajo.fftt.php?cmd=getElemento_by_TipoRed", {
            tipo_planta: tipo_planta, 
            tipo_red: tipo_red,
            is_pex:'1'
        },
        function(data) {
            $.each(data, function(i, item) {
                $("select[name=cmb_elemento]").append("<option value='"+data[i].value+"' class='"+data[i].hide+"'>"+data[i].text+"</option>")
            });
        },'json');
    }
}

//TRAER ELEMENTO DE UN TIPO PLANTA: BASICA / STB - ADSL
changeMdf = function(ptr){
    var tipo_red     = $("input[name=tipored]").val();
    var tipo_planta  = $("input[name=codplantaptr]").val();
    var cod_elemento = $("input[name=codelementoptr], input[name=elementoptr]").val();
    var mdf          = $("select[name=mdf], select[name=cmb_mdf]").val();
    var cod_zonal    = $("input[name=codzonal]").val();
    
    $("select[dir=mdf]").html("<option value=''>Seleccione</option>");
    $('input[name=primario], input[name=cmb_primario]').val('').attr('readonly','readonly');
    $('input[name=direccioncliente]').val('').attr('readonly','readonly');
 
    if(mdf == ''){
        if(cod_elemento=='19' && ptr){$('select[name=cmb_primario]').find('option[value=""]').remove();$('select[name=cmb_primario]').multiselect().multiselect('refresh');}
        if(cod_elemento=='8' && ptr){$('select[name=cmb_armario]').find('option[value=""]').remove();$('select[name=cmb_armario]').multiselect().multiselect('refresh');}
        if((cod_elemento=='5'|| cod_elemento=='3') && ptr){$('select[name=cmb_terminal]').find('option[value=""]').remove();$('select[name=cmb_terminal]').multiselect().multiselect('refresh');}
        if(cod_elemento == '23' && ptr){$('select[name=cmb_secundario]').find('option[value=""]').remove();$('select[name=cmb_secundario]').multiselect().multiselect('refresh');}
        return false;
    }
    if(tipo_planta=='B'){
        if(tipo_red=='D'){
            if(cod_elemento=='19' || cod_elemento=='5' || cod_elemento=='3'){
                $.post("modulos/gescom/registro.trabajo.fftt.php?cmd=getCable_Armario_Terminal_Zonal_MDF", {
                    tipo_planta : tipo_planta,
                    cod_elemento: cod_elemento,
                    tipo_red    : tipo_red,
                    mdf         : mdf,
                    cod_zonal   : cod_zonal
                },
                function(data) {
                    $.each(data, function(i, item) { 
                        $("select[name=primario], select[name=cmb_primario]").append("<option value='"+data[i].primario+"'>"+data[i].primario+"</option>")
                    });
                    if(cod_elemento=='19' && ptr){
                        $('select[name=cmb_primario]').find('option[value=""]').remove(); 
                        $('select[name=cmb_primario]').multiselect().multiselect('refresh').multiselectfilter();                       
                    }
                    if((cod_elemento=='5' || cod_elemento=='3') && ptr){
                        $('select[name=cmb_terminal]').find('option[value=""]').remove(); 
                        $('select[name=cmb_terminal]').multiselect().multiselect('refresh').multiselectfilter();
                    }
                },'json');
            }
        }else if(tipo_red=='F'){
            if(cod_elemento=='19'){
                $('input[name=primario], input[name=cmb_primario]').val('').attr('readonly','').focus();
            }
            if(cod_elemento=='8' || cod_elemento == '3' || cod_elemento == '5' || cod_elemento == '23'){
                $.post("modulos/gescom/registro.trabajo.fftt.php?cmd=getCable_Armario_Terminal_Zonal_MDF", {
                    tipo_planta : tipo_planta,
                    cod_elemento: cod_elemento,
                    tipo_red    : tipo_red,
                    mdf         : mdf,
                    cod_zonal   : cod_zonal
                },
                function(data) {
                    $.each(data, function(i, item) { 
                        $("select[name=armario], select[name=cmb_armario]").append("<option value='"+data[i].armario+"'>"+data[i].armario+"</option>")
                    });
                    if(cod_elemento=='8' && ptr){
                        $('select[name=cmb_armario]').find('option[value=""]').remove(); 
                        $('select[name=cmb_armario]').multiselect().multiselect('refresh').multiselectfilter();                        
                    }
                    if((cod_elemento == '3' || cod_elemento == '5') && ptr){
                        $('select[name=cmb_terminal]').find('option[value=""]').remove(); 
                        $('select[name=cmb_terminal]').multiselect().multiselect('refresh').multiselectfilter();
                    }
                    if(cod_elemento == '23' && ptr){
                        $('select[name=cmb_secundario]').find('option[value=""]').remove(); 
                        $('select[name=cmb_secundario]').multiselect().multiselect('refresh').multiselectfilter();
                    }
                },'json');
            }
        }
        if(cod_elemento=='6' || cod_elemento == '12' || cod_elemento == '13'){
            var domicilio = $.trim($('input[name=direccioncliente]').val());
            if(domicilio==''){$('input[name=direccioncliente]').attr('readonly','').focus();}
        } 
    } 
}

changePrimario = function(ptr){
    var tipo_red        = $("input[name=tipored]").val();
    var tipo_planta     = $("input[name=codplantaptr]").val();
    var cod_elemento    = $("input[name=codelementoptr], input[name=elementoptr]").val();
    var mdf             = $("select[name=mdf], select[name=cmb_mdf]").val();
    var primario        = $("select[name=primario], select[name=cmb_primario]").val();
    var cod_zonal       = $("input[name=codzonal]").val();
       
    $("select[lang=armario]").html("<option value=''>Seleccione</option>");
    $("input[name=direccioncliente]").val('');

    if(mdf==''){return false;}
    
    if(tipo_planta=='B'){
        if(tipo_red=='D'){
            if(cod_elemento=='5' || cod_elemento=='3'){
                $.post("modulos/gescom/registro.trabajo.fftt.php?cmd=getTerminal_by_Armario_Zonal_MDF", {
                    tipo_planta     : tipo_planta,
                    cod_elemento    : cod_elemento,
                    tipo_red        : tipo_red,
                    mdf             : mdf,
                    cod_zonal       : cod_zonal,
                    cablearmario    : primario
                },
                function(data) {
                    $.each(data, function(i, item) {
                        $("select[name=terminal], select[name=cmb_terminal]").append("<option value='"+data[i].terminal+"' class='"+data[i].direccion+"'>"+data[i].terminal+"</option>")
                    });
                    if(ptr){
                        $('select[name=cmb_terminal]').find('option[value=""]').remove(); 
                        $('select[name=cmb_terminal]').multiselect().multiselect('refresh').multiselectfilter();
                    }    
                },'json');
            }
        }else if(tipo_red=='F'){
            
        }
    }
}

changeArmario=function(ptr){
    var tipo_red     = $("input[name=tipored]").val();
    var tipo_planta  = $("input[name=codplantaptr]").val();
    var cod_elemento = $("input[name=codelementoptr], input[name=elementoptr]").val();
    var mdf          = $("select[name=mdf], select[name=cmb_mdf]").val();
    var armario      = $("select[name=armario], select[name=cmb_armario]").val();
    var cod_zonal    = $("input[name=codzonal]").val();
        
    $("select[lang=armario]").html("<option value=''>Seleccione</option>");
    $("input[name=direccioncliente]").val('');
    if(armario==''){
        if((cod_elemento == '3' || cod_elemento == '5') && ptr){ 
            $('select[name=cmb_terminal]').find('option[value=""]').remove(); 
            $('select[name=cmb_terminal]').multiselect().multiselect('refresh');}
        if(cod_elemento == '23' && ptr){ 
            $('select[name=cmb_secundario]').find('option[value=""]').remove(); 
            $('select[name=cmb_secundario]').multiselect().multiselect('refresh');
            }
        return false;
    }
    if(tipo_planta=='B'){
        if(tipo_red=='D'){
               
        }else if(tipo_red=='F'){
            if(cod_elemento == '3' || cod_elemento == '5'){
                $.post("modulos/gescom/registro.trabajo.fftt.php?cmd=getTerminal_by_Armario_Zonal_MDF", {
                    tipo_planta  : tipo_planta,
                    cod_elemento : cod_elemento,
                    tipo_red     : tipo_red,
                    mdf          : mdf,
                    cod_zonal    : cod_zonal,
                    cablearmario : armario
                },
                function(data) {
                    $.each(data, function(i, item) {
                        $("select[name=terminal], select[name=cmb_terminal]").append("<option value='"+data[i].terminal+"' class='"+data[i].direccion+"'>"+data[i].terminal+"</option>")
                    });
                    if(ptr){
                        $('select[name=cmb_terminal]').find('option[value=""]').remove(); 
                        $('select[name=cmb_terminal]').multiselect().multiselect('refresh').multiselectfilter();
                    }    
                },'json');
            }
            else if(cod_elemento == '23'){
                $.post("modulos/gescom/registro.trabajo.fftt.php?cmd=getSecundario_by_Armario_Zonal_MDF", {
                    tipo_planta  : tipo_planta,
                    cod_elemento : cod_elemento,
                    tipo_red     : tipo_red,
                    mdf          : mdf,
                    nom_zonal    : $('option:selected', $("select[name=codjefatura], select[name=cmb_jefatura]")).text(),
                    armario      : armario
                },
                function(data) {
                    $.each(data, function(i, item) {
                        $("select[name=secundario], select[name=cmb_secundario]").append("<option value='"+data[i].secundario+"'>"+data[i].secundario+"</option>")
                    });
                    if(ptr){
                        $('select[name=cmb_secundario]').find('option[value=""]').remove(); 
                        $('select[name=cmb_secundario]').multiselect().multiselect('refresh').multiselectfilter();
                    }    
                },'json');
            }
            else if(cod_elemento == '8'){
                $.post("modulos/gescom/registro.trabajo.fftt.php?cmd=getDireccion_byArmario", {
                	cablearmario : armario,
                    mdf          : mdf,
                    cod_zonal    : cod_zonal
                },
                function(data) {
                	$('input[name=direccioncliente]').val(data.direccion).attr('readonly','').focus();
                },'json');
                return false;
            }
                            
        }
    }
}

changeTerminal = function(){
    var direccion   = $.trim($("select[name=terminal] option:selected").attr('class'));
    $("input[name=direccioncliente]").val(direccion).attr('readonly','').focus();
}
// FIN TRAER ELEMENTO DE TIPO PLANTA: BASICA / STB - ADSL

//TRAER ELEMENTO DE UN TIPO PLANTA: CATV
changeNodo = function(ptr){
    var tipo_planta  = $("input[name=codplantaptr]").val();
    var cod_elemento = $("input[name=codelementoptr], input[name=elementoptr]").val();
    var nodo         = $("select[name=nodo], select[name=cmb_nodo]").val();
       
    $(".nodo").html("<option value=''>Seleccione</option>");
    $('input[name=borne], input[name=cmb_borne]').val('').attr('readonly','readonly');
    $('input[name=direccioncliente]').val('').attr('readonly','readonly');

    if(nodo == ''){
        if((cod_elemento=='11' || cod_elemento=='9' || cod_elemento=='25' || cod_elemento=='4' || cod_elemento=='24') && ptr){
            $('select[name=cmb_troba]').find('option[value=""]').remove();$('select[name=cmb_troba]').multiselect().multiselect('refresh');
        }
        if((cod_elemento=='2' || cod_elemento=='26') && ptr){
            $('select[name=cmb_amplificador]').find('option[value=""]').remove();$('select[name=cmb_amplificador]').multiselect().multiselect('refresh');
        }
        if((cod_elemento=='21' || cod_elemento=='1') && ptr){
            $('select[name=cmb_tap]').find('option[value=""]').remove();$('select[name=cmb_tap]').multiselect().multiselect('refresh');
        }
        return false;}
    if(tipo_planta=='C'){
        if(cod_elemento!='18' && cod_elemento!='17' && cod_elemento!='16'){
            $.post("modulos/gescom/registro.trabajo.fftt.php?cmd=getTroba_byNodo", {
                tipo_planta : tipo_planta,
                cod_elemento: cod_elemento,
                nodo        : nodo
            },
            function(data) {
                $.each(data, function(i, item) { 
                    $("select[name=troba], select[name=cmb_troba]").append("<option value='"+data[i].troba+"'>"+data[i].troba+"</option>")
                });
                if((cod_elemento=='11' || cod_elemento=='9' || cod_elemento=='25' || cod_elemento=='4' || cod_elemento=='24') && ptr){
                    $('select[name=cmb_troba]').find('option[value=""]').remove(); 
                    $('select[name=cmb_troba]').multiselect().multiselect('refresh').multiselectfilter();                       
                }
                if((cod_elemento=='2' || cod_elemento=='26') && ptr){
                    $('select[name=cmb_amplificador]').find('option[value=""]').remove();
                    $('select[name=cmb_amplificador]').multiselect().multiselect('refresh');
                }
                if((cod_elemento=='21' || cod_elemento=='1') && ptr){
                    $('select[name=cmb_tap]').find('option[value=""]').remove();
                    $('select[name=cmb_tap]').multiselect().multiselect('refresh');
                }
            },'json');
        }else{
            if(cod_elemento=='18' || cod_elemento == '17' || cod_elemento == '16'){
                var domicilio = $.trim($('input[name=direccioncliente]').val());
                if(domicilio==''){
                    $('input[name=direccioncliente]').attr('readonly','').focus();
                }
            }
        }
    } 
}

changeTroba = function(ptr){
    var tipo_planta     = $("input[name=codplantaptr]").val();
    var cod_elemento    = $("input[name=codelementoptr], input[name=elementoptr]").val();
    var nodo            = $("select[name=nodo], select[name=cmb_nodo]").val();
    var troba           = $("select[name=troba], select[name=cmb_troba]").val();
    
    $(".troba").html("<option value=''>Seleccione</option>");
    if(troba == ''){
        if((cod_elemento=='2' || cod_elemento=='26') && ptr){
            $('select[name=cmb_amplificador]').find('option[value=""]').remove();$('select[name=cmb_amplificador]').multiselect().multiselect('refresh');
        }
        if((cod_elemento=='21' || cod_elemento=='1') && ptr){
            $('select[name=cmb_tap]').find('option[value=""]').remove();$('select[name=cmb_tap]').multiselect().multiselect('refresh');
        }
        return false;
    }
    if(tipo_planta=='C'){
        if(cod_elemento!='9' && cod_elemento!='4' && cod_elemento!='24' && cod_elemento!='25' && cod_elemento!='11'){
            $.post("modulos/gescom/registro.trabajo.fftt.php?cmd=getAmplificador_byTrobaNodo", {
                tipo_planta : tipo_planta,
                cod_elemento: cod_elemento,
                nodo        : nodo,
                troba       : troba
            },
            function(data) {
                $.each(data, function(i, item) { 
                    $("select[name=amplificador], select[name=cmb_amplificador]").append("<option value='"+data[i].amp+"'>"+data[i].amp+"</option>")
                });
                if((cod_elemento=='2' || cod_elemento=='26') && ptr){
                    $('select[name=cmb_amplificador]').find('option[value=""]').remove(); 
                    $('select[name=cmb_amplificador]').multiselect().multiselect('refresh').multiselectfilter();                       
                }
                if((cod_elemento=='21' || cod_elemento=='1') && ptr){
                    $('select[name=cmb_tap]').find('option[value=""]').remove();
                    $('select[name=cmb_tap]').multiselect().multiselect('refresh');
                }
            },'json');
        }else{
            
        }
    } 
}

changeAmplificador = function(ptr){
    var tipo_planta     = $("input[name=codplantaptr]").val();
    var cod_elemento    = $("input[name=codelementoptr], input[name=elementoptr]").val();
    var nodo            = $("select[name=nodo], select[name=cmb_nodo]").val();
    var troba           = $("select[name=troba], select[name=cmb_troba]").val();
    var amplificador    = $("select[name=amplificador], select[name=cmb_amplificador]").val();
    
    $(".amplificador").html("<option value=''>Seleccione</option>");
    if(amplificador == ''){
        if((cod_elemento=='21' || cod_elemento=='1') && ptr){
            $('select[name=cmb_tap]').find('option[value=""]').remove(); 
            $('select[name=cmb_tap]').multiselect().multiselect('refresh').multiselectfilter();                       
        }
        return false;
    }
    if(tipo_planta=='C'){
        if(cod_elemento!='2' && cod_elemento!='26'){
            $.post("modulos/gescom/registro.trabajo.fftt.php?cmd=getTap_byAmplificadorTrobaNodo", {
                tipo_planta : tipo_planta,
                cod_elemento: cod_elemento,
                nodo        : nodo,
                troba       : troba,
                amplificador: amplificador
            },
            function(data) {
                $.each(data, function(i, item) { 
                    $("select[name=tap], select[name=cmb_tap]").append("<option value='"+data[i].tap+"'>"+data[i].tap+"</option>")
                });
                if((cod_elemento=='21' || cod_elemento=='1') && ptr){
                    $('select[name=cmb_tap]').find('option[value=""]').remove(); 
                    $('select[name=cmb_tap]').multiselect().multiselect('refresh').multiselectfilter();                       
                }
            },'json');
        }else{
            
        }
    } 
}

changeTap = function(ptr){
    var tap     = $("select[name=tap], select[name=cmb_tap]").val();
    var cod_elemento    = $("input[name=codelementoptr], input[name=elementoptr]").val();
    $("input[name=borne], input[name=cmb_borne]").attr("readonly","").focus();
    
//    if(tap == ''){
        $("input[name=borne], input[name=cmb_borne]").val('').attr("readonly","readonly");
//    }
    if((cod_elemento=='15' || cod_elemento == '20')&& tap!=''){
        $("input[name=borne], input[name=cmb_borne]").removeAttr("readonly").focus();        
    }
}
// FIN TRAER ELEMENTO DE TIPO PLANTA: CATV

// VALIDACIONES DE TIPO PLANTA: BASICA | CATV | FIBRA
validateFFTT = function(){
    var tipo_planta  = $("input[name=codplantaptr]").val();
    var cod_elemento = $("input[name=codelementoptr], input[name=elementoptr]").val();
    
    var direccion = '';
    if(tipo_planta=='B'){
        var tipo_red     = $("input[name=tipored]").val();
        var mdf          = $("select[name=mdf], select[name=cmb_mdf]").val();
        var armario      = $("select[name=armario], select[name=cmb_armario]").val();
        var primario     = $("select[name=primario], select[name=cmb_primario], input[name=cmb_primario], input[name=primario]").val();
        var secundario   = $("select[name=secundario], select[name=cmb_secundario]").val();
        var terminal     = $("select[name=terminal], select[name=cmb_terminal]").val();
        direccion        = $.trim($("input[name=direccioncliente]").val());
                
        if(mdf==''){alert("Seleccione correctamente el MDF");return false;}
        if(cod_elemento=='5' || cod_elemento=='3'){
            if(tipo_red == 'D'){
                if(primario=='' || primario==null){alert("Seleccione correctamente el C. Directo.");return false;}
                if(terminal=='' || terminal==null){alert("Seleccione correctamente el Terminal.");return false;}
            }else if(tipo_red == 'F'){
                if(armario=='' || armario==null){alert("Seleccione correctamente el Armario.");return false;}
                if(terminal=='' || terminal==null){alert("Seleccione correctamente el Terminal.");return false;}
            }
        }else if(cod_elemento=='8'){
            if(tipo_red == 'D'){
            }else if(tipo_red == 'F'){    
                if(armario=='' || armario==null){alert("Seleccione correctamente el Armario.");return false;}
            }   
        } else if(cod_elemento=='19'){
            if(tipo_red == 'D'){
                if(primario=='' || primario==null){alert("Seleccione correctamente el C. Directo.");return false;}
            }else if(tipo_red == 'F'){
                if(primario=='' || primario==null){alert("Seleccione correctamente el C. Directo.");return false;}
            }  
        } else if(cod_elemento=='23'){
            if(tipo_red == 'D'){
                
            }else if(tipo_red == 'F'){
                if(armario=='' || armario==null){alert("Seleccione correctamente el Armario.");return false;}
                if(secundario=='' || secundario==null){alert("Seleccione correctamente el C. Secundario.");return false;}
            } 
        } else if(cod_elemento=='6'|| cod_elemento=='12' || cod_elemento=='13'){
            if(direccion==''){alert("Ingrese correctamente la Direcci\u00F3n.");return false;}
        }        
    }else if(tipo_planta=='C'){
        var nodo         = $("select[name=nodo], select[name=cmb_nodo]").val();
        var troba        = $("select[name=troba], select[name=cmb_troba]").val();
        var amplificador = $("select[name=amplificador], select[name=cmb_amplificador]").val();
        var tap          = $("select[name=tap], select[name=cmb_tap]").val();
        var borne        = $("input[name=borne], input[name=cmb_borne]").val();
        direccion        = $.trim($("input[name=direccioncliente]").val());
        
        if(nodo==''){alert("Seleccione correctamente el Nodo");return false;}
        if(cod_elemento=='11' || cod_elemento=='24' || cod_elemento=='4' || cod_elemento=='9' || cod_elemento=='25'){
            if(troba=='' || troba==null){alert("Seleccione correctamente la Troba.");return false;}
        }
        if(cod_elemento=='2' || cod_elemento=='26'){
            if(troba=='' || troba==null){alert("Seleccione correctamente la Troba.");return false;}
            if(amplificador=='' || amplificador==null){alert("Seleccione correctamente el Amplificador.");return false;}
        }
        if(cod_elemento=='1' || cod_elemento=='21'){
            if(troba=='' || troba==null){alert("Seleccione correctamente la Troba.");return false;}
            if(amplificador=='' || amplificador==null){alert("Seleccione correctamente el Amplificador.");return false;}
            if(tap=='' || tap==null){alert("Seleccione correctamente el Tap.");return false;}
        }
        if(cod_elemento=='20' || cod_elemento=='15'){
            if(troba=='' || troba==null){alert("Seleccione correctamente la Troba.");return false;}
            if(amplificador=='' || amplificador==null){alert("Seleccione correctamente el Amplificador.");return false;}
            if(tap=='' || tap==null){alert("Seleccione correctamente el Tap.");return false;}
            if(borne=='' || borne==null){alert("Ingrese correctamente el Borne.");return false;}
        }else if(cod_elemento=='18'|| cod_elemento=='17' || cod_elemento=='16'){
            if(direccion==''){alert("Ingrese correctamente la Direcci\u00F3n.");return false;}
        }  
        
    }
}

validateDepProDis = function(){
    var coddepartamento = $("select[name=coddepartamento], select[name=cmb_departamento]").val();
    var codprovincia    = $("select[name=codprovincia], select[name=cmb_provincia]").val();
    var coddistrito     = $("select[name=coddistrito], select[name=cmb_distrito]").val();

    if(coddepartamento == ""){
        alert('Seleccione correctamente un Departamento');
        return false;
    }
    if(codprovincia == ""){
        alert('Seleccione correctamente una Provincia');
        return false;
    }
    if(coddistrito == ""){
        alert('Seleccione correctamente un Distrito');
        return false;
    }  
}
// FIN VALIDACIONES DE TIPO PLANTA: BASICA | CATV | FIBRA

changeDepartamento = function(){
    var cod_departamento = $('select[name=cmb_departamento], select[name=coddepartamento]').val();
    var cod_zonal        = $('input[name=codzonal]').val();
    
    $("select[name=codprovincia], select[name=cmb_provincia], select[name=coddistrito], select[name=cmb_distrito]").html("<option value=''>Seleccione</option>")
    
    if(cod_departamento!=''){
        $.post("modulos/gescom/registro.trabajo.fftt.php?cmd=getProvincia_by_Departamento_Zonal", {
            cod_departamento    : cod_departamento,
            cod_zonal           : cod_zonal
        },
        function(data) {
            $.each(data, function(i, item) {
                $("select[name=codprovincia], select[name=cmb_provincia]").append("<option value='"+data[i].codprov+"'>"+data[i].nombre+"</option>")
            });
        },'json');
    }    
}

changeProvincia = function(){
    var cod_departamento = $('select[name=cmb_departamento], select[name=coddepartamento]').val();
    var cod_zonal        = $('input[name=codzonal]').val();
    var cod_jefatura     = $('input[name=codjefaturaptr], select[name=codjefatura],input[name=codjefatura]').val();
    var cod_provincia    = $("select[name=codprovincia], select[name=cmb_provincia]").val();
    
    $("select[name=coddistrito], select[name=cmb_distrito]").html("<option value=''>Seleccione</option>");
    
    if(cod_provincia!=''){
        $.post("modulos/gescom/registro.trabajo.fftt.php?cmd=getDistrito_byProvinciaZonalJefatura", {
            cod_departamento    : cod_departamento,
            cod_zonal           : cod_zonal,
            cod_jefatura        : cod_jefatura,
            cod_provincia       : cod_provincia
        },
        function(data) {
            $.each(data, function(i, item) {
                $("select[name=coddistrito], select[name=cmb_distrito]").append("<option value='"+data[i].coddist+"'>"+data[i].nombre+"</option>")
            });
        },'json');
    }    
}

function loader(estado){
    if(estado=='start')    {
        $(".bgLoader").css("display","block");
    }
    if(estado=='end')    {
        $(".bgLoader").css("display","none");
    }
}

validarNumber=function(e){
    tecla = (document.all) ? e.keyCode : e.which;
    if (tecla==8) return true;
    patron =/\d/;//valida solo numeros
    te = String.fromCharCode(tecla);
    return patron.test(te);
}

getJefaturaByEmpresa = function(){
    var id_empresa = $("#cmb_empresa").val();
    $("#cmb_jefatura").find('option[value!=""]').remove();
    if(id_empresa == ''){
        return false;
    }
    $.post("modulos/maps/bandeja.registro.pexBAK.php?cmd=getJefaturas_byEmpresa", {
        id_empresa : id_empresa
    },
    function(data) {
        $.each(data, function(i, item) {
            $("#cmb_jefatura").append("<option value='"+data[i].jefatura+"' title='"+data[i].zonal+"'>"+data[i].jefatura_desc+"</option>")
        });
    },'json');
}
