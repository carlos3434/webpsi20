function asignarTecnico() {

    var chx = getCheckedBoxFrom("div", "content", "1")

    if (chx == "") {
        alert("Debe seleccionar al menos una actuacion");
    } else {
        $.post("modulos/gescom/asigtec.php", {
            strActu : chx
        }, function(data) {
            $("#childModal").html(data);
        });

        $("#childModal").dialog({
            modal : true,
            width : '500px',
            hide : 'slide',
            title : 'Asignar Tecnicos',
            position : 'top'
        });
    }
}

function doAsgTecActu() {

    var x_actu = "";
    var str_actu_date = "";
    var actus = getCheckedBoxFrom("div", "content", "1").substr(1);
    var actus_arr = actus.split(",");

    for ( var i = 0; i < actus_arr.length; i++) {

        x_actu = actus_arr[i].split("|");
        x_actu = x_actu[0];

        hini = $("#fi_" + x_actu).html().substr(11);
        hend = $("#ff_" + x_actu).html().substr(11);

        str_actu_date += "|" + x_actu + "_" + hini + "_" + hend;
    }
    str_actu_date = str_actu_date.substr(1);

    $.post("modulos/gescom/asigtec.php", {
        doAsgTecActu : "ok",
        fecha : $("#txtDateActuTec").val(),
        cip : $("#newCipActu").val(),
        actu : actus,
        str_actu_date : str_actu_date
    }, function(data) {
        $("#asgTecProc").html(data);
    });

}

function mapaGestion(idactu, tipo, gestion) {
	
    $("#childModal").html("");

    $.post("modulos/maps/mapController.php", {
        idactu:idactu, 
        tipo:tipo, 
        gestion:gestion, 
        action:"quiebre.actu"
    }, function(data) {
        $("#childModal").html(data);
    });

    $("#childModal").dialog({
        modal : true,
        width : '80%',
        hide : 'slide',
        title : 'Titulo',
        position : 'top'
    });
}

function imprimirSeleccion(){
    /*var pr = "a,b|c,d|e,f";
    var exp = pr.split("|");
    var r=0;
    var exp1="";
    for(r=0;r<exp.length;r++){
        exp1 = exp[r].split(",");
        alert(exp1[0]+"-"+exp1[1]);
    }*/
    var chk_val = contar_check_temas(".lista_chk");    
    //alert(chk_val);
	//return false;
    
    if (chk_val.length>0){           
        $("#childModal").html("");                
        $.post("modulos/gescom/vistas/principal/boleta_eecc_masivo.php", {
            printAll : "Ok",
            seleccion : chk_val
        },function(data){            
            $("#childModal").html(data);
        });
        $("#childModal").dialog({
            modal : true,
            width : '750px',
            maxHeight : '500px',
            hide : 'slide',
            position : 'top',
            title : "Imprimir boleta"
        });
    }else{
        alert("Debe seleccionar una o más actuaciones a imprimir!");
    }
}

function imprimirSeleccionXY(){
    var chk_val = contar_check_temas(".lista_chk");
    if (chk_val.length>0){           
        $("#childModal").html("");                
        $.post("../../../modulos/gescom/vistas/principal/boleta_eecc_masivo.php", {
            printAll : "Ok",
            seleccion : chk_val
        },function(data){            
            parent.$("#childModal").html(data);
        });
        parent.$("#childModal").dialog({
            modal : true,
            width : '750px',
            maxHeight : '500px',
            hide : 'slide',
            position : 'top',
            title : "Imprimir boleta"
        });        
    }else{
        alert("Ninguna Averia seleccionada para imprimir!");
    }
}

function descargaBandeja(){
    $("#childModal").html("");
    $("#childModal").load(
        "modulos/gescom/bandeja_descarga.php"
    );
    $("#childModal").dialog({
        modal : true,
        width : '850px',
        maxHeight : '500px',
        hide : 'slide',
        position : 'top',
        title : "Descarga"
    })
}

function downloadPteLiq(format){
    var zonal = contar_check_temas(".multiselect_zonal");
    var negocio = contar_check_temas(".multiselect_negocio");
    var empresa = $("#d_empresa").val();
    
    var tipo = $("#d_tipo").val();
    var abc = $("#d_abc").val();
    
    var estado = $("#d_estado").val();
    var txt_tipo = $("#d_tipo option:selected").text();
    var txt_abc = $("#d_abc option:selected").text();
    
    if(zonal==""){
        alert('Debe Seleccionar una Zonal'); return false;
    }
    if(empresa==""){
        alert('Debe Seleccionar una EECC'); return false;
    }
    if(tipo==""){
        alert('Debe Seleccionar una Tipo de Pedido'); return false;
    }

    $.post("../../../../pages/valida_descarga.php", {
        action : "verifica_num_reg",
        tipo_p_a   : tipo
    },function(data){
        //alert(data); return false;
        if(data==0){
            alert('Se esta acctualizando la Data. Intente descargar en unos instantes');
            return false;
        }else{
            
            
            
            window.top.location = '../../../../pages/descarga.php?zonal=' + zonal + '&negocio=' + negocio + '&empresa='
            + empresa + '&tipo=' + tipo + '&abc=' + abc + '&estado=' + estado
            + '&fmt=' + format + '&txt_tipo=' + txt_tipo + '&txt_abc='
            + txt_abc;
        }
    });
       


    
}

function downloadData_xContrata(){
    window.top.location = '../../../../pages/descarga_data_contrata.php';
}

function contar_check_temas(clase){
    var checkboxes = $(clase);
    var cont = 0;
    var id_chk_temas = new Array();
    if (checkboxes.length == undefined){
        if (checkboxes.value != ""){
            if (checkboxes.checked)
                id_chk_temas[cont] = checkboxes.value;
        }
    }
    for (var x=0; x < checkboxes.length; x++) {
        if (checkboxes[x].checked){
            id_chk_temas[cont] = checkboxes[x].value;
            cont+=1;
        }
    }
    return id_chk_temas;
	//return checkboxes.length;
}