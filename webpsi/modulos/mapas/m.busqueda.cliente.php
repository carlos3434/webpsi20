<?php

/**
 * @package     /modulos/maps
 * @name        m.busqueda.cliente.php
 * @category    Controller
 * @author      Wilder Sandoval Huamani <wsandoval@gmd.com.pe>
 * @copyright   2011 Telefonica del Peru
 * @version     1.0 - 2012/06/26
 */

require_once "../../config/web.config.php";
include_once APP_DIR . 'autoload.php';
include_once APP_DIR . 'session.php';

$dirModulo = '../../';


function vistaInicial()
{
    try {
        global $conexion, $dirModulo;

        include 'vistas/m.v_busqueda.cliente.php';

    } catch(PDOException $e) {
        echo 'Se ha producido un error en la aplicacion, vuelva ' .
            'a intentarlo mas tarde.';
        exit;
    }
}

function buscarCliente($valor='')
{
    try {
        global $conexion, $dirModulo;

        $arrClientesMarkers = array();
        if ( $valor != '' ) {
            
            $objTerminales = new Data_FfttTerminales();

            //busca en fftt_secundaria
            $arrClientesRs = $objTerminales->listClientes(
                $conexion, 'F', '', '', '', '', $valor
            );
            foreach ( $arrClientesRs as $clientesRs ) {
                $arrClientesMarkers[] = $clientesRs;
            }
            
            //busca en fftt_directa
            $arrClientesRd = $objTerminales->listClientes(
                $conexion, 'D', '', '', '', '', $valor
            );
            foreach ( $arrClientesRd as $clientesRd ) {
                $arrClientesMarkers[] = $clientesRd;
            }
        }

        $arrDataResult = array(
            'flag'     => '1',
            'clientes' => $arrClientesMarkers,
            'msg'      => 'ok'
        );

        echo json_encode($arrDataResult);
        exit;

    } catch(PDOException $e) {
        
        $arrDataResult = array(
            'flag'     => '0',
            'clientes' => array(),
            'msg'      => 'Se ha producido un error en la aplicacion, vuelva ' .
                'a intentarlo mas tarde.'
        );
        
        echo json_encode($arrDataResult);
        exit;
    }
}

function verMapa($ddnTelefono, $x, $y)
{
    try {
        global $conexion, $dirModulo;

        include 'vistas/m.v_busqueda.cliente.mapa.php';

    } catch(PDOException $e) {
        echo 'Se ha producido un error en la aplicacion, vuelva ' .
                'a intentarlo mas tarde.';
        exit;
    }
}


if (!isset($_GET['cmd']))
    $_GET['cmd'] = '';

switch ($_GET['cmd']) {
    case 'verMapa':
        verMapa($_REQUEST['ddnTelefono'], $_REQUEST['x'], $_REQUEST['y']);
        break;
    case 'buscarCliente':
        buscarCliente($_POST['valor']);
        break;
    default:
        vistaInicial();
        break;
}