<?php

/**
 * @package     /modulos/maps
 * @name        bandeja.control.armarios.php
 * @category    Controller
 * @author      Wilder Sandoval Huamani <wsandoval@gmd.com.pe>
 * @copyright   2011 Telefonica del Peru
 * @version     1.0 - 2011/12/19
 */

require_once "../../config/web.config.php";
require_once APP_DIR . 'autoload.php';
require_once APP_DIR . 'session.php';


function bandejaMain()
{
    global $conexion;

    $objArmarioEstado    = new Data_ArmarioEstado();
    $arrObjArmarioEstado = $objArmarioEstado->obtenerArmarioEstado($conexion);

    $totalRegistros = 0;
        
    include 'vistas/v_bandeja.control.armarios.php';
}

function filtroBusqueda($pagina='', $total='', $estado='', $zonal='', $mdf='', 
        $campoValor='', $orderField='', $orderType='')
{
    try {
        global $conexion;

        $objArmario = new Data_Armario();

        $arrFiltro = array();
        if ( $estado != '' ) {
            $arrFiltro['estado'] = $estado;
        }
        if ( $zonal != '' ) {
            $arrFiltro['zonal'] = $zonal;
        }
        if ( $mdf != '' ) {
            $arrFiltro['mdf'] = $mdf;
        }
        if ( $campoValor != '' ) {
            $arrFiltro['campo_valor'] = $campoValor;
        }

        if ( $orderField != '' ) {
            $arrFiltro['order_field'] = $orderField;
        }
        if ( $orderType != '' ) {
            $arrFiltro['order_type'] = $orderType;
        }

        $pagina = 1;
        $arrObjArmario = $objArmario->listarArmarios(
            $conexion, $pagina, '', $arrFiltro
        );

        $totalRegistros = $objArmario->totalArmarios($conexion, '', $arrFiltro);

        $numPaginas = ceil($totalRegistros/TAM_PAG_LISTADO);


        include 'vistas/v_bandeja.control.armarios.paginado.php';

        $cadena = '<script type="text/javascript">
                    $("#cont_num_resultados_head").html(' . $totalRegistros . ');
                    $("#cont_num_resultados_foot").html(' . $totalRegistros . ');
                    $("#pag_actual").attr("value", "1");
                    $("#tl_registros").attr("value", "' . $totalRegistros . '");
                    $("#paginacion").empty();
                    ';
                    if ($totalRegistros > 0) {
                        $cadena .= '
                        $(function() {
                            $("#paginacion").paginate({
                                count                   : '.$numPaginas.',
                                start                   : 1,
                                display                 : 10,
                                border                  : true,
                                border_color            : "#FFFFFF",
                                text_color              : "#FFFFFF",
                                background_color        : "#0080AF",
                                border_hover_color      : "#CCCCCC",
                                text_hover_color        : "#000000",
                                background_hover_color  : "#FFFFFF",
                                images                  : false,
                                mouse                   : "press",
                                onChange : function(page){
                                    loader("start");
                                    $("#pag_actual").attr("value", page);
                                    var tl=$("#tl_registros").attr("value");
                                    $.post("modulos/maps/bandeja.control.armarios.php?cmd=cambiarPaginaArmario", { 
                                                    pagina: page, total: tl, estado: "' . $estado . '", zonal: "' . $zonal . '", mdf: "' . $mdf . '", campo_valor: "' . $campoValor . '", order_field: order_field, order_type: order_type   
                                            }, function(data){
                                                    $("#tb_resultado tbody").empty();
                                                    $("#tb_resultado tbody").append(data);
                                                    loader("end");
                                            }
                                    );
                                }
                            });
                        });';
                    }
            $cadena .= '</script>';
        echo $cadena;
        
    } catch(PDOException $e) {
        //throw $e;
    }
}

function cambiarPaginaArmario($pagina='', $total='', $estado='', $zonal='', 
        $mdf='', $campoValor='', $orderField='', $orderType='')
{
    try {
        global $conexion;

        if ($pagina == '') $pagina = 1;

        $objArmario = new Data_Armario();


        $arrFiltro = array();
        if ( $estado != '' ) {
            $arrFiltro['estado'] = $estado;
        }
        if ( $zonal != '' ) {
            $arrFiltro['zonal'] = $zonal;
        }
        if ( $mdf != '' ) {
            $arrFiltro['mdf'] = $mdf;
        }
        if ( $campoValor != '' ) {
            $arrFiltro['campo_valor'] = $campoValor;
        }

        if ( $orderField != '' ) {
            $arrFiltro['order_field'] = $orderField;
        }
        if ( $orderType != '' ) {
            $arrFiltro['order_type'] = $orderType;
        }

        $arrObjArmario = $objArmario->listarArmarios($conexion, $pagina, '', $arrFiltro);

        $totalRegistros = $objArmario->totalArmarios($conexion, '', $arrFiltro);

        $numPaginas = ceil($totalRegistros/TAM_PAG_LISTADO);


        include 'vistas/v_bandeja.control.armarios.paginado.php';
        
    } catch(PDOException $e) {
        //throw $e;
    }
}

//lista historico de fotos de armarios
function fotosArmario($mtgespkarm, $map='')
{
    try {
        global $conexion;

        $objArmario            = new Data_Armario();
        $objArmarioEstados    = new Data_ArmarioEstado();

        $arrObjArmario = $objArmario->obtenerArmario($conexion, $mtgespkarm);
        
        $arrObjArmarioEstados = $objArmarioEstados->obtenerArmarioEstado(
            $conexion
        );

        include 'vistas/v_bandeja.control.armarios.fotos.main.php';

    } catch(PDOException $e) {
        echo 'Se ha producido un error en el Sistema. Vuelva a intentarlo en unos minutos.';
        exit;
    }
}

//Editar informacion del armario
function editarArmario($mtgespkarm)
{
    try {
        global $conexion;

        $objArmario         = new Data_Armario();
        $objArmarioEstados  = new Data_ArmarioEstado();

        $arrObjArmario = $objArmario->obtenerArmario($conexion, $mtgespkarm);
        
        $arrObjArmarioEstados = $objArmarioEstados->obtenerArmarioEstado(
            $conexion
        );

        include 'vistas/v_bandeja.control.armarios.editar.php';

    } catch(PDOException $e) {
        echo 'Se ha producido un error en el Sistema. Vuelva a intentarlo en unos minutos.';
        exit;
    }
    
}

//foto detalle
function verFotoDetalle($idArmariosFoto)
{
    try {
        global $conexion;
        
        $objArmarioFotos      = new Data_ArmarioFotos();
        
        $arrObjArmarioFotosDetalle = $objArmarioFotos->obtenerFotosDetalleArmario($conexion, $idArmariosFoto);
        
        $arrDataFotos = array();
        if ( !empty($arrObjArmarioFotosDetalle) ) {
            if ( trim($arrObjArmarioFotosDetalle[0]->__get('_foto1')) != '' ) {
                $arrDataFotos[] = array('image' => trim($arrObjArmarioFotosDetalle[0]->__get('_foto1')));
            }
            if ( trim($arrObjArmarioFotosDetalle[0]->__get('_foto2')) != '' ) {
                $arrDataFotos[] = array('image' => trim($arrObjArmarioFotosDetalle[0]->__get('_foto2')));
            }
            if ( trim($arrObjArmarioFotosDetalle[0]->__get('_foto3')) != '' ) {
                $arrDataFotos[] = array('image' => trim($arrObjArmarioFotosDetalle[0]->__get('_foto3')));
            }
        }
        

        include 'vistas/v_bandeja.control.armarios.fotos.detalle.php';

    } catch(PDOException $e) {
        echo 'Se ha producido un error en el Sistema. Vuelva a intentarlo en unos minutos.';
        exit;
    }
    
}

function listadoFotosArmario($mtgespkarm)
{
    try {
        global $conexion;

        $objArmarioFotos      = new Data_ArmarioFotos();
        
        $arrObjArmarioFotos = $objArmarioFotos->obtenerFotosArmario(
            $conexion, $mtgespkarm
        );
        
        include 'vistas/v_bandeja.control.armarios.fotos.listado.php';

    } catch(PDOException $e) {
        echo 'Se ha producido un error en el Sistema. Vuelva a intentarlo en unos minutos.';
        exit;
    }
    
}

function registrarFotos($arrDataFoto, $arrFilesArmarios)
{
    try {
        global $conexion;
        
        //validando tama�o y dimensiones de fotos
        $validaTamanoImagen   = validateTamanoImagen(
            $arrFilesArmarios['imagen_armario']
        );
        if ($validaTamanoImagen == '1') { 
            $mensaje = array(
                "flag"      => "0",
                "mensaje"   => "El tama&ntilde;o de 1 o mas archivos no es correcto.\n\n500Kb como maximo.",
                "tipo"      => "imagen"
            );
            echo json_encode($mensaje);
            exit;
        }
        //FIN validando tama�o y dimensiones de fotos atis
        
        //Upload de fotos armarios
        $listNameFotos    = moveFile($arrFilesArmarios['imagen_armario'], 'arm');
        if ( !empty($listNameFotos) && count($listNameFotos) > 0 ) {
            $arrDataFoto['foto_armario1'] = ( isset($listNameFotos[0]) ? $listNameFotos[0] : '' );
            $arrDataFoto['foto_armario2'] = ( isset($listNameFotos[1]) ? $listNameFotos[1] : '' );
            $arrDataFoto['foto_armario3'] = ( isset($listNameFotos[2]) ? $listNameFotos[2] : '' );
        } else {
            $mensaje = array(
                "flag"      => "0",
                "mensaje"   => "No se logro subir las fotos del armario, Vuelva a intentarlo.",
                "tipo"      => "imagen"
            );
            echo json_encode($mensaje);
            exit;
        }
        //FIN Upload de fotos
        
        
        $arrFechaEstado = explode("/", $arrDataFoto['fecha_estado']);
        $arrDataFoto['fecha_estado'] = $arrFechaEstado[2] . '-' . $arrFechaEstado[1] . '-' . $arrFechaEstado[0];

        
        $idUsuario          = $_SESSION['USUARIO']->__get('_idUsuario');
        $objArmario        = new Data_Armario();
        $objArmarioFotos  = new Data_ArmarioFotos();
        

        //insertando en Armario Fotos
        $idArmariosFoto = $objArmarioFotos->insertarFotoArmario($conexion, $arrDataFoto, $idUsuario);
        
        //actualizando fotos en armario
        $objArmario->updateFotosArmario($conexion, $arrDataFoto, $idUsuario);
        
        
        $arrDataResult = array('flag'=>'1','mensaje'=>'Datos grabados correctamente.','tipo'=>'');
        echo json_encode($arrDataResult);
        exit;

    } catch (PDOException $e) {
        $arrDataResult = array(
            'flag'=>'0',
            'mensaje'=>'Hubo un error al registrar las fotos, intentelo nuevamente.', 
            'tipo'=>''
        );
        echo json_encode($arrDataResult);
        exit;
    }
}

function updateDatosArmario($_POST)
{
    try {
        global $conexion;
        
        $idUsuario   = $_SESSION['USUARIO']->__get('_idUsuario');
        $objArmario  = new Data_Armario();
        
        //actualizando datos en armario
        $objArmario->updateDatosArmario($conexion, $_POST, $idUsuario);
        
        
        $arrDataResult = array('flag'=>'1','mensaje'=>'Datos grabados correctamente.','tipo'=>'');
        echo json_encode($arrDataResult);
        exit;

    } catch (PDOException $e) {
        $arrDataResult = array(
            'flag'=>'0',
            'mensaje'=>'Hubo un error al actualizar el armario, intentelo nuevamente.', 
            'tipo'=>''
        );
        echo json_encode($arrDataResult);
        exit;
    }
}

function validateTamanoImagen($imagen)
{
    $tamano = '0';
    
    //validando el peso de las imagenes
    foreach ($imagen['size'] as $key => $value) {
        if ( $value > 0 ) {
            //if ($_FILES['imagen_atis']['size'][$key] > 2048000) { //si el archivo pesa mas que 2Mb
            if ($imagen['size'][$key] > 512000 || $imagen['size'][$key] == 0 ) { //si el archivo pesa mas que 500Kb
                $tamano = '1';
                break;
            }
        }
    }

    return $tamano; 
}

function moveFile($imagen, $directorio)
{
    $nameImage = array();
    $dir = FFTT . $directorio . "/";

    foreach ($imagen['name'] as $key => $value) {
        if (is_uploaded_file($imagen['tmp_name'][$key])) {
            
            if ($imagen['name'][$key]!= "") {
                $tmpName = explode(".", $imagen['name'][$key]);
                $imagenName = Logic_Utils::constructUrl($tmpName[0]) . '_' . rand(0, 100);
                $extension  = $tmpName[count($tmpName)-1];
                if (isset($imagen['tmp_name'][$key])) {
                    if ( move_uploaded_file($imagen['tmp_name'][$key], $dir . $imagenName.'.'.$extension) ) {
                        $nameImage[] = $imagenName.'.'.$extension;
                    }
                }
                
            }
        }
    }
    return $nameImage;
}



if (!isset($_GET['cmd']))
    $_GET['cmd'] = '';

switch ($_GET['cmd']) {
    case 'verFotoDetalle':
        verFotoDetalle($_POST['idarmarios_foto']);
        break;
    case 'listadoFotosArmario':
        listadoFotosArmario($_POST['mtgespkarm']);
        break;
    case 'registrarFotos':
        registrarFotos($_POST, $_FILES);
        break;
    case 'fotosArmario':
        $_POST['map'] = ( isset($_POST['map']) ) ? $_POST['map'] : '';
        fotosArmario($_POST['mtgespkarm'], $_POST['map']);
        break;
    case 'editarArmario':
        editarArmario($_POST['mtgespkarm']);
        break;
    case 'updateDatosArmario':
        updateDatosArmario($_POST);
        break;
    case 'cambiarPaginaArmario':
        cambiarPaginaArmario(
            $_POST['pagina'], $_POST['total'], $_POST['estado'], 
            $_POST['zonal'], $_POST['mdf'], $_POST['campo_valor'], 
            $_POST['order_field'], $_POST['order_type']
        );
        break;
    case 'filtroBusqueda':
        filtroBusqueda(
            $_POST['pagina'], $_POST['total'], $_POST['estado'], 
            $_POST['zonal'], $_POST['mdf'], $_POST['campo_valor'], 
            $_POST['order_field'], $_POST['order_type']
        );
        break;
    default:
        bandejaMain();
        break;
}