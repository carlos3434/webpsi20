<?php

/**
 * @package     modulos/maps/
 * @name        gescomp.poligono.php
 * @category    Controller
 * @author      Wilder Sandoval Huamani <wsandoval@gmd.com.pe>
 * @copyright   2011 Telefonica del Peru
 * @version     1.0 - 2011/08/25
 * @version     2.0 - 2013/05/03   -- Sergio Miranda
 */

//require_once "../../config/web.config.php";
//include_once APP_DIR . 'session.php';

include_once  'autoload.php';
//include_once 'js.php';

//require_once "bbb.php";

$cnxObj = new ConexionMapas();
$conexion = $cnxObj->conectarBD();


function generaColorPoligono()
{
    $arrValores = array('0','1','2','3','4','5','6','7','8','9',
            'A','B','C','D','E','F');
    
    $color = '';
    for ( $i=1; $i<=6; $i++ ) {
        $rand = rand(1, 15);
        $color .= $arrValores[$rand];
    }
    
    return '#' . $color;
}

function obtenerColorCaja($capAtenua, $qparlib='', $atenuacion='')
{
    //determinamos el color de la caja (capacidad/atenuacion)
    //colores de la caja (Por capacidad)
    //1: Verde
    //2: Amarillo
    //3: Rojo
    
    //colores de la caja (Por atenuacion)
    //1: Verde
    //2: Amarillo
    //3: Rojo
    //4: Blanco
    //5: Lila
    
    $colorCaja = 1;
    if ( $capAtenua == 'capacidad' ) {
        if ( $qparlib == 0 ) {
            $colorCaja = 3;
        } else if ( $qparlib > 0 &&
                $qparlib < 3 ) {
            $colorCaja = 2;
        } else if ( $qparlib > 2 ) {
            $colorCaja = 1;
        }
    } else if ( $capAtenua == 'atenuacion' ) {
        if ( $atenuacion == '' ) {
            $colorCaja = 4;
        } else if ( $atenuacion >= 0 && $atenuacion < 8 ) {
            $colorCaja = 4;
        } else if ( $atenuacion >= 8 && $atenuacion < 61 ) {
            $colorCaja = 1;
        } else if ( $atenuacion >= 61 && $atenuacion < 66 ) {
            $colorCaja = 2;
        } else if ( $atenuacion >= 66 ) {
            $colorCaja = 3;
        }
    }
    //FIN determinamos el color de la caja (capacidad/atenuacion)
    
    return $colorCaja;
}

function buscarCliente($post)
{
    try {
        
        global $conexion;
        
        $objTerminales = new Data_FfttTerminales();
        
        $arrClientesMarkers = array();
        
        $_POST['direccion'] = ( isset($_POST['direccion']) ) ? 
            $_POST['direccion'] : '';
        
        //busca en fftt_secundaria
        $arrClientesRs = $objTerminales->listClientes(
            $conexion, 'F', $_POST['telefono'], $_POST['cliente1'],
            $_POST['cliente2'], $_POST['direccion']
        );
        foreach ( $arrClientesRs as $clientesRs ) {
            $arrClientesMarkers[] = $clientesRs;
        }
        
        //busca en fftt_directa
        $arrClientesRd = $objTerminales->listClientes(
            $conexion, 'D', $_POST['telefono'], $_POST['cliente1'],
            $_POST['cliente2'], $_POST['direccion']
        );
        foreach ( $arrClientesRd as $clientesRd ) {
            $arrClientesMarkers[] = $clientesRd;
        }
        
        
        $arrDataResult = array(
            'edificios' => $arrClientesMarkers
        );
        
        echo json_encode($arrDataResult);
        exit;
        
    } catch (Exception $e) {
        $arrDataResult = array(
            'edificios' => array()
        );
        
        echo json_encode($arrDataResult);
        exit;
    }
}


function buscarXyTerminal ( $zonal, $mdf, $cable, $armario, $caja )
{
    global $conexion;

    /*
    echo 'zon=' . $zonal;
    echo 'mdf=' . $mdf;
    echo 'cab=' . $cable;
    echo 'arm=' . $armario;
    echo 'ter=' . $caja;
    */
    
    $arrGeoref = array(
        'x' => '',
        'y' => ''
    );
    
    //standarizando la caja
    if ( $caja != '' ) {
        if ( strlen($caja) == 1 ) {
            $caja = '00' . $caja;
        } elseif ( strlen($caja) == 2 ) {
            $caja = '0' . $caja;
        } elseif ( strlen($caja) == 3 ) {
            $caja = $caja;
        } elseif ( strlen($caja) == 4 ) {
            $caja = substr($caja, 1);
        }
    
        $mtgespktrm = $zonal . $mdf . $cable . $armario . $caja;
    
        $objterminal = new Data_FfttTerminales();
    
        $arrTerminal = $objterminal->getTerminalByMtgespktrm(
            $conexion, $mtgespktrm
        );

        if ( !empty($arrTerminal) ) {
            if ( $arrTerminal[0]['x'] != '' && $arrTerminal[0]['y'] != '' ) {
                $arrGeoref['x'] = $arrTerminal[0]['x'];
                $arrGeoref['y'] = $arrTerminal[0]['y'];
            }
        }
    }
    
    return $arrGeoref;
}

function buscarXyTap ( $nodo, $troba, $amplificador, $tap )
{
    global $conexion;
    
    /*
    echo 'nod=' . $nodo;
    echo 'tro=' . $troba;
    echo 'amp=' . $amplificador;
    echo 'tap=' . $tap;
    */

    $arrGeoref = array(
        'x' => '',
        'y' => ''
    );

    if ( strlen($tap) == 1 ) {
        $tap = '0' . $tap;
    } elseif ( strlen($tap) == 2 ) {
        $tap = $tap;
    } elseif ( strlen($tap) == 3 ) {
        $tap = substr($tap, 1);
    }
    
    $mtgespktap = $nodo . $troba . $amplificador . $tap;
    
    $objterminal = new Data_FfttTerminales();
    
    $arrTap = $objterminal->getDireccionTap($conexion, $mtgespktap);
    
    if ( !empty($arrTap) ) {
        if ( $arrTap[0]['x_coords'] != '' && $arrTap[0]['y_coords'] != '' ) {
            $arrGeoref['x'] = $arrTap[0]['x_coords'];
            $arrGeoref['y'] = $arrTap[0]['y_coords'];
        }
    }

    return $arrGeoref;
}

/**
 * Muestra mas informacion del terminal. Agregado el 20/06/2012
 *
 * @author Wilder Sandoval Huamani <wsandoval@gmd.com.pe>
 * @method masInformacionTerminal
 * @param array $_POST parametros enviados por POST
 * @return string)
 */
function masInformacionTerminal($post)
{
    try {
        global $conexion;
        
        $objTerminal = new Data_FfttTerminales();
        
        $mtgespktrm = $_POST['zonal'] . $_POST['mdf'] . $_POST['cable'] . 
            $_POST['armario'] . $_POST['caja'];
        
        $arrMasInfoTerminal = $objTerminal->obtenerMasDatosTerminal(
            $conexion, $mtgespktrm
        );
        
        include 'vistas/v_mas.informacion.terminal.php';
        
        
    } catch (Exception $e) {
        echo 'Se ha producido un error en el Sistema. Vuelva a intentarlo ' .
                'en unos minutos.';
        exit;
    }
        
}

/**
 * Busca los pedidos de pendientes o liquidadas. Agregado el 20/06/2012
 *
 * @author Wilder Sandoval Huamani <wsandoval@gmd.com.pe>
 * @method buscarPorPedido
 * @param array $_POST parametros enviados por POST

 * @return array (json)
 */
function buscarPorPedido($post)
{
    try {
        global $conexion;
        
        $arrPedidosEncontrados = array();
        if ( $_POST['estadoPedido'] == 'pen' ) {
            
            $objData  = new Data_Data();
            $objCampo = new Data_CampoBusqueda();
            
            // Tipo de Pedido
            $idTipoPedido = '';
            
            // Campo de busqueda
            $idCampo = $_POST['campo'];
            $objCampo = $objCampo->offsetGet($conexion, $idCampo);
            $campo = $objCampo->__get('_nombreSistema');
            
            // Valor a buscar
            $valor = $_POST['valor'];
            $valor = htmlentities($valor);
            $valor = ereg_replace("[^A-Za-z0-9]", "", $valor);
            
            // Indicador de Gestionadas
            $checkGestionadas = 0;
            
            // Indicador de Incidencia de Sistema
            $checkErrorSistema = 0;
            
            // Numero de pagina
            $pagina = 1;
            

            $arrParamConsulta = array();
            $arrParamConsulta[] = $idTipoPedido;
            $arrParamConsulta[] = $campo;
            $arrParamConsulta[] = trim($valor);
            $arrParamConsulta[] = $checkGestionadas;
            $arrParamConsulta[] = $checkErrorSistema;
            $arrParamConsulta[] = $pagina;
            
            $arrObjPedido = $objData->listar($conexion, $arrParamConsulta);
            
            if ( !empty($arrObjPedido) ) {
                foreach ($arrObjPedido as $objPedido) {
                    
                    $x = $objPedido->__get('_coordenadaX');
                    $y = $objPedido->__get('_coordenadaY');
                    
                    $cablePrimario = $objPedido->__get('_cablePrimario');
                    $armario = $objPedido->__get('_armario');
                    $terminal = $objPedido->__get('_terminal');
                    
                    $nodo = $objPedido->__get('_nodo');
                    $troba = $objPedido->__get('_troba');
                    $amplificador = $objPedido->__get('_amplificador');
                    $tap = $objPedido->__get('_tap');
                    
                    //pedido gestionado
                    if ( $objPedido->__get('_identificador') != '' ) {
    
                        if ( $objPedido->__get('_tipo') == 'Provision' ) {

                            $objGestProvision = new Data_GestProvision();
                            
                            $objProv = $objGestProvision->offsetGet(
                                $conexion, $objPedido->__get('_codigo')
                            );

                            if ($objProv) {
                                $x = $objProv->__get('_x');
                                $y = $objProv->__get('_y');
                                //seteando mas fftt
                                $cablePrimario = $objProv->__get('_c7');
                                $armario = $objProv->__get('_c6');
                                $terminal = $objProv->__get('_c8');
                                
                                $nodo = $objProv->__get('_mdf');
                                $troba = $objProv->__get('_c6');
                                $amplificador = $objProv->__get('_c7');
                                $tap = $objProv->__get('_c8');
                            }
                            
                        } elseif ( $objPedido->__get('_tipo') == 'Averia' ) {

                            $objAveria = new Data_GestAveria();

                            $objAver = $objAveria->offsetGet(
                                $conexion, $objPedido->__get('_codigo')
                            );

                            if ( $objAver ) {
                                $x = $objAver->__get('_ordenadaX');
                                $y = $objAver->__get('_ordenadaY');
                                //seteando mas fftt
                                $cablePrimario = $objAver->__get('_ccable');
                                $armario = $objAver->__get('_carmario');
                                $terminal = $objAver->__get('_terminal');
                                
                                $nodo = $objAver->__get('_mdf');
                                $troba = $objAver->__get('_carmario');
                                $amplificador = $objAver->__get('_cbloque');
                                $tap = $objAver->__get('_terminal');
                            }

                        }                        
                    }
                    //FIN pedido gestionado

                    $arrPedidosEncontrados[] = array(
                        'estadoPedido'=> 'pen',
                        'codigo'      => $objPedido->__get('_codigo'),
                        'tipo'        => $objPedido->__get('_tipo'),
                        'negocio'     => $objPedido->__get('_negocio'),
                        'inscripcion' => $objPedido->__get('_inscripcion'),
                        'telefono'    => $objPedido->__get('_telefono'),
                        'cliente'     => $objPedido->__get('_cliente'),
                        'fecha'   => obtenerFecha($objPedido->__get('_fecha')),
                        'fechaLiq'    => '',
                        'zonal'       => $objPedido->__get('_zonal'),
                        'mdf'         => $objPedido->__get('_mdf'),
                        'distrito'    => $objPedido->__get('_distrito'),
                        'empresa'     => $objPedido->__get('_empresa'),
                        'direccion'   => $objPedido->__get('_direccion'),
                        'x'           => $x,
                        'y'           => $y,
                        'cablePrimario' => $cablePrimario,
                        'parPrimario' => $objPedido->__get('_parPrimario'),
                        'armario'     => $armario,
                        'cableSecundario' => $objPedido->__get(
                            '_cableSecundario'
                        ),
                        'parSecundario' => $objPedido->__get('_parSecundario'),
                        'terminal'    => $terminal,
                        'nodo'        => $nodo,
                        'troba'       => $troba,
                        'amplificador' => $amplificador,
                        'tap'         => $tap
                    );
                }
            }
            
        } elseif ( $_POST['estadoPedido'] == 'liq' ) {
            
            $objLiquidadas = new Data_LiqMatProvAver();
            $arrPedidos = array();
            
            // Valor a buscar
            $valor = $_POST['valor'];
            $valor = htmlentities($valor);
            $valor = ereg_replace("[^A-Za-z0-9]", "", $valor);
            
            $mes  = date('m');
            $anio = date('Y');
            
            
            //verificando el pedido en el mes y anio actual
            //sino existe la tabla liquidadas_averias_provision_mmYYYY
            //devuelvo un array vacio
            try {
                $arrPedidos = $objLiquidadas->buscarPedido(
                    $conexion, $valor, $mes . $anio
                );
            } catch (Exception $e) {
                $arrPedidos = array();
            }
            
            
            //verifico si no se encontro el pedido para el mes y anio actual,
            //sino, busco el pedido en el mes anterior
            if ( empty($arrPedidos) ) {
                
                $mesAnteriorOk = $mes;
                $anioOk = $anio;
                if ( $mes == '01' ) {
                    $mesAnteriorOk = '12';
                    $cAnio = (int)$anio;
                    $anioOk = $cAnio - 1;
                } else {
                    $cMes = (int)$mes;
                    $mesAnterior = $cMes - 1;
                    
                    $mesAnteriorOk = ( strlen($mesAnterior) == 1 ) ?
                        '0' . $mesAnterior : $mesAnterior;
                }

                try {
                    $arrPedidos = $objLiquidadas->buscarPedido(
                        $conexion, $valor, $mesAnteriorOk . $anioOk
                    );
                } catch (Exception $e) {
                    $arrPedidos = array();
                }
            }

            if ( !empty($arrPedidos) ) {
                foreach ($arrPedidos as $pedido) {

                    $zonal = trim($pedido['zonal']);
                    $mdf = trim($pedido['mdf']);
                    
                    $cable = $armario = $caja = '';
                    $x = $y = '';
                    //validacion negocio = STB/ADSL
                    if ( $pedido['desc_neg'] == 'STB' ) {

                        $carmario = trim($pedido['C6']);
                        $ccable = trim($pedido['C7']);
                        $terminal = trim($pedido['C8']);
                
                        //si carmario <> vacio => tipo_red = Flexible
                        if ( $carmario != '' ) { // tipored = F
                            $cable = '';
                            $armario = $carmario;
                            $caja = $terminal;
                        } elseif ( $ccable != '' ) { //tipored = D
                            $cable = $ccable;
                            $armario = '';
                            $caja = $terminal;
                        }

                        //obtiene el xy del terminal
                        $arrGeoref = buscarXyTerminal(
                            $zonal, $mdf, $cable, $armario, $caja
                        );
                        $x = $arrGeoref['x'];
                        $y = $arrGeoref['y'];
                        
                
                    } elseif ( $pedido['desc_neg'] == 'ADS' || 
                        $pedido['desc_neg'] == 'ADSL' ) {

                        $carmario = trim($pedido['C6']);
                        $ccable = trim($pedido['C7']);
                        $terminal = trim($pedido['C8']);
                
                        //si carmario <> vacio => tipo_red = Flexible
                        if ( $carmario != '' ) { // tipored = F
                            $cable = '';
                            $armario = $carmario;
                            $caja = $terminal;
                        } elseif ( $ccable != '' ) { //tipored = D
                            $cable = $ccable;
                            $armario = '';
                            $caja = $terminal;
                        }
                        
                        //obtiene el xy del terminal
                        $arrGeoref = buscarXyTerminal(
                            $zonal, $mdf, $cable, $armario, $caja
                        );
                        $x = $arrGeoref['x'];
                        $y = $arrGeoref['y'];
                
                    } elseif ( $pedido['desc_neg'] == 'CATV' ) {
                
                        $nodo         = trim($pedido['mdf']);
                        $troba        = trim($pedido['C6']);
                        $amplificador = trim($pedido['C7']);
                        $tap          = trim($pedido['C8']);
                        
                        //obtiene el xy del tap
                        $arrGeoref = buscarXyTap(
                            $nodo, $troba, $amplificador, $tap
                        );
                        $x = $arrGeoref['x'];
                        $y = $arrGeoref['y'];
                
                    }

                    $arrPedidosEncontrados[] = array(
                        'estadoPedido'=> 'liq',
                        'codigo'      => $pedido['idliqmat'], //REVISAR
                        'tipo'        => $pedido['tipo'],
                        'negocio'     => $pedido['desc_neg'],
                        'inscripcion' => $pedido['CODCLI'],
                        'telefono'    => $pedido['telefono'],
                        'cliente'     => $pedido['cliente'],
                        'fecha'       => obtenerFecha($pedido['fecreg']),
                        'fechaLiq'    => $pedido['fec_liq'],
                        'zonal'       => $pedido['zonal'],
                        'mdf'         => $pedido['mdf'],
                        'distrito'    => $pedido['distrito'],
                        'empresa'     => $pedido['desc_empresa'],
                        'direccion'   => $pedido['dir'],
                        'x'           => $x,
                        'y'           => $y,
                        'c6'          => $pedido['C6'],
                        'c7'          => $pedido['C7'],
                        'c8'          => $pedido['C8'],
                        'c9'          => $pedido['C9'],
                        'c10'         => $pedido['C10']
                    );
                }
            }
        }
        
        //devuelve a la vista para pintar en el mapa
        $arrDataResult = array(
            'flag'      => '1',
            'edificios' => $arrPedidosEncontrados
        );

        echo json_encode($arrDataResult);
        exit;

    } catch (Exception $e) {
        $arrDataResult = array(
            'flag'      => '0',
            'edificios' => array(),
            'msg'       => $e->getMessage()
        );

        echo json_encode($arrDataResult);
        exit;
    }
}

function buscarCapasRuta($post)
{
    global $conexion;

    $script = '<script type="text/javascript">
        $(function(){
            $("select.clsMultiple").multiselect().multiselectfilter();
        });
    </script>';

    $arrRutas = array();
    if ( isset($_POST['idpoligono_tipo']) && $_POST['idpoligono_tipo'] != '' ) {
        $objPoligono = new Data_Poligono();
        $arrRutas = $objPoligono->listar(
            $conexion, $_POST['idpoligono_tipo'], '1'
        );
    }

    $arrDataResult = '<select name="capaRuta" id="capaRuta" ';
    $arrDataResult .= 'style="font-size: 11px;height: 20px;padding: 1px;" ';
    $arrDataResult .= 'class="clsMultiple" multiple="multiple">';
    if ( !empty($arrRutas) ) {
        foreach ($arrRutas as $ruta) {
            if ( $ruta['idpoligono'] != '' ) {
                $arrDataResult .= '<option value="' . $ruta['idpoligono'] . 
                    '">' . $ruta['nombre'] . '</option>';
            }
        }
    }
    $arrDataResult .= '</select>';
    
    echo $script . $arrDataResult;
    exit;
}

function buscarCapasPoligono($post)
{
    global $conexion;

    $script = '<script type="text/javascript">
        $(function(){
            $("select.clsMultiple").multiselect().multiselectfilter();
        });
    </script>';

    $arrPoligonos = array();
    if ( isset($_POST['idpoligono_tipo']) && $_POST['idpoligono_tipo'] != '' ) {
        $objPoligono = new Data_Poligono();
        $arrPoligonos = $objPoligono->listar(
            $conexion, $_POST['idpoligono_tipo'], '1'
        );
    }
    
    $arrDataResult = '<select name="capaPoligono" id="capaPoligono" ';
    $arrDataResult .= 'style="font-size: 11px;height: 20px;padding: 1px;" ';
    $arrDataResult .= 'class="clsMultiple" multiple="multiple">';
    if ( !empty($arrPoligonos) ) {
        foreach ($arrPoligonos as $poligono) {
            if ( $poligono['idpoligono'] != '' ) {
                $arrDataResult .= '<option value="' . 
                    $poligono['idpoligono'] . '">';
                $arrDataResult .= $poligono['nombre'] . '</option>';
            }
        }
    }
    $arrDataResult .= '</select>';
    
    echo $script . $arrDataResult;
    exit;
}

function buscarDistritos($post)
{
    global $conexion;

    $script = '<script type="text/javascript">
        $(function(){
            $("select.clsMultiple").multiselect().multiselectfilter();
        });
    </script>';

    if ( isset($_POST['zonales_array']) && !empty($_POST['zonales_array']) ) {

        $objTerminales = new Data_FfttTerminales();
        $arrDistrito = $objTerminales->getDistritoByZonalFromFfttDirecta(
            $conexion, '', $_POST['zonales_array']
        );
            
        $arrData = array();
        foreach ($arrDistrito as $distrito) {
            if ( $distrito['distrito'] != '' ) {
                $arrData[] = array(
                    'distrito' => $distrito['distrito']
                );
            }
        }

        $selDistrito = '<select id="selDistrito" name="selDistrito" ';
        $selDistrito .= 'class="clsMultiple" multiple="multiple">';
        foreach ( $arrData as $data ) {
            $selDistrito .= '<option value="' . $data['distrito'] . '">' . 
                $data['distrito'] . '</option>';
        }
        $selDistrito .= '</select>';    
        
    } else {
        $selDistrito = '<select id="selDistrito" name="selDistrito" ';
        $selDistrito .= 'class="clsMultiple" multiple="multiple">';
        $selDistrito .= '<option value="">Todos</option>';
        $selDistrito .= '</select>';
    }
    
    echo $script . $selDistrito;
    exit;
}

function buscarDireccion($post)
{
    try {
        
        global $conexion;
        
        $objTerminales = new Data_FfttTerminales();
        
        $arrClientesMarkers = array();
        
        $numeros = array();
        if ( isset($_POST['numero']) && trim($_POST['numero']) != '' ) {
        
            $pNumero = (int)$_POST['numero'];
        
            if ( $pNumero >= 2) {
                $numeros = array( $pNumero-1, $pNumero, $pNumero+1 );
            } else {
                $numeros = array( $pNumero, $pNumero+1 );
            }
        }
        
        //busca en fftt_secundaria
        $arrClientesRs = $objTerminales->listClientesDireccion(
            $conexion, 'F', $_POST['zonal'], $_POST['distrito'],
            $_POST['direccion1']
        );
        foreach ( $arrClientesRs as $clientesRs ) {
        
            $string = str_replace(' ', '', trim($clientesRs['Direccion']));
        
            if ( !empty($numeros) ) {
                foreach ( $numeros as $num ) {
                    $pos = strpos($string, (string)$num);
        
                    if ($pos !== false) {
                        $caracterAnt = substr($string, $pos-1, 1);
                        //si es caracter => si es valido
                        if ( !ctype_digit($caracterAnt) ) {
                            $arrClientesMarkers[] = $clientesRs;
                        }
                    }
                }
            } else {
        
                $arrClientesMarkers[] = $clientesRs;
            }
        }
        
        //busca en fftt_directa
        $arrClientesRd = $objTerminales->listClientesDireccion(
            $conexion, 'D', $_POST['zonal'], $_POST['distrito'],
            $_POST['direccion1']
        );
        foreach ( $arrClientesRd as $clientesRd ) {
        
            $string = str_replace(' ', '', trim($clientesRd['Direccion']));
        
            if ( !empty($numeros) ) {
                foreach ( $numeros as $num ) {
                    $pos = strpos($string, (string)$num);
        
                    if ($pos !== false) {
                        $caracterAnt = substr($string, $pos-1, 1);
                        //si es caracter => si es valido
                        if ( !ctype_digit($caracterAnt) ) {
                            $arrClientesMarkers[] = $clientesRd;
                        }
                    }
                }
            } else {
        
                $arrClientesMarkers[] = $clientesRd;
            }
        }
        
        
        $arrDataResult = array(
            'edificios' => $arrClientesMarkers
        );
        
        echo json_encode($arrDataResult);
        exit;
        
    } catch (Exception $e) {
        $arrDataResult = array(
            'edificios' => array()
        );
        
        echo json_encode($arrDataResult);
        exit;
    }
}

function buscarLocalidad($post)
{
    try {
        global $conexion;
        
        $objUbigeo = new Data_Ubigeo();
        
        $arrMarkers = array();
        
        $codDpto = $_POST['dpto'];
        $codProv = $_POST['prov'];
        $codDist = $_POST['dist'];
        $localidad = $_POST['localidad'];
        
        $arrObjLocalidades = $objUbigeo->buscarLocalidad(
            $conexion, $codDpto, $codProv, $codDist, $localidad
        );
        if ( !empty($arrObjLocalidades) ) {
            foreach ( $arrObjLocalidades as $objUbigeo ) {
                $arrMarkers[] = array(
                    'x' => $objUbigeo->__get('_x'),
                    'y' => $objUbigeo->__get('_y'),
                    'departamento' => $objUbigeo->__get('_departamento'),
                    'provincia' => $objUbigeo->__get('_provincia'),
                    'distrito' => $objUbigeo->__get('_distrito'),
                    'localidad' => $objUbigeo->__get('_localidad'),
                    'ubigeo' => $objUbigeo->__get('_ubigeo')
                );
            }
        }
        
        $arrDataResult = array(
                'edificios' => $arrMarkers
        );
        
        echo json_encode($arrDataResult);
        exit;
        
    } catch (Exception $e) {
        $arrDataResult = array(
            'edificios' => array()
        );
        
        echo json_encode($arrDataResult);
        exit;
    }
}

function buscarTrmTipoRed($post)
{
    try {
        global $conexion;
    
        $objZonal = new Data_Zonal();
        $objTerminales = new Data_FfttTerminales();
        
        //carga de datos (x,y) de Zonal
		
        $arrZonal = $objZonal->listar($conexion, '', $_POST['zonal']);
        $arrDataZonal = array(
            'abv'   => $arrZonal[0]->__get('_abvZonal'),
            'name'  => $arrZonal[0]->__get('_descZonal'),
            'x'     => $arrZonal[0]->__get('_x'),
            'y'     => $arrZonal[0]->__get('_y')
        );
        
        //carga de datos (x,y) de Mdf
		//
		// SERGIO 2013-03-18 --> POR EL MOMENTO NO BUSCAR MDFS POR USUARIO
        //$idUsuario = $_SESSION['USUARIO']->__get('_idUsuario');
		$idUsuario = 1235;
		
        $arrMdf = $objTerminales->getMdfByCodMdf($conexion, $_POST['mdf']);
        $arrDataMdf = array(
            'abv'    => $arrMdf[0]['mdf'],
            'name'    => $arrMdf[0]['nombre'],
            'estado'=> $arrMdf[0]['estado'],
            'x'     => $arrMdf[0]['x'],
            'y'     => $arrMdf[0]['y']
        );
        
        $arrDataArmario = array();
        if ( $_POST['tipred'] == 'F' ) {
            //carga de datos (x,y) de Armario
            $arrArmario = $objTerminales->getArmarioByCodArmario(
                $conexion, $_POST['zonal'], $_POST['mdf'],
                $_POST['cableArmario']
            );
            $arrDataArmario = array(
                'zonal'   => $arrArmario[0]['zonal'],
                'mdf'     => $arrArmario[0]['mdf'],
                'armario' => $arrArmario[0]['armario'],
                'x'       => $arrArmario[0]['x'],
                'y'       => $arrArmario[0]['y'],
                'estado'  => $arrArmario[0]['estado']
            );
        }
        
        
        $arrTerminalesMarkers = array();
        $arrTerminalesConXy = array();

        $arrTerminales = $objTerminales->getTerminales(
            $conexion, $_POST['zonal'], $_POST['mdf'], $_POST['tipred'], 
            $_POST['cableArmario']
        );
        
        $tipo = 'capacidad';
        if ( $_POST['tipo'] == 'atenua' ) {
            $tipo = 'atenuacion';
        } 
    
        if ( !empty($arrTerminales) ) {
            foreach ( $arrTerminales as $terminales ) {
                
                if ( $terminales['y'] != '' && $terminales['x'] != '' ) {
                    $arrTerminalesConXy[] = $terminales;
                }
                
                if ( $tipo == 'capacidad' ) {
                    $terminales['color_caja'] = obtenerColorCaja(
                        'capacidad', $terminales['qparlib'], ''
                    );
                } elseif ( $tipo == 'atenuacion' ) {
                    $terminales['color_caja'] = obtenerColorCaja(
                        'atenuacion', '', $terminales['atenuacion']
                    );
                }
                
                $arrTerminalesMarkers[] = $terminales;
            }
        }
        
        $numTerminalesConXy = 0;
        if ( !empty($arrTerminalesConXy) ) {
            $numTerminalesConXy = count($arrTerminalesConXy);
        }
        
        $arrDataResult = array(
            'zonal'     => $arrDataZonal,
            'mdf'       => $arrDataMdf,
            'armario'   => $arrDataArmario,
            'edificios' => $arrTerminalesMarkers,
            'num_edificios_con_xy' => $numTerminalesConXy
        );
        
        echo json_encode($arrDataResult);
        exit;
        
    } catch (Exception $e) {
        $arrDataResult = array(
            'edificios' => array()
        );
    
        echo json_encode($arrDataResult);
        exit;
    }
}


/* SERGIO 2013-10-14 */
function buscarTrmAsignarXY_clientes($post)
{
    try {
        global $conexion;
    
        $objTerminales = new Data_FfttTerminales_Smc();
    
        $cableArmario = $_POST["cableArmario"];
        $caja = $_POST["caja"];
        $tipored = $_POST["tipred"];
        $marcaSpd = "";

        $arrClientes = $objTerminales->getClientesPorTerminal($conexion, $_POST['zonal'], $_POST['mdf'],  $cableArmario, 
            $cableArmario, $caja, $tipored, $marcaSpd);

        $i = 0;
        foreach ( $arrClientes as $clientes ) {
                $arrClientesFin[$i]['zonal'] = $clientes["zonalgestel"];
                $arrClientesFin[$i]['mdf'] = $clientes["mdf"];
                $arrClientesFin[$i]['armario_cable'] = $clientes["armario_cable"];
                $arrClientesFin[$i]['caja'] = $clientes["caja"];
                $arrClientesFin[$i]['cliente'] = $clientes["cliente"];
                $arrClientesFin[$i]['direccion'] = $clientes["direccion"];
                $i++;
        }

        //var_dump($arrClientesFin);

        $arrDataResult = array(
            'edificios' => $arrClientesFin
        );

        echo json_encode($arrDataResult);
        exit;
        
    } catch (Exception $e) {
        $arrDataResult = array(
            'edificios' => array()
        );
    
        echo json_encode($arrDataResult);
        exit;
    }
}


function buscarTrmUbigeo($post)
{
    try {
        
        global $conexion;
        
        $objTerminales = new Data_FfttTerminales();

        $arrTerminalesMarkers = array();
        $arrTerminalesConXy = array();
        
        //busca terminales por Ubigeo / sector y mza
        $arrTerminales = $objTerminales->getTerminalesByUbigeo(
            $conexion, $_POST['dpto'], $_POST['prov'], $_POST['dist'],
            $_POST['sector'], $_POST['mza']
        );
        
        if ( !empty($arrTerminales) ) {
            foreach ( $arrTerminales as $terminales ) {
                
                if ( $terminales['y'] != '' && $terminales['x'] != '' ) {
                    $arrTerminalesConXy[] = $terminales;
                }
                
                $terminales['color_caja'] = obtenerColorCaja(
                    'capacidad', $terminales['qparlib'], ''
                );
                
                $arrTerminalesMarkers[] = $terminales;
            }
        }
        
        $numTerminalesConXy = 0;
        if ( !empty($arrTerminalesConXy) ) {
            $numTerminalesConXy = count($arrTerminalesConXy);
        }
        
        $arrDataResult = array(
            'edificios' => $arrTerminalesMarkers,
            'num_edificios_con_xy' => $numTerminalesConXy
        );
        
        echo json_encode($arrDataResult);
        exit;
        
    } catch (Exception $e) {
        $arrDataResult = array(
            'edificios' => array()
        );
        
        echo json_encode($arrDataResult);
        exit;
    }
}


function buscarProvincias($post)
{
    global $conexion;

    $objUbigeo = new Data_Ubigeo();

    $arrProvincias = array();
    $codDpto = $_POST['dpto'];
    
    $arrObjProvincias = $objUbigeo->provinciasUbigeo($conexion, $codDpto);
    
    if ( !empty($arrObjProvincias) ) {
        foreach ( $arrObjProvincias as $objProvincia ) {
            $arrProvincias[] = array(
                'codprov' => $objProvincia->__get('_codProv'),
                'nombre'  => $objProvincia->__get('_nombre')
            );
        }
    }

    echo json_encode($arrProvincias);
    exit;
}

function buscarProvicniasDistritos($post)
{
    global $conexion;

    $objUbigeo = new Data_Ubigeo();

    $arrDistritos = array();
    $codDpto = $_POST['dpto'];
    $codProv = $_POST['prov'];
    
    $arrObjDistritos = $objUbigeo->distritosUbigeo(
        $conexion, $codDpto, $codProv
    );
    
    if ( !empty($arrObjDistritos) ) {
        foreach ( $arrObjDistritos as $objUbigeo ) {
            $arrDistritos[] = array(
                'coddist' => $objUbigeo->__get('_codDist'),
                'nombre'  => $objUbigeo->__get('_nombre')
            );
        }
    }

    echo json_encode($arrDistritos);
    exit;
}

function buscarRuta($post)
{
    global $conexion;

    $objPoligono = new Data_Poligono();

    $arrRutas = array();
    
    //lleno el array para la ruta
    if ( !empty( $_POST['arreglo_capaRuta'] ) ) {
        foreach ( $_POST['arreglo_capaRuta'] as $capaRuta ) {
            //capturando los X,Y de los elementos seleccionados
            $arrPuntosXyCapaRuta = $objPoligono->listXYRutaPoligono(
                $conexion, $capaRuta
            );
            $arrRutas[] = array(
                'coords' => $arrPuntosXyCapaRuta,
                'color'  => generaColorPoligono(),
                'tipo'   => $arrPuntosXyCapaRuta[0]['nombre_poligono_tipo'],
                'nombre' => $arrPuntosXyCapaRuta[0]['nombre_poligono']
            ); 
        }
    }
    
    $x = $y = '';
    $promX = $promY = 0;
    if ( !empty($arrRutas) ) {
        //capturando el primer punto de las capas
        $x = $arrRutas[0]['coords'][0]['x'];
        $y = $arrRutas[0]['coords'][0]['y'];


        $countXy = count($arrRutas[0]['coords']);

        $sumX = $sumY = 0;
        foreach ( $arrRutas[0]['coords'] as $puntoXY ) {
                $sumX += $puntoXY['x'];
                $sumY += $puntoXY['y'];
        }

        $promX = $sumX / $countXy;
        $promY = $sumY / $countXy;
    }
        
    $arrDataResult = array(
        'xy_rutas'   => $arrRutas,
        'x'          => $x,
        'y'          => $y,
        'center_map' => array(
            'x' => $promX,
            'y' => $promY
        )
    );
    
    echo json_encode($arrDataResult);
    exit;
}

function buscarPoligono($post)
{
    global $conexion;

    $objPoligono = new Data_Poligono();

    $arrPoligonos = array();
    
    //lleno el array para la ruta
    if ( !empty( $_POST['arreglo_capaPoligono'] ) ) {
        foreach ( $_POST['arreglo_capaPoligono'] as $capaPoligono ) {
            //capturando los X,Y de los elementos seleccionados
            $puntosXyCapaPoligono = $objPoligono->listXYRutaPoligono(
                $conexion, $capaPoligono
            );
            
            if ( !empty($puntosXyCapaPoligono) ) {
                $arrPoligonos[] = array(
                    'coords' => $puntosXyCapaPoligono,
                    'color'  => generaColorPoligono(),
                    'tipo' => $puntosXyCapaPoligono[0]['nombre_poligono_tipo'],
                    'nombre' => $puntosXyCapaPoligono[0]['nombre_poligono'],
                );
            }
        }
    }
    
        
    $x = $y = '';
    $promX = $promY = 0;
    if ( !empty($arrPoligonos) ) {
        
        //capturando el primer punto de las capas
        $x = $arrPoligonos[0]['coords'][0]['x'];
        $y = $arrPoligonos[0]['coords'][0]['y'];


        $countXy = count($arrPoligonos[0]['coords']);

        $sumX = $sumY = 0;
        foreach ( $arrPoligonos[0]['coords'] as $puntoXY ) {
                $sumX += $puntoXY['x'];
                $sumY += $puntoXY['y'];
        }

        $promX = $sumX / $countXy;
        $promY = $sumY / $countXy;
        
    }

    $arrDataResult = array(
        'xy_poligonos' => $arrPoligonos,
        'x'            => $x,
        'y'            => $y,
        'center_map'   => array(
            'x' => $promX,
            'y' => $promY
        )
    );
    
    echo json_encode($arrDataResult);
    exit;
}

function cargaInicial()
{
    global $conexion;
    
    $objPoligonoTipo = new Data_PoligonoTipo();
    $objTerminales = new Data_FfttTerminales();
    $objUbigeo = new Data_Ubigeo();
    $objTipoPedido = new Data_TipoPedido();
    $objUsuario = new Data_Usuario();

    
    $arrTipoPoligonos = $objPoligonoTipo->listar($conexion, 'P', '', '1');
    $arrTipoRutas = $objPoligonoTipo->listar($conexion, 'R', '', '1');
    
	// SERGIO: 2013-03-16 -- Se quita filtro de zonales por usuario
    /*$arrZonales  = $objTerminales->getZonalesByIdusuario(
        $conexion, $_SESSION['USUARIO']->__get('_idUsuario')
    );
    $arrDistritos = $objTerminales->getDistritoByZonalFromFfttDirecta(
        $conexion, 'LIMA'
    );
    */
	
	
	$arrZonales  = $objTerminales->getZonalesByIdusuario(
        $conexion, 1
    );
	
	
    $arrDistritos = $objTerminales->getDistritoByZonalFromFfttDirecta(
        $conexion, 'LIMA'
    );
	
	
	
    $arrObjDptos = $objUbigeo->departamentosUbigeo($conexion);
    $arrObjProvs = $objUbigeo->provinciasUbigeo($conexion, '15');
    $arrObjDists = $objUbigeo->distritosUbigeo($conexion, '15', '01');
    
    
    $arrDptos = array();
    foreach ( $arrObjDptos as $objUbigeo ) {
         $arrDptos[] = array(
             'coddpto' => $objUbigeo->__get('_codDpto'),
             'nombre'  => $objUbigeo->__get('_nombre')
         );
    }
    
    $arrProvs = array();
    foreach ( $arrObjProvs as $objUbigeo ) {
        $arrProvs[] = array(
            'codprov' => $objUbigeo->__get('_codProv'),
            'nombre'  => $objUbigeo->__get('_nombre')
        );
    }
    
    $arrDists = array();
    foreach ( $arrObjDists as $objUbigeo ) {
        $arrDists[] = array(
            'coddist' => $objUbigeo->__get('_codDist'),
            'nombre'  => $objUbigeo->__get('_nombre')
        );
    }
    
    $arrTipoPedido = array();
    if ( !empty($_SESSION['USUARIO_TIPO_PEDIDO']) ) {
        foreach ( $_SESSION['USUARIO_TIPO_PEDIDO'] as $objUsuarioTipoPedido ) {
            $objTipoPedido = $objTipoPedido->offsetGet(
                $conexion, $objUsuarioTipoPedido->__get('_idTipoPedido')
            );
            
            $arrTipoPedido[] = array(
                'tipoPedido' => $objTipoPedido->__get('_tipoPedido')
            );
            
        }
    }
    
    //wsandoval 28/06/2012
    //Opciones para modulo de Georeferencia
	
	// SERGIO: 2013-03-16 -- Se quita filtro de zonales por usuario
    /*$objUsuario = $objUsuario->offsetGet(
        $conexion, $_SESSION['USUARIO']->__get('_idUsuario')
    );*/
	$objUsuario = $objUsuario->offsetGet(
        $conexion, 1
    );
	
	// SERGIO: 2013-03-16 -- Se quita filtro de zonales por usuario
    
	/*$	flagGeoRef = $objUsuario->__get('_flagGeoRef');
		$arrGeoRef = unserialize($flagGeoRef);
	*/
	$flagGeoRef = 'a:2:{s:11:"verClientes";s:1:"1";s:15:"masInfoTerminal";s:1:"1";}';
    $arrGeoRef = unserialize($flagGeoRef);
	
    //fin wsandoval 28/06/2012
    
    
    $arrDataResult = array(
        'arrTipoPoligonos' => $arrTipoPoligonos,
        'arrTipoRutas'     => $arrTipoRutas,
        'arrZonales'       => $arrZonales,
        'arrDistritos'     => $arrDistritos,
        'arrDptos'         => $arrDptos,
        'arrProvs'         => $arrProvs,
        'arrDists'         => $arrDists,
        'arrTipoPedido'    => $arrTipoPedido,
        'arrGeoRef'        => $arrGeoRef
    );
    
    echo json_encode($arrDataResult);
    exit;
}

function grabarRutaAcometida($arrDatos)
{
    try {
        global $conexion;

        $idUsuario   = $_SESSION['USUARIO']->__get('_idUsuario');
        $objExcesoAcometida = new Data_ExcesoAcometida();
        $objTmpGaudi = new Data_TmpGaudi();
        $objPenPais = new Data_PenPais();


        //si chkPedido = 1, entonces valido el pedido en la BD
        $strPedido = '';
        $objPedido = array();
        if ( $arrDatos['chkPedido'] == 1 ) {

            if ( $arrDatos['tipo_pedido'] == 'Provision' ) {
                //peticion
                $objPedido = $objTmpGaudi->offsetGet(
                    $conexion, $arrDatos['pedido']
                );
                print_r($objPedido);
                if ( isset($objPedido) ) {
                    $strPedido = $objPedido->__get('_data17');
                }
            } else if ( $arrDatos['tipo_pedido'] == 'Averia' ) {
                //averia
                $objPedido = $objPenPais->offsetGet(
                    $conexion, $arrDatos['pedido']
                );
                if ( isset($objPedido) ) {
                    $strPedido = $objPedido->__get('_averia');
                }
            }

        } else {
            $strPedido = $arrDatos['pedido'];
        }


        if ( trim($strPedido) == '' ) {
            $dataResult = array(
                    'flag'=>'2',
                    'mensaje'=>'El pedido que ha ingresado no es un ' .
                    'pedido valido'
            );
            echo json_encode($dataResult);
            exit;

        } else {

            $arrObjExcesoAcometida = $objExcesoAcometida->obtenerPorPedido(
                $conexion, $arrDatos['tipo_pedido'], $arrDatos['pedido']
            );

            if ( !empty($arrObjExcesoAcometida) ) {

                $dataResult = array(
                        'flag'=>'3',
                        'mensaje'=>'El pedido: ' . $arrDatos['tipo_pedido'] . ' ' .
                        $arrDatos['pedido'] . ' ya se encuentra registrado.'
                );

            } else {
                //generando el codigo
                $arrMaxId = $objExcesoAcometida->maximoCodigo($conexion);
                $codigoExAc = $arrDatos['mdf'] . date('Ymd') . '-1';
                if ( !empty($arrMaxId) ) {
                    $maxIdNuevo = $arrMaxId[0]['maxExcesoAcometida'] + 1;
                    $codigoExAc = $arrDatos['mdf'].date('Ymd').'-'.$maxIdNuevo;
                }

                $arrDatos['codigo'] = $codigoExAc;


                //parseando la ruta
                $arrRutasXy = array();
                if ( isset($arrDatos['ruta']) && !empty($arrDatos['ruta']) ) {
                    $i=1;
                    foreach ( $arrDatos['ruta'] as $strRuta ) {
                        $arrRutas = explode('|', $strRuta);
                        $arrRutasXy[] = array(
                                'orden'    => $i,
                                'x'        => $arrRutas[0],
                                'y'        => $arrRutas[1],
                                'nombre_poligono' => 'Exceso Acometida',
                                'nombre_poligono_tipo' => $arrDatos['codigo']
                        );
                        $i++;
                    }
                }

                //serializando la ruta
                $arrDatos['ruta'] = serialize($arrRutasXy);


                //Grabando la ruta de la acometida
                $objExcesoAcometida->grabar($conexion, $arrDatos, $idUsuario);

                $dataResult = array(
                        'flag'    => '1',
                        'mensaje' => 'Datos grabados correctamente.',
                        'codGen'  => $codigoExAc
                );
            }


        }


        echo json_encode($dataResult);
        exit;

    } catch (PDOException $e) {
        $dataResult = array(
                'flag'=>'0',
                'mensaje'=>'Hubo un error al registar la ruta de Acometida, ' .
                'intentelo nuevamente.',
                'mensaje2'=>$e->getMessage()
        );
        echo json_encode($dataResult);
        exit;
    }

}

function vistaInicial()
{
    //template por default
	
	//echo "PIntar cabecera = ".pintar_cabecera(); 
    $template = '<iframe id="ifrMain" name="ifrMain" ';
    $template .= 'src="vistas/v_gescomp_poligono.ifr.php" ';
    $template .= 'width="100%" height="480"></iframe>';
    
    echo $template;

}


if (!isset($_REQUEST['action']))
    $_REQUEST['action'] = '';

switch ($_REQUEST['action']) {
    case 'masInformacionTerminal':
        masInformacionTerminal($_POST);
        break;
    case 'buscarPorPedido':
        buscarPorPedido($_POST);
        break;
    case 'buscarTrmTipoRed':
        buscarTrmTipoRed($_POST);
        break;
    case 'buscarTrmAsignarXY_clientes':
        buscarTrmAsignarXY_clientes($_POST);
        break;
    case 'buscarTrmUbigeo':
        buscarTrmUbigeo($_POST);
        break;
    case 'grabarRutaAcometida':
        grabarRutaAcometida($_POST);
        break;
    case 'buscarCliente':
        buscarCliente($_POST);
        break;
    case 'buscarCapasRuta':
        buscarCapasRuta($_POST);
        break;
    case 'buscarCapasPoligono':
        buscarCapasPoligono($_POST, $_GET);
        break;
    case 'buscarDistritos':
        buscarDistritos($_POST);
        break;
    case 'buscarDireccion':
        buscarDireccion($_POST);
        break;
    case 'buscarLocalidad':
        buscarLocalidad($_POST);
        break;
    case 'buscarProvincias':
        buscarProvincias($_POST);
        break;
    case 'buscarProvicniasDistritos':
        buscarProvicniasDistritos($_POST);
        break;
    case 'buscarRuta':
        buscarRuta($_POST);
        break;
    case 'buscarPoligono':
        buscarPoligono($_POST);
        break;
    case 'cargaInicial':
        cargaInicial();
        break;
    default:
        vistaInicial();
        break;
}

