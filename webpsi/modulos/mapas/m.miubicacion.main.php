<?php

/**
 * @package     /modulos/maps
 * @name        m.miubicacion.main.php
 * @category    Controller
 * @author      Wilder Sandoval Huamani <wsandoval@gmd.com.pe>
 * @copyright   2011 Telefonica del Peru
 * @version     1.0 - 2011/10/25
 */

require_once "../../config/web.config.php";
include_once APP_DIR . 'autoload.php';
include_once APP_DIR . 'session.php';

$isLogin = 0;
$dirModulo = '../../';
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML Mobile 1.0//EN" "http://www.wapforum.org/DTD/xhtml-mobile10.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="viewport" content="width=device-width; height=device-height; maximum-scale=1.4; initial-scale=1.0; user-scalable=yes"/>
<title>Webunificada - Movil</title>
<script type="text/javascript" src="../../js/jquery/jquery-latest.js"></script>
<script type="text/javascript">
$(document).ready(function() {
    $('#tblPrincipal tr.lnk').click(function(event) {
            $(this).toggleClass('rowSelected');
			
			var href = $(this).find("a").attr("href");
            window.top.location = href;
    });
});
</script>
<link href="../../css/movil.css" rel="stylesheet" type="text/css" />
</head>
<body>

<?php include '../../m.header.php';?>

<table id="tblPrincipal" class="principal" width="100%">
<tr>
	<td class="titulo">Mi ubicaci&oacute;n</td>
</tr>
<tr class="lnk">
	<td class="list"><span class="imgList"><img src="../../img/btn_modulos_24.png"></span><a href="m.buscomp.main.php">Buscar componentes</a></td>
</tr>
</table>

</body>
</html>