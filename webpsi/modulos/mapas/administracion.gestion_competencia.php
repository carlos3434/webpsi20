<?php

/**
 * [Web Unificada] :: Controlador de la Administracion de Responsable de Campo.
 * @package    /modulos/maps/
 * @name       administracion.responsable.campo.php
 * @category   Controller
 * @author     Fernando Esteban Valerio <festeban@gmd.com.pe>
 * @copyright  GMD S.A.
 * @version    1.0
 */

class AdministracionGestionCompetencia extends Data_GestionCompetenciaEdi
{
    /**
     * Directorio
     * @access protected
     * @var string
     */
    protected $_ruta;
    
    /**
     * Numero de pagina
     * @access protected
     * @var int
     */
    protected $_pagina;
    
    /**
     * @method  __construct
     * Metodo constructor de la clase
     */
    public function __construct()
    {
        $this->_pagina = 1;
        $this->_ruta = 'vistas/administracion/gestion/';
    }
    
    /**
     * @author Fernando Esteban Valerio <festeban@gmd.com.pe>
     * @method getRuta
     * Obtener el valor del atributo "ruta".
     * @return string
     */
    public function getRuta()
    {
        return $this->_ruta;
    }
    
    /**
     * @author Fernando Esteban Valerio <festeban@gmd.com.pe>
     * @method setRuta
     * Asignar un valor al atributo "ruta".
     * @param string $ruta Valor a asignar.
     */
    public function setRuta($ruta)
    {
        $this->_ruta = $ruta;
    }
    
    /**
     * @author Fernando Esteban Valerio <festeban@gmd.com.pe>
     * @method getPagina
     * Obtener el valor del atributo "pagina".
     * @return string
     */
    public function getPagina()
    {
        return $this->_pagina;
    }
    
    /**
     * @author Fernando Esteban Valerio <festeban@gmd.com.pe>
     * @method setRuta
     * Asignar un valor al atributo "pagina".
     * @param string $pagina Valor a asignar.
     */
    public function setPagina($pagina)
    {
        $this->_pagina = $pagina;
    }

    /**
     * @author Fernando Esteban Valerio <festeban@gmd.com.pe>
     * @method viewAddGestion
     * Mostrar el formulario de registro de la gestion de competencia.
     * @global conexion $conexion Objeto de conexion a la BD creada con PDO.
     * @return void
     */
    public function viewAddGestion()
    {
        global $conexion;
    
        $ruta = $this->getRuta() . 'v_gestion_add.phtml';
        include $ruta;
    }
    
    /**
     * @author Fernando Esteban Valerio <festeban@gmd.com.pe>
     * @method editGestion
     * Mostrar el formulario de edicion de la gestion de competencia.
     * @global conexion $conexion Objeto de conexion a la BD creada con PDO.
     * @param int $id Id de la gestion de competencia.
     * @return void
     */
    public function editGestion($id)
    {
        global $conexion;
    
        if (is_numeric($id)) {            
            $objeto = $this->obtener($id);
            if ($objeto->__get('_idGestionCompetencia')) {
                $vista = $this->getRuta() . 'v_gestion_edit.phtml';
            } else {
                $vista = $this->getRuta() . 'v_error.phtml';
            }
        } else {
            $vista = $this->getRuta() . 'v_error.phtml';
        }
        include $vista;
    }

    /**
     * @author Fernando Esteban Valerio <festeban@gmd.com.pe>
     * @method saveGestion
     * Registrar la informacion de la gestion de competencia.
     * @global conexion $conexion Objeto de conexion a la BD creada con PDO.
     * @param string $nombre Nombre de la gestion de competencia.
     * @return string $mensaje Mensaje de respuesta.
     */
    public function saveGestion($nombre)
    {
        global $conexion;
    
        $arrParametros = array();
        $arrParametros['id'] = NULL;
        $arrParametros['desc'] = $nombre;
        $arrParametros['flag'] = 1;

        $id = 0;
        $mensaje = '';
        $objeto = $this->obtener($id, $nombre);
        if ($objeto->__get('_idGestionCompetencia')) {
            $mensaje = 'existe';
        } else {
            $id = $this->guardar($arrParametros);
            if ($id) {
                $mensaje = 'ok';
            } else {
                $mensaje = 'error';
            }
        }
        echo $mensaje;
    }
    
    /**
     * @author Fernando Esteban Valerio <festeban@gmd.com.pe>
     * @method updateGestion
     * Actualizar la informacion de la gestion de competencia.
     * @global conexion $conexion Objeto de conexion a la BD creada con PDO
     * @param int $id Id de la gestion de competencia.
     * @param string $nombre Nombre de la gestion de competencia.
     * @param int $flag Estado de la gestion de competencia
     * @return string
     */
    public function updateGestion($id, $nombre, $flag=1)
    {
        global $conexion;
    
        $mensaje = '';
        if (is_numeric($id) && is_numeric($flag)) {
            $arrParametros = array();
            $arrParametros['id'] = $id;
            $arrParametros['desc'] = $nombre;
            $arrParametros['flag'] = $flag;
    
            $numElementos = $this->buscarRegistro($id, $nombre);
            if ($numElementos) {
                $mensaje = 'existe';
            } else {
                $resultado = $this->guardar($arrParametros);
                if ($resultado) {
                    $mensaje = 'ok';
                } else {
                    $mensaje = 'error';
                }
            }
        } else {
            $mensaje = 'error';
        }
        echo $mensaje;
    }
    
    /**
     * @author Fernando Esteban Valerio <festeban@gmd.com.pe>
     * @method cambiarPaginaGestion
     * Mostrar las gestiones de competencia ubicados en la pagina seleccionada.
     * @global conexion $conexion Objeto de conexion a la BD creada con PDO.
     * @param int $pagina Numero de pagina.
     * @param int $total Total de registros.
     * @param string $nombre Nombre de la gestion de competencia.
     * @return void
     */
    public function cambiarPaginaGestion($pagina, $total, $nombre)
    {
        global $conexion;

        $arrGestion = $this->filtrar($pagina, $nombres);
        $numPaginas = ceil($total/TAM_PAG_LISTADO_MAP);
        $vista = $this->getRuta() . 'v_gestion_paginado.phtml';
        include $vista;
        $cadena = '<script type="text/javascript">'
                . '$(document).ready(function(){'
                . '$("#cont_num_resultados_head").empty();'
                . '$("#cont_num_resultados_head").html(' . $total . ');'
                . '$("#tl_registros").val("' . $total . '");'
                . '$("#pag_actual").val("' . $pagina . '");'
                . '});'
                . '</script>';
        echo $cadena;
    }
    
    /**
     * @author Fernando Esteban Valerio <festeban@gmd.com.pe>
     * @method viewMantGestion
     * Mostrar el modulo de administracion de gestion de competencia.
     * @global conexion $conexion Objeto de conexion a la BD creada con PDO.
     * @global int $pagina Numero de pagina.
     * @return void
     */
    public function viewMantGestion($opcion=1, $nombre='')
    {
        global $conexion;

        // Numero de pagina inicial
        $pagina = $this->getPagina();
        
        // Total de registros
        $total = $objeto->listTotal($nombre);
        
        // Arreglo de gestiones de obra
        $arrGestion = $objeto->filtrar($pagina, $nombre);
        
        // Numero de paginas
        $numPaginas = ceil($total/TAM_PAG_LISTADO_MAP);
        
        // Indicador de error
        $error = 0;
        
        // Directorio
        $ruta = $this->getRuta();
        if ($opcion) {
            $ruta .= 'v_gestion_lista.phtml';
        } else {
            $ruta .= 'v_gestion_lista_paginado.phtml';
        }
        if ($numPaginas > 1) {
            $cadena = '<script type="text/javascript">'
                    . '$(document).ready(function(){'
                    . '$("#cont_num_resultados_head").empty();'
                    . '$("#cont_num_resultados_head").html(' . $total . ');'
                    . '$("#pag_actual").val("' . $pagina . '");'
                    . '$("#paginacion").show();'
                    . '});'
                    . '</script>'
                    . '<script type="text/javascript">'
                    . '$(function() {'
                    . '$("#paginacion").paginate({'
                    . 'count : ' . $numPaginas . ','
                    . 'start : 1,'
                    . 'display : 5,'
                    . 'border : true,'
                    . 'border_color : "#FFFFFF",'
                    . 'text_color : "#FFFFFF",'
                    . 'background_color : "#0080AF",'
                    . 'border_hover_color : "#CCCCCC",'
                    . 'text_hover_color : "#000000",'
                    . 'background_hover_color : "#FFFFFF",'
                    . 'images : false,'
                    . 'mouse : "press",'
                    . 'onChange : function(page){'
                    . 'loader("start");'
                    . '$("#pag_actual").attr("value", page);'
                    . 'var tl = $("#tl_registros").attr("value");'
                    . 'var filtro = $("#desc").attr("value");'
                    . '$.post("modulos/maps/administracion.maps.php?'
                    . 'cmd=cambiarPaginaGestion",'
                    . '{ pagina: page, total: tl, filtro: filtro }, '
                    . 'function(data){'
                    . '$("#tb_resultado").empty();'
                    . '$("#tb_resultado").append(data);'
                    . 'loader("end");'
                    . '});'
                    . '}'
                    . '});'
                    . '});'
                    . '</script>';
        } else {
            $cadena = '<script type="text/javascript">'
                    . '$(document).ready(function(){'
                    . '$("#tb_resultado").show();'
                    . '$("#cont_num_resultados_head").empty();'
                    . '$("#cont_num_resultados_head").html(' . $total . ');'
                    . '$("#tl_registros").val(' . $total . ');'
                    . '$("#pag_actual").val("' . $pagina . '");'
                    . '$("#paginacion").hide();'
                    . '});'
                    . '</script>';
        }
        include $vista;
        echo $cadena;
    }
}