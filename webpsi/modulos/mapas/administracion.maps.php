<?php

/**
 * [Web Unificada] :: Controlador del Modulo de Administracion Maps.
 * @package    /modulos/maps/
 * @name       administracion_maps.php
 * @category   Controller
 * @author     Fernando Esteban Valerio <festeban@gmd.com.pe>
 * @copyright  GMD S.A.
 * @version    1.0
 * @version    2.0  -- SERGIO MIRANDA -- 2013/05/08
 */

//require_once "../../config/web.config.php";
//include_once APP_DIR . 'autoload.php';
//include_once APP_DIR . 'session.php';

include_once  'autoload.php';


include_once 'administracion.fuente.identificacion.php';
include_once 'administracion.zona.competencia.php';
include_once 'administracion.nse.php';
include_once 'administracion.cobertura.php';
include_once 'administracion.tipo.proyecto.php';
include_once 'administracion.segmento.php';
include_once 'administracion.gestion.obra.php';
include_once 'administracion.gestion.competencia.php';
include_once 'administracion.responsable.campo.php';

define("TAM_PAG_LISTADO_MAP", 10);

$pagina = 1;

class AdministracionMaps
{
    /**
     * Id del usuario
     * @access protected
     * @var int
     */
    protected $_idUsuario;
    
    /**
     * Id de la empresa a la que pertenece el usuario
     * @access protected
     * @var int
     */
    protected $_idEmpresa;
    
    /**
     * Login del usuario
     * @access protected
     * @var string
     */
    protected $_loginUsuario;

    /**
     * Directorio
     * @access protected
     * @var string 
     */
    protected $_ruta;

    /**
     * @method  __construct
     * Metodo constructor de la clase
     */
    public function __construct()
    {
        $this->_idUsuario = $_SESSION['USUARIO']->__get('_idUsuario');
        $this->_loginUsuario = $_SESSION['USUARIO']->__get('_login');
        $this->_idEmpresa = $_SESSION['USUARIO']->__get('_idEmpresa');
        $this->_ruta = 'vistas/administracion/';

        $opcion = 0;

        $objFuente = new AdministracionFuenteIdentificacion();
        $objTipoProyecto = new AdministracionTipoProyecto();
        $objSegmento = new AdministracionSegmento();
        $objGestObra = new AdministracionGestionObra();
        $objGestCompetencia = new AdministracionGestionCompetencia();
        $objRespCampo= new AdministracionResponsableCampo();
        $objCobertura = new AdministracionCobertura();
        $objNse = new AdministracionNivelSocioeconomico();
        $objZonaCompetencia = new AdministracionZonaCompetencia();

        if (!isset($_GET['cmd'])) {
            $_GET['cmd'] = '';
        }
        switch ($_GET['cmd']) {
            // Fuente de Identificacion
            case "mantFuente":
                $objFuente->vistaMantenimiento();
                break;                
            case "addFuente":
                $objFuente->vistaAgregar();
                break;
            case 'buscaFuente':
                $opcion = 1;
                $nombre = isset($_POST['valor']) ? $_POST['valor'] : '';
                $nombre = $this->limpiar($nombre);
                $objFuente->vistaMantenimiento($opcion, $nombre);
                break;
            case 'editFuente':
                $id = isset($_POST['id']) ? $_POST['id'] : 0;
                $objFuente->vistaEditar($id);
                break;
            case 'saveFuente':
                $nombre = isset($_POST['desc']) ? $_POST['desc'] : '';
                $objFuente->guardar($nombre);
                break;
            case 'updateFuente':
                $id = isset($_POST['id']) ? $_POST['id'] : 0;
                $nombre = isset($_POST['desc']) ? $_POST['desc'] : '';
                $flag = isset($_POST['estado']) ? $_POST['estado'] : 1;
                $nombre = $this->limpiar($nombre);
                $objFuente->actualizar($id, $nombre, $flag);
                break;
            case 'cambiarPaginaFuente':
                $pagina = isset($_POST['pagina']) ? $_POST['pagina'] : 1;
                $total = isset($_POST['total']) ? $_POST['total'] : 0;
                $nombre = isset($_POST['filtro']) ? $_POST['filtro'] : '';
                $nombre = $this->limpiar($nombre);
                $objFuente->cambiarPagina($pagina, $total, $nombre);
                break;

            // Fuente de Identificacion X Region
            case "mantFuenteXRegion":
                $this->viewMantFuenteXRegion();
                break;
            case "addFuenteXRegion":
                $this->viewAddFuenteXRegion();
                break;
            case 'buscaFuenteXRegion':
                $nombre = isset($_POST['valor']) ? $_POST['valor'] : '';
                $this->buscaFuenteXRegion($nombre);
                break;
            case 'saveFuenteXRegion':
                $this->saveFuenteXRegion($_POST['region'], $_POST['fuente']);
                break;
            case 'updateFuenteXRegion':
                $id = isset($_POST['id']) ? $_POST['id'] : 0;
                $flag = isset($_POST['estado']) ? $_POST['estado'] : 1;
                $this->updateFuenteXRegion($id, $flag);
                break;
            case 'cambiarPaginaFuenteXRegion':
                $pagina = isset($_POST['pagina']) ? $_POST['pagina'] : 1;
                $total = isset($_POST['total']) ? $_POST['total'] : 0;
                $nombre = isset($_POST['filtro']) ? $_POST['filtro'] : '';
                $this->cambiarPaginaFuenteXRegion($pagina, $total, $nombre);
                break;
                
            // Tipo de Proyecto            
            case "mantTipoProyecto":
                $objTipoProyecto->vistaMantenimiento();
                break;
            case "addTipoProyecto":
                $objTipoProyecto->vistaAgregar();
                break;
            case 'buscaTipoProyecto':
                $opcion = 1;
                $idSegmento = isset($_POST['id']) ? $_POST['id'] : 0;
                $objTipoProyecto->vistaMantenimiento($opcion, $idSegmento);
                break;
            case 'saveTipoProyecto':
                $idSegmento = isset($_POST['segmento']) 
                            ? $_POST['segmento'] : 0;
                $nombre = isset($_POST['desc']) ? $_POST['desc'] : '';
                $nombre = $this->limpiar($nombre);
                $objTipoProyecto->guardar($idSegmento, $nombre);
                break;
            case 'updateTipoProyecto':
                $id = isset($_POST['id']) ? $_POST['id'] : 0;
                $flag = isset($_POST['estado']) ? $_POST['estado'] : 1;
                $objTipoProyecto->actualizar($id, $flag);
                break;
            case 'cambiarPaginaTipoProyecto':
                $pagina = isset($_POST['pagina']) ? $_POST['pagina'] : 1;
                $total = isset($_POST['total']) ? $_POST['total'] : 0;
                $idSegmento = isset($_POST['filtro']) ? $_POST['filtro'] : 0;
                $objTipoProyecto->cambiarPagina($pagina, $total, $idSegmento);
                break;

            // Segmento
            case "mantSegmento":
                $objSegmento->vistaMantenimiento();
                break;
            case "addSegmento":
                $objSegmento->vistaAgregar();
                break;
            case 'buscaSegmento':
                $opcion = 1;
                $nombre = isset($_POST['valor']) ? $_POST['valor'] : '';
                $nombre = $this->limpiar($nombre);
                $objSegmento->vistaMantenimiento($opcion, $nombre);
                break;
            case 'editSegmento':
                $id = isset($_POST['id']) ? $_POST['id'] : 0;
                $objSegmento->vistaEditar($id);
                break;
            case 'saveSegmento':
                $nombre = isset($_POST['desc']) ? $_POST['desc'] : '';
                $nombre = $this->limpiar($nombre);
                $objSegmento->guardar($nombre);
                break;
            case 'updateSegmento':
                $id = isset($_POST['id']) ? $_POST['id'] : 0;
                $nombre = isset($_POST['desc']) ? $_POST['desc'] : '';
                $flag = isset($_POST['estado']) ? $_POST['estado'] : 1;
                $nombre = $this->limpiar($nombre);
                $objSegmento->actualizar($id, $nombre, $flag);
                break;
            case 'cambiarPaginaSegmento':
                $pagina = isset($_POST['pagina']) ? $_POST['pagina'] : 1;
                $total = isset($_POST['total']) ? $_POST['total'] : 0;
                $nombre = isset($_POST['filtro']) ? $_POST['filtro'] : '';
                $nombre = $this->limpiar($nombre);
                $objSegmento->cambiarPagina($pagina, $total, $nombre);
                break;

            // Nivel Socioeconomico
            case "mantNse":
                $objNse->vistaMantenimiento();
                break;
            case "addNse":
                $objNse->vistaAgregar();
                break;
            case 'buscaNse':
                $opcion = 1;
                $nombre = isset($_POST['valor']) ? $_POST['valor'] : '';
                $nombre = $this->limpiar($nombre);
                $objNse->vistaMantenimiento($opcion, $nombre);
                break;
            case 'editNse':
                $id = isset($_POST['id']) ? $_POST['id'] : 0;
                $objNse->vistaEditar($id);
                break;
            case 'saveNse':
                $nombre = isset($_POST['desc']) ? $_POST['desc'] : '';
                $objNse->guardar($nombre);
                break;
            case 'updateNse':
                $id = isset($_POST['id']) ? $_POST['id'] : 0;
                $nombre = isset($_POST['desc']) ? $_POST['desc'] : '';
                $flag = isset($_POST['estado']) ? $_POST['estado'] : 1;
                $nombre = $this->limpiar($nombre);
                $objNse->actualizar($id, $nombre, $flag);
                break;
            case 'cambiarPaginaNse':
                $pagina = isset($_POST['pagina']) ? $_POST['pagina'] : 1;
                $total = isset($_POST['total']) ? $_POST['total'] : 0;
                $nombre = isset($_POST['filtro']) ? $_POST['filtro'] : '';
                $nombre = $this->limpiar($nombre);
                $objNse->cambiarPagina($pagina, $total, $nombre);
                break;

            // Zona de Competencia
            case "mantZona":
                $objZonaCompetencia->vistaMantenimiento();
                break;
            case "addZona":
                $objZonaCompetencia->vistaAgregar();
                break;
            case 'buscaZona':
                $opcion = 1;
                $nombre = isset($_POST['valor']) ? $_POST['valor'] : '';
                $nombre = $this->limpiar($nombre);
                $objZonaCompetencia->vistaMantenimiento($opcion, $nombre);
                break;
            case 'editZona':
                $id = isset($_POST['id']) ? $_POST['id'] : 0;
                $objZonaCompetencia->vistaEditar($id);
                break;
            case 'saveZona':
                $nombre = isset($_POST['desc']) ? $_POST['desc'] : '';
                $nombre = $this->limpiar($nombre);
                $objZonaCompetencia->guardar($nombre);
                break;
            case 'updateZona':
                $id = isset($_POST['id']) ? $_POST['id'] : 0;
                $nombre = isset($_POST['desc']) ? $_POST['desc'] : '';
                $flag = isset($_POST['estado']) ? $_POST['estado'] : 1;
                $nombre = $this->limpiar($nombre);
                $objZonaCompetencia->actualizar($id, $nombre, $flag);
                break;
            case 'cambiarPaginaZona':
                $pagina = isset($_POST['pagina']) ? $_POST['pagina'] : 1;
                $total = isset($_POST['total']) ? $_POST['total'] : 0;
                $nombre = isset($_POST['filtro']) ? $_POST['filtro'] : '';
                $nombre = $this->limpiar($nombre);
                $objZonaCompetencia->cambiarPagina($pagina, $total, $nombre);
                break;

            // Gestion de Competencia
            case "mantGestion":
                $objGestCompetencia->vistaMantenimiento();
                break;
            case "addGestion":
                $objGestCompetencia->vistaAgregar();
                break;
            case 'buscaGestion':
                $nombre = isset($_POST['valor']) ? $_POST['valor'] : '';
                $opcion = 1;
                $nombre = $this->limpiar($nombre);
                $objGestCompetencia->vistaMantenimiento($opcion, $nombre);
                break;
            case 'editGestion':
                $id = isset($_POST['id']) ? $_POST['id'] : 0;
                $objGestCompetencia->vistaEditar($id);
                break;
            case 'saveGestion':
                $nombre = isset($_POST['desc']) ? $_POST['desc'] : '';
                $nombre = $this->limpiar($nombre);
                $objGestCompetencia->guardar($nombre);
                break;
            case 'updateGestion':
                $id = isset($_POST['id']) ? $_POST['id'] : 0;
                $nombre = isset($_POST['desc']) ? $_POST['desc'] : '';
                $flag = isset($_POST['estado']) ? $_POST['estado'] : 1;
                $nombre = $this->limpiar($nombre);
                $objGestCompetencia->actualizar($id, $nombre, $flag);
                break;
            case 'cambiarPaginaGestion':
                $pagina = isset($_POST['pagina']) ? $_POST['pagina'] : 1;
                $total = isset($_POST['total']) ? $_POST['total'] : 0;
                $nombre = isset($_POST['filtro']) ? $_POST['filtro'] : '';
                $nombre = $this->limpiar($nombre);
                $objGestCompetencia->cambiarPagina($pagina, $total, $nombre);
                break;

            // Gestion de obra
            case "mantObra":
                $objGestObra->vistaMantenimiento();
                break;
            case "addObra":
                $objGestObra->vistaAgregar();
                break;
            case 'buscaObra':
                $opcion = 1;
                $nombre = isset($_POST['valor']) ? $_POST['valor'] : '';
                $nombre = $this->limpiar($nombre);
                $objGestObra->vistaMantenimiento($opcion, $nombre);
                break;
            case 'editObra':
                $id = isset($_POST['id']) ? $_POST['id'] : 0;
                $objGestObra->vistaEditar($id);
                break;
            case 'saveObra':
                $nombre = isset($_POST['desc']) ? $_POST['desc'] : '';
                $nombre = $this->limpiar($nombre);
                $objGestObra->guardar($nombre);
                break;
            case 'updateObra':
                $id = isset($_POST['id']) ? $_POST['id'] : 0;
                $nombre = isset($_POST['desc']) ? $_POST['desc'] : '';
                $flag = isset($_POST['estado']) ? $_POST['estado'] : 1;
                $nombre = $this->limpiar($nombre);
                $objGestObra->actualizar($id, $nombre, $flag);
                break;
            case 'cambiarPaginaObra':
                $pagina = isset($_POST['pagina']) ? $_POST['pagina'] : 1;
                $total = isset($_POST['total']) ? $_POST['total'] : 0;
                $nombre = isset($_POST['filtro']) ? $_POST['filtro'] : '';
                $nombre = $this->limpiar($nombre);
                $objGestObra->cambiarPagina($pagina, $total, $nombre);
                break;

            // Responsable Campo
            case "mantResponsable":
                $objRespCampo->vistaMantenimiento();
                break;
            case "addResponsable":
                $objRespCampo->vistaAgregar();
                break;
            case 'buscaResponsable':
                $opcion = 1;
                $nombre = isset($_POST['valor']) ? $_POST['valor'] : '';
                $nombre = $this->limpiar($nombre);
                $objRespCampo->vistaMantenimiento($opcion, $nombre);
                break;
            case 'editResponsable':
                $id = isset($_POST['id']) ? $_POST['id'] : 0;
                $objRespCampo->vistaEditar($id);
                break;
            case 'saveResponsable':
                $nombre = isset($_POST['desc']) ? $_POST['desc'] : '';
                $nombre = $this->limpiar($nombre);
                $objRespCampo->guardar($nombre);
                break;
            case 'updateResponsable':
                $id = isset($_POST['id']) ? $_POST['id'] : 0;
                $nombre = isset($_POST['desc']) ? $_POST['desc'] : '';
                $flag = isset($_POST['estado']) ? $_POST['estado'] : 1;
                $nombre = $this->limpiar($nombre);
                $objRespCampo->actualizar($id, $nombre, $flag);
                break;
            case 'cambiarPaginaResponsable':
                $pagina = isset($_POST['pagina']) ? $_POST['pagina'] : 1;
                $total = isset($_POST['total']) ? $_POST['total'] : 0;
                $nombre = isset($_POST['filtro']) ? $_POST['filtro'] : '';
                $nombre = $this->limpiar($nombre);
                $objRespCampo->cambiarPagina($pagina, $total, $nombre);
                break;

            // Cobertura 2G - 3G
            case "mantCobertura":
                $tipo = isset($_POST['tipo']) ? $_POST['tipo'] : '';
                $objCobertura->vistaMantenimiento($opcion, $tipo);
                break;
            case "addCobertura":
                $tipo = isset($_POST['tipo']) ? $_POST['tipo'] : '';
                $objCobertura->vistaAgregar($tipo);
                break;
            case 'buscaCobertura':
                $opcion = 1;
                $tipo = isset($_POST['tipo']) ? $_POST['tipo'] : '';
                $nombre = isset($_POST['valor']) ? $_POST['valor'] : '';
                $nombre = $this->limpiar($nombre);
                $objCobertura->vistaMantenimiento($opcion, $tipo, $nombre);
                break;
            case 'editCobertura':
                $tipo = isset($_POST['tipo']) ? $_POST['tipo'] : '';
                $id = isset($_POST['id']) ? $_POST['id'] : '';
                $objCobertura->vistaEditar($tipo, $id);
                break;
            case 'saveCobertura':
                $tipo = isset($_POST['tipo']) ? $_POST['tipo'] : '';
                $nombre = isset($_POST['desc']) ? $_POST['desc'] : '';
                $nombre = $this->limpiar($nombre);
                $objCobertura->guardar($tipo, $nombre);
                break;
            case 'updateCobertura':
                $tipo = isset($_POST['tipo']) ? $_POST['tipo'] : '';
                $id = isset($_POST['id']) ? $_POST['id'] : 0;
                $nombre = isset($_POST['desc']) ? $_POST['desc'] : '';
                $flag = isset($_POST['estado']) ? $_POST['estado'] : 1;
                $nombre = $this->limpiar($nombre);
                $objCobertura->actualizar($tipo, $id, $nombre, $flag);
                break;
            case 'cambiarPaginaCobertura':
                $tipo = isset($_POST['tipo']) ? $_POST['tipo'] : '';
                $pagina = isset($_POST['pagina']) ? $_POST['pagina'] : 1;
                $total = isset($_POST['total']) ? $_POST['total'] : 0;
                $nombre = isset($_POST['filtro']) ? $_POST['filtro'] : '';
                $nombre = $this->limpiar($nombre);
                $objCobertura->cambiarPagina($tipo, $pagina, $total, $nombre);
                break;

            // Ubigeo
            case "mantUbigeo":
                $this->viewMantUbigeo();
                break;
            case "addUbigeo":
                $this->viewAddUbigeo();
                break;
            case 'buscaUbigeo':
                $idRegion = isset($_POST['id']) ? $_POST['id'] : 0;
                $dpto = isset($_POST['dpto']) ? $_POST['dpto'] : '';
                $this->buscaUbigeo($idRegion, $dpto);
                break;
            case 'saveUbigeo':
                $idRegion = isset($_POST['region']) ? $_POST['region'] : 0;
                $codDpto = isset($_POST['dpto']) ? $_POST['dpto'] : '';
                $this->saveUbigeo($idRegion, $codDpto);
                break;
            case 'updateUbigeo':
                $id = isset($_POST['id']) ? $_POST['id'] : 0;
                $flag = isset($_POST['estado']) ? $_POST['estado'] : 1;
                $this->updateUbigeo($id, $flag);
                break;
            case 'cambiarPaginaUbigeo':
                $pagina = isset($_POST['pagina']) ? $_POST['pagina'] : 1;
                $total = isset($_POST['total']) ? $_POST['total'] : 0;
                $idRegion = isset($_POST['id']) ? $_POST['id'] : 0;
                $dpto = isset($_POST['dpto']) ? $_POST['dpto'] : '';
                $this->cambiarPaginaUbigeo($pagina, $total, $idRegion, $dpto);
                break;
            case 'mostrarDepartamento':
                $idRegion = isset($_POST['id']) ? $_POST['id'] : '';
                $this->mostrarDepartamento($idRegion);
                break;

            default:
                $this->viewAdministracionMain();
                break;
        }
    }

    /**
     * @author Fernando Esteban Valerio <festeban@gmd.com.pe>
     * @method limpiar
     * Limpiar datos de entrada.
     * @param string $nombre Dato de entrada
     * @return string Dato depurado
     */
    public function limpiar($nombre)
    {
        $nombre = trim($nombre);
        $nombre = mb_strtoupper($nombre, 'UTF-8');
        $nombre = ereg_replace('[^A-Za-z0-9_-ñÑ\-]', ' ', $nombre);
        $nombre = htmlentities($nombre);
        return $nombre;
    }
    
    /**
     * @author Fernando Esteban Valerio <festeban@gmd.com.pe>
     * @method updateFuenteXRegion
     * Actualizar estado del objeto Data_RegionFuenteIdentificacionEdi.
     * @global conexion $conexion Objeto de conexion a la BD creada con PDO
     * @param int $id Id de la fuente de identificacion x region     
     * @param int $flag Estado de la fuente de identificacion x region
     * @return string
     */
    public function updateFuenteXRegion($id, $flag=1)
    {
        global $conexion;
        
        if (is_numeric($id) && is_numeric($flag)) {
            $arrParametros = array();
            $arrParametros['id'] = $id;
            $arrParametros['flag'] = $flag;
    
            $objeto = new Data_RegionFuenteIdentificacionEdi();
            $resultado = $objeto->actualizar($arrParametros);
            if ($resultado) {
                echo 'ok';
            } else {
                echo 'error';
            }
        } else {
            echo 'error';
        }
    }
    
    /**
     * @author Fernando Esteban Valerio <festeban@gmd.com.pe>
     * @method saveFuenteXRegion
     * Registrar objeto Data_RegionFuenteIdentificacionEdi.
     * @global conexion $conexion Objeto de conexion a la BD creada con PDO
     * @param int $region Id de la region
     * @param int $fuente Id de la fuente de identificacion
     * @return string
     */
    public function saveFuenteXRegion($region, $fuente)
    {
        global $conexion;

        if (is_numeric($region) && is_numeric($fuente)) {
            $arrParametros = array();
            $arrParametros['region'] = $region;
            $arrParametros['fuente'] = $fuente;
            $arrParametros['flag'] = 1;
    
            $objeto = new Data_RegionFuenteIdentificacionEdi();
            $resultado = $objeto->validar($arrParametros);
            if ($resultado) {
                $id = $objeto->guardar($arrParametros);
                if ($id) {
                    echo 'ok';
                } else {
                    echo 'error';
                }
            } else {
                echo 'Ya existe ese registro';
            }
        } else {
            echo 'error';
        }
    }
    
    /**
     * @author Fernando Esteban Valerio <festeban@gmd.com.pe>
     * @method cambiarPaginaFuenteXRegion
     * Mostrar listado de fuentes de identificacion x region para una pagina
     * determinada.
     * @global conexion $conexion Objeto de conexion a la BD creada con PDO
     * @param int $pagina Numero de pagina
     * @param int $total Total de registros
     * @param string $nombre Valor de busqueda
     * @return void
     */
    public function cambiarPaginaFuenteXRegion($pagina, $total, $nombre)
    {
        global $conexion;

        $nombre = $this->limpiar($nombre);
        $objeto = new Data_RegionFuenteIdentificacionEdi();
        $arrFuenteXRegion = $objeto->filtrar($pagina, $nombre);
        $numPaginas = ceil($total/TAM_PAG_LISTADO_MAP);
        $vista = $this->_ruta . 'region_fuente_identificacion/'
               . 'v_region_fuente_identificacion_paginado.phtml';
        include $vista;
        $cadena = '<script type="text/javascript">'
                . '$(document).ready(function(){'
                . '$("#cont_num_resultados_head").empty();'
                . '$("#cont_num_resultados_head").html(' . $total . ');'
                . '$("#tl_registros").val("' . $total . '");'
                . '$("#pag_actual").val("' . $pagina . '");'
                . '});'
                . '</script>';
        echo $cadena;
    }
    
    /**
     * @author Fernando Esteban Valerio <festeban@gmd.com.pe>
     * @method viewMantFuenteXRegion
     * Mostrar modulo de administracion de fuente de identificacion x region.
     * @global conexion $conexion Objeto de conexion a la BD creada con PDO
     * @global int $pagina Numero de pagina
     * @return void
     */
    public function viewMantFuenteXRegion()
    {
        global $conexion, $pagina;

        // Obtener las regiones
        $objRegion = new Data_RegionEdi();
        $arrRegion = $objRegion->listar($conexion);
        
        $objeto = new Data_RegionFuenteIdentificacionEdi();
        $arrFuenteXRegion = $objeto->filtrar($pagina);
        $total = $objeto->listTotal();
        $numPaginas = ceil($total/TAM_PAG_LISTADO_MAP);
        $vista = $this->_ruta . 'region_fuente_identificacion/'
               . 'v_region_fuente_identificacion_lista.phtml';    
        include $vista;
        if ($numPaginas > 1) {
            $cadena = '<script type="text/javascript">'
                    . '$(document).ready(function(){'
                    . '$("#cont_num_resultados_head").empty();'
                    . '$("#cont_num_resultados_head").html(' . $total . ');'
                    . '$("#pag_actual").val("' . $pagina . '");'
                    . '$("#paginacion").show();'
                    . '});'
                    . '</script>'
                    . '<script type="text/javascript">'
                    . '$(function() {'
                    . '$("#paginacion").paginate({'
                    . 'count : ' . $numPaginas . ','
                    . 'start : 1,'
                    . 'display : 5,'
                    . 'border : true,'
                    . 'border_color : "#FFFFFF",'
                    . 'text_color : "#FFFFFF",'
                    . 'background_color : "#0080AF",'
                    . 'border_hover_color : "#CCCCCC",'
                    . 'text_hover_color : "#000000",'
                    . 'background_hover_color : "#FFFFFF",'
                    . 'images : false,'
                    . 'mouse : "press",'
                    . 'onChange : function(page){'
                    . 'loader("start");'
                    . '$("#pag_actual").attr("value", page);'
                    . 'var tl = $("#tl_registros").attr("value");'
                    . 'var filtro = $("#desc").attr("value");'
                    . '$.post("modulos/maps/administracion.maps.php?'
                    . 'cmd=cambiarPaginaFuenteXRegion",'
                    . '{ pagina: page, total: tl, filtro: filtro }, '
                    . 'function(data){'
                    . '$("#tb_resultado").empty();'
                    . '$("#tb_resultado").append(data);'
                    . 'loader("end");'
                    . '});'
                    . '}'
                    . '});'
                    . '});'
                    . '</script>';
        } else {
            $cadena = '<script type="text/javascript">'
                    . '$(document).ready(function(){'
                    . '$("#cont_num_resultados_head").empty();'
                    . '$("#cont_num_resultados_head").html(' . $total . ');'
                    . '$("#tl_registros").val(' . $total . ');'
                    . '$("#pag_actual").val("' . $pagina . '");'
                    . '$("#paginacion").hide();'
                    . '});'
                    . '</script>';
        }
        echo $cadena;
    }
    
    /**
     * @author Fernando Esteban Valerio <festeban@gmd.com.pe>
     * @method buscaFuenteXRegion
     * Buscar una fuente de identificacion x region de acuerdo a un criterio
     * de busqueda (Region).
     * @global conexion $conexion Objeto de conexion a la BD creada con PDO
     * @global int $pagina Numero de pagina
     * @param string $nombre Valor de busqueda
     * @return void
     */
    public function buscaFuenteXRegion($nombre='')
    {
        global $conexion, $pagina;

        $objeto = new Data_RegionFuenteIdentificacionEdi();
        $arrFuenteXRegion = $objeto->filtrar($pagina, $this->limpiar($nombre));
        $total = $objeto->listTotal($nombre);
        $numPaginas = ceil($total/TAM_PAG_LISTADO_MAP);
        if ($numPaginas > 1) {
            $cadena = '<script type="text/javascript">'
                    . '$(document).ready(function(){'
                    . '$("#tb_resultado").show();'
                    . '$("#cont_num_resultados_head").empty();'
                    . '$("#cont_num_resultados_head").html("' . $total . '");'
                    . '$("#tl_registros").val("' . $total . '");'
                    . '$("#pag_actual").val("' . $pagina . '");'
                    . '$("#paginacion").show();'
                    . '});'
                    . '</script>'
                    . '<script type="text/javascript">'
                    . '$(function() {'
                    . '$("#paginacion").paginate({'
                    . 'count : ' . $numPaginas . ','
                    . 'start : 1,'
                    . 'display : 5,'
                    . 'border : true,'
                    . 'border_color : "#FFFFFF",'
                    . 'text_color : "#FFFFFF",'
                    . 'background_color : "#0080AF",'
                    . 'border_hover_color : "#CCCCCC",'
                    . 'text_hover_color : "#000000",'
                    . 'background_hover_color : "#FFFFFF",'
                    . 'images : false,'
                    . 'mouse : "press",'
                    . 'onChange : function(page){'
                    . 'loader("start");'
                    . 'var tl = $("#tl_registros").attr("value");'
                    . 'var filtro = $("#desc").attr("value");'
                    . '$.post("modulos/maps/administracion.maps.php?'
                    . 'cmd=cambiarPaginaFuenteXRegion",'
                    . '{ pagina: page, total: tl, filtro: filtro }, '
                    . 'function(data){'
                    . '$("#tb_resultado").empty();'
                    . '$("#tb_resultado").append(data);'
                    . 'loader("end");'
                    . '});'
                    . '}'
                    . '});'
                    . '});'
                    . '</script>';
        } else {
            $cadena = '<script type="text/javascript">'
                    . '$(document).ready(function(){'
                    . '$("#tb_resultado").show();'
                    . '$("#cont_num_resultados_head").empty();'
                    . '$("#cont_num_resultados_head").html("' . $total . '");'
                    . '$("#tl_registros").val("' . $total . '");'
                    . '$("#pag_actual").val("' . $pagina . '");'
                    . '$("#paginacion").hide();'
                    . '});'
                    . '</script>';
        }
        if ($total > 0) {
            $vista = $this->_ruta . 'region_fuente_identificacion/'
                   . 'v_region_fuente_identificacion_paginado.phtml';
            include $vista;
        } else {
            $cadena .= '<script type="text/javascript">'
                     . '$(document).ready(function(){'
                     . '$("#tb_resultado").hide();'
                     . '});'
                     . '</script>';
        }
        echo $cadena;
    }
    
    /**
     * @author Fernando Esteban Valerio <festeban@gmd.com.pe>
     * @method viewAddFuenteXRegion
     * Mostrar formulario de registro de fuentes de identificacion x region.
     * @global conexion $conexion Objeto de conexion a la BD creada con PDO
     * @return void
     */
    public function viewAddFuenteXRegion()
    {
        global $conexion;
        
        $objRegion = new Data_RegionEdi();
        $arrRegion = $objRegion->listar($conexion);
        
        $objFuente = new Data_FuenteIdentificacionEdi();
        $arrFuente = $objFuente->listar($conexion);

        $ruta = $this->_ruta .'region_fuente_identificacion/'
              . 'v_region_fuente_identificacion_add.phtml';
        include $ruta;
    }
        
    /**
     * @author Fernando Esteban Valerio <festeban@gmd.com.pe>
     * @method updateUbigeo
     * Actualizar estado del ubigeo.
     * @global conexion $conexion Objeto de conexion a la BD creada con PDO
     * @param int $id Id del ubigeo x region
     * @param int $flag Estado del ubigeo x region
     * @return string
     */
    public function updateUbigeo($id, $flag=1)
    {
        global $conexion;
    
        if (is_numeric($id) && is_numeric($flag)) {
            $arrParametros = array();
            $arrParametros['id'] = $id;
            $arrParametros['flag'] = $flag;
    
            $objeto = new Data_RegionUbigeoEdi();
            $resultado = $objeto->actualizar($arrParametros);
            if ($resultado) {
                echo 'ok';
            } else {
                echo 'error';
            }
        } else {
            echo 'error';
        }
    }

    /**
     * @author Fernando Esteban Valerio <festeban@gmd.com.pe>
     * @method saveUbigeo
     * Registrar informacion del ubigeo x region.
     * @global conexion $conexion Objeto de conexion a la BD creada con PDO
     * @param int $idRegion Id de la region
     * @param string $codDpto Codigo del departamento
     * @return string
     */
    public function saveUbigeo($idRegion, $codDpto)
    {
        global $conexion;
    
        $arrParametros = array();
        $arrParametros['region'] = $idRegion;
        $arrParametros['dpto'] = $codDpto;
        $arrParametros['flag'] = 1;
    
        $id = 0;
        $objeto = new Data_RegionUbigeoEdi();
        $existe = $objeto->validar($arrParametros);
        if (!$existe) {
            echo 'existe';
        } else {
            $id = $objeto->guardar($arrParametros);
            if ($id) {
                echo 'ok';
            } else {
                echo 'error';
            }
        }
    }

    /**
     * @author Fernando Esteban Valerio <festeban@gmd.com.pe>
     * @method cambiarPaginaUbigeo
     * Mostrar lista de ubigeos contenidos en la pagina seleccionada.
     * @global conexion $conexion Objeto de conexion a la BD creada con PDO
     * @param int $pagina Numero de pagina
     * @param int $total Total de registros
     * @param int $idRegion Id de la region
     * @param string $dpto Codigo del departamento
     * @return void
     */
    public function cambiarPaginaUbigeo($pagina, $total, $idRegion, $dpto)
    {
        global $conexion;
    
        $objeto = new Data_RegionUbigeoEdi();
        $arrUbigeo = $objeto->filtrar($pagina, $idRegion, $dpto);
        $numPaginas = ceil($total/TAM_PAG_LISTADO_MAP);
        $vista = $this->_ruta . 'ubigeo/v_region_ubigeo_paginado.phtml';
        include $vista;
        $cadena = '<script type="text/javascript">'
                . '$(document).ready(function(){'
                . '$("#cont_num_resultados_head").empty();'
                . '$("#cont_num_resultados_head").html(' . $total . ');'
                . '$("#tl_registros").val("' . $total . '");'
                . '$("#pag_actual").val("' . $pagina . '");'
                . '});'
                . '</script>';
        echo $cadena;
    }
    
    /**
     * @author Fernando Esteban Valerio <festeban@gmd.com.pe>
     * @method viewMantUbigeo
     * Mostrar modulo de administracion de ubigeo.
     * @global conexion $conexion Objeto de conexion a la BD creada con PDO
     * @global int $pagina Numero de pagina
     * @return void
     */
    public function viewMantUbigeo()
    {
        global $conexion, $pagina;
    
        $total = 0;
        $objeto = new Data_RegionEdi();
        $arrRegion = $objeto->listar($conexion);
        $vista = $this->_ruta . 'ubigeo/v_region_ubigeo_lista.phtml';
        include $vista;
        $cadena = '<script type="text/javascript">'
                . '$(document).ready(function(){'
                . '$("#cont_num_resultados_head").empty();'
                . '$("#cont_num_resultados_head").html(' . $total . ');'
                . '$("#tl_registros").val(' . $total . ');'
                . '$("#pag_actual").val("' . $pagina . '");'
                . '$("#paginacion").hide();'
                . '});'
                . '</script>';        
        echo $cadena;
    }

    /**
     * @author Fernando Esteban Valerio <festeban@gmd.com.pe>
     * @method buscaUbigeo
     * Buscar ubigeo de acuerdo al criterio de busqueda (Region).
     * @global conexion $conexion Objeto de conexion a la BD creada con PDO
     * @global int $pagina Numero de pagina
     * @param int $idRegion Id de la region
     * @param string $dpto Codigo del departamento
     * @return void
     */
    public function buscaUbigeo($idRegion, $dpto)
    {
        global $conexion, $pagina;
    
        $objeto = new Data_RegionUbigeoEdi();
        $arrUbigeo = $objeto->filtrar($pagina, $idRegion, $dpto);
        $total = $objeto->listTotal($idRegion, $dpto);
        $numPaginas = ceil($total/TAM_PAG_LISTADO_MAP);
        if ($numPaginas > 1) {
            $cadena = '<script type="text/javascript">'
                    . '$(document).ready(function(){'
                    . '$("#tb_resultado").show();'
                    . '$("#cont_num_resultados_head").empty();'
                    . '$("#cont_num_resultados_head").html("' . $total . '");'
                    . '$("#tl_registros").val("' . $total . '");'
                    . '$("#pag_actual").val("' . $pagina . '");'
                    . '$("#paginacion").show();'
                    . '});'
                    . '</script>'
                    . '<script type="text/javascript">'
                    . '$(function() {'
                    . '$("#paginacion").paginate({'
                    . 'count : ' . $numPaginas . ','
                    . 'start : 1,'
                    . 'display : 5,'
                    . 'border : true,'
                    . 'border_color : "#FFFFFF",'
                    . 'text_color : "#FFFFFF",'
                    . 'background_color : "#0080AF",'
                    . 'border_hover_color : "#CCCCCC",'
                    . 'text_hover_color : "#000000",'
                    . 'background_hover_color : "#FFFFFF",'
                    . 'images : false,'
                    . 'mouse : "press",'
                    . 'onChange : function(page){'
                    . 'loader("start");'
                    . 'var tl = $("#tl_registros").attr("value");'
                    . 'var p_filtro = $("#id").attr("value");'
                    . 'var s_filtro = $("#dpto").attr("value");'
                    . '$.post("modulos/maps/administracion.maps.php?'
                    . 'cmd=cambiarPaginaUbigeo", { pagina: page, total: tl, '
                    . 'id: p_filtro, dpto: s_filtro }, function(data){'
                    . '$("#tb_resultado").empty();'
                    . '$("#tb_resultado").append(data);'
                    . 'loader("end");'
                    . '});'
                    . '}'
                    . '});'
                    . '});'
                    . '</script>';
        } else {
            $cadena = '<script type="text/javascript">'
                    . '$(document).ready(function(){'
                    . '$("#tb_resultado").show();'
                    . '$("#cont_num_resultados_head").empty();'
                    . '$("#cont_num_resultados_head").html("' . $total . '");'
                    . '$("#tl_registros").val("' . $total . '");'
                    . '$("#pag_actual").val("' . $pagina . '");'
                    . '$("#paginacion").hide();'
                    . '});'
                    . '</script>';
        }
        if ($total > 0) {
            $vista = $this->_ruta . 'ubigeo/v_region_ubigeo_paginado.phtml';
            include $vista;
        } else {
            $cadena .= '<script type="text/javascript">'
                     . '$(document).ready(function(){'
                     . '$("#tb_resultado").hide();'
                     . '});'
                     . '</script>';
        }
        echo $cadena;
    }
    
    /**
     * @author Fernando Esteban Valerio <festeban@gmd.com.pe>
     * @method buscaUbigeo
     * Buscar ubigeo de acuerdo al criterio de busqueda (Region).
     * @global conexion $conexion Objeto de conexion a la BD creada con PDO
     * @global int $pagina Numero de pagina
     * @param string $idRegion Valor de busqueda
     * @return void
     */
    public function mostrarDepartamento($idRegion)
    {
        global $conexion, $pagina;
    
        $cadena = '<option value="">Todos</option>';
        $objeto = new Data_RegionUbigeoEdi();
        $arrUbigeo = $objeto->traerDepartamentosRegion($conexion, $idRegion);
        foreach ($arrUbigeo as $objUbigeo) {
            $objRegion = $objUbigeo->getObjRegion();
            $cadena .= '<option value="' . $objUbigeo->__get('_codDpto')
                     . '">' . $objUbigeo->__get('_nombre') . '</option>';
        }
        echo $cadena;
    }
    
    /**
     * @author Fernando Esteban Valerio <festeban@gmd.com.pe>
     * @method viewAddUbigeo
     * Mostrar formulario de registro de ubigeo.
     * @global conexion $conexion Objeto de conexion a la BD creada con PDO
     * @return void
     */
    public function viewAddUbigeo()
    {
        global $conexion;
    
        $objeto = new Data_RegionEdi();
        $arrRegion = $objeto->listar($conexion);

        $objeto = new Data_Ubigeo();
        $arrUbigeo = $objeto->departamentosUbigeo($conexion);

        $ruta = $this->_ruta . 'ubigeo/v_region_ubigeo_add.phtml';
        include $ruta;
    }

    /**
     * @author Fernando Esteban Valerio <festeban@gmd.com.pe>
     * @method viewAdministracionMain     
     * Mostrar el listado de mantenimientos.
     * @global conexion $conexion Objeto de conexion a la BD creada con PDO.
     * @return void
     */
    public function viewAdministracionMain()
    {
        global $conexion;
        
        //Validacion de acceso al submodulo
        $objAcceso = new Data_SubmodulosAccesos();
        $acceso = $objAcceso->verificarAccesoSubmodulo(
            $conexion, $_SESSION['USUARIO_PERFIL']->__get('_idPerfil'),
            $_SESSION['USUARIO']->__get('_idEmpresa'),
            $_SESSION['USUARIO']->__get('_idArea'),
            $_SESSION['USUARIO']->__get('_idZonal')
        );
        if ( $_SESSION['USUARIO_PERFIL']->__get('_codPerfil') == 'spu' ) {
            $acceso = 1;
        }
        //FIN Validacion de acceso alsubmodulos
        
        include 'vistas/administracion.maps.main.phtml';
    }    
}
$objAdministracion = new AdministracionMaps();