<?php

/**
 * @package     /modulos/maps
 * @name        asignacion.tecnicos.php
 * @category    Controller
 * @author      Wilder Sandoval Huamani <wsandoval@gmd.com.pe>
 * @copyright   2011 Telefonica del Peru
 * @version     1.0 - 2011/12/19
 */

require_once "../../config/web.config.php";
include_once APP_DIR . 'autoload.php';
include_once APP_DIR . 'session.php';


function generaColorPoligono()
{
    $arrValores = array(
        '0','1','2','3','4','5','6','7','8','9',
        'A','B','C','D','E','F'
    );

    $color = '';
    for ( $i=1; $i<=6; $i++ ) {
        $rand = rand(1, 15);
        $color .= $arrValores[$rand];
    }

    return '#' . $color;
}

function cambiarZonal($negocio='', $zonal='', $idEmpresa='')
{
    try {
        global $conexion;

        $idUsuario = $_SESSION['USUARIO']->__get('_idUsuario');
        
        if ( $negocio == 'CATV' ) {
            
            $script = '<script type="text/javascript">
                $(function(){
                    $("select.clsMultiple").multiselect().multiselectfilter();
                });
                $("#nodo").multiselect({
                    close: function(){
                        var nodos_array = [];
                        var nodos_arr = $("#nodo").val();
                        
                        if ( nodos_arr != null ) {
                            for( i in nodos_arr) {
                                nodos_array.push(nodos_arr[i]);
                            }
                        } else {
                            alert("Ningun nodo seleccionado");
                            return false;
                        }

                        var data_content = {
                            "action"        : "buscarTroba",
                            "nodos_array"   : nodos_array
                        };
                        $.ajax({
                            type:   "POST",
                            url:    "../asignacion.tecnicos.php",
                            data:   data_content,
                            dataType: "html",
                            success: function(datos) {
                                if ( datos != "" ) {
                                    $("#divTroba").html(datos);
                                }
                            }
                        });
                    }
                });
            </script>';
            
            $objUsuarioNodo = new Data_UsuarioNodo();
            
            //verifico si no ha seleccionado ninguna empresa
            $arrEmpresas = array();
            if ( $idEmpresa == '' ) {
                $objEmpresa = new Data_Empresa();
                foreach ($_SESSION['USUARIO_EMPRESA'] as $empresa) {
                    $objEmpresas = $objEmpresa->offSetGet($conexion, $empresa->__get('_idEmpresa'));
                    $arrEmpresas[] = $objEmpresas->__get('_idEmpresa');
                }
            } else {
                $arrEmpresas[] = $idEmpresa;
            }
            
            $arrNodo = $objUsuarioNodo->listarNodosXUsuarioZonalEmpresa(
                $conexion, $idUsuario, $zonal, $arrEmpresas
            );

            $selNodo = '<select id="nodo" name="nodo" class="clsMultiple" multiple="multiple">';
            foreach ($arrNodo as $nodo) {
                $selNodo .= '<option value="' . $nodo['nodo'] . '">' . $nodo['nodo'] . ' - ' . $nodo['desc_nodo'] . '</option>';
            }
            $selNodo .= '</select>';


            $selTroba = '<select id="troba" name="troba" class="clsMultiple" multiple="multiple">';
            $selTroba .= '<option value="">Todos</option>';
            $selTroba .= '</select>';
            
            echo $script . $selNodo . '|' . $script . $selTroba;
            exit;
            
        } else {
            
            $script = '<script type="text/javascript">
                $(function(){
                    $("select.clsMultiple").multiselect().multiselectfilter();
                });
            </script>';
            
            $objTerminales  = new Data_FfttTerminales();
            
            $arrMdf = $objTerminales->get_mdf_by_zonal_and_idusuario($conexion, $zonal, $idUsuario);

            $selMdf = '<select id="mdf" name="mdf" class="clsMultiple" multiple="multiple">';
            foreach ($arrMdf as $mdf) {
                $selMdf .= '<option value="' . $mdf['mdf'] . '">' . $mdf['mdf'] . ' - ' . $mdf['nombre'] . '</option>';
            }
            $selMdf .= '</select>';
            
            echo $script . $selMdf;
            exit;
        }


    } catch(PDOException $e) {
        //echo "Failed: " . $e->getMessage();
    }
}

function buscarTroba($arrayNodos)
{
    global $conexion;
    
    $script = '<script type="text/javascript">
        $(function(){
            $("select.clsMultiple").multiselect().multiselectfilter();
            $("#troba").multiselect({
                close: function(){
                    $("#poligono_troba").removeAttr("checked");
                }
            });
        });
    </script>';

    
    $selTroba = '<select id="troba" name="troba" class="clsMultiple" multiple="multiple">';
    $selTroba .= '<option value="">Todos</option>';
    $selTroba .= '</select>';
    if ( isset($arrayNodos) && !empty($arrayNodos) ) {
        $objCms = new Data_FfttCms();
        $arrTroba = $objCms->trobasXNodoCatv($conexion, '', $arrayNodos);
        
        $arrData = array();
        foreach ($arrTroba as $troba) {
            $arrData[] = array(
                'troba' => $troba['troba'],
                'nodo'	=> $troba['nodo']
            );
        }

        $selTroba = '<select id="troba" name="troba" class="clsMultiple" multiple="multiple">';
        foreach ( $arrData as $data ) {
            $selTroba .= '<option value="' . $data['nodo'] . $data['troba'] . '">' . $data['nodo'] . ' - ' . $data['troba'] . '</option>';
        }
        $selTroba .= '</select>';	

    }

    
    echo $script . $selTroba;
    exit;
}

function buscarTecnico($empresa='', $zonal='', $q='')
{
    try {
        global $conexion;

        $objTecnicos = new Data_Tecnicos();

        $arrObjTecnicos = $objTecnicos->__listTecnicosAsignacion($conexion, $empresa, $zonal, '', $q);

        $arrDataTecnicos = array();
        foreach ( $arrObjTecnicos as $objTecnico ) {
            $arrDataTecnicos[] = array(
                'idtecnico' => $objTecnico->__get('_idtecnico'),
                'cip'       => $objTecnico->__get('_cip'),
                'nombre'    => $objTecnico->__get('_app_tecnico') . ' ' . $objTecnico->__get('_apm_tecnico') . ' ' . $objTecnico->__get('_nom_tecnico')
            );
        }

        echo json_encode($arrDataTecnicos);
        exit;
        
    } catch(PDOException $e) {
        //echo "Failed: " . $e->getMessage();
    }
}

function buscarTecnicos($empresa, $zonal)
{
    global $conexion;
    
    $objTecnicos = new Data_Tecnicos();

    $arrObjTecnicos = $objTecnicos->listTecnicosAsignacion(
        $conexion, $empresa, $zonal
    );

    $arrDataTecnicos = array();
    foreach ( $arrObjTecnicos as $objTecnico ) {
        $arrDataTecnicos[] = array(
            'cip'    => $objTecnico->__get('_cip'),
            'nombre' => $objTecnico->__get('_appTecnico') . ' ' . $objTecnico->__get('_apmTecnico') . ' ' . $objTecnico->__get('_nomTecnico')
        );
    }

    echo json_encode($arrDataTecnicos);
    exit;
}

function clearasignacionMultiple()
{
    //seteamos los valores por default
    $_SESSION['TECNICO_TAPS_SELECTED'] 	= array();
    $_SESSION['TECNICO_AVERIAS']        = array();

    $arrDataResult = array('result'=>'1');

    echo json_encode($arrDataResult);
    exit;    
}

function dibujaMapaNodo($areaMdf, $arrNodo=array())
{
    try {
        global $conexion;

        $arrPoligonos = array();
        $promX = $promY = 0;

        if ( isset($areaMdf) && $areaMdf == 'si' ) {
            $objCapasarea = new Data_FfttCapasArea();

            $arrPoligonos = array();

            //lleno el array para el tipo NODO
            if ( !empty( $arrNodo ) ) {
                foreach ( $arrNodo as $nodo ) {
                    //capturando los X,Y de los elementos seleccionados
                    $puntosXyNodo = $objCapasarea->getXysByTipoCapaAndElemento(
                        $conexion, 'NODO', $nodo, 0
                    );
                    if ( !empty($puntosXyNodo) ) {
                        $arrPoligonos[] = array(
                            'coords' => $puntosXyNodo,
                            'color'  => generaColorPoligono()
                        ); 
                    }
                }

                if ( !empty($arrPoligonos) ) {
                    $countXy = count($arrPoligonos[0]['coords']);

                    $sumX = $sumY = 0;
                    foreach ( $arrPoligonos[0]['coords'] as $puntoXY ) {
                        $sumX += $puntoXY['x'];
                        $sumY += $puntoXY['y'];
                    }

                    $promX = $sumX / $countXy;
                    $promY = $sumY / $countXy;
                }
            }
        }

        $arrDataResult = array(
            'success'       => '1',
            'xy_poligono'   => $arrPoligonos,
            'center_map'    => array(
                'x' => $promX,
                'y' => $promY
            )
        );

        echo json_encode($arrDataResult);
        exit;

    } catch(PDOException $e) {
        $arrDataResult = array(
            'success'   => '0',
            'msg'       => 'Ha ocurrido un error. Vuelva a intentarlo mas tarde.'
        );
        echo json_encode($arrDataResult);
        exit;
    }
}

function dibujaMapaTroba($areaTroba, $arrTroba=array())
{
    try {
        global $conexion;

        $arrPoligonos = array();
        $promX = $promY = 0;

        if ( isset($areaTroba) && $areaTroba == 'si' ) {
            $objCapasarea = new Data_FfttCapasArea();

            $arrPoligonos = array();

            //lleno el array para el tipo TROBA/PLANO
            if ( !empty( $arrTroba ) ) {
                foreach ( $arrTroba as $troba ) {
                    //capturando los X,Y de los elementos seleccionados
                    $puntosXyTroba = $objCapasarea->getXysByTipoCapaAndElemento(
                        $conexion, 'PLANOS', $troba, 0
                    );
                    if ( !empty($puntosXyTroba) ) {
                        $arrPoligonos[] = array(
                            'coords' => $puntosXyTroba,
                            'color'  => generaColorPoligono()
                        ); 
                    }
                }

                if ( !empty($arrPoligonos) ) {
                    $countXy = count($arrPoligonos[0]['coords']);

                    $sumX = $sumY = 0;
                    foreach ( $arrPoligonos[0]['coords'] as $puntoXY ) {
                        $sumX += $puntoXY['x'];
                        $sumY += $puntoXY['y'];
                    }

                    $promX = $sumX / $countXy;
                    $promY = $sumY / $countXy;
                }
            }
        }

        $arrDataResult = array(
            'success'     => '1',
            'xy_poligono' => $arrPoligonos,
            'center_map'  => array(
                'x' => $promX,
                'y' => $promY
            )
        );

        echo json_encode($arrDataResult);
        exit;

    } catch(PDOException $e) {
        $arrDataResult = array(
            'success'   => '0',
            'msg'       => 'Ha ocurrido un error. Vuelva a intentarlo mas tarde.'
        );
        echo json_encode($arrDataResult);
        exit;
    }
}

function asignacionMultiple($zonal, $mdf, $carmario, $cbloque, $terminal)
{
    global $conexion;
    
    $_SESSION['TECNICO_TAPS_SELECTED'][$zonal . '|' . $mdf . '|' . $carmario . '|' . $cbloque . '|' . $terminal] = 1;

    $objPenPais = new Data_PenPais();

    $averias = $objPenPais->listaraveriasTerminal(
        $conexion, $zonal, $mdf, $carmario, $cbloque, $terminal
    );

    $arrDataResult = array();
    if ( !empty($averias) ) {

        $objGestionActuaciones = new Data_GestionActuaciones();

        $i = 1;
        foreach ( $averias as $averia ) {

            //Verifico si existe en webunificada.gestion_actuaciones 
            $gestActuacionExists = $objGestionActuaciones->getIdGestionActuacion($conexion, $averia['averia']);

            $flagAveria = 0;
            $idGestionActu = '';
            if ( $gestActuacionExists ) {
                $flagAveria = 1;
                $idGestionActu = $gestActuacionExists;
            }

            $_SESSION['TECNICO_AVERIAS'][$averia['averia']][] = $averia['averia'] . '|' . $flagAveria . '|Averia|' . $idGestionActu . '|' . $averia['idempresa_webu'] . '|PEN_PAIS';
            $arrDataResult['averias'][] = array('averia'=>$averia['averia']);
        }
    }

    $arrDataResult['num_taps'] = count($_SESSION['TECNICO_TAPS_SELECTED']);

    echo json_encode($arrDataResult);
    exit;

}

function averiasTerminal($zonal, $mdf, $carmario, $cbloque, $terminal, 
        $asignacionMultiple=0)
{
    global $conexion;
    
    $objPenPais = new Data_PenPais();

    $asignacionMultiple = ( $asignacionMultiple ) ? 1 : 0;
    $clientes = array();
    if ( $asignacionMultiple ) {
        $tituloAverias = 'AVERIAS SELECCIONADAS - ASIGNACION DE TECNICO';

        $arrAverias = array();
        foreach ( $_SESSION['TECNICO_AVERIAS'] as $codAveria => $averias ) {
            $arrAverias[] = $codAveria;
        }

        $clientes = $objPenPais->listarAveriasTerminal(
            $conexion, '', '', '', '', '', $arrAverias
        );

    } else {
        $tituloAverias = 'AVERIAS ENCONTRADAS EN ESTE TERMINAL: ' . $terminal;
        $clientes = $objPenPais->listarAveriasTerminal(
            $conexion, $zonal, $mdf, $carmario, $cbloque, $terminal
        );
    }
    
    include 'vistas/v_averias_terminal.popup.php';
}

function buscarPendientes($tipo, $negocio, $zonal, $arrNodo, $arrTroba, 
        $arrMdf, $tecnico, $empresa, $prioridad)
{
    try {
        global $conexion;

        $idUsuario = $_SESSION['USUARIO']->__get('_idUsuario');
        
        $objZonal = new Data_Zonal();
        $objGestionActuacionesTecnicos = new Data_GestionActuacionesTecnicos();
        
        //seteamos los valores por default
        $_SESSION['TECNICO_TAPS_SELECTED']  = array();
        $_SESSION['TECNICO_AVERIAS']        = array();


        //carga de datos (x,y) de Zonal
        $arrayZonal = $objZonal->listar($conexion, '', $zonal);
        $arrDataZonal = array(
            'abv'	=> $arrayZonal[0]->__get('_abvZonal'),
            'name'	=> $arrayZonal[0]->__get('_descZonal'),
            'x'    	=> $arrayZonal[0]->__get('_x'),
            'y' 	=> $arrayZonal[0]->__get('_y')
        );

        
        if ( empty( $arrNodo ) ) {
            $objUsuarioNodo = new Data_UsuarioNodo();

            $arrayNodo = $objUsuarioNodo->listarNodosXUsuarioZonal(
                $conexion, $zonal, $idUsuario, '', 'CATV'
            );
            foreach ($arrayNodo as $nodo) {
                $arrNodo[] = $nodo['nodo'];
            }
        }
        

        $arrayActuaciones = array();
        if ($tipo == 'Provision') {
            $arrayActuaciones = $objGestionActuacionesTecnicos->listaProvisionMap(
                $conexion, $negocio, $zonal, $arrNodo, $arrTroba, $arrMdf, 
                $tecnico, $empresa, $prioridad
            );
        }
        if ($tipo == 'Averia') {
            $arrayActuaciones = $objGestionActuacionesTecnicos->listaAveriasMap(
                $conexion, $negocio, $zonal, $arrNodo, $arrTroba, 
                $arrMdf, $tecnico, $empresa, $prioridad
            );
        }

        $arrDataMarkers = array();
        $arrDataMarkersConXy = array();
        if ( !empty($arrayActuaciones) ) {
            
            //agrupamiento por zonal+mdf+carmario+ccable+cbloque+terminal
            //esto para georeferenciar los terminales o taps
            $arrAgrupamiento = array();
            foreach ($arrayActuaciones as $terminales) {
                $arrAgrupamiento[$terminales['zonal'].'|'.$terminales['mdf'].'|'.$terminales['carmario'].'|'.$terminales['ccable'].'|'.$terminales['cbloque'].'|'.$terminales['terminal']][] = array(
                    'l_averia'      => $terminales['pedido'],
                    'l_cliente'     => $terminales['cliente'],
                    'l_direccion'   => $terminales['direccion'],
                    'l_cip'         => $terminales['cip'],
                    'l_nomtecnico'  => $terminales['nomtecnico'],
                    'l_fecreg'      => $terminales['fec_reg']
                );
            }

            $i = 1;
            $keyAgrupamiento = '';
            foreach ($arrayActuaciones as $terminales) {
                if ( $terminales['y'] != '' && $terminales['x'] != '' ) {
                    $arrDataMarkersConXy[] = $terminales;
                }

                $keyAgrupamiento = $terminales['zonal'].'|'.$terminales['mdf'].'|'.$terminales['carmario'].'|'.$terminales['ccable'].'|'.$terminales['cbloque'].'|'.$terminales['terminal'];

                $terminales['averias_list'] = $arrAgrupamiento[$keyAgrupamiento];

                
                $terminales['fecha_agenda_full'] = '';
                if ( $terminales['idgestion_actuaciones'] ) {
                    /* Obtengo el objeto con el detalle del movimiento de la actuacion */
                    $objDetalle = $objGestionActuacionesTecnicos->ultimoMovimiento(
                        $conexion, $terminales['idgestion_actuaciones'], '0'
                    );

                    if (!$objDetalle) {
                        $objDetalle = $objGestionActuacionesTecnicos->ultimoMovimiento(
                            $conexion, $terminales['idgestion_actuaciones'], '1'
                        );
                    }

                    /* Indicadores para los sem�foros de Agendamiento */
                    $strFechaAgendaFull = $objDetalle->__get('_fechaInicioAgendamiento');

                    $terminales['fecha_agenda_full'] = $strFechaAgendaFull;
                }
                
                $arrDataMarkers[] = $terminales;
                $i++;
            }
        }

        $arrDataResult = array(
            'zonal'           => $arrDataZonal,
            'pedidos'         => $arrDataMarkers,
            'pedidos_con_xy'  => $arrDataMarkersConXy
        );

        echo json_encode($arrDataResult);
        exit;
        
    } catch(PDOException $e) {        
        $arrDataResult = array(
            'msg'   => 'Se ha producido un error en el Sistema, vuelva a intentarlo mas tarde'
        );
        
        echo json_encode($arrDataResult);
        exit;
    }
}

function iniDefault() {

    //template por default
    echo '<iframe id="ifrMain" name="ifrMain" src="modulos/maps/vistas/v_asignacion_tecnicos.ifr.php" width="100%" height="480"></iframe>';
}



        
if (!isset($_REQUEST['action']))
    $_REQUEST['action'] = '';

switch ($_REQUEST['action']) {
    case 'buscarTecnicos':
        buscarTecnicos($_POST['empresa'], $_POST['zonal']);
        break;
    case 'clear':
        clearasignacionMultiple();
        break;
    case 'dibujaMapaNodo':
        $arrNodo = ( isset($_POST['arreglo_nodo']) ) ? 
            $_POST['arreglo_nodo'] : array();
        dibujaMapaNodo($_POST['area_mdf'], $arrNodo);
        break;
    case 'dibujaMapaTroba':
        $arrTroba = ( isset($_POST['arreglo_troba']) ) ? 
            $_POST['arreglo_troba'] : array();
        dibujaMapaTroba($_POST['area_troba'], $arrTroba);
        break;
    case 'asignacionMultiple':
        asignacionMultiple(
            $_POST['zonal'], $_POST['mdf'], $_POST['carmario'], 
            $_POST['cbloque'], $_POST['terminal']
        );
        break;
    case 'buscarPendientes':
        
        $_POST['arreglo_nodo']  = ( isset($_POST['arreglo_nodo']) ) ? 
            $_POST['arreglo_nodo'] : array();
        $_POST['arreglo_troba'] = ( isset($_POST['arreglo_troba']) ) ? 
            $_POST['arreglo_troba'] : array();
        $_POST['arreglo_mdf']   = ( isset($_POST['arreglo_mdf']) ) ? 
            $_POST['arreglo_mdf'] : array();
        
        buscarPendientes(
            $_POST['tipo'], $_POST['negocio'], $_POST['zonal'], 
            $_POST['arreglo_nodo'], $_POST['arreglo_troba'], 
            $_POST['arreglo_mdf'], $_POST['tecnico'], $_POST['empresa'], 
            $_POST['prioridad']
        );
        break;
    case 'averiasTerminal':
        averiasTerminal(
            $_POST['zonal'], $_POST['mdf'], $_POST['carmario'], 
            $_POST['cbloque'], $_POST['terminal'], 
            $_POST['asignacion_multiple']
        );
        break;
    case 'averiasTerminalMultiple':
        averiasTerminal('', '', '', '', '', $_POST['asignacion_multiple']);
        break;
    case 'buscarTecnico':
        buscarTecnico($_REQUEST['empresa'], $_REQUEST['zonal'], $_REQUEST['q']);
        break;
    case 'buscarTroba':
        buscarTroba($_POST['nodos_array']);
        break;
    case 'buscarNodo':
        BuscarNodo($_POST['zonal']);
        break;
    case 'cambiarZonal':
        cambiarZonal($_POST['negocio'], $_POST['zonal'], $_POST['empresa']);
        break;
    default:
        iniDefault();
        break;
}