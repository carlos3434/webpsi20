<?php
ini_set('error_reporting', E_ALL);

require_once "../../config/web.config.php";
require_once APP_DIR . 'autoload.php';
require_once APP_DIR . 'session.php';

require_once '../../modulos/maps/util/functions.registro.trabajo.fftt.php';


 /*
 * Sistema Web Unificada - STC TDP
 * Planificacion y Soluciones Informaticas
 * GMD S.A
 * Autor <wsandoval@gmd.com.pe>
 * Modulo Marcaci�n de trabajos en planta y aver�as masivas
 */


$etiqueta_titulo = ETIQUETA_TITULO . ' | Bandeja de trabajos en planta y aver�as masivas';
$nombre_modulo = 'modulos/maps/';

$_SESSION['SUBMODULO_ACTUAL'] = 'maps/bandeja.registro.trabajo.fftt.php';


function IniDefault() {
    global $conexion, $etiqueta_titulo, $nombre_modulo;
    
    $obj_jefatura           = new data_ffttJefaturaZonales();
    $obj_tipo_negocio       = new data_TipoNegocio();
    $obj_registro_trabajo   = new data_RegistroTrabajoPlanta();

    /*
    $obj_GestionRegistroTrabajoFFtt = new Logic_GestionRegistroTrabajoFFtt();
    
    $data = array(
        'tipo'      => 'Averia',
        'negocio'   => 'CATV',
        'mdf'       => 'LO',
        'carmario'  => 'R001',
        'ccable'    => '',
        'parpri'    => '',
        'cbloque'   => '08',
        'terminal'  => '08',
        'borne'     => ''
    );
    $data = array(
        'tipo'      => 'Provision',
        'negocio'   => 'ADSL',
        'mdf'       => 'LO',
        'c6'        => 'A088',
        'c7'        => 'P/18',
        'c8'        => '005'
    );
    
    $registro_trabajo_fftt = $obj_GestionRegistroTrabajoFFtt->__getRegistroTrabajoFFtt($conexion, $data);
    //print_r($registro_trabajo_fftt);
    
    if( !empty($registro_trabajo_fftt) ) {
        echo 'SI existe';
    }else {
        echo 'NO existe';
    }
    */
    
    
    $id_area    = $_SESSION['USUARIO']->__get('idarea');
    /*
     * Validacion para que solos usuarios de las areas puedan registrar
     * 41 - MANTENIMIENTO DE PLANTA EXTERNA TDP
     */
    $user_permitido = '0'; 
    if($id_area == '41') { 
        $user_permitido = '1'; 
    }
    

    $filtro_array = array();

    $is_tdp     = ($_SESSION['USUARIO']->__get('idempresa')=='1')?'1':'0';
    $is_eecc    = ($_SESSION['USUARIO']->__get('idempresa')!='1')?'1':'0';

    if( $is_tdp == '1' ) {
        $arreglo_jefatura = $obj_jefatura->__getJefaturaZonal_CodJefatura($conexion);
    }else {
        $arreglo_jefatura = $obj_jefatura->__getJefaturaZonal_CodJefatura($conexion,'','','', $_SESSION['USUARIO']->__get('idempresa'));
    }
    
    $arreglo_negocio_pex    = $obj_tipo_negocio->__listNegociosInnova($conexion);

    
    $filtro_array = array();
    
    $jefatura_arr = array();
    foreach( $arreglo_jefatura as $jefatura_obj ) {
        $jefatura_arr[] = $jefatura_obj->__get('jefatura');
    }
    $filtro_array['codjefatura'] = $jefatura_arr;
    
    
    $pagina = 1;
    $trabajos_planta = $obj_registro_trabajo->__list($conexion, $filtro_array, $pagina);

    $total_registros = $obj_registro_trabajo->__listTotal($conexion, $filtro_array);
    
	
    $num_paginas = ceil($total_registros/TAM_PAG_LISTADO);

    
    $etiqueta_titulo .= ' | Areas | Lista ';
    include 'vistas/v_bandeja.registro.trabajo.fftt.php';

    $cadena = '<script type="text/javascript">
                    $(function() {

                        $("#paginacion").paginate({
                            count                   : '.$num_paginas.',
                            start                   : 1,
                            display                 : 10,
                            border                  : true,
                            border_color            : "#FFFFFF",
                            text_color              : "#FFFFFF",
                            background_color        : "#0080AF",
                            border_hover_color      : "#CCCCCC",
                            text_hover_color        : "#000000",
                            background_hover_color  : "#FFFFFF",
                            images                  : false,
                            mouse                   : "press",
                            onChange : function(page){
                                loader("start");
                                $("#pag_actual").attr("value", page);
                                var tl=$("#tl_registros").attr("value");
                                $.post("modulos/maps/bandeja.registro.trabajo.fftt.php?cmd=cambiarPaginaRegistroTrabajos", { 
                                    pagina: page, total: tl   
                                    }, function(data){
                                        $("#tb_resultado").empty();
                                        $("#tb_resultado").append(data);
                                        loader("end");
                                    }
                                );
                            }
                        });
                    });
                </script>';
    echo $cadena;     
}

function FiltroBusqueda($pagina='', $total='', $codjefatura='', $estado='', $negocio='', $tipored='', $codelementoptr='', $campo_filtro='', $campo_valor=''){
    global $conexion, $nombre_modulo;

    $obj_jefatura           = new data_ffttJefaturaZonales();
    $obj_registro_trabajo   = new data_RegistroTrabajoPlanta();
    
    if($pagina == '') $pagina = 1;
    
    $is_tdp     = ($_SESSION['USUARIO']->__get('idempresa')=='1')?'1':'0';
    $is_eecc    = ($_SESSION['USUARIO']->__get('idempresa')!='1')?'1':'0';

    $filtro_array = array();
    if( $codjefatura != '' ) {
        $filtro_array['codjefatura'] = array($codjefatura);
    }else {
        if( $is_tdp == '1' ) {
            $arreglo_jefatura = $obj_jefatura->__getJefaturaZonal_CodJefatura($conexion);
        }else {
            $arreglo_jefatura = $obj_jefatura->__getJefaturaZonal_CodJefatura($conexion,'','','', $_SESSION['USUARIO']->__get('idempresa'));
        }
        
        $jefatura_arr = array();
        foreach( $arreglo_jefatura as $jefatura_obj ) {
            $jefatura_arr[] = $jefatura_obj->__get('jefatura');
        }
        $filtro_array['codjefatura'] = $jefatura_arr;
    }
    
    
    if( $estado != '' ) {
        $filtro_array['estado'] = $estado;
    }
    if( $negocio != '' ) {
        $filtro_array['negocio'] = $negocio;
    }
    if( $tipored != '' ) {
        $filtro_array['tipored'] = $tipored;
    }
    if( $codelementoptr != '' ) {
        $filtro_array['codelementoptr'] = $codelementoptr;
    }
    if( $campo_filtro != '' ) {
        $filtro_array['campo_filtro'] = $campo_filtro;
    }
    if( $campo_valor != '' ) {
        $filtro_array['campo_valor'] = $campo_valor;
    }
	
    
    $pagina = 1;
    $trabajos_planta = $obj_registro_trabajo->__list($conexion, $filtro_array, $pagina);

    $total_registros = $obj_registro_trabajo->__listTotal($conexion, $filtro_array);
    //echo $total_registros;
	
    $num_paginas = ceil($total_registros/TAM_PAG_LISTADO);
	

    include 'vistas/v_bandeja.registro.trabajo.fftt.paginado.php';
	
	
	
    $cadena = '<script type="text/javascript">
                    $("#cont_num_resultados_head").html(' . $total_registros . ');
                    $("#cont_num_resultados_foot").html(' . $total_registros . ');
                    $("#tl_registros").attr("value", "' . $total_registros . '");
                    $("#paginacion").empty();
                    ';
                    if($total_registros > 0) {
                            $cadena .= '
                                    $(function() {
                                        $("#paginacion").paginate({
                                            count                   : '.$num_paginas.',
                                            start                   : 1,
                                            display                 : 10,
                                            border                  : true,
                                            border_color            : "#FFFFFF",
                                            text_color              : "#FFFFFF",
                                            background_color        : "#0080AF",
                                            border_hover_color      : "#CCCCCC",
                                            text_hover_color        : "#000000",
                                            background_hover_color  : "#FFFFFF",
                                            images                  : false,
                                            mouse                   : "press",
                                            onChange : function(page){
                                                loader("start");
                                                $("#pag_actual").attr("value", page);
                                                var tl=$("#tl_registros").attr("value");
                                                $.post("modulos/maps/bandeja.registro.trabajo.fftt.php?cmd=cambiarPaginaRegistroTrabajos", { 
                                                        pagina: page, total: tl, codjefatura: "' . $codjefatura . '", estado: "' . $estado . '", negocio: "' . $negocio . '", tipored: "' . $tipored . '", codelementoptr: "' . $codelementoptr . '", campo_filtro: "' . $campo_filtro . '", campo_valor: "' . $campo_valor . '" 
                                                    }, function(data){
                                                        $("#tb_resultado").empty();
                                                        $("#tb_resultado").append(data);
                                                        loader("end");
                                                    }
                                                );
                                            }
                                        });
                                    });';
                    }
                    $cadena .= '</script>';
	echo $cadena;

}

function cambiarPaginaRegistroTrabajos($pagina='', $total='', $codjefatura='', $estado='', $negocio='', $tipored='', $codelementoptr='', $campo_filtro='', $campo_valor=''){
    global $conexion, $nombre_modulo;
    
    $obj_jefatura           = new data_ffttJefaturaZonales();
    $obj_registro_trabajo   = new data_RegistroTrabajoPlanta();
    
    if($pagina == '') $pagina = 1;
    
    $is_tdp     = ($_SESSION['USUARIO']->__get('idempresa')=='1')?'1':'0';
    $is_eecc    = ($_SESSION['USUARIO']->__get('idempresa')!='1')?'1':'0';

    $filtro_array = array();
    if( $codjefatura != '' ) {
        $filtro_array['codjefatura'] = array($codjefatura);
    }else {
        if( $is_tdp == '1' ) {
            $arreglo_jefatura = $obj_jefatura->__getJefaturaZonal_CodJefatura($conexion);
        }else {
            $arreglo_jefatura = $obj_jefatura->__getJefaturaZonal_CodJefatura($conexion,'','','', $_SESSION['USUARIO']->__get('idempresa'));
        }
        
        $jefatura_arr = array();
        foreach( $arreglo_jefatura as $jefatura_obj ) {
            $jefatura_arr[] = $jefatura_obj->__get('jefatura');
        }
        $filtro_array['codjefatura'] = $jefatura_arr;
    }
    
    
    if( $estado != '' ) {
        $filtro_array['estado'] = $estado;
    }
    if( $negocio != '' ) {
        $filtro_array['negocio'] = $negocio;
    }
    if( $tipored != '' ) {
        $filtro_array['tipored'] = $tipored;
    }
    if( $codelementoptr != '' ) {
        $filtro_array['codelementoptr'] = $codelementoptr;
    }
    if( $campo_filtro != '' ) {
        $filtro_array['campo_filtro'] = $campo_filtro;
    }
    if( $campo_valor != '' ) {
        $filtro_array['campo_valor'] = $campo_valor;
    }

    
    $trabajos_planta = $obj_registro_trabajo->__list($conexion, $filtro_array, $pagina);

    $total_registros = $obj_registro_trabajo->__listTotal($conexion, $filtro_array);
    //echo $total_registros;
	
    $num_paginas = ceil($total_registros/TAM_PAG_LISTADO);
	

    include 'vistas/v_bandeja.registro.trabajo.fftt.paginado.php';

}

function viewDetalleRegistroTrabajo($id_registro_trabajo_planta){
    global $conexion, $nombre_modulo;

    $obj_registro_trabajo       = new data_RegistroTrabajoPlanta();
    $obj_registro_trabajo_mov   = new Data_RegistroTrabajoMov(); 
    
    $estados_array = array('PENDIENTE','CANCELADO','LIQUIDADO');
    
    $registro_trabajo = $obj_registro_trabajo->__get_by_id($conexion, $id_registro_trabajo_planta);

    if( empty($registro_trabajo) ) {
        echo 'No se encontro datos para el registro seleccionado';
        exit;
    }
    
    $id_registro_trabajo_planta = $registro_trabajo[0]['id_registro_trabajo_planta'];
    $arr_obj_registro_trabajo_mov = $obj_registro_trabajo_mov->movimientosRegistroTrabajo($conexion, $id_registro_trabajo_planta);
	
    include 'vistas/v_bandeja.registro.trabajo.fftt.detalle.php';

}

function viewCambiarFechaRegistroTrabajo($id_registro_trabajo_planta){
    global $conexion, $nombre_modulo;

    $obj_registro_trabajo       = new data_RegistroTrabajoPlanta();
    
    $estados_array = array('PENDIENTE','CANCELADO','LIQUIDADO');
    
    $registro_trabajo = $obj_registro_trabajo->__get_by_id($conexion, $id_registro_trabajo_planta);

    if( empty($registro_trabajo) ) {
        echo 'No se encontro datos para el registro seleccionado';
        exit;
    }
	
    include 'vistas/v_bandeja.registro.trabajo.fftt.cambiarfecha.php';

}

function viewGestionarRegistroTrabajo($id_registro_trabajo_planta){
    global $conexion, $nombre_modulo;

    $obj_registro_trabajo   = new data_RegistroTrabajoPlanta();
    
    $estados_array = array('PENDIENTE','CANCELADO','LIQUIDADO');
    
    $registro_trabajo = $obj_registro_trabajo->__get_by_id($conexion, $id_registro_trabajo_planta);

    if( empty($registro_trabajo) ) {
        echo 'No se encontro datos para el registro seleccionado';
        exit;
    }
	
    include 'vistas/v_bandeja.registro.trabajo.fftt.gestionar.php';

}

function RegistrarCambioEstado($idregistro_trabajo_planta, $estado, $fecha_fin_real_solucion, $observaciones) {
    try {
        global $conexion, $nombre_modulo;

        $obj_registro_trabajo   = new data_RegistroTrabajoPlanta();
        
        $f_fin_real_solucion = substr($fecha_fin_real_solucion,6,4) . '-' . substr($fecha_fin_real_solucion,3,2) . '-' . substr($fecha_fin_real_solucion,0,2) . ' ' . substr($fecha_fin_real_solucion,11,2) . ':' . substr($fecha_fin_real_solucion,14,2);
        
        $data = array(
            'idregistro_trabajo_planta' => $idregistro_trabajo_planta,
            'estado'                    => $estado,
            'fecha_fin_real_solucion'   => $f_fin_real_solucion,
            'observaciones'             => $observaciones,
            'user_insert'               => $_SESSION['USUARIO']->__get('idusuario'),
            'fecha_insert'              => date('Y-m-d H:i:s')
        );

        $data_result = $obj_registro_trabajo->__saveCambioEstado($conexion, $data);

        echo json_encode($data_result);
        exit;

    } catch (PDOException $e) {
        die($e->getMessage());
    }  
}

function RegistrarCambioFecha($idregistro_trabajo_planta, $fecha_inicio_trabajo, $fecha_fin_trabajo) {
    try {
        global $conexion, $nombre_modulo;

        $obj_registro_trabajo   = new data_RegistroTrabajoPlanta();
        
        $f_inicio_trabajo   = substr($fecha_inicio_trabajo,6,4) . '-' . substr($fecha_inicio_trabajo,3,2) . '-' . substr($fecha_inicio_trabajo,0,2) . ' ' . substr($fecha_inicio_trabajo,11,2) . ':' . substr($fecha_inicio_trabajo,14,2);
        $f_fin_trabajo      = substr($fecha_fin_trabajo,6,4) . '-' . substr($fecha_fin_trabajo,3,2) . '-' . substr($fecha_fin_trabajo,0,2) . ' ' . substr($fecha_fin_trabajo,11,2) . ':' . substr($fecha_fin_trabajo,14,2);
        
        $data = array(
            'idregistro_trabajo_planta' => $idregistro_trabajo_planta,
            'fecha_inicio_trabajo'      => $f_inicio_trabajo,
            'fecha_fin_trabajo'         => $f_fin_trabajo,
            'user_insert'               => $_SESSION['USUARIO']->__get('idusuario'),
            'fecha_insert'              => date('Y-m-d H:i:s')
        );

        $data_result = $obj_registro_trabajo->__saveCambioFecha($conexion, $data);

        echo json_encode($data_result);
        exit;

    } catch (PDOException $e) {
        die($e->getMessage());
    }  
}


if (!isset($_GET['cmd']))
    $_GET['cmd'] = '';

switch ($_GET['cmd']) {
    case 'RegistrarCambioFecha':
        RegistrarCambioFecha($_POST['id_registro_trabajo_planta'], $_POST['fecha_inicio_trabajo'], $_POST['fecha_fin_trabajo']);
        break;
    case 'RegistrarCambioEstado':
        RegistrarCambioEstado($_POST['id_registro_trabajo_planta'], $_POST['estado'], $_POST['fecha_fin_real_solucion'], $_POST['observaciones']);
        break;
    case 'viewDetalleRegistroTrabajo':
        viewDetalleRegistroTrabajo($_POST['id_registro_trabajo_planta']);
        break;
    case 'viewCambiarFechaRegistroTrabajo':
        viewCambiarFechaRegistroTrabajo($_POST['id_registro_trabajo_planta']);
        break;
    case 'viewGestionarRegistroTrabajo':
        viewGestionarRegistroTrabajo($_POST['id_registro_trabajo_planta']);
        break;
    case 'cambiarPaginaRegistroTrabajos':
        cambiarPaginaRegistroTrabajos($_POST['pagina'], $_POST['total'], $_POST['codjefatura'], $_POST['estado'], $_POST['negocio'], $_POST['tipored'], $_POST['codelementoptr'], $_POST['campo_filtro'], $_POST['campo_valor']);
        break;
    case 'FiltroBusqueda':
        FiltroBusqueda($_POST['pagina'], $_POST['total'], $_POST['codjefatura'], $_POST['estado'], $_POST['negocio'], $_POST['tipored'], $_POST['codelementoptr'], $_POST['campo_filtro'], $_POST['campo_valor']);
        break;
    default:
	IniDefault();
        break;
}
?>

