<?php
require_once "/var/www/webunificada/config/web.config.php";
include_once APP_DIR . 'autoload.php';
include_once APP_DIR . 'session.php';


function genera_color_poligono() {
	$valores_arr = array('0','1','2','3','4','5','6','7','8','9','A','B','C','D','E','F');
	
	$color = '';
	for( $i=1; $i<=6; $i++ ) {
		$rand = rand(1, 16);
		$color .= $valores_arr[$rand];
	}
	
	return '#' . $color;
}


if( isset($_POST['action']) && $_POST['action'] == 'buscar' ) {

	$obj_capasarea = new data_ffttCapasArea();

	$poligonos_array = array();
	
	//lleno el array para el tio DISTRITO
	if( !empty( $_POST['arreglo_distrito'] ) ) {
		foreach( $_POST['arreglo_distrito'] as $distrito ) {
			//capturando los X,Y de los elementos seleccionados
			$puntosXY_distrito = $obj_capasarea->get_xys_by_tipocapa_and_elemento($conexion, 'DISTRITO', $distrito);
			$poligonos_array[] = array(
				'coords' => $puntosXY_distrito,
				'color'	 => genera_color_poligono()
			); 
		}
	}
	
	//lleno el array para el tio NODO
	if( !empty( $_POST['arreglo_nodo'] ) ) {
		foreach( $_POST['arreglo_nodo'] as $nodo ) {
			//capturando los X,Y de los elementos seleccionados
			$puntosXY_nodo = $obj_capasarea->get_xys_by_tipocapa_and_elemento($conexion, 'NODO', $nodo);
			$poligonos_array[] = array(
				'coords' => $puntosXY_nodo,
				'color'	 => genera_color_poligono()
			); 
		}
	}
	
	//lleno el array para el tio MDF
	if( !empty( $_POST['arreglo_mdf'] ) ) {
		foreach( $_POST['arreglo_mdf'] as $mdf ) {
			//capturando los X,Y de los elementos seleccionados
			$puntosXY_mdf = $obj_capasarea->get_xys_by_tipocapa_and_elemento($conexion, 'MDF', $mdf);
			$poligonos_array[] = array(
				'coords' => $puntosXY_mdf,
				'color'	 => genera_color_poligono()
			); 
		}
	}
	
	//lleno el array para el tio PLANO
	if( !empty( $_POST['arreglo_plano'] ) ) {
		foreach( $_POST['arreglo_plano'] as $plano ) {
			//capturando los X,Y de los elementos seleccionados
			$puntosXY_plano = $obj_capasarea->get_xys_by_tipocapa_and_elemento($conexion, 'PLANOS', $plano);
			$poligonos_array[] = array(
				'coords' => $puntosXY_plano,
				'color'	 => genera_color_poligono()
			); 
		}
	}
	
	
	if( empty($poligonos_array) ) {
	
		$poligonos_array = array();
	}
	
	//capturando el primer punto de las capas
	$x = $poligonos_array[0]['coords'][0]['x'];
	$y = $poligonos_array[0]['coords'][0]['y'];
	
	
	$count_xy = count($poligonos_array[0]['coords']);
	
	$sum_x = $sum_y = 0;
	foreach( $poligonos_array[0]['coords'] as $puntoXY ) {
		$sum_x += $puntoXY['x'];
		$sum_y += $puntoXY['y'];
	}
	
	$prom_x = $sum_x / $count_xy;
	$prom_y = $sum_y / $count_xy;
	

	$data_result = array(
		'xy_poligono' => $poligonos_array,
		'x'			  => $x,
		'y'			  => $y,
		'center_map'  => array(
			'x' => $prom_x,
			'y' => $prom_y
		)
	);
	
	echo json_encode($data_result);
	exit;

}
else {

?>

	<!-- template por default -->
	<iframe id="ifrMain" name="ifrMain" src="modulos/maps/vistas/v_agregarcomp_poligono.ifr.php" width="100%" height="480"></iframe>

<?php 
}
?>