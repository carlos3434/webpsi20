<?php

/**
 * @package     modulos/maps/
 * @name        gescomp.duna.adsl.php
 * @category    Controller
 * @author      Wilder Sandoval Huamani <wsandoval@gmd.com.pe>
 * @copyright   2011 Telefonica del Peru
 * @version     1.0 - 2011/10/25
 */

require_once "../../config/web.config.php";
include_once APP_DIR . 'autoload.php';
include_once APP_DIR . 'session.php';


function generaColorPoligono()
{
    $arrValores = array('0','1','2','3','4','5','6','7','8','9',
            'A','B','C','D','E','F');

    $color = '';
    for ( $i=1; $i<=6; $i++ ) {
        $rand = rand(1, 15);
        $color .= $arrValores[$rand];
    }

    return '#' . $color;
}

//listados anidados de ubigeo
function listarProvincias( $departamento )
{
    global $conexion;
    
    $objDunaAdsl = new Data_DunaAdsl();

    $arrObjProvincias = $objDunaAdsl->listarProvincias($conexion, $departamento);

    $arrData = array();
    foreach ($arrObjProvincias as $objProvincia) {
        $arrData[] = array(
            'IDprov'     => $objProvincia->__get('_provincia'),
            'Desprov'    => $objProvincia->__get('_provincia')
        );
    }
    
    echo json_encode($arrData);
    exit;
}

function listarDistritos( $departamento, $provincia )
{
    global $conexion;
    
    $objDunaAdsl = new Data_DunaAdsl();

    $arrObjDistritos = $objDunaAdsl->listarDistritos($conexion, $departamento, $provincia);

    $arrData = array();
    foreach ($arrObjDistritos as $objDistrito) {
        $arrData[] = array(
            'IDdist'     => $objDistrito->__get('_distrito'),
            'Desdist'    => $objDistrito->__get('_distrito')
        );
    }
    echo json_encode($arrData);
    exit;
}

function listarDunaAdsl($departamento, $provincia, $distrito, $datosXY)
{
    global $conexion;

    $objDunaAdsl = new Data_DunaAdsl();
    
    $arrDataZonal = array(
        'x' => '-77.0572634277344',
        'y' => '-12.0638365722451'
    );
    
    $arrDuna = $objDunaAdsl->listarDunaAdslMapa($conexion, $departamento, $provincia, $distrito, $datosXY);
    
    $arrDataMarkers = array();
    $arrDataMarkersConXy = array();
    $i = 1;
    foreach ($arrDuna as $duna) {
        if ( $duna['x'] != '' && $duna['y'] != '' ) {
            $arrDataMarkersConXy[] = $duna;
        }

        $arrDataMarkers[] = $duna;
        $i++;
    }

        
    $arrDataResult = array(
        'zonal'             => $arrDataZonal,
        'edificios'         => $arrDataMarkers,
        'edificios_con_xy'  => $arrDataMarkersConXy
    );
    
    echo json_encode($arrDataResult);
    exit;   
}

function grabarDatos($departamento, $provincia, $distrito, $xy)
{
    global $conexion;
    
    $objDunaAdsl = new Data_DunaAdsl();

    $arrDuna = $objDunaAdsl->listarDunaAdslMapa($conexion, $departamento, $provincia, $distrito);
    
    $idUsuario = $_SESSION['USUARIO']->__get('_idUsuario');
    
    $arrDunaSelected = array();
    foreach ( $xy as $pkduna => $valduna ) {
        if ( $valduna['x'] != '' || $valduna['y'] != '' ) {
        
            $arrDunaSelected[$pkduna] = array(
                'x' => $valduna['x'],
                'y' => $valduna['y']
            );
        }
    }
    
    //update solo a los que surgieron cambios en duna adsl
    $flag = 1;
    foreach ( $arrDuna as $duna ) {
        if ( array_key_exists($duna['idduna'], $arrDunaSelected) ) {
            
            if ( $duna['x'] != $arrDunaSelected[$duna['idduna']]['x'] ||
                $duna['y'] != $arrDunaSelected[$duna['idduna']]['y'] ) {

                //grabando los xy en duna_adsl
                $result = $objDunaAdsl->updateXYDunaAdsl(
                    $conexion, 
                    $duna['idduna'], $arrDunaSelected[$duna['idduna']]['x'], $arrDunaSelected[$duna['idduna']]['y'], $idUsuario);
                if ( !$result ) {
                    $flag = 0;
                    break;
                }
            }
        }
    }

    $arrDataResult = ($flag) ? array('success'=>1,'msg'=>'Datos grabados correctamente.') : array('success'=>0,'msg'=>'Error al grabar los datos.');
    
    echo json_encode($arrDataResult);
    exit;
}

function cambiarEstado($arrIds, $estado)
{
    global $conexion;

    $objPosiblesClientes = new Data_PosibleCliente();
    $idUsuario = $_SESSION['USUARIO']->__get('_idUsuario');
    
    $flag = 1;
    if ( !empty($arrIds) ) {
        //update estado en posibles_clientes
        $result = $objPosiblesClientes->updateEstadoPosibleCliente($conexion, $arrIds, $estado, $idUsuario);
        if ( !$result ) {
            $flag = 0;
            break;
        }
    }

    $arrDataResult = ($flag) ? array('success'=>1,'msg'=>'Datos grabados correctamente.') : array('success'=>0,'msg'=>'Error al grabar los datos.');
    
    echo json_encode($arrDataResult);
    exit;
}

//listados anidados de FFTT
function ffttMdfporZonal( $zonal )
{
    global $conexion;
    
    $objTerminales = new Data_FfttTerminales();

    $idUsuario = $_SESSION['USUARIO']->__get('_idUsuario');
    $arrMdf = $objTerminales->getMdfByZonalAndIdUsuario($conexion, $zonal, $idUsuario);

    $arrData = array();
    foreach ($arrMdf as $mdf) {
        $arrData[] = array(
            'IDmdf'     => $mdf['mdf'],
            'Descmdf'    => $mdf['nombre']
        );
    }
    echo json_encode($arrData);
    exit;
}

function ffttCableArmarioBAK($idZonal, $idMdf, $idTipRed)
{
    global $conexion;
    
    $objTerminales = new Data_FfttTerminales();
    $arrArmarioCable = $objTerminales->getCableArmarioByZonalAndMdfAndTipoRed($conexion, $idZonal, $idMdf, $idTipRed);

    $arrData = array();
    foreach ($arrArmarioCable as $armarioCable) {
        $arrData[] = array(
            'IDcableArmario'     => $armarioCable['cablearmario'],
            'DescableArmario'    => $armarioCable['cablearmario']
        );
    }
    echo json_encode($arrData);
    exit;
}

function ffttCajaBAK($idZonal, $idMdf, $idTipRed, $idCableArmario)
{
    global $conexion;
    
    $objTerminales = new Data_FfttTerminales();
    $arrCaja = $objTerminales->getTerminales($conexion, $idZonal, $idMdf, $idTipRed, $idCableArmario);

    $arrData = array();
    foreach ($arrCaja as $caja) {
        $arrData[] = array(
            'mtgespktrm'    => $caja['mtgespktrm'],
            'Caja'          => $caja['caja']
        );
    }
    echo json_encode($arrData);
    exit;
}

function ffttCajaDetalleBAK($mtgespktrm)
{
    global $conexion;
    
    $objTerminales = new Data_FfttTerminales();
    $arrCaja = $objTerminales->getTerminalByMtgespktrm($conexion, $mtgespktrm);

    $arrData = array();
    if ( !empty($arrCaja) ) {
        $arrData = $arrCaja;
    }
    echo json_encode($arrData);
    exit;
}

function ffttCableArmario($idZonal, $idMdf, $idTipRed)
{
    global $conexion;

    $objTerminales = new Data_FfttTerminales();
    $arrArmarioCable = $objTerminales->getCableArmarioByZonalAndMdfAndTipoRed(
        $conexion, 
        $idZonal, 
        $idMdf, 
        $idTipRed
    );

    $arrData = array();
    foreach ($arrArmarioCable as $armarioCable) {
        $arrData[] = array(
            'idCableArmario' 	=> $armarioCable['cablearmario'],
            'desCableArmario'	=> $armarioCable['cablearmario']
        );
    }
    echo json_encode($arrData);
    exit;
}

function ffttCaja($idZonal, $idMdf, $idTipRed, $idCableArmario)
{
    global $conexion;

    $objTerminales = new Data_FfttTerminales();
    $arrCaja = $objTerminales->getTerminales($conexion, $idZonal, $idMdf, $idTipRed, $idCableArmario);

    $arrData = array();
    foreach ($arrCaja as $caja) {
        $arrData[] = array(
            'mtgespktrm'    => $caja['mtgespktrm'],
            'Caja'          => $caja['caja']
        );
    }
    echo json_encode($arrData);
    exit;
}

function ffttCajaDetalle($mtgespktrm)
{
    global $conexion;

    $objTerminales = new Data_FfttTerminales();
    $arrCaja = $objTerminales->getTerminalByMtgespktrm($conexion, $mtgespktrm);

    $arrData = array();
    if ( !empty($arrCaja) ) {
        $arrData = $arrCaja;
    }
    echo json_encode($arrData);
    exit;
}

function cargaInicial()
{
    global $conexion;

    $objDunaAdsl = new Data_DunaAdsl();
    
    //filtro para departamento, provincia, distrito por default
    $arrObjDepartamentos = $objDunaAdsl->listarDepartamentos($conexion);
    $arrObjProvincias = $objDunaAdsl->listarProvincias($conexion, 'LIMA');
    $arrObjDistritos = $objDunaAdsl->listarDistritos($conexion, 'LIMA', 'LIMA');
    
    $arrZonal = $_SESSION['USUARIO_ZONAL'];
    
    $arrZonales = array();
    foreach ($arrZonal as $objZonal) {
        $arrZonales[] = array(
            'abvZonal'     => $objZonal->__get('_abvZonal'),
            'descZonal'    => $objZonal->__get('_descZonal')
        );
    }

    $arrDptos = array();
    foreach ($arrObjDepartamentos as $objDunaAdsl) {
        $arrDptos[] = array(
            'coddpto' => $objDunaAdsl->__get('_departamento'),
            'nombre'  => $objDunaAdsl->__get('_departamento')
        );
    }
    
    $arrProvs = array();
    foreach ( $arrObjProvincias as $objDunaAdsl ) {
        $arrProvs[] = array(
            'codprov' => $objDunaAdsl->__get('_provincia'),
            'nombre'  => $objDunaAdsl->__get('_provincia')
        );
    }
    
    $arrDists = array();
    foreach ( $arrObjDistritos as $objDunaAdsl ) {
        $arrDists[] = array(
            'coddist' => $objDunaAdsl->__get('_distrito'),
            'nombre'  => $objDunaAdsl->__get('_distrito')
        );
    }
    

    $arrDataResult = array(
        'arrZonales'  => $arrZonales,
        'arrDptos'    => $arrDptos,
        'arrProvs'    => $arrProvs,
        'arrDists'    => $arrDists
    );

    echo json_encode($arrDataResult);
    exit;
}

function iniDefault()
{

    //template por default
    echo '<iframe id="ifrMain" name="ifrMain" src="modulos/maps/vistas/v_gescomp_duna_adsl.ifr.php" width="100%" height="480"></iframe>';
    
}



        
if (!isset($_REQUEST['action']))
    $_REQUEST['action'] = '';

switch ($_REQUEST['action']) {
    case 'listarProvincias':
        listarProvincias($_POST['departamento']);
        break;
    case 'listarDistritos':
        listarDistritos($_POST['departamento'], $_POST['provincia']);
        break;
    case 'listarDunaAdsl':
        listarDunaAdsl($_POST['departamento'], $_POST['provincia'], $_POST['distrito'], $_POST['datosXY']);
        break;
    case 'grabarDatos':
        grabarDatos($_GET['departamento'], $_GET['provincia'], $_GET['distrito'], $_POST['xy']);
        break;
    case 'ffttCableArmario':
        ffttCableArmario($_POST['idZonal'], $_POST['idMdf'], $_POST['idTipRed']);
        break;
    case 'ffttCaja':
        ffttCaja($_POST['idZonal'], $_POST['idMdf'], $_POST['idTipRed'], $_POST['idCableArmario']);
        break;
    case 'ffttCajaDetalle';
        ffttCajaDetalle($_POST['mtgespktrm']);
        break;
    case 'cargaInicial':
        cargaInicial();
        break;
    default:
        iniDefault();
        break;
}