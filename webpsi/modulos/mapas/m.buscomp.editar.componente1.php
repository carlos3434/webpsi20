<?php
require_once "../../config/web.config.php";
require_once APP_DIR . 'autoload.php';
require_once APP_DIR . 'session.php';


$array_capas = $_SESSION['USUARIO_CAPAS'];

$is_login = 0;
$dir_modulo = '../../';


if( isset($_REQUEST['action']) && $_REQUEST['action'] == 'searchItem' ) {

	$obj_capas = new data_ffttCapas();
	
	$item_exists = $obj_capas->get_edificio_by_item($conexion, $_POST['item'], $_POST['item_inicial']);
	
	if( empty( $item_exists ) ) {
		$result = array(
			'success' 	=> 1,
			'msg'		=> 'Ok'
		);
	}else {
		$result = array(
			'success' 	=> 0,
			'msg'		=> 'Item existe'
		);
	}
	
	echo json_encode($result);
	exit;

}
elseif( isset($_REQUEST['action']) && $_REQUEST['action'] == 'GrabaEdificio' ) {

	$obj_capas = new data_ffttCapas();
	
	//formateando fecha para insert a db
	$_POST['fecha_registro_proyecto'] 	 = ( $_POST['fecha_registro_proyecto'] != '' ) ? date('Y-m-d', strtotime($_POST['fecha_registro_proyecto'])) : '0000-00-00';
	$_POST['fecha_termino_construccion'] = ( $_POST['fecha_termino_construccion'] != '' ) ? date('Y-m-d', strtotime($_POST['fecha_termino_construccion'])) : '0000-00-00';
	$_POST['fecha_seguimiento'] 		 = ( $_POST['fecha_seguimiento'] != '' ) ? date('Y-m-d', strtotime($_POST['fecha_seguimiento'])) : '0000-00-00';

	$_POST['montante_fecha'] 			= ( $_POST['montante_fecha'] != '' ) ? date('Y-m-d', strtotime($_POST['montante_fecha'])) : '0000-00-00';
	$_POST['ducteria_interna_fecha'] 	= ( $_POST['ducteria_interna_fecha'] != '' ) ? date('Y-m-d', strtotime($_POST['ducteria_interna_fecha'])) : '0000-00-00';
	$_POST['punto_energia_fecha'] 		= ( $_POST['punto_energia_fecha'] != '' ) ? date('Y-m-d', strtotime($_POST['punto_energia_fecha'])) : '0000-00-00';
	
	$idusuario = $_SESSION['USUARIO']->__get('idusuario');
	
	$data_result = $obj_capas->__updateEdificioComponente($conexion, $_POST, $idusuario);

	$result = ($data_result) ? array('success'=>1,'msg'=>'Datos grabados correctamente.') : array('success'=>0,'msg'=>'Error al grabar los datos.');

	
	echo json_encode($result);
	exit;
	
}
elseif( isset($_REQUEST['action']) && $_REQUEST['action'] == 'GrabaCompetencia' ) {

	$obj_capas = new data_ffttCapas();
	
	$idusuario = $_SESSION['USUARIO']->__get('idusuario');
	
	$data_result = $obj_capas->__updateCompetenciaComponente($conexion, $_POST, $idusuario);

	$result = ($data_result) ? array('success'=>1,'msg'=>'Datos grabados correctamente.') : array('success'=>0,'msg'=>'Error al grabar los datos.');

	
	echo json_encode($result);
	exit;
	
}


if( $_REQUEST['capa'] == 'edi' ) {
	
	$obj_capas              = new data_ffttCapas();
	$obj_terminales         = new data_ffttTerminales();
        $obj_edificio_estado    = new Data_ffttEdificioEstado();
	
	$tipo_cchh_list 			= $obj_capas->get_descriptor_by_grupo($conexion, 1);
	$tipo_proyecto_list 		= $obj_capas->get_descriptor_by_grupo($conexion, 2);
	$tipo_infraestructura_list 	= $obj_capas->get_descriptor_by_grupo($conexion, 3);
	$tipo_via_list 				= $obj_capas->get_descriptor_by_grupo($conexion, 4);
	
	$mdfs_array = $obj_terminales->get_mdfs_by_xy($conexion, $_REQUEST['x'], $_REQUEST['y'], 10, 5);
	
	$edificio = $obj_capas->get_edificio_by_id($conexion, $_REQUEST['idedificio']);

	if( empty($edificio) ) {
		echo '<p>Edificio no existe</p>';
		exit;
	}
	
	
	$cod_dep 	= substr($edificio[0]['distrito'],0,2);
	$cod_prov	= substr($edificio[0]['distrito'],2,2);
	$dpto_list  = $obj_capas->get_departamentos($conexion);
	$prov_list  = $obj_capas->get_provincias_by_dpto($conexion, $cod_dep);
	$dist_list  = $obj_capas->get_distritos_by_prov($conexion, $cod_dep, $cod_prov);
	
        /*
	$estado_arr = array(
		'I' => 'Iniciado',
		'P' => 'En proceso',
		'T' => 'Terminado'
	);
        */
        
        //estados de los edificios
        $estado_arr = array();
        $arr_obj_edificio_estado = $obj_edificio_estado->obtenerEdificioEstado($conexion);
        if( !empty($arr_obj_edificio_estado) ) {
            foreach( $arr_obj_edificio_estado as $obj_edificio_estado ) {
                $estado_arr[$obj_edificio_estado->__get('codedificio_estado')] = $obj_edificio_estado->__get('des_estado');
            }
        }
	
	
	$etapa_arr = array(
		'A' => 'A',
		'B' => 'B',
		'C' => 'C',
		'D' => 'D',
		'E' => 'E',
		'F' => 'F',
		'G' => 'G',
		'H' => 'H',
		'I' => 'I',
		'J' => 'J',
		'K' => 'K',
		'L' => 'L',
		'M' => 'M',
		'N' => 'N',
		'O' => 'O',
		'P' => 'P',
		'Q' => 'Q',
		'R' => 'R',
		'S' => 'S',
		'T' => 'T',
		'U' => 'U',
		'V' => 'V',
		'W' => 'W',
		'X' => 'X',
		'Y' => 'Y',
		'Z' => 'Z'
	);
	
	$meses_arr = array(
		'01' => 'Enero',
		'02' => 'Febrero',
		'03' => 'Marzo',
		'04' => 'Abril',
		'05' => 'Mayo',
		'06' => 'Junio',
		'07' => 'Julio',
		'08' => 'Agosto',
		'09' => 'Setiembre',
		'10' => 'Octubre',
		'11' => 'Noviembre',
		'12' => 'Diciembre'
	);

	$ano_actual = date('Y');
	$mes_actual = date('m');
	$dia_actual = date('d');
	
	include 'vistas/m.v_buscomp.editar.componente.php'; 

}
elseif( $_REQUEST['capa'] == 'com' ) {

	$obj_capas = new data_ffttCapas();
	$obj_terminales = new data_ffttTerminales();

	//opciones multiple -> competencia
	$competencia_list 			= $obj_capas->get_descriptor_by_grupo($conexion, 5);
	$elemento_encontrado_list 	= $obj_capas->get_descriptor_by_grupo($conexion, 6);
	
	$mdfs_array = $obj_terminales->get_mdfs_by_xy($conexion, $_REQUEST['x'], $_REQUEST['y'], 10, 5);
	
	$competencia = $obj_capas->get_competencia_by_id($conexion, $_REQUEST['idcompetencia']);


	if( empty($competencia) ) {
		echo '<p>Competencia no existe</p>';
		exit;
	}

	include 'vistas/m.v_buscomp.editar.componente.php'; 

}







