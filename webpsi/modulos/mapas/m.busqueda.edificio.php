<?php

/**
 * @package     /modulos/maps
 * @name        m.busqueda.edificio.php
 * @category    Controller
 * @author      Wilder Sandoval Huamani <wsandoval@gmd.com.pe>
 * @copyright   2011 Telefonica del Peru
 * @version     1.0 - 2011/10/25
 */

require_once "../../config/web.config.php";
include_once APP_DIR . 'autoload.php';
include_once APP_DIR . 'session.php';

include_once APP_DIR . 'modulos/maps/util/functions.maps.php';

$dirModulo = '../../';


function vistaInicial()
{
    try {
        global $conexion, $dirModulo;

        include 'vistas/m.v_busqueda.edificio.php';

    } catch(PDOException $e) {
        
    }
}

function buscarEdificio($campoFiltro='', $campoValor='')
{
    try {
        global $conexion, $dirModulo;

        $arrEdificos = array();
        if ( $campoFiltro != '' && $campoValor != '' ) {
            
            $objEdificio = new Data_FfttEdificio();

            $arrFiltro = array();

            //listado de empresas que tiene asignado
            $arrDataEmpresa = array();
            if (!empty($_SESSION['USUARIO_EMPRESA_DETALLE'])) {
                foreach ($_SESSION['USUARIO_EMPRESA_DETALLE'] as $objEmpresa) {
                    $arrDataEmpresa[] = $objEmpresa->__get('_idEmpresa');
                }
            }
            $arrFiltro['empresa'] = $arrDataEmpresa;
            

            //obtengo las regiones a partir de las zonales que tiene asignado
            //el usuario
            $arrRegion = listarRegionesPorZonalUsuario();
        
            $arrDataRegion = array();
            if (!empty($arrRegion)) {
                foreach ($arrRegion as $region) {
                    $arrDataRegion[] = $region['idregion'];
                }
            }
            $arrFiltro['region'] = $arrDataRegion;
            
            
            //zonales asignadas al usuario
            $arrData = array();
            if (!empty($_SESSION['USUARIO_ZONAL'])) {
                foreach ($_SESSION['USUARIO_ZONAL'] as $objZonal) {
                    $arrData[] = $objZonal->__get('_abvZonal');
                }
            }
            $arrFiltro['zonal'] = $arrData;

            
            //listado de mdfs
            /*
            $arrMdf = array();
            if (!empty($_SESSION['USUARIO_MDF'])) {
                foreach ($_SESSION['USUARIO_MDF'] as $abvMdf) {
                    $arrMdf[]['mdf'] = $abvMdf;
                }
            }
            $arrData = array();
            if (!empty($arrMdf)) {
                foreach ($arrMdf as $abvMdf) {
                    $arrData[] = $abvMdf['mdf'];
                }
            }
            $arrFiltro['mdf'] = $arrData;
            */
            
            
            //Item/cod proy web de edificio
            $arrFiltro['campo_filtro'] = $campoFiltro;
            $arrFiltro['campo_valor'] = $campoValor;
            

            $pagina = 1;
            $arrObjEdificios = $objEdificio->listarEdificios(
                $conexion, $pagina, '', $arrFiltro
            );


            foreach ( $arrObjEdificios as $objEdificio) {
                $arrEdificos[] = array(
                    'idedificio'                => $objEdificio->__get('_idEdificio'),
                    'item'                      => $objEdificio->__get('_item'),
                    'fecha_registro_proyecto'   => $objEdificio->__get('_fechaRegistroProyecto'),
                    'zonal'                     => $objEdificio->__get('_zonal'),
                    'ura'                       => $objEdificio->__get('_ura'),
                    'sector'                    => $objEdificio->__get('_sector'),
                    'mza_tdp'                   => $objEdificio->__get('_mzaTdp'),
                    'direccion_obra'            => $objEdificio->__get('_direccionObra'),
                    'numero'                    => $objEdificio->__get('_numero'),
                    'tipo_cchh'                 => $objEdificio->__get('_tipoCchh'),
                    'cchh'                      => $objEdificio->__get('_cchh'),
                    'estado'                    => $objEdificio->__get('_estado'),
                    'des_estado'                => $objEdificio->__get('_desEstado'),
                    'x'                         => $objEdificio->__get('_x'),
                    'y'                         => $objEdificio->__get('_y')
                );
            }
        }

        $arrDataResult = array(
            'edificios'   => $arrEdificos
        );

        echo json_encode($arrDataResult);
        exit;

    } catch(PDOException $e) {
        
        echo "<br><br><br><span style=\"font-weight:bold;\">Se ha producido un 
            error en el Sistema</span><br>
            Vuelva en otro momento. Gracias.";
        exit;
    }
}


if (!isset($_GET['cmd']))
    $_GET['cmd'] = '';

switch ($_GET['cmd']) {
    case 'buscarEdificio':
        buscarEdificio($_POST['campo_filtro'], $_POST['campo_valor']);
        break;
    default:
        vistaInicial();
        break;
}