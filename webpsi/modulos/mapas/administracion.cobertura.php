<?php

/**
 * [Web Unificada]
 * Controlador de la Administracion de Cobertura Movil (2G/3G).
 * @package    /modulos/maps/
 * @name       administracion.cobertura.php
 * @category   Controller
 * @author     Fernando Esteban Valerio <festeban@gmd.com.pe>
 * @copyright  GMD S.A.
 * @version    1.0
 */

class AdministracionCobertura extends Data_CoberturaMovilEdi
{
    /**
     * Directorio
     * @access protected
     * @var string
     */
    protected $_ruta;
    
    /**
     * Numero de pagina
     * @access protected
     * @var int
     */
    protected $_pagina;
    
    /**
     * @name  __construct
     * Metodo constructor de la clase
     */
    public function __construct()
    {
        parent::__construct();
        $this->_pagina = 1;
        $this->_ruta = 'vistas/administracion/';
    }
    
    /**
     * @author Fernando Esteban Valerio <festeban@gmd.com.pe>
     * @name getRuta
     * Obtener el valor del atributo "_ruta".
     * @return string
     */
    public function getRuta()
    {
        return $this->_ruta;
    }
    
    /**
     * @author Fernando Esteban Valerio <festeban@gmd.com.pe>
     * @name setRuta
     * Asignar un valor al atributo "_ruta".
     * @param string $ruta Ruta donde se ubican las vistas del mantenimiento de
     * segmento.
     */
    public function setRuta($ruta)
    {
        $this->_ruta = $ruta;
    }
    
    /**
     * @author Fernando Esteban Valerio <festeban@gmd.com.pe>
     * @name getPagina
     * Obtener el valor del atributo "_pagina".
     * @return string
     */
    public function getPagina()
    {
        return $this->_pagina;
    }
    
    /**
     * @author Fernando Esteban Valerio <festeban@gmd.com.pe>
     * @name setRuta
     * Asignar un valor al atributo "_pagina".
     * @param string $pagina Numero de pagina.
     */
    public function setPagina($pagina)
    {
        $this->_pagina = $pagina;
    }
    
    
    /**
     * @author Fernando Esteban Valerio <festeban@gmd.com.pe>
     * @name vistaAgregar
     * Mostrar el formulario de registro de cobertura movil (2G o 3G).
     * @param string $tipo Tipo de cobertura movil (2G o 3G).
     * @return void
     */
    public function vistaAgregar($tipo)
    {
        $ruta = '';
        if ($tipo == '2G') {
            $ruta = $this->getRuta() . 'cobertura_2g/v_cobertura_add.phtml';
        }
        if ($tipo == '3G') {
            $ruta = $this->getRuta() . 'cobertura_3g/v_cobertura_add.phtml';
        }
        include $ruta;
    }
        
    /**
     * @author Fernando Esteban Valerio <festeban@gmd.com.pe>
     * @name vistaEditar
     * Mostrar el formulario de edicion de cobertura movil (2G o 3G).
     * @global conexion $conexion Objeto de conexion a la BD creada con PDO.
     * @param string $tipo Tipo de cobertura movil (2G o 3G).
     * @param int $id Id de la cobertura movil (2G o 3G).
     * @return void
     */
    public function vistaEditar($tipo, $id)
    {
        global $conexion;
    
        $vista = '';
        if (is_numeric($id)) {
            $objeto = $this->obtener($tipo, $id);
            if ($objeto->getIdCoberturaMovil()) {
                if ($tipo == '2G') {
                    $vista = $this->getRuta()
                           . 'cobertura_2g/v_cobertura_edit.phtml';
                }
                if ($tipo == '3G') {
                    $vista = $this->getRuta()
                           . 'cobertura_3g/v_cobertura_edit.phtml';
                }
            } else {
                $vista = $this->getRuta() . 'v_error.phtml';
            }
        } else {
            $vista .= $this->getRuta() . 'v_error.phtml';
        }
        include $vista;
    }
    
    /**
     * @author Fernando Esteban Valerio <festeban@gmd.com.pe>
     * @name guardar
     * Registrar la informacion de la cobertura movil (2G/3G).
     * @global conexion $conexion Objeto de conexion a la BD creada con PDO.
     * $param string $tipo Tipo de cobertura movil (2G o 3G).
     * @param string $nombre Nombre de la cobertura movil (2G o 3G).
     * @return string
     */
    public function guardar($tipo, $nombre)
    {
        global $conexion;
    
        $arrParametros = array();
        $arrParametros['id'] = NULL;
        $arrParametros['desc'] = $nombre;
        $arrParametros['flag'] = 1;
        $arrParametros['tipo'] = $tipo;
    
        $id = 0;
        $objeto = $this->obtener($tipo, $id, $nombre);
        if ($objeto->getIdCoberturaMovil()) {
            echo 'existe';
        } else {
            $id = $this->registrar($arrParametros);
            if ($id) {
                echo 'ok';
            } else {
                echo 'error';
            }
        }
    }
    
    /**
     * @author Fernando Esteban Valerio <festeban@gmd.com.pe>
     * @name actualizar
     * Actualizar la informacion de la cobertura movil (2G/3G).
     * @global conexion $conexion Objeto de conexion a la BD creada con PDO.
     * @param string $tipo Tipo de la cobertura movil (2G o 3G).
     * @param int $id Id de la cobertura movil (2G o 3G).
     * @param string $nombre Nombre de la cobertura movil (2G o 3G).
     * @param int $flag Estado de la cobertura movil (2G o 3G).
     * @return string
     */
    public function actualizar($tipo, $id, $nombre, $flag=1)
    {
        global $conexion;
    
        $arrParametros = array();
        $arrParametros['id'] = $id;
        $arrParametros['desc'] = $nombre;
        $arrParametros['flag'] = $flag;
        $arrParametros['tipo'] = $tipo;
        
        $numElementos = $this->buscar($tipo, $id, $nombre);
        if ($numElementos) {
            echo 'existe';
        } else {
            $resultado = $this->registrar($arrParametros);
            if ($resultado) {
                echo 'ok';
            } else {
                echo 'error';
            }
        }
    }
    
    /**
     * @author Fernando Esteban Valerio <festeban@gmd.com.pe>
     * @name cambiarPagina
     * Mostrar las coberturas moviles ubicados en la pagina seleccionada.
     * @global conexion $conexion Objeto de conexion a la BD creada con PDO.
     * @param string $tipo Tipo de cobertura movil (2G o 3G).
     * @param int $pagina Numero de pagina.
     * @param int $total Total de registros.
     * @param string $nombre Valor de busqueda.
     * @return void
     */
    public function cambiarPagina($tipo, $pagina, $total, $nombre)
    {
        global $conexion;
                    
        $arrCobertura = $this->filtrar($tipo, $pagina, $nombre);
        $numPaginas = ceil($total/TAM_PAG_LISTADO_MAP);
        $vista = '';
        if ($tipo == '2G') {
            $vista = $this->getRuta()
                   . 'cobertura_2g/v_cobertura_paginado.phtml';
        }
        if ($tipo == '3G') {
            $vista = $this->getRuta()
                   . 'cobertura_3g/v_cobertura_paginado.phtml';
        }
        include $vista;
        $cadena = '<script type="text/javascript">'
                . '$(document).ready(function(){'
                . '$("#cont_num_resultados_head").empty();'
                . '$("#cont_num_resultados_head").html(' . $total . ');'
                . '$("#tl_registros").val("' . $total . '");'
                . '$("#pag_actual").val("' . $pagina . '");'
                . '});'
                . '</script>';
        echo $cadena;
    }
    
    /**
     * @author Fernando Esteban Valerio <festeban@gmd.com.pe>
     * @name viewMantCobertura
     * Mostrar modulo de administracion de cobertura movil (2G o 3G)
     * @global conexion $conexion Objeto de conexion a la BD creada con PDO
     * @global int $pagina Numero de pagina
     * @param string $tipo Tipo de cobertura movil (2G o 3G)
     * @return void
     */
    public function vistaMantenimiento($opcion=0, $tipo='', $nombre='')
    {
        global $conexion;
    
        $cadena = $vista = '';
        $pagina = $this->getPagina();
        $total = $this->contar($tipo, $nombre);
        $arrCobertura = $this->filtrar($tipo, $pagina, $nombre);
        $numPaginas = ceil($total/TAM_PAG_LISTADO_MAP);
        if ($tipo == '2G') {
            $vista = $this->getRuta()
                   . 'cobertura_2g/v_cobertura_lista.phtml';
        }
        if ($tipo == '3G') {
            $vista = $this->getRuta()
                   . 'cobertura_3g/v_cobertura_lista.phtml';
        }
        if ($numPaginas > 1) {
            $cadena = '<script type="text/javascript">'
                    . '$(document).ready(function(){'
                    . '$("#cont_num_resultados_head").empty();'
                    . '$("#cont_num_resultados_head").html(' . $total . ');'
                    . '$("#pag_actual").val("' . $pagina . '");'
                    . '$("#tl_registros").val("' . $total . '");'
                    . '$("#paginacion").show();'
                    . '});'
                    . '</script>'
                    . '<script type="text/javascript">'
                    . '$(function() {'
                    . '$("#paginacion").paginate({'
                    . 'count : ' . $numPaginas . ','
                    . 'start : 1,'
                    . 'display : 5,'
                    . 'border : true,'
                    . 'border_color : "#FFFFFF",'
                    . 'text_color : "#FFFFFF",'
                    . 'background_color : "#0080AF",'
                    . 'border_hover_color : "#CCCCCC",'
                    . 'text_hover_color : "#000000",'
                    . 'background_hover_color : "#FFFFFF",'
                    . 'images : false,'
                    . 'mouse : "press",'
                    . 'onChange : function(page){'
                    . 'loader("start");'
                    . '$("#pag_actual").attr("value", page);'
                    . 'var tl = $("#tl_registros").attr("value");'
                    . 'var filtro = $("#desc").attr("value");'
                    . '$.post("modulos/maps/administracion.maps.php?'
                    . 'cmd=cambiarPaginaCobertura",'
                    . '{ tipo: "' . $tipo . '", pagina: page, total: tl, '
                    . 'filtro: filtro }, '
                    . 'function(data){'
                    . '$("#tb_resultado").empty();'
                    . '$("#tb_resultado").append(data);'
                    . 'loader("end");'
                    . '});'
                    . '}'
                    . '});'
                    . '});'
                    . '</script>';
        } else {
            $cadena = '<script type="text/javascript">'
                    . '$(document).ready(function(){';
            if ($total > 0) {
                $cadena .= '$("#tb_resultado").show();';
            } else {
                $cadena .= '$("#tb_resultado").hide();';
            }
            $cadena .= '$("#cont_num_resultados_head").empty();'
                     . '$("#cont_num_resultados_head").html(' . $total . ');'
                     . '$("#tl_registros").val(' . $total . ');'
                     . '$("#pag_actual").val("' . $pagina . '");'
                     . '$("#paginacion").hide();'
                     . '});'
                     . '</script>';
        }
        if ($total > 0) {
            if (!$opcion) {
                if ($tipo == '2G') {
                    $vista = $this->getRuta()
                           . 'cobertura_2g/v_cobertura_lista.phtml';
                }
                if ($tipo == '3G') {
                    $vista = $this->getRuta()
                           . 'cobertura_3g/v_cobertura_lista.phtml';
                }
            } else {
                if ($tipo == '2G') {
                    $vista = $this->getRuta()
                           . 'cobertura_2g/v_cobertura_paginado.phtml';
                }
                if ($tipo == '3G') {
                    $vista = $this->getRuta()
                           . 'cobertura_3g/v_cobertura_paginado.phtml';
                }
            }
        }
        include $vista;
        echo $cadena;
    }
    
}