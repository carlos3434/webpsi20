<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 12/11/14
 * Time: 04:44 PM
 */
//ini_set('display_errors', 'On');
//error_reporting(E_ALL);
$PATH = /*$_SERVER['DOCUMENT_ROOT'] . */"C:/server/http/webpsi20/";
require_once 'C:/server/http/webpsi20/webpsi/clases/class.Conexion.php';
require_once 'C:/server/http/webpsi20/webpsi/modulos/officetrack/clases/class.Empresa.php';
require_once 'C:/server/http/webpsi20/webpsi/modulos/officetrack/clases/class.Celula.php';
require_once 'C:/server/http/webpsi20/webpsi/modulos/officetrack/clases/class.Location.php';
require_once 'C:/server/http/webpsi20/webpsi/modulos/historico/clases/class.TecnicosCriticos.php';
require_once 'C:/server/http/webpsi20/webpsi/modulos/officetrack/clases/class.Tareas.php';

$Conexion           = new Conexion();
$Empresa            = new Empresa();
$Celula             = new Celula();
$tecnico            = new TecnicosCriticos();
$Location           = new Location();
$Tareas             = new Tareas();
//CONEXCION A BD HEREDAD POR OTRAS CLASES
$db = $Conexion->conectarPDO();

//REQUEST
$accion = $_REQUEST["accion"];
$idempresa = $_REQUEST["idempresa"];
$idcelula = $_REQUEST["idcelula"];
$carnet = $_REQUEST["carnet"];

if($accion == "getContratas"){

    $empArray = $Empresa->getEmpresa($db, "", 1);
    print json_encode($empArray);
    exit();
}elseif($accion == "getCelulas"){

    $array = $Celula->getCelulaEmp($db, $idempresa);
    print json_encode($array);
    exit();
}elseif($accion == "getAsistencia"){


    $array = $Celula->getCelulaEmp($db, $idempresa);
    print json_encode($array);
    exit();
}elseif($accion == "mostrarAsistencia"){


    $tabla = $tecnico->AsistenciaTecnicosMovil(  $idempresa , $idcelula );
    print $tabla;
    exit();
}elseif($accion == "mostrarTareasPendientes"){

    $fini=date("Y-m-d");
    $nuevafecha = strtotime ( '+1 day' , strtotime ( $fini ) ) ;
    $ffin = date ( 'Y-m-d' , $nuevafecha );
    $estados ="1,8,9,10,20";

    $agenda =     $Location->getAgendasTodos( $db, "", "", $fini, $ffin, $carnet, $estados);
    print count($agenda["data"]);
    exit();
}elseif($accion == "mostrarTareasCerradas"){

    $fini=date("Y-m-d");
    $nuevafecha = strtotime ( '+1 day' , strtotime ( $fini ) ) ;
    $ffin = date ( 'Y-m-d' , $nuevafecha );

    $result = $Tareas->getTareas($fini , $ffin , $idempresa , $idcelula , $pasos = array("0003-Cierre") , $carnet);
    print json_encode($result["data"]);
    exit();
}