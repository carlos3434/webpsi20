



$(document).ready(function() {
    if(agendas.mayor != null){
        initialize();
        calcRoute();
        //google.maps.event.addDomListener(window, 'load', initialize);
    }else{
        mapaLimpio()
        alert("No hay tareas Agendadas")
    }

    ubicacionTecnico()


    $(".navbar-brand").click(function(){
        window.location.reload()
    })
});

var markers = [];
var map;

var directionsDisplay;
var directionsService = new google.maps.DirectionsService();


function initialize() {
    directionsDisplay = new google.maps.DirectionsRenderer();
    var lima = new google.maps.LatLng(agendas.mayor.y , agendas.mayor.x);

    var mapOptions = {
        zoom: 6,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        center: lima
    }
    map = new google.maps.Map(document.getElementById("map-canvas"), mapOptions);
    directionsDisplay.setMap(map);



}

function mapaLimpio(){
    var myLatlng = new google.maps.LatLng(-12.046374, -77.0427934);
    map = new google.maps.Map(document.getElementById('map-canvas'), {
        zoom:12,
        center: myLatlng,
        mapTypeId: google.maps.MapTypeId.ROADMAP
    });

}

function calcRoute() {

    var waypts = [];
    $.each(agendas.entre, function(){

        var point = new google.maps.LatLng(this.y , this.x);
        waypts.push({
            location:point,
            stopover:true
        });

    })

    var point_mayor = new google.maps.LatLng(agendas.mayor.y , agendas.mayor.x);
    var point_menor = new google.maps.LatLng(agendas.menor.y , agendas.menor.x);
    var request = {
        origin: point_menor,
        destination: point_mayor,
        waypoints: waypts,
        optimizeWaypoints: true,
        travelMode: google.maps.TravelMode.DRIVING
    };

    directionsService.route(request, function(response, status) {
        if (status == google.maps.DirectionsStatus.OK) {
            directionsDisplay.setDirections(response);
            showSteps(response)
        }
    });
}

function ubicacionTecnico(){

    if( current_geo_tec.length != 0 ){
        var x = current_geo_tec.X.replace(",",".");
        var y = current_geo_tec.Y.replace(",",".");

        var p = new google.maps.LatLng(Number(y),Number(x));
        //var p = new google.maps.LatLng(-12.046374, -77.0427934);

        var marker = new google.maps.Marker({
            position: p,
            map: map,
            icon: "../../officetrack/images/icons/" + "tec_0e8499.png",
            zIndex: 999
        });

    }

}

var markerArray = []
function showSteps(directionResult) {

    agregarMarker(agendas.mayor)
    agregarMarker(agendas.menor)
    $.each(agendas.entre, function(){
        agregarMarker(this)
    })



}

function agregarMarker(data){

    if(data.tipoactu != undefined){
        var info = infoContent(data);
        var infowindow = new google.maps.InfoWindow({    content: info     });
        var p = new google.maps.LatLng(data.y , data.x);
        var marker = new google.maps.Marker({
            position: p,
            map: map,
            icon: "../../officetrack/images/icons/" + "cal_0e8499.png",
            zIndex: 999
        });
        google.maps.event.addListener(marker, 'click', function() {
            infowindow.open(map,marker);
        });
    }


}

function infoContent(ele,tipo_actu){

    var info =   "<div class=\"infow\" style=\"width:300px; height:200px; overflow: auto\">" +
        "<div class=\"detalle_actu\">" +
        ele.tipoactu + "<br>" +
        ele.nombre_cliente_critico + "<br>" +
        ele.fecha_agenda + " / " +
        ele.horario + "<br>" +
        ele.observacion + "<br>" +
        ele.direccion + "<br>" +
        ele.codactu + "<br>" +
        ele.fftt + "<br>" +
        ele.codcli + "<br>" +
        ele.mdf + "<br>" +
        ele.id_atc + "<br>" +
        ele.lejano + "<br>" +
        ele.carnet_critico + "<br>" +
        ele.paquete + "<br>" +
        ele.quiebre + "<br>" +
        ele.tecnico + "<br>" +
   //+      "<div><a href=\"javascript:void(0)\" onclick=\"gestionActu('" + ele.id  + "', '0', '" + ele.tipo_actividad + "')\">Gestionar <img src=\"../historico/img/gestionar.png\" style=\"vertical-align: middle\"></a>&nbsp;" +
   // "<div class=\"detalle_gestion\"></div>"+
        "</div>" ;
    return info;
}

