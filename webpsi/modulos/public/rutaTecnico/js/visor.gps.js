

var markers = [];
var markerArray = [];

//Poligonos
var polygonObject;
var polygonCoords = [];
var polygonArray = [];
//Address
var addressArray = [];
var addressMarkerArray = [];
//Objeto geocoder, libreria Google Maps
var geocoder;
//Objeto tipo infowindow
var infowindow;
//Cadena de datos a enviar via ajax
var data_content;
//Elementos seleccionados para mostrar en el mapa
var selectedItem;
//Items con check
var itemsChecked;
var itemsCheckedOpen;
//Item envio (concatenado)
var itemSet;
//Enviar peticion
var sendRequest;
//Elementos a seleccionar
var geoItemArray = new Array();
//Objetos del mapa
var mapObjects = {};
//Grupo de capas
var layerGroup = {};
//Variables globales estilos
var iconSelected = "";
var colorSelected = "";
var colorLineSelected = "";

var polygonOpacity = "";
var polygonLineWidth = "";

//z-index
var zIndexItem = 0;

//Carga y busqueda de direcciones
var myInterval;
var dynamicInc = 0;

//Capas por files KML o KMZ
var fileLayers = new Array();

//Google base icon
var googleMarktxt = "http://chart.apis.google.com/chart"
    + "?chst=d_map_pin_letter&chld=";

//Capas intermitentes
var flickLayer = new Array();

//PolyLine
var polyLineCoords = [];



var usuario_movil_x;
var usuario_movil_y;

// VARIABLE PARA VALIDAR EL NAMESPACE  PARA UN SUAURIO MOVIL
window.usuario_movil = 1;

$().ready(function(){
$("#tools").hide();
$("#tools_tmp").hide();
$("#map-tec").hide();
$("#wrap-proyectos").hide();

    getLocation();  // CONSIGUE LA UBICACION DEL MOVIL QUE ESTA VIENDO EL APLICATIVO


    $("#tools fieldset").append("<div class=\"toolBox\"> <label>&nbsp;</label> " +
    " <div> <span class=\"filtro-cerrar\">x cerrar</span> </div> </div>");

    $("#wrap-proyectos .left").append("<label>&nbsp;</label> " +
    "  <span class=\"filtro-cerrar\">x cerrar</span> ");

    $(".navbar-brand.filtros").click(function(){

        ocultarTodos("tools")

        if($("#tools").hasClass("block-active")){
            $("#tools").hide("slow").removeClass("block-active");
        }else{
            $("#tools").show().addClass("block-active");
        }
    });

    $(".navbar-brand.detalles").click(function(){

        ocultarTodos("map-tec")

        if($("#map-tec").hasClass("block-active")){
            $("#map-tec").hide("slow").removeClass("block-active");
        }else{
            $("#map-tec").show().addClass("block-active");
        }

    });

    $(".navbar-brand.proyectos").click(function(){

        ocultarTodos("wrap-proyectos");

        if($("#wrap-proyectos").hasClass("block-active")){
            $("#wrap-proyectos").hide("slow").removeClass("block-active");
        }else{
            $("#wrap-proyectos").show().addClass("block-active");
        }

    });

    $("#buscar_agenda,.filtro-cerrar").click(function(){
        getLocation()
        $(".block-active").hide("slow").removeClass("block-active");
    });

    //OPCION DEL MENU PARA ACTUALIZAR LA PAGINA
    $(".reload").click(function(){
       window.location.reload();
    });


    //CARGA DE PROYECTOS
    $("#proyectos").change(function(){
        showProjectLayer()
    });

});




function ocultarTodos(excepto){
    var activo = 0;
    if($("#"+excepto).hasClass("block-active")){
        activo = 1;
    }

    $(".block-active").hide().removeClass("block-active");
    if(activo == 1){ //lo mantendra activo si ya estaba activo
        $("#"+excepto).show().addClass("block-active");
    }

}

function getLocation() {

    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(showPosition);
    } else {
        // alert("geolocalizacion no es soportado por su browser")
    }
}
function showPosition(position) {

    usuario_movil_y = position.coords.latitude;
    usuario_movil_x =  position.coords.longitude;

    var info = "Usted se encuentra aquí";
    var infowindow = new google.maps.InfoWindow({    content: info     });
    var p = new google.maps.LatLng(usuario_movil_y , usuario_movil_x);
    var marker = new google.maps.Marker({
        position: p,
        map: map,
        //icon: "images/icons/" + "tec_ffffff.png",
        zIndex: 1
    });
    //infowindow.open(map,marker);
    google.maps.event.addListener(marker, 'click', function() {
        infowindow.open(map,marker);
    });


}

directionsDisplay = new google.maps.DirectionsRenderer();

function GoToTecnico(cordx, cordy){



    directionsDisplay.setMap(map);
    var waypts = [];

    var origen = new google.maps.LatLng(usuario_movil_y , usuario_movil_x);
    var destino = new google.maps.LatLng(cordy , cordx);
    var request = {
        origin: origen,
        destination: destino,
        waypoints: waypts,
        optimizeWaypoints: true,
        travelMode: google.maps.TravelMode.DRIVING
    };

    directionsService.route(request, function(response, status) {
        if (status == google.maps.DirectionsStatus.OK) {
            directionsDisplay.setDirections(response);
        }
    });


}

function ocultarGoToTecnico(){
    directionsDisplay.setMap(null)
}


function showProjectLayer(){
    //Limpiar bloque de capas del proyecto
    $("#currentProject").html("");

    var idProject = $("#proyectos").val();

    var htmlProjectHeader = "<div style=\"display: table;\">"
        + "<div style=\"display: table-row;\">"
        + "<div style=\"display: table-cell; text-align: center\">Capa</div>"

        + "<div style=\"display: table-cell; text-align: center\">Show</div>"
        + "<div style=\"display: table-cell; text-align: center\">Flick</div>"
        + "</div>";
    var htmlProjectBody = "";

    var dataObjects = "action=getProjectLayers&id=" + idProject;
    $.ajax({
        type: "POST",
        url: "../georeferencia/geo.main.php",
        data: dataObjects,
        dataType: 'json',
        success: function(datos) {
            //Id de la capa
            var keyLayerId;


            $.each(datos, function(key, value) {
                //Layer ID
                keyLayerId = value.layer_id
                + "___"
                + value.tipo
                + "___"
                + value.origen;
                //Estilos de la capa
                //layerStyle[keyLayerId] = value.estilo;

                htmlProjectBody += "<div style=\"display: table-row;\">"
                + "<div id=\"" + keyLayerId + "\" style=\"display: table-cell;\" class=\"rowEditLayer\">"
                + "<a href=\"edit\" title=\"" + keyLayerId + "\" class=\"onProjectLayer\">"
                + value.layer
                + "</a></div>"

                + "<div style=\"display: table-cell; text-align: center\">"
                + "<input type=\"checkbox\" value=\"" + keyLayerId + "\" class=\"hideShowLayer\">"
                + "</div>"
                + "<div style=\"display: table-cell; text-align: center\">"
                + "<input type=\"checkbox\" value=\"" + keyLayerId + "\" class=\"flickLayer\">"
                + "</div>"
                + "</div>";
            });
            htmlProjectBody += "</div>";
            $("#currentProject").append(htmlProjectHeader + htmlProjectBody);



            //Ocultar o mostrar capas de forma individual
            $(".hideShowLayer").click(function() {
                var keyLayer = $(this).val();
                if ($(this).prop("checked") === true) {
                    //Mostrar objetos
                    showMapLayer(keyLayer);
                } else if ($(this).prop("checked") === false) {
                    //Ocultar objetos
                    hideMapLayer(keyLayer);
                }
            });

            //Test: flick Layer :)
            $(".flickLayer").click(function() {
                var keyLayer = $(this).val();
                var myVar;

                /**
                 * Calcula segundos actuales
                 * Si es par muestra el layer
                 * Si es impar oculta el layer
                 * @returns invoca funcion
                 */
                function myTimer()
                {
                    var d = new Date();
                    var n = d.getSeconds();
                    if (n % 2 === 0) {
                        showMapLayer(keyLayer);
                    } else {
                        hideMapLayer(keyLayer);
                    }
                }

                if ($(this).prop("checked") === true) {
                    //Mostrar objetos
                    myVar = setInterval(function() {
                        myTimer();
                    }, 1000);
                    flickLayer[keyLayer] = myVar;
                } else if ($(this).prop("checked") === false) {
                    //Ocultar objetos
                    clearInterval(flickLayer[keyLayer]);
                    showMapLayer(keyLayer);
                    delete flickLayer[keyLayer];
                }
            });

            /**
             * Al hacer click en una de las capas del proyecto
             * muestra las opciones de edicion de acuerdo al tipo:
             * Poligono o marcador
             */
            $(".onProjectLayer").click(function(event) {
                //debugger
                event.preventDefault();
                var action = $(this).attr("href");
                var keyLayer = $(this).attr("title");

                $("#keyEdit").val(keyLayer);

                //Todas las capas con fondo blanco
                $(".rowEditLayer").css("background-color", "#FFFFFF");

                //Capa seleccionada con nuevo estilo
                $(".rowEditLayer[id='" + keyLayer + "']").css("background-color", "#CCFF66");

                //Loading
                $("body").addClass("loading");

                if (action === "drop") {

                    for (key in mapObjects) {
                        if (mapObjects.hasOwnProperty(key)) {
                            //Keys coinciden, eliminar elemento
                            if (key.substring(0, keyLayer.length) === keyLayer) {
                                mapObjects[ key ].setMap(null);

                                //Eliminando del arreglo que pinta en mapa
                                delete mapObjects[key];

                                //Elimina del arreglo que guarda las capas
                                delete projectLayers[keyLayer];
                            }
                        }
                    }
                    //Elimina elemento HTML
                    $("div#" + keyLayer).remove();
                    $(".currentProject").click();

                } else if (action === "edit") {
                    //Editar layer
                    var tipo = keyLayer.split("___");


                    //Elimina capa de arreglo de capas
                    for (key in layerGroup) {
                        if (layerGroup.hasOwnProperty(key)) {
                            //Keys coinciden, eliminar elemento
                            if (key.indexOf(keyLayer) >= 0) {
                                //Eliminando del arreglo que pinta en mapa
                                delete layerGroup[key];
                            }
                        }
                    }

                    //Include: _script_add_01.js

                    //recuperar datos de puntos y/o poligonos
                    var dataObjects = "action=getProjectLayer&id=" + tipo[0];
                    $.ajax({
                        type: "POST",
                        url: "../georeferencia/geo.main.php",
                        data: dataObjects,
                        dataType: 'json',
                        success: function(datos) {
                            polygonCoords = [];

                            //Marcar capa seleccionada
                            $(".hideShowLayer[value='" + keyLayer + "']").prop("checked", true);

                            //bounds (limites del mapa por coordenadas)
                            $.each(datos, function() {
                                //Si el elemento generado es un poligono
                                if (this.tipo == "poli") {

                                    var xy = this.coords;

                                    //draw Polygon
                                    $.each(xy, function() {
                                        var pt = new google.maps.LatLng(this.coord_y, this.coord_x);
                                        bounds.extend(pt);
                                        polygonCoords.push(pt);
                                    });
                                    var poliColor = this.chars.split("___");

                                    // Construct the polygon.
                                    polygon = new google.maps.Polygon({
                                        paths: polygonCoords,
                                        strokeColor: '#' + poliColor[2],
                                        strokeOpacity: 0.8,
                                        strokeWeight: poliColor[3],
                                        fillColor: '#' + poliColor[0],
                                        fillOpacity: poliColor[1],
                                        zIndex: poliColor[4]
                                    });

                                    //Agregar capa al mapa
                                    mapObjects[this.layer_id
                                    + "___"
                                    + this.tipo
                                    + "___"
                                    + this.origen
                                    + "|^"
                                    + this.datos
                                        ] = polygon;


                                    //Verificar la nueva propiedad del Objeto
                                    if (Object.prototype.toString.call(
                                            layerGroup[
                                            this.layer
                                            + "|~"
                                            + this.layer_id
                                            + "___"
                                            + this.tipo
                                            + "___"
                                            + this.origen
                                            + "___"
                                            + this.chars
                                                ]
                                        ) !== '[object Array]')
                                    {
                                        layerGroup[
                                        this.layer
                                        + "|~"
                                        + this.layer_id
                                        + "___"
                                        + this.tipo
                                        + "___"
                                        + this.origen
                                        + "___"
                                        + this.chars] = new Array();
                                    }
                                    if (Object.prototype.toString.call(
                                            layerGroup[
                                            this.layer
                                            + "|~"
                                            + this.layer_id
                                            + "___"
                                            + this.tipo
                                            + "___"
                                            + this.origen
                                            + "___"
                                            + this.chars
                                                ][this.capa]
                                        ) !== '[object Array]')
                                    {
                                        layerGroup[
                                        this.layer
                                        + "|~"
                                        + this.layer_id
                                        + "___"
                                        + this.tipo
                                        + "___"
                                        + this.origen
                                        + "___"
                                        + this.chars][this.capa] = new Array();
                                    }


                                    //Layer Group
                                    var datosArray = this.datos.split("___");

                                    layerGroup[
                                    this.layer
                                    + "|~"
                                    + this.layer_id
                                    + "___"
                                    + this.tipo
                                    + "___"
                                    + this.origen
                                    + "___"
                                    + this.chars][this.capa].push(datosArray[ datosArray.length - 1 ] + "___" + this.chars);

                                    datosArray.push(this.datos);
                                    datosArray.push(this.layer_id
                                    + "___"
                                    + this.tipo
                                    + "___"
                                    + this.origen);

                                    google.maps.event.addListener(polygon, "click", function(event) {
                                        polygonDo(this, event, datosArray, true);
                                    });

                                    polygon.setMap(map);
                                    //map.fitBounds(bounds);

                                    polygonArray.push(polygon);

                                    polygon = null;
                                    polygonCoords = [];

                                } else if (this.tipo == "punto") {
                                    var xy = this.coords;
                                    var itemIcon = this.chars;
                                    var itemChars, arrItemIcon;

                                    var puntoNombre = this.layer;
                                    var puntoOrigen = this.origen;
                                    var puntoLayer = this.layer_id;
                                    var puntoDatos = this.datos;
                                    var puntoCapa = this.capa;
                                    var puntoExt = this.chars.split("|");

                                    var markerNumber = 1;
                                    var pt;
                                    $.each(xy, function() {
                                        pt = new google.maps.LatLng(this.coord_y, this.coord_x);
                                        bounds.extend(pt);
                                        markers.push(pt);
                                        //});

                                        if (itemIcon == "" || itemIcon == "undefined") {
                                            iconSelected = "";
                                            itemChars = "";
                                        } else if (itemIcon.indexOf("custom|") >= 0) {
                                            iconSelected = itemIcon.substr(7);
                                            itemChars = "custom|" + $("#tmpIcon").val();
                                        } else {
                                            iconSelected = "http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=" + itemIcon;
                                            arrItemIcon = itemIcon.split("|");
                                            itemChars = "&chld=" + markerNumber + "|" + arrItemIcon[1] + "|000000";
                                        }

                                        var marker = new google.maps.Marker({
                                            position: pt,
                                            map: map,
                                            icon: iconSelected
                                        });
                                        //if ( itemIcon == "undefined" ) {
                                        marker.metadata = {type: "point", id: puntoExt[0]};
                                        //}

                                        //Agregar capa al mapa
                                        mapObjects[puntoLayer
                                        + "___"
                                        + "punto"
                                        + "___"
                                        + puntoOrigen
                                        + "|^"
                                        + puntoDatos
                                            ] = marker;


                                        var newItemChars, arrItemChars;
                                        if (itemChars.indexOf("&chld=") >= 0) {
                                            arrItemChars = itemChars.split("|");
                                            newItemChars = "chld=|"
                                            + arrItemChars[1]
                                            + "|"
                                            + arrItemChars[2];
                                        } else {
                                            newItemChars = itemChars;
                                        }

                                        //Verificar la nueva propiedad del Objeto
                                        if (Object.prototype.toString.call(layerGroup[
                                            puntoNombre
                                            + "|~"
                                            + puntoLayer
                                            + "___"
                                            + "punto"
                                            + "___"
                                            + puntoOrigen
                                            + "___"
                                            + newItemChars]) !== '[object Array]') {
                                            layerGroup[
                                            puntoNombre
                                            + "|~"
                                            + puntoLayer
                                            + "___"
                                            + "punto"
                                            + "___"
                                            + puntoOrigen
                                            + "___"
                                            + newItemChars] = new Array();
                                        }
                                        if (Object.prototype.toString.call(layerGroup[
                                            puntoNombre
                                            + "|~"
                                            + puntoLayer
                                            + "___"
                                            + "punto"
                                            + "___"
                                            + puntoOrigen
                                            + "___"
                                            + newItemChars][puntoCapa]) !== '[object Array]') {
                                            layerGroup[
                                            puntoNombre
                                            + "|~"
                                            + puntoLayer
                                            + "___"
                                            + "punto"
                                            + "___"
                                            + puntoOrigen
                                            + "___"
                                            + newItemChars][puntoCapa] = new Array();
                                        }

                                        //Layer Group
                                        var puntoDatosArray = puntoDatos.split("___");

                                        layerGroup[
                                        puntoNombre
                                        + "|~"
                                        + puntoLayer
                                        + "___"
                                        + "punto"
                                        + "___"
                                        + puntoOrigen
                                        + "___"
                                        + newItemChars][puntoCapa].push(puntoDatosArray[ puntoDatosArray.length - 1 ] + "___" + itemIcon);

                                        puntoDatosArray.push(puntoDatos);
                                        puntoDatosArray.push(puntoLayer
                                        + "___"
                                        + "punto"
                                        + "___"
                                        + puntoOrigen);

                                        //Solo para direcciones cargadas
                                        if (puntoOrigen === "address") {
                                            puntoDatosArray = puntoDatos.split("___");
                                            var target = puntoDatosArray[0];
                                            puntoDatosArray[0] = "address";
                                            puntoDatosArray[1] = target
                                            + "___"
                                            + this.direccion;
                                            puntoDatosArray[2] = "";
                                        }

                                        //Infowindow del marcador
                                        google.maps.event.addListener(marker, "click", function(event) {
                                            markerDo(marker, event, puntoDatosArray, true);
                                        });

                                        markerArray.push(marker);
                                        marker.setMap(map);
                                        marker = null;
                                        markerNumber++;

                                    });
                                    //map.fitBounds(bounds);

                                } else if (this.tipo == "circle") {

                                    var xy = this.coords;
                                    var itemIcon = this.chars;
                                    var itemChars, arrItemIcon;

                                    var puntoNombre = this.layer;
                                    var puntoOrigen = this.origen;
                                    var puntoLayer = this.layer_id;
                                    var puntoDatos = this.datos;
                                    var puntoCapa = this.capa;
                                    var puntoExt = this.chars.split("___");
                                    var puntoRadio;

                                    $.each(xy, function() {
                                        var pt = new google.maps.LatLng(this.coord_y, this.coord_x);

                                        if (Number(this.radio) > 0) {
                                            puntoRadio = Number(this.radio);
                                        } else {
                                            puntoRadio = Number(puntoExt[5]);
                                        }

                                        var circleOptions = {
                                            strokeColor: puntoExt[2],
                                            strokeOpacity: 0.8,
                                            strokeWeight: puntoExt[3],
                                            fillColor: puntoExt[0],
                                            fillOpacity: puntoExt[1],
                                            map: map,
                                            center: pt,
                                            radius: puntoRadio
                                        };

                                        // Add the circle for this city to the map.
                                        var circle = new google.maps.Circle(circleOptions);

                                        bounds.union(circle.getBounds());

                                        //Agregar capa al mapa
                                        mapObjects[puntoLayer
                                        + "___"
                                        + "circle"
                                        + "___"
                                        + puntoOrigen
                                        + "|^"
                                        + puntoDatos
                                            ] = circle;



                                        //Verificar la nueva propiedad del Objeto
                                        if (Object.prototype.toString.call(layerGroup[
                                            puntoNombre
                                            + "|~"
                                            + puntoLayer
                                            + "___"
                                            + "circle"
                                            + "___"
                                            + puntoOrigen
                                            + "___"
                                            + itemIcon]) !== '[object Array]') {
                                            layerGroup[
                                            puntoNombre
                                            + "|~"
                                            + puntoLayer
                                            + "___"
                                            + "circle"
                                            + "___"
                                            + puntoOrigen
                                            + "___"
                                            + itemIcon] = new Array();
                                        }
                                        if (Object.prototype.toString.call(layerGroup[
                                            puntoNombre
                                            + "|~"
                                            + puntoLayer
                                            + "___"
                                            + "circle"
                                            + "___"
                                            + puntoOrigen
                                            + "___"
                                            + itemIcon][puntoCapa]) !== '[object Array]') {
                                            layerGroup[
                                            puntoNombre
                                            + "|~"
                                            + puntoLayer
                                            + "___"
                                            + "circle"
                                            + "___"
                                            + puntoOrigen
                                            + "___"
                                            + itemIcon][puntoCapa] = new Array();
                                        }

                                        //Layer Group
                                        var puntoDatosArray = puntoDatos.split("___");

                                        layerGroup[
                                        puntoNombre
                                        + "|~"
                                        + puntoLayer
                                        + "___"
                                        + "circle"
                                        + "___"
                                        + puntoOrigen
                                        + "___"
                                        + itemIcon][puntoCapa].push(puntoDatosArray[ puntoDatosArray.length - 1 ] + "___" + itemIcon);

                                        puntoDatosArray.push(puntoDatos);
                                        puntoDatosArray.push(puntoLayer
                                        + "___"
                                        + "circle"
                                        + "___"
                                        + puntoOrigen);

                                        //Infowindow del marcador
                                        google.maps.event.addListener(circle, "click", function(event) {
                                            polygonDo(circle, event, puntoDatosArray, true);
                                        });

                                    });
                                    //map.fitBounds(bounds);
                                }
                            });
                            //Ajustar elementos al mapa
                            map.fitBounds(bounds);
                            //Remover class "loading" luego de dibujar elementos
                            $("body").removeClass("loading");
                        }
                    });
                    //FIN: include _script_add_01.js


                    //Ubicar estilo
                    var layerCss = "";

                    $("#estilo_punto_edit").hide();
                    $("#estilo_poligono_edit").hide();

                    //Marcar casilla de color de linea
                    $("#colorLineEdit").prop("checked", true);

                    //Edicion por tipo de elemento
                    if (tipo[1] == "punto") {

                        /**
                         *
                         * Estilos de la capa seleccioanda
                         *
                         */
                        var markerColor;
                        var estilosLayer;
                        if (layerCss == "undefined" || layerCss == "") {
                            //Color del marcador por defecto de Google Maps
                            markerColor = "F7584C";
                            $("input[name=iconSelectEdit][value='default']").prop("checked", true);
                        } else if (layerCss.indexOf("custom|") >= 0) {
                            //Marcar radio button
                            $("input[name=iconSelectEdit][value='custom']").prop("checked", true);
                            //Mostrar imagen seleccionada
                            $("#customIconPreviewEdit").prop("src", layerCss.substring(7));
                        } else {
                            estilosLayer = layerCss.split("|");
                            markerColor = estilosLayer[1];
                            $("input[name=iconSelectEdit][value='numeric']").prop("checked", true);
                        }

                        //Color de fondo
                        $(".simpleColorDisplay").each(function(key, val) {
                            if (key == 5) {
                                $(this).css("background-color", "#" + markerColor);
                                //codigo de color
                                $(this).html("#" + markerColor);
                                //Input con codigo de color
                                $("#colorPickerPuntoEdit").val("#" + markerColor);
                            }
                        });

                        $("#estilo_punto_edit").show();

                    } else if (tipo[1] == "poli" || tipo[1] == "circle") {

                        /**
                         *
                         * Estilos de la capa seleccioanda
                         *
                         */

                        var estilosLayer = layerCss.split("___");
                        //Color de fondo
                        $(".simpleColorDisplay").each(function(key, val) {
                            if (key == 3) {
                                $(this).css("background-color", estilosLayer[0]);
                                //Opacidad
                                $(this).css("opacity", estilosLayer[1]);
                                $(".sliderEdit a").css("left", (estilosLayer[1] * 100) + "px");
                                //Código de color
                                $(this).html(estilosLayer[0]);
                                //Input con color de fondo
                                $("#colorPickerPoligonoEdit").val(estilosLayer[0]);
                            }
                            //Color de linea
                            if (key == 4) {
                                //Color de fondo
                                $(this).css("background-color", estilosLayer[2]);
                                //Código de color
                                $(this).html(estilosLayer[2]);
                                //Input con color de fondo
                                $("#colorPickerLineEdit").val(estilosLayer[2]);
                            }
                            //Espesor de linea
                            $("#lineEditWeight option").filter(function() {
                                return $(this).val() == estilosLayer[3];
                            }).prop('selected', true);
                        });

                        $("#estilo_poligono_edit").show();
                    }
                }
            });

            //Mostrar herramientas de diseño de elementos
            $(".showEditTools").click(function (event){
                event.preventDefault();

                var title = $(this).attr("title");
                var arrTitle = title.split("___");

                for (key in layerGroup) {
                    //Capa cargada en el arreglo
                    if ( layerGroup.hasOwnProperty(key)
                        && key.indexOf(title) >= 0 ) {
                        if (arrTitle[1] === "punto") {
                            $( "#estilo_poligono_edit" ).dialog( "close" );
                            $( "#estilo_punto_edit" ).dialog( "open" );

                        } else {
                            $( "#estilo_punto_edit" ).dialog( "close" );
                            $( "#estilo_poligono_edit" ).dialog( "open" );
                        }
                    } else {
                        alert("La capa no ha sido cargada aun."
                        + "Haga click sobre el nombre de la capa");
                    }
                }

            });

        }
    });
}

/**
 * Muestra una capa del proyecto
 *
 * @param {type} keyLayer, el ID de la capa
 * @returns {Boolean}
 */
function showMapLayer(keyLayer) {
    for (key in mapObjects) {
        if (mapObjects.hasOwnProperty(key)) {
            //Keys coinciden
            if (key.substring(0, keyLayer.length) == keyLayer) {
                //Mostrar elementos del mapa
                mapObjects[ key ].setMap(map);
            }
        }
    }
    return true;
}

/**
 * Oculta una capa del proyecto
 *
 * @param {type} keyLayer el ID de la capa
 * @returns {Boolean}
 */
function hideMapLayer(keyLayer) {
    for (key in mapObjects) {
        if (mapObjects.hasOwnProperty(key)) {
            //Keys coinciden
            if (key.substring(0, keyLayer.length) == keyLayer) {
                //Mostrar elementos del mapa
                mapObjects[ key ].setMap(null);
            }
        }
    }
    return true;
}
