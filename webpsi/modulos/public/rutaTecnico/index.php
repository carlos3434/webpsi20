<?php

//ini_set('display_errors', 'On');
//error_reporting(E_ALL);

require_once '../../../clases/class.Conexion.php';
require_once '../../officetrack/clases/class.Location.php';
require_once '../../officetrack/clases/class.Tecnico.php';
require_once '../../officetrack/clases/class.OtUtils.php';
require_once '../../georeferencia/clases/class.GeoUtils.php';
require_once '../../georeferencia/clases/class.GeoTap.php';
require_once '../../georeferencia/clases/class.GeoTerminald.php';
require_once '../../georeferencia/clases/class.GeoTerminalf.php';


$Conexion       = new Conexion();
$GeoTap         = new GeoTap();
$GepTerminald   = new GeoTerminald();
$GepTerminalf   = new GeoTerminalf();
$Utils = new OtUtils();
$GeoUtils = new GeoUtils();
$Location = new Location();
$Tecnico = new Tecnico();
$db = $Conexion->conectarPDO();


//FILTROS A MOSTRAR

//si proviene de un usuario logeado
$estados = $_REQUEST["estados"]; // para mostrar estados fuera de los que estan por default
$tareas = $_REQUEST["tareas"]; //cuando se quieren ver tareas en especifico
$sin_fecha = $_REQUEST["sin_fecha"]; // llega "si" para mostrar agendas fuera del dia de hoy


if(empty($estados)){
    $estados ="1,8,9,10,20"; //estados por defecto publicos
}

if(empty($sin_fecha)){
    //OBTENER TAREAS DEL DIA DE HOY
    $fini=date("Y-m-d");
    $nuevafecha = strtotime ( '+1 day' , strtotime ( $fini ) ) ;
    $ffin = date ( 'Y-m-d' , $nuevafecha );
}

if(!empty($tareas)){ // las ids llegan como codigo ATC , si existen las proceso y dejamos una lista de ids concatenadas
    $array_tasks = explode(",",$tareas);
    $tasks = array();
    foreach($array_tasks as $value){
        list($type,$anio,$id) = explode("_",$value);
        $tasks[]=$id;
    }
    $tasks = implode(",",$tasks);
}


//GET TECNICO
$carnet  = $_REQUEST["cc"];
$tec = $Tecnico->getTecnico($db, "", $carnet, "", "");
//$tarea = $_REQUEST["tarea"];

//UBICACION TECNICO
$codArray = array($carnet);
$date =date("Y/m/d"); // formato aceptado por la clase
$ubi_tec = $Location->getLocations($db, $codArray, array(), $date);

$current_geo_tec = $ubi_tec["data"][0];
if($current_geo_tec == null){
    $current_geo_tec = array();
}else{
    //en gmaps se usan PUNTOS no COMAS , se necesita hacer el reemplazo
    $current_geo_tec["X"]  = str_replace(",",".",$current_geo_tec["X"]);
    $current_geo_tec["Y"]  = str_replace(",",".",$current_geo_tec["Y"]);
}

//OBTENGO AVERIAS DEL TECNICO $db, $empresa, $celula, $fini, $fend, $carnet, $estado , $task_id = "",$tasks = ""
$agenda =     $Location->getAgendasTodos( $db, "", "", $fini, $ffin, $carnet, $estados, $task_id = "",$tasks);
if ($agenda["estado"]===true and !empty($agenda["data"])){
    //SE AGREGAN COORDENADAS
    $dataTec["agenda"] = $Utils->getActuCoord(  $agenda, $db, $GeoTap, $GepTerminald, $GepTerminalf   );
}

//INICIALIZACION DE VARIABLES VACIAS , PARA EVITAR ERRORES EN LA RENDERIZACION DEL MAPA
if(!is_array($dataTec["agenda"])){    $dataTec["agenda"] = array();}

$data = $dataTec["agenda"];


//PROCESO DE ESTABLECER EL MAYOR Y MENOR Y PUNTOS INTERNOS
$data_neto = array();
$id_menor = 0;
$id_menor_key = 0;
$id_mayor = 0;
$id_mayor_key = 0;
$count = 0;
foreach($data as $val){

    if($val["y"] != ""){
        $d = $GeoUtils->haversineGreatCircleDistance(0, 0, $val["y"], $val["x"]);
        $data_neto[] = $val;
        if($d <$id_menor || $id_menor == 0){
            $id_menor = $d;
            $id_menor_array = $data_neto[$count];
            $id_menor_key = $count;
        }
        if($d > $id_mayor){
            $id_mayor = $d;
            $id_mayor_array = $data_neto[$count];
            $id_mayor_key = $count;
        }
        $count++;
    }
}

$deb = 1;
//SI LA UBICACION DEL TECNICO EXISTE AGREGARLO AL DIRECTION SERVICES
//como punto de origen
if(!empty($current_geo_tec)){
    $id_menor_array  = array("y"=> $current_geo_tec["Y"] , "x"=> $current_geo_tec["X"]);
}else{
    //si no existe ubicaicon de tecnico , elimino del arreglo el punto menor por default , para que el $data_neto solo tenga los puntos medios
    unset($data_neto[$id_menor_key]);
}

//ELIMINAMOS MAYOR , para que $data_neto solo tenga los puntos medios
unset($data_neto[$id_mayor_key]);

$agenda_datos["mayor"] = $id_mayor_array;
$agenda_datos["menor"] = $id_menor_array;
$agenda_datos["entre"] = $data_neto; // PUNTOS MEDIOS


?>

<!DOCTYPE html>
<html lang="en">
	<head>
		<meta http-equiv="content-type" content="text/html; charset=UTF-8">
		<meta charset="utf-8">
		<title>Ruta del dia</title>
		<meta name="generator" content="Bootply" />
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
		<link href="css/bootstrap.min.css" rel="stylesheet">
		<!--[if lt IE 9]>
			<script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script>
		<![endif]-->
		<link href="css/styles.css" rel="stylesheet">
	</head>
	<body>
<!-- begin template -->
<div class="navbar navbar-custom navbar-fixed-top">
 <div class="navbar-header"><a class="navbar-brand" href="#">Actualizar</a>
      <a class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </a>
    </div>

</div>

<div id="map-canvas"></div>
<div class="container-fluid" id="main">
  <div class="row">
    <div class="col-xs-12"></div>
  </div>
</div>

<script>
    //OBTENER PUNTOS Y RUTAS
    var agendas = <?= json_encode( $agenda_datos )?>;
    var current_geo_tec = <?= json_encode( $current_geo_tec )?>;

</script>
		<script src="js/jquery.min.js"></script>
		<script src="js/bootstrap.min.js"></script>
		<script src="http://maps.googleapis.com/maps/api/js?sensor=false&extension=.js&output=embed"></script>
<!--		<script src="../../officetrack/js/officetrack.js"></script>-->
		<script src="js/scripts.js"></script>
	</body>
</html>