<?php

//ini_set('display_errors', 'On');
//error_reporting(E_ALL);

require_once '../../../clases/class.Conexion.php';
require_once '../../officetrack/clases/class.Location.php';
require_once '../../officetrack/clases/class.Tecnico.php';
require_once '../../officetrack/clases/class.OtUtils.php';
require_once '../../georeferencia/clases/class.GeoUtils.php';
require_once '../../georeferencia/clases/class.GeoTap.php';
require_once '../../georeferencia/clases/class.GeoTerminald.php';
require_once '../../georeferencia/clases/class.GeoTerminalf.php';


$Conexion       = new Conexion();
$GeoTap         = new GeoTap();
$GepTerminald   = new GeoTerminald();
$GepTerminalf   = new GeoTerminalf();
$Utils = new OtUtils();
$GeoUtils = new GeoUtils();
$db = $Conexion->conectarPDO();


//GET TECNICO
$carnet  = $_REQUEST["cc"];

$Tecnico = new Tecnico();
$tec = $Tecnico->getTecnico($db, "", $carnet, "", "");

$Location = new Location();

$fini=date("Y-m-d");
$nuevafecha = strtotime ( '+1 day' , strtotime ( $fini ) ) ;
$ffin = date ( 'Y-m-d' , $nuevafecha );


$agenda =     $Location->getAgendaAveria( $db, "", "", $fini, $ffin, $carnet, "1,8,9,10,20");
if ($agenda["estado"]===true and !empty($agenda["data"])){
    $dataTec["agenda"] = $Utils->getActuCoord(  $agenda, $db, $GeoTap, $GepTerminald, $GepTerminalf   );
}

$agenda =     $Location->getAgendaProvision( $db, "", "", $fini, $ffin, $carnet, "1,8,9,10,20");
if ($agenda["estado"]===true and !empty($agenda["data"])){
    $dataTecProv["agenda"] = $Utils->getActuCoord(  $agenda, $db, $GeoTap, $GepTerminald, $GepTerminalf   );
}


//MOSTRAR PUNTOS
if(!is_array($dataTec["agenda"])){
    $dataTec["agenda"] = array();
}
if(!is_array($dataTecProv["agenda"])){
    $dataTecProv["agenda"] = array();
}

$data = array_merge($dataTec["agenda"], $dataTecProv["agenda"]) ;
$data_neto = array();
$id_menor = 0;
$id_menor_key = 0;
$id_mayor = 0;
$id_mayor_key = 0;
$count = 0;
foreach($data as $val){

    if($val["y"] != ""){
        $d = $GeoUtils->haversineGreatCircleDistance(0, 0, $val["y"], $val["x"]);
        $data_neto[] = $val;

        if($d <$id_menor || $id_menor == 0){
            $id_menor = $d;
            $id_menor_array = $data_neto[$count];
            $id_menor_key = $count;
        }

        if($d > $id_mayor){
            $id_mayor = $d;
            $id_mayor_array = $data_neto[$count];
            $id_mayor_key = $count;
        }

        $count++;
    }
}

//ELIMINAMOS MAYOR Y MENOR
unset($data_neto[$id_mayor_key]);
unset($data_neto[$id_menor_key]);

$agenda_datos["mayor"] = $id_mayor_array;
$agenda_datos["menor"] = $id_menor_array;
$agenda_datos["entre"] = $data_neto;

?>


<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"  content="width=device-width, initial-scale=1, maximum-scale=1" />
    <title>Ruta del dia</title>
    <script src="http://code.jquery.com/jquery-1.11.1.min.js"></script>
    <script src="http://code.jquery.com/ui/1.11.1/jquery-ui.js"></script>
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?v=3.exp&libraries=places"></script>

</head>
<body>
<div id='map_canvas' style="width:100%; height:400px;"></div>


<script type="text/javascript">
    $(document).ready(function() {
        if(agendas.mayor != null){
            initialize();
            calcRoute();
        }else{
            var myLatlng = new google.maps.LatLng(-12.046374, -77.0427934);
           map = new google.maps.Map(document.getElementById('map_canvas'), {
                zoom:12,
                center: myLatlng,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            });

            alert("No Agendas Agendadas")
        }
    });

    var markers = [];
    var map;
    //OBTENER PUNTOS Y RUTAS
    var agendas = <?= json_encode( $agenda_datos )?>;
    var directionsDisplay;
    var directionsService = new google.maps.DirectionsService();

    function initialize() {
        directionsDisplay = new google.maps.DirectionsRenderer();
        var chicago = new google.maps.LatLng(agendas.mayor.y , agendas.mayor.x);

        var mapOptions = {
            zoom: 6,
            mapTypeId: google.maps.MapTypeId.ROADMAP,
            center: chicago
        }
        map = new google.maps.Map(document.getElementById("map_canvas"), mapOptions);
        directionsDisplay.setMap(map);


    }

    function calcRoute() {

        var waypts = [];
        $.each(agendas.entre, function(){

                var point = new google.maps.LatLng(this.y , this.x);
                waypts.push({
                    location:point,
                    stopover:true
                });

        })

        var point_mayor = new google.maps.LatLng(agendas.mayor.y , agendas.mayor.x);
        var point_menor = new google.maps.LatLng(agendas.menor.y , agendas.menor.x);
        var request = {
            origin: point_menor,
            destination: point_mayor,
            waypoints: waypts,
            optimizeWaypoints: true,
            travelMode: google.maps.TravelMode.DRIVING
        };

        directionsService.route(request, function(response, status) {
            if (status == google.maps.DirectionsStatus.OK) {
                directionsDisplay.setDirections(response);
               showSteps(response)
            }
        });
    }

    var markerArray = []
    function showSteps(directionResult) {

        agregarMarker(agendas.mayor)
        agregarMarker(agendas.menor)
        $.each(agendas.entre, function(){
            agregarMarker(this)
        })



    }

function agregarMarker(data){

    var info = infoContent(data);
    var infowindow = new google.maps.InfoWindow({    content: info     });
    var p = new google.maps.LatLng(data.y , data.x);
    var marker = new google.maps.Marker({
        position: p,
        map: map,
        zIndex: 999
    });
    google.maps.event.addListener(marker, 'click', function() {
        infowindow.open(map,marker);
    });


}

function infoContent(ele){

    var info =   "<div class=\"infow\" style=\"width:300px; height:200px; overflow: auto\">" +
        "<div class=\"detalle_actu\">" +
        ele.tipoactu + "<br>" +
        ele.nombre_cliente_critico + "<br>" +
        ele.fecha_agenda + " / " +
        ele.horario + "<br>" +
        ele.observacion + "<br>" +
        ele.direccion + "<br>" +
        ele.codactu + "<br>" +
        ele.fftt + "<br>" +
        ele.codcli + "<br>" +
        ele.mdf + "<br>" +
        ele.id_atc + "<br>" +
        ele.lejano + "<br>" +
        ele.carnet_critico + "<br>" +
        ele.paquete + "<br>" +
        ele.quiebre + "<br>" +
        ele.tecnico + "<br>" +
        "</div>" ;
    return info;
}



</script>
</body>
</html>