<?php

include_once "class.ConexionRss.php";

class Miembros {
    private $id = '';
    private $Nombre = '';
    private $Apellido = '';
    private $Numero = '';
	private $Grupo = '';
    private $Estado = '';

    public function getId() {
        return $this->id;
    }

    public function setId($x) {
        $this->id = $x;
    }

    public function getNombre() {
        return $this->Nombre;
    }

    public function setNombre($x) {
        $this->Nombre = $x;
    }

    public function getApellido() {
        return $this->Apellido;
    }

    public function setApellido($x) {
        $this->Apellido = $x;
    }

    public function getNumero() {
        return $this->Numero;
    }

    public function setNumero($x) {
        $this->Numero = $x;
    }
	
	public function getGrupo() {
        return $this->Grupo;
    }

    public function setGrupo($x) {
        $this->Grupo = $x;
    }

    public function getEstado() {
        return $this->Estado;
    }

    public function setEstado($x) {
        $this->Estado = $x;
    }
    
    public function IngresarMiembros($Nombre,$Apellido,$Numero,$Grupo) {
		
		$db = new ConexionRss();
        $cnx = $db->conectarBD();

        $cad = "INSERT INTO `tt_rss`.`miembros` 
	(`id`, 
	`nombre`, 
	`apellido`, 
	`numero`, 
	`grupo`, 
	`estado`
	)
	VALUES
	(null, 
	'".$Nombre."', 
	'".$Apellido."', 
	'".$Numero."', 
	'".$Grupo."', 
	'0'
	);";

        $res = mysql_query($cad, $cnx) or die(mysql_error());
        $cnx = NULL;
        $db = NULL;
        return $arr;
    }
    
    public function ActualizarMiembros($Id,$Nombre,$Apellido,$Numero,$Grupo,$Estado) {
        $db = new ConexionRss();
        $cnx = $db->conectarBD();
				
		$cad = "UPDATE `tt_rss`.`miembros` 
	SET 
	`nombre` = '".$Nombre."' , 
	`apellido` = '".$Apellido."' , 
	`numero` = '".$Numero."' , 
	`grupo` = '".$Grupo."' , 
	`estado` = '".$Estado."'
	
	WHERE
	`id` = '".$Id."' ;";

        $res = mysql_query($cad, $cnx) or die(mysql_error());
        $cnx = NULL;
        $db = NULL;
        return $arr;
    }
	
	public function ListarMiembros() {
		$db = new ConexionRss();
		$cnx = $db->conectarBD();
		
		$cad = "
		SELECT `id`, 
		`nombre`, 
		`apellido`, 
		`numero`, 
		`grupo`, 
		`estado`
		 
		FROM 
		`tt_rss`.`miembros` 
		LIMIT 0, 1000;";
		
		$arr = array();
		
		$res = mysql_query($cad, $cnx) or die(mysql_error());
		while ($row = mysql_fetch_array($res))
		{
			$arr[] = $row;
		}
		return $arr;
	
	}

		public function RetornaMiembros($cod) {
		$db = new ConexionRss();
		$cnx = $db->conectarBD();
		
		$cad = "
		SELECT `id`, 
		`nombre`, 
		`apellido`, 
		`numero`, 
		`grupo`, 
		`estado`
		 
		FROM 
		`tt_rss`.`miembros` 
		where id=".$cod;
		
		$arr = array();
		
		$res = mysql_query($cad, $cnx) or die(mysql_error());
		while ($row = mysql_fetch_array($res))
		{
			$arr[] = $row;
		}
		return $arr;
	
	}
}