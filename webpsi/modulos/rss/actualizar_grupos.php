<?php
$level_pag=1023;
require_once("clases/class.ConexionSigas.php");

require_once("../../cabecera.php");

$idsess = "";

?>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>SMS Libre</title>
<?php include ("../../includes.php") ?>  
<link href="estilos.css" rel="stylesheet" type="text/css" media="all"/>

<script type="text/javascript" src="jquery-1.7.2.min.js"></script>

<script type="text/javascript">

function actualiza_03()
{
    var codigo = $("#codigo").val();
	var descripcion = $("#descripcion").val();
	var estado = $("#estado").val();

	$.ajax({
	  type: "POST",
	  url: "actualizar_grupo_ajax.php",
	  data: { 
		actualiza_grupo: 1,
		codigo:codigo,
		descripcion: descripcion,
		estado:estado
	  }
	}).done(function( msg ) {
		alert("Datos Actualizados Correctamente");
	});
}

</script>

</head>
<?php
require_once("clases/class.Grupitos.php");
$cod=$_GET["cod"];
//echo $cod;
$user = new Grupitos();
    $a = $user->RetornaGrupos($cod);

?>
<body background="imagenes/fondo.gif">
<?php
echo pintar_cabecera(); 
?>
        <input type="hidden" value="" name="txt_idusuario" id="txt_idusuario"/>
        <div id="page-wrap">
            <div id="main-content">
                <div id="id0" class="div0">
								<?php
	$i=0;
	foreach ($a as $row) {
	?>
                <form method="post" action="listado_02.php">
				<table border="2">
                <thead>
                    <tr class="odd">
                        <th scope="col" abbr="Home" colspan="4" class="col_titulo">Actualizacion de Grupos</th>
                    </tr>	
                </thead>
                <tbody>
                    <tr>
                        <th scope="row" class="column1">Codigo:</th>
                        <td class="column2"><input name="codigo" id="codigo" type="text" value="<?php echo $row["codigo"]?>" /></td>
                    </tr>
					
                    <tr>
                        <th scope="row" class="column1">Descripcion:</th>
                        <td class="column2"><input name="descripcion" id="descripcion" type="text" value="<?php echo $row["descripcion"]?>" /></td>

                    </tr>
					
					<tr>
					   <th scope="row" class="column1">Estado:</th>
                        <td class="column2">
						<select id="estado" >
						<?php
							$estado=$row["estado"];
							if ($estado==1){
						?>
						<option value="1" selected>Habilitado</option>
						<option value="0" >Deshabilitado</option>
						<?php
							}else{
						?>
						<option value="1" >Habilitado</option>
						<option value="0" selected>Deshabilitado</option>
						<?php
						}
						?>
						</select>
						</td>
					</tr>
						<?php
	$i++;
	}
?>				
                    <tr>
                        <td colspan="2" class="td_center">
						<input name="actualizar" type="button" value="Actualizar" onclick="actualiza_03();"/>
						<input name="regresar" type="submit" value="Regresar"/>
						</td>
                    </tr>
                </tbody>
                </table>
				</form>
                </div>

                <div id="div_res" class="div0"></div>                

        </div>

        </div>
		              <div id="footer">
                &copy;      2013 PSI - Planificacion de Soluciones Informaticas
            </div>
</body>
</html>
