<?php
//require_once("clases/class.ConexionSigas.php");
require_once("../../cabecera.php");
require_once("clases/class.Miembros.php");

$idsess = "";


?>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>SMS Libre</title>
<?php include ("../../includes.php") ?> 
<link rel="stylesheet" href="jquery-ui-1.10.3/themes/base/jquery.ui.all.css" />
             <script src="jquery-ui-1.10.3/jquery-1.9.1.js"></script>
             <script src="jquery-ui-1.10.3/ui/jquery-ui.js"></script>

<script type="text/javascript">

function ingreso()
{
 
	var nombre = $("#name").val();
	var apellido = $("#apellido").val();
	var celular = $("#celular").val();
	var grupo = $("#grupo").val();
	$.ajax({
	  type: "POST",
	  url: "ingresar_miembros_ajax.php",
	  data: { 
		ingresa_miembros: 1,
		nombre: nombre,
		apellido: apellido,
		celular: celular,
		grupo: grupo
	  }
	}).done(function( msg ) {
		alert("Datos Guardados Correctamente");
	});
}

</script>

</head>
    
<body background="imagenes/fondo.gif">
<?php
echo pintar_cabecera(); 
?>
        <input type="hidden" value="" name="txt_idusuario" id="txt_idusuario"/>
        <div id="page-wrap">
            <div id="main-content">
                <div id="id0" class="div0">
                <form method="post" action="listado_01.php">
				<table border="2">
                <thead>
                    <tr class="odd">
                        <th scope="col" abbr="Home" colspan="4" class="col_titulo">Ingreso de Usuarios</th>
                    </tr>	
                </thead>
                <tbody>
                    <tr>
                        <th scope="row" class="column1">Nombre:</th>
                        <td class="column2"><input name="name" id="name" type="text" maxlength="10" /></td>
                    </tr>
                    <tr>
                        <th scope="row" class="column1">Apellido:</th>
                        <td class="column2"><input name="apellido" id="apellido" type="text" maxlength="30" /></td>

                    </tr>
                    <tr>
                        <th scope="row" class="column1">Celular:</th>
                        <td class="column2"><input name="celular" id="celular" type="text" maxlength="10" /></td>
                    </tr>
					
					<tr>
                        <th scope="row" class="column1">Grupo:</th>
                        <td class="column2"><input name="grupo" id="grupo" type="text" maxlength="10" /></td>
                    </tr>
					
                    <tr>
                        <td colspan="2" class="td_center">
						<input name="ingresar" type="button" value="Ingresar" onclick="ingreso();"/>
						<input name="regresar" type="submit" value="Regresar"/>
					</td>
                    </tr>
                </tbody>
                </table>
				</form>
                </div>

                <div id="div_res" class="div0"></div>                

        </div>

        </div>
		
		              <div id="footer">
                &copy;      2013 PSI - Planificacion de Soluciones Informaticas
            </div>
</body>
</html>
