<?php
$level_pag=1023;
require_once("clases/class.ConexionSigas.php");

require_once("../../cabecera.php");

$idsess = "";

?>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>SMS Libre</title>
<?php include ("../../includes.php") ?>   
<link rel="stylesheet" href="jquery-ui-1.10.3/themes/base/jquery.ui.all.css" />

<script type="text/javascript">

function actualizar_miembros()
{
    var codigo = $("#codigo").val();
	var nombre = $("#nombre").val();
	var apellido = $("#apellido").val();
	var celular = $("#celular").val();
	var grupo = $("#grupo").val();
	var estado = $("#estado").val();
	
	$.ajax({
	  type: "POST",
	  url: "actualizar_miembros_ajax.php",
	  data: { 
		actualiza_miembros: 1,
		codigo:codigo,
		nombre: nombre,
		apellido: apellido,
		celular: celular,
		grupo: grupo,
		estado:estado
	  }
	}).done(function( msg ) {
		alert("Datos Actualizados Correctamente");
	});
}

</script>

</head>

<?php
require_once("clases/class.Miembros.php");
$cod=$_GET["cod"];
//echo $cod;
$user = new Miembros();
    $a = $user->RetornaMiembros($cod);

?>
    
<body background="imagenes/fondo.gif">
<?php
echo pintar_cabecera(); 
?>
        <input type="hidden" value="" name="txt_idusuario" id="txt_idusuario"/>
        <div id="page-wrap">
            <div id="main-content">
                <div id="id0" class="div0">
				<?php
	$i=0;
	foreach ($a as $row) {
	?>
                <form method="post" action="listado_01.php">
				<table border="2">
                <thead>
                    <tr class="odd">
         <th scope="col" abbr="Home" colspan="4" class="col_titulo">Actualizacion de Usuarios</th>
                    </tr>	
                </thead>
                <tbody>
				    <tr>
                        <th scope="row" class="column1">Codigo:</th>
                        <td class="column2"><input name="codigo" id="codigo" type="text" maxlength="10" value="<?php echo $row["id"]?>" /></td>
                    </tr>
                    <tr>
                        <th scope="row" class="column1">Nombre:</th>
                        <td class="column2"><input name="nombre" id="nombre" type="text" maxlength="10" value="<?php echo $row["nombre"]?>" /></td>
                    </tr>
                    <tr>
                        <th scope="row" class="column1">Apellido:</th>
                        <td class="column2"><input name="apellido" id="apellido" type="text" maxlength="20" value= "<?php echo $row["apellido"]?>" /></td>

                    </tr>
                    <tr>
                        <th scope="row" class="column1">Celular:</th>
                        <td class="column2"><input name="celular" id="celular" type="text" maxlength="10" value="<?php echo $row["numero"]?>"/></td>
                    </tr>
					
					<tr>
                        <th scope="row" class="column1">Grupo:</th>
                        <td class="column2"><input name="grupo" id="grupo" type="text" maxlength="10" value="<?php echo $row["grupo"]?>"/></td>
                    </tr>
					    
					<tr>
					   <th scope="row" class="column1">Estado:</th>
                        <td class="column2">
						<select id="estado" >
						<?php
							$estado=$row["estado"];
							if ($estado==1){
						?>
						<option value="1" selected>Habilitado</option>
						<option value="0" >Deshabilitado</option>
						<?php
							}else{
						?>
						<option value="1">Habilitado</option>
						<option value="0" selected>Deshabilitado</option>
						<?php
						}
						?>
						</select>
						</td>
					</tr>
					<?php
	$i++;
	}
?>
                    <tr>
                        <td colspan="2" class="td_center">
						<input name="actualizar" type="button" value="Actualizar" onclick="actualizar_miembros();"/>
						<input name="regresar" type="submit" value="Regresar"/>
						</td>
                    </tr>
                </tbody>
                </table>
				</form>
                </div>

                <div id="div_res" class="div0"></div>                

        </div>

        </div>
		              <div id="footer">
                &copy;      2013 PSI - Planificacion de Soluciones Informaticas
            </div>
</body>
</html>
