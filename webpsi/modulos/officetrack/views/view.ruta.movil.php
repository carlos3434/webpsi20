<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <title>Visor GPS</title>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

    <link href="../public/rutaTecnico/css/bootstrap.min.css" rel="stylesheet">
    <script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <link href="../public/rutaTecnico/css/styles.css" rel="stylesheet">

    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.1/themes/smoothness/jquery-ui.css">
    <link rel="stylesheet" href="css/officetrack.css">
    <link type="text/css" href='css/jquery.multiselect.css' rel="Stylesheet" />

    <script src="https://maps.googleapis.com/maps/api/js?v=3.exp"></script>
    <script src="http://code.jquery.com/jquery-1.11.1.min.js"></script>
    <script src="http://code.jquery.com/ui/1.11.1/jquery-ui.js"></script>
    <script src="js/ruta.config.js?t=<?php echo time();?>"></script>
    <script src="js/officetrack.js?t=<?php echo time();?>"></script>
    <script src="js/CelulaGruposTecnicos.js?t=<?php echo time();?>"></script>
    <script src="js/utils.js?t=<?php echo time();?>"></script>

    <script src="js/jquery.multiselect.js"></script>

    <style>
        #wrap-proyectos{
            min-width: 100%;
            min-height: 20%;
        }

        #wrap-proyectos.block-active,
        #tools.block-active{
            position: absolute;
            top:50px;
            left: 0px;
            z-index: 99999;
            background: #fff;
            height: auto;
            width: auto;
            padding: 10px 20px;
        }
        #map-tec{
            z-index: 999999;
            background: #fff;
            position: absolute;
            top: 50px;
            left: 0px;
            height: 100%;
            width: 60% !important;;
        }        #map-canvas{
            height: 100%;
            width: 100%;
        }
        .filtro-cerrar {
            background: red;
            color: #fff;
            font-weight: bold;
            border-radius: 5px;
            display: inline-block;
            padding: 2px 5px;
            margin: 0px 10px;
            cursor: pointer;
        }
        legend{
            font-size: 12px;;
        }
        h4,
        .navbar-brand{
            font-size: 12px;
        }

        .ui-multiselect-menu{
            z-index: 999999;
        }

        [onclick^=gestionActu] {
            display: none;
        }


    </style>
</head>
<body>

<div class="navbar navbar-custom navbar-fixed-top">
    <div class="navbar-header">
        <a class="navbar-brand reload" href="#">Actualizar</a>
        <a class="navbar-brand filtros" href="#">Filtros</a>
        <a class="navbar-brand detalles" href="#">Detalles</a>
<!--        <a class="navbar-brand proyectos" href="#">Proyectos</a>-->
        <a class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </a>
    </div>
<!--    <div class="navbar-collapse collapse">-->
<!--              <ul class="nav navbar-nav">-->
<!--                <li class="active"><a href="#">Home</a></li>-->
<!--                <li><a href="#">Link</a></li>-->
<!--                <li><a href="#">Link</a></li>-->
<!--                <li>&nbsp;</li>-->
<!--              </ul>-->
<!--              <form class="navbar-form">-->
<!--                <div class="form-group" style="display:inline;">-->
<!--                  <div class="input-group">-->
<!--                    <div class="input-group-btn">-->
<!--                      <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown"><span class="glyphicon glyphicon-chevron-down"></span></button>-->
<!--                      <ul class="dropdown-menu">-->
<!--                        <li><a href="#">Category 1</a></li>-->
<!--                        <li><a href="#">Category 2</a></li>-->
<!--                        <li><a href="#">Category 3</a></li>-->
<!--                        <li><a href="#">Category 4</a></li>-->
<!--                        <li><a href="#">Category 5</a></li>-->
<!--                      </ul>-->
<!--                    </div>-->
<!--                    <input type="text" class="form-control" placeholder="What are searching for?">-->
<!--                    <span class="input-group-addon"><span class="glyphicon glyphicon-search"></span> </span>-->
<!--                  </div>-->
<!--                </div>-->
<!--              </form>-->
<!--    </div>-->
</div>
<div class="modalPop"></div>
<div id="wrap-proyectos">
    <div class="left">
        <label for="">Proyectos</label>
        <select name="proyectos" id="proyectos">
            <option value="">Seleccione</option>
            <option value="104">Ancon</option>
            <option value="34">Alex</option>
            <option value="26">Prueba</option>
        </select>
    </div>
    <div class="right"><div id="currentProject"></div>
    </div>
</div>
<div id="tools">
    <form name="fagd" id="fagd" method="post" action="">
        <fieldset style="padding: 2px;">
            <legend style="text-align: center">
                <span>Agendadas</span>
                <select name="zonal" id="zonal">
                    <option value="LIM">LIMA</option>
                </select>
                <select name="tipo_actu" id="tipo_actu">
                    <option value="Averia">Aver&iacute;a</option>
                    <option value="Provision">Provisi&oacute;n</option>
                </select>
                <span>Estado</span>
                <select name="tipo_estado" id="tipo_estado">
                    <option value="1">Agendado con t&eacute;cnico</option>
                    <option value="8">Agendado sin t&eacute;cnico</option>
                    <option value="21">Cancelado</option>
                    <option value="4,5,6">Devuelto</option>
                    <option value="27">Pre liquidado</option>
                    <option value="3,19">Liquidado</option>
                    <option value="2">Pendiente</option>
                    <option value="9,10,20">T&eacute;cnico en sitio</option>
                </select>
                <input type="checkbox" name="show_traffic" id="show_traffic" value="ok">
                <span>Tr&aacute;fico</span>
            </legend>
            <div class="toolBox">
                <label>EECC</label>
                <div>
                    <select name="empresa" id="empresa">
                        <option value=""> - Seleccione - </option>
                        <?php
                        foreach ($empArray["data"] as $key=>$val) {
                            $id = $val["ideecc"];
                            $nom = $val["eecc"];
                            echo "<option value=\"$id\">$nom</option>";
                        }
                        ?>
                    </select>
                </div>
            </div>
            <div class="toolBox">
                <label>C&eacute;lula</label>
                <div>
                    <select name="celula" id="celula"></select>
                </div>
            </div>
            <div class="toolBox">
                <label>Grupos</label>
                <div>
                    <select name="grupos-filtros" id="grupos-filtros" class="select-grupos">
                        <option value="">Todos</option>
                        <option value="1">Grupo 1</option>
                        <option value="2">Grupo 2</option>
                        <option value="3">Grupo 3</option>
                        <option value="4">Grupo 4</option>
                        <option value="5">Grupo 5</option>
                        <option value="6">Grupo 6</option>
                        <option value="7">Grupo 7</option>
                        <option value="8">Grupo 8</option>
                        <option value="9">Grupo 9</option>
                        <option value="10">Grupo 10</option>
                    </select>
                </div>
            </div>
            <div class="toolBox">
                <label>Fecha agenda</label>
                <div>
                    <input type="text" id="agenda" name="agenda" size="7" readonly="true" value="" placeholder="Desde" />
                    <input type="text" id="to_agenda" name="to_agenda" size="7" readonly="true" value="" placeholder="Hasta" />
                </div>
            </div>
            <div class="toolBox">
                <label>&nbsp;</label>
                <div>
                    <input type="button" id="buscar_agenda" name="buscar_agenda" value="Buscar" />
                </div>
            </div>
            <div class="toolBox">
                <label>&nbsp;</label>
                <div>
                    <input type="reset" id="reset_agenda" name="reset_agenda" value="Reset" />
                </div>
            </div>
        </fieldset>
    </form>
</div>
<div id="tools_tmp">
    <form name="ftmp" id="ftmp" method="post" action="">
        <fieldset style="padding: 2px;">
            <legend style="text-align: center">
                <span>No asignadas</span>

                <span>C&oacute;digo</span>
                <input type="text" id="tipo_codigo" name="tipo_codigo" size="10">
                <input type="submit" id="tipo_buscar" name="tipo_buscar" value="Buscar" />
                <input type="button" id="tipo_limpiar" name="tipo_limpiar" value="Limpiar Tmp" class="clear_button" />
                <input type="button" id="tipo_limpiar_todo" name="tipo_limpiar_todo" value="Limpiar Todo" class="clear_button" />

                <input type="button" id="geo_plan" name="geo_plan" value="!!!" />
            </legend>
            <div class="toolBox">
                <label>Quiebre</label>
                <div>
                    <select name="tipo_quiebre" id="tipo_quiebre">
                        <?php
                        foreach ($quiebreArray["data"] as $key=>$val) {
                            $nom = $val["apocope"];
                            echo "<option value=\"$nom\">$nom</option>";
                        }
                        ?>
                    </select>
                </div>
            </div>
            <div class="toolBox">
                <label>Producto</label>
                <div>
                    <select name="tipo_producto" id="tipo_producto">
                        <option value="MOVISTAR UNO">Movistar Uno</option>
                        <option value="DUO HFC">Duo HFC</option>
                        <option value="_negative">Sin marca</option>
                    </select>
                </div>
             <?php
            foreach ($empArray["data"] as $key=>$val) {
                $id = $val["ideecc"];
                $nom = $val["eecc"];
                echo "<option value=\"$id\">$nom</option>";
            }
            ?>
                        </select>
                    </div>
                </div>
                -->
            <div class="toolBox">
                <label>Servicio</label>
                <div>
                    <select name="tipo_servicio" id="tipo_servicio">
                        <option value="bas">BASICA</option>
                        <option value="adsl">ADSL</option>
                        <option value="catv">CATV</option>
                    </select>
                </div>
            </div>
            <div class="toolBox">
                <label>Con XY / Sin XY</label>
                <div>
                    <div name="n_tmp" id="n_tmp">00/00 (00)</div>
                </div>
            </div>
        </fieldset>
    </form>
</div>
<div id="map-tec">
    <div class="MostrarGrupo">
        <label>Grupos</label>
        <select name="grupos-detalle" id="grupos-detalle" class="select-grupos">
            <option value="">Mostrar Todos</option>
            <option value="1">Grupo 1</option>
            <option value="2">Grupo 2</option>
            <option value="3">Grupo 3</option>
            <option value="4">Grupo 4</option>
            <option value="5">Grupo 5</option>
            <option value="6">Grupo 6</option>
            <option value="7">Grupo 7</option>
            <option value="8">Grupo 8</option>
            <option value="9">Grupo 9</option>
            <option value="10">Grupo 10</option>
        </select>
  -
        <span><a href="#" class="grupo-editar">Editar grupo</a></span> -
        <span class="filtro-cerrar">x cerrar</span>

    </div>
    <h4 class="show_hide_tec">
        <div class="showAllTec">
            <input type="checkbox" name="show_tec" id="show_tec" />
            <label for="show_tec">Mostrar t&eacute;cnicos / agendas</label>
        </div>
        <br>
        <input type="checkbox" name="show_pdt" id="show_pdt" />
        <label for="show_pdt"> Pendiente / En curso</label>
    </h4>
    <div id="tec-list"></div>
</div>
<div id="map-canvas"></div>
<div class="container-fluid" id="main">
    <div class="row">
        <div class="col-xs-12"><!--map-canvas will be postioned here--></div>
    </div>
</div>
<!--<script src="../public/rutaTecnico/js/jquery.min.js"></script>-->
<script src="../public/rutaTecnico/js/bootstrap.min.js"></script>
<script src="../public/rutaTecnico/js/visor.gps.js"></script>
</body>
</html>

