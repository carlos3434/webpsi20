<!DOCTYPE html>
<html>
    <head>
        <title>Simple Map</title>
        <meta name="viewport" content="initial-scale=1.0, user-scalable=no">
        <meta charset="utf-8">
        
        <link rel="stylesheet" href="//code.jquery.com/ui/1.11.1/themes/smoothness/jquery-ui.css">
        <link rel="stylesheet" href="css/officetrack.css">
        <link type="text/css" href='css/jquery.multiselect.css' rel="Stylesheet" />
        
        <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&libraries=drawing"></script>
        <script src="http://code.jquery.com/jquery-1.11.1.min.js"></script>
        <script src="http://code.jquery.com/ui/1.11.1/jquery-ui.js"></script>
        <script src="js/ruta.config.js?t=<?php echo time();?>"></script>
        <script src="js/officetrack.js?t=<?php echo time();?>"></script>
        <script src="js/CelulaGruposTecnicos.js?t=<?php echo time();?>"></script>
        <script src="js/utils.js?t=<?php echo time();?>"></script>
        
        <script src="js/jquery.multiselect.js"></script>
    </head>
    <body>
        <div class="modalPop"></div>
        <div id="tools">
            <form name="fagd" id="fagd" method="post" action="">
            <fieldset style="padding: 2px;">
                <legend style="text-align: center">
                    <span>Agendadas</span>
                    <select name="zonal" id="zonal">
                        <option value="LIM">LIMA</option>
                    </select>
                    <select name="tipo_actu" id="tipo_actu">
                        <option value="Averia">Aver&iacute;a</option>
                        <option value="Provision">Provisi&oacute;n</option>
                    </select>
                    <span>Estado</span>
                    <select name="tipo_estado" id="tipo_estado">
                        <option value="1">Agendado con t&eacute;cnico</option>
                        <option value="8">Agendado sin t&eacute;cnico</option>
                        <option value="21">Cancelado</option>
                        <option value="4,5,6">Devuelto</option>
                        <option value="3,19">Liquidado</option>
                        <option value="2">Pendiente</option>
                        <option value="9,10,20">T&eacute;cnico en sitio</option>
                        <option value="28">Planificado</option>
                    </select>
                    <input type="checkbox" name="show_traffic" id="show_traffic" value="ok">
                    <span>Tr&aacute;fico</span>
                </legend>
                <div class="toolBox">
                    <label>EECC</label>
                    <div>
                        <select name="empresa" id="empresa">
                            <option value=""> - Seleccione - </option>
                            <?php
                            foreach ($empArray["data"] as $key=>$val) {
                                $id = $val["ideecc"];
                                $nom = $val["eecc"];
                                echo "<option value=\"$id\">$nom</option>";
                            }
                            ?>
                        </select>
                    </div>
                </div>
                <div class="toolBox">
                    <label>C&eacute;lula</label>
                    <div>
                        <select name="celula" id="celula"></select>
                    </div>
                </div>
                <div class="toolBox">
                    <label>Grupos</label>
                    <div>
                        <select name="grupos-filtros" id="grupos-filtros" class="select-grupos">
                            <option value="">Todos</option>
                            <option value="1">Grupo 1</option>
                            <option value="2">Grupo 2</option>
                            <option value="3">Grupo 3</option>
                            <option value="4">Grupo 4</option>
                            <option value="5">Grupo 5</option>
                            <option value="6">Grupo 6</option>
                            <option value="7">Grupo 7</option>
                            <option value="8">Grupo 8</option>
                            <option value="9">Grupo 9</option>
                            <option value="10">Grupo 10</option>
                        </select>
                    </div>
                </div>
                <div class="toolBox">
                    <label>Fecha agenda</label>
                    <div>
                        <input type="text" id="agenda" name="agenda" size="7" readonly="true" value="" placeholder="Desde" />
                        <input type="text" id="to_agenda" name="to_agenda" size="7" readonly="true" value="" placeholder="Hasta" />
                    </div>
                </div>
                <div class="toolBox">
                    <label>&nbsp;</label>
                    <div>
                        <input type="button" id="buscar_agenda" name="buscar_agenda" value="Buscar" />
                    </div>
                </div>
                <div class="toolBox">
                    <label>&nbsp;</label>
                    <div>
                        <input type="reset" id="reset_agenda" name="reset_agenda" value="Reset" />
                    </div>
                </div>
            </fieldset>
            </form>
        </div>
        <div id="tools_tmp">
            <form name="ftmp" id="ftmp" method="post" action="">
            <fieldset style="padding: 2px;">
                <legend style="text-align: center">
                    <span>No asignadas</span>
                    
                    <span>C&oacute;digo</span>
                    <input type="text" id="tipo_codigo" name="tipo_codigo" size="10">
                    <input type="submit" id="tipo_buscar" name="tipo_buscar" value="Buscar" />
                    <input type="button" id="tipo_limpiar" name="tipo_limpiar" value="Limpiar Tmp" class="clear_button" />
                    <input type="button" id="tipo_limpiar_todo" name="tipo_limpiar_todo" value="Limpiar Todo" class="clear_button" />
                    
                    <input type="button" id="geo_plan" name="geo_plan" value="!!!" />
                </legend>
                <!--
                <div class="toolBox">
                    <label>Zonal</label>
                    <div>
                        <select name="tipo_zonal" id="tipo_zonal">
                            <option value="LIM">LIMA</option>
                        </select>
                    </div>
                </div>
                -->
                <div class="toolBox">
                    <label>Quiebre</label>
                    <div>
                        <select name="tipo_quiebre" id="tipo_quiebre">
                            <?php
                            foreach ($quiebreArray["data"] as $key=>$val) {
                                $nom = $val["apocope"];
                                echo "<option value=\"$nom\">$nom</option>";
                            }
                            ?>
                        </select>
                    </div>
                </div>
                <div class="toolBox">
                    <label>Producto</label>
                    <div>
                        <select name="tipo_producto" id="tipo_producto">
                            <option value="MOVISTAR UNO">Movistar Uno</option>
                            <option value="DUO HFC">Duo HFC</option>
                            <option value="_negative">Sin marca</option>
                        </select>
                    </div>
                </div>
                <!--
                <div class="toolBox">
                    <label>EECC</label>
                    <div>
                        <select name="tipo_empresa" id="tipo_empresa">
                            <?php
                            foreach ($empArray["data"] as $key=>$val) {
                                $id = $val["ideecc"];
                                $nom = $val["eecc"];
                                echo "<option value=\"$id\">$nom</option>";
                            }
                            ?>
                        </select>
                    </div>
                </div>
                -->
                <div class="toolBox">
                    <label>Servicio</label>
                    <div>
                        <select name="tipo_servicio" id="tipo_servicio">
                            <option value="bas">BASICA</option>
                            <option value="adsl">ADSL</option>
                            <option value="catv">CATV</option>
                        </select>
                    </div>
                </div>
                <div class="toolBox">
                    <label>Con XY / Sin XY</label>
                    <div>
                        <div name="n_tmp" id="n_tmp">00/00 (00)</div>
                    </div>
                </div>
                <!--
                <div class="toolBox">
                    <label>MDF/NODO</label>
                    <div>
                        <select name="tipo_mdf" id="tipo_mdf">
                            <option value=""></option>
                        </select>
                    </div>
                </div>
                
                <div class="toolBox">
                    <label>&nbsp;</label>
                    <div>
                        <input type="submit" id="tipo_buscar" name="tipo_buscar" value="Buscar" />
                        <input type="button" id="tipo_limpiar" name="tipo_limpiar" value="Limpiar" />                        
                    </div>
                </div>
                
                <div class="toolBox">5</div>
                <div class="toolBox">5</div>
                -->                
            </fieldset>
            </form>
        </div>
        <div id="map-tec">
            <div class="MostrarGrupo">
                <span><a href="#" class="grupo-editar">Editar grupo</a></span> -


            </div>
            <h4 class="show_hide_tec">
                <div class="showAllTec">
                    <input type="checkbox" name="show_tec" id="show_tec" />
                    Mostrar t&eacute;cnicos / agendas
                </div>
                <br>
                <input type="checkbox" name="show_pdt" id="show_pdt" />
                Pendiente / En curso
                <br>
                <input type="checkbox" name="show_coo" id="show_coo" />
                Coordinado
            </h4>
            <div id="tec-list"></div>            
        </div>
        <div id="map-canvas"></div>
    </body>
</html>

