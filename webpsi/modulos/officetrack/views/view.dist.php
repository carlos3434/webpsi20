<!DOCTYPE html>
<html>
    <head>
        <title>Google Developers</title>

        <link rel="stylesheet" type="text/css" href="https://google-developers.appspot.com/_static/0d6c89115d/css/screen-docs.css" />
        <link rel="stylesheet" href="http://www.google.com/cse/style/look/default.css" type="text/css" />
        <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Open+Sans:300,400" type="text/css">
        <script src="https://google-developers.appspot.com/_static/0d6c89115d/js/prettify-bundle.js"></script>

        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.6.1/jquery.min.js"></script>
        <script id="jqueryui" src="//ajax.googleapis.com/ajax/libs/jqueryui/1.9.2/jquery-ui.min.js" defer async></script>
        <script src="http://www.google.com/jsapi?key=AIzaSyCZfHRnq7tigC-COeQRmoa9Cxr0vbrK6xw"></script>
        <!--[if lt IE 9]>
        <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->
        <script src="https://google-developers.appspot.com/_static/0d6c89115d/js/framebox.js"></script>
    </head>
    <body class="docs slim framebox_body">
        ï»¿
        <script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?sensor=false"></script>
        <script type="text/javascript">

            var origins = [
                new google.maps.LatLng(12.075387, -77.019612),
                new google.maps.LatLng(12.075387, -77.019612),
                new google.maps.LatLng(-12.113405, -77.037894)
            ];

            var destinations = [
                new google.maps.LatLng(-12.123308, -76.996523),
                new google.maps.LatLng(-12.076982, -77.034718),
                new google.maps.LatLng(-12.080423, -76.981760)
            ];

            var query = {
                origins: origins,
                destinations: destinations,
                travelMode: google.maps.TravelMode.DRIVING,
                unitSystem: google.maps.UnitSystem.METRIC
            };

            var map, dms;
            var dirService, dirRenderer;
            var highlightedCell;
            var routeQuery;
            var bounds;
            var panning = false;

            function initialize() {
                var mapOptions = {
                    zoom: 12,
                    center: new google.maps.LatLng(-12.092508, -77.013689),
                    mapTypeId: google.maps.MapTypeId.ROADMAP
                }
                map = new google.maps.Map(document.getElementById("map"), mapOptions);
                createTable();

                for (var i = 0; i < origins.length; i++) {
                    origins[i] += ', PE';
                }

                for (var j = 0; j < destinations.length; j++) {
                    destinations[j] += ', PE';
                }

                dms = new google.maps.DistanceMatrixService();

                dirService = new google.maps.DirectionsService();
                dirRenderer = new google.maps.DirectionsRenderer({preserveViewport: true});
                dirRenderer.setMap(map);

                google.maps.event.addListener(map, 'idle', function() {
                    if (panning) {
                        map.fitBounds(bounds);
                        panning = false;
                    }
                });
                updateMatrix();
            }

            function updateMatrix() {
                dms.getDistanceMatrix(query, function(response, status) {
                    if (status == 'OK') {
                        populateTable(response.rows);
                    }
                });
            }

            function createTable() {
                var table = document.getElementById('matrix');
                var tr = addRow(table);
                var t = addElement(tr);
                t.style.borderLeft = 0;
                for (var j = 0; j < destinations.length; j++) {
                    var td = addElement(tr);
                    td.setAttribute('class', 'destination');
                    td.appendChild(document.createTextNode(destinations[j]));
                }

                for (var i = 0; i < origins.length; i++) {
                    var tr = addRow(table);
                    var td = addElement(tr);
                    td.setAttribute('class', 'origin');
                    td.appendChild(document.createTextNode(origins[i]));
                    for (var j = 0; j < destinations.length; j++) {
                        var td = addElement(tr, 'element-' + i + '-' + j);
                        td.onmouseover = getRouteFunction(i, j);
                        td.onclick = getRouteFunction(i, j);
                    }
                }
            }

            function populateTable(rows) {
                for (var i = 0; i < rows.length; i++) {
                    for (var j = 0; j < rows[i].elements.length; j++) {
                        var distance = rows[i].elements[j].distance.text;
                        var duration = rows[i].elements[j].duration.text;
                        var td = document.getElementById('element-' + i + '-' + j);
                        td.innerHTML = distance + '<br>' + duration;
                    }
                }
            }

            function getRouteFunction(i, j) {
                return function() {
                    routeQuery = {
                        origin: origins[i],
                        destination: destinations[j],
                        travelMode: query.travelMode,
                        unitSystem: query.unitSystem,
                    };
                    if (highlightedCell) {
                        highlightedCell.style.backgroundColor = '#ffffff';
                    }
                    highlightedCell = document.getElementById('element-' + i + '-' + j);
                    highlightedCell.style.backgroundColor = '#e0ffff';
                    showRoute();
                }
            }

            function showRoute() {
                dirService.route(routeQuery, function(result, status) {
                    if (status == google.maps.DirectionsStatus.OK) {
                        dirRenderer.setDirections(result);
                        bounds = new google.maps.LatLngBounds();
                        bounds.extend(result.routes[0].overview_path[0]);
                        var k = result.routes[0].overview_path.length;
                        bounds.extend(result.routes[0].overview_path[k - 1]);
                        panning = true;
                        map.panTo(bounds.getCenter());
                    }
                });
            }

            function updateMode() {
                switch (document.getElementById('mode').value) {
                    case 'driving':
                        query.travelMode = google.maps.TravelMode.DRIVING;
                        break;
                    case 'walking':
                        query.travelMode = google.maps.TravelMode.WALKING;
                        break;
                }
                updateMatrix();
                if (routeQuery) {
                    routeQuery.travelMode = query.travelMode;
                    showRoute();
                }
            }

            function updateUnits() {
                switch (document.getElementById('units').value) {
                    case 'km':
                        query.unitSystem = google.maps.UnitSystem.METRIC;
                        break;
                    case 'mi':
                        query.unitSystem = google.maps.UnitSystem.IMPERIAL;
                        break;
                }
                updateMatrix();
            }

            function addRow(table) {
                var tr = document.createElement('tr');
                table.appendChild(tr);
                return tr;
            }

            function addElement(tr, id) {
                var td = document.createElement('td');
                if (id) {
                    td.setAttribute('id', id);
                }
                tr.appendChild(td);
                return td;
            }

            google.maps.event.addDomListener(window, 'load', initialize);
        </script>
        <style type="text/css">
            body {
                font-family: sans-serif;
                text-align: center;
                margin: 0px;
                padding: 0px;
            }

            #container {
                position: relative;
                width: 800px;
                margin: auto;
            }

            #map {
                width: 800px;
                height: 300px;
            }

            #matrix {
                font-size: 10px;
                border: 0 !important;
            }

            #matrix td {
                border-right: 0 !important;
            }

            #controls {
                position: absolute;
                top: 0;
                left: 0;
                text-align: right;
            }

            .origin,.destination {
                font-weight: bold;
                text-align: center;
                background-color: #e0ffe0;
            }

            .origin {
                border-left: 0 !important;
                border-right: 1px solid #BBB !important;;
            }

            td {
                border: 1px solid grey;
                width: 80px !important;
                padding: 2px !important;
                cursor: default;
                background-color: #ffffff;
            }

        </style>
        <div id="container">
            <div id="controls">
                <select id="mode" onChange="updateMode()">
                    <option value="driving">En coche</option>
                    <option value="walking" selected="selected">A pie</option>
                </select><br/>
                <select id="units" onChange="updateUnits()">
                    <option value="km">Kilometros</option>
                    <option value="mi" selected="selected">Millas</option>
                </select>
            </div>
            <div id="map"></div>
            <table id="matrix"></table>
        </div>

        <script>

            devsite.github.Link.convertAnchors();

            window.prettyPrint();


        </script>
    </body>
</html>
