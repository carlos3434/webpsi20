<!DOCTYPE html>
<html>
    <head>
        <title>Planificaci&oacute;n</title>
        <meta name="viewport" content="initial-scale=1.0, user-scalable=no">
        <meta charset="utf-8">        
        
        <?php include ("../../includes.php") ?>
        
        <link rel="stylesheet" href="//code.jquery.com/ui/1.11.1/themes/smoothness/jquery-ui.css">
        <link rel="stylesheet" href="css/officetrack.css">
        <link type="text/css" href='css/jquery.multiselect.css' rel="Stylesheet" />
        
        <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&libraries=drawing"></script>
        <script src="http://code.jquery.com/jquery-1.11.1.min.js"></script>
        <script src="http://code.jquery.com/ui/1.11.1/jquery-ui.js"></script>
        <script src="js/plan.config.js?t=<?php echo time();?>"></script>
        <script src="js/planificacion.js?t=<?php echo time();?>"></script>
        <script src="js/utils.js?t=<?php echo time();?>"></script>
        <script src="js/plan.js?t=<?php echo time();?>"></script>
        
        <script src="js/jquery.multiselect.js"></script>
    </head>
    <body>
        <?php echo pintar_cabecera(); ?>
        
        <div class="modalPop"></div>
        
        <div id="tabs" style="height: 16%">
            <ul>
                <li><a href="#tabs-1">B&uacute;squeda</a></li>
                <li><a href="#tabs-2">Por archivo</a></li>
                <!--<li><a href="#tabs-3">Aenean lacinia</a></li>-->
            </ul>
            <div id="tabs-1" style="height: 18%">
                <div id="tools">
                    <form name="fagd" id="fagd" method="post" action="">
                    <fieldset style="padding: 2px;">
                        <legend style="text-align: center">
                            <span>Agendadas</span>
                            <select name="zonal" id="zonal">
                                <option value="LIM">LIMA</option>
                            </select>
                            <select name="tipo_actu" id="tipo_actu">
                                <option value="Averia">Aver&iacute;a</option>
                                <option value="Provision">Provisi&oacute;n</option>
                            </select>
                            <span>Estado</span>
                            <select name="tipo_estado" id="tipo_estado">
                                <option value="1">Agendado con t&eacute;cnico</option>
                                <option value="8">Agendado sin t&eacute;cnico</option>
                                <option value="21">Cancelado</option>
                                <option value="4,5,6">Devuelto</option>
                                <option value="3,19">Liquidado</option>
                                <option value="2">Pendiente</option>
                                <option value="9,10,20">T&eacute;cnico en sitio</option>
                            </select>
                            <input type="checkbox" name="show_traffic" id="show_traffic" value="ok">
                            <span>Tr&aacute;fico</span>
                        </legend>
                        <div class="toolBox">
                            <label>EECC</label>
                            <div>
                                <select name="empresa" id="empresa">
                                    <option value=""> - Seleccione - </option>
                                    <?php
                                    foreach ($empArray["data"] as $key=>$val) {
                                        $id = $val["ideecc"];
                                        $nom = $val["eecc"];
                                        echo "<option value=\"$id\">$nom</option>";
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>
                        <div class="toolBox">
                            <label>C&eacute;lula</label>
                            <div>
                                <select name="celula" id="celula"></select>
                            </div>
                        </div>
                        <div class="toolBox">
                            <label>Fecha agenda</label>
                            <div>
                                <input type="text" id="agenda" name="agenda" size="7" readonly="true" value="" placeholder="Desde" />
                                <input type="text" id="to_agenda" name="to_agenda" size="7" readonly="true" value="" placeholder="Hasta" />
                            </div>
                        </div>
                        <div class="toolBox">
                            <label>&nbsp;</label>
                            <div>
                                <input type="button" id="buscar_agenda" name="buscar_agenda" value="Buscar" />
                            </div>
                        </div>
                        <div class="toolBox">
                            <label>&nbsp;</label>
                            <div>
                                <input type="reset" id="reset_agenda" name="reset_agenda" value="Reset" />
                            </div>
                        </div>
                    </fieldset>
                    </form>
                </div>
                <div id="tools_tmp">
                    <form name="ftmp" id="ftmp" method="post" action="">
                    <fieldset style="padding: 2px;">
                        <legend style="text-align: center">
                            <span>No asignadas</span>

                            <span>C&oacute;digo</span>
                            <input type="text" id="tipo_codigo" name="tipo_codigo" size="10">
                            <input type="submit" id="tipo_buscar" name="tipo_buscar" value="Buscar" />
                            <input type="button" id="tipo_limpiar" name="tipo_limpiar" value="Limpiar Tmp" class="clear_button" />
                            <input type="button" id="tipo_limpiar_todo" name="tipo_limpiar_todo" value="Limpiar Todo" class="clear_button" />

                            <input type="button" id="geo_plan" name="geo_plan" value="!!!" />
                        </legend>
                        <div class="toolBox">
                            <label>Quiebre</label>
                            <div>
                                <select name="tipo_quiebre" id="tipo_quiebre">
                                    <?php
                                    foreach ($quiebreArray["data"] as $key=>$val) {
                                        $nom = $val["apocope"];
                                        echo "<option value=\"$nom\">$nom</option>";
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>
                        <div class="toolBox">
                            <label>Producto</label>
                            <div>
                                <select name="tipo_producto" id="tipo_producto">
                                    <option value="MOVISTAR UNO">Movistar Uno</option>
                                    <option value="DUO HFC">Duo HFC</option>
                                    <option value="_negative">Sin marca</option>
                                </select>
                            </div>
                        </div>
                        <div class="toolBox">
                            <label>Servicio</label>
                            <div>
                                <select name="tipo_servicio" id="tipo_servicio">
                                    <option value="bas">BASICA</option>
                                    <option value="adsl">ADSL</option>
                                    <option value="catv">CATV</option>
                                </select>
                            </div>
                        </div>
                        <div class="toolBox">
                            <label>Con XY / Sin XY</label>
                            <div>
                                <div name="n_tmp" id="n_tmp">00/00 (00)</div>
                            </div>
                        </div>    
                    </fieldset>
                    </form>
                </div>
            </div>
            <div id="tabs-2">
                <div class="toolBox">
                    <label>EECC</label>
                    <div>
                        <select name="empresa" id="empresa" class="empresa">
                            <option value=""> - Seleccione - </option>
                            <?php
                            foreach ($empArray["data"] as $key=>$val) {
                                $id = $val["ideecc"];
                                $nom = $val["eecc"];
                                echo "<option value=\"$id\">$nom</option>";
                            }
                            ?>
                        </select>
                    </div>
                </div>
                <div class="toolBox">
                    <label>C&eacute;lula</label>
                    <div>
                        <select name="celula" id="celula" class="celula"></select>
                    </div>
                </div>
                <div class="toolBox">
                    <label>Archivo</label>
                    <div>
                        <input type="file" name="file_tmp" id="file_tmp" />
                    </div>
                </div>
                <div class="toolBox">
                    <label>&nbsp;</label>
                    <div>
                        <input type="button" id="buscar_tmp_file" name="buscar_tmp_file" value="Buscar" />
                    </div>
                </div>
                <div class="toolBox">
                    <label>Asignadas</label>
                    <div>
                        <div class="nro_asg_file"></div>
                        <input type="checkbox" name="show_today" id="show_today" checked="true" onclick="showHideActuPlan('today', 'show_today')" /> Hoy
                        <input type="checkbox" name="show_notoday" id="show_notoday" checked="true" onclick="showHideActuPlan('notoday', 'show_notoday')" /> !Hoy
                    </div>
                </div>
                <div class="toolBox">
                    <label>Temporales</label>
                    <div>
                        <div class="nro_tmp_file"></div>
                        <input type="checkbox" name="show_tmp" id="show_tmp" checked="true" onclick="showHideActuPlan('tmp', 'show_tmp')" /> Mostrar
                    </div>                    
                </div>
                <div class="toolBox">
                    <label>No encontradas</label>
                    <div class="no_tmp_file" style="overflow: auto; height: 40px">
                        <a href="" class="downNotFound">Download</a>
                    </div>
                </div>
                <div class="toolBox">
                    <label>Coordinadas</label>
                    <div class="no_tmp_file" style="overflow: auto; height: 40px">
                        <input type="checkbox" class="show_coo" />
                    </div>
                </div>
            </div>
            <!--
            <div id="tabs-3">
                <p><strong>Click this tab again to close the content pane.</strong></p>
                <p>Duis cursus. Maecenas ligula eros, blandit nec, pharetra at, semper at, magna. Nullam ac lacus. Nulla facilisi. Praesent viverra justo vitae neque. Praesent blandit adipiscing velit. Suspendisse potenti. Donec mattis, pede vel pharetra blandit, magna ligula faucibus eros, id euismod lacus dolor eget odio. Nam scelerisque. Donec non libero sed nulla mattis commodo. Ut sagittis. Donec nisi lectus, feugiat porttitor, tempor ac, tempor vitae, pede. Aenean vehicula velit eu tellus interdum rutrum. Maecenas commodo. Pellentesque nec elit. Fusce in lacus. Vivamus a libero vitae lectus hendrerit hendrerit.</p>
            </div>
            -->
        </div>
        
        
        <div id="map-tec">
            <h4 class="show_hide_tec">
                <input type="checkbox" name="show_tec" id="show_tec" />
                Mostrar t&eacute;cnicos / agendas
                <br>
                <input type="checkbox" name="show_pdt" id="show_pdt" />
                Pendiente / En curso
                <br>
                <input type="checkbox" name="show_coo" id="show_coo" />
                Coordinado
            </h4>
            <div id="tec-list"></div>            
        </div>
        <div id="map-canvas"></div>
        <div id="dialog" title="Planificaci&oacute;n"></div>
    </body>
</html>

