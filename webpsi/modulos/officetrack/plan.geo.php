<?php
//session_start();
require_once("../../cabecera.php");
require_once '../../clases/class.Conexion.php';
require_once './clases/class.Location.php';
require_once './clases/class.Empresa.php';
require_once './clases/class.Usuario_eecc.php';
require_once './clases/class.TmpAveria.php';
require_once './clases/class.TbQuiebre.php';

$Conexion = new Conexion();
$Location = new Location();
$Empresa = new Empresa();
$Usuario_eecc =  new Usuario_eecc();
$getTmpAveria = new TmpAveria();
$TbQuiebre = new TbQuiebre();

//Obj DB
$db = $Conexion->conectarPDO();

//ID usuario
$idUser = $_SESSION["exp_user"]["id"];

//Lista empresas
//$empArray = $Empresa->getEmpresa($db, "", "1");
$empArray = $Usuario_eecc->getEeccUser($db, $idUser);

$fecha = date("Y/m/d");
$xy = $Location->getLocations(
        $db, 
        $codArray = array(), 
        $numArray = array(),
        $fecha
    );

//Lista quiebres averia
$quiebreArray = $TbQuiebre->getAll($db);

if ($xy["estado"] === true) {
    $coord = json_encode($xy["data"]);
} else {
    die("Error al recuperar localizaciones.");
}

$gmaps["draw"] = true;

include_once './views/view.plan.php';