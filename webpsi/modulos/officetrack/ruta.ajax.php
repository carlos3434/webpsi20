<?php
session_start();
require_once '../../clases/class.Conexion.php';
require_once './clases/class.Location.php';
require_once './clases/class.Celula.php';
require_once './clases/class.Tecnico.php';
require_once './clases/class.OtUtils.php';
require_once './clases/class.Paso_detalle.php';
require_once './clases/class.TmpAveria.php';
require_once './clases/class.TmpProvision.php';
require_once './clases/class.HorarioPlan.php';
require_once '../georeferencia/clases/class.GeoTap.php';
require_once '../georeferencia/clases/class.GeoTerminald.php';
require_once '../georeferencia/clases/class.GeoTerminalf.php';
require_once './clases/class.Gestion_geo.php';
require_once '../georeferencia/clases/class.GeoUtils.php';
require_once '../historico/clases/zonales.php';
require_once '../historico/clases/gestionCriticos.php';

$Conexion       = new Conexion();
$Location       = new Location();
$Celula         = new Celula();
$Tecnico        = new Tecnico();
$OtUtils        = new OtUtils();
$PasoDetalle    = new Paso_detalle();
$GeoTap         = new GeoTap();
$GepTerminald   = new GeoTerminald();
$GepTerminalf   = new GeoTerminalf();
$GestionGeo     = new Gestion_geo();
$GeoUtils       = new GeoUtils();
$Zonales        = new Zonales();
$gestionCriticos= new gestionCriticos();
$HorarioPlan    = new HorarioPlan();

$user_id = $_SESSION['exp_user']['id'];

$db = $Conexion->conectarPDO();

if ( isset( $_POST["codePath"] ) ) 
{
    $code = $_POST["codePath"];
    $date = $_POST["pdate"];
    
    $fecha = substr($date, 6, 4)
             . "-" 
             . substr($date, 3, 2)
             . "-" 
             . substr($date, 0, 2);
    
    $fromTime = $fecha . " 07:00:00";
    $toTime = $fecha . " 22:00:00";
    $path = $Location->getPath($db, $fecha, $code, $fromTime, $toTime);
    
    echo json_encode($path);
}

if ( isset( $_POST["getCelulaEmp"] ) )
{
    $celula = $Celula->getCelulaEmp($db, $_POST["getCelulaEmp"]);
    
    if ( $celula["estado"] and !empty($celula["data"]) )
    {
        echo json_encode($celula["data"]);
    }
}

if ( isset( $_POST["showLastLiq"] ) )
{
   //MOSTRAR DATA
    $carnet = $_POST["showLastLiq"];
    $tarea = $PasoDetalle->ultimaLiquidada($db,$carnet);

    //PREGUNTAR SI ESTA ES UNA AVERIA O PROVISION
    $tipo_actividad = $tarea["data"]["tipo_actividad"];
    $task_id = $tarea["data"]["task_id"];
    $metodo = "getAgenda";
    if(!empty($tipo_actividad)){
        $call_method = $metodo.$tipo_actividad;
        //OBTENER LA AGENDA DE ESA TAREA
        $agenda =     $Location->$call_method( $db, "", "", "", "", "", "",$task_id);
        if (!empty($agenda["data"])){
            $data["agenda"] = $OtUtils->getActuCoord(  $agenda, $db, $GeoTap, $GepTerminald, $GepTerminalf   );
        }
        //MANDAR A IMPRIMIR EN MAPA
        print json_encode($data["agenda"][0]);
        exit();
    }else{
        print json_encode(array());
        exit();
    }



}


if ( isset( $_POST["getCelulaTec"] ) )
{
    //Iconos
    $icons = $OtUtils->iconArray();
    $iconArray = array();    
    
    $fecha = "";
    $data = explode("|", $_POST["getCelulaTec"]);
    $idCel = $data[0];
    $idEmp = $data[1];
    
    if (trim($data[2]) !== "")
    {
        $fecha = substr($data[2], 6, 4)
                 . "-" 
                 . substr($data[2], 3, 2)
                 . "-" 
                 . substr($data[2], 0, 2);
    }
    $to_fecha = $fecha;
    
    if (trim($data[3]) !== "")
    {
        $to_fecha = substr($data[3], 6, 4)
             . "-" 
             . substr($data[3], 3, 2)
             . "-" 
             . substr($data[3], 0, 2);
    }    
    
    $tecnico = $Tecnico->getTecnico($db, $idEmp, "", "", $idCel);
    
    $method = "getAgenda" . $_POST["tipo"];
    $estado = $_POST["estado"];
    
    if ( $tecnico["estado"] and !empty($tecnico["data"]) )
    {
        //Obtener localizaciones
        $tecArray = array();
        
        foreach ($tecnico["data"] as $key=>$val) {
            $tecArray[] = $val["carnet"];
            
            //Iconos
            shuffle($icons);
            $iconArray[$val["carnet"]] = array_pop($icons);
        }

        $fecha2=date("Y-m-d");

        $xy = $Location->getLocations(
            $db, 
            $tecArray, 
            $numArray = array(),
            str_replace("-", "/", $fecha2)
        );
        
        //Obtener agendas de toda la celula
        $agenda = $Location->$method(
                $db, $idEmp, $idCel, $fecha, $to_fecha, "", $estado);
        //Obtener XY de cada orden
        if ($agenda["estado"]===true and !empty($agenda["data"])) 
        {
            $dataTec["agenda"] = getActuCoord(
                    $agenda, $db, $GeoTap, $GepTerminald, $GepTerminalf
                    );
        }
        
        $dataTec["alert"] = "ok";
        $dataTec["coord"] = $xy["data"];
        $dataTec["names"] = $tecnico["data"];
        $dataTec["icons"] = $iconArray;
        
    } else {
        $dataTec["alert"] = "ko";        
    }
    
    //Retornar data
    echo json_encode($dataTec);
}

function getActuCoord($agenda, $db, $GeoTap, $GepTerminald, $GepTerminalf){
    foreach ($agenda["data"] as $key=>$val) {
        $x = "";
        $y = "";
        
        /**
         * Primera condicion: Gestionadas (-catv-)
         * Segunda condicion: Temporales (_catv)
         */
        if (
                strpos($val["tipoactu"], "-catv-") !== false  or 
                strpos($val["tipoactu"], "_catv") !== false 
            )
        {
            //Tipo CATV -> fftt obtener xy del tap
            $ffttArray = explode("|", $val["fftt"]);
            
            /**
             * las tablas temporales retornan 5 datos
             */
            $e_troba = "";
            $e_amplificador = "";
            $e_tap = "";
            
            if (isset($ffttArray[2])) {
                $e_troba = $ffttArray[2];
            }            
            if (isset($ffttArray[3])) {
                $e_amplificador = $ffttArray[3];
            }            
            if (isset($ffttArray[4])) {
                $e_tap = $ffttArray[4];
            }            
            
            if ( count($ffttArray) === 5 )
            {
                $e_troba = $ffttArray[1];
                $e_amplificador = $ffttArray[2];
                $e_tap = $ffttArray[3];
            }
            
            if (ctype_digit($e_amplificador) ) {
                $e_amplificador = intval($e_amplificador);
            }
            if (ctype_digit($e_tap) ) {
                $e_tap = intval($e_tap);
            }
            
            $tap = $GeoTap->listar(
                    $db, 
                    array(
                            "LIM",
                            $ffttArray[0], 
                            $e_troba,
                            $e_amplificador,
                            $e_tap
                        )
                    );
            
            if ( !empty( $tap ) )
            {
                $x = $tap[0]["coord_x"];
                $y = $tap[0]["coord_y"];
            }
            $agenda["data"][$key]["x"] = $x;
            $agenda["data"][$key]["y"] = $y;
        } else {
            //Tipo BASICA / ADSL -> llave obtener xy del terminal
            $ffttArray = explode("|", $val["fftt"]);
            
            /**
             * Algunas llaves tienen 4 datos
             * MDF|ARMARIO|CABLE|TERMINAL
             * Las tablas temporal de provision tiene este formato
             */
            $e_ter = "";
            if ( isset( $ffttArray[6] ) ) {
                $e_ter = str_pad($ffttArray[6], 3, "0", STR_PAD_LEFT);
            }
            
            if ( count($ffttArray)===4 )
            {
                $e_ter = str_pad($ffttArray[3], 3, "0", STR_PAD_LEFT);
            }

            $terminal = array();

            //Sin armario red directa, usa cable
            // CRU0||P/23|649||0|065|9 -> Directa
            // MAU2|A003|P/07|385|S/03|25|014|25 -> Flexible
            if ( isset( $ffttArray[1] ) ) {
                if (trim($ffttArray[1])==="") 
                {
                    $terminal = $GepTerminald->listar(
                            $db, 
                            array(
                                "LIM",
                                $ffttArray[0], 
                                $ffttArray[2],
                                $e_ter
                                )
                            );
                } else {
                    //Con armario red flexible
                    $terminal = $GepTerminalf->listar(
                            $db, 
                            array(
                                "LIM",
                                $ffttArray[0], 
                                $ffttArray[1],
                                $e_ter
                                )
                            );
                }
            }            

            if ( !empty( $terminal ) )
            {
                $x = $terminal[0]["coord_x"];
                $y = $terminal[0]["coord_y"];
            }
            $agenda["data"][$key]["x"] = $x;
            $agenda["data"][$key]["y"] = $y;
        }

        if($val["xr"]!='' and $val["yr"]!=''){
            $agenda["data"][$key]["x"] = $val["xr"];
            $agenda["data"][$key]["y"] = $val["yr"];
        }

    }
    return $agenda["data"];
}

if ( isset( $_POST["getCoordTec"] ) )
{
    echo json_decode($_POST["getCoordTec"]);
}

if ( isset( $_POST["getAgendaTec"] ) )
{
    $empresa = $_POST["empresa"];
    $celula = $_POST["celula"];
    $carnet = $_POST["getAgendaTec"];
    $fecha = substr($_POST["agenda"], 6, 4)
             . "-" 
             . substr($_POST["agenda"], 3, 2)
             . "-" 
             . substr($_POST["agenda"], 0, 2);
    
    $agenda = $Location->getAgendaTec(
            $db, $empresa, $celula, $fecha, $fecha, $carnet);
    
    //Obtener XY de cada orden
    if ($agenda["estado"]===true and !empty($agenda["data"])) 
    {
        foreach ($agenda["data"] as $key=>$val) {
            $x = "";
            $y = "";
            if ( strpos($val["tipoactu"], "-catv-") !== false )
            {
                //Tipo CATV -> fftt obtener xy del tap
                $ffttArray = explode("|", $val["fftt"]);
                $tap = $GeoTap->listar(
                        $db, 
                        array(
                                "LIM",
                                $ffttArray[0], 
                                $ffttArray[2], 
                                intval($ffttArray[3]), 
                                intval($ffttArray[4])
                            )
                        );
                if ( !empty( $tap ) )
                {
                    $x = $tap[0]["coord_x"];
                    $y = $tap[0]["coord_y"];
                }
                $agenda["data"][$key]["x"] = $x;
                $agenda["data"][$key]["y"] = $y;
            } else {
                //Tipo BASICA / ADSL -> llave obtener xy del terminal
                
            }
            
        }
    }
    
    echo json_encode($agenda);
}

if ( isset( $_POST["getDetallePaso"] ) )
{
    /**
     * Datos, detalle del paso a mostrar
     */
    $vdata = array();
    
    $data = explode("|", $_POST["getDetallePaso"]);
    $tareaArray = explode("_", $data[1]);
    
    $paso = $data[0];
    $tarea = $tareaArray[2];
    $detalle = "";
    
    if ($paso === "0001-") {
        $detalle = $PasoDetalle->paso_uno($db, $tarea);
        
        if (!empty($detalle["data"]))
        {
            $vdata = $detalle["data"][0];
            include './views/pasos/detalle_uno.php';
        } else {
            echo "No se encontraron registros.";
        }
        
    }
    
    if ($paso === "0002-") {
        $detalle = $PasoDetalle->paso_dos($db, $tarea);
        
        if (!empty($detalle["data"]))
        {
            $vdata = $detalle["data"][0];
            include './views/pasos/detalle_dos.php';
        } else {
            echo "No se encontraron registros.";
        }
    }
    
    if ($paso === "0003-") {
        $detalle = $PasoDetalle->paso_tres($db, $tarea);
        
        if (!empty($detalle["data"]))
        {
            $vdata = $detalle["data"][0];
            include './views/pasos/detalle_tres.php';
        } else {
            echo "No se encontraron registros.";
        }
    }
    
}

if ( isset( $_POST["getTmpAveria"] ) )
{
    $zonal  = trim($_POST["tipo_zonal"]);
    $eecc   = trim($_POST["eecc"]);
    $tipo   = trim($_POST["tipo_servicio"]);
    $mdf    = "";//trim($_POST["mdf"]);
    $codigo = trim($_POST["tipo_codigo"]);
    $quiebre = trim($_POST["tipo_quiebre"]);
    $producto = trim($_POST["tipo_producto"]);
    
    $getTmpAveria = new TmpAveria();
    $tmp = $getTmpAveria->getTmp(
            $db, $zonal, $eecc, $tipo, $mdf, $quiebre, $producto, $codigo
            );
    
    if ($tmp["estado"]===true and !empty($tmp["data"])) 
    {
        $dataTec["agenda"] = getActuCoord(
                $tmp, $db, $GeoTap, $GepTerminald, $GepTerminalf
                );        
    } else {
        $dataTec["agenda"] = array();
    }
    echo json_encode($dataTec);
}

if ( isset( $_POST["getTmpProvision"] ) )
{
    $zonal  = trim($_POST["tipo_zonal"]);
    $eecc   = trim($_POST["eecc"]);
    $tipo   = trim($_POST["tipo_servicio"]);
    $mdf    = "";//trim($_POST["mdf"]);
    $codigo = trim($_POST["tipo_codigo"]);
    $quiebre = trim($_POST["tipo_quiebre"]);
    $producto = trim($_POST["tipo_producto"]);
    
    $getTmpProvision = new TmpProvision();
    $tmp = $getTmpProvision->getTmp(
            $db, $zonal, $eecc, $tipo, $mdf, $quiebre, $producto, $codigo
            );
    
    if ($tmp["estado"]===true and !empty($tmp["data"])) 
    {
        $dataTec["agenda"] = getActuCoord(
                $tmp, $db, $GeoTap, $GepTerminald, $GepTerminalf
                );
    } else {
        $dataTec["agenda"] = array();
    }
    
    echo json_encode($dataTec);
}

//Recuperar waypoints para optimizaciond e ruta
if ( isset( $_POST["getAllWaypoints"] ) ) 
{
    $waypts = $GestionGeo->getAllWaypoints($db);
    
    $id_menor = "";
    $d_menor = "";
    
    foreach ($waypts["data"] as $key=>$val) {
        
        $d = $GeoUtils->haversineGreatCircleDistance(0, 0, $val["coord_y"], $val["coord_x"]);
        
        if ($key == 0){
            $id_menor = $val["id"];
            $d_menor = $d;
        } else {
            if ($d < $d_menor) {
                $d_menor = $d;
                $id_menor = $val["id"];
            }
        }
        
    }
    
    $GestionGeo->updateWaypoint($db, 1, $id_menor);
    
    function ordenarPuntos($GestionGeo, $GeoUtils, $db){
        
        $id_menor = 0;
        $extremo = 100000;
        
        $wptini = $GestionGeo->getIniWaypoint($db);
        $waypts = $GestionGeo->getNoOrderWaypoints($db);
        
        $x = $wptini["data"][0]["coord_x"];
        $y = $wptini["data"][0]["coord_y"];
        $id = $wptini["data"][0]["id"];
        $max = $wptini["data"][0]["orden"] + 1;
        
        $disArray = array();
        foreach ($waypts["data"] as $key=>$val) {
            
            $d = $GeoUtils->haversineGreatCircleDistance($y, $x, $val["coord_y"], $val["coord_x"]);
            
            if ($d < $extremo) {
                $id_menor = $val["id"];
                $extremo = $d;
            }

        }
        
        $GestionGeo->updateWaypoint($db, $max, $id_menor);
        
        //Comprobar si quedan puntos sin evaluar
        $data = $GestionGeo->getNoOrderWaypoints($db);
        if ( count($data["data"]) > 0 ) {
            ordenarPuntos($GestionGeo, $GeoUtils, $db);
        }
        
    }
    ordenarPuntos($GestionGeo, $GeoUtils, $db);
    
    $waypts = $GestionGeo->getAllWaypoints($db);
    echo json_encode($waypts);
}


//Planificar conjunto de ordenes
if ( isset( $_POST["doPathPlan"] ) ) 
{
    
    //1. Registrar puntos
    $group      = $_POST["doPathPlan"]; 
    $data       = explode("|^", substr($_POST["data"], 2));
    $actuStr    = "";
    $tipo       = $_POST["tipoactu"];
    $celula_id  = $_POST["celula"];
    $empresa_id = $_POST["empresa"];
    $zonal_id   = $Zonales->getIdZonal($db, $_POST["zonal"]);
    
    foreach ($data as $val) {
        $block = explode("|", $val);
        $tipoactu = $block[0];
        $codactu = $block[1];
        $coord_x = $block[2];
        $coord_y = $block[3];
        $actuStr .= "," . $codactu;
        
        //Distancia al punto (0, 0)
        $d = $GeoUtils->haversineGreatCircleDistance(0, 0, $coord_y, $coord_x);
        
        //Registrar punto
        $GestionGeo->saveWaypoint(
            $db, $user_id, $tipoactu, $codactu, $coord_x, $coord_y, $d, "", $group
        );
    }
    
    $actuStr = substr($actuStr, 1);
    
    //2. Retornar puntos con orden de planificacion
    $waypts = $GestionGeo->getAllWaypoints($db, $group);
    
    $id_menor = "";
    $d_menor = "";
    
    foreach ($waypts["data"] as $key=>$val) {
        
        $d = $GeoUtils->haversineGreatCircleDistance(0, 0, $val["coord_y"], $val["coord_x"]);
        
        if ($key == 0){
            $id_menor = $val["id"];
            $d_menor = $d;
        } else {
            if ($d < $d_menor) {
                $d_menor = $d;
                $id_menor = $val["id"];
            }
        }
        
    }
    
    $GestionGeo->updateWaypoint($db, 1, $id_menor);
    
    function ordenarPuntos($GestionGeo, $GeoUtils, $db, $grupo){
        
        $id_menor = 0;
        $extremo = 100000;
        
        $wptini = $GestionGeo->getIniWaypoint($db, $grupo);
        $waypts = $GestionGeo->getNoOrderWaypoints($db, $grupo);
        
        $x = $wptini["data"][0]["coord_x"];
        $y = $wptini["data"][0]["coord_y"];
        $id = $wptini["data"][0]["id"];
        $max = $wptini["data"][0]["orden"] + 1;
        
        $disArray = array();
        foreach ($waypts["data"] as $key=>$val) {
            
            $d = $GeoUtils->haversineGreatCircleDistance($y, $x, $val["coord_y"], $val["coord_x"]);
            
            if ($d < $extremo) {
                $id_menor = $val["id"];
                $extremo = $d;
            }

        }
        
        $GestionGeo->updateWaypoint($db, $max, $id_menor);
        
        //Comprobar si quedan puntos sin evaluar
        $data = $GestionGeo->getNoOrderWaypoints($db, $grupo);
        if ( count($data["data"]) > 0 ) {
            ordenarPuntos($GestionGeo, $GeoUtils, $db, $grupo);
        }
        
    }
    ordenarPuntos($GestionGeo, $GeoUtils, $db, $group);
    
    //Planificado y gestionado, recuperar puntos con id gestion
    //$waypts = $GestionGeo->getAllWaypointsProvision($db, $group);
    if ($tipo=="Provision"){
        //$waypts = $GestionGeo->getAllWaypointsProvision($db, $group);
    }
    //$waypts = $GestionGeo->getAllWaypoints($db, $group, false, true);
    
    /**
     * Una vez planificado, registrar en tablas de gestión
     * 
     * Motivo = 2, Submotivo = 2
     * Estado = 28 => Planificado
     */
    
    //Clase para recuperar TMP
    $tmpClass = 'Tmp' . $tipo;
    $TmpObj = new $tmpClass();
    $actArray = $TmpObj->getTmp($db, '', '', '', '', '', '', $actuStr, true);
    
    //Clase para agregar gestion a temporales
    $addClass = 'addGestion' . $tipo;
        
    foreach ($actArray["data"] as $data) {
        $reg = $GestionGeo->$addClass($db, $data, $empresa_id, $zonal_id, $user_id, $celula_id);
    }
    
    //Actualizar waypoints a estado=2 => Planificado y gestionado
    $GestionGeo->updateGroupState($db, 2, $group, $user_id);
    
    $waypts = $GestionGeo->getAllWaypointsProvision($db, $group);
    echo json_encode($waypts);
    
}

if ( isset($_FILES['archivoTmp']) ) {
    //Debe existir la siguiente carpeta
    $upload_folder = 'tmpfile';
    
    $empresa = $_POST["empresa"];
    $empresa_id = $_POST["empresa_id"];

    $nombre_archivo = $_FILES['archivoTmp']['name'];
    $ext_archivo = end((explode(".", $nombre_archivo)));

    $tipo_archivo = $_FILES['archivoTmp']['type'];

    $tamano_archivo = $_FILES['archivoTmp']['size'];

    $tmp_archivo = $_FILES['archivoTmp']['tmp_name'];

    //Nuevo nombre de archivo MD5
    $archivo_nuevo = md5(date("YmdHis") . $nombre_archivo) . "." . $ext_archivo;

    $file = $upload_folder . '/' . $nombre_archivo;

    if (!move_uploaded_file($tmp_archivo, $file)) {
        $return = array(
            'upload' => FALSE, 
            'msg' => "Ocurrio un error al subir el archivo. No pudo guardarse.", 
            'error' => $_FILES['archivoTmp']
        );
    } else {
        $data = $GeoUtils->fileToJsonAddress($file);
        $return = array(
            'upload' => TRUE, 
            'data' => $data,
            'agenda' => array()
        );
        
        $dataTec["agenda"] = array();
        
        //Iconos
        $icons = $OtUtils->iconArray();
        $iconArray = array();
        
        $averias_total=implode(",",$data);
        $ges["data"]=$gestionCriticos->getGestionCriticosAll($db, 'averias', $averias_total) ;
        foreach ($ges["data"] as $key=>$val) {
            if (array_key_exists($val["carnet_critico"], $iconArray) === false ) {
                $icon = array_pop($icons);
                $iconArray[$val["carnet_critico"]] = $icon["cal"];
                $ges["data"][$key]["icon"] = $icon["cal"];
            } else {
                $ges["data"][$key]["icon"] = $iconArray[$val["carnet_critico"]];
            }
        }

        $getActuCoord = getActuCoord(
            $ges, $db, $GeoTap, $GepTerminald, $GepTerminalf
        );
        $return["agenda"]=$getActuCoord;  
        
    }

    echo json_encode($return);
}

//Obtener horario de planificacion
if ( isset( $_POST["getHorarioPlan"] ) ) 
{
    $horario = $HorarioPlan->getHorarioPlan($db, $id_empresa);
}

//Guardar Planificacion
if ( isset( $_POST["savePlanAll"] ) )
{
    //Resultado del proceso
    $saveActu = array();
    
    //Todos los datos
    $dataArray = explode("|^~", $_POST["savePlanAll"]);
    
    /**
     * $genArray[0]: empresa_id
     * $genArray[1]: celula_id
     * $genArray[2]: carnet tecnico
     * $genArray[3]: fecha agenda: dd/mm/yyyy
     */
    $genArray   = explode("|", $dataArray[0]);
    $actuArray  = explode("|^", $dataArray[1]);
    
    $tecnico = $Tecnico->getTecnico($db, $genArray[0], $genArray[2], "", $genArray[1]);
    
    foreach ($actuArray as $key=>$val) {
                
        /**
         * $data[0]: 'Averia' o 'Provision'
         * $data[1]: codigo (requerimiento o averia)
         * $data[2]: id_gestion (0=temporal, num=gestionada)
         * $data[3]: horario_id
         * $data[4]: 1=coordinado,0=no coordinado
         */
        $data = explode("|", $val);
        $date = substr($genArray[3],6,4) . "-"
                . substr($genArray[3],3,2) . "-"
                . substr($genArray[3],0,2);
        $dia_agenda = date('w', strtotime($date)) + 1;
        
        //registro temporal
        $actuData = array();
        if ($data[2] == 0) {
            if ($data[0] == "Averia") {
                $TmpAveria = new TmpAveria();
                $actuData = $TmpAveria->getTmp($db, "", "", "", "", "", "", $data[1]);
            } else if ($data[0] == "Provision") {
                $TmpProvision = new TmpProvision();
                $actuData = $TmpProvision->getTmp($db, "", "", "", "", "", "", $data[1], false);
            }
            
            //Datos recuperados
            if (!empty($actuData["data"][0])) {
                
                $actuData["data"][0]["usuario_id"] = $user_id;
                $actuData["data"][0]["empresa_id"] = $genArray[0];
                $actuData["data"][0]["horario_id"] = $data[3];
                $actuData["data"][0]["tecnico_id"] = $tecnico["data"][0]["id"];
                $actuData["data"][0]["nomtecnico"] = $tecnico["data"][0]["nombre_tecnico"];
                $actuData["data"][0]["dia_id"]     = $dia_agenda;
                $actuData["data"][0]["fec_agenda"] = $date;
                $actuData["data"][0]["coordinado"] = $data[4];
                
                $saveActu[$data[0]][$data[1]] = saveTmp($db, $gestionCriticos, $actuData["data"][0], $GestionGeo);
            }
            
        } else if ($data[2] != 0) {
            //registro gestionado
            if ($data[0]=="Averia"){
                $data[0] = "Averias";
            }
            
            $datos = $GestionGeo->getDatosCriticos($db, $data[2]);
            
            $actuData["horario_agenda"] = $data[3];
            $actuData["dia_agenda"] = $dia_agenda;
            $actuData["actividad"] = $data[0];
            $actuData["id_gestion"] = $data[2];
            $actuData["idfila"] = $data[2];
            $actuData["id_empresa"] = $genArray[0];
            $actuData["cb_empresa"] = $genArray[0];
            $actuData["vcb_empresa"] = "";
            $actuData["id_zonal"] = 'LIM';
            $actuData["motivo"] = 1;
            $actuData["submotivo"] = 1;
            $actuData["estado"] = 1;
            $actuData["cr_nombre"] = $datos["nombre"];
            $actuData["cr_telefono"] = $datos["telefono"];
            $actuData["cr_celular"] = $datos["celular"];
            $actuData["cr_observacion"] = $datos["observacion"];
            $actuData["txt_idusuario"] = $user_id;
            $actuData["fecha_agenda"] = $date;
            $actuData["nombretecnico"] = $tecnico["data"][0]["nombre_tecnico"];
            $actuData['tecnico'] = $tecnico["data"][0]["id"];
            $actuData["nombretecnico_movimiento"] = "";
            $actuData["tecnico_movimiento"] = "";
            $actuData["fecha_agenda_ini"] = ""; //para los estados que requieren llevar a agenda
            $actuData["horario_agenda_ini"] = ""; //para los estados que requieren llevar a agenda
            $actuData["ultimo_estado"] = ""; //para los estados que requieren llevar a agenda
            $actuData['datosfinal'] = ""; //Si officetrack = OK -> enviado correctamente
            $actuData["gestion_critico"] = "gestion_critico";
            $actuData["coordinado"] = $data[4];
            
            $saveActu[$data[0]][$data[1]] = saveGes($db, $gestionCriticos, $actuData, $GestionGeo);
        }
    }
    
    echo json_encode($saveActu);
}

function saveTmp($db, $gestionCriticos, $data, $GestionGeo){

    if (trim($data["telefono"])==""){
        $data["telefono"] = "0000000";
    }
    if (trim($data["obs_dev"])==""){
        $data["obs_dev"] = "Planificado";
    }
    
    $_POST["cr_nombre"] = $data["nombre_cliente"];
    $_POST["cr_telefono"] = $data["telefono"];
    $_POST["cr_celular"] = $data["telefono"];
    $_POST["horario_agenda"] = $data["horario_id"];
    $_POST["dia_agenda"] = $data["dia_id"];
    $_POST["cb_empresa"] = $data["empresa_id"];
    $_POST["vcb_empresa"] = $data["eecc_final"];

    $_POST["cr_observacion"] = $data["obs_dev"];
    $_POST["tipo_averia"] = $data["tipoactu"];
    $_POST["horas_averia"] = $data["horas_actu"];
    $_POST["fecha_registro"] = $data["fecha_registro"];
    $_POST["ciudad"] = $data["ciudad"];
    $_POST["averia"] = $data["codactu"];
    $_POST["inscripcion"] = $data["codigo_del_cliente"];
    $_POST["fono1"] = $data["fono1"];
    $_POST["telefono"] = $data["telefono"];
    $_POST["mdf"] = $data["mdf"];
    $_POST["observacion_102"] = $data[""];
    $_POST["segmento"] = $data["codigosegmento"];
    $_POST["area_"] = $data[""];
    $_POST["direccion_instalacion"] = $data["direccion_instalacion"];
    $_POST["codigo_distrito"] = $data["distrito"];
    $_POST["nombre_cliente"] = $data["nombre_cliente"];
    $_POST["orden_trabajo"] = $data["orden"];
    $_POST["veloc_adsl"] = $data["veloc_adsl"];
    $_POST["clase_servicio_catv"] = $data["servicio"];
    $_POST["codmotivo_req_catv"] = $data["tipo_motivo"];
    $_POST["total_averias_cable"] = $data["tot_aver_cab"];
    $_POST["total_averias_cobre"] = $data["tot_aver_cob"];
    $_POST["total_averias"] = $data["tot_averias"];
    $_POST["fftt"] = $data["fftt"];
    $_POST["llave"] = $data["llave"];
    $_POST["zonal"] = $data["zonal"];

    $_POST["wu_nagendas"] = $data["wu_nagendas"];
    $_POST["wu_nmovimientos"] = $data["wu_nmovimient"];
    $_POST["wu_fecha_ult_agenda"] = $data["wu_fecha_ult_age"];
    $_POST["total_llamadas_tecnicas"] = $data["tot_llam_tec"];
    $_POST["total_llamadas_seguimiento"] = $data["tot_llam_seg"];
    $_POST["llamadastec15dias"] = $data["llamadastec15d"];
    $_POST["llamadastec30dias"] = $data["llamadastec30d"];

    $_POST["dir_terminal"] = $data[""];
    $_POST["fonos_contacto"] = $data[""];
    $_POST["contrata"] = $data["contrata"];
    $_POST["quiebre"] = $data["quiebre"];
    $_POST["lejano"] = $data["lejano"];
    $_POST["distrito"] = $data["des_distrito"];
    $_POST["eecc_zona"] = $data["eecc_zon"];
    $_POST["eecc_final"] = $data["eecc_final"];
    $_POST["microzona"] = $data["microzona"];
    $_POST["zona_movistar_uno"] = $data["zona_movuno"];
    $_POST["paquete"] = $data["paquete"];
    $_POST["data_multiproducto"] = $data["data_multip"];
    $_POST["averia_m1"] = $data["aver_m1"];
    $_POST["fecha_data_fuente"] = $data["fecha_data_fuente"];
    $_POST["telefono_codclientecms"] = $data["telefono_codclientecms"];
    $_POST["rango_dias"] = $data["rango_dias"];
    $_POST["sms1"] = $data["sms1"];
    $_POST["sms2"] = $data["sms2"];
    $_POST["area2"] = $data["area2"];
    $_POST["eecc_final"] = $data["eecc_final"];
    
    $_POST["txt_idusuario"] = $data["usuario_id"];
    $_POST["nombretecnico"] = $data["nomtecnico"];
    $_POST["tecnico"] = $data["tecnico_id"];
    $_POST["flag_tecnico"] = "si";
    $_POST["tipo_actividad"] = $data["tipoact"];
    $_POST["motivo_registro"] = "1";
    $_POST["tipo_actuacion"] = $data["tipoact"];
    $_POST["fecha_agenda"] = $data["fec_agenda"];

    $_POST['datosfinal'] = "";
    $_POST["registro_critico"] = "registro_critico";
    
    $_POST["coordinado"] = $data["coordinado"];
    
    /**
     * $registro['estado']: 1=OK
     * $registro['msg']: Respuesta de registro
     */
    $registro = $gestionCriticos->addClienteCritico($db);
    
    //Get gestion_id
    $gestion_id = "";
    if ($registro['estado']==1 or $registro['estado']==true) {
        $atcArray = explode("_", $registro['msg']);
        $gestion_id = $atcArray[2];
    }
    $_POST["tmpGestionId"] = $gestion_id;
    
    //Cadena de datos hacia officetrack
    ob_start();
    include("../historico/controladorHistorico/eventoController.php");
    $json = ob_get_clean();
    ob_end_clean();
    
    //Envío hacia officetrack
    $registro["officetrack"] = sendOfficeTrack($json);
    
    //Actualizar n_evento
    if ($registro['estado'] and $registro["officetrack"]=="OK"){
        $GestionGeo->updateAfterOt($db, $gestion_id);
    }
    
    return $registro;
}


function saveGes($db, $gestionCriticos, $data, $GestionGeo){
    $_POST = $data;
    
    /**
     * Verificar Coordinado
     */
    $check = $GestionGeo->getIfCoordinado($db, $data["id_gestion"]);
    
    $coordinado     = $check["data"]["coordinado"];
    $fecha_agenda   = $check["data"]["fecha_agenda"];
    $id_movimiento  = $check["data"]["id"];
    
    if ( $coordinado == 1 and  $fecha_agenda != "0000-00-00") {
        /**
         * Actualizar tecnico
         */
        $registro = $GestionGeo->updTecnicoCoordinado(
            $db,
            $id_movimiento,
            $data["tecnico"],
            $data["nombretecnico"]
        );
    } else {
        /**
         * $registro['estado']: 1=OK
         * $registro['msg']: Respuesta de registro
         */
        $registro = $gestionCriticos->addMovimientoCritico($db);

        //Cadena de datos hacia officetrack
        ob_start();
        include("../historico/controladorHistorico/eventoController.php");
        $json = ob_get_clean();
        ob_end_clean();

        //Envío hacia officetrack
        $registro["officetrack"] = sendOfficeTrack($json);

        //Actualizar n_evento
        if ($registro['estado'] and $registro["officetrack"]=="OK"){
            $GestionGeo->updateAfterOt($db, $data["id_gestion"]);
        }
    }
    
    return $registro;
}

/**
 * Funcion de envio y retorno hacia OT
 * 
 * @param type $cadena Json hacia officetrack
 * @return type
 */
function sendOfficeTrack($cadena){
    //Url OT
    $url = 'http://psiweb.ddns.net/test/integracion/office_track.php';
    
    //Data
    $postData = array(  
        'cadena'=>$cadena
    );  
    $ch = curl_init();  
    curl_setopt($ch, CURLOPT_URL, $url);  
    curl_setopt($ch, CURLOPT_HEADER, false);  
    curl_setopt($ch, CURLOPT_POST, true);  
    curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($postData));  
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);  
    
    //Retorno  
    $result = curl_exec($ch);
    curl_close($ch);
    
    return $result;
}

/**
 * Obtiene los tecnicos de una celula
 * Enviar empresa_id y celula_id
 */
if (isset($_POST["getTecnicoCelula"])) {
    $id_empresa = $_POST["empresa_id"];
    $idcelula = $_POST["celula_id"];
    $tecnico = $Tecnico->getTecnico($db, $id_empresa, "", "", $idcelula);
    
    $tecArray = array();
    $tecArray["names"] = $tecnico["data"];
    
    echo json_encode($tecArray);
}