<?php

class TmpAveria
{

    private $_db = "webpsi_coc";
    private $_table = "averias_criticos_final";
    private $_data;
    private $_array;
    private $_sql;
        
    public function getTmp(
            $db, $zonal, $eecc, $tipo, $mdf, $quiebre, $producto, $codigo) {
        try {
            //Iniciar transaccion
            $db->beginTransaction();
            
            $sql = "SELECT 
                        a.tipo_averia tipoactu, a.horas_averia horas_actu, 
                        a.fecha_registro, a.ciudad,
                        a.averia codactu, a.inscripcion, a.fono1,
                        a.telefono, a.mdf, a.observacion_102,
                        a.segmento, a.area_, a.direccion_instalacion,
                        a.codigo_distrito, a.nombre_cliente, a.orden_trabajo,
                        a.veloc_adsl, a.clase_servicio_catv, a.codmotivo_req_catv,
                        a.total_averias_cable, a.total_averias_cobre, a.total_averias,
                        a.fftt, a.llave, a.dir_terminal,
                        a.fonos_contacto, a.contrata, a.zonal,
                        a.wu_nagendas, a.wu_nmovimientos, a.wu_fecha_ult_agenda,
                        a.total_llamadas_tecnicas, a.total_llamadas_seguimiento, 
                        a.llamadastec15dias, a.llamadastec30dias, a.quiebre, a.lejano,
                        a.distrito, a.eecc_zona, a.zona_movistar_uno,
                        a.paquete, a.data_multiproducto, a.averia_m1,
                        a.fecha_data_fuente, a.telefono_codclientecms, a.rango_dias,
                        a.sms1, a.sms2, a.area2,
                        a.velocidad_caja_recomendada, a.tipo_servicio, 
                        a.tipo_actuacion, a.eecc_final, a.microzona,
                        b.averia no_averia, 'Averia' tipoact
                    FROM
                        $this->_db.$this->_table a
                    LEFT JOIN 
                        webpsi_criticos.gestion_averia b 
                        ON a.averia=b.averia
                    WHERE
                        a.tipo_averia IS NOT NULL";
            
            if ($zonal !== "")
            {
                $sql .= " AND a.zonal = ?";
                $this->_data[] = $zonal;
            }
            
            if ($eecc !== "")
            {
                $sql .= " AND a.eecc_final LIKE CONCAT('%', ?, '%')";
                $this->_data[] = $eecc;
            }
            
            if ($tipo !== "")
            {
                $sql .= $this->concatOr(
                        $tipo, " OR a.tipo_averia LIKE CONCAT('%', ?, '%')");
            }
            
            if ($mdf !== "")
            {
                $sql .= " AND a.mdf = ?";
                $this->_data[] = $mdf;
            }
            
            if ($quiebre !== "")
            {
                $sql .= $this->concatOr($quiebre, " OR a.quiebre = ?");
            }
            
            if ($producto !== "")
            {
                $sql .= $this->concatOr($producto, " OR a.averia_m1 = ?");
            }
            
            if ($codigo !== "")
            {
                //$sql .= " AND a.averia = ?";
                //$this->_data[] = $codigo;
                $sql .= $this->concatOr($codigo, " OR a.averia = ?");
            }
            
            $sql .= " HAVING no_averia IS NULL";
            
            $bind = $db->prepare($sql);
            $bind->execute($this->_data);
            
            $reporte = array();
            while ($data = $bind->fetch(PDO::FETCH_ASSOC)) {
                foreach ($data as $key=>$val) {
                    $data[$key] = utf8_encode($val);
                }
                $reporte[] = $data;
            }            
            
            $db->commit();
            $result["estado"] = true;
            $result["msg"] = "Datos recuperados";
            $result["data"] = $reporte;
            return $result;
        } catch (PDOException $error) {
            $db->rollback();
            $result["estado"] = false;
            $result["msg"] = $error->getMessage();
            return $result;
        }
    }
    
    
    public function getAllQuiebre($db){
        try {
            //Iniciar transaccion
            $db->beginTransaction();
            
            $sql = "SELECT 
                        quiebre 
                    FROM 
                        $this->_db.$this->_table  
                    GROUP BY 
                        quiebre";
            
            $bind = $db->prepare($sql);
            $bind->execute();
            
            $reporte = array();
            while ($data = $bind->fetch(PDO::FETCH_ASSOC)) {
                foreach ($data as $key=>$val) {
                    $data[$key] = utf8_encode($val);
                }
                $reporte[] = $data;
            }            
            
            $db->commit();
            $result["estado"] = true;
            $result["msg"] = "Datos recuperados";
            $result["data"] = $reporte;
            return $result;
        } catch (PDOException $error) {
            $db->rollback();
            $result["estado"] = false;
            $result["msg"] = $error->getMessage();
            return $result;
        }
    }
    
    
    private function concatOr($data, $condition){
        $this->_sql = "";
        $this->_array = explode(",", $data);
                
        foreach ($this->_array as $val) {
            
            if ($val === '_negative') 
            {
                $this->_sql .= str_replace("= ?", "IS NULL", $condition);
            } else {
                $this->_sql .= " $condition";
                $this->_data[] = $val;
            }
            
        }
        
        $this->_sql = substr($this->_sql, 4);
        return " AND ( $this->_sql )";
    }

}
