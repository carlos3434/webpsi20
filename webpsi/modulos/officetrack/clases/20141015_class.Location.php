<?php

class Location
{

    private $_db = "webpsi_officetrack";
    private $_table = "locations";
    private $_data;
    
    protected $_LastX;
    protected $_LastY;
    protected $_MobileNumber;
    protected $_EmployeeNum;
    protected $_LastBattery;
    protected $_TimeStamp;

    public function getLocations($db, $codArray, $numArray, $date) {
        try {
            //Iniciar transaccion
            $db->beginTransaction();
            
            $sql = "SELECT
                        a.id, a.LastX X, a.LastY Y, a.MobileNumber, 
                        a.EmployeeNum, a.LastBattery Battery, 
                        DATE_FORMAT(a.TIMESTAMP, '%Y-%m-%d %H:%i:%s') t
                    FROM
                        $this->_db.$this->_table a
                        JOIN (
                            SELECT 
                                MAX(id) id 
                            FROM 
                                $this->_db.$this->_table
                            WHERE
                                DATE(TIMESTAMP)='$date'
                            GROUP BY 
                                EmployeeNum
                        ) mx ON a.id = mx.id";
            
            if (!empty($codArray))
            {
                $sql .= " AND (" 
                            . $this->employeeList("EmployeeNum", $codArray)
                            . ")";
                $this->_data = $codArray;
            }
            
            if (!empty($numArray))
            {
                $sql .= " AND (" 
                            . $this->employeeList("MobileNumber", $numArray)
                            . ")";
                $this->_data = $numArray;
            }
            
            if ( $date !== "" )
            {
                $sql .= " AND TimeStamp LIKE '$date%'";
            }
            
            //Eliminar a Moty y JR (temporal)
            $sql .= " AND !(a.EmployeeNum = 89745 OR a.EmployeeNum = 564856) ";
            
            //Agrupar por empleado
            //$sql .= " GROUP BY EmployeeNum";
            
            $bind = $db->prepare($sql);
            $bind->execute($this->_data);
            
            $reporte = array();
            while ($data = $bind->fetch(PDO::FETCH_ASSOC)) {
                $reporte[] = $data;
            }            
            
            $db->commit();
            $result["estado"] = true;
            $result["msg"] = "Datos recuperados";
            $result["data"] = $reporte;
            return $result;
        } catch (PDOException $error) {
            //var_dump($error);
            $db->rollback();
            $result["estado"] = false;
            $result["msg"] = $error->getMessage();
            return $result;
        }
    }
    
    private function employeeList($field, $array)
    {
        $sql = "";
        
        foreach ($array as $val)
        {
            $sql .= " OR $field=?";
        }
        return substr($sql, 4);
    }
    
    public function getPath($db, $date, $code, $fromTime, $toTime){
        try {
            //Iniciar transaccion
            $db->beginTransaction();
            $sql = "SELECT 
                        LastX X, LastY Y, MobileNumber, 
                        EmployeeNum, LastBattery Battery, 
                        DATE_FORMAT(TIMESTAMP, '%Y-%m-%d %H:%i:%s') t                        
                    FROM 
                        $this->_db.$this->_table 
                    WHERE 
                        DATE(TIMESTAMP)=? AND EmployeeNum=? 
                        AND DATE_FORMAT(TIMESTAMP, '%Y-%m-%d %H:%i:%s') >= ?
                        AND DATE_FORMAT(TIMESTAMP, '%Y-%m-%d %H:%i:%s') <= ?
                    ORDER BY t";            
            
            $bind = $db->prepare($sql);
            $bind->execute( array($date, $code, $fromTime, $toTime) );
            
            $reporte = array();
            while ($data = $bind->fetch(PDO::FETCH_ASSOC)) {
                $reporte[] = $data;
            }            
            
            $db->commit();
            $result["estado"] = true;
            $result["msg"] = "Datos recuperados";
            $result["data"] = $reporte;
            return $result;
        } catch (PDOException $error) {
            //var_dump($error);
            $db->rollback();
            $result["estado"] = false;
            $result["msg"] = $error->getMessage();
            return $result;
        }
    }
    
    public function getAgendaAveria(
            $db, $empresa, $celula, $fini, $fend, $carnet
    ){
        try {
            $this->_data = array();
            //Iniciar transaccion
            $db->beginTransaction();
            
            //Gestion movimientos tecnicos
            $db->query("SELECT webpsi.GenerarIdMovIdTec()");
            
            //Query total, agendado en curso + liquidado
            
            $sql = "SELECT * FROM (
                        SELECT 
                            gc.id, gc.id_atc, gc.nombre_cliente_critico, 
                            gc.observacion, gm.tecnico, gm.id_empresa, 
                            t.idcedula, t.carnet_critico, h.horario, 
                            g.averia codactu, g.inscripcion codcli, 
                            g.telefono, g.mdf, 
                            g.fftt, g.direccion_instalacion direccion, 
                            g.quiebre, g.lejano, g.llave, 
                            g.paquete, g.eecc_final, 
                            g.microzona, g.tipo_averia tipoactu, 
                            gm.fecha_agenda, gm.id_estado
                        FROM 
                            webpsi_criticos.gestion_averia g
                        INNER JOIN 
                            webpsi_criticos.gestion_criticos gc 
                            ON gc.id=g.id_gestion
                        INNER JOIN 
                            webpsi_criticos.gestion_movimientos gm 
                            ON g.id_gestion=gm.id_gestion
                        INNER JOIN 
                            webpsi_criticos.estados e 
                            ON e.id=gm.id_estado
                        INNER JOIN 
                            webpsi_criticos.gestion_movimientos_tecnicos mt 
                            ON mt.idmovimiento=gm.id
                        INNER JOIN 
                            webpsi_criticos.tecnicos t 
                            ON t.id=mt.idtecnico
                        INNER JOIN 
                            webpsi_criticos.horarios h 
                            ON h.id=gm.id_horario
                        WHERE 
                            gm.fecha_movimiento IN (
                                SELECT
                                    MAX(gm2.fecha_movimiento)
                                FROM 
                                    webpsi_criticos.gestion_movimientos gm2
                                WHERE gm2.id_gestion=g.id_gestion
                                    AND gm2.id_estado NOT IN ('20','10','9')
                                GROUP BY gm2.id_gestion
                                )
                            AND gm.tecnico!=''
                            AND gm.id_estado=1
                            AND gm.fecha_agenda BETWEEN ? AND ?
                            AND gm.id_empresa=?
                            AND t.idcedula=?
                                
                ) encurso
                        
                UNION ALL 

                SELECT * FROM(
                    SELECT 
                        gc.id, gc.id_atc, gc.nombre_cliente_critico, 
                        gc.observacion, gm.tecnico, gm.id_empresa, 
                        t.idcedula, t.carnet_critico, h.horario, 
                        g.averia, g.inscripcion, g.telefono, g.mdf, 
                        g.fftt, g.direccion_instalacion, g.quiebre, 
                        g.lejano, g.llave, g.paquete, g.eecc_final, 
                        g.microzona, g.tipo_averia tipoactu, 
                        gm.fecha_agenda, gm.id_estado
                    FROM 
                        webpsi_criticos.gestion_averia g 
                    INNER JOIN 
                        webpsi_criticos.gestion_criticos gc 
                        ON gc.id=g.id_gestion
                    INNER JOIN 
                        webpsi_criticos.gestion_movimientos gm 
                        ON g.id_gestion=gm.id_gestion
                    INNER JOIN 
                        (   
                            SELECT 
                                gm_2.id_gestion, gm_2.fecha_agenda, 
                                gm_2.id_horario,gm_2.id_estado
                            FROM 
                                webpsi_criticos.gestion_movimientos gm_2
                            WHERE 
                                gm_2.fecha_movimiento IN 
                                (
                                    SELECT 
                                        MAX(gm2_2.fecha_movimiento) 
                                    FROM 
                                        webpsi_criticos.gestion_movimientos gm2_2 
                                    WHERE 
                                        gm_2.id_gestion=gm2_2.id_gestion 
                                    AND gm2_2.id_estado 
                                    NOT IN ('9','10','20','3','19') 
                                    GROUP BY 
                                        gm2_2.id_gestion
                                )
                                AND gm_2.tecnico!=''
                                AND gm_2.id_estado='1'
                            ) gm_aux ON gm_aux.id_gestion=gm.id_gestion 
                    INNER JOIN 
                        webpsi_criticos.estados e 
                        ON e.id=gm.id_estado 
                    INNER JOIN 
                        webpsi_criticos.gestion_movimientos_tecnicos mt 
                        ON mt.idmovimiento=gm.id
                    INNER JOIN 
                        webpsi_criticos.tecnicos t 
                        ON t.id=mt.idtecnico
                    INNER JOIN 
                        webpsi_criticos.horarios h 
                        ON h.id=gm_aux.id_horario
                    WHERE gm.fecha_movimiento IN 
                    (
                        SELECT 
                            MAX(gm2.fecha_movimiento) 
                        FROM 
                            webpsi_criticos.gestion_movimientos gm2 
                        WHERE 
                            gm2.id_gestion=g.id_gestion 
                        GROUP BY 
                            gm2.id_gestion
                    )
                    AND gm.tecnico!=''
                    AND gm.id_estado IN ('3','19')
                    AND gm_aux.fecha_agenda BETWEEN ? AND ?
                    AND gm.id_empresa=?
                    AND t.idcedula=?
                    
                ) liquidado";
            
            $this->_data[] = $fini;
            $this->_data[] = $fend;
            $this->_data[] = $empresa;
            $this->_data[] = $celula;
            
            $this->_data[] = $fini;
            $this->_data[] = $fend;
            $this->_data[] = $empresa;
            $this->_data[] = $celula;
                        
            if ( $carnet !== "" ) 
            {
                $sql .= " AND t.carnet_critico=?";
                $this->_data[] = $carnet;
            }
            
            //Ordenamiento: carnet, fecha, hora
            //$sql .= " ORDER BY t.carnet_critico, gm.fecha_agenda, h.id";
            $sql .= " ORDER BY carnet_critico, fecha_agenda, id";

            $bind = $db->prepare($sql);
            $bind->execute( $this->_data );
            
            $reporte = array();
            while ($data = $bind->fetch(PDO::FETCH_ASSOC)) {
                foreach ($data as $key=>$val) {
                    $data[$key] = utf8_encode($val);
                }
                $reporte[] = $data;
            }            
            
            $db->commit();
            $result["estado"] = true;
            $result["msg"] = "Datos recuperados";
            $result["data"] = $reporte;
            return $result;
        } catch (PDOException $error) {
            //var_dump($error);
            $db->rollback();
            $result["estado"] = false;
            $result["msg"] = $error->getMessage();
            return $result;
        }
    }
    
    
    public function getAgendaProvision(
            $db, $empresa, $celula, $fini, $fend, $carnet
    ){
        try {
            $this->_data = array();
            //Iniciar transaccion
            $db->beginTransaction();
            
            //Gestion movimientos tecnicos
            $db->query("SELECT webpsi.GenerarIdMovIdTec()");
            
            //Query total, agendado en curso + liquidado
            
            $sql = "SELECT * FROM (
                        SELECT 
                            gc.id, gc.id_atc, gc.nombre_cliente_critico, 
                            gc.observacion, gm.tecnico, gm.id_empresa, 
                            t.idcedula, t.carnet_critico, h.horario, 
                            g.codigo_req codactu, g.codigo_del_cliente codcli, 
                            g.telefono, g.mdf, 
                            g.fftt, g.direccion, g.quiebre, 
                            g.lejano, g.llave, g.paquete, g.eecc_final, 
                            g.microzona, g.origen tipoactu, 
                            gm.fecha_agenda, gm.id_estado
                        FROM 
                            webpsi_criticos.gestion_provision g
                        INNER JOIN 
                            webpsi_criticos.gestion_criticos gc 
                            ON gc.id=g.id_gestion
                        INNER JOIN 
                            webpsi_criticos.gestion_movimientos gm 
                            ON g.id_gestion=gm.id_gestion
                        INNER JOIN 
                            webpsi_criticos.estados e 
                            ON e.id=gm.id_estado
                        INNER JOIN 
                            webpsi_criticos.gestion_movimientos_tecnicos mt 
                            ON mt.idmovimiento=gm.id
                        INNER JOIN 
                            webpsi_criticos.tecnicos t 
                            ON t.id=mt.idtecnico
                        INNER JOIN 
                            webpsi_criticos.horarios h 
                            ON h.id=gm.id_horario
                        WHERE 
                            gm.fecha_movimiento IN (
                                SELECT
                                    MAX(gm2.fecha_movimiento)
                                FROM 
                                    webpsi_criticos.gestion_movimientos gm2
                                WHERE gm2.id_gestion=g.id_gestion
                                    AND gm2.id_estado NOT IN ('20','10','9')
                                GROUP BY gm2.id_gestion
                                )
                            AND gm.tecnico!=''
                            AND gm.id_estado=1
                            AND gm.fecha_agenda BETWEEN ? AND ?
                            AND gm.id_empresa=?
                            AND t.idcedula=?
                                
                ) encurso
                        
                UNION ALL 

                SELECT * FROM(
                    SELECT 
                        gc.id, gc.id_atc, gc.nombre_cliente_critico, 
                        gc.observacion, gm.tecnico, gm.id_empresa, 
                        t.idcedula, t.carnet_critico, h.horario, 
                        g.codigo_req codactu, g.codigo_del_cliente codcli, 
                        g.telefono, g.mdf, 
                        g.fftt, g.direccion, g.quiebre, 
                        g.lejano, g.llave, g.paquete, g.eecc_final, 
                        g.microzona, g.origen tipoactu, 
                        gm.fecha_agenda, gm.id_estado
                    FROM 
                        webpsi_criticos.gestion_provision g 
                    INNER JOIN 
                        webpsi_criticos.gestion_criticos gc 
                        ON gc.id=g.id_gestion
                    INNER JOIN 
                        webpsi_criticos.gestion_movimientos gm 
                        ON g.id_gestion=gm.id_gestion
                    INNER JOIN 
                        (   
                            SELECT 
                                gm_2.id_gestion, gm_2.fecha_agenda, 
                                gm_2.id_horario,gm_2.id_estado
                            FROM 
                                webpsi_criticos.gestion_movimientos gm_2
                            WHERE 
                                gm_2.fecha_movimiento IN 
                                (
                                    SELECT 
                                        MAX(gm2_2.fecha_movimiento) 
                                    FROM 
                                        webpsi_criticos.gestion_movimientos gm2_2 
                                    WHERE 
                                        gm_2.id_gestion=gm2_2.id_gestion 
                                    AND gm2_2.id_estado 
                                    NOT IN ('9','10','20','3','19') 
                                    GROUP BY 
                                        gm2_2.id_gestion
                                )
                                AND gm_2.tecnico!=''
                                AND gm_2.id_estado='1'
                            ) gm_aux ON gm_aux.id_gestion=gm.id_gestion 
                    INNER JOIN 
                        webpsi_criticos.estados e 
                        ON e.id=gm.id_estado 
                    INNER JOIN 
                        webpsi_criticos.gestion_movimientos_tecnicos mt 
                        ON mt.idmovimiento=gm.id
                    INNER JOIN 
                        webpsi_criticos.tecnicos t 
                        ON t.id=mt.idtecnico
                    INNER JOIN 
                        webpsi_criticos.horarios h 
                        ON h.id=gm_aux.id_horario
                    WHERE gm.fecha_movimiento IN 
                    (
                        SELECT 
                            MAX(gm2.fecha_movimiento) 
                        FROM 
                            webpsi_criticos.gestion_movimientos gm2 
                        WHERE 
                            gm2.id_gestion=g.id_gestion 
                        GROUP BY 
                            gm2.id_gestion
                    )
                    AND gm.tecnico!=''
                    AND gm.id_estado IN ('3','19')
                    AND gm_aux.fecha_agenda BETWEEN ? AND ?
                    AND gm.id_empresa=?
                    AND t.idcedula=?
                    
                ) liquidado";
            
            $this->_data[] = $fini;
            $this->_data[] = $fend;
            $this->_data[] = $empresa;
            $this->_data[] = $celula;
            
            $this->_data[] = $fini;
            $this->_data[] = $fend;
            $this->_data[] = $empresa;
            $this->_data[] = $celula;
                        
            if ( $carnet !== "" ) 
            {
                $sql .= " AND t.carnet_critico=?";
                $this->_data[] = $carnet;
            }
            
            //Ordenamiento: carnet, fecha, hora
            //$sql .= " ORDER BY t.carnet_critico, gm.fecha_agenda, h.id";
            $sql .= " ORDER BY carnet_critico, fecha_agenda, id";

            $bind = $db->prepare($sql);
            $bind->execute( $this->_data );
            
            $reporte = array();
            while ($data = $bind->fetch(PDO::FETCH_ASSOC)) {
                foreach ($data as $key=>$val) {
                    $data[$key] = utf8_encode($val);
                }
                $reporte[] = $data;
            }            
            
            $db->commit();
            $result["estado"] = true;
            $result["msg"] = "Datos recuperados";
            $result["data"] = $reporte;
            return $result;
        } catch (PDOException $error) {
            //var_dump($error);
            $db->rollback();
            $result["estado"] = false;
            $result["msg"] = $error->getMessage();
            return $result;
        }
    }

}
