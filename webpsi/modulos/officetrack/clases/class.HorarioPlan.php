<?php

class HorarioPlan
{

    private $_db = "webpsi_criticos";
    private $_table = "";
    private $_data;
    

    public function getHorarioPlan($db, $id_empresa, $dia) {
        try {
            //Iniciar transaccion
            $db->beginTransaction();
            
            $sql = "SELECT 
                        h.id, h.horario, ch.capacidad
                    FROM 
                        $this->_db.horarios h, 
                        $this->_db.capacidad_horarios ch
                    WHERE
                        h.id=ch.horario
                        AND h.tipo = 2
                        AND ch.empresa = ?
                        AND ch.dia = 1
                        AND ch.zona = 1";
            
            $bind = $db->prepare($sql);
            $bind->execute(array($id_empresa, $dia));
            
            $reporte = array();
            while ($data = $bind->fetch(PDO::FETCH_ASSOC)) {
                $reporte[] = $data;
            }            
            
            $db->commit();
            $result["estado"] = true;
            $result["msg"] = "Datos recuperados";
            $result["data"] = $reporte;
            return $result;
        } catch (PDOException $error) {
            $db->rollback();
            $result["estado"] = false;
            $result["msg"] = $error->getMessage();
            return $result;
        }
    }
    

}
