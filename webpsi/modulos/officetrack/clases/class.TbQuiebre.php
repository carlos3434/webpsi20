<?php

class TbQuiebre
{

    private $_db = "webpsi";
    private $_table = "tb_quiebre";
    private $_data;
    private $_array;
    private $_sql;
        
    public function getAll($db) {
        try {
            //Iniciar transaccion
            $db->beginTransaction();
            
            $sql = "SELECT 
                        id_quiebre, nombre, cestado, apocope
                    FROM
                        $this->_db.$this->_table";
            
            $bind = $db->prepare($sql);
            $bind->execute();
            
            $reporte = array();
            while ($data = $bind->fetch(PDO::FETCH_ASSOC)) {
                foreach ($data as $key=>$val) {
                    $data[$key] = utf8_encode($val);
                }
                $reporte[] = $data;
            }            
            
            $db->commit();
            $result["estado"] = true;
            $result["msg"] = "Datos recuperados";
            $result["data"] = $reporte;
            return $result;
        } catch (PDOException $error) {
            $db->rollback();
            $result["estado"] = false;
            $result["msg"] = $error->getMessage();
            return $result;
        }
    }

}
