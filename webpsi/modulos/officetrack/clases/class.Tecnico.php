<?php

class Tecnico
{

    private $_db = "webpsi_criticos";
    private $_table = "tecnicos";
    private $_data;
    

    public function getTecnico($db, $id_empresa, $carnet, $dni, $idcedula) {
        try {
            //Iniciar transaccion
            $db->beginTransaction();
            
            $sql = "SELECT
                        id, nombre_tecnico, ape_paterno, 
                        ape_materno, carnet_critico carnet, dni , IFNULL(g.grupos,0) grupos
                    FROM
                        $this->_db.$this->_table
                    LEFT JOIN (
                        select  cg.idtecnico , GROUP_CONCAT(cg.idgrupo) grupos
                        from webpsi_officetrack.celula_grupos cg
                        GROUP BY cg.idtecnico
										) g on g.idtecnico = id
                    WHERE 
                        carnet_critico <> '' AND activo=1";
            
            if ($id_empresa !== "")
            {
                $sql .= " AND id_empresa = ?";
                $this->_data[] = $id_empresa;
            }
            
            if ($carnet !== "")
            {
                $sql .= " AND carnet_critico = ?";
                $this->_data[] = $carnet;
            }
            
            if ($dni !== "")
            {
                $sql .= " AND dni = ?";
                $this->_data[] = $dni;
            }
            
            if ($idcedula !== "")
            {
                $sql .= " AND idcedula = ?";
                $this->_data[] = $idcedula;
            }
            
            $bind = $db->prepare($sql);
            $bind->execute($this->_data);
            
            $reporte = array();
            while ($data = $bind->fetch(PDO::FETCH_ASSOC)) {
                foreach ($data as $key=>$val) {
                    $data[$key] = utf8_encode($val);
                }
                $reporte[] = $data;
            }            
            
            $db->commit();
            $result["estado"] = true;
            $result["msg"] = "Datos recuperados";
            $result["data"] = $reporte;
            return $result;
        } catch (PDOException $error) {
            //var_dump($error);
            $db->rollback();
            $result["estado"] = false;
            $result["msg"] = $error->getMessage();
            return $result;
        }
    }
    

}
