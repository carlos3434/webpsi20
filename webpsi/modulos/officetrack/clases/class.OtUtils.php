<?php
class OtUtils
{
    private $_iconList;
    
    private function doIconList(){
        $folder = "./images/icons/";
        
        if ($od = opendir($folder)) 
        {
            while (false !== ($file = readdir($od))) {
                if ($file != "." && $file != "..") {
                    $this->_iconList[] = $file;
                }
            }
            closedir($od);
        }
        
        return $this->_iconList;
    }
    
    public function iconArray(){
        $iconArray = array();
        
        $list = $this->doIconList();
        foreach ($list as $val) {
            $part = explode("_", $val);
            $iconArray[substr($part[1], 0, 6)]["tec"] = "tec_" . $part[1];
            $iconArray[substr($part[1], 0, 6)]["cal"] = "cal_" . $part[1];
            $iconArray[substr($part[1], 0, 6)]["car"] = "car_" . $part[1];
        }
        
        return $iconArray;
    }
    
    public function getActuCoord($agenda, $db, $GeoTap, $GepTerminald, $GepTerminalf){
        foreach ($agenda["data"] as $key=>$val) {
            $x = "";
            $y = "";

            /**
             * Primera condicion: Gestionadas (-catv-)
             * Segunda condicion: Temporales (_catv)
             */
            if (
                    strpos($val["tipoactu"], "-catv-") !== false  or 
                    strpos($val["tipoactu"], "_catv") !== false 
                )
            {
                //Tipo CATV -> fftt obtener xy del tap
                $ffttArray = explode("|", $val["fftt"]);

                /**
                 * las tablas temporales retornan 5 datos
                 */
                $e_troba = $ffttArray[2];
                $e_amplificador = $ffttArray[3];
                $e_tap = $ffttArray[4];

                if ( count($ffttArray) === 5 )
                {
                    $e_troba = $ffttArray[1];
                    $e_amplificador = $ffttArray[2];
                    $e_tap = $ffttArray[3];
                }

                if (ctype_digit($e_amplificador) ) {
                    $e_amplificador = intval($e_amplificador);
                }
                if (ctype_digit($e_tap) ) {
                    $e_tap = intval($e_tap);
                }

                $tap = $GeoTap->listar(
                        $db, 
                        array(
                                "LIM",
                                $ffttArray[0], 
                                $e_troba,
                                $e_amplificador,
                                $e_tap
                            )
                        );

                if ( !empty( $tap ) )
                {
                    $x = $tap[0]["coord_x"];
                    $y = $tap[0]["coord_y"];
                }
                $agenda["data"][$key]["x"] = $x;
                $agenda["data"][$key]["y"] = $y;
            } else {
                //Tipo BASICA / ADSL -> llave obtener xy del terminal
                $ffttArray = explode("|", $val["fftt"]);

                /**
                 * Algunas llaves tienen 4 datos
                 * MDF|ARMARIO|CABLE|TERMINAL
                 * Las tablas temporal de provision tiene este formato
                 */
                $e_ter = str_pad($ffttArray[6], 3, "0", STR_PAD_LEFT);
                if ( count($ffttArray)===4 )
                {
                    $e_ter = str_pad($ffttArray[3], 3, "0", STR_PAD_LEFT);
                }

                $terminal = array();

                //Sin armario red directa, usa cable
                // CRU0||P/23|649||0|065|9 -> Directa
                // MAU2|A003|P/07|385|S/03|25|014|25 -> Flexible
                if (trim($ffttArray[1])==="") 
                {
                    $terminal = $GepTerminald->listar(
                            $db, 
                            array(
                                "LIM",
                                $ffttArray[0], 
                                $ffttArray[2],
                                $e_ter
                                )
                            );
                } else {
                    //Con armario red flexible
                    $terminal = $GepTerminalf->listar(
                            $db, 
                            array(
                                "LIM",
                                $ffttArray[0], 
                                $ffttArray[1],
                                $e_ter
                                )
                            );
                }

                if ( !empty( $terminal ) )
                {
                    $x = $terminal[0]["coord_x"];
                    $y = $terminal[0]["coord_y"];
                }
                $agenda["data"][$key]["x"] = $x;
                $agenda["data"][$key]["y"] = $y;
            }

        }
        return $agenda["data"];
    }
}