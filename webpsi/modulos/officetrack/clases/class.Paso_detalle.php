<?php
class Paso_detalle
{
    private $_db = "webpsi_officetrack";
    private $_table = "";
    private $_data;
    
    public function paso_uno($mysql, $tarea){
        try {
            
            //Iniciar transaccion
            $mysql->beginTransaction();
            
            //Registrar error
            $sql = "SELECT 
                        pu.x, pu.y, pu.casa_img1, 
                        pu.casa_img2, pu.casa_img3
                    FROM
                        webpsi_officetrack.tareas t, 
                        webpsi_officetrack.paso_uno pu 
                    WHERE
                        t.id=pu.task_id 
                        AND t.task_id = ?";

            $valArray = array($tarea);
            $stmt = $mysql->prepare($sql);
            $stmt->execute($valArray);
            
            $reporte = array();
            while ($data = $stmt->fetch(PDO::FETCH_ASSOC)) {
                $reporte[] = $data;
            } 
            
            $mysql->commit();
            $result["estado"] = true;
            $result["msg"] = "Paso uno recuperado correctamente";
            $result["data"] = $reporte;
            return $result;
        } catch (PDOException $error) {var_dump($error);
            //Rollback
            $mysql->rollback();
            $result["estado"] = false;
            $result["msg"] = $error->getMessage();
            return $result;
        }
    }
    
    public function paso_dos($mysql, $tarea){
        try {
            
            //Iniciar transaccion
            $mysql->beginTransaction();
            
            //Registrar error
            $sql = "SELECT 
                        pu.motivo, pu.observacion, 
                        pu.problema_img1, pu.problema_img2, 
                        pu.modem_img1, pu.modem_img2, 
                        pu.tap_img1, pu.tap_img2, 
                        pu.tv_img1, pu.tv_img2
                    FROM
                        webpsi_officetrack.tareas t, 
                        webpsi_officetrack.paso_dos pu 
                    WHERE
                        t.id=pu.task_id 
                        AND t.task_id = ?";

            $valArray = array($tarea);
            $stmt = $mysql->prepare($sql);
            $stmt->execute($valArray);
            
            $reporte = array();
            while ($data = $stmt->fetch(PDO::FETCH_ASSOC)) {
                $reporte[] = $data;
            } 
            
            $mysql->commit();
            $result["estado"] = true;
            $result["msg"] = "Paso dos recuperado correctamente";
            $result["data"] = $reporte;
            return $result;
        } catch (PDOException $error) {            
            //Rollback
            $mysql->rollback();
            $result["estado"] = false;
            $result["msg"] = $error->getMessage();
            return $result;
        }
    }
    
    public function paso_tres($mysql, $tarea){
        try {
            
            //Iniciar transaccion
            $mysql->beginTransaction();
            
            //Registrar error
            $sql = "SELECT 
                        pu.estado, pu.observacion, 
                        pu.final_img1, pu.final_img2, 
                        pu.firma_img
                    FROM
                        webpsi_officetrack.tareas t, 
                        webpsi_officetrack.paso_tres pu 
                    WHERE
                        t.id=pu.task_id 
                        AND t.task_id = ?";

            $valArray = array($tarea);
            $stmt = $mysql->prepare($sql);
            $stmt->execute($valArray);
            
            $reporte = array();
            while ($data = $stmt->fetch(PDO::FETCH_ASSOC)) {
                $reporte[] = $data;
            } 
            
            $mysql->commit();
            $result["estado"] = true;
            $result["msg"] = "Paso tres recuperado correctamente";
            $result["data"] = $reporte;
            return $result;
        } catch (PDOException $error) {            
            //Rollback
            $mysql->rollback();
            $result["estado"] = false;
            $result["msg"] = $error->getMessage();
            return $result;
        }
    }

    public function ultimaLiquidada($mysql , $carnet)
    {
        try {

            //Registrar error
            $sql = "select   task_id, tipo_actividad
                    from webpsi_officetrack.tareas t
                    inner join webpsi_criticos.gestion_criticos gc on gc.id = t.task_id
                     where t.cod_tecnico = ?  and t.paso = '0003-Cierre'
                    order by t.id desc
                    limit 1;";

            $valArray = array($carnet);
            $stmt = $mysql->prepare($sql);
            $stmt->execute($valArray);
            $task = $stmt->fetch();


            $result["estado"] = true;
            $result["msg"] = "Paso tres recuperado correctamente";
            $result["data"] = $task;
            return $result;
        } catch (PDOException $error) {
            //Rollback

            $result["estado"] = false;
            $result["msg"] = $error->getMessage();
            return $result;
        }
    }
    
}