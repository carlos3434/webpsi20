<?php

class Empresa
{

    private $_db = "webpsi_criticos";
    private $_table = "empresa";
    private $_data;
    

    public function getEmpresa($db, $nombre, $activo) {
        try {
            //Iniciar transaccion
            $db->beginTransaction();
            
            $sql = "SELECT
                        id, nombre, activo
                    FROM
                        $this->_db.$this->_table 
                    WHERE 
                        id IS NOT NULL";
            
            if ($nombre !== "")
            {
                $sql .= " AND nombre LIKE CONCAT('%', ?, '%')";
                $this->_data[] = $nombre;
            }
            
            if ($activo !== "")
            {
                $sql .= " AND activo = ?";
                $this->_data[] = $activo;
            }
            
            $bind = $db->prepare($sql);
            $bind->execute($this->_data);
            
            $reporte = array();
            while ($data = $bind->fetch(PDO::FETCH_ASSOC)) {
                $reporte[] = $data;
            }            
            
            $db->commit();
            $result["estado"] = true;
            $result["msg"] = "Datos recuperados";
            $result["data"] = $reporte;
            return $result;
        } catch (PDOException $error) {
            //var_dump($error);
            $db->rollback();
            $result["estado"] = false;
            $result["msg"] = $error->getMessage();
            return $result;
        }
    }
    

}
