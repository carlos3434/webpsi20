<?php

class Location
{

    private $_db = "webpsi_officetrack";
    private $_table = "locations";
    private $_data;
    
    protected $_sql;
    protected $_array;
    
    protected $_LastX;
    protected $_LastY;
    protected $_MobileNumber;
    protected $_EmployeeNum;
    protected $_LastBattery;
    protected $_TimeStamp;

    /**
     * Obtiene ultimas localizaciones de los tecnicos
     * 
     * @param Object $db Objeto de conexion a DB
     * @param Array $codArray Arreglo de codigos de tecnicos
     * @param Array $numArray Arreglo de numeros telefonicos
     * @param String $date Dia de consulta '2014-09-23'
     * @return Array
     */
    public function getLocations($db, $codArray, $numArray, $date) {
        try {
            //Iniciar transaccion
            $db->beginTransaction();
            
            $sql = "SELECT
                        a.id, a.LastX X, a.LastY Y, a.MobileNumber, 
                        a.EmployeeNum, a.LastBattery Battery, 
                        DATE_FORMAT(a.TIMESTAMP, '%Y-%m-%d %H:%i:%s') t
                    FROM
                        $this->_db.$this->_table a
                        JOIN (
                            SELECT 
                                MAX(id) id 
                            FROM 
                                $this->_db.$this->_table
                            WHERE
                                DATE(TIMESTAMP)='$date'
                            GROUP BY 
                                EmployeeNum
                        ) mx ON a.id = mx.id";
            
            if (!empty($codArray))
            {
                $sql .= " AND (" 
                            . $this->employeeList("EmployeeNum", $codArray)
                            . ")";
                $this->_data = $codArray;
            }
            
            if (!empty($numArray))
            {
                $sql .= " AND (" 
                            . $this->employeeList("MobileNumber", $numArray)
                            . ")";
                $this->_data = $numArray;
            }
            
            if ( $date !== "" )
            {
                $sql .= " AND TimeStamp LIKE '$date%'";
            }
            
            //Eliminar a Moty y JR (temporal)
            $sql .= " AND !(a.EmployeeNum = 89745 OR a.EmployeeNum = 564856) ";
            
            //Agrupar por empleado
            //$sql .= " GROUP BY EmployeeNum";
            
            $bind = $db->prepare($sql);
            $bind->execute($this->_data);
            
            $reporte = array();
            while ($data = $bind->fetch(PDO::FETCH_ASSOC)) {
                $reporte[] = $data;
            }            
            
            $db->commit();
            $result["estado"] = true;
            $result["msg"] = "Datos recuperados";
            $result["data"] = $reporte;
            return $result;
        } catch (PDOException $error) {
            //var_dump($error);
            $db->rollback();
            $result["estado"] = false;
            $result["msg"] = $error->getMessage();
            return $result;
        }
    }
    
    private function employeeList($field, $array)
    {
        $sql = "";
        
        foreach ($array as $val)
        {
            $sql .= " OR $field=?";
        }
        return substr($sql, 4);
    }
    
    public function getPath($db, $date, $code, $fromTime, $toTime){
        try {
            //Iniciar transaccion
            $db->beginTransaction();
            $sql = "SELECT 
                        LastX X, LastY Y, MobileNumber, 
                        EmployeeNum, LastBattery Battery, 
                        DATE_FORMAT(TIMESTAMP, '%Y-%m-%d %H:%i:%s') t                        
                    FROM 
                        $this->_db.$this->_table 
                    WHERE 
                        DATE(TIMESTAMP)=? AND EmployeeNum=? 
                        AND DATE_FORMAT(TIMESTAMP, '%Y-%m-%d %H:%i:%s') >= ?
                        AND DATE_FORMAT(TIMESTAMP, '%Y-%m-%d %H:%i:%s') <= ?
                    ORDER BY t";            
            
            $bind = $db->prepare($sql);
            $bind->execute( array($date, $code, $fromTime, $toTime) );
            
            $reporte = array();
            while ($data = $bind->fetch(PDO::FETCH_ASSOC)) {
                $reporte[] = $data;
            }            
            
            $db->commit();
            $result["estado"] = true;
            $result["msg"] = "Datos recuperados";
            $result["data"] = $reporte;
            return $result;
        } catch (PDOException $error) {
            //var_dump($error);
            $db->rollback();
            $result["estado"] = false;
            $result["msg"] = $error->getMessage();
            return $result;
        }
    }


    public function getAgendaAverias(
        $db, $empresa, $celula, $fini, $fend, $carnet, $estado , $task_id = "", $acu_cod = ""
    ){
       return  $this->getAgendaAveria(
            $db, $empresa, $celula, $fini, $fend, $carnet, $estado , $task_id, $acu_cod
        );
    }

    public function getAgendaAveria(
            $db, $empresa, $celula, $fini, $fend, $carnet, $estado , $task_id = "", $acu_cod = ""
    ){
        try {
            $this->_data = array();
            //Iniciar transaccion
            $db->beginTransaction();
            
            //Query total, agendado en curso + liquidado            
            $sql = "SELECT
                        id, id_atc, nombre_cliente_critico, 
                        observacion, tecnico, id_empresa, 
                        idcedula, carnet_critico, horario, 
                        codactu, codcli, 
                        telefono, mdf, 
                        fftt, direccion, 
                        quiebre, lejano, llave, 
                        paquete, eecc_final, 
                        microzona, tipoactu, 
                        fecha_agenda, id_estado, 'Averia' tipoact,
                        coordinado
                    FROM
                    (
                        SELECT 
                            gc.id, gc.id_atc, gc.nombre_cliente_critico, 
                            gc.observacion, gm.tecnico, gm.id_empresa, 
                            t.idcedula, t.carnet_critico, h.horario, 
                            g.averia codactu, g.inscripcion codcli, 
                            g.telefono, g.mdf, 
                            g.fftt, g.direccion_instalacion direccion, 
                            g.quiebre, g.lejano, g.llave, 
                            g.paquete, g.eecc_final, 
                            g.microzona, g.tipo_averia tipoactu, 
                            gm.fecha_agenda, gm.id_estado, gm.coordinado
                        FROM 
                            webpsi_criticos.gestion_averia g
                        INNER JOIN 
                            webpsi_criticos.gestion_criticos gc 
                            ON gc.id=g.id_gestion
                        INNER JOIN 
                            webpsi_criticos.gestion_movimientos gm 
                            ON g.id_gestion=gm.id_gestion
                        INNER JOIN 
                            webpsi_criticos.estados e 
                            ON e.id=gm.id_estado
                        INNER JOIN 
                            webpsi_criticos.tecnicos t 
                            ON t.id=gm.idtecnico
                        INNER JOIN 
                            webpsi_criticos.horarios h 
                            ON h.id=gm.id_horario
                        WHERE 
                            gm.id IN (
                                SELECT
                                    MAX(gm2.id)
                                FROM 
                                    webpsi_criticos.gestion_movimientos gm2
                                WHERE 
                                    gm2.id_gestion=g.id_gestion
                                GROUP 
                                    BY gm2.id_gestion
                                )";
            
            if ($empresa !== "") {
                $sql .= " AND gm.id_empresa=?";
                $this->_data[] = $empresa;
            }
            
            if ($celula !== "") {
                $sql .= " AND t.idcedula=?";
                $this->_data[] = $celula;
            }
            
            if ($fini !== "" and $fend !== "") {
                $sql .= " AND gm.fecha_agenda BETWEEN ? AND ?";
                $this->_data[] = $fini;
                $this->_data[] = $fend;
            }
                        
            if ( $carnet !== "" ) 
            {
                $sql .= " AND t.carnet_critico=?";
                $this->_data[] = $carnet;
            }
            
            if ( $estado !== "" ) 
            {
                $sql .= $this->concatOr($estado, " OR gm.id_estado = ?");
            }

            if ( $task_id !== "" )
            {
                $sql .= " AND gc.id=?";
                $this->_data[] = $task_id;
            }

            if ( $acu_cod !== "" )
            {
                $sql .= " AND g.averia = '?' ";
                $this->_data[] = $acu_cod;
            }

            $sql .= " UNION ALL 

                        SELECT 
                            gc.id, gc.id_atc, gc.nombre_cliente_critico, 
                            gc.observacion, gm.tecnico, gm.id_empresa, 
                            t.idcedula, t.carnet_critico, h.horario, 
                            g.averia codactu, g.inscripcion codcli, 
                            g.telefono, g.mdf, 
                            g.fftt, g.direccion_instalacion direccion, 
                            g.quiebre, g.lejano, g.llave, 
                            g.paquete, g.eecc_final, 
                            g.microzona, g.tipo_averia tipoactu, 
                            gm.fecha_agenda, gm.id_estado, gm.coordinado
                        FROM 
                            webpsi_criticos.gestion_rutina_manual g
                        INNER JOIN 
                            webpsi_criticos.gestion_criticos gc 
                            ON gc.id=g.id_gestion
                        INNER JOIN 
                            webpsi_criticos.gestion_movimientos gm 
                            ON g.id_gestion=gm.id_gestion
                        INNER JOIN 
                            webpsi_criticos.estados e 
                            ON e.id=gm.id_estado
                        INNER JOIN 
                            webpsi_criticos.tecnicos t 
                            ON t.id=gm.idtecnico
                        INNER JOIN 
                            webpsi_criticos.horarios h 
                            ON h.id=gm.id_horario
                        WHERE 
                            gm.id IN (
                                SELECT
                                    MAX(gm2.id)
                                FROM 
                                    webpsi_criticos.gestion_movimientos gm2
                                WHERE 
                                    gm2.id_gestion=g.id_gestion
                                GROUP 
                                    BY gm2.id_gestion
                                )";
            
            if ($empresa !== "") {
                $sql .= " AND gm.id_empresa=?";
                $this->_data[] = $empresa;
            }
            
            if ($celula !== "") {
                $sql .= " AND t.idcedula=?";
                $this->_data[] = $celula;
            }
            
            if ($fini !== "" and $fend !== "") {
                $sql .= " AND gm.fecha_agenda BETWEEN ? AND ?";
                $this->_data[] = $fini;
                $this->_data[] = $fend;
            }
                        
            if ( $carnet !== "" ) 
            {
                $sql .= " AND t.carnet_critico=?";
                $this->_data[] = $carnet;
            }
            
            if ( $estado !== "" ) 
            {
                $sql .= $this->concatOr($estado, " OR gm.id_estado = ?");
            }

            if ( $task_id !== "" )
            {
                $sql .= " AND gc.id=?";
                $this->_data[] = $task_id;
            }

            if ( $acu_cod !== "" )
            {
                $sql .= " AND g.averia = '?' ";
                $this->_data[] = $acu_cod;
            }
            
            $sql .= "        ) t ";
            
            //Ordenamiento: carnet, fecha, id
            $sql .= " ORDER BY carnet_critico, fecha_agenda, id";

            $bind = $db->prepare($sql);
            $bind->execute( $this->_data );
            
            $reporte = array();
            while ($data = $bind->fetch(PDO::FETCH_ASSOC)) {
                foreach ($data as $key=>$val) {
                    $data[$key] = utf8_encode($val);
                }
                $reporte[] = $data;
            }            
            
            $db->commit();
            $result["estado"] = true;
            $result["msg"] = "Datos recuperados";
            $result["data"] = $reporte;
            return $result;
        } catch (PDOException $error) {
            //var_dump($error);
            $db->rollback();
            $result["estado"] = false;
            $result["msg"] = $error->getMessage();
            return $result;
        }
    }
    
    public function getAgendaProvision(
            $db, $empresa, $celula, $fini, $fend, $carnet, 
            $estado , $task_id = "", $codactu = ""
    ){
        try {
            $this->_data = array();
            //Iniciar transaccion
            $db->beginTransaction();
            
            //Query total, agendado en curso + liquidado            
            $sql = "SELECT 
                        id, id_atc, nombre_cliente_critico, 
                        observacion, tecnico, id_empresa, 
                        idcedula, carnet_critico, horario, 
                        codactu, codcli, 
                        telefono, mdf, 
                        fftt, direccion, quiebre, 
                        lejano, llave, paquete, eecc_final, 
                        microzona, tipoactu, 
                        fecha_agenda, id_estado, 'Provision' tipoact,
                        coordinado

                    FROM 
                    (
                        SELECT 
                            gc.id, gc.id_atc, gc.nombre_cliente_critico, 
                            gc.observacion, gm.tecnico, gm.id_empresa, 
                            t.idcedula, t.carnet_critico, h.horario, 
                            g.codigo_req codactu, g.codigo_del_cliente codcli, 
                            g.telefono, g.mdf, 
                            g.fftt, g.direccion, g.quiebre, 
                            g.lejano, g.llave, g.paquete, g.eecc_final, 
                            g.microzona, g.origen tipoactu, 
                            gm.fecha_agenda, gm.id_estado, gm.coordinado
                        FROM 
                            webpsi_criticos.gestion_provision g
                        INNER JOIN 
                            webpsi_criticos.gestion_criticos gc 
                            ON gc.id=g.id_gestion
                        INNER JOIN 
                            webpsi_criticos.gestion_movimientos gm 
                            ON g.id_gestion=gm.id_gestion
                        INNER JOIN 
                            webpsi_criticos.estados e 
                            ON e.id=gm.id_estado
                        INNER JOIN 
                            webpsi_criticos.tecnicos t 
                            ON t.id=gm.idtecnico
                        INNER JOIN 
                            webpsi_criticos.horarios h 
                            ON h.id=gm.id_horario
                        WHERE 
                            gm.id IN (
                                SELECT
                                    MAX(gm2.id)
                                FROM 
                                    webpsi_criticos.gestion_movimientos gm2
                                WHERE 
                                    gm2.id_gestion=g.id_gestion
                                GROUP BY 
                                    gm2.id_gestion
                                )";
            
            if ($empresa !== "") {
                $sql .= " AND gm.id_empresa=?";
                $this->_data[] = $empresa;
            }
            
            if ($celula !== "") {
                $sql .= " AND t.idcedula=?";
                $this->_data[] = $celula;
            }
            
            if ($fini !== "" and $fend !== "") {
                $sql .= " AND gm.fecha_agenda BETWEEN ? AND ?";
                $this->_data[] = $fini;
                $this->_data[] = $fend;
            }
                        
            if ( $carnet !== "" ) 
            {
                $sql .= " AND t.carnet_critico=?";
                $this->_data[] = $carnet;
            }
            
            if ( $estado !== "" ) 
            {
                $sql .= $this->concatOr($estado, " OR gm.id_estado = ?");
            }

            if ( $task_id !== "" )
            {
                $sql .= " AND gc.id=?";
                $this->_data[] = $task_id;
            } 

            if ( $codactu !== "")
            {
                $sql .=" AND g.codigo_req = '?' ";
                $this->_data[] = $codactu;
            }           

            $sql .= "            UNION ALL 

                        SELECT 
                            gc.id, gc.id_atc, gc.nombre_cliente_critico, 
                            gc.observacion, gm.tecnico, gm.id_empresa, 
                            t.idcedula, t.carnet_critico, h.horario, 
                            g.codigo_req codactu, g.codigo_del_cliente codcli, 
                            g.telefono, g.mdf, 
                            g.fftt, g.direccion, g.quiebre, 
                            g.lejano, g.llave, g.paquete, g.eecc_final, 
                            g.microzona, g.origen tipoactu, 
                            gm.fecha_agenda, gm.id_estado, gm.coordinado
                        FROM 
                            webpsi_criticos.gestion_rutina_manual_provision g
                        INNER JOIN 
                            webpsi_criticos.gestion_criticos gc 
                            ON gc.id=g.id_gestion
                        INNER JOIN 
                            webpsi_criticos.gestion_movimientos gm 
                            ON g.id_gestion=gm.id_gestion
                        INNER JOIN 
                            webpsi_criticos.estados e 
                            ON e.id=gm.id_estado
                        INNER JOIN 
                            webpsi_criticos.tecnicos t 
                            ON t.id=gm.idtecnico
                        INNER JOIN 
                            webpsi_criticos.horarios h 
                            ON h.id=gm.id_horario
                        WHERE 
                            gm.id IN (
                                SELECT
                                    MAX(gm2.id)
                                FROM 
                                    webpsi_criticos.gestion_movimientos gm2
                                WHERE 
                                    gm2.id_gestion=g.id_gestion
                                GROUP BY 
                                    gm2.id_gestion
                                )";
            
            if ($empresa !== "") {
                $sql .= " AND gm.id_empresa=?";
                $this->_data[] = $empresa;
            }
            
            if ($celula !== "") {
                $sql .= " AND t.idcedula=?";
                $this->_data[] = $celula;
            }
            
            if ($fini !== "" and $fend !== "") {
                $sql .= " AND gm.fecha_agenda BETWEEN ? AND ?";
                $this->_data[] = $fini;
                $this->_data[] = $fend;
            }
                        
            if ( $carnet !== "" ) 
            {
                $sql .= " AND t.carnet_critico=?";
                $this->_data[] = $carnet;
            }
            
            if ( $estado !== "" ) 
            {
                $sql .= $this->concatOr($estado, " OR gm.id_estado = ?");
            }

            if ( $task_id !== "" )
            {
                $sql .= " AND gc.id=?";
                $this->_data[] = $task_id;
            }

            if ( $codactu !== "")
            {
                $sql .=" AND g.codigo_req = '?' ";
                $this->_data[] = $codactu;
            }

            $sql .= " ) t ";
            
            //Ordenamiento: carnet, fecha, id
            $sql .= " ORDER BY carnet_critico, fecha_agenda, id";

            $bind = $db->prepare($sql);
            $bind->execute( $this->_data );
            
            $reporte = array();
            while ($data = $bind->fetch(PDO::FETCH_ASSOC)) {
                foreach ($data as $key=>$val) {
                    $data[$key] = utf8_encode($val);
                }
                $reporte[] = $data;
            }            
            
            $db->commit();
            $result["estado"] = true;
            $result["msg"] = "Datos recuperados";
            $result["data"] = $reporte;
            return $result;
        } catch (PDOException $error) {
            //var_dump($error);
            $db->rollback();
            $result["estado"] = false;
            $result["msg"] = $error->getMessage();
            return $result;
        }
    }
        
    private function concatOr($data, $condition){
        $this->_sql = "";
        $this->_array = explode(",", $data);
        
        if ($data === '_negative') 
        {
            $this->_sql = substr( str_replace("= ?", "IS NULL", $condition), 4);
        } else {
            foreach ($this->_array as $val) {
                $this->_sql .= " $condition";
                $this->_data[] = $val;
            }
            $this->_sql = substr($this->_sql, 4);
        }
        return " AND ( $this->_sql )";
    }

    public function getAgendaManual(
        $db, $empresa, $celula, $fini, $fend, $carnet, $estado , $task_id = ""
    ){
        try {
            $this->_data = array();
            //Iniciar transaccion
            $db->beginTransaction();

            //Query total, agendado en curso + liquidado
            $sql = "SELECT
                        gc.id, gc.id_atc, gc.nombre_cliente_critico,
                        gc.observacion, gm.tecnico, gm.id_empresa,
                        t.idcedula, t.carnet_critico, h.horario,
                        g.averia codactu, g.inscripcion codcli,
                        g.telefono, g.mdf,
                        g.fftt, g.direccion_instalacion direccion,
                        g.quiebre, g.lejano, g.llave,
                        g.paquete, g.eecc_final,
                        g.microzona, g.tipo_averia tipoactu,
                        gm.fecha_agenda, gm.id_estado
                    FROM
                        webpsi_criticos.gestion_rutina_manual g
                    INNER JOIN
                        webpsi_criticos.gestion_criticos gc
                        ON gc.id=g.id_gestion
                    INNER JOIN
                        webpsi_criticos.gestion_movimientos gm
                        ON g.id_gestion=gm.id_gestion
                    INNER JOIN
                        webpsi_criticos.estados e
                        ON e.id=gm.id_estado
                    INNER JOIN
                        webpsi_criticos.tecnicos t
                        ON t.id=gm.idtecnico
                    INNER JOIN
                        webpsi_criticos.horarios h
                        ON h.id=gm.id_horario
                    WHERE
                        gm.id IN (
                            SELECT
                                MAX(gm2.id)
                            FROM
                                webpsi_criticos.gestion_movimientos gm2
                            WHERE
                                gm2.id_gestion=g.id_gestion
                            GROUP
                                BY gm2.id_gestion
                            )";

            if ($empresa !== "") {
                $sql .= " AND gm.id_empresa=?";
                $this->_data[] = $empresa;
            }

            if ($celula !== "") {
                $sql .= " AND t.idcedula=?";
                $this->_data[] = $celula;
            }

            if ($fini !== "" and $fend !== "") {
                $sql .= " AND gm.fecha_agenda BETWEEN ? AND ?";
                $this->_data[] = $fini;
                $this->_data[] = $fend;
            }

            if ( $carnet !== "" )
            {
                $sql .= " AND t.carnet_critico=?";
                $this->_data[] = $carnet;
            }

            if ( $estado !== "" )
            {
                $sql .= $this->concatOr($estado, " OR gm.id_estado = ?");
            }

            if ( $task_id !== "" )
            {
                $sql .= " AND gc.id=?";
                $this->_data[] = $task_id;
            }


            //Ordenamiento: carnet, fecha, id
            $sql .= " ORDER BY carnet_critico, fecha_agenda, id";

            $bind = $db->prepare($sql);
            $bind->execute( $this->_data );

            $reporte = array();
            while ($data = $bind->fetch(PDO::FETCH_ASSOC)) {
                foreach ($data as $key=>$val) {
                    $data[$key] = utf8_encode($val);
                }
                $reporte[] = $data;
            }

            $db->commit();
            $result["estado"] = true;
            $result["msg"] = "Datos recuperados";
            $result["data"] = $reporte;
            return $result;
        } catch (PDOException $error) {
            //var_dump($error);
            $db->rollback();
            $result["estado"] = false;
            $result["msg"] = $error->getMessage();
            return $result;
        }
    }

    public function getAgendaManual_Provision(
        $db, $empresa, $celula, $fini, $fend, $carnet, $estado , $task_id = ""
    ){
        try {
            $this->_data = array();
            //Iniciar transaccion
            $db->beginTransaction();

            //Query total, agendado en curso + liquidado
            $sql = "SELECT
                        gc.id, gc.id_atc, gc.nombre_cliente_critico,
                        gc.observacion, gm.tecnico, gm.id_empresa,
                        t.idcedula, t.carnet_critico, h.horario,
                        g.codigo_req codactu, g.codigo_del_cliente codcli,
                        g.telefono, g.mdf,
                        g.fftt, g.direccion, g.quiebre,
                        g.lejano, g.llave, g.paquete, g.eecc_final,
                        g.microzona, g.origen tipoactu,
                        gm.fecha_agenda, gm.id_estado
                    FROM
                        webpsi_criticos.gestion_rutina_manual_provision g
                    INNER JOIN
                        webpsi_criticos.gestion_criticos gc
                        ON gc.id=g.id_gestion
                    INNER JOIN
                        webpsi_criticos.gestion_movimientos gm
                        ON g.id_gestion=gm.id_gestion
                    INNER JOIN
                        webpsi_criticos.estados e
                        ON e.id=gm.id_estado
                    INNER JOIN
                        webpsi_criticos.tecnicos t
                        ON t.id=gm.idtecnico
                    INNER JOIN
                        webpsi_criticos.horarios h
                        ON h.id=gm.id_horario
                    WHERE
                        gm.id IN (
                            SELECT
                                MAX(gm2.id)
                            FROM
                                webpsi_criticos.gestion_movimientos gm2
                            WHERE
                                gm2.id_gestion=g.id_gestion
                            GROUP BY
                                gm2.id_gestion
                            )";

            if ($empresa !== "") {
                $sql .= " AND gm.id_empresa=?";
                $this->_data[] = $empresa;
            }

            if ($celula !== "") {
                $sql .= " AND t.idcedula=?";
                $this->_data[] = $celula;
            }

            if ($fini !== "" and $fend !== "") {
                $sql .= " AND gm.fecha_agenda BETWEEN ? AND ?";
                $this->_data[] = $fini;
                $this->_data[] = $fend;
            }

            if ( $carnet !== "" )
            {
                $sql .= " AND t.carnet_critico=?";
                $this->_data[] = $carnet;
            }

            if ( $estado !== "" )
            {
                $sql .= $this->concatOr($estado, " OR gm.id_estado = ?");
            }

            if ( $task_id !== "" )
            {
                $sql .= " AND gc.id=?";
                $this->_data[] = $task_id;
            }

            //Ordenamiento: carnet, fecha, id
            $sql .= " ORDER BY carnet_critico, fecha_agenda, id";

            $bind = $db->prepare($sql);
            $bind->execute( $this->_data );

            $reporte = array();
            while ($data = $bind->fetch(PDO::FETCH_ASSOC)) {
                foreach ($data as $key=>$val) {
                    $data[$key] = utf8_encode($val);
                }
                $reporte[] = $data;
            }

            $db->commit();
            $result["estado"] = true;
            $result["msg"] = "Datos recuperados";
            $result["data"] = $reporte;
            return $result;
        } catch (PDOException $error) {
            //var_dump($error);
            $db->rollback();
            $result["estado"] = false;
            $result["msg"] = $error->getMessage();
            return $result;
        }
    }


    /**
     * @param $db  conexcion
     * @param $empresa id empresa
     * @param $celula  id celula  // se debe usar para tecnicos tareas pero no para ultimo movimientos
     * @param $fini
     * @param $fend
     * @param $carnet   carnet critico
     * @param $estado estados unidos por comas (,)
     * @param string $task_id  id de tarea de la tabla gestion_critico
     * @param string $tasks    , conjunto de tareas unidos por comas (,)
     * @param string $carnets conjuntos de carnets criticos unidos por  (',') para ser usados en un IN sql
     * @return mixed   retorna un array con todas los datos de las tarea de todos los tipos de averias , provision m rutina manual , rutina manual provision
     */
    function getAgendasTodos(
        $db, $empresa, $celula, $fini, $fend, $carnet, $estado , $task_id = "",$tasks = "" , $carnets = ""
    ){
        try {
            $this->_data = array();
            //Iniciar transaccion
            $db->beginTransaction();
            $db->exec('SET NAMES utf8');


            //filtros
            $where = " ";
            if(!empty($empresa)){
                $where .= " AND gm.id_empresa=$empresa ";
            }

            if(!empty($celula)){
                $where .= " AND t.idcedula=$celula  ";
            }

            if(!empty($estado)){
                $where .= " AND (  gm.id_estado  in ($estado) ) ";
            }


            //LA FECHA AGENDA NO SALE DE LA TABLA GESTION MOVIMIENTOS
            //SI NO DE UN SELECT DE LA MAYOR FECHA DE LSO MOVIMIENTOS DE UNA ACTIVIDAD
            // YA QUE CUADNO UNA ACTIVIDAD PADA A LIQUIDADO , SU FECHA DE AGENDA CAMBIA A 00-00-00
            if ($fini != "" and $fend != "") {
                $where .= " AND fg.fecha_agenda BETWEEN '$fini' AND '$fend'";
            }elseif ($fini != "") {
                $where .= " AND fg.fecha_agenda >= '$fini'";
            }


            if ( $carnet !== "" )
            {
                $where .= " AND t.carnet_critico='$carnet'";
            }

            // para consultar dentro de un grupo de carnets
            // solucion para saber el ultimo estado de las tareas por carnet tecnicoo
            //ya que hay inconsistencia en celulas
             if ( $carnets != "" )
            {
                $where .= " AND t.carnet_critico in ( '$carnets' )";

            }



            if ( $task_id !== "" )
            {
                $where .= " AND gc.id=$task_id";

            }
            if(!empty($tasks)){
                $where .= " AND (  gc.id  in ($tasks) ) ";
            }



            //Query total, agendado en curso + liquidado
            $sql = "select q.*, es.estado
 ,IF(q.fecha_agenda = DATE(NOW()),1,IF(q.fecha_agenda <  DATE(NOW()),0,2))  pendiente
  ,IF(q.fecha_agenda = DATE(NOW()),'hoy',IF(q.fecha_agenda <  DATE(NOW()),'pasados','futuros'))  programados
,ee.eecc
,ce.nombre
, CONCAT_WS(' ',u.apellido,u.nombre) supervisor
 from (
SELECT
                        gc.id, gc.id_atc, gc.nombre_cliente_critico,
                        gc.observacion, gm.tecnico, gm.id_empresa,
                        t.idcedula, t.carnet_critico, h.horario,
                        g.averia codactu, g.inscripcion codcli,
                        g.telefono, g.mdf,
                        g.fftt, g.direccion_instalacion direccion,
                        g.quiebre, g.lejano, g.llave,
                        g.paquete, g.eecc_final,
                        g.microzona, g.tipo_averia tipoactu,
                        fg.fecha_agenda
                        , gm.id_estado,gc.tipo_actividad ,gm.id_usuario
                    FROM
                        webpsi_criticos.gestion_averia g
                    INNER JOIN
                        webpsi_criticos.gestion_criticos gc
                        ON gc.id=g.id_gestion
                    INNER JOIN
                        webpsi_criticos.gestion_movimientos gm
                        ON g.id_gestion=gm.id_gestion
                    INNER JOIN
                        webpsi_criticos.estados e
                        ON e.id=gm.id_estado
                    INNER JOIN
                        webpsi_criticos.tecnicos t
                        ON t.id=gm.idtecnico
                    INNER JOIN
                        webpsi_criticos.horarios h
                        ON h.id=gm.id_horario
                    INNER JOIN
                        (select  id_gestion , max(fecha_agenda) fecha_agenda from webpsi_criticos.gestion_movimientos GROUP BY id_gestion ) fg
                        on fg.id_gestion = gc.id
                    WHERE
                        gm.id IN (
                            SELECT
                                MAX(gm2.id)
                            FROM
                                webpsi_criticos.gestion_movimientos gm2
                            WHERE
                                gm2.id_gestion=g.id_gestion
                            GROUP
                                BY gm2.id_gestion
                            )   $where
union ALL

SELECT
                        gc.id, gc.id_atc, gc.nombre_cliente_critico,
                        gc.observacion, gm.tecnico, gm.id_empresa,
                        t.idcedula, t.carnet_critico, h.horario,
                        g.codigo_req codactu, g.codigo_del_cliente codcli,
                        g.telefono, g.mdf,
                        g.fftt, g.direccion, g.quiebre,
                        g.lejano, g.llave, g.paquete, g.eecc_final,
                        g.microzona, g.origen tipoactu,
                        fg.fecha_agenda, gm.id_estado,gc.tipo_actividad ,gm.id_usuario
                    FROM
                        webpsi_criticos.gestion_provision g
                    INNER JOIN
                        webpsi_criticos.gestion_criticos gc
                        ON gc.id=g.id_gestion
                    INNER JOIN
                        webpsi_criticos.gestion_movimientos gm
                        ON g.id_gestion=gm.id_gestion
                    INNER JOIN
                        webpsi_criticos.estados e
                        ON e.id=gm.id_estado
                    INNER JOIN
                        webpsi_criticos.tecnicos t
                        ON t.id=gm.idtecnico
                    INNER JOIN
                        webpsi_criticos.horarios h
                        ON h.id=gm.id_horario
                    INNER JOIN
                        (select  id_gestion , max(fecha_agenda) fecha_agenda from webpsi_criticos.gestion_movimientos GROUP BY id_gestion ) fg
                        on fg.id_gestion = gc.id
                    WHERE
                        gm.id IN (
                            SELECT
                                MAX(gm2.id)
                            FROM
                                webpsi_criticos.gestion_movimientos gm2
                            WHERE
                                gm2.id_gestion=g.id_gestion
                            GROUP BY
                                gm2.id_gestion
                            ) $where
union ALL
SELECT
                        gc.id, gc.id_atc, gc.nombre_cliente_critico,
                        gc.observacion, gm.tecnico, gm.id_empresa,
                        t.idcedula, t.carnet_critico, h.horario,
                        g.averia codactu, g.inscripcion codcli,
                        g.telefono, g.mdf,
                        g.fftt, g.direccion_instalacion direccion,
                        g.quiebre, g.lejano, g.llave,
                        g.paquete, g.eecc_final,
                        g.microzona, g.tipo_averia tipoactu,
                        fg.fecha_agenda, gm.id_estado,gc.tipo_actividad ,gm.id_usuario
                    FROM
                        webpsi_criticos.gestion_rutina_manual g
                    INNER JOIN
                        webpsi_criticos.gestion_criticos gc
                        ON gc.id=g.id_gestion
                    INNER JOIN
                        webpsi_criticos.gestion_movimientos gm
                        ON g.id_gestion=gm.id_gestion
                    INNER JOIN
                        webpsi_criticos.estados e
                        ON e.id=gm.id_estado
                    INNER JOIN
                        webpsi_criticos.tecnicos t
                        ON t.id=gm.idtecnico
                    INNER JOIN
                        webpsi_criticos.horarios h
                        ON h.id=gm.id_horario
                    INNER JOIN
                        (select  id_gestion , max(fecha_agenda) fecha_agenda from webpsi_criticos.gestion_movimientos GROUP BY id_gestion ) fg
                        on fg.id_gestion = gc.id
                    WHERE
                        gm.id IN (
                            SELECT
                                MAX(gm2.id)
                            FROM
                                webpsi_criticos.gestion_movimientos gm2
                            WHERE
                                gm2.id_gestion=g.id_gestion
                            GROUP
                                BY gm2.id_gestion
                             ) $where
union ALL
SELECT
                        gc.id, gc.id_atc, gc.nombre_cliente_critico,
                        gc.observacion, gm.tecnico, gm.id_empresa,
                        t.idcedula, t.carnet_critico, h.horario,
                        g.codigo_req codactu, g.codigo_del_cliente codcli,
                        g.telefono, g.mdf,
                        g.fftt, g.direccion, g.quiebre,
                        g.lejano, g.llave, g.paquete, g.eecc_final,
                        g.microzona, g.origen tipoactu,
                        fg.fecha_agenda, gm.id_estado,gc.tipo_actividad ,gm.id_usuario
                    FROM
                        webpsi_criticos.gestion_rutina_manual_provision g
                    INNER JOIN
                        webpsi_criticos.gestion_criticos gc
                        ON gc.id=g.id_gestion
                    INNER JOIN
                        webpsi_criticos.gestion_movimientos gm
                        ON g.id_gestion=gm.id_gestion
                    INNER JOIN
                        webpsi_criticos.estados e
                        ON e.id=gm.id_estado
                    INNER JOIN
                        webpsi_criticos.tecnicos t
                        ON t.id=gm.idtecnico
                    INNER JOIN
                        webpsi_criticos.horarios h
                        ON h.id=gm.id_horario
                    INNER JOIN
                        (select  id_gestion , max(fecha_agenda) fecha_agenda from webpsi_criticos.gestion_movimientos GROUP BY id_gestion ) fg
                        on fg.id_gestion = gc.id
                    WHERE
                        gm.id IN (
                            SELECT
                                MAX(gm2.id)
                            FROM
                                webpsi_criticos.gestion_movimientos gm2
                            WHERE
                                gm2.id_gestion=g.id_gestion
                            GROUP BY
                                gm2.id_gestion
                             ) $where

) q
inner join webpsi_criticos.estados es on es.id  = q.id_estado
inner join webpsi.tb_eecc ee on ee.id = q.id_empresa
inner join webpsi_criticos.cedula ce on ce.idcedula = q.idcedula
inner join webpsi.tb_usuario u on u.id = q.id_usuario
order by q.carnet_critico,pendiente
";



            $bind = $db->prepare($sql);
            $bind->execute( );

            $reporte = array();
            while ($data = $bind->fetch(PDO::FETCH_ASSOC)) {
                foreach ($data as $key=>$val) {
                    $data[$key] = utf8_encode($val);
                }
                $reporte[] = $data;
            }

            $db->commit();
            $result["estado"] = true;
            $result["msg"] = "Datos recuperados";
            $result["data"] = $reporte;
            return $result;
        } catch (PDOException $error) {
            //var_dump($error);
            $db->rollback();
            $result["estado"] = false;
            $result["msg"] = $error->getMessage();
            return $result;
        }
    }


}
