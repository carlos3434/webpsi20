<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 12/11/14
 * Time: 04:45 PM
 */
$PATH = $_SERVER['DOCUMENT_ROOT'] . "/webpsi/";
require_once($PATH . '/clases/class.Conexion.php');
class CelulaGrupos {

    protected $cnx;

    public function __construct()
    {
        $con = new Conexion();
        $this->db = $con->conectarPDO();


    }


    /*
     * Registra la accion de actualizar o insertar
     */
    public function RegistrarGrupo( $grupo, $idcelula, $idtecnico, $operacion ) {

        if( is_numeric($idcelula) && is_numeric($idtecnico)){
            try {
                $db = $this->db;
                //Iniciar transaccion
                $db->beginTransaction();
                $deb = 1;
                //POR SI INSERTA O SE ELIMINA
                if($operacion == "true"){  //true para insertar 1
                    //VALIDAMOS QUE SI EXISTE
                    $sql = "select 1 from webpsi_officetrack.celula_grupos  where idcelula = $idcelula and idtecnico = $idtecnico  and idgrupo = $grupo";
                    $stmt = $db->query($sql);
                    $data = $stmt->fetch(PDO::FETCH_ASSOC);
                    //si no se encuentra registrado
                    if(!$data){
                        //iNSERTAMOS REGISTRO
                        $stmt = $db->prepare("INSERT INTO webpsi_officetrack.celula_grupos set idcelula = :celula , idgrupo = :grupo , idtecnico = :tecnico");
                        $stmt->execute(array(":celula"=>$idcelula,":tecnico"=>$idtecnico,":grupo"=>$grupo));
                    }
                }elseif($operacion == "false"){
                    //VALIDAMOS QUE SI EXISTE
                    $sql = "delete from webpsi_officetrack.celula_grupos  where idcelula = $idcelula and idtecnico = $idtecnico  and idgrupo = $grupo";
                    $db->exec($sql);
                }elseif($operacion == "todos"){
                    //borramos los que existian
                    $sql = "delete from webpsi_officetrack.celula_grupos  where idcelula = $idcelula and idtecnico = $idtecnico";
                    $db->exec($sql);
                    //agregamos los grupos
                    for($i=1;$i<11;$i++){
                        $stmt = $db->prepare("INSERT INTO webpsi_officetrack.celula_grupos set idcelula = :celula , idgrupo = :grupo , idtecnico = :tecnico");
                        $stmt->execute(array(":celula"=>$idcelula,":tecnico"=>$idtecnico,":grupo"=>$i));
                    }
                }elseif($operacion == "ninguno"){
                    //borramos los que existian
                    $sql = "delete from webpsi_officetrack.celula_grupos  where idcelula = $idcelula and idtecnico = $idtecnico";
                    $db->exec($sql);
                }


                $db->commit();
                $result["estado"] = true;
                $result["msg"] = "Datos recuperados";
                $result["data"] = $data;
                return $result;
            } catch (PDOException $error) {
                //var_dump($error);
                $db->rollback();
                $result["estado"] = false;
                $result["msg"] = $error->getMessage();
                return $result;
            }

        }else{

            $result["estado"] = false;
            $result["msg"] ="No se enviaron los datos correctamente";
            return $result;
        }

    }
} 