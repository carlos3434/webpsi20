<?php

class Celula
{

    private $_db = "webpsi_criticos";
    private $_table = "cedula";
    private $_data;
    
    protected $_id;
    protected $_nombre;
    protected $_activo;

    public function getCelulaEmp($db, $idempresa) {
        try {
            //Iniciar transaccion
            $db->beginTransaction();
            
            $sql = "SELECT 
                        idcedula idcelula, nombre, 
                        responsable, idempresa
                    FROM
                        $this->_db.$this->_table
                    WHERE
                        idempresa = ?";            
            
            $this->_data[] = $idempresa;
                        
            $bind = $db->prepare($sql);
            $bind->execute($this->_data);
            
            $reporte = array();
            while ($data = $bind->fetch(PDO::FETCH_ASSOC)) {
                foreach ($data as $key=>$val) {
                    $data[$key] = utf8_encode($val);
                }
                $reporte[] = $data;
            }            
            
            $db->commit();
            $result["estado"] = true;
            $result["msg"] = "Datos recuperados";
            $result["data"] = $reporte;
            return $result;
        } catch (PDOException $error) {
            //var_dump($error);
            $db->rollback();
            $result["estado"] = false;
            $result["msg"] = $error->getMessage();
            return $result;
        }
    }
    

}
