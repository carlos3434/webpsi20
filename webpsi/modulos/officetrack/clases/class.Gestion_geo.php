<?php

class Gestion_geo
{

    private $_db = "webpsi_officetrack";
    private $_table = "gestion_geo";
    private $_data;
    private $_array;
    private $_sql;
    
    public function deleteWaypoints($db){
        try {
            //Iniciar transaccion
            $db->beginTransaction();
            
            $sql = "DELETE FROM $this->_db.$this->_table";
                        
            $bind = $db->prepare($sql);
            $bind->execute();
            
            $reporte = array();
            
            $db->commit();
            $result["estado"] = true;
            $result["msg"] = "Datos eliminados";
            $result["data"] = $reporte;
            return $result;
        } catch (PDOException $error) {
            $db->rollback();
            $result["estado"] = false;
            $result["msg"] = $error->getMessage();
            return $result;
        }
    }
        
    public function saveWaypoint(
            $db, $user_id, $tipoactu, $codactu, $coord_x, $coord_y, 
            $distancia, $fftt="", $grupo="") {
        try {
            //Iniciar transaccion
            $db->beginTransaction();
            
            $sql = "INSERT INTO $this->_db.$this->_table 
                        (user_id, tipoactu, codactu, coord_x, coord_y, 
                        distancia, fftt, grupo, orden)
                    VALUES
                        (?, ?, ?, ?, ?, ?, ?, ?, 0)";
            
            $this->_data[] = $tipoactu;
            $this->_data[] = $codactu;
            $this->_data[] = $coord_x;
            $this->_data[] = $coord_y;
            
            $bind = $db->prepare($sql);
            $bind->execute(array(
                $user_id, $tipoactu, $codactu, $coord_x, 
                $coord_y, $distancia, $fftt, $grupo
                )
            );
            
            $reporte = array();
            
            $db->commit();
            $result["estado"] = true;
            $result["msg"] = "Datos registrados";
            $result["data"] = $reporte;
            return $result;
        } catch (PDOException $error) {
            $db->rollback();
            $result["estado"] = false;
            $result["msg"] = $error->getMessage();
            return $result;
        }
    }
    
    
    public function getAllWaypoints($db, $grupo="", $ordByDis=false, $ordByOrd=false){
        try {
            //Iniciar transaccion
            $db->beginTransaction();
            $whereArray = array();
            
            $sql = "SELECT 
                        id, tipoactu, codactu, coord_x, coord_y, orden
                    FROM 
                        $this->_db.$this->_table 
                    WHERE 
                        id IS NOT NULL";
            
            if ($grupo !== "") {
                $sql .= " AND grupo=?";
                $whereArray[] = $grupo;
            }
            
            if ($ordByOrd) {
                $sql .= " ORDER BY orden";
            }
            
            if ($ordByDis) {
                $sql .= " ORDER BY distancia";
            }
            
            $bind = $db->prepare($sql);
            $bind->execute($whereArray);
            
            $reporte = array();
            while ($data = $bind->fetch(PDO::FETCH_ASSOC)) {
                foreach ($data as $key=>$val) {
                    $data[$key] = utf8_encode($val);
                }
                $reporte[] = $data;
            }
            
            $db->commit();
            $result["estado"] = true;
            $result["msg"] = "Datos recuperados";
            $result["data"] = $reporte;
            return $result;
        } catch (PDOException $error) {
            $db->rollback();
            $result["estado"] = false;
            $result["msg"] = $error->getMessage();
            return $result;
        }
    }
    
    
    public function getAllWaypointsProvision($db, $grupo){
        try {
            //Iniciar transaccion
            $db->beginTransaction();
            
            $sql = "SELECT 
                        gg.id, gg.tipoactu, gg.codactu, 
                        gg.coord_x, gg.coord_y, gg.orden,
                        gp.id_gestion
                    FROM 
                        webpsi_officetrack.gestion_geo gg, 
                        webpsi_criticos.gestion_provision gp 
                    WHERE 
                        gg.grupo = ? AND gg.estado=2
                        AND gg.codactu=gp.codigo_req
                    ORDER BY 
                        orden";
                        
            $bind = $db->prepare($sql);
            $bind->execute(array($grupo));
            
            $reporte = array();
            while ($data = $bind->fetch(PDO::FETCH_ASSOC)) {
                foreach ($data as $key=>$val) {
                    $data[$key] = utf8_encode($val);
                }
                $reporte[] = $data;
            }
            
            $db->commit();
            $result["estado"] = true;
            $result["msg"] = "Datos recuperados";
            $result["data"] = $reporte;
            return $result;
        } catch (PDOException $error) {
            $db->rollback();
            $result["estado"] = false;
            $result["msg"] = $error->getMessage();
            return $result;
        }
    }
    
    
    public function getNoOrderWaypoints($db, $grupo){
        try {
            //Iniciar transaccion
            $db->beginTransaction();
            
            $sql = "SELECT 
                        id, tipoactu, codactu, coord_x, coord_y
                    FROM 
                        $this->_db.$this->_table 
                    WHERE 
                        grupo = '$grupo' AND orden = 0";
            
            $bind = $db->prepare($sql);
            $bind->execute();
            
            $reporte = array();
            while ($data = $bind->fetch(PDO::FETCH_ASSOC)) {
                foreach ($data as $key=>$val) {
                    $data[$key] = utf8_encode($val);
                }
                $reporte[] = $data;
            }
            
            $db->commit();
            $result["estado"] = true;
            $result["msg"] = "Datos recuperados";
            $result["data"] = $reporte;
            return $result;
        } catch (PDOException $error) {
            $db->rollback();
            $result["estado"] = false;
            $result["msg"] = $error->getMessage();
            return $result;
        }
    }
    
    public function getIniWaypoint($db, $grupo){
        try {
            //Iniciar transaccion
            $db->beginTransaction();
            
            $sql = "SELECT 
                        orden, id, tipoactu, codactu, coord_x, coord_y
                    FROM 
                        $this->_db.$this->_table
                    WHERE
                        grupo = '$grupo' 
                    ORDER BY 
                        orden 
                    DESC LIMIT 0,1";
            
            $bind = $db->prepare($sql);
            $bind->execute();
            
            $reporte = array();
            while ($data = $bind->fetch(PDO::FETCH_ASSOC)) {
                foreach ($data as $key=>$val) {
                    $data[$key] = utf8_encode($val);
                }
                $reporte[] = $data;
            }
            
            $db->commit();
            $result["estado"] = true;
            $result["msg"] = "Datos recuperados";
            $result["data"] = $reporte;
            return $result;
        } catch (PDOException $error) {
            $db->rollback();
            $result["estado"] = false;
            $result["msg"] = $error->getMessage();
            return $result;
        }
    }
    
    public function updateWaypoint($db, $orden, $id){
        try {
            //Iniciar transaccion
            $db->beginTransaction();
            
            $sql = "UPDATE 
                        $this->_db.$this->_table
                    SET 
                        orden = ? 
                    WHERE 
                        id = ?";
            
            $bind = $db->prepare($sql);
            $bind->execute(array($orden, $id));
            
            $reporte = array();
            
            $db->commit();
            $result["estado"] = true;
            $result["msg"] = "Datos recuperados";
            $result["data"] = $reporte;
            return $result;
        } catch (PDOException $error) {
            $db->rollback();
            $result["estado"] = false;
            $result["msg"] = $error->getMessage();
            return $result;
        }
    }
    
    
    public function updateGroupState($db, $estado, $grupo, $user_id){
        try {
            //Iniciar transaccion
            $db->beginTransaction();
            
            $sql = "UPDATE 
                        $this->_db.$this->_table
                    SET 
                        estado = ? 
                    WHERE 
                        grupo = ? 
                        AND user_id = ? 
                        AND estado = 1";
            
            $bind = $db->prepare($sql);
            $bind->execute(array($estado, $grupo, $user_id));
            
            $reporte = array();
            
            $db->commit();
            $result["estado"] = true;
            $result["msg"] = "Datos actualizados";
            $result["data"] = $reporte;
            return $result;
        } catch (PDOException $error) {
            $db->rollback();
            $result["estado"] = false;
            $result["msg"] = $error->getMessage();
            return $result;
        }
    }
    
    
    public function addGestionProvision(
            $db, $tmpArray, $empresa_id, $zonal_id, $user_id, $celula_id){
        try {
            
            date_default_timezone_set('America/Lima');
            
            $fecha = date("Y-m-d H:i:s");
            
            $tipoactu = $tmpArray["tipoactu"];
            $horas_actu = $tmpArray["horas_actu"];
            $fecha_registro = $tmpArray["fecha_registro"];
            $ciudad = $tmpArray["ciudad"];
            $codactu = $tmpArray["codactu"];
            $codigo_del_cliente = $tmpArray["codigo_del_cliente"];
            $fono1 = $tmpArray["fono1"];
            $telefono = $tmpArray["telefono"];
            $mdf = $tmpArray["mdf"];
            $obs_dev = $tmpArray["obs_dev"];
            $codigosegmento = $tmpArray["codigosegmento"];
            $estacion = $tmpArray["estacion"];
            $direccion_instalacion = $tmpArray["direccion_instalacion"];
            $distrito = $tmpArray["distrito"];
            $nombre_cliente = $tmpArray["nombre_cliente"];
            $orden = $tmpArray["orden"];
            $veloc_adsl = $tmpArray["veloc_adsl"];
            $servicio = $tmpArray["servicio"];
            $tipo_motivo = $tmpArray["tipo_motivo"];
            $tot_aver_cab = $tmpArray["tot_aver_cab"];
            $tot_aver_cob = $tmpArray["tot_aver_cob"];
            $tot_averias = $tmpArray["tot_averias"];
            $fftt = $tmpArray["fftt"];
            $llave = $tmpArray["llave"];
            $dir_terminal = $tmpArray["dir_terminal"];
            $fonos_contacto = $tmpArray["fonos_contacto"];
            $contrata = $tmpArray["contrata"];
            $zonal = $tmpArray["zonal"];
            $wu_nagendas = $tmpArray["wu_nagendas"];
            $wu_nmovimient = $tmpArray["wu_nmovimient"];
            $wu_fecha_ult_age = $tmpArray["wu_fecha_ult_age"];
            $tot_llam_tec = $tmpArray["tot_llam_tec"];
            $tot_llam_seg = $tmpArray["tot_llam_seg"];
            $llamadastec15d = $tmpArray["llamadastec15d"];
            $llamadastec30d = $tmpArray["llamadastec30d"];
            $quiebre = $tmpArray["quiebre"];
            $lejano = $tmpArray["lejano"];
            $des_distrito = $tmpArray["des_distrito"];
            $eecc_zon = $tmpArray["eecc_zon"];
            $zona_movuno = $tmpArray["zona_movuno"];
            $paquete = $tmpArray["paquete"];
            $data_multip = $tmpArray["data_multip"];
            $aver_m1 = $tmpArray["aver_m1"];
            $fecha_data_fuente = $tmpArray["fecha_data_fuente"];
            $telefono_codclientecms = $tmpArray["telefono_codclientecms"];
            $rango_dias = $tmpArray["rango_dias"];
            $sms1 = $tmpArray["sms1"];
            $sms2 = $tmpArray["sms2"];
            $area2 = $tmpArray["area2"];
            $veloc_caja_recomen = $tmpArray["veloc_caja_recomen"];
            $tipo_servicio = $tmpArray["tipo_servicio"];
            $tipo_actuacion = $tmpArray["tipo_actuacion"];
            $eecc_final = $tmpArray["eecc_final"];
            $microzona = $tmpArray["microzona"];
            $no_cod_req = $tmpArray["no_cod_req"];
            
            //Para la asignacion de tecnico
            $reg_tecnico_id  = "";
            $reg_tecnico_nom = "";
            
            //Numero de actuaciones asignadas por tecnico
            $natArray = $this->getActuTecnicoAsignado($db, $celula_id);
            if ( isset($natArray["data"][0]) ) {
                $reg_tecnico_id  = $natArray["data"][0]["idtecnico"];
                $reg_tecnico_nom = $natArray["data"][0]["nombre_tecnico"];
            }
            
            //Iniciar transaccion
            $db->beginTransaction();
            
            //Inserta en gestion_provision
            $sql = "INSERT INTO webpsi_criticos.gestion_criticos 
                    (
                        id_atc, nombre_cliente_critico,
                        telefono_cliente_critico, celular_cliente_critico,
                        fecha_agenda, id_horario, id_motivo,
                        id_submotivo, id_estado, observacion,
                        fecha_creacion, flag_tecnico, 
                        tipo_actividad, nmov, n_evento
                    ) 
                    VALUES 
                    (
                        '', '$nombre_cliente', 
                        '$telefono', '$telefono', 
                        '', 1, 2, 
                        2, 28, 'Planificado', 
                        '$fecha', '', 
                        'Provision', '1', '0'
                    )";
            $bind = $db->prepare($sql);
            $bind->execute();
            
            //ID gestion
            $id_gestion = $db->lastInsertId();
            
            //Actualizando el codigo de Atención
            $id_atc = "ATC_" . date("Y") . "_" . $id_gestion;
            $sql = "update webpsi_criticos.gestion_criticos 
                    set id_atc='$id_atc' where id=$id_gestion";
            $db->exec($sql);
            
            //Registro en tabla gestion_provision
            $sql = "INSERT INTO webpsi_criticos.gestion_provision 
                    (
                        id_gestion, origen,
                        horas_pedido, fecha_Reg, ciudad,
                        codigo_req, codigo_del_cliente, fono1,
                        telefono, mdf, obs_dev,
                        codigosegmento, estacion, direccion,
                        distrito, nomcliente, orden,
                        veloc_adsl, servicio, tipo_motivo,
                        tot_aver_cab, tot_aver_cob, tot_averias,
                        fftt, llave, dir_terminal,
                        fonos_contacto, contrata, zonal,
                        wu_nagendas, wu_nmovimient, wu_fecha_ult_age,
                        tot_llam_tec, tot_llam_seg, llamadastec15d,
                        llamadastec30d, quiebre, lejano,
                        des_distrito, eecc_zon, zona_movuno,
                        paquete, data_multip, aver_m1,
                        fecha_data_fuente, telefono_codclientecms, rango_dias,
                        sms1, sms2, area2,
                        tipo_actuacion, eecc_final, microzona
                    ) VALUES 
                    (
                        $id_gestion, '$tipoactu',
                        '$horas_actu', '$fecha_registro', '$ciudad',
                        '$codactu', '$codigo_del_cliente', '$fono1',
                        '$telefono', '$mdf', '$obs_dev',
                        '$codigosegmento', '$estacion', '$direccion_instalacion',
                        '$distrito', '$nombre_cliente', '$orden',
                        '$veloc_adsl', '$servicio', '$tipo_motivo',
                        '$tot_aver_cab', '$tot_aver_cob', '$tot_averias',
                        '$fftt', '$llave', '$dir_terminal',
                        '$fonos_contacto', '$contrata', '$zonal',
                        '$wu_nagendas', '$wu_nmovimient', '$wu_fecha_ult_age',
                        '$tot_llam_tec', '$tot_llam_seg', '$llamadastec15d',
                        '$llamadastec30d', '$quiebre', '$lejano',
                        '$des_distrito', '$eecc_zon', '$zona_movuno',
                        '$paquete', '$data_multip', '$aver_m1',
                        '$fecha_data_fuente', '$telefono_codclientecms', '$rango_dias',
                        '$sms1', '$sms2', '$area2',
                        '$tipo_actuacion', '$eecc_final', '$microzona'
                    )";
            $bind = $db->prepare($sql);
            $bind->execute();
            
            //Registro en tabla movimientos
            $sql = "INSERT INTO webpsi_criticos.gestion_movimientos 
                    (
                        id_gestion, id_empresa, id_zonal,
                        fecha_agenda, id_horario, id_dia,
                        id_motivo, id_submotivo, id_estado,
                        tecnicos_asignados, observacion, id_usuario,
                        fecha_movimiento, tecnico, fecha_consolidacion,idtecnico
                    ) VALUES 
                    (
                        $id_gestion, $empresa_id, $zonal_id,
                        '', 1, 1,
                        2, 2, 28,
                        '', 'Planificado', $user_id,
                        '$fecha', '$reg_tecnico_nom', '', '$reg_tecnico_id'
                    )";
            $bind = $db->prepare($sql);
            $bind->execute();
            
            $reporte = array();
            
            $db->commit();
            $result["estado"] = true;
            $result["msg"] = "Datos registrados";
            $result["data"] = $reporte;
            return $result;
        } catch (PDOException $error) {
            $db->rollback();
            $result["estado"] = false;
            $result["msg"] = $error->getMessage();
            return $result;
        }
        
    }
    
    /**
     * Retorna la cantidad de ordenes asignadas por tecnico.
     * Requiere el ID de la celula
     * 
     * @param type $db Obj. conexion a DB
     * @param type $celula_id ID de la celula
     * @return type
     */
    public function getActuTecnicoAsignado($db, $celula_id){
        try {
            //Iniciar transaccion
            $db->beginTransaction();
            
            $sql = "SELECT 
                        gc.id, gc.id_atc, gc.nombre_cliente_critico, 
                        gc.observacion, gm.tecnico, gm.id_empresa, 
                        t.idcedula, t.carnet_critico, h.horario, 
                        g.codigo_req codactu, g.codigo_del_cliente codcli, 
                        g.telefono, g.mdf, 
                        g.fftt, g.direccion, g.quiebre, 
                        g.lejano, g.llave, g.paquete, g.eecc_final, 
                        g.microzona, g.origen tipoactu, 
                        gm.fecha_agenda, gm.id_estado, t.id idtecnico,
                        t.nombre_tecnico,
                        IF(gc.id IS NULL,0,COUNT(gc.id)) nactu
                    FROM 
                            webpsi_criticos.tecnicos t 
                    LEFT JOIN 
                        webpsi_criticos.gestion_movimientos gm 
                            ON t.id=gm.idtecnico
                    LEFT JOIN 
                            webpsi_criticos.gestion_provision g
                            ON g.id_gestion=gm.id_gestion
                    LEFT JOIN 
                        webpsi_criticos.gestion_criticos gc 
                            ON gc.id=g.id_gestion
                    LEFT JOIN 
                        webpsi_criticos.estados e 
                        ON e.id=gm.id_estado   
                    LEFT JOIN 
                        webpsi_criticos.horarios h 
                        ON h.id=gm.id_horario
                    WHERE t.idcedula = ?
                            AND 
                            (    (
                                    gm.id_estado IN (1, 8, 2, 9, 10, 20, 28)
                                    AND gm.id IN (
                            SELECT
                                MAX(gm2.id)
                            FROM 
                                webpsi_criticos.gestion_movimientos gm2
                            WHERE 
                                gm2.id_gestion=g.id_gestion
                            GROUP BY 
                                gm2.id_gestion
                            )
                                )
                                OR gc.id IS NULL
                            )
                    GROUP BY mt.idtecnico
                    ORDER BY nactu, id";
            
            $bind = $db->prepare($sql);
            $bind->execute(array($celula_id));
            
            $reporte = array();
            while ($data = $bind->fetch(PDO::FETCH_ASSOC)) {
                foreach ($data as $key=>$val) {
                    $data[$key] = utf8_encode($val);
                }
                $reporte[] = $data;
            }
            
            $db->commit();
            $result["estado"] = true;
            $result["msg"] = "Datos recuperados";
            $result["data"] = $reporte;
            return $result;
        } catch (PDOException $error) {
            $db->rollback();
            $result["estado"] = false;
            $result["msg"] = $error->getMessage();
            return $result;
        }
    }
    
    
    public function getDatosCriticos($db, $id){
        try {
            //Iniciar transaccion
            $db->beginTransaction();
            
            $sql = "SELECT 
                        id_atc atc, nombre_cliente_critico nombre,
                        telefono_cliente_critico telefono, 
                        celular_cliente_critico celular,
                        observacion
                    FROM 
                        webpsi_criticos.gestion_criticos
                    WHERE
                        id = ?";
            
            $bind = $db->prepare($sql);
            $bind->execute( array($id) );
            
            $reporte = array();
            while ($data = $bind->fetch(PDO::FETCH_ASSOC)) {
                $reporte[] = $data;
            }
            
            $db->commit();
            $result["estado"] = true;
            $result["msg"] = "Datos recuperados";
            $result["data"] = $reporte;
            return $result;
        } catch (PDOException $error) {
            $db->rollback();
            $result["estado"] = false;
            $result["msg"] = $error->getMessage();
            return $result;
        }
    }
    
    /**
     * Actualiza el valor de n_evento = 1 
     * si la respuesta de Officetrack es OK
     * 
     * @param type $db Objeto de conexion a DB
     * @param type $gestion_id ID de gestion
     * @return array
     */
    public function updateAfterOt($db, $gestion_id){
        try {
            //Iniciar transaccion
            $db->beginTransaction();
            
            $sql = "UPDATE 
                        webpsi_criticos.gestion_criticos
                    SET 
                        n_evento = 1 
                    WHERE 
                        id = ?";
            
            $bind = $db->prepare($sql);
            $bind->execute(array($gestion_id));
            
            $reporte = array();
            
            $db->commit();
            $result["estado"] = true;
            $result["msg"] = "Datos actualizados";
            $result["data"] = $reporte;
            return $result;
        } catch (PDOException $error) {
            $db->rollback();
            $result["estado"] = false;
            $result["msg"] = $error->getMessage();
            return $result;
        }
    }
    
    /**
     * Obtiene el ultimo movimiento de una orden
     * para evaluar posteriormente si fue coordinada
     * 
     * @param type $db Objeto de conexion a DB
     * @param type $gestion_id ID de la gestion
     * @return array
     */
    public function getIfCoordinado($db, $gestion_id){
        try {
            //Iniciar transaccion
            $db->beginTransaction();
            
            $sql = "SELECT 
                        id, coordinado, fecha_agenda, idtecnico 
                    FROM 
                        webpsi_criticos.gestion_movimientos 
                    WHERE 
                        id_gestion = ? 
                    ORDER BY 
                        id DESC ";
            
            $bind = $db->prepare($sql);
            $bind->execute( array($gestion_id) );
            
            $reporte = array();
            while ($data = $bind->fetch(PDO::FETCH_ASSOC)) {
                $reporte[] = $data;
            }
            
            $db->commit();
            $result["estado"] = true;
            $result["msg"] = "Datos recuperados";
            if ( isset($reporte[0]) ) {
                $result["data"] = $reporte[0];
            } else {
                $result["data"] = array();
            }            
            return $result;
        } catch (PDOException $error) {
            $db->rollback();
            $result["estado"] = false;
            $result["msg"] = $error->getMessage();
            return $result;
        }
    }
    
    /**
     * Actualiza el tecnico asignado a una orden previamente coordinada
     * 
     * @param type $db Objeto conexion a DB
     * @param type $mov_id ID del movimiento
     * @param type $tec_id ID del tecnico
     * @param type $tecnico Nombre del tecnico
     * @return array
     */
    public function updTecnicoCoordinado($db, $mov_id, $tec_id, $tecnico){
        try {
            //Iniciar transaccion
            $db->beginTransaction();
            
            $sql = "UPDATE 
                        webpsi_criticos.gestion_movimientos
                    SET 
                        idtecnico = ?,
                        tecnico = ?
                    WHERE 
                        id = ?";
            
            $bind = $db->prepare($sql);
            $bind->execute(array($tec_id, $tecnico, $mov_id));
            
            $reporte = array();
            
            $db->commit();
            $result["estado"] = true;
            $result["msg"] = "Datos actualizados";
            $result["data"] = $reporte;
            return $result;
        } catch (PDOException $error) {
            $db->rollback();
            $result["estado"] = false;
            $result["msg"] = $error->getMessage();
            return $result;
        }
    }
    
    
}
