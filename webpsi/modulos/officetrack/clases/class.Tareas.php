<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 18/11/14
 * Time: 03:04 PM
 */
$PATH = $_SERVER['DOCUMENT_ROOT'] . "/webpsi/";
require_once( 'C:/server/http/webpsi20/webpsi/clases/class.Conexion.php');

class Tareas {

    protected $cnx;

    public function __construct()
    {
        $db = new Conexion();
        $cnx = $db->conectarPDO();
        $this->cnx = $cnx;
    }

    /*
     * Devuelve las tareas por estados officetrack en un rango de fecha dado
     */
    public function getTareas($fecha_ini , $fecha_fin , $empresa = "" , $celula = "" , $pasos = array() , $carnet = "")
    {
        $where = "";
        if(!empty($pasos)){
            $where .= " and ta.paso in ('". implode("','",$pasos) ."') ";
        }
        if($carnet != ""){
            $where .= " and ta.cod_tecnico = '".$carnet."' ";
        }
        if($empresa != ""){
            $where .= " and tec.id_empresa = $empresa ";
        }
        if($celula != ""){
           $where .= " and tec.idcelulas like  '%$celula%' ";
         }


        $sql="  select
                 gc.id_atc, ta.task_id , ta.cod_tecnico , ta.paso , ta.cliente ,ta. fecha_recepcion , ta.fecha_agenda
                , tec.idcelulas , tec.id_empresa , tec.nombre_tecnico
                , p3.estado , p3.observacion
                from webpsi_officetrack.tareas  ta
                inner JOIN (
                    select MAX(id) id    from webpsi_officetrack.tareas where fecha_recepcion BETWEEN '$fecha_ini' and '$fecha_fin' GROUP BY task_id , cod_tecnico
                    )  u on u.id = ta.id
                inner join (
                    select t. carnet_critico,t.nombre_tecnico  , t.id_empresa ,
                    GROUP_CONCAT(t.idcedula) idcelulas
                    from webpsi_criticos.tecnicos t
                    inner join webpsi_criticos.cedula ce on ce.idcedula = t.idcedula
                    where t.activo = 1 GROUP BY t.carnet_critico
                )tec on tec.carnet_critico = ta.cod_tecnico
                inner join webpsi_criticos.gestion_criticos gc on gc.id = ta.task_id
                left join webpsi_officetrack.paso_tres p3 on p3.task_id = ta.id
                where
                    ta.fecha_recepcion BETWEEN '$fecha_ini' and '$fecha_fin'
                     $where
                order by ta.cod_tecnico, ta.paso  , ta.fecha_recepcion desc, p3.estado;
                ";

        $res = $this->cnx->query($sql);
        while ($row = $res->fetch(PDO::FETCH_ASSOC))
        {
            $arr[] = $row;
        }
        $estado = 1;
        if(count($arr)<1){
            $estado = "";
        }

        return array("estado"=>$estado,"data"=>$arr);
    }


} 