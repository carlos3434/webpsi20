<?php

class Usuario_eecc
{

    private $_db = "webpsi";
    private $_table = "usuarios_eecc";
    private $_data;
        
    protected $_idusuarioeecc;
    protected $_idusuario;
    protected $_ideecc;
    protected $_activo;

    public function getEeccUser($db, $user) {
        try {
            //Iniciar transaccion
            $db->beginTransaction();
            
            $sql = "SELECT 
                        u.idusuarioeecc, u.idusuario, u.ideecc, e.eecc
                    FROM 
                        $this->_db.$this->_table u, webpsi.tb_eecc e 
                    WHERE 
                        u.ideecc=e.id AND e.estado=1 ";
            
            if ( $user !== "" ) 
            {
                $sql .= " AND u.idusuario = ?";
                $this->_data[] = $user;
            }
            
            $sql .= "ORDER BY e.eecc";
                        
            $bind = $db->prepare($sql);
            $bind->execute($this->_data);
            
            $reporte = array();
            while ($data = $bind->fetch(PDO::FETCH_ASSOC)) {
                $reporte[] = $data;
            }            
            
            $db->commit();
            $result["estado"] = true;
            $result["msg"] = "Datos recuperados";
            $result["data"] = $reporte;
            return $result;
        } catch (PDOException $error) {
            //var_dump($error);
            $db->rollback();
            $result["estado"] = false;
            $result["msg"] = $error->getMessage();
            return $result;
        }
    }

    public function getEecc($db) {
        try {
            //Iniciar transaccion
            $db->beginTransaction();

            $sql = "SELECT
                  id ideecc, eecc
                    FROM
                       webpsi.tb_eecc
                    WHERE
                        estado=1 ";



            $sql .= "ORDER BY eecc";

            $bind = $db->prepare($sql);
            $bind->execute($this->_data);

            $reporte = array();
            while ($data = $bind->fetch(PDO::FETCH_ASSOC)) {
                $reporte[] = $data;
            }

            $db->commit();
            $result["estado"] = true;
            $result["msg"] = "Datos recuperados";
            $result["data"] = $reporte;
            return $result;
        } catch (PDOException $error) {
            //var_dump($error);
            $db->rollback();
            $result["estado"] = false;
            $result["msg"] = $error->getMessage();
            return $result;
        }
    }

}
