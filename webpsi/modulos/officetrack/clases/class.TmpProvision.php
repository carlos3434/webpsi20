<?php

class TmpProvision
{

    private $_db = "webpsi_coc";
    private $_table = "tmp_provision";
    private $_data;
    private $_array;
    private $_sql;
        
    public function getTmp(
            $db, $zonal, $eecc, $tipo, $mdf, 
            $quiebre, $producto, $codigo, $orden_geo = false) {
        try {
            //Iniciar transaccion
            $db->beginTransaction();
            
            $sql = "SELECT
                        a.origen tipoactu, a.horas_pedido horas_actu, 
                        a.fecha_Reg fecha_registro, 
                        a.ciudad, a.codigo_req codactu, a.codigo_del_cliente, 
                        a.fono1, a.telefono, a.mdf, 
                        a.obs_dev, a.codigosegmento, a.estacion, 
                        a.direccion direccion_instalacion, a.distrito, 
                        a.nomcliente nombre_cliente, 
                        a.orden, a.veloc_adsl, a.servicio, 
                        a.tipo_motivo, a.tot_aver_cab, a.tot_aver_cob, 
                        a.tot_averias, a.fftt, a.llave, 
                        a.dir_terminal, a.fonos_contacto, a.contrata, 
                        a.zonal, a.wu_nagendas, a.wu_nmovimient, 
                        a.wu_fecha_ult_age, a.tot_llam_tec, a.tot_llam_seg, 
                        a.llamadastec15d, a.llamadastec30d, a.quiebre, 
                        a.lejano, a.des_distrito, a.eecc_zon, 
                        a.zona_movuno, a.paquete, a.data_multip, 
                        a.aver_m1, a.fecha_data_fuente, a.telefono_codclientecms, 
                        a.rango_dias, a.sms1, a.sms2, 
                        a.area2, a.veloc_caja_recomen, a.tipo_servicio, 
                        a.tipo_actuacion, a.eecc_final, a.microzona,
                        'Provision' tipoact, b.codigo_req no_cod_req[field_orden_geo]

                    FROM 
                        $this->_db.$this->_table a
                    LEFT JOIN 
                        webpsi_criticos.gestion_provision b 
                        ON a.codigo_req=b.codigo_req
                    [join_orden_geo]
                    WHERE
                        a.codigo_req IS NOT NULL";
            
            //Reemplazo si se busca orden tmp planificada
            if ($orden_geo === false) {
                $sql = str_replace(
                        "[field_orden_geo]", 
                        "", 
                        $sql
                    );
                $sql = str_replace(
                        "[join_orden_geo]", 
                        "", 
                        $sql
                    );
            } else if ($orden_geo === true) {
                $sql = str_replace(
                        "[field_orden_geo]", 
                        ", c.orden ordengeo", 
                        $sql
                    );
                $sql = str_replace(
                        "[join_orden_geo]", 
                        "JOIN 
			webpsi_officetrack.gestion_geo c 
			ON a.codigo_req=c.codactu ", 
                        $sql
                    );
            }
            
            if ($zonal !== "")
            {
                $sql .= " AND a.zonal = ?";
                $this->_data[] = $zonal;
            }
            
            if ($eecc !== "")
            {
                $sql .= " AND a.eecc_final LIKE CONCAT('%', ?, '%')";
                $this->_data[] = $eecc;
            }
            
            if ($tipo !== "")
            {
                $sql .= $this->concatOr(
                        $tipo, " OR a.origen LIKE CONCAT('%', ?, '%')");
            }
            
            if ($mdf !== "")
            {
                $sql .= " AND a.mdf = ?";
                $this->_data[] = $mdf;
            }
            
            if ($quiebre !== "")
            {
                $sql .= $this->concatOr($quiebre, " OR a.quiebre = ?");
            }
            
            if ($producto !== "")
            {
                $sql .= $this->concatOr($producto, " OR a.aver_m1 = ?");
            }
            
            if ($codigo !== "")
            {
                //$sql .= " AND a.codigo_req = ?";
                //$this->_data[] = $codigo;
                $sql .= $this->concatOr($codigo, " OR a.codigo_req = ?");
            }
            
            $sql .= " HAVING no_cod_req IS NULL";
            
            //Orden por geo planificacion
            if ($orden_geo===true) {
                $sql .= " ORDER BY ordengeo";
            }
            
            $bind = $db->prepare($sql);
            $bind->execute($this->_data);
            
            $reporte = array();
            while ($data = $bind->fetch(PDO::FETCH_ASSOC)) {
                foreach ($data as $key=>$val) {
                    $data[$key] = utf8_encode($val);
                }
                $reporte[] = $data;
            }            
            
            $db->commit();
            $result["estado"] = true;
            $result["msg"] = "Datos recuperados";
            $result["data"] = $reporte;
            return $result;
        } catch (PDOException $error) {
            $db->rollback();
            $result["estado"] = false;
            $result["msg"] = $error->getMessage();
            return $result;
        }
    }
    
    
    public function getAllQuiebre($db){
        try {
            //Iniciar transaccion
            $db->beginTransaction();
            
            $sql = "SELECT 
                        quiebre 
                    FROM 
                        $this->_db.$this->_table  
                    GROUP BY 
                        quiebre";
            
            $bind = $db->prepare($sql);
            $bind->execute();
            
            $reporte = array();
            while ($data = $bind->fetch(PDO::FETCH_ASSOC)) {
                foreach ($data as $key=>$val) {
                    $data[$key] = utf8_encode($val);
                }
                $reporte[] = $data;
            }            
            
            $db->commit();
            $result["estado"] = true;
            $result["msg"] = "Datos recuperados";
            $result["data"] = $reporte;
            return $result;
        } catch (PDOException $error) {
            $db->rollback();
            $result["estado"] = false;
            $result["msg"] = $error->getMessage();
            return $result;
        }
    }
    
    private function concatOr($data, $condition){
        $this->_sql = "";
        $this->_array = explode(",", $data);
                
        foreach ($this->_array as $val) {
            
            if ($val === '_negative') 
            {
                $this->_sql .= str_replace("= ?", "IS NULL", $condition);
            } else {
                $this->_sql .= " $condition";
                $this->_data[] = $val;
            }
            
        }
        
        $this->_sql = substr($this->_sql, 4);
        return " AND ( $this->_sql )";
    }

}
