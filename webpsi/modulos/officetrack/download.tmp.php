<?php
require_once '../../clases/class.Conexion.php';
require_once './clases/class.TmpAveria.php';
require_once './clases/class.TmpProvision.php';

$Conexion = new Conexion();
$TmpAveria = new TmpAveria();
$TmpProvision = new TmpProvision();

$db = $Conexion->conectarPDO();
$data = array();
$file = "";
$file_cab = "";

$tipo = $_GET["tipo"];
$codigo = $_GET["codes"];

if ($tipo === "Averia")
{
    $data = $TmpAveria->getTmp($db, "", "", "", "", "", "", $codigo);
} else if ($tipo === "Provision") {
    $data = $TmpProvision->getTmp($db, "", "", "", "", "", "", $codigo);
}

$cab = true;
foreach ($data["data"] as $key=>$val) {
    if ($file_cab !== "") {
        $cab = false;
    }
    foreach ($val as $id=>$dato) {
        $file .= $dato . "\t";
        if ($cab)
        {
            $file_cab .= $id . "\t";
        }
    }
    $file .= "\r\n";
}

header( 'Content-Type: application/octet-stream' );
header( 'Content-Disposition: attachment; filename=tmp_sin_xy.xls' );
header( 'Expires: 0' );
header( 'Cache-Control: must-revalidate, post-check=0, pre-check=0' );
header( "Content-Transfer-Encoding: binary" );
header( 'Pragma: public' );
echo $file_cab . "\r\n" . $file;
