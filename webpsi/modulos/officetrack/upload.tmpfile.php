<?php
session_start();
include_once 'autoload.php';
require_once '../georeferencia/clases/class.GeoUtils.php';

$cnxObj = new ConexionMapas();
$conexion = $cnxObj->conectarBD();

$GeoUtils = new GeoUtils();
/**
 * Upload icono personalizado
 */

//Debe existir la siguiente carpeta
$upload_folder = 'tmpfile';

$nombre_archivo = $_FILES['archivo']['name'];
$ext_archivo = end((explode(".", $nombre_archivo)));

$tipo_archivo = $_FILES['archivo']['type'];

$tamano_archivo = $_FILES['archivo']['size'];

$tmp_archivo = $_FILES['archivo']['tmp_name'];

//Nuevo nombre de archivo MD5
$archivo_nuevo = md5(date("YmdHis") . $nombre_archivo) . "." . $ext_archivo;

$file = $upload_folder . '/' . $nombre_archivo;

if (!move_uploaded_file($tmp_archivo, $file)) {
    $return = array(
        'upload' => FALSE, 
        'msg' => "Ocurrio un error al subir el archivo. No pudo guardarse.", 
        'error' => $_FILES['archivo']
    );
} else {
    $data = $GeoUtils->fileToJsonAddress($file);
    $return = array(
        'upload' => TRUE, 
        'data' => $data
    );
}

echo json_encode($return);
