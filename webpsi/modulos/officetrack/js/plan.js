var dataToDownload = "data:application/octet-stream,";

/**
 * Upload de archivo de direcciones
 * luego de subir el archivo invoca a la funcion de lectura:
 * readAddressTimer()
 * @returns {Boolean}
 */
function uploadTmpFile() {

    var inputFileImage = document.getElementById("file_tmp");
    var file = inputFileImage.files[0];
    var data = new FormData();
    var url = "ruta.ajax.php";

    data.append('archivoTmp', file);
    data.append('empresa_id', $(".empresa").val());
    data.append('empresa', $(".empresa option:selected").text());
    
    $("body").addClass("loading");

    $.ajax({
        url: url,
        type: 'POST',
        contentType: false,
        data: data,
        processData: false,
        cache: false,
        dataType: "json",
        success: function(datos) {
            if (datos.upload) {
                //Mostrar asignadas y temporales
                var nro_asg = 0;
                var nro_tmp = 0;
                var nro_not = 0;
                
                //No encontrados                
                $.each(datos.data, function (ind, codactu){
                    var notFound = true;
                    $.each(datos.agenda, function (id, field){
                        if (codactu === field.codactu)
                        {
                            notFound = false;
                            //console.log(codactu + "/" + field.codactu + "/" + notFound);
                        }
                    });
                    
                    if (notFound) {
                        if (typeof codactu !== 'undefined' && codactu!='')
                        {
                            dataToDownload += codactu + "%0A";
                            nro_not++;
                        }                        
                        //$(".no_tmp_file").append("<p>" + codactu + "</p>");
                    }
                });
                
                $(".downNotFound").attr("href", dataToDownload);
                $(".downNotFound").html("Download ( " + nro_not + " )");
                
                $.each(datos.agenda, function (id, field){
                    
                    if (typeof field.id_atc !== 'undefined' && field.id_atc!='')
                    {
                        //Gestionados
                        nro_asg++;
                        //Carnet de tecnico
                        var carnet = field.carnet_critico;

                        //Arreglo de actuaciones por tecnico
                        if (typeof tecActu[carnet] === "undefined") {
                            tecActu[carnet] = new Array();
                        }

                        //Agendas con XY (taps o terminales)
                        if (field.x !== "" && field.y !== "")
                        {
                            myLatlng = new google.maps.LatLng(field.y, field.x);

                            bounds.extend(myLatlng);

                            try {
                                //Marcador
                                marker = new google.maps.Marker({
                                    position: myLatlng,
                                    map: map,
                                    title: field.EmployeeNum,
                                    icon: "images/icons/" + field.icon,
                                    zIndex: zIndex++,
                                    draggable: true,
                                    estado: "",
                                    codactu: field.codactu,
                                    tipoactu: field.tipoactu,
                                    isTmp: false,
                                    idgestion: field.id,
                                    agenda: field.fecha_agenda,
                                    tipoact: field.tipoactu,
                                    coordinado: field.coordinado,
                                    fecha_agenda: field.fecha_agenda,
                                    horario: field.horario,
                                    tecnico: field.tecnico
                                });

                                //Efecto "BOUNCE" para las agendas pendientes o en curso
                                if ( field.id_estado != 3 && field.id_estado != 19 )
                                {
                                    //marker.setAnimation(google.maps.Animation.BOUNCE);
                                    marker.estado = "pendiente";
                                    /*
                                    circle = new google.maps.Circle({
                                        strokeColor: '#FF0000',
                                        strokeOpacity: 0.8,
                                        strokeWeight: 3,
                                        map: map,
                                        center: myLatlng,
                                        radius: 100
                                    });
                                    */
                                } else {
                                    marker.estado = "liquidado";
                                }

                                //Agrega marcados al arreglo de agendas por tecnico
                                tecActu[field.carnet_critico].push(marker);

                                infocontent =   "<div class=\"infow\" style=\"width:300px; height:200px; overflow: auto\">" +
                                                    "<input type=\"button\" id=\"detalle_actu\" value=\"Mostrar/Ocultar detalle\" onclick=\"mostrarOcultarDetalle()\">" + 
                                                    "<div class=\"detalle_actu\">" + 
                                                        field.tipoactu + "<br>" + 
                                                        field.nombre_cliente_critico + "<br>" + 
                                                        field.fecha_agenda + " / " + 
                                                        field.horario + "<br>" + 
                                                        field.observacion + "<br>" + 
                                                        field.direccion + "<br>" + 
                                                        field.codactu + "<br>" + 
                                                        field.fftt + "<br>" + 
                                                        field.codcli + "<br>" + 
                                                        field.mdf + "<br>" + 
                                                        field.id_atc + "<br>" + 
                                                        field.lejano + "<br>" +  
                                                        field.carnet_critico + "<br>" + 
                                                        field.paquete + "<br>" + 
                                                        field.quiebre + "<br>" + 
                                                        field.tecnico + "<br>" + 
                                                    "</div>";
                                                    if($.trim($("#sinagenda").val())==''){
                                infocontent+=       "<div><a href=\"javascript:void(0)\" onclick=\"gestionActu('" + field.id  + "', '0', '" + field.tipo_actividad + "')\">Gestionar <img src=\"../historico/img/gestionar.png\" style=\"vertical-align: middle\"></a>&nbsp;" + 
                                                    "<div class=\"detalle_gestion\"></div>";
                                                    }
                                infocontent+=       "<div><a href=\"javascript:void(0)\" onclick=\"get_detalle_paso('0001-|" + field.id_atc + "')\">1. Inicio</a>&nbsp;" + 
                                                    "<a href=\"javascript:void(0)\" onclick=\"get_detalle_paso('0002-|" + field.id_atc + "')\">2. Supervision</a>&nbsp;" + 
                                                    "<a href=\"javascript:void(0)\" onclick=\"get_detalle_paso('0003-|" + field.id_atc + "')\">3. Cierre</a></div>" +
                                                    "<div class=\"detalle_paso\"></div>" +                                    
                                                "</div>";                                    

                                doInfoWindow(marker, infocontent);
                            } catch (e) {
                                console.log(e);
                            }

                        } else {
                            //Agrega agendas sin XY, se agrega ATC
                            tecActu[this.carnet_critico].push(this.id_atc);
                        }
                        
                    } else {
                        //Numero temporales encontrados
                        nro_tmp++;
                        //Mostrar solo tmp con XY
                        if (field.x !== '' && field.y !== '' && field.x !== null && field.y !== null)
                        {
                            //Lat/Lng del temporal
                            myLatlng = new google.maps.LatLng(field.y, field.x);

                            bounds.extend(myLatlng);

                            //Marcador
                            marker = new google.maps.Marker({
                                position: myLatlng,
                                map: map,
                                title: field.EmployeeNum,
                                icon: "images/tmp_actu.png",
                                animation: google.maps.Animation.DROP,
                                zIndex: zIndex++,
                                draggable: true,
                                codactu: field.codactu,
                                tipoactu: field.tipoactu,
                                isTmp: true,
                                idgestion: 0,
                                tipoact: field.tipo_actividad,
                                coordinado: field.coordinado
                            });

                            //Agrega marcadores temporales por averia o Provision
                            tmpActu.push(marker);

                            //Infowindow
                            infocontent =   "<div class=\"infow\" style=\"width:300px; height:200px; overflow: auto\">" +
                                                "<input type=\"button\" id=\"detalle_actu\" value=\"Mostrar/Ocultar detalle\" onclick=\"mostrarOcultarDetalle()\">" + 
                                                "<div class=\"detalle_actu\">" + 
                                                    "Tipo: " + field.tipoactu + "<br>" + 
                                                    "Codigo: " + field.codactu + "<br>" + 
                                                    "Horas: " + field.horas_actu + "<br>" + 
                                                    "Fec. Registro: " + field.fecha_registro + "<br>" + 
                                                    field.nombre_cliente + "<br>" + 
                                                    field.direccion_instalacion + "<br>" + 
                                                    field.fftt + "<br>" + 
                                                    field.distrito + "<br>" + 
                                                    field.mdf + "<br>" + 
                                                    field.telefono + "<br>" +  
                                                    field.paquete + "<br>" +
                                                    field.x + " / " + field.y + "<br>" + 
                                                "</div>" +
                                                "<div><a href=\"javascript:void(0)\" onclick=\"gestionActuTmp('" + field.codactu  + "', '0', '" + field.tipo_actividad + "')\">Gestionar <img src=\"../historico/img/gestionar.png\" style=\"vertical-align: middle\"></a>&nbsp;" + 
                                                "<div class=\"detalle_gestion\"></div>" + 
                                            "</div>";

                            doInfoWindow(marker, infocontent);
                        }
                    }
                });
                //Actualizar numero tmp y asg
                $(".nro_asg_file").html(nro_asg);
                $(".nro_tmp_file").html(nro_tmp);
            } else {
                //Upload Error
                alert("Error al cargar el archivo, vuelva a intentarlo.");
            }
            
            $("body").removeClass("loading");
        },
        error: function(datos) {
            console.log(datos);
        }
    });
    return true;
}

function showHideActuPlan(action, element){
    var show = $("#" + element).prop("checked");
    
    var d = new Date();
    var month = d.getMonth()+1;
    var day = d.getDate();
    var output = d.getFullYear() + '-' +
    (month<10 ? '0' : '') + month + '-' +
    (day<10 ? '0' : '') + day;
        
    if (action==='today'){
        //Mostrar agendadas para hoy
        for (key in tecActu) {
            if (tecActu.hasOwnProperty(key))
            {
                $.each(tecActu[key], function(id, val){
                    if (typeof val === 'object')
                    {
                        if (!show && this.agenda===output)
                        {
                            //Ocultar agendas
                            this.setMap(null);
                        } else if (show && this.agenda===output) {
                            //Mostrar agendas
                            this.setMap(map);
                        }
                    }
                });                
            }
        }
    } else if (action==='notoday')
    {
        //Mostrar agendadas para <> hoy
        for (key in tecActu) {
            if (tecActu.hasOwnProperty(key))
            {
                $.each(tecActu[key], function(id, val){
                    if (typeof val === 'object')
                    {
                        if (!show && this.agenda!==output)
                        {
                            //Ocultar agendas
                            this.setMap(null);
                        } else if (show && this.agenda!==output) {
                            //Mostrar agendas
                            this.setMap(map);
                        }
                    }
                });                
            }
        }
    } else if (action==='tmp')
    {
        //Mostrar temporales
        $.each(tmpActu, function(id, val){
            if (typeof val === 'object')
            {
                if (!show)
                {
                    //Ocultar agendas
                    this.setMap(null);
                } else if (show) {
                    //Mostrar agendas
                    this.setMap(map);
                }
            }
        });
    }
    
}


function onDownload() {
    document.location = 'data:Application/octet-stream,' +
                         encodeURIComponent(dataToDownload);
}

function bounceActuPlan(codactu, checked){
    //Efecto bounce a la orden seleccionada
    $.each(polyActu, function (id, val){
        if (val.codactu===codactu && checked)
        {
            val.setAnimation(google.maps.Animation.BOUNCE);
        }
        if (val.codactu===codactu && !checked)
        {
            val.setAnimation(null);
        }
    });
}


$(document).ready(function (){
    //Buscar por archivo
    $("#buscar_tmp_file").click(function (){
        uploadTmpFile();
    });
    
    //Seleccionar empresa
    $(".empresa").change(function() {
        //Remover options
        $('.celula').find('option').remove();

        var id = $(this).val();
        
        if (id !== '')
        {
            $.ajax({
                type: "POST",
                url: "ruta.ajax.php",
                data: "getCelulaEmp=" + id,
                dataType: 'json',
                error: function(data) {
                    console.log(data);
                },
                success: function(data) {
                    //Agregar options
                    $(".celula").append(
                            "<option value=\"\">-Seleccione-</option>"
                        );
                    $.each(data, function() {
                        $(".celula").append(
                            "<option value=\"" 
                            + this.idcelula 
                            + "\">" 
                            + this.nombre 
                            + "</option>"
                        );
                    });
                }
            });
        }

    });
    
    //Cuadro de dialogo de planificacion
    $( "#dialog" ).dialog({
      autoOpen: false,
      width: 'auto'
    });
    
    
})