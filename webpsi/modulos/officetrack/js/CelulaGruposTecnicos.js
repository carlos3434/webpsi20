/**
 * Created by root on 21/11/14.
 */
//ESTE METODO ES LLAMDO DESDE OFFICETRACK  var grupo_html = "";
function MostrarGrupos(){
    $(".grupoCelula").multiselect({
        click: function(event, ui) {

            var idtec = $(this).attr("idtec");
            var idcel = $(this).attr("idcel");
            var grupo = ui.value;
            var estado = ui.checked;
            var self = this
            //EVENTO AL HACER CLICK
            $.ajax({
                type: "POST",
                url: "../public/visorMovil/visor.movil.ajax.php",
                dataType : "json",
                data: {
                    accion:"RegistrarGrupoTecnico",
                    idtec:idtec,
                    idcel:idcel,
                    grupo:grupo,
                    estado:estado
                },
                success: function (obj) {

                    var tr = $(self).parent().parent()
                    if(estado){
                        tr.addClass("g-"+grupo);
                    }else{
                        tr.removeClass("g-"+grupo);
                    }

                },
                error: function (e) { }
            });
        },
        checkAll: function(e){
            var idtec = $(this).attr("idtec");
            var idcel = $(this).attr("idcel");
            var self = this
            //EVENTO AL HACER CLICK
            $.ajax({
                type: "POST",
                url: "..//public/visorMovil/visor.movil.ajax.php",
                dataType : "json",
                data: {
                    accion:"RegistrarGrupoTecnico",
                    idtec:idtec,
                    idcel:idcel,
                    estado:"todos"
                },
                success: function (obj) {
                    var tr = $(self).parent().parent()
                    for(var i=1;i<11;i++){
                        tr.addClass("g-"+i);
                    }
                },
                error: function (e) { }
            });
        },
        uncheckAll: function(){
            var idtec = $(this).attr("idtec");
            var idcel = $(this).attr("idcel");
            var self = this
            //EVENTO AL HACER CLICK
            $.ajax({
                type: "POST",
                url: "..//public/visorMovil/visor.movil.ajax.php",
                dataType : "json",
                data: {
                    accion:"RegistrarGrupoTecnico",
                    idtec:idtec,
                    idcel:idcel,
                    estado:"ninguno"
                },
                success: function (obj) {
                    var tr = $(self).parent().parent()
                    for(var i=1;i<11;i++){
                        tr.removeClass("g-"+i);
                    }
                },
                error: function (e) { }
            });
        }
    });
}



$(function(){
    //VALIDACION DE GRUPO FILTRO MAIN QUE SOLO SE VEA CUANDO EXISTA SELECCIONA UNA CELULA
    if($("#celula").val() == null || $("#celula").val() == ""){
        $(".select-grupos").attr("disabled",true).val("");
    }

    $("#celula").change(function(){
        if($(this).val() != null || $("#celula").val() == ""){
            $(".select-grupos").attr("disabled",false);
        }
    });

    //multiples selects en la columna de tecnicos del visor
    //que aparace luego de hacer una busqueda
    $(".grupo-editar").click(function(e){
        e.preventDefault();
        MostrarGrupos();
    });

    $(".select-grupos").change(function(){

        $("#show_tec").prop("checked",false);
        showHideAll(); //OCULTO TODOS
        //MUESTRO EL GRUPO SELECCIONADO
        var grupo_selected = $(this).val();

        if(grupo_selected == ""){//muestro todos otra vez
            $("#show_tec").prop("checked",true);
            showHideAll();
            $(".tecRow").show()
            $(".showAllTec").show()
        }else{
            $(".showAllTec").hide()
            $(".tecRow").hide();
            $(".g-"+grupo_selected+" .chb_tec").trigger("click")
            $(".tecRow"+".g-"+grupo_selected).show();
        }
        //SINCRONIZAR AMBOS SELECTS CON EL MISMO VAL
        $(".select-grupos").val(grupo_selected)

    });

})