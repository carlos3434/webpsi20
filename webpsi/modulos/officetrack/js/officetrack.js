//Valores de coordenadas
var x;
var y;

//Un numero cualquiera
var xN = 0;

//Objeto mapa creado
var map;

//Marcador individual
var marker;

//Limites del mapa segun los elementos
var bounds;

//Limites de un solo elemento
var boundsElement;

var coord;

//Una ubicacion en particular
var myLatlng;

//Coordenadas de una polilinea
var drawPath;

//Contenido de un infowindow
var infocontent;

/**
 * Objeto Infowindow
 * @type google.maps.InfoWindow
 */
var infowindow;

/**
 * Ruta (coordenadas) de las actuaciones
 * @type Array
 */
var actuPath = [];

/**
 * Nombres de tecnicos asociados a carnet
 * @type Array
 */
var tecData = [];

/**
 * Lista de actuaciones por tecnico
 * @type Array
 */
var tecActu = [];

/**
 * Lista de actuaciones temporales, sin gestión
 * @type Array
 */
var tmpActu = [];

//Codigo de tecnico
var tecCode;

/**
 * Ubicaciones actuales (finales) de tecnicos como Google Markers
 * @type Array|@exp;marker
 */
var tecMark = {};

/**
 * Lista de tecnicos recuperados por carnet
 * @type Array
 */
var tecList = [];

/**
 * Ruta de un tecnico segun su ubicacion
 * @type Array
 */
var tecPath = [];

/**
 * z-index de los elementos del mapa
 * @type Number|Number
 */
var zIndex = 1;

/**
 * Coleccion de todos los elementos del mapa
 * @type Array
 */
var mapObjects = {};

/**
 * Elemento de cualquier tipo para ser usado globalmente
 * @type type
 */
var globalElement;

/**
 * Mostrar trafico en Google Maps
 * 
 * @type google.maps.TrafficLayer
 */
var trafficLayer

/**
 * Uso de direcciones y optimizacion de rutas.
 * 
 * @type google.maps.DirectionsService
 */
var directionsService

/**
 * Arreglo de Hitos de una ruta
 * 
 * @type Array
 */
var waypts = [];

/**
 * Objeto de poligonos creados para planificacion
 * 
 * @type type
 */
var planOverlay = {};



var polyActu = [];


var isTmpActu = false;
var idGestionActu = 0;

/**
 * Inicializa el mapa, centro ubicado en TdP Surquillo
 * @returns {Boolean}
 */
function initialize() {
    var mapOptions = {
        zoom: 10,
        center: new google.maps.LatLng(-12.033284, -77.0715493)
    };
    
    directionsService = new google.maps.DirectionsService();
    
    map = new google.maps.Map(document.getElementById('map-canvas'),
            mapOptions);
    
    //Inicializar infowindow
    infowindow = new google.maps.InfoWindow({
        content: "Loading..."
    });
    
    //Dibujar poligono
    google.maps.event.addDomListener(map.getDiv(), 'mousedown', function(e) {
        //do it with the right mouse-button only
        if (e.button != 2)
            return;
        //the polygon
        poly = new google.maps.Polyline({map: map, clickable: false});
        //move-listener
        var move = google.maps.event.addListener(map, 'mousemove', function(e) {
            poly.getPath().push(e.latLng);
        });
        //mouseup-listener
        google.maps.event.addListenerOnce(map, 'mouseup', function(e) {
            google.maps.event.removeListener(move);
            //if (document.getElementById('overlay').value == 'Polygon') {
                var path = poly.getPath();
                poly.setMap(null);
                poly = new google.maps.Polygon({map: map, path: path});
            //}
        });

    });
    
    var drawingManager = new google.maps.drawing.DrawingManager({
        //drawingMode: google.maps.drawing.OverlayType.MARKER,
        drawingControl: true,
        drawingControlOptions: {
            position: google.maps.ControlPosition.TOP_CENTER,
            drawingModes: [
                //google.maps.drawing.OverlayType.MARKER,
                //google.maps.drawing.OverlayType.CIRCLE,
                google.maps.drawing.OverlayType.POLYGON,
                //google.maps.drawing.OverlayType.POLYLINE,
                //google.maps.drawing.OverlayType.RECTANGLE
            ]
        },
        //markerOptions: {
        //    icon: 'images/beachflag.png'
        //},
        //circleOptions: {
        //    fillColor: '#ffff00',
        //    fillOpacity: 1,
        //    strokeWeight: 2,
        //    clickable: false,
        //    editable: true,
        //    zIndex: 1
        //}
    });
    
    //Al completar un polygon agregar el listener CLICk
    google.maps.event.addListener(drawingManager, 'overlaycomplete', function(event) {
        //event.type == google.maps.drawing.OverlayType.MARKER
        //var infowindow = new google.maps.InfoWindow({
        //    content: '<div id="content" onclick="testPoly()">Hello</div>',
        //    maxWidth: 10
        //});
        var element = event.overlay;
        
        google.maps.event.addListener(event.overlay, 'click', function(){
            if (typeof this.name === 'undefined')
            {
                this.name = prompt("Nombre", "");
            }
            
            planPoly(event, this);
        });
        
       
    });
    
    //Mostrar u ocultar herramientas de dibujo
    if (drawTool)
    {
        drawingManager.setMap(map);
    }
    
    return true;
}

function planPoly(event, polygon){
    
    var nPoliActu = 0;
    var pos;
    polyActu = [];
    globalElement = polygon;
    
    infocontent = "<div style=\"width: 300px; height: 150px; overflow: auto\">";
    
    $.each(tmpActu, function (){
        
        if (google.maps.geometry.poly.containsLocation(this.getPosition(), polygon)) 
        {
            /**
             * pos:
             * La última ubicación del marcador será 
             * la posición del infowindow
             */
            pos = this.getPosition();
            polyActu.push(this);
            infocontent += "<p>" + this.tipoactu + " / " + this.codactu + "</p>";            
            nPoliActu++;
        }
        
    });
    
    
    infocontent += "<p>" + nPoliActu + " trabajos</p>";
    infocontent += "<a href=\"javascript:void(0)\" onclick=\"doPlanPoly()\">Planificar</a>";
    infocontent += "&nbsp;<a href=\"javascript:void(0)\" onclick=\"doPlanPoly()\">Deshacer Plan</a>";
    infocontent += "&nbsp;<a href=\"javascript:void(0)\" onclick=\"deletePlanPoly()\">Borrar</a>";
    infocontent += "</div>";
    
    infowindow.setPosition(pos);
    infowindow.setContent(infocontent);
    infowindow.open(self.map);    
}

function doPlanPoly(){
    //Validar seleccion de Celula
    if ( $("#celula").val()=="" ) 
    {
        alert("Seleccione celula");
        $("#celula").focus();
        return false;
    }
    
    //Validar que los puntos no se encuentren en otra planificacion
    var doPlan = true;
    for (key in planOverlay) {
        if ( planOverlay.hasOwnProperty(key) ) 
        {
            $.each(planOverlay[key], function(id, val){
                $.each(polyActu, function (){
                    if (val.codactu === this.codactu)
                    {
                        doPlan = false;
                    }
                });
            });
        }
    }
    
    if (doPlan)
    {
        //Registrar en tabla
        var dataPlan = "";
        $.each(polyActu, function(){
            dataPlan += "|^" 
                     + this.tipoactu 
                     + "|" 
                     + this.codactu
                     + "|" 
                     + this.position.lat()
                     + "|" 
                     + this.position.lng();
        });
        
        $.ajax({
            type: "POST",
            url: "ruta.ajax.php",
            data: "doPathPlan=" + globalElement.name 
                    + "&tipoactu=" + $("#tipo_actu").val() 
                    + "&empresa=" + $("#empresa").val() 
                    + "&celula=" + $("#celula").val() 
                    + "&zonal=" + $("#zonal").val() 
                    + "&data=" + dataPlan,
            dataType: 'json',
            error: function(data) {
                console.log(data);
            },
            success: function(data) {
                //Planificados
                planOverlay[globalElement.name] = polyActu;
                
                //Eliminar del mapa
                $.each(polyActu, function(){
                    this.setMap(null);
                });
                
                //Mostrar orden planificado
                if (data.estado === true) {
                    
                    var n = 1;
                    var color = getRandomColor();
                    
                    //globalElement.setMap(null);
                    
                    $.each(data.data, function (ida, va){
                        $.each(polyActu, function(idp, vp){
                            
                            if (va.codactu == vp.codactu)
                            {
                                vp.icon = "http://chart.apis.google.com/chart" 
                                    + "?chst=d_map_pin_letter&chld="
                                    + n
                                    + "|" 
                                    + color.substring(1) 
                                    + "|"
                                    + textByColor(color).substring(1);
                                vp.isTmp = false;
                                vp.idgestion = va.id_gestion;
                                vp.setMap(map);
                            }
                        });
                        /*
                        marker = new google.maps.Marker({
                            position: myLatlng = new google.maps.LatLng(this.coord_y, this.coord_x),
                            icon: "http://chart.apis.google.com/chart" 
                                    + "?chst=d_map_pin_letter&chld="
                                    + n
                                    + "|" 
                                    + color.substring(1) 
                                    + "|"
                                    + textByColor(color).substring(1),
                            map: map,
                            zIndex: n * 100
                        });console.log(marker);
                        */
                        n++;
                    });
                    
                } else {
                    alert("Ocurrio un error");
                }
        
                console.log(planOverlay);
            }
        });
        
        
    } else {
        alert("Alguna(s) orden(s) ya se encuentran planificadas.");
    }
    
}

function deletePlanPoly(){
    globalElement.setMap(null);
}

function loadMarker(map, coord, icons, names) {
    
    var color;
    var nombre_tecnico = "";
    var carnet_tecnico = "";

    $.each(coord, function() {
        //Carnet 
        carnet_tecnico = this.EmployeeNum;
        
        //Ubicacion
        x = Number(this.X.replace(",", "."));
        y = Number(this.Y.replace(",", "."));
        myLatlng = new google.maps.LatLng(y, x);

        bounds.extend(myLatlng);
        
        //Color tecnico
        color = icons[this.EmployeeNum]['tec'].substring(4,10);
        
        //Marcador
        marker = new google.maps.Marker({
            position: myLatlng,
            icon: "images/icons/" + icons[this.EmployeeNum]['tec'],
            map: map,
            title: this.EmployeeNum,
            zIndex: zIndex++
        });
        
        $.each(names, function (){
            if (carnet_tecnico==this.carnet)
            {
                nombre_tecnico = this.nombre_tecnico;
            }
        });





        //DATA PARA VISOR PUBLICO MOVIL
        var   link_go = "";
        var   link_go_ocultar = "";
        if(window.usuario_movil){
            link_go = " - <a href=\"javascript:void(0)\" onClick=\"GoToTecnico('"+x+"','"+y+"')\"> >> Go </a>";
            link_go_ocultar = " - <a href=\"javascript:void(0)\" onClick=\"ocultarGoToTecnico()\"> Hide GoTo </a>";
        }
        var link_ultima_liquida = " - <a href=\"javascript:void(0)\" onclick=\"showLastLiq('" + carnet_tecnico + "')\">LIQ</a> ";

        //Detalle de ultima posicion
        infocontent = "<table>"
                + "<tr>"
                + "    <td rowspan=\"6\"><img src=\"imgtec/tecnico.png\"></td>"
                + "    <td></td>"
                + "</tr>"
                + "<tr>"
                + "    <td>" + this.EmployeeNum + "</td>"
                + "</tr>"
                + "<tr>"
                + "    <td>" + "<a href=\"tel:"+this.MobileNumber.substr(-9)+"\">" + this.MobileNumber + "</a></td>"
                + "</tr>"
                + "<tr>"
                + "    <td>" + nombre_tecnico + "</td>"
                + "</tr>"
                + "<tr>"
                + "    <td>" + this.t + "</td>"
                + "</tr>"
                + "<tr>"
                + "    <td><a href=\"javascript:void(0)\" onClick=\"doPath('" 
                    + this.EmployeeNum + "', '" 
                    + color + "')\">Mostrar ruta</a> " + link_go + link_ultima_liquida
                +  "</td>"
                + "</tr>"
                + "<tr>"
                + "    <td><a href=\"javascript:void(0)\" onClick=\"doNotPath('" 
                    + this.EmployeeNum 
                    + "')\">Ocultar ruta</a> " + link_go_ocultar  +" </td>"
                + "</tr>"
                + "</table>";

        //Crear infowindow de la ultima posicion
        doInfoWindow(marker, infocontent);

        if (typeof tecMark[this.EmployeeNum] === "undefined") {
            tecMark[this.EmployeeNum] = new Array();
        }
        tecMark[this.EmployeeNum] = marker;

    });
}


function mostrarTrafico(){
    var show = $("#show_traffic").prop("checked");
        
    //Trafico en mapa
    if (!show)
    {
        //Ocultar tecnicos
        trafficLayer.setMap(null);
    } else {
        //Mostrar tecnicos
        trafficLayer = new google.maps.TrafficLayer();
        trafficLayer.setMap(map);
    }
    
}

/**
 * Dibuja el recorrido de un tecnico por su ubicacion
 * @param {type} code Carnet de tecnico
 * @param {type} color Color de la ruta a dibujar
 * @returns {Boolean}
 */
function doPath(code, color) {
    //Get coords by address                
    $.ajax({
        type: "POST",
        url: "ruta.ajax.php",
        data: "codePath=" + code + "&pdate=" + $("#agenda").val(),
        dataType: 'json',
        error: function(data) {
            console.log(data);
        },
        success: function(data) {
            
            if(data.data.length === 0)
            {
                alert("No se encontro ruta para el tecnico seleccionado.");
                return false;
            }

            if (data.estado === true)
            {
                var n = 1;
                var markerIcon;
                tecPath = [];
                
                /**
                 * Bounds para un solo elemento: path, marker, etc.
                 */
                boundsElement = new google.maps.LatLngBounds();
                
                $.each(data.data, function() {
                    x = Number(this.X.replace(",", "."));
                    y = Number(this.Y.replace(",", "."));
                    myLatlng = new google.maps.LatLng(y, x);

                    tecPath.push(myLatlng);

                    boundsElement.extend(myLatlng);
                    
                    //Marcador de inicio
                    if (n===1)
                    {
                        markerIcon = "http://chart.apis.google.com/chart" 
                            + "?chst=d_map_pin_letter&chld=1|" 
                            + color
                            + "|"
                            + textByColor("#" + color).substring(1,7);
                    } else {
                        markerIcon = "images/Marker-Ball-Pink.png";
                    }

                    //Marcador
                    marker = new google.maps.Marker({
                        position: myLatlng,
                        map: map,
                        title: this.EmployeeNum,
                        icon: markerIcon,
                        zIndex: zIndex++
                    });

                    if (typeof mapObjects[this.EmployeeNum] === "undefined") {
                        mapObjects[this.EmployeeNum] = new Array();
                    }
                    mapObjects[this.EmployeeNum].push(marker);
                    tecCode = this.EmployeeNum;


                    //Contenido + Infowindow
                    infocontent = this.EmployeeNum
                            + "<br>"
                            + this.t
                            + "<br>"
                            + this.Battery
                            + "<br>"
                            + "<a href=\"javascript:void(0)\" onClick=\"doNotPath('" + this.EmployeeNum + "')\">Ocultar ruta</a>";

                    doInfoWindow(marker, infocontent);

                    n++;
                });
                map.fitBounds(boundsElement);

                //Dibujar Path
                drawPath = new google.maps.Polyline({
                    path: tecPath,
                    geodesic: true,
                    strokeColor: '#' + color,
                    strokeOpacity: 1.0,
                    strokeWeight: 3
                });

                drawPath.setMap(map);
                mapObjects[code].push(drawPath);
            } else {
                console.log("Error");
            }
        }
    });
    return true;
}

/**
 * Oculta una ruta de tecnico
 * 
 * @param {type} code
 * @returns {undefined}
 */
function doNotPath(code) {
    try {
        //Elimina ruta de un tecnico
        $.each(mapObjects[code], function() {
            this.setMap(null);
        });
    } catch (e) {
        console.log(e);
    }
}

/**
 * Limpia (oculta) todos elementos del mapa
 * @returns {Boolean}
 */
function clearMap() {
    try {
        //Elimina ultima ubicacion tecnicos
        $.each(tecMark, function() {
            this.setMap(null);
        });
        
        //Elimina localizacion de actuaciones
        $.each(tecActu, function() {
            this.setMap(null);
        });
        
        //Elimina localizacion de actuaciones temporales
        $.each(tmpActu, function() {
            this.setMap(null);
        });

        //Elimina ruta de tecnicos
        $.each(mapObjects, function(id, obj) {
            doNotPath(id);
        });
        
        //Variables en blanco
        actuPath = [];
        tecData = [];
        tecActu = [];
        tmpActu = [];
        tecMark = {};
        tecList = [];
        tecPath = [];
        zIndex = 1;
        mapObjects = {};
        bounds = new google.maps.LatLngBounds();
        
        initialize();
        
        return true;
    } catch (e) {
        console.log(e);
    }
}

/**
 * Ajusta los limites del mapa de acuerdo a las
 * coordenadas de sus elementos
 * @returns {Boolean}
 */
function fitmap(){
    map.fitBounds(bounds);
    return true;
}

/**
 * Oculta un tecnico y sus actuaciones por su carnet
 * @param {type} id Carnet de tecnico
 * @returns {Boolean}
 */
function hideAllTec(id) {
    try {
        //Elimina ultima ubicacion tecnicos
        $.each(tecActu[id], function() {
            this.setMap(null);
        });
        
        //Elimina ruta de tecnicos
        $.each(mapObjects, function(id, obj) {
            doNotPath(id);
        });
        return true;
    } catch (e) {
        console.log(e);
    }
}

function doInfoWindow(element, content) {
    google.maps.event.addListener(element, 'click', (
        function(marker, infocontent, infowindow) {
            return function() {
                globalElement = marker;
                isTmpActu = marker.isTmp;
                idGestionActu = marker.idgestion;
                infowindow.setContent(infocontent);
                infowindow.open(map, marker);
            };
        })(element, content, infowindow));
}

function batteryIcon(level){
    var icon = "";
    
    if (level >=0 && level < 10)
    {
        icon = "0";
    } else if (level >=10 && level < 20)
    {
        icon = "10";
    } else if (level >=20 && level < 40)
    {
        icon = "20";
    } else if (level >=40 && level < 60)
    {
        icon = "40";
    } else if (level >=60 && level < 80)
    {
        icon = "60";
    } else if (level >=80 && level < 90)
    {
        icon = "80";
    } else if (level >=90 && level <= 100)
    {
        icon = "100";
    } else {
        icon = "0";
    }
    
    icon = "<img alt=\"" + level + "%\" title=\"" + level + "%\" src=\"images/battery/battery_" 
            + icon 
            + "percent.png\" style=\"height:24px; vertical-align:middle\">";
    
    return icon;
}

function doTecList(teclist, coord, icons) {
    try {
        var n = 0;
        var carnet;
        var tecnico = "";
        var color;
        var batteryLevel = 0;
        
        $.each(teclist, function() {
            
            n++;
            carnet = this.carnet;

            if (typeof mapObjects[carnet] === "undefined") {
                mapObjects[carnet] = new Array();
            }

            //Nivel de bateria del tecnico
            var tec_phone = "";
            var tec_lastUpdate = "";
            var tec_coorx= "";
            var tec_coory= "";
            $.each(coord, function(){
                if (carnet == this.EmployeeNum)
                {

                    batteryLevel = Number(this.Battery);
                    tec_phone = this.MobileNumber.substr(-9)
                    tec_lastUpdate = this.t.substr(-8)
                    tec_coorx = this.X.replace(",",".")
                    tec_coory = this.Y.replace(",",".")
                }
            });

            var go_gmap = "";
            tecnico = this.carnet + " / " + this.nombre_tecnico

            var link_ultima_liquida = " <a href=\"javascript:void(0)\" onclick=\"showLastLiq('" + carnet + "')\">LIQ</a> ";

            if(window.usuario_movil){
                tecnico = this.carnet + " / " + "<a href=\"tel:"+tec_phone+"\">" + this.nombre_tecnico + "</a>"+" ";
                if(tec_coory != "" && tec_coorx != ""){
                    go_gmap = " http://maps.google.com/maps?saddr="+usuario_movil_y+","+ usuario_movil_x +"&daddr="+ tec_coory + ","+ tec_coorx;
                    go_gmap = " <a class='go-map' href='"+go_gmap+"'> <img src='images/car.png' style=\"height: 24px; vertical-align: middle\"> </a>";
                }
            }


            //AGREGANDOP GRUPO POR CELULA
            var grupo_html = "";
            var celula = $("#celula").val();
            var grupos = this.grupos.split(",")

            grupo_html += "<select idcel='"+celula+"' idtec='"+this.id+"' id='grupo-"+carnet+"' class='grupoCelula' carnet='"+carnet +"' multiple style='display:none;'>"

            for(var i=1; i<11;i++){
                var existe = $.inArray(i+"",grupos); // existe tendra el indice del item del array
                var selected =""
                if(existe >-1){
                    selected = "selected='selected'";
                }
                grupo_html+= "<option value='"+i+"' "+selected+">Grupo "+i+"</option>";
            }

            grupo_html +="</select>";


            var grupos_class =""
            grupos.forEach(function(i){
                grupos_class += " g-"+i
            });


            color = icons[carnet].tec.substring(4,10);
            $("#tec-list").append(
                "<div class=\"tecRow "+grupos_class+ " \">"
                +    "<div>"
                +        "<span>" + n + ".</span>"
                +        "<span>"  + tecnico +  "</span>"
                +            grupo_html
                +    "</div>"
                +    "<div style=\"margin-left: 12px;\">"
                +        "<input type=\"checkbox\" value=\"" + carnet  + "\" class=\"chb_tec\" checked>"
                +        "<a href=\"javascript:void(0)\" onclick=\"showActuTec('" + carnet + "', 'asg')\"><img src=\"images/icons/" + icons[carnet].tec + "\" style=\"height: 24px; vertical-align: middle\" alt=\"Info Tecnico\" title=\"Info Tecnico\"></a>"
                +        "<a href=\"javascript:void(0)\" onclick=\"doPath('" + carnet + "', '" + color + "')\"><img src=\"images/icons/" + icons[carnet].car + "\" style=\"height: 24px; vertical-align: middle\" alt=\"Ruta Tecnico\" title=\"Ruta Tecnico\"></a>"
                +        "<a href=\"javascript:void(0)\" onclick=\"actuTecPath('" + carnet + "')\"><img src=\"images/icons/" + icons[carnet].cal + "\" style=\"height: 24px; vertical-align: middle\" alt=\"Ruta Agendas\" title=\"Ruta Agendas\"></a>"
                +        batteryIcon(batteryLevel)
                +        "<span id=\"nagtec_" + carnet + "\">(00)</span>"  + tec_lastUpdate + go_gmap + link_ultima_liquida
                +    "</div>"
                +"</div>"
            );

        });
        
        //Mostrar y marcar checkbox
        if (n >= 1) 
        {
            $(".show_hide_tec").show();
            $("#show_tec").prop('checked', true);
        }
        
        //Class chb_tec
        $(".chb_tec").click(function (){
            showHideTec($(this).val(), $(this).prop("checked"));
        });
        
    } catch (e) {
        console.log(e);
    }
}

function openInfoWin(code) {
    try {
        if ( typeof tecMark[code] === 'undefined' )
        {
            throw "[show]T\u00E9cnico sin ubicaci\u00F3n.";
        }
        google.maps.event.trigger(tecMark[code], 'click');
    } catch (e) {
        errorMessage(e);
    }
}

function errorMessage(errMsg){
    var header = errMsg.substring(0, 6);
    if( header === '[show]' )
    {
        alert( errMsg.substring(6) );
    } else if (header === '[hide]') {
        console.log(errMsg.substring(6));
    }
}

function showAgendaCel(data, icons){
    var carnet;
    var agendaIcon = "";
    var tipo_actu = $("#tipo_actu").val();
    if (tipo_actu === "Averia") 
    {
        tipo_actu = "Averias";
    }
    
    $.each(data, function(){
        
        //Carnet de tecnico
        carnet = this.carnet_critico;
        
        //Arreglo de actuaciones por tecnico
        if (typeof tecActu[carnet] === "undefined") {
            tecActu[carnet] = new Array();
        }
        
        //Agendas con XY (taps o terminales)
        if (this.x !== "" && this.y !== "")
        {
            myLatlng = new google.maps.LatLng(this.y, this.x);

            bounds.extend(myLatlng);

            $.each(icons, function(id, val){
                if (carnet === id)
                {
                    agendaIcon = val.cal;
                }
            });
            
            try {
                //Marcador
                marker = new google.maps.Marker({
                    position: myLatlng,
                    map: map,
                    title: this.EmployeeNum,
                    icon: "images/icons/" + agendaIcon,
                    zIndex: zIndex++,
                    codactu: this.codactu,
                    tipoactu: this.tipoactu,
                    isTmp: false,
                    idgestion: this.id,
                    estado: "",
                    coordinado: this.coordinado,
                    carnet: this.carnet_critico
                });


                //Efecto "BOUNCE" para las agendas pendientes o en curso
                if ( this.id_estado != 3 && this.id_estado != 19 )
                {
                    //marker.setAnimation(google.maps.Animation.BOUNCE);
                    marker.estado = "pendiente";
                    /*
                    circle = new google.maps.Circle({
                        strokeColor: '#FF0000',
                        strokeOpacity: 0.8,
                        strokeWeight: 3,
                        map: map,
                        center: myLatlng,
                        radius: 100
                    });
                    */
                } else {
                    marker.estado = "liquidado";
                }

                //Agrega marcados al arreglo de agendas por tecnico
                tecActu[this.carnet_critico].push(marker);

                infocontent =   "<div class=\"infow\" style=\"width:300px; height:200px; overflow: auto\">" +
                                    "<input type=\"button\" id=\"detalle_actu\" value=\"Mostrar/Ocultar detalle\" onclick=\"mostrarOcultarDetalle()\">" + 
                                    "<div class=\"detalle_actu\">" + 
                                        this.tipoactu + "<br>" + 
                                        this.nombre_cliente_critico + "<br>" + 
                                        this.fecha_agenda + " / " + 
                                        this.horario + "<br>" + 
                                        this.observacion + "<br>" + 
                                        this.direccion + "<br>" + 
                                        this.codactu + "<br>" + 
                                        this.fftt + "<br>" + 
                                        this.codcli + "<br>" + 
                                        this.mdf + "<br>" + 
                                        this.id_atc + "<br>" + 
                                        this.lejano + "<br>" +  
                                        this.carnet_critico + "<br>" + 
                                        this.paquete + "<br>" + 
                                        this.quiebre + "<br>" + 
                                        this.tecnico + "<br>" + 
                                    "</div>";
                                    if($.trim($("#sinagenda").val())==''){
                infocontent+=       "<div><a href=\"javascript:void(0)\" onclick=\"gestionActu('" + this.id  + "', '0', '" + tipo_actu + "')\">Gestionar <img src=\"../historico/img/gestionar.png\" style=\"vertical-align: middle\"></a>&nbsp;" + 
                                    "<div class=\"detalle_gestion\"></div>";
                                    }
                infocontent+=       "<div><a href=\"javascript:void(0)\" onclick=\"get_detalle_paso('0001-|" + this.id_atc + "')\">1. Inicio</a>&nbsp;" + 
                                    "<a href=\"javascript:void(0)\" onclick=\"get_detalle_paso('0002-|" + this.id_atc + "')\">2. Supervision</a>&nbsp;" + 
                                    "<a href=\"javascript:void(0)\" onclick=\"get_detalle_paso('0003-|" + this.id_atc + "')\">3. Cierre</a></div>" +
                                    "<div class=\"detalle_paso\"></div>" +                                    
                                "</div>";                                    
            
                doInfoWindow(marker, infocontent);
            } catch (e) {
                console.log(e);
            }
            
        } else {
            //Agrega agendas sin XY, se agrega ATC
            tecActu[this.carnet_critico].push(this.id_atc);
        }
        
    });
    
    /**
     * Completar cantidad de agendas
     * al generar lista de tecnicos
     */
    for (key in tecActu)
    {        
        if (typeof tecActu[key] !== 'undefined')
        {
            xN = tecActu[key].length;
            $("#nagtec_" + key).html( "(" +  pad(xN, 2) + ")" );
        }
    }
    
    fitmap();
}

function mostrarOcultarDetalle(){
    $( ".detalle_actu" ).toggle( "slow" );
}

function printData(){
    $(".detalle_paso").html(12211);
}

function get_detalle_paso(paso){
        
    $.ajax({
        type: "POST",
        url: "ruta.ajax.php",
        data: "getDetallePaso=" + paso,
        error: function(data) {
            console.log("Error:");
            console.log(data);
        },
        success: function(data) {
            $(".detalle_paso").html(data);
        }
    });
}

function showActuTec(id, tipo){
    try {
        openInfoWin(id);
        /*
        $.ajax({
            type: "POST",
            url: "ruta.ajax.php",
            data: "getAgendaTec=" + id 
                   + "&tipo=" + tipo 
                   + "&empresa=" + $("#empresa").val()
                   + "&celula=" + $("#celula").val()
                   + "&agenda=" + $("#agenda").val(),
            dataType: 'json',
            error: function(data) {
                console.log(data);
            },
            success: function(data) {
                $.each(data.data, function(){
                    myLatlng = new google.maps.LatLng(this.y, this.x);

                    bounds.extend(myLatlng);

                    //Marcador
                    marker = new google.maps.Marker({
                        position: myLatlng,
                        map: map,
                        title: this.EmployeeNum,
                        icon: "images/alert-triangle-red.png",
                        zIndex: zIndex++
                    });
                    
                    if (typeof tecActu[id] === "undefined") {
                        tecActu[id] = new Array();
                    }
                    tecActu[id].push(marker);
                    
                    infocontent =   this.nombre_cliente_critico + "<br>" + 
                                    this.horario + "<br>" + 
                                    this.observacion + "<br>" + 
                                    this.direccion_instalacion + "<br>" + 
                                    this.averia + "<br>" + 
                                    this.fftt + "<br>" + 
                                    this.inscripcion + "<br>" + 
                                    this.mdf + "<br>" + 
                                    this.id_atc + "<br>" + 
                                    this.lejano + "<br>" +  
                                    this.carnet_critico + "<br>" + 
                                    this.paquete + "<br>" + 
                                    this.quiebre + "<br>" + 
                                    this.tecnico;
                    
                    doInfoWindow(marker, infocontent);
                });
                fitmap();
            }
        });
        */
    } catch (e) {
        console.log(e);
    }
}

/**
 * Muestra la ruta de actuaciones asignadas a un tecnico,
 * en base a su carnet (code). La ruta se muestra como
 * una linea discontinua.
 * 
 * @param {type} code
 * @returns {Boolean}
 */
function actuTecPath(code){
    
    tecPath = [];
    var color;
    
    var lineSymbol = {
        path: 'M 0,-1 0,1',
        strokeOpacity: 1,
        scale: 4
    };
    
    $.each(tecActu[code], function(id, val){
        
        if (typeof val === 'object')
        {
            myLatlng = new google.maps.LatLng(
                    this.getPosition().lat(), 
                    this.getPosition().lng()
                            );
            color = this.icon.substring(17, 23);

            tecPath.push(this.position);
        }
        
    });
    
    drawPath = new google.maps.Polyline({
        path: tecPath,
        strokeOpacity: 0,
        strokeColor: '#' + color,
        icons: [{
          icon: lineSymbol,
          offset: '0',
          repeat: '18px'
        }],
        map: map
    });

    return true;
}

function showHideAll(){
    var show = $("#show_tec").prop("checked");

    //Tecnicos en mapa
    $.each(tecMark, function(){
        if (!show)
        {
            //Ocultar tecnicos
            this.setMap(null);
        } else {
            //Mostrar tecnicos
            this.setMap(map);
        }
    });
    
    //Agendas en mapa
    for (key in tecActu) {
        if (tecActu.hasOwnProperty(key))
        {
            $.each(tecActu[key], function(id, val){

                if (typeof val === 'object')
                {
                        if (!show)
                    {
                        //Ocultar agendas
                        this.setMap(null);
                    } else {
                        //Mostrar agendas
                        this.setMap(map);
                    }
                }
                
                
            });
        }
    }
    
    //Checkbox por tecnico
    $.each($(".chb_tec"), function(){
        if (!show)
        {
            //Ocultar agendas
            $(this).prop("checked", false);
        } else {
            //Mostrar agendas
            $(this).prop("checked", true);
        }
    });
    
}

function showHideTec(code, show){
    
    //Tecnicos en mapa
    if ( typeof tecMark[code] !== 'undefined' )
    {
        if (!show)
        {
            //Ocultar tecnico
            tecMark[code].setMap(null);
        } else {
            //Mostrar tecnicos
            tecMark[code].setMap(map);
        }
    }    
    
    //Agendas en mapa
    if ( typeof tecActu[code] !== 'undefined' )
    {
        $.each(tecActu[code], function(id, val){
            
            if (typeof val === 'object')
            {
                if (!show)
                {
                    //Ocultar agendas
                    this.setMap(null);
                } else {
                    //Mostrar agendas
                    this.setMap(map);
                }
            }
            
        });
    }            
    
}

function showBounce(){
    var show = $("#show_pdt").prop("checked");        
    
    //Agendas en mapa
    for (key in tecActu) {
        if (tecActu.hasOwnProperty(key))
        {
            $.each(tecActu[key], function(){
                
                //Solo objetos con la propiedad estado = marcador
                if (typeof this.estado !== 'undefined')
                {
                    if (show)
                    {
                        if (this.estado==="pendiente") 
                        {
                            this.setAnimation(google.maps.Animation.BOUNCE);
                        }
                    } else {
                        this.setAnimation(null);
                    }
                }                
                
            });
        }
    }
        
}


function showTmpActu(){
    //Validar seleccion de EECC
    if ( $("#empresa").val() === '' )
    {
        alert("Seleccione empresa");
        $("#empresa").focus();
        return false;
    }
    
    var datos = $("#ftmp").serialize() 
            + "&eecc=" 
            + $("#empresa option:selected").text()
            + "&tipo_quiebre=" + getMultiSelect("#", "tipo_quiebre")
            + "&tipo_producto=" + getMultiSelect("#", "tipo_producto")
            + "&tipo_servicio=" + getMultiSelect("#", "tipo_servicio")
            + "&tipo_zonal=" + $("#zonal").val()
            + "&tipo_codigo=" + $("#tipo_codigo").val()
    
    var tipo = $("#tipo_actu").val();
    var conXy = 0;
    var sinXy = 0;
    
    var conXyLink = "";
    var sinXyLink = "";
    
    //Loading
    $("body").addClass("loading");
    
    $.ajax({
        type: "POST",
        url: "ruta.ajax.php",
        data: "getTmp" + tipo + "=ok&" + datos,
        dataType: 'json',
        error: function(data) {
            console.log(data);
        },
        success: function(data) {
            
            //Sin datos
            if (data.agenda.length === 0)
            {
                alert("No se encontraron resultados");
                
                //Remove Loading
                $("body").removeClass("loading");
                
                return false;
            }
            
            if (typeof data.agenda !== 'undefined')
            {                
                $.each(data.agenda, function (){
                    //Mostrar solo tmp con XY
                    if (this.x !== '' && this.y !== '' && this.x !== null && this.y !== null)
                    {
                        //Sumar con XY
                        conXy++;
                        
                        //Lat/Lng del temporal
                        myLatlng = new google.maps.LatLng(this.y, this.x);

                        bounds.extend(myLatlng);

                        //Marcador
                        marker = new google.maps.Marker({
                            position: myLatlng,
                            map: map,
                            title: this.EmployeeNum,
                            icon: "images/tmp_actu.png",
                            animation: google.maps.Animation.DROP,
                            zIndex: zIndex++,
                            codactu: this.codactu,
                            tipoactu: this.tipoactu,
                            isTmp: true,
                            idgestion: 0,
                            coordinado: 0,
                            carnet: this.carnet_critico
                        });
                                                
                        //Agrega marcadores temporales por averia o Provision
                        tmpActu.push(marker);
                        
                        //Averia(s) o Provision
                        if (tipo === "Averia"){
                            tipo = "Averias";
                        }
                        
                        //Infowindow
                        infocontent =   "<div class=\"infow\" style=\"width:300px; height:200px; overflow: auto\">" +
                                            "<input type=\"button\" id=\"detalle_actu\" value=\"Mostrar/Ocultar detalle\" onclick=\"mostrarOcultarDetalle()\">" + 
                                            "<div class=\"detalle_actu\">" + 
                                                "Tipo: " + this.tipoactu + "<br>" + 
                                                "Codigo: " + this.codactu + "<br>" + 
                                                "Horas: " + this.horas_actu + "<br>" + 
                                                "Fec. Registro: " + this.fecha_registro + "<br>" + 
                                                this.nombre_cliente + "<br>" + 
                                                this.direccion_instalacion + "<br>" + 
                                                this.fftt + "<br>" + 
                                                this.distrito + "<br>" + 
                                                this.mdf + "<br>" + 
                                                this.telefono + "<br>" +  
                                                this.paquete + "<br>" +
                                                this.x + " / " + this.y + "<br>" + 
                                            "</div>" +
                                            "<div><a href=\"javascript:void(0)\" onclick=\"gestionActuTmp('" + this.codactu  + "', '0', '" + tipo + "')\">Gestionar <img src=\"../historico/img/gestionar.png\" style=\"vertical-align: middle\"></a>&nbsp;" + 
                                            "<div class=\"detalle_gestion\"></div>" + 
                                        "</div>";

                        doInfoWindow(marker, infocontent);
                    } else {
                        //Actu sin XY
                        sinXyLink += "," + this.codactu;
                        //Sumar con XY
                        sinXy++;
                    }
                });
                                
                sinXyLink = "<a onclick=\"downloadTmp('" + tipo + "', '" 
                                + sinXyLink.substring(1)
                                + "')\" href=\"javascript:void(0)\">" 
                                + sinXy + "</a>"
                
                //Actualizar numero de tmp con y sin XY
                $("#n_tmp").html(
                        conXy + 
                        "/" + 
                        sinXyLink + 
                        " (" + 
                        (conXy + sinXy)  + 
                        ")"
                    );
                
                map.fitBounds(bounds);
                
                //Remove Loading
                $("body").removeClass("loading");
            }
            
        }
    });
    
}

function downloadTmp(tipo, codes){
    window.top.location = 'download.tmp.php?tipo=' + tipo + '&codes=' + codes;
}

function limpiarTmpActu(){
    //Elimina marcadores tmp del mapa
    if (tmpActu.length > 0)
    {
        $.each(tmpActu, function (){
            this.setMap(null);
        });
        tmpActu = [];
    }
    
    //Limpiar cantidad de tmp encontrados
    $("#n_tmp").html("00/00 (00)");
}

function agenda_tmp(tipo, cod, zonal, eecc)
{
    $.ajax({
        type: "POST",
        url: "../historico/controladorHistorico/agendaController.php",
        data: "action=getAgendaData&zonal=" + zonal + "&eecc=" + eecc,
        error: function(data) {
            console.log(data);
        },
        success: function(data) {
            $(".detalle_agenda").html(data);
            //$(".detalle_agenda").append("<input type=\"button\" onclick=\"grabar_agenda()\">");
        }
    });
}

function grabar_agenda(){
    console.log( $("#fecha_agenda").val() );
    console.log( $("#horario_agenda").val() );
}

function gestionActu(id, indice, actividad){
        
    var url = "../historico/gestion_clientes_criticos.php?id="+id+"&indice="+indice+"&actividad="+actividad;
    var pagina = '<iframe style="border: 0px; " src="' + url + '" width="100%" height="400px"></iframe>'
    
    var content = infowindow.getContent();
    content = content.replace("width:300px", "width:800px");
    content = content.replace("height:200px", "height:600px");
    
    infowindow.close();
    infowindow.setContent(content);
    infowindow.open(map, globalElement);
    
    $(".detalle_gestion").append(pagina);
}

function gestionActuTmp(id, indice, actividad){
    
    if (isTmpActu===false)
    {
        gestionActu(idGestionActu, indice, actividad);
    } else {
    
        var page = "registro_clientes_criticos.php";
        if (actividad == "Provision") {
            page = "registro_clientes_criticos_provision.php";
        }    

        var url = "../historico/" + page + "?averia_ini="+id+"&indice="+indice+"&actividad="+actividad;
        var pagina = '<iframe style="border: 0px; " src="' + url + '" width="100%" height="400px"></iframe>'

        var content = infowindow.getContent();
        content = content.replace("width:300px", "width:800px");
        content = content.replace("height:200px", "height:600px");

        infowindow.close();
        infowindow.setContent(content);
        infowindow.open(map, globalElement);

        $(".detalle_gestion").append(pagina);
    
    }
}


function doGeoPlan(){
    var ntec = prompt("Numero de tecnicos", "");
    var naxt = prompt("Actuaciones por tecnico", "");
    var wpt = [];
    var objWpt = {}
    
    waypts = [];
    directionsService = new google.maps.DirectionsService();
    
    $.ajax({
        type: "POST",
        url: "ruta.ajax.php",
        data: "getAllWaypoints=ok",
        dataType: 'json',
        error: function(data) {
            console.log(data);
        },
        success: function(data) {
            var all = data.data;
            var CapD = (ntec * naxt);
            var dias = Math.ceil( all.length/(ntec * naxt) );
            var color = getRandomColor();
            
            console.log("Actus: " + all.length);
            console.log("Tec: " + ntec);
            console.log("AxT: " + naxt);
            console.log("Cap.D: " + CapD);
            console.log("Dias: " + dias);
            
            var n = 1;
            $.each(all, function(){
                
                marker = new google.maps.Marker({
                    position: myLatlng = new google.maps.LatLng(this.coord_y, this.coord_x),
                    icon: "http://chart.apis.google.com/chart" 
                            + "?chst=d_map_pin_letter&chld="
                            + this.orden
                            + "|" 
                            + color.substring(1) 
                            + "|"
                            + textByColor(color).substring(1),
                    map: map,
                    zIndex: zIndex++
                });n++;
                
                if (n > CapD) {
                    n = 1;
                    color = getRandomColor();
                }
                /*
                if (n<=naxt) {
                    objWpt.x = this.coord_x;
                    objWpt.y = this.coord_y;
                    objWpt.tipoactu = this.tipoactu;
                    objWpt.codactu = this.codactu;
                    
                    wpt.push(objWpt);
                    
                    waypts.push({
                        location:new google.maps.LatLng(this.coord_y, this.coord_x),
                        stopover:true
                    });
                    
                    objWpt = {};
                    
                    n++;
                } else {
                    getOptimizedRoute(waypts, wpt);//return false;
                    n = 1;
                    waypts = [];
                    wpt = [];                    
                }
                */
            });
            
        }
    });
}

function getOptimizedRoute(waypts, wpt){console.log(waypts);console.log(wpt);
    var request = {
        origin: new google.maps.LatLng(-12.033284, -77.0715493),
        //destination: new google.maps.LatLng(-12.033284, -77.0715493),
        //origin: waypts[0].location,
        destination: waypts[0].location,
        waypoints: waypts,
        optimizeWaypoints: true,
        travelMode: google.maps.TravelMode.DRIVING
    };
    directionsService.route(request, function(response, status) {
        if (status == google.maps.DirectionsStatus.OK)
        {
            var optimized = response.routes[0].waypoint_order;console.log(optimized);
            console.log(response.routes[0]);
            //Color y numeracion
            var color = getRandomColor();
            var nmark = 1;
            
            $.each(optimized, function (id, val){
                console.log(wpt[val]);
                marker = new google.maps.Marker({
                    position: myLatlng = new google.maps.LatLng(wpt[val].y, wpt[val].x),
                    icon: "http://chart.apis.google.com/chart" 
                            + "?chst=d_map_pin_letter&chld="
                            + nmark
                            + "|"
                            + color.substring(1)
                            + "|"
                            + textByColor(color).substring(1),
                    map: map,
                    title: wpt[val].codactu,
                    zIndex: zIndex++
                });
                
                nmark++;
            });
        }
    });
}

var liq_marker = "";
var infowindow = "";
function showLastLiq(carnet){

    if(liq_marker != ""){
        liq_marker.setMap(null);
    }
    $.ajax({
        type: "POST",
        url: "ruta.ajax.php",
        data: "showLastLiq=" + carnet,
        dataType: 'json',
        error: function(data) {
            console.log(data);
        },
        success: function(data) {
            //Agregar options
           //OBTENGO TAREAS CON XY

            if(data.length != 0 ){
                var info = infoContentLiq(data);
                infowindow = new google.maps.InfoWindow({    content: info     });
                var p = new google.maps.LatLng(data.y , data.x);
                liq_marker = new google.maps.Marker({
                    position: p,
                    map: map,
                     icon: "images/" + "administration.png",
                    zIndex: 99999
                });
                infowindow.open(map,liq_marker);
                google.maps.event.addListener(liq_marker, 'click', function() {
                    infowindow.open(map,liq_marker);
                });
            }else{
                alert("No se encontro ultima tarea Liquidada");
            }



        }
    });
}

function  infoContentLiq(data){

    var infocontent =   "<div class=\"infow\" style=\"width:300px; height:200px; overflow: auto\">" +
        "<input type=\"button\" id=\"detalle_actu\" value=\"Mostrar/Ocultar detalle\" onclick=\"mostrarOcultarDetalle()\">" +
        "<div class=\"detalle_actu\">" +
        data.tipoactu + "<br>" +
        data.nombre_cliente_critico + "<br>" +
        data.fecha_agenda + " / " +
        data.horario + "<br>" +
        data.observacion + "<br>" +
        data.direccion + "<br>" +
        data.codactu + "<br>" +
        data.fftt + "<br>" +
        data.codcli + "<br>" +
        data.mdf + "<br>" +
        data.id_atc + "<br>" +
        data.lejano + "<br>" +
        data.carnet_critico + "<br>" +
        data.paquete + "<br>" +
        data.quiebre + "<br>" +
        data.tecnico + "<br>" +
        "</div>";

    infocontent+="<div><a href=\"javascript:void(0)\" onclick=\"get_detalle_paso('0001-|" + data.id_atc + "')\">1. Inicio</a>&nbsp;" +
    "<a href=\"javascript:void(0)\" onclick=\"get_detalle_paso('0002-|" + data.id_atc + "')\">2. Supervision</a>&nbsp;" +
    "<a href=\"javascript:void(0)\" onclick=\"get_detalle_paso('0003-|" + data.id_atc + "')\">3. Cierre</a></div>" +
    "<div class=\"detalle_paso\"></div>" +
    "</div>";

    return infocontent;
}

function showCooBounce(){
    var show = $("#show_coo").prop("checked");
    var gf = $("#grupos-filtros").val();
    var setBounce;
    
    //Agendas en mapa
    for (key in tecActu) {
        if (tecActu.hasOwnProperty(key))
        {
            $.each(tecActu[key], function(){
                var arrSel = $("#grupo-" + key).val();
                setBounce = false
                
                if (gf === "")
                {
                    setBounce = true;
                }
                
                if ( arrSel !== null && arrSel.indexOf(gf) >= 0 ) 
                {
                    setBounce = true;
                }
                //Solo objetos con la propiedad cooordinado = marcador
                if (typeof this.coordinado !== 'undefined')
                {
                    if (show)
                    {
                        if (this.coordinado==1 && setBounce) 
                        {
                            //this.setMap(map);
                            this.setAnimation(google.maps.Animation.BOUNCE);
                        }
                    } else {
                        this.setAnimation(null);
                    }
                }                
                
            });
        }
    }
        
}

$(document).ready(function() {
    //Mostrar mapa
    google.maps.event.addDomListener(window, 'load', initialize);
    
    //bounds
    bounds = new google.maps.LatLngBounds();
    
    //Ocultar checkbox ocultar/mostrar tecnicos y agendas
    $(".show_hide_tec").hide();
    
    //Seleccionar empresa
    $("#empresa").change(function() {
        //Remover options
        $('#celula').find('option').remove();

        var id = $(this).val();

        if (id !== '')
        {
            $.ajax({
                type: "POST",
                url: "ruta.ajax.php",
                data: "getCelulaEmp=" + id,
                dataType: 'json',
                error: function(data) {
                    console.log(data);
                },
                success: function(data) {
                    //Agregar options
                    $("#celula").append(
                            "<option value=\"\">-Seleccione-</option>"
                        );
                    $.each(data, function() {
                        $("#celula").append(
                            "<option value=\"" 
                            + this.idcelula 
                            + "\">" 
                            + this.nombre 
                            + "</option>"
                        );
                    });
                }
            });
        }

    });

    //Seleccionar celula
    $("#buscar_agenda").click(function() {
        //ID de celula, ID empresa
        var id_cel = $("#celula").val();
        var id_emp = $("#empresa").val();
        var agenda = $("#agenda").val();
        var to_agenda = $("#to_agenda").val();
        
        if (id_emp === '')
        {
            alert("Seleccione empresa.");
            $("#empresa").focus();
            return false;
        }
        
        if (id_cel === '')
        {
            alert("Seleccione Celula.");
            $("#celula").focus();
            return false;
        }
        
        //Limpiar lista de tecnicos
        $("#tec-list").html("");
        
        //No resaltar pendientes (bounce)
        $("#show_pdt").prop("checked", false);
        
        //Limpiar mapa
        clearMap();

        if (id_cel !== '' && id_emp !== '')
        {
            //Loading
            $("body").addClass("loading");
            
            $.ajax({
                type: "POST",
                url: "ruta.ajax.php",
                data: "getCelulaTec=" + id_cel 
                        + "|" + id_emp 
                        + "|" + agenda
                        + "|" + to_agenda
                        + "&tipo=" + $("#tipo_actu").val()
                        + "&estado=" + getMultiSelect("#", "tipo_estado"),
                dataType: 'json',
                error: function(data) {
                    console.log(data);
                },
                success: function(data) {
                    //Sin datos
                    if (data.alert === "ko")
                    {
                        alert("No se encontraron resultados");
                        
                        //Remove loading
                        $("body").removeClass("loading");
                        
                        return false;
                    }
                    //Lista tecnicos
                    if (typeof data.names !== 'undefined' && data.names.length > 0)
                    {
                        doTecList(data.names, data.coord, data.icons);
                    }
                    
                    //Agendas de la celula
                    if (typeof data.agenda !== 'undefined' && data.agenda.length > 0)
                    {
                        showAgendaCel(data.agenda, data.icons);
                    }

                    //Mapa tecnicos
                    if (typeof data.coord !== 'undefined' && data.coord.length > 0)
                    {
                        loadMarker(map, data.coord, data.icons, data.names);
                        map.fitBounds(bounds);
                    }

                    //MUESTRO EL GRUPO SELECCIONADO
                        var grupo_selected = $(".select-grupos").val();
                        if(grupo_selected == "" || grupo_selected == undefined ){//muestro todos otra vez
                            $("#show_tec").prop("checked",true);
                            showHideAll();
                            $(".tecRow").show()
                            $(".showAllTec").show()
                        }else{
                            $("#show_tec").prop("checked",false);
                            showHideAll(); //OCULTO TODOS
                            $(".showAllTec").hide()
                            $(".tecRow").hide();
                            $(".g-"+grupo_selected+" .chb_tec").trigger("click")
                            $(".tecRow"+".g-"+grupo_selected).show();
                        }


                    
                    //Remove loading
                    $("body").removeClass("loading");
                }
            });
        }
    });

    //Inicializa datepicker
    $("#agenda").datepicker(
            {
                closeText: 'Cerrar',
                prevText: '<Ant',
                nextText: 'Sig>',
                currentText: 'Hoy',
                monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
                monthNamesShort: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'],
                dayNames: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
                dayNamesShort: ['Dom', 'Lun', 'Mar', 'Mié', 'Juv', 'Vie', 'Sáb'],
                dayNamesMin: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sá'],
                weekHeader: 'Sm',
                dateFormat: 'dd/mm/yy',
                firstDay: 1,
                isRTL: false,
                showMonthAfterYear: false,
                yearSuffix: ''
            }
    );
    
    $("#to_agenda").datepicker(
            {
                closeText: 'Cerrar',
                prevText: '<Ant',
                nextText: 'Sig>',
                currentText: 'Hoy',
                monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
                monthNamesShort: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'],
                dayNames: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
                dayNamesShort: ['Dom', 'Lun', 'Mar', 'Mié', 'Juv', 'Vie', 'Sáb'],
                dayNamesMin: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sá'],
                weekHeader: 'Sm',
                dateFormat: 'dd/mm/yy',
                firstDay: 1,
                isRTL: false,
                showMonthAfterYear: false,
                yearSuffix: ''
            }
    );
    
    //Mostrar, ocultar tecnicos
    $("#show_tec").click(function (){
        showHideAll();
    });
    
    //Mostrar actuaciones pendientes BOUNCE
    $("#show_pdt").click(function (){
        showBounce();
    });
    
    //Mostrar actuaciones coordinadas BOUNCE
    $("#show_coo").click(function (){
        showCooBounce();
    });
    
    //Mostrar trafico
    $("#show_traffic").click(function (){
        mostrarTrafico();
    });
    
    //Buscar temporales
    $("#ftmp").submit(function (event){
        event.preventDefault();
        showTmpActu();
    });
    
    //Limpiar mapa de temporales
    $("#tipo_limpiar").click(function (){
        limpiarTmpActu();
    });
    
    $("#tipo_limpiar_todo").click(function (){
        limpiarTmpActu();
        clearMap();
    });
    
    //Elementos multiselect
    $("#tipo_estado").multiselect();
    $("#tipo_quiebre").multiselect();
    $("#tipo_producto").multiselect({
        click: function(event, ui){
            if (ui.value === '_negative' && ui.checked)
            {
                $(this).multiselect("uncheckAll");
                $("#tipo_producto option[value='_negative']").attr("selected", true);
                $("#tipo_producto").multiselect("refresh");
            }           
         }
    });
    $("#tipo_servicio").multiselect();
    
    //Planificacion por zona y capacidad
    $("#geo_plan").click(function (){
        doGeoPlan();
    });
    
    $( "#tabs" ).tabs({});
    
});