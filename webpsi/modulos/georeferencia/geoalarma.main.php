<?php
set_time_limit(0);
session_start();
include_once 'autoload.php';
$_SESSION['exp_user']['expires'] = time() + (45 * 60);
$cnxObj = new ConexionMapas();
$conexion = $cnxObj->conectarBD();

/**
 *
 * Acciones
 *
 */

//Lista de trobas alarmadas
if ($_POST["action"] == "geoAlarmaTrobas") {
    $GeoAlarmaTap = new GeoAlarmaTap();
    
    $data["zonal"] = $_POST["zonal"];
    
    $troba = $GeoAlarmaTap->trobasAlarma(
            $conexion,
            $data
    );
    echo json_encode($troba);
}

//Areas de trobas alarmadas
if ($_POST["action"] == "geoTrobaArea") {
    $GeoTroba = new GeoTroba();
    $puntos = $GeoTroba->listar(
            $conexion, array(
                $_POST["zonal"],
                $_POST["nodo"],
                $_POST["troba"]
            )
    );
    echo json_encode($puntos);
}

//Taps alarmados
if ($_POST["action"] == "geoTapAlarma") {
    $GeoAlarmaTap = new GeoAlarmaTap();
    $tap = $GeoAlarmaTap->tapAlarma(
            $conexion
    );
    echo json_encode($tap);
}

//Taps alarmados
if ($_POST["action"] == "geoTapClientes") {
    $GeoAlarmaTap = new GeoAlarmaTap();
    $tap = $GeoAlarmaTap->tapAlarmaClientes(
            $conexion,
            $_POST["zonal"],
            $_POST["nodo"],
            $_POST["troba"]
    );
    
    $amp = $GeoAlarmaTap->ampAlarmaClientes(
            $conexion,
            $_POST["zonal"],
            $_POST["nodo"],
            $_POST["troba"]
    );
    
    $res = array("tap"=>$tap, "amp"=>$amp);
    
    echo json_encode($res);
}