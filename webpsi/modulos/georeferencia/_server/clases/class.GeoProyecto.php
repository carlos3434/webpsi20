<?php

class GeoProyecto {

    protected $_db = 'webpsi';
    protected $_table = 'geo_proyectos';

    public function saveProject($dbh, $proyecto, $capas) {
        $result = array();
        try {
            //Iniciar transaccion
            $dbh->beginTransaction();

            //"Editar" proyecto
            if ($proyecto["keyId"] != "" and $proyecto["keyId"] > 0) {
                //Eliminar proyecto anterior
                $sql = "DELETE FROM "
                        . $this->_db
                        . "."
                        . $this->_table
                        . " WHERE id = "
                        . $proyecto["keyId"];
                $dbh->exec($sql);
            }

            //Registrar proyecto
            $nombre = $proyecto["nombre"];

            if ($nombre != "") {
                $sql = "INSERT INTO " . $this->_db . "." . $this->_table . " (nombre, estado) 
						VALUES ('$nombre', 1)";
                $dbh->exec($sql);
            }

            //Ultimo ID registrado
            $id = $dbh->lastInsertId();

            //Registrar capas del proyecto
            foreach ($capas as $key => $val) {
                $layer = $val["layer"];
                $tipo = $val["tipo"];
                $origen = $val["origen"];
                $datos = $val["datos"];
                $datos = str_replace("chld=", "", $datos);
                if ($tipo != "" and $origen != "" and $datos != "") {
                    $sql = "INSERT INTO " . $this->_db . ".geo_proyecto_capas (geo_proyecto_id, layer, tipo, origen, datos)
								VALUES ($id, '$layer', '$tipo', '$origen', '$datos')";
                    $dbh->exec($sql);
                }
            }

            $dbh->commit();
            $result["estado"] = true;
            $result["msg"] = "Proyecto registrado correctamente";
            return $result;
        } catch (PDOException $error) {
            $dbh->rollback();
            $result["estado"] = false;
            $result["msg"] = $error->getMessage();
            return $result;
            exit();
        }
    }

    public function doListLayer($conexion) {
        try {

            $sql = "SELECT id, nombre  
                    FROM "
                    . $this->_db . "."
                    . $this->_table
                    . " WHERE estado=1 ";

            $bind = $conexion->prepare($sql);
            $bind->execute();

            $reporte = array();
            while ($data = $bind->fetch(PDO::FETCH_ASSOC)) {
                $reporte[] = $data;
            }
            return $reporte;
        } catch (PDOException $error) {
            exit();
        }
    }

    public function getProjectLayer($conexion, $id) {
        try {

            $sql = "SELECT layer, tipo, origen, datos   
                    FROM "
                    . $this->_db . ".geo_proyecto_capas"
                    . " WHERE geo_proyecto_id=$id";

            $bind = $conexion->prepare($sql);
            $bind->execute();

            $reporte = array();
            while ($data = $bind->fetch(PDO::FETCH_ASSOC)) {
                $reporte[] = $data;
            }
            return $reporte;
        } catch (PDOException $error) {
            exit();
        }
    }

    public function deleteProject($dbh, $id) {
        try {
            //Iniciar transaccion
            $dbh->beginTransaction();

            $sql = "DELETE FROM "
                    . $this->_db . "." . $this->_table
                    . " WHERE id=$id";
            $dbh->exec($sql);

            $dbh->commit();
            $result["estado"] = true;
            $result["msg"] = "Proyecto eliminado correctamente";
            return $result;
        } catch (PDOException $error) {
            $dbh->rollback();
            $result["estado"] = false;
            $result["msg"] = $error->getMessage();
            return $result;
            exit();
        }
    }

}
