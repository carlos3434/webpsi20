<?php

class GeoNodopoligono
{

    protected $_db = 'webpsi_new';
    protected $_table = 'geo_nodopoligono';

    public function listar($conexion, $arreglo = array())
    {
        try {
            $zonal = $arreglo[0];
            $nodo = $arreglo[1];

            $sql = "SELECT zonal, nodo, coord_x, coord_y, direccion, orden  
                    FROM " . $this->_db . "." . $this->_table . " WHERE nodo<>'' ";

            $sql .= " AND zonal='$zonal' 
                      AND nodo='$nodo'";
            
            $sql .= " ORDER BY nodo, orden";
            $bind = $conexion->prepare($sql);
            $bind->execute();

            $reporte = array();
            while ($data = $bind->fetch(PDO::FETCH_ASSOC)) {
                $reporte[] = $data;
            }
            return $reporte;
        } catch (PDOException $error) {
            exit();
        }
    }

    public function primerFiltro($conexion){
        try {
            $sql = "SELECT zonal nombre, zonal id 
                    FROM " . $this->_db . "." . $this->_table 
                    . " GROUP BY 1 ORDER BY 1";
            $bind = $conexion->prepare($sql);
            $bind->execute();

            $reporte = array();
            while ($data = $bind->fetch(PDO::FETCH_ASSOC)) {
                $reporte[] = $data;
            }
            return $reporte;
        } catch (PDOException $e) {
            exit();
        }
    }

    public function listarNodopoligono($conexion, $arreglo=array()){
        try {
            $zonal = $arreglo[0];

            $sql = "SELECT nodo campo, nodo id 
                    FROM " . $this->_db . "." . $this->_table 
                    . " WHERE zonal='$zonal'"
                    . " GROUP BY 1 ORDER BY 1";
            $bind = $conexion->prepare($sql);
            $bind->execute();

            $reporte = array();
            while ($data = $bind->fetch(PDO::FETCH_ASSOC)) {
                $reporte[] = $data;
            }
            return $reporte;
        } catch (PDOException $e) {
            exit();
        }   
    }

}