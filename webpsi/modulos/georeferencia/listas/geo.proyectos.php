<?php
session_start();
include_once '../autoload.php';
require_once '../clases/class.GeoProyecto.php';

$cnxObj = new ConexionMapas();
$conexion = $cnxObj->conectarBD();

$filtro['userid'] = $_SESSION['exp_user']['id'];

$GeoProyecto = new GeoProyecto();
$pro = $GeoProyecto->doListLayer($conexion, $filtro);

$page = isset($_POST['page']) ? $_POST['page'] : 1;
$rp = isset($_POST['rp']) ? $_POST['rp'] : 10;
$sortname = isset($_POST['sortname']) ? $_POST['sortname'] : 'name';
$sortorder = isset($_POST['sortorder']) ? $_POST['sortorder'] : 'desc';
$query = isset($_POST['query']) ? $_POST['query'] : false;
$qtype = isset($_POST['qtype']) ? $_POST['qtype'] : false;

if($qtype and $query){
    $query = strtolower(trim($query));
    foreach($pro AS $key => $row){
        if(strpos(strtolower($row[$qtype]),$query) === false){
            //echo strtolower($row[$qtype]) . " / " . $query . "<br>";
            unset($pro[$key]);
        }
    }
}
//print_r($pro);
$jsonData['page'] = $page;
$jsonData['total'] = count( $pro );

$pageIni = ($page * $rp) - $rp + 1;
$pageEnd = $page * $rp;

$n = 1;
foreach ( $pro as $key=>$val ) {
    if ( $n >= $pageIni and $n <= $pageEnd ) {
        foreach ($val as $index=>$data) {
            $val[$index] = utf8_encode($data);
            
            $editIcon = "";
            if ($val["editar"] === '0') {
                $editIcon = "&nbsp;<img src=\"images/alert_16x16.png\" "
                            . "style=\"vertical-align: middle\" "
                            . "title=\"No editable\" />";
            }
            
            if ($index === 'fec_creacion') {
                $val[$index] = date("d/m/Y", strtotime($data)) . $editIcon;
            }
        }
        $val["inc"] = $n;
        $val["btn"] = "<img src=\"images/map_show.png\" class=\"showLayer\">";
        $jsonData['rows'][] = array("id"=>$val['id'], "cell"=>$val) ;
    }
    $n++;
}

echo json_encode($jsonData);
