<?php
set_time_limit(0);
session_start();
include_once 'autoload.php';

$cnxObj = new ConexionMapas();
$conexion = $cnxObj->conectarBD();

/**
 *
 * Acciones
 *
 */
if ($_POST["action"] == "geoElementos") {
    $GeoElemento = new GeoElemento();
    $elementoArray = $GeoElemento->listar(
            $conexion, true
    );
    echo json_encode($elementoArray);
}

if ($_POST["action"] == "getGeoDedependencia") {
    $GeoDependencia = new GeoDependencia();
    $dependenciaArray = $GeoDependencia->listar(
            $conexion, $_POST["elemento_id"]
    );

    $geoSelect = "";
    $depString = "";
    $n = 1;
    foreach ($dependenciaArray as $key => $val) {
        $multiple = "";
        $geoOption = "";
        if ($n == 1) {
            $object = "Geo" . ucfirst($_POST["origen"]);
            $Item = new $object();
            $itemArray = $Item->primerFiltro(
                    $conexion
            );

            //$geoOption .= "<option value=\"\">-Seleccione-</option>";
            foreach ($itemArray as $a => $b) {
                $geoOption .= "<option value=\"{$b['nombre']}\">"
                            . "{$b['nombre']}"
                            . "</option>";
            }
        }
        if ($n > 0) {
            $multiple = 'multiple="multiple" size="5"';
        }
        $depString .= "_" . $val['campo'];
        $geoSelect .= "<select title=\"title_prop\" class=\"geo_item\" "
                    . "name=\"geo_{$val['campo']}\" id=\"geo_{$val['campo']}\" "
                    . " style=\"width: 100%\" $multiple>$geoOption</select>";
        $n++;
    }
    $depString = substr($depString, 1);
    echo str_replace('title_prop', $depString, $geoSelect);
}

if ($_POST["action"] == "getGeoItemIni") {
    $object = "Geo" . ucfirst($_POST["origen"]);
    $Item = new $object();
    $itemArray = $Item->primerFiltro(
            $conexion
    );
    echo json_encode($itemArray);
}
/*
if ($_POST["action"] == "getGeoItem") {
    $object = "Geo" . ucfirst($_POST["origen"]);
    $Item = new $object();
    $method = "listar" . ucfirst($_POST["element"]);
    $itemArray = $Item->$method(
            $conexion, explode("___", $_POST["dependencia"])
    );
    
    echo json_encode($itemArray);
}
*/

if ($_POST["action"] == "getGeoItem") {
    $object = "Geo" . ucfirst($_POST["origen"]);
    $Item = new $object();
    $method = "listar" . ucfirst($_POST["element"]);
    
    //Datos a mostrar
    $data = array();
    
    //Primer nivel de arreglo de datos
    $sendArray = explode(",", $_POST["dependencia"]);
    
    foreach($sendArray as $key=>$val){
        $result = array();
        if ( strpos($val, "___") !== false ) {
            foreach ( explode("___", $val) as $ind=>$var ) {
                $itemArray[$val] = $Item->$method($conexion, explode("___", $val));
            }
        } else {
            $itemArray[$val] = $Item->$method($conexion, array($val));
        }
    }
    
    echo json_encode($itemArray);
}

if ($_POST["action"] == "drawPolygon") {

    $object = "Geo" . $_POST["origen"];
    $Item = new $object();

    $data = explode("___", $_POST["data"]);

    $geoArray = $Item->listar($conexion, $data);
    echo json_encode($geoArray);
}

if ($_POST["action"] == "drawMarker") {

    $object = "Geo" . $_POST["origen"];
    $Item = new $object();

    $data = explode("___", $_POST["data"]);

    $geoArray = $Item->listar($conexion, $data);
    echo json_encode($geoArray);
}

if ($_POST["action"] == "drawCircle") {

    $object = "Geo" . $_POST["origen"];
    $Item = new $object();

    $data = explode("___", $_POST["data"]);
    $data[] = $_POST["item"];

    $geoArray = $Item->listar($conexion, $data);
    echo json_encode($geoArray);
}

if ($_POST["action"] == "saveObject") {

    $proyecto = array();
    $capas = array();

    //Datos del proyecto
    $proyecto["nombre"] = $_POST["proyecto"];
    //Project Key
    $proyecto["keyId"] = $_POST["keyProject"];
    //Reemplazar ruta de icono final
    $_POST["capas"] = str_replace("/tmp/", "/upload.icon/", $_POST["capas"]);

    $capas = explode("[{layer}]", $_POST["capas"]);
    foreach ($capas as $key => $capa) {
        /**
         * 0|~1c128e9f53783dc0bd8456e77d7be9c1___poli___distrito|^LIMA___LIMA___ANCON[{chars}]c7328e___0.53___008000___2
         * 
         * $src[0]: 0|~1c128e9f53783dc0bd8456e77d7be9c1___poli___distrito
         * $src[1]: LIMA___LIMA___ANCON[{chars}]c7328e___0.53___008000___2
         * 
         * $data[0]: 0|~1c128e9f53783dc0bd8456e77d7be9c1
         * $data[1]: poli
         * $data[2]: distrito
         * 
         */
        $src = explode("|^", $capa);
        $data = explode("___", $src[0]);

        $idcapa = $data[0];
        $tipo = $data[1];
        $origen = $data[2];
        $filtro = $src[1];

        $layer[] = array(
            "layer" => $idcapa,
            "tipo" => $tipo,
            "origen" => $origen,
            "datos" => $filtro);
    }

    //Guardar cambios
    $GeoProyecto = new GeoProyecto();
    $save = $GeoProyecto->saveProject(
            $conexion, 
            $proyecto, 
            $layer, 
            $_SESSION['exp_user']['id']);

    //Verificar y guardar icono temporal
    if ($save["estado"]) {

        //Almacenar capas ya asociadas al icono
        $layerIcon = array();

        $layerGroup = explode("[{layer}]", $_POST["capas"]);
        if (is_array($layerGroup) and count($layerGroup) > 0) {
            foreach ($layerGroup as $key => $val) {

                //Dividir capa en elementos
                $data = explode("___", $val);
                if (array_search($data[0], $layerIcon) === false and $data[1] === "punto") {
                    //Agregar Layer al array
                    $layerIcon[] = $data[0];

                    $custom = explode("custom|", $val);
                    //Existe un icono precargado en la carpeta temporal
                    if (is_array($custom) and isset($custom[1])) {
                        if (trim($custom[1]) != "") {
                            $origen = $custom[1];
                            $destino = str_replace("upload.icon/", "tmp/", $origen);

                            $copy = copy($destino, $origen);
                            unlink($destino);
                        }
                    }
                }
            }
        }
    }

    //Respuesta guardar proyecto
    echo json_encode($save);
}

if ($_POST["action"] == "doListLayer") {
    $GeoProyecto = new GeoProyecto();
    $list = $GeoProyecto->doListLayer($conexion);

    $proyectos = "<div style=\"display: table; width: 100%\">
				  <div style=\"display: table-row;\">
                    <div 
                        style=\"display: table-cell; 
                                text-align: center; 
                                width: 70%; 
                                background-color: #1B5790; 
                                color: #FFFFFF; 
                                padding: 5px\">
                        Proyectos
                    </div>
                    <div 
                        style=\"display: table-cell; 
                        text-align: center; 
                        background-color: #1B5790; 
                        color: #FFFFFF;
                        padding: 5px\">
                        Eliminar               
                    </div>
                    <div 
                        style=\"display: table-cell; 
                        text-align: center; 
                        background-color: #1B5790; 
                        color: #FFFFFF;
                        padding: 5px\">
                        Combinar               
                    </div>
                  </div>";
    foreach ($list as $key => $val) {
        $id = $val["id"];
        $proyectos .= "<div class=\"itemListaProyecto\">
	                    <div style=\"display: table-cell;\">
	                        <a href=\"$id\" class=\"loadProject\">" . $val['nombre'] . "</a>
	                    </div>
	                    <div style=\"display: table-cell; text-align: center\">
	                        <a href=\"$id\" class=\"deleteProject\">
                                    <img src=\"images/DeleteRed.png\" />
                                </a>              
	                    </div>
                            <div style=\"display: table-cell; text-align: center\">
	                       <input type=\"checkbox\" id=\"mix$id\" class=\"mixProject\">
	                    </div>
	                  </div>";
    }
    $proyectos .= "</div>";
    echo $proyectos;
}

if ($_POST["action"] == "getProjectLayer") {
    $GeoProyecto = new GeoProyecto();
    $list = $GeoProyecto->getProjectLayer($conexion, $_POST["id"]);

    $layer = array();

    $num = 1;
    foreach ($list as $key => $val) {
        $capa = "";
        $object = "Geo" . ucfirst($val["origen"]);
        $Item = new $object();

        $datos = explode("[{chars}]", $val["datos"]);
        $data = explode("___", $datos[0]);

        $geoArray = $Item->listar($conexion, $data);

        $capa = substr($datos[0], 0, strrpos($datos[0], "___"));

        $layer[] = array(
            "capa" => $capa,
            "datos" => $datos[0],
            "origen" => $val["origen"],
            "layer" => $val["layer"],
            "layer_id" => $val["layer_id"],
            "tipo" => $val["tipo"],
            "coords" => $geoArray,
            "chars" => $datos[1]
        );
        $num++;
    }
    $list       = null;
    $datos      = null;
    $data       = null;
    $geoArray   = null;
    echo json_encode($layer);
    $layer      = null;
}

if ($_POST["action"] == "deleteProject") {
    $GeoProyecto = new GeoProyecto();
    $delete = $GeoProyecto->deleteProject($conexion, $_POST["keyProject"]);

    echo json_encode($delete);
}

if ($_POST["action"] == "getProjectLayers") {
    $GeoProyectoCapas = new GeoProyectoCapas();
    $layers = $GeoProyectoCapas->getProjectLayer($conexion, $_POST["id"]);

    echo json_encode($layers);
}

if ( $_POST["action"] == "saveAddressList" ) {
    $GeoAddress = new GeoAddress();
    
    $save = $GeoAddress->saveList(
                $conexion,
                $_POST["proyecto"], 
                $_POST["capa"], 
                $_POST["capas"],
                $_SESSION['exp_user']['id']
            );
    
    echo json_encode($save);
}

if ( $_POST["action"] == "geoTramaAddress" ) 
{
    require_once 'clases/class.GeoTrama.php';
    $GeoTrama = new GeoTrama();

    $calle      = trim(addslashes($_POST["geo_trama_calle"]));
    $numero     = trim(addslashes($_POST["geo_trama_numero"]));
    $distrito   = trim(addslashes($_POST["geo_trama_distrito"]));

    //Numeracion izquierda o derecha
    $izq = true;
    if ( $_POST["geo_trama_numero"]%2 === 0 ) 
    {
        $izq = false;
    }

    $data = $GeoTrama->buscarDireccion(
        $conexion, $distrito, $calle, $numero, $izq
    );
    
    //->TMP archivo
    $fo = fopen("buscadir_troba.txt", "a");
    foreach ($data as $key=>$val) {
        $fw = fwrite($fo, $val["SEARCH"] . "\t" . $val["XY"] . "\r\n");
    }
    fclose($fo);
    //<-FIN
    
    echo json_encode($data);
}

if ( isset($_FILES['archivo']) and !empty($_FILES['archivo']) ) 
{
    require_once 'clases/class.GeoUtils.php';
    require_once 'clases/class.GeoTrama.php';
    $GeoTrama = new GeoTrama();
    $GeoUtils = new GeoUtils();
    
    //Debe existir la siguiente carpeta
    $upload_folder = 'tmpfile';

    $nombre_archivo = $_FILES['archivo']['name'];
    $ext_archivo = end((explode(".", $nombre_archivo)));

    $tipo_archivo = $_FILES['archivo']['type'];

    $tamano_archivo = $_FILES['archivo']['size'];

    $tmp_archivo = $_FILES['archivo']['tmp_name'];

    //Nuevo nombre de archivo MD5
    $archivo_nuevo = md5(date("YmdHis") . $nombre_archivo) . "." . $ext_archivo;

    $file = $upload_folder . '/' . $archivo_nuevo;

    if (!move_uploaded_file($tmp_archivo, $file)) {
        $return = array(
            'upload' => FALSE, 
            'msg' => "Ocurrio un error al subir el archivo. No pudo guardarse.", 
            'error' => $_FILES['archivo']
        );
    } else {
        $adrsArray = array();
        $datos = $GeoUtils->fileToJsonAddress($file);
        /*
        foreach ($datos as $key=>$val) {
            $array = explode(",", $val);
            $adrsArray[] = array(
                "ubigeo"=>$array[0],
                "nombre"=>$array[1],
                "via"=>$array[2],
                "numero"=>$array[3],
                );
        }
        echo json_encode($adrsArray);
        */
        $return = array(
            'upload' => TRUE, 
            'data' => $datos
        );
    }
    echo json_encode($return);
    
}
