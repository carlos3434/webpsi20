<?php
require_once("../../cabecera.php");
$_SESSION["proyecto"] = array();
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />

        <title></title>

        <?php include ("../../includes.php") ?>    


        <link type="text/css" href='../../css/jquery.multiselect.css' rel="Stylesheet" />
        <link rel="stylesheet" href="css/magnific-popup.css" />
        <link rel="stylesheet" href="css/easy-responsive-tabs.css" />
        <link href='../../js/jqueryui_1.8.2/css/redmond/jquery-ui-1.8.1.custom.css' rel="Stylesheet" />
        <link rel="stylesheet" type="text/css" href="css/georeferencia.css" />
        <link rel="stylesheet" type="text/css" href="css/colpick.css" />


        <script type="text/javascript" src="../../js/jquery.multiselect.js"></script>
        <script src="js/jquery.magnific-popup.min.js"></script>
        <script src="js/easyResponsiveTabs.js"></script>
        <script src="js/jquery.simple-color.min.js"></script>
        <script src="js/json2.js"></script>
        
        <script src="https://maps.googleapis.com/maps/api/js?libraries=geometry&v=3.exp&sensor=true"></script>
        <script src="js/georeferencia.js"></script>
        <script src="js/colpick.js"></script>
        <script src="js/jquery.md5.js"></script>

    </head>

    <body>

        <input type="hidden" value="<?php echo $IDUSUARIO ?>" name="txt_idusuario" id="txt_idusuario"/>

        <?php echo pintar_cabecera(); ?>

        <div id="map-wrapper">

        <div id="menu">
            <div id="map-ctrl">
                <div id="map-ctrl-element">
                    <div id="map-tab">
                        <ul class="resp-tabs-list">
                            <li class="searchElementPanel">
                                <img src="images/search_flat.png" style="vertical-align: middle" />
                                &nbsp;
                                Buscar elemento
                            </li>
                            <li class="projectDbList">
                                <img src="images/list_flat.png" style="vertical-align: middle" />
                                &nbsp;
                                Proyectos
                            </li>
                            <li class="currentProject">
                                <img src="images/edit_flat.png" style="vertical-align: middle" />
                                &nbsp;
                                Editar proyecto
                            </li>
                            <li class="filesProject">
                                <img src="images/earth_icon-24.png" style="vertical-align: middle" />
                                &nbsp;
                                Files
                            </li>
                        </ul>
                        <!--
                        <div style="position: relative; text-align: right" class="closeMenu">
                            <a href="">
                                Ocultar <img src="images/cross-button.png" />
                            </a>
                        </div>
                        -->
                        <div class="resp-tabs-container">
                            <div>
                                <div class="map-ctrl-container">
                                    <div>
                                        <div id="map-ctrl-elementos">
                                            <div class="geo_item_init">
                                                <!--
                                                Elemento
                                                <select name="geo_elemento" id="geo_elemento" title="" class="searchElementForm">
                                                    <option value=""> - Seleccione - </option>
                                                </select>
                                                <input type="button" name="btnClearItem" id="btnClearItem" value="LIMPIAR" class="button_example" title="Esta opción borrará todos los elementos del mapa" style="width: 100px" />
                                                -->
                                                
                                                <div style="display: table;">
                                                    <div style="display: table-row;">
                                                        <div style="display: table-cell;" class="cellLabel">
                                                            Elemento
                                                        </div>
                                                        <div style="display: table-cell;">
                                                            <select name="geo_elemento" id="geo_elemento" title="" class="searchElementForm">
                                                                <option value=""> - Seleccione - </option>
                                                            </select>
                                                            <input type="button" name="btnClearItem" id="btnClearItem" value="LIMPIAR" class="button_example" title="Esta opción borrará todos los elementos del mapa" />
                                                            <!-- <img src="images/basket_empty.png" id="basket_layer" style="vertical-align: middle" /> -->
                                                        </div>
                                                    </div>
                                                    <div style="display: table-row;">
                                                        <div style="display: table-cell;" class="cellLabel">
                                                            Patrón
                                                        </div>
                                                        <div style="display: table-cell;" class="patron_busqueda"></div>
                                                    </div>
                                                    <!--
                                                    <div style="display: table-row;">
                                                        <div style="display: table-cell;" class="cellLabel">
                                                            Desde archivo
                                                        </div>
                                                        <div style="display: table-cell;">
                                                            <select name="typeFile" id="typeFile">
                                                                <option value="">-Formato-</option>
                                                                <option value="txt">Tabulado</option>
                                                                <option value="csv">CSV</option>
                                                            </select>
                                                            <input type="file" name="file" id="file" />
                                                            <input type="button" name="uplayer" id="uplayer" value="Upload" />
                                                        </div>
                                                    </div>
                                                    -->
                                                    <div style="display: table-row;">
                                                        <div style="display: table-cell;" class="cellLabel">
                                                            Dependencias
                                                        </div>
                                                        <div style="display: table-cell;">
                                                            <div id="geo_response"></div>
                                                        </div>
                                                    </div>
                                                    
                                                </div>                                                
                                                
                                            </div>
                                            <!-- <div id="geo_response"></div> -->
                                        </div>
                                    </div>

                                </div>
                                <div id="map-ctrl-msg"></div>
                                <div id="map-ctrl-inherit">
                                    <div id="inherit1" style="max-height: 200px; overflow: auto"></div>
                                    <select name="cascade1" id="cascade1"></select>

                                    <fieldset style="padding: 10px; display: none" class="estiloItem" id="estilo_poligono">
                                        <legend>Pol&iacute;gono</legend>
                                        <div style="display: table;">
                                            <div style="display: table-row;">
                                                <div style="display: table-cell;">
                                                    <input type="radio" name="colorSelect" value="static" checked="checked"> Color &uacute;nico
                                                </div>
                                                <div style="display: table-cell;">
                                                    <input class="simple_color_color_code" id="colorPickerPoligono" value="#0066cc"/>
                                                </div>
                                            </div>
                                            <div style="display: table-row;">
                                                <div style="display: table-cell;">
                                                    <input type="radio" name="colorSelect" value="random"> Color aleatorio
                                                </div>
                                                <div style="display: table-cell;">
                                                    <div class="slider" data-wjs-element="box" style="width: 100px"></div>
                                                </div>
                                            </div>
                                            <div style="display: table-row;">
                                                <div style="display: table-cell;">
                                                    <input type="checkbox" name="colorLine" id="colorLine" value="color_line">
                                                        Color de l&iacute;nea
                                                </div>
                                                <div style="display: table-cell;">
                                                    <input class="simple_color_color_code" id="colorPickerLine" value="#0066cc"/>
                                                </div>
                                            </div>
                                            <div style="display: table-row;">
                                                <div style="display: table-cell;">
                                                    Espesor de l&iacute;nea
                                                </div>
                                                <div style="display: table-cell;">
                                                    <select name="lineWeight" id="lineWeight">
                                                        <?php
                                                        for ($i=0; $i<=10; $i++) {
                                                            echo "<option value=\"$i\">$i</option>";
                                                        }
                                                        ?>
                                                    </select> Px.
                                                </div>
                                            </div>
                                        </div>
                                    </fieldset>

                                    <fieldset style="padding: 10px; display: none" class="estiloItem" id="estilo_punto">
                                        <legend>Punto</legend>
                                        <div style="display: table;">
                                            <div style="display: table-row; height: 25px">
                                                <div style="display: table-cell;">
                                                    <input type="radio" name="iconSelect" value="default" checked="checked"> Por defecto
                                                </div>
                                                <div style="display: table-cell;">

                                                </div>
                                            </div>
                                            <div style="display: table-row; height: 25px">
                                                <div style="display: table-cell;">
                                                    <input type="radio" name="iconSelect" value="numeric" /> Numerico
                                                </div>
                                                <div style="display: table-cell;">
                                                    <input class="simple_color_color_code" id="colorPickerPunto" value="#0066cc"/>
                                                </div>
                                            </div>
                                            <div style="display: table-row; height: 25px">
                                                <div style="display: table-cell;">
                                                    <input type="radio" name="iconSelect" value="custom" /> Personalizado
                                                    <img id="customIconPreview" src="images/icon-image.png" style="vertical-align: middle; padding: 3px" />
                                                </div>
                                                <div style="display: table-cell;">
                                                    <form action="georeferencia.request.php" method="post" enctype="multipart/form-data" target="uploadIcon">
                                                        <input type="hidden" name="action" id="action" value="uploadIcon" />
                                                        <input type="hidden" name="tmpIcon" id="tmpIcon" />
                                                        <input type="file" name="archivoImage" id="archivoImage" />                                                       
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </fieldset>                                
                                    <div class="buttonSet" style="display: none; text-align: center; width: 100%">
                                        <input type="button" name="btnDrawItem" id="btnDrawItem" value="DIBUJAR CAPA" class="button_example" title="Esta opción creará una nueva capa" />
                                        <!-- <input type="button" name="btnClearItem" id="btnClearItem" value="LIMPIAR" class="button_example" title="Esta opción borrará todos los elementos del mapa" style="width: 100px" /> -->
                                    
                                        <p>&nbsp;</p>

                                        Nombre del proyecto <input type="text" name="proyecto" id="proyecto" class="searchElementForm">
                                        <input type="button" name="btnSaveLayout" id="btnSaveLayout" value="Guardar Proyecto" class="button_example" />                                
                                    </div>
                                    </div>
                                </div>
                                <div>
                                    <!-- Capas generadas -->
                                    <!-- <input type="button" name="btnUpdateLayer" id="btnUpdateLayer" value="Actualizar"> -->
                                    <div id="layoutList"></div>
                                </div>
                                <div>
                                    <!-- Current project -->
                                    <h3 class="projectTitle" style="text-align: center"></h3>
                                    <div style="display: table; width: 100%">
                                        <div style="display: table-row">
                                            <div style="display: table-cell" id="currentProject"></div>
                                            <div style="display: table-cell; width: 45%" id="editZone">
                                                <input type="hidden" name="keyEdit" id="keyEdit">
                                                <input type="hidden" name="proIdEdit" id="proIdEdit" value="0">

                                                <fieldset style="padding: 10px; display: none" id="estilo_poligono_edit">
                                                    <legend>Pol&iacute;gono</legend>
                                                    <div style="display: table;">
                                                        <div style="display: table-row;">
                                                            <div style="display: table-cell;">
                                                                <input type="radio" name="colorSelectEdit" value="static" checked="checked"> Color &uacute;nico
                                                            </div>
                                                            <div style="display: table-cell;">
                                                                <input class="simple_color_color_code" id="colorPickerPoligonoEdit" value="#0066cc"/>
                                                            </div>
                                                        </div>
                                                        <div style="display: table-row;">
                                                            <div style="display: table-cell;">
                                                                <input type="radio" name="colorSelectEdit" value="random"> Color aleatorio
                                                            </div>
                                                            <div style="display: table-cell;">
                                                                <div class="sliderEdit" data-wjs-element="box" style="width: 100px"></div>
                                                            </div>
                                                        </div>
                                                        <div style="display: table-row;">
                                                            <div style="display: table-cell;">
                                                                <input type="checkbox" name="colorLineEdit" id="colorLineEdit" value="color_line">
                                                                    Color de l&iacute;nea
                                                            </div>
                                                            <div style="display: table-cell;">
                                                                <input class="simple_color_color_code" id="colorPickerLineEdit" value="#0066cc"/>
                                                            </div>
                                                        </div>
                                                        <div style="display: table-row;">
                                                            <div style="display: table-cell;">
                                                                Espesor de l&iacute;nea
                                                            </div>
                                                            <div style="display: table-cell;">
                                                                <select name="lineEditWeight" id="lineEditWeight">
                                                                    <?php
                                                                    for ($i=0; $i<=10; $i++) {
                                                                        echo "<option value=\"$i\">$i</option>";
                                                                    }
                                                                    ?>
                                                                </select> Px.
                                                            </div>
                                                        </div>
                                                        <div style="display: table-row;">
                                                            <div style="display: table-cell;">
                                                                <input type="button" id="savePoli" value="Aplicar" class="button_example" />
                                                            </div>
                                                            <div style="display: table-cell;">
                                                                <input type="button" class="saveAllEditLayer" value="Guardar" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                </fieldset>

                                                <fieldset style="padding: 10px; display: none" id="estilo_punto_edit">
                                                    <legend>Punto</legend>
                                                    <div style="display: table;">
                                                        <div style="display: table-row;">
                                                            <div style="display: table-cell;">
                                                                <input type="radio" name="iconSelectEdit" value="default" checked="checked"> Por defecto
                                                            </div>
                                                        </div>
                                                        <div style="display: table-row;">
                                                            <div style="display: table-cell;">
                                                                <input type="radio" name="iconSelectEdit" value="numeric"> Numerico
                                                                <input class="simple_color_color_code" id="colorPickerPuntoEdit" value="#0066cc"/>
                                                            </div>
                                                        </div>
                                                        <div style="display: table-row; height: 25px">
                                                            <div style="display: table-cell;">
                                                                <input type="radio" name="iconSelectEdit" value="custom" /> Personalizado
                                                                <img id="customIconPreviewEdit" src="images/icon-image.png" style="vertical-align: middle; padding: 3px" />
                                                                <form action="georeferencia.request.php" method="post" enctype="multipart/form-data" target="uploadIcon">
                                                                    <input type="hidden" name="actionEdit" id="actionEdit" value="uploadIcon" />
                                                                    <input type="hidden" name="tmpIconEdit" id="tmpIconEdit" />
                                                                    <input type="file" name="archivoImageEdit" id="archivoImageEdit" />                                                       
                                                                </form>
                                                            </div>
                                                        </div>
                                                        <div style="display: table-row;">
                                                            <div style="display: table-cell; text-align: center">
                                                                <input type="button" id="savePunto" value="Aplicar" class="button_example" />
                                                                <input type="button" class="saveAllEditLayer" value="Guardar" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                </fieldset>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div>
                                    <!--<h3>KML o KMZ</h3>-->
                                    <!--
                                    <input type="file" name="loadfile" id="loadfile" />
                                    <br>
                                    <a title="file0" href="http://zoolbox.com/kmlkmz/continents.kmz" class="getFileLayer">Continents</a>
                                    <br>
                                    <a title="file1" href="http://zoolbox.com/kmlkmz/polygon-fade.kml" class="getFileLayer">Polygon Fade</a>
                                    <br>
                                    <a title="file2" href="http://zoolbox.com/kmlkmz/nuevas-trobas-2014.kmz" class="getFileLayer">Nuevas trobas 2014</a>
                                    <br>
                                    <a title="file2" href="http://190.233.113.45:2230/webpsi/modulos/georeferencia/files/nuevas-trobas-2014.kmz" class="getFileLayer">Local: Nuevas trobas 2014</a>
                                    <br>
                                    <a title="file5" href="http://zoolbox.com/kmlkmz/space-needle.kml" class="getFileLayer">Space Needle</a>
                                    <br>
                                    <a title="file6" href="http://zoolbox.com/kmlkmz/macky-alt.kmz" class="getFileLayer">Macky alt</a>
                                    -->
                                    <!--
                                    <br>
                                    <a title="file0" href="http://zoolbox.com/kmlkmz/_punto.kml" class="getFileLayer">_punto</a>
                                    <br>
                                    <a title="file1" href="http://zoolbox.com/kmlkmz/_Resto_2014.kml" class="getFileLayer">_Resto_2014</a>
                                    <br>
                                    <a title="file2" href="http://zoolbox.com/kmlkmz/_Trobas_M1_93_2014.kml" class="getFileLayer">_Trobas_M1_93_2014</a>
                                    <br>
                                    <a title="file3" href="http://zoolbox.com/kmlkmz/_Trobas_M1_94_2014.kml" class="getFileLayer">_Trobas_M1_94_2014</a>
                                    <br>
                                    <a title="file4" href="http://zoolbox.com/kmlkmz/_Trobas_M1_resto_de_las_457.kml" class="getFileLayer">_Trobas_M1_resto_de_las_457</a>
                                    <br>
                                    <a title="file5" href="http://zoolbox.com/kmlkmz/_uranac.kml" class="getFileLayer">_uranac</a>
                                    <br>
                                    <a title="file6" href="http://zoolbox.com/kmlkmz/_uraNac.kmz" class="getFileLayer">_uraNac</a>
                                    <br>
                                    <a title="file7" href="http://zoolbox.com/kmlkmz/_Trobas_Lima.kml" class="getFileLayer">_Trobas_Lima</a>
                                    <br>
                                    <a title="file8" href="http://zoolbox.com/kmlkmz/_Trobas_M1_2013.kml" class="getFileLayer">_Trobas_M1_2013</a>
                                    <br>
                                    <a title="file9" href="http://zoolbox.com/kmlkmz/_Zona_HFC.kml" class="getFileLayer">_Zona_HFC</a>
                                    
                                    <hr />
                                    -->
                                    <h4>Formato</h4>
                                    <p>Direcci&oacute;n, distrito, provincia, pais</p>
                                    <input type="text" name="address" id="address" size="70" />
                                    <input type="button" name="buscadir" id="buscadir" value="Buscar" />
                                    <hr />
                                    Busqueda por archivo <input type="file" name="dirfile" id="dirfile" />
                                    <input type="button" name="updirfile" id="updirfile" value="Direcciones" />
                                    <hr />
                                    <div id="addressControl"></div>
                                    <div id="address_result" style="margin: 8px; max-height: 300px; overflow: auto"></div>
                                    
                                </div>

                            </div>
                        </div>
                        <div class="white-popup mfp-hide" id="polygonOptions">
                            <h3>Polygon Options</h3>
                            <input class="simple_color_color_code" id="colorPickerPoligonoEdit" value=""/>
                            <div class="slider" data-wjs-element="box" style="width: 100px"></div>
                            <input type="button" name="savePolygon" id="savePolygon" value="Guardar Cambios">
                        </div>

                    </div>

                </div>
            </div>
            <div id="tabControl">
                <a href=""><img src="images/map_edit.png" /></a>
            </div>
        <div id="content"></div>
        
        </div>
        
        <!-- Modal loading effect -->
        <div class="modalPop"></div>
</body>
</html>