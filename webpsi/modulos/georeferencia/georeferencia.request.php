<?php
set_time_limit(0);
session_start();
include_once 'autoload.php';

$cnxObj = new ConexionMapas();
$conexion = $cnxObj->conectarBD();

/**
 *
 * Acciones
 *
 */
if ($_POST["action"] == "geoElementos") {
    $GeoElemento = new GeoElemento();
    $elementoArray = $GeoElemento->listar(
            $conexion, true
    );
    echo json_encode($elementoArray);
}

if ($_POST["action"] == "getGeoDedependencia") {
    $GeoDependencia = new GeoDependencia();
    $dependenciaArray = $GeoDependencia->listar(
            $conexion, $_POST["elemento_id"]
    );

    $geoSelect = "";
    $depString = "";
    $n = 1;
    foreach ($dependenciaArray as $key => $val) {
        $multiple = "";
        $geoOption = "";
        if ($n == 1) {
            $object = "Geo" . ucfirst($_POST["origen"]);
            $Item = new $object();
            $itemArray = $Item->primerFiltro(
                    $conexion
            );

            $geoOption .= "<option value=\"\">-Seleccione-</option>";
            foreach ($itemArray as $a => $b) {
                $geoOption .= "<option value=\"{$b['nombre']}\">{$b['nombre']}</option>";
            }
        }
        if ($n == count($dependenciaArray)) {
            $multiple = 'multiple="multiple" size="1"';
        }
        $depString .= "_" . $val['campo'];
        $geoSelect .= "<select title=\"title_prop\" class=\"geo_item\" name=\"geo_{$val['campo']}\" id=\"geo_{$val['campo']}\" $multiple>$geoOption</select>";
        $n++;
    }
    $depString = substr($depString, 1);
    echo str_replace('title_prop', $depString, $geoSelect);
}

if ($_POST["action"] == "getGeoItemIni") {
    $object = "Geo" . ucfirst($_POST["origen"]);
    $Item = new $object();
    $itemArray = $Item->primerFiltro(
            $conexion
    );
    echo json_encode($itemArray);
}

if ($_POST["action"] == "getGeoItem") {
    $object = "Geo" . ucfirst($_POST["origen"]);
    $Item = new $object();
    $method = "listar" . ucfirst($_POST["element"]);
    $itemArray = $Item->$method(
            $conexion, explode("___", $_POST["dependencia"])
    );

    $geoOption = "<option value=\"\">-Seleccione-</option>";
    foreach ($itemArray as $key => $val) {
        $geoOption .= "<option value=\"{$val['campo']}\">{$val['campo']}</option>";
    }
    echo $geoOption;
}

if ($_POST["action"] == "drawPolygon") {

    $object = "Geo" . $_POST["origen"];
    $Item = new $object();

    $data = explode("___", $_POST["data"]);
    $data[] = $_POST["item"];

    $geoArray = $Item->listar($conexion, $data);
    echo json_encode($geoArray);
}

if ($_POST["action"] == "drawMarker") {

    $object = "Geo" . $_POST["origen"];
    $Item = new $object();

    $data = explode("___", $_POST["data"]);
    $data[] = $_POST["item"];

    $geoArray = $Item->listar($conexion, $data);
    echo json_encode($geoArray);
}

if ($_POST["action"] == "drawCircle") {

    $object = "Geo" . $_POST["origen"];
    $Item = new $object();

    $data = explode("___", $_POST["data"]);
    $data[] = $_POST["item"];

    $geoArray = $Item->listar($conexion, $data);
    echo json_encode($geoArray);
}

if ($_POST["action"] == "saveObject") {

    $proyecto = array();
    $capas = array();

    //Datos del proyecto
    $proyecto["nombre"] = $_POST["proyecto"];
    //Project Key
    $proyecto["keyId"] = $_POST["keyProject"];
    //Reemplazar ruta de icono final
    $_POST["capas"] = str_replace("/tmp/", "/upload.icon/", $_POST["capas"]);

    $capas = explode("[{layer}]", $_POST["capas"]);
    foreach ($capas as $key => $capa) {
        $src = explode("|^", $capa);
        $data = explode("___", $src[0]);

        $idcapa = $data[0];
        $tipo = $data[1];
        $origen = $data[2];
        $filtro = $src[1];

        $layer[] = array(
            "layer" => $idcapa,
            "tipo" => $tipo,
            "origen" => $origen,
            "datos" => $filtro);

        //echo "Detalle" . $tipo . "-" . $origen . "-" . $filtro;
    }

    //Guardar cambios
    $GeoProyecto = new GeoProyecto();
    $save = $GeoProyecto->saveProject($conexion, $proyecto, $layer);

    //Verificar y guardar icono temporal
    if ($save["estado"]) {

        //Almacenar capas ya asociadas al icono
        $layerIcon = array();

        $layerGroup = explode("[{layer}]", $_POST["capas"]);
        if (is_array($layerGroup) and count($layerGroup) > 0) {
            foreach ($layerGroup as $key => $val) {

                //Dividir capa en elementos
                $data = explode("___", $val);
                if (array_search($data[0], $layerIcon) === false and $data[1] === "punto") {
                    //Agregar Layer al array
                    $layerIcon[] = $data[0];

                    $custom = explode("custom|", $val);
                    //Existe un icono precargado en la carpeta temporal
                    if (is_array($custom) and isset($custom[1])) {
                        if (trim($custom[1]) != "") {
                            $origen = $custom[1];
                            $destino = str_replace("upload.icon/", "tmp/", $origen);

                            $copy = copy($destino, $origen);
                            unlink($destino);
                        }
                    }
                }
            }
        }
    }

    echo json_encode($save);
}

if ($_POST["action"] == "doListLayer") {
    $GeoProyecto = new GeoProyecto();
    $list = $GeoProyecto->doListLayer($conexion);

    $proyectos = "<div style=\"display: table; width: 100%\">
				  <div style=\"display: table-row;\">
                    <div 
                        style=\"display: table-cell; 
                                text-align: center; 
                                width: 70%; 
                                background-color: #1B5790; 
                                color: #FFFFFF; 
                                padding: 5px\">
                        Proyectos
                    </div>
                    <div 
                        style=\"display: table-cell; 
                        text-align: center; 
                        background-color: #1B5790; 
                        color: #FFFFFF;
                        padding: 5px\">
                        Eliminar               
                    </div>
                    <div 
                        style=\"display: table-cell; 
                        text-align: center; 
                        background-color: #1B5790; 
                        color: #FFFFFF;
                        padding: 5px\">
                        Combinar               
                    </div>
                  </div>";
    foreach ($list as $key => $val) {
        $id = $val["id"];
        $proyectos .= "<div class=\"itemListaProyecto\">
	                    <div style=\"display: table-cell;\">
	                        <a href=\"$id\" class=\"loadProject\">" . $val['nombre'] . "</a>
	                    </div>
	                    <div style=\"display: table-cell; text-align: center\">
	                        <a href=\"$id\" class=\"deleteProject\">
                                    <img src=\"images/DeleteRed.png\" />
                                </a>              
	                    </div>
                            <div style=\"display: table-cell; text-align: center\">
	                       <input type=\"checkbox\" id=\"mix$id\" class=\"mixProject\">
	                    </div>
	                  </div>";
    }
    $proyectos .= "</div>";
    echo $proyectos;
}

if ($_POST["action"] == "getProjectLayer") {
    $GeoProyecto = new GeoProyecto();
    $list = $GeoProyecto->getProjectLayer($conexion, $_POST["id"]);

    $layer = array();

    $num = 1;
    foreach ($list as $key => $val) {
        $capa = "";
        $object = "Geo" . ucfirst($val["origen"]);
        $Item = new $object();

        $datos = explode("[{chars}]", $val["datos"]);
        $data = explode("___", $datos[0]);

        $geoArray = $Item->listar($conexion, $data);

        $capa = substr($datos[0], 0, strrpos($datos[0], "___"));

        $layer[] = array(
            "capa" => $capa,
            "datos" => $datos[0],
            "origen" => $val["origen"],
            "layer" => $val["layer"],
            "layer_id" => $val["layer_id"],
            "tipo" => $val["tipo"],
            "coords" => $geoArray,
            "chars" => $datos[1]
        );
        $num++;
    }
    $list       = null;
    $datos      = null;
    $data       = null;
    $geoArray   = null;
    echo json_encode($layer);
    $layer      = null;
}

if ($_POST["action"] == "deleteProject") {
    $GeoProyecto = new GeoProyecto();
    $delete = $GeoProyecto->deleteProject($conexion, $_POST["keyProject"]);

    echo json_encode($delete);
}

if ($_POST["action"] == "getProjectLayers") {
    $GeoProyectoCapas = new GeoProyectoCapas();
    $layers = $GeoProyectoCapas->getProjectLayer($conexion, $_POST["id"]);

    echo json_encode($layers);
}

if ( $_POST["action"] == "saveAddressList" ) {
    $GeoAddress = new GeoAddress();
    
    $save = $GeoAddress->saveList(
                $conexion,
                $_POST["proyecto"], 
                $_POST["capa"], 
                $_POST["capas"]
            );
    
    echo json_encode($save);
}


