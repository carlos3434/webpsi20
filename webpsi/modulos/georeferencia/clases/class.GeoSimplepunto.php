<?php

class GeoSimplepunto
{

    protected $_db = 'webpsi';
    protected $_table = 'geo_simplepunto';

    public function listar($conexion, $arreglo = array())
    {
        try {
            $codigo = $arreglo[0];

            $sql = "SELECT id, detalle campo, coord_x, coord_y  
                    FROM " . $this->_db . "." . $this->_table . " WHERE id<>'' ";

            $sql .= " AND codigo='$codigo'";
            
            //$sql .= " ORDER BY tipo, cliente";
            $bind = $conexion->prepare($sql);
            $bind->execute();

            $reporte = array();
            while ($data = $bind->fetch(PDO::FETCH_ASSOC)) {
                $reporte[] = $data;
            }
            return $reporte;
        } catch (PDOException $error) {
            exit();
        }
    }

    public function primerFiltro($conexion){
        try {
            $sql = "SELECT zonal nombre, zonal id 
                    FROM " . $this->_db . "." . $this->_table 
                    . " GROUP BY 1 ORDER BY 1";
            $bind = $conexion->prepare($sql);
            $bind->execute();

            $reporte = array();
            while ($data = $bind->fetch(PDO::FETCH_ASSOC)) {
                $reporte[] = $data;
            }
            return $reporte;
        } catch (PDOException $e) {
            exit();
        }
    }

    public function listarSimplepunto($conexion, $arreglo=array()){
        try {
            $codigo = $arreglo[0];

            $sql = "SELECT detalle campo, codigo id 
                    FROM " . $this->_db . "." . $this->_table 
                    . " WHERE codigo='$codigo'"
                    . " GROUP BY 1 ORDER BY 1";
            $bind = $conexion->prepare($sql);
            $bind->execute();

            $reporte = array();
            while ($data = $bind->fetch(PDO::FETCH_ASSOC)) {
                $reporte[] = $data;
            }
            return $reporte;
        } catch (PDOException $e) {
            exit();
        }   
    }

}