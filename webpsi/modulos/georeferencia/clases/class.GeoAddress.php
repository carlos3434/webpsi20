<?php

class GeoAddress {

    protected $_db = 'webpsi';
    protected $_table = 'geo_address';

    public function saveList($dbh, $nombre, $capa, $capas, $usuario_id) {
        try {
            //Iniciar transaccion
            $dbh->beginTransaction();

            //Registrar proyecto
            $sql = "INSERT INTO " 
                    . $this->_db 
                    . ".geo_proyectos 
                    (nombre, fec_creacion, usuario_id, estado) 
                    VALUES ('$nombre', NOW(), $usuario_id, 1)";
            $dbh->exec($sql);
            
            //Ultimo ID registrado
            $keyId = $dbh->lastInsertId();
            $layerId = md5( $keyId . $capa );
            
            //Proyecto y permisos de usuario
            $sql = "INSERT INTO 
                        $this->_db.geo_proyecto_usuarios 
                        (geo_proyecto_id, usuario_id, editar, fec_registro) 
                    VALUES 
                        ('$keyId', '$usuario_id', '1', NOW())";
            $dbh->exec($sql);
            
            //Registrar capa
            $sql = "INSERT INTO " . $this->_db . ".geo_proyecto_capas "
                    . "(layer_id, geo_proyecto_id, layer, tipo, "
                    . "origen, estilo, estilo_activo) "
                    . "VALUES ('$layerId', '$keyId', '$capa', "
                    . "'punto', 'address', '', '0')";
            $dbh->exec($sql);
            
            //Elementos
            $layerArray = explode("[{layer}]", $capas);
            foreach ($layerArray as $id=>$val) {
                $addressArray = explode("[{chars}]", $val);
                $dataArray = explode("[|^]", $addressArray[0]);
                $datos = $dataArray[0]
                         . "___"
                         . md5($dataArray[1])
                         . "___"
                         . $dataArray[2]
                         . "[{chars}]"
                         . $addressArray[1];
                
                //Tabla elementos
                $sql = "INSERT INTO "
                        . $this->_db
                        . ".geo_proyecto_elementos (geo_proyecto_id, 
                            layer_id, layer, tipo, origen, datos)
                        VALUES 
                        ($keyId, '$layerId', '$capa', "
                        . "'punto', 'address', '$datos')";
                $dbh->exec($sql);
                
                //Tabla geo_address
                $nom = $dataArray[0];
                $dir = $dataArray[1];
                $did = md5( $dir );
                $idd = $dataArray[2];
                $lng = $dataArray[3];
                $lat = $dataArray[4];
                $sql = "INSERT INTO "
                        . $this->_db
                        . ".geo_address (nombre, dir, dir_id, direccion, "
                        . "coord_x, coord_y) VALUES ("
                        . "'$nom', '$did', '$idd', '$dir', '$lng', '$lat')";
                $dbh->exec($sql);
            }

            $dbh->commit();
            $result["estado"] = true;
            $result["msg"] = "Proyecto registrado correctamente";
            $result["id"] = $id;
            return $result;
        } catch (PDOException $error) {
            $dbh->rollback();
            $result["estado"] = false;
            $result["msg"] = $error->getMessage();
            return $result;
            exit();
        }
    }

    private function getAddressLayer($capas) {
        
    }

    private function getAddressData($capas) {
        
    }

    public function primerFiltro($conexion) {
        try {
            $sql = "SELECT nombre, nombre id 
                    FROM " . $this->_db . "." . $this->_table
                    . " GROUP BY 1 ORDER BY 1";
            $bind = $conexion->prepare($sql);
            $bind->execute();

            $reporte = array();
            while ($data = $bind->fetch(PDO::FETCH_ASSOC)) {
                $reporte[] = $data;
            }
            return $reporte;
        } catch (PDOException $e) {
            exit();
        }
    }

    public function listarDir($conexion, $arreglo = array()) {
        try {
            $nombre = $arreglo[0];

            $sql = "SELECT dir campo, dir id 
                    FROM " . $this->_db . "." . $this->_table
                    . " WHERE nombre='$nombre'"
                    . " GROUP BY 1 ORDER BY 1";
            $bind = $conexion->prepare($sql);
            $bind->execute();

            $reporte = array();
            while ($data = $bind->fetch(PDO::FETCH_ASSOC)) {
                $reporte[] = $data;
            }
            return $reporte;
        } catch (PDOException $e) {
            exit();
        }
    }

    public function listarDir_id($conexion, $arreglo = array()) {
        try {
            $nombre = $arreglo[0];
            $dir = $arreglo[1];

            $sql = "SELECT dir_id campo, dir_id id 
                    FROM " . $this->_db . "." . $this->_table
                    . " WHERE nombre='$nombre'"
                    . " AND dir='$provincia'"
                    . " GROUP BY 1 ORDER BY 1";
            $bind = $conexion->prepare($sql);
            $bind->execute();

            $reporte = array();
            while ($data = $bind->fetch(PDO::FETCH_ASSOC)) {
                $reporte[] = $data;
            }
            return $reporte;
        } catch (PDOException $e) {
            exit();
        }
    }

    public function listar($conexion, $data = array()) {
        try {
            $nombre = $data[0];
            $dir    = $data[1];
            $dir_id = $data[2];

            $sql = "SELECT dir_id id, direccion, coord_x, coord_y   
                    FROM " . $this->_db . "." . $this->_table . " WHERE dir_id<>'' ";

            $sql .= " AND nombre='$nombre' 
                      AND dir='$dir'
                      AND dir_id='$dir_id'";

            $sql .= "ORDER BY nombre";
            $bind = $conexion->prepare($sql);
            $bind->execute();

            $reporte = array();
            while ($data = $bind->fetch(PDO::FETCH_ASSOC)) {
                $reporte[] = $data;
            }
            return $reporte;
        } catch (PDOException $error) {
            exit();
        }
    }

}
