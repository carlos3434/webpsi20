<?php

/**
 * @package     class/data/
 * @name        class.RegionZonalEdi.php
 * @category    Model
 * @author      Wilder Sandoval Huamani <wsandoval@gmd.com.pe>
 * @copyright   2012 Telefonica del Peru
 * @version     1.0 - 2012/04/16
 */

class Data_RegionZonalEdi
{

    protected $_table = 'webunificada_fftt.edi_region_zonal';
    
    protected $_idRegionZonal = '';
    protected $_idRegion = '';
    protected $_zonal = '';
    protected $_flag = '';

    /**
     * Metodo constructor
     * @method __construct
     */
    public function __construct()
    {
    }
    
    /**
     * Obtiene el valor de un atributo
     *
     * @return string $returnValue
     * @param string $propiedad nombre del atributo a obtener
     */
    public function __get($propiedad)
    {
        $returnValue = (string) '';
        $returnValue = $this->$propiedad;
        return (string) $returnValue;
    }

    /**
     * Asigna un valor a un atributo
     *
     * @param string $propiedad nombre del atributo a setear
     * @param string $valor valor a asignar
     */
    public function __set($propiedad, $valor)
    {
        $this->$propiedad = $valor;
    }

    /**
     * Obtiene un listado de las zonales por region
     *
     * @return array $array[] array de objetos de tipo Data_RegionZonalEdi
     * @param object $conexion Objeto de conexion a la BD creada con PDO
     * @param array $arrRegion array de regiones
     * @param array $arrZonal array de zonales
     */
    public function listar($conexion, $arrRegion=array(), $arrZonal=array())
    {
        try {
            $strWhere = "";
            if (!empty($arrRegion)) {
                $strWhere .= " AND edi.idregion 
                    IN ('" . implode('\',\'', $arrRegion) . "') ";
            }
            if (!empty($arrZonal)) {
                $strWhere .= " AND edi.zonal
                IN ('" . implode('\',\'', $arrZonal) . "') ";
            }

            $sql = "SELECT edi.idregion_zonal, edi.idregion, edi.zonal, 
                    edi.flag, reg.nom_region, reg.det_region, zon.desc_zonal 
                    FROM " . $this->_table . " edi, 
                    webunificada_fftt.edi_region reg, webunificada.zonal zon 
                    WHERE edi.idregion = reg.idregion 
                    AND zon.abv_zonal = edi.zonal 
                    " . $strWhere . " 
                    AND edi.flag = '1' 
                    ORDER BY edi.idregion";
            $array = array();
            foreach ($conexion->query($sql) as $row):
                if($row['idregion_zonal'] != ''):                
                    $objRegionZonal = new Data_RegionZonalEdi();
                    $objRegionZonal->__set(
                        '_idRegionZonal', $row['idregion_zonal']
                    );
                    $objRegionZonal->__set('_idRegion', $row['idregion']);
                    $objRegionZonal->__set('_zonal', $row['zonal']);
                    $objRegionZonal->__set('_flag', $row['flag']);
                    $objRegionZonal->__set('_nomRegion', $row['nom_region']);
                    $objRegionZonal->__set('_detRegion', $row['det_region']);
                    $objRegionZonal->__set('_descZonal', $row['desc_zonal']);
                    $array[] = $objRegionZonal;
                endif;
            endforeach;
            
            return $array;
        } catch(PDOException $e) {
            throw $e;
        }
    }
}