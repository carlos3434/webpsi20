<?php

class GeoDependencia
{

    protected $_db = 'webpsi';
    protected $_table = 'geo_dependencias';

    public function listar($conexion, $elemento_id = false)
    {
        try {
            
            $sql = "SELECT campo, orden, target  
                    FROM " . $this->_db . "." . $this->_table 
                    . " WHERE orden<>0 ";

            if ( $elemento_id !== false ) {
                (int) $elemento_id;
                $sql .= " AND elemento_id = $elemento_id ";
            }
            $sql .= " ORDER BY orden";
            
            $bind = $conexion->prepare($sql);
            $bind->execute();

            $reporte = array();
            while ($data = $bind->fetch(PDO::FETCH_ASSOC)) {
                $reporte[] = $data;
            }
            return $reporte;
        } catch (PDOException $error) {
            exit();
        }
    }

}