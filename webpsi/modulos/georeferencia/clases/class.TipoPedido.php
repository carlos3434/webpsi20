<?php
/*
 * Sistema Web Unificada
 * Planificacion y Soluciones Informaticas - Telefonica del Peru
 * GMD S.A
 * Fecha de Actualizacion: 10-08-2011
 * @autor: Gonzalo Chacaltana Buleje <gchacaltana@gmd.com.pe>
 */

class Data_TipoPedido
{

    protected $_idTipoPedido = 0;
    protected $_tipoPedido = '';
    protected $_codPedido = '';
    protected $_tiempo = 0;
    protected $_tiempoEecc = 0;
    
    public $table = 'tipo_pedido';

    public function __construct()
    {
    }

    public function __get($propiedad)
    {
        $returnValue = (string) '';
        $returnValue = $this->$propiedad;
        return (string) $returnValue;
    }

    public function __set($propiedad, $valor)
    {
        $this->$propiedad = $valor;
    }

    public function save($conexion, $tipoPedido, $codPedido, $tiempo)
    {
        try {
            $sql = "INSERT INTO ".$this->table." (tipo_pedido,cod_pedido,";
            $sql .= "tiempo) VALUES (:tipo_pedido,:cod_pedido,:tiempo)";
            $bind = $conexion->prepare($sql);
            $bind->bindParam(":tipo_pedido", $tipoPedido);
            $bind->bindParam(":cod_pedido", $codPedido);
            $bind->bindParam(":tiempo", $tiempo);
            $bind->execute();
        } catch(PDOException $error) {
            return $error;
            exit();
        }
    }

    public function update($conexion, $idTipoPedido, $tipoPedido, 
$codPedido, $tiempo)
    {
        try {
            $sql = "UPDATE ".$this->table." SET tipo_pedido = :tipo_pedido, ";
            $sql .= "cod_pedido = :cod_pedido, tiempo = :tiempo ";
            $sql .= "WHERE idtipo_pedido = ".$idTipoPedido;
            $bind = $conexion->prepare($sql);
            $bind->bindParam(":tipo_pedido", $tipoPedido);
            $bind->bindParam(":cod_pedido", $codPedido);
            $bind->bindParam(":tiempo", $tiempo);
            $bind->bindParam(":idtipo_pedido", $idTipoPedido);
            $bind->execute();
        } catch(PDOException $error) {
            return $error;
            exit();
        }
    }

    public function offsetGet($conexion, $idTipoPedido, $etiqueta='')
    {
        $objTipPedido = new Data_TipoPedido();
        $arrTipoPedido = $objTipPedido->listar($conexion);              
        foreach($arrTipoPedido as $objeto):
           if($etiqueta == ''):
               if($objeto->__get('_idTipoPedido') == $idTipoPedido):
                   return $objeto;
               endif;
           else:               
               if(strcmp($objeto->__get('_tipoPedido'), $idTipoPedido) == 0):
                   return $objeto;
               endif;
           endif;
        endforeach;
        return false;
    }

    public function listar($conexion)
    {
        try {
            $sql = "SELECT idtipo_pedido, tipo_pedido, cod_pedido, tiempo, ";
            $sql .= "tiempo_eecc FROM ".$this->table." ORDER BY tipo_pedido";

            $array = array();
            foreach ($conexion->query($sql) as $row):
                if($row['idtipo_pedido'] != ''):
                    $objTipPedido = new Data_TipoPedido();
                    $objTipPedido->__set(
                        '_idTipoPedido', 
                        $row['idtipo_pedido']
                    );
                    $objTipPedido->__set('_tipoPedido', $row['tipo_pedido']);
                    $objTipPedido->__set('_codPedido', $row['cod_pedido']);
                    $objTipPedido->__set('_tiempo', $row['tiempo']);
                    $objTipPedido->__set('_tiempoEecc', $row['tiempo_eecc']);
                    $array[] = $objTipPedido;
                endif; 
            endforeach;
            return $array;
        } catch(PDOException $error) {
            return $error;
            exit();
        }
    }
    public function listarTipoxPerifil($conexion, $ids='')
    {
        try {
            $array=array();
            $sql="SELECT idtipo_pedido,tipo_pedido FROM ".$this->table." ";
            if ($ids!='') {
                $sql.=" WHERE idtipo_pedido IN ($ids)";
            }
            $rs = $conexion->query($sql);
            $array = $rs->fetchAll(PDO::FETCH_ASSOC);
            return $array;            
        } catch(PDOException $error) {
            return $error;
            exit();
        }
    }
}
