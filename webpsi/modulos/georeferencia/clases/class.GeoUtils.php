<?php

class GeoUtils {

    public function fileToJsonAddress($file) {
        $data = array();
        $gestor = fopen($file, "r");
        $n = 0;
        if ($gestor) {
            while (($bufer = fgets($gestor, 4096)) !== false) {
                if ($n > 0) {
                    $s = array("á", "é", "í", "ó", "ú", "\r", "\n", "\r\n", "\"");
                    $r = array("a", "e", "i", "o", "u", "", "", "", "");

                    $line = str_replace($s, $r, $bufer);

                    $data[] = utf8_encode($line);
                }
                $n++;
            }
            fclose($gestor);
        }
        unlink($file);
        return $data;
    }

    public function haversineGreatCircleDistance(
        $latitudeFrom, $longitudeFrom, $latitudeTo, 
        $longitudeTo, $earthRadius = 6371000
    ) {
        // convert from degrees to radians
        $latFrom = deg2rad($latitudeFrom);
        $lonFrom = deg2rad($longitudeFrom);
        $latTo = deg2rad($latitudeTo);
        $lonTo = deg2rad($longitudeTo);

        $latDelta = $latTo - $latFrom;
        $lonDelta = $lonTo - $lonFrom;

        $angle = 2 * asin(sqrt(pow(sin($latDelta / 2), 2) +
                                cos($latFrom) * cos($latTo) * pow(sin($lonDelta / 2), 2)));
        return $angle * $earthRadius;
    }

}
