<?php

class GeoDistritopunto
{

    protected $_db = 'webpsi';
    protected $_table = 'geo_distritopunto';

    public function primerFiltro($conexion){
        try {
            $sql = "SELECT departamento nombre, departamento id 
                    FROM " . $this->_db . "." . $this->_table 
                    . " GROUP BY 1 ORDER BY 1";
            $bind = $conexion->prepare($sql);
            $bind->execute();

            $reporte = array();
            while ($data = $bind->fetch(PDO::FETCH_ASSOC)) {
                $reporte[] = $data;
            }
            return $reporte;
        } catch (PDOException $e) {
            exit();
        }
    }

    public function listarProvincia($conexion, $arreglo=array()){
        try {
            $departamento = $arreglo[0];

            $sql = "SELECT provincia campo, provincia id 
                    FROM " . $this->_db . "." . $this->_table 
                    . " WHERE departamento='$departamento'"
                    . " GROUP BY 1 ORDER BY 1";
            $bind = $conexion->prepare($sql);
            $bind->execute();

            $reporte = array();
            while ($data = $bind->fetch(PDO::FETCH_ASSOC)) {
                $reporte[] = $data;
            }
            return $reporte;
        } catch (PDOException $e) {
            exit();
        }   
    }

    public function listarDistritopunto($conexion, $arreglo = array()){
        try {
            $departamento = $arreglo[0];
            $provincia = $arreglo[1];

            $sql = "SELECT distrito campo, distrito id 
                    FROM " . $this->_db . "." . $this->_table 
                    . " WHERE departamento='$departamento'"
                    . " AND provincia='$provincia'"
                    . " GROUP BY 1 ORDER BY 1";
            $bind = $conexion->prepare($sql);
            $bind->execute();

            $reporte = array();
            while ($data = $bind->fetch(PDO::FETCH_ASSOC)) {
                $reporte[] = $data;
            }
            return $reporte;
        } catch (PDOException $e) {
            exit();
        }   
    }

    public function listar($conexion, $data = array())
    {
        try {
            $departamento = $data[0];
            $provincia = $data[1];
            $distrito = $data[2];

            $sql = "SELECT id, distrito, coord_x, coord_y  
                    FROM " . $this->_db . "." . $this->_table . " WHERE ubigeo<>'' ";

            $sql .= " AND departamento='$departamento' 
                      AND provincia='$provincia'
                      AND distrito='$distrito'";
            
            $sql .= "ORDER BY distrito";
            $bind = $conexion->prepare($sql);
            $bind->execute();

            $reporte = array();
            while ($data = $bind->fetch(PDO::FETCH_ASSOC)) {
                $reporte[] = $data;
            }
            return $reporte;
        } catch (PDOException $error) {
            exit();
        }
    }   


}