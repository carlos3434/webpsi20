<?php

class GeoTerminalf
{

    protected $_db = 'webpsi';
    protected $_table = 'geo_terminalf';

    public function primerFiltro($conexion){
        try {
            $sql = "SELECT zonal nombre, zonal id 
                    FROM " . $this->_db . "." . $this->_table 
                    . " GROUP BY 1 ORDER BY 1";
            $bind = $conexion->prepare($sql);
            $bind->execute();

            $reporte = array();
            while ($data = $bind->fetch(PDO::FETCH_ASSOC)) {
                $reporte[] = $data;
            }
            return $reporte;
        } catch (PDOException $e) {
            exit();
        }
    }

    public function listarMdf($conexion, $arreglo=array()){
        try {
            $zonal = $arreglo[0];

            $sql = "SELECT mdf campo, mdf id 
                    FROM " 
                    . $this->_db 
                    . "." 
                    . $this->_table 
                    . " WHERE zonal='$zonal' GROUP BY 1 ORDER BY 1";
            $bind = $conexion->prepare($sql);
            $bind->execute();

            $reporte = array();
            while ($data = $bind->fetch(PDO::FETCH_ASSOC)) {
                $reporte[] = $data;
            }
            return $reporte;
        } catch (PDOException $e) {
            exit();
        }
    }

    public function listarArmario($conexion, $arreglo=array()){
        try {
            $zonal = $arreglo[0];
            $mdf = $arreglo[1];

            $sql = "SELECT armario campo, armario id FROM " 
                    . $this->_db 
                    . "." 
                    . $this->_table 
                    . " WHERE zonal='$zonal' AND mdf='$mdf' GROUP BY 1 ORDER BY 1";
            $bind = $conexion->prepare($sql);
            $bind->execute();

            $reporte = array();
            while ($data = $bind->fetch(PDO::FETCH_ASSOC)) {
                $reporte[] = $data;
            }
            return $reporte;
        } catch (PDOException $e) {
            exit();
        }
    }

    public function listarTerminalf($conexion, $arreglo=array()){
        try {
            $zonal = $arreglo[0];
            $mdf = $arreglo[1];
            $armario = $arreglo[2];

            $sql = "SELECT terminalf campo, terminalf id FROM " 
                    . $this->_db 
                    . "." 
                    . $this->_table 
                    . " WHERE zonal='$zonal' AND mdf='$mdf' and armario='$armario'" 
                    . " GROUP BY 1 ORDER BY 1";
            $bind = $conexion->prepare($sql);
            $bind->execute();

            $reporte = array();
            while ($data = $bind->fetch(PDO::FETCH_ASSOC)) {
                $reporte[] = $data;
            }
            return $reporte;
        } catch (PDOException $e) {
            exit();
        }
    }
    
    public function listar($conexion, $data = array()){
        try {
            $zonal  = $data[0];
            $mdf    = $data[1];
            $armario  = $data[2];
            $terminalf = $data[3];

            $sql = "SELECT terminalf id, terminalf, coord_x, coord_y, direccion FROM " 
                    . $this->_db 
                    . "." 
                    . $this->_table 
                    . " WHERE zonal='$zonal' AND mdf='$mdf'" 
                    . " AND armario='$armario' AND terminalf='$terminalf'";

            $bind = $conexion->prepare($sql);
            $bind->execute();

            $reporte = array();
            while ($data = $bind->fetch(PDO::FETCH_ASSOC)) {
                $reporte[] = $data;
            }
            return $reporte;
        } catch (PDOException $e) {
            exit();
        }
    }

}