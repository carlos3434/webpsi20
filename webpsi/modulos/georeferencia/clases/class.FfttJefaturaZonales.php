<?php
/**
 * Sistema Web Unificada
 * Planificacion y Soluciones Informaticas - Telefonica
 * @author Gonzalo Chacaltana Buleje <gchacaltana@gmd.com.pe>
 * Clase Facilidades Tecnicas - Jefaturas Zonales 
 */
class Data_FfttJefaturaZonales
{
    protected $_zonal = '';
    protected $_jefatura='';
    protected $_jefaturaDesc = '';
    protected $_gerencia = '';
    protected $_idEmpresa;
    

    protected $_table = 'webunificada_fftt.jefaturas_zonales';

    public function __construct()
    {
    }

    public function __get($propiedad)
    {
        $returnValue = (string) '';
        $returnValue = $this->$propiedad;
        return (string) $returnValue;
    }

    public function __set($propiedad, $valor)
    {
        $this->$propiedad = $valor;
    }

    public function getJefaturaGerenciaByZonalEECC($conexion,$idEmpresa,
            $zonal)
    {
        try {
            $strWhere = '';
            
            if ($idEmpresa!='' && $idEmpresa!='-1') {
                $strWhere.=' AND idempresa='.$idEmpresa;
            }
            if ($zonal!='' && $zonal!='-1') {
                $strWhere.=" AND zonal='".$zonal."'";
            }
            
            $sql = "SELECT gerencia,jefatura,jefatura_desc 
                    FROM ".$this->_table." WHERE 1=1 ".$strWhere;
            
            $objJefaturaZonal = new Data_FfttJefaturaZonales();
            foreach ($conexion->query($sql) as $row):
                if ($row['gerencia'] != ''):
                    $objJefaturaZonal->__set('_gerencia', $row['gerencia']);
                    $objJefaturaZonal->__set('_jefatura', $row['jefatura']);
                    $objJefaturaZonal->__set(
                        '_jefaturaDesc', $row['jefatura_desc']
                    );
                endif;
            endforeach;
            return $objJefaturaZonal;
        } catch(PDOException $error) {
            return $error;
            exit();
        }
    }
    
    public function getJefaturaZonalCodJefatura($conexion, $zonal='',
            $jefatura='', $jefaturaDesc='', $idEmpresa='')
    {
        try {
            $strWhere = '';
            
            if ($zonal!='' && $zonal!='-1') {
                $strWhere.=" AND zonal='".$zonal."'";
            }
            if ($jefatura!='' && $jefatura!='-1') {
                $strWhere.=" AND jefatura='".$jefatura."'";
            }
            
            if ($jefaturaDesc!='' && $jefaturaDesc!='-1') {
                $strWhere.=" AND jefatura_desc='".$jefaturaDesc."'";
            }
            
            if ($idEmpresa!='' && $idEmpresa!='-1') {
                $strWhere.=" AND idempresa='".$idEmpresa."'";
            }
            
            $sql = "SELECT zonal,gerencia,jefatura,jefatura_desc,idempresa 
                    FROM ".$this->_table." WHERE 1=1 ".$strWhere;
            
            $array = array();
            foreach ($conexion->query($sql) as $row):
                if ($row['gerencia'] != ''):
                    $objJefaturaZonal = new Data_FfttJefaturaZonales();
                    $objJefaturaZonal->__set('_zonal', $row['zonal']);
                    $objJefaturaZonal->__set('_gerencia', $row['gerencia']);
                    $objJefaturaZonal->__set('_jefatura', $row['jefatura']);
                    $objJefaturaZonal->__set(
                        '_jefaturaDesc', $row['jefatura_desc']
                    );
                    $objJefaturaZonal->__set('_idEmpresa', $row['idempresa']);
                    $array[] = $objJefaturaZonal;
                endif;
            endforeach;
            return $array;
        } catch(PDOException $error) {
            return $error;
            exit();
        }
    }
}