<?php
/**
 * @package     class/data/
 * @name        class.FfttCms.php
 * @category    class
 * @author      Gonzalo Chacaltana <gchacaltana@gmd.com.pe>
 * @copyright   2011 Telefonica del Peru
 * @version     1.0 - 2011/08/09
 */

class Data_FfttCms
{

    protected $_ofAdm = '';
    protected $_plano='';
    protected $_troba= '';
    protected $_nodo= '';
    protected $_troba2='';
    protected $_amp='';
    protected $_tap='';
    protected $_p='';
    protected $_l='';
    protected $_x='';
    protected $_borne='';
    protected $_x2='';
    protected $_perdida='';
    protected $_troba3='';
    protected $_borne2='';
    protected $_codigo='';
    protected $_n1='';
    protected $_n2='';
    protected $_fecha1='';
    protected $_fecha2='';
    protected $_fecha3='';
    protected $_fecha4='';
    protected $_fecha5='';
    protected $_cod2='';
    protected $_direc1='';
    protected $_direc2='';
    protected $_direc3='';
    protected $_direc4='';
    protected $_direc5='';
    protected $_direc6='';
    protected $_direc7='';
    protected $_direc8='';
    protected $_direc9='';
    protected $_direc10='';
    protected $_user='';
    protected $_cejec='';
    protected $_zonaPeligrosa=0;

    public $table = 'webunificada_fftt.fftt_cms';

    public function __construct()
    {
    }

    public function __get($propiedad)
    {
        $returnValue = (string) '';
        $returnValue = $this->$propiedad;
        return (string) $returnValue;
    }

    public function __set($propiedad, $valor)
    {
        $this->$propiedad = $valor;
    }

    public function offSetGet($conexion, $nodo,$troba,$amplificador,$tap)
    {
        try{
            $nodo=trim($nodo);
            $nodo=addslashes($nodo);
            $sql = "SELECT of_adm,plano,troba,nodo,troba2,amp,tab,p,l,x,borne,
                x2,perdida,troba3,borne2,codigo,n1,n2,fecha1,fecha2,fecha3,
                fecha4,fecha5,cod2,direc1,direc2,direc3,direc4,direc5,direc6,
                direc7,direc8,direc9,direc10,user,cejec,zona_peligrosa 
                FROM ".$this->table." 
                WHERE nodo='".$nodo."' AND troba2='".$troba."' 
                AND amp='".$amplificador."' AND tap='".$tab."'";

            foreach ($conexion->query($sql) as $row):
                    $objCms = new data_ffttCMS();
                    $objCms->__set('_ofAdm', $row['of_adm']);
                    $objCms->__set('_plano', $row['plano']);
                    $objCms->__set('_troba', $row['troba']);
                    $objCms->__set('_nodo', $row['nodo']);
                    $objCms->__set('_troba2', $row['troba2']);
                    $objCms->__set('_amp', $row['amp']);
                    $objCms->__set('_tab', $row['tab']);
                    $objCms->__set('_descNodo2', $row['desc_nodo2']);
                    $objCms->__set('_p', $row['p']);
                    $objCms->__set('_l', $row['l']);
                    $objCms->__set('_x', $row['x']);
                    $objCms->__set('_borne', $row['borne']);
                    $objCms->__set('_x2', $row['x2']);
                    $objCms->__set('_perdida', $row['perdida']);
                    $objCms->__set('_troba3', $row['troba3']);
                    $objCms->__set('_borne2', $row['borne2']);
                    $objCms->__set('_codigo', $row['codigo']);
                    $objCms->__set('_n1', $row['n1']);
                    $objCms->__set('_n2', $row['n2']);
                    $objCms->__set('_fecha1', $row['fecha1']);
                    $objCms->__set('_fecha2', $row['fecha2']);
                    $objCms->__set('_fecha3', $row['fecha3']);
                    $objCms->__set('_fecha4', $row['fecha4']);
                    $objCms->__set('_fecha5', $row['fecha5']);
                    $objCms->__set('_cod2', $row['cod2']);
                    $objCms->__set('_direc1', $row['direc1']);
                    $objCms->__set('_direc2', $row['direc2']);
                    $objCms->__set('_direc3', $row['direc3']);
                    $objCms->__set('_direc4', $row['direc4']);
                    $objCms->__set('_direc5', $row['direc5']);
                    $objCms->__set('_direc6', $row['direc6']);
                    $objCms->__set('_direc7', $row['direc7']);
                    $objCms->__set('_direc8', $row['direc8']);
                    $objCms->__set('_direc9', $row['direc9']);
                    $objCms->__set('_direc10', $row['direc10']);
                    $objCms->__set('_user', $row['user']);
                    $objCms->__set('_cejec', $row['cejec']);
                    $objCms->__set('_zonaPeligrosa', $row['zona_peligrosa']);
            endforeach;
            
            return $objCms;
            
        }catch(PDOException $error){
            return $error;
        }
    }

    public function listTotal($conexion)
    {
        try {
            $sql = "SELECT count(1) as 'Total' FROM ".$this->table;
            
            foreach ($conexion->query($sql) as $row):
                $totalRegistros=$row['Total'];
            endforeach;
            return $totalRegistros;
        } catch(PDOException $error) {
            return $error;
            exit();
        }
    }
    
    public function totalTrobasXNodo($conexion, $nodo)
    {
        try {
            (int) $totalTrobas = 0;
            $sql = "SELECT count(troba2) as 'Total' 
                FROM ".$this->table." 
                WHERE nodo='".$nodo."' 
                ORDER BY nodo ASC";
             foreach ($conexion->query($sql) as $row):
                $totalTrobas=$row['Total'];
            endforeach;
            
            return $totalTrobas;

        } catch(PDOException $error) {
            return $error;
            exit();
        }
    }
    
    public function trobasXNodoCatv($conexion,$nodo)
    {
        try {
            
            $sql = " SELECT DISTINCT(troba2) AS 'troba' 
                FROM ".$this->table." 
                WHERE nodo='".$nodo."' 
                ORDER BY troba2 ASC";
            $bind = $conexion->prepare($sql);
            $bind->execute();
            $arrTrobas=array();
            while ($data=$bind->fetch(PDO::FETCH_ASSOC)) {
                $arrTrobas[]=$data;
            }
            
            return $arrTrobas;

        } catch(PDOException $error) {
            return $error;
            exit();
        }
    }
    
    public function amplificadorXNodoCatvTroba($conexion,$nodo,$troba)
    {
        try {
            
            $sql = "SELECT DISTINCT(amp) AS 'amp' 
                FROM ".$this->table." 
                WHERE nodo='".$nodo."' 
                AND troba2='".$troba."' 
                ORDER BY amp ASC";
            $bind = $conexion->prepare($sql);
            $bind->execute();
            $arrAmp=array();
            while ($data=$bind->fetch(PDO::FETCH_ASSOC)) {
                $arrAmp[]=$data;
            }
            
            return $arrAmp;

        } catch(PDOException $error) {
            return $error;
            exit();
        }
    }
    
    public function tapXNodoCatvTrobaAmplificador(
        $conexion,$nodo,$troba,$amp,$datosXY=''
    )
    {
        try {
            
            $strWhere = "";
            if ( $datosXY == 'conXY' ) {
                $strWhere .= " AND x_coords IS NOT NULL 
                               AND y_coords IS NOT NULL ";
            } elseif ( $datosXY == 'sinXY' ) {
                $strWhere .= " AND x_coords IS NULL AND 
                               y_coords IS NULL ";
            }
            
            $sql = "SELECT nodo, troba2 AS troba, amp AS amplificador, tap, 
                    zona_peligrosa, x_coords as x, y_coords as y 
                    FROM ".$this->table." WHERE nodo='".$nodo."' 
                    AND troba2='".$troba."' AND amp='".$amp."' 
                    " . $strWhere . " 
                    ORDER BY tap";
            $bind = $conexion->prepare($sql);
            $bind->execute();
            $arrTap=array();
            while ($data=$bind->fetch(PDO::FETCH_ASSOC)) {
                $arrTap[]=$data;
            }
            
            return $arrTap;

        } catch(PDOException $error) {
            return $error;
            exit();
        }
    }
    
    //listar trobas
    public function trobasXNodo($conexion,$nodo, $datosXY='')
    {
        try {
        
            $strWhere = "";
            if ( $datosXY == 'conXY' ) {
                $strWhere .= " AND x IS NOT NULL AND y IS NOT NULL ";
            } elseif ( $datosXY == 'sinXY' ) {
                $strWhere .= " AND x IS NULL AND y IS NULL ";
            }
        
            $sql = "SELECT nodo, troba, x, y FROM webunificada_fftt.fftt_trobas 
                    WHERE nodo='".$nodo."' " . $strWhere . " ORDER BY troba";
            $bind = $conexion->prepare($sql);
            $bind->execute();
            $arrTap=array();
            while ($data=$bind->fetch(PDO::FETCH_ASSOC)) {
                $arrTap[]=$data;
            }
            
            return $arrTap;

        } catch(PDOException $error) {
            return $error;
            exit();
        }
    }
    
    public function getTrobasByXy($conexion, $x, $y, $distancia=10, 
        $nroTerminales=10)
    {
        try {
            
            $sql = "SELECT nodo, troba, x, y, 
                    ( 6371 * ACOS( COS( RADIANS(" . $y. ") ) 
                    * COS( RADIANS( y ) ) * 
                    COS( RADIANS( x ) - RADIANS(" . $x. ") ) 
                    + SIN( RADIANS(" . $y. ") ) * 
                    SIN( RADIANS( y ) ) ) ) AS distance 
                    FROM webunificada_fftt.fftt_trobas     
                    HAVING distance < " . $distancia . " 
                    ORDER BY distance LIMIT 0," . $nroTerminales . "";
            $array = array();
            foreach ($conexion->query($sql) as $row):
                $array[] = $row;
            endforeach;
            return $array;
        } catch(PDOException $error) {
            return $error;
            exit();
        }
    }
    
    public function getTapsByXy($conexion, $x, $y, $distancia=10, 
            $nroTerminales=10)
    {
        try {
            
            $sql = "SELECT nodo, troba2 as troba, amp as amplificador, tap, 
                    x_coords as x, y_coords as y, 
                    ( 6371 * ACOS( COS( RADIANS(" . $y. ") ) 
                    * COS( RADIANS( y_coords ) ) * 
                    COS( RADIANS( x_coords ) - RADIANS(" . $x. ") )
                    + SIN( RADIANS(" . $y. ") ) * 
                    SIN( RADIANS( y_coords ) ) ) ) AS distance 
                    FROM ".$this->table."    
                    HAVING distance < " . $distancia . " 
                    ORDER BY distance LIMIT 0," . $nroTerminales . "";
            $array = array();
            foreach ($conexion->query($sql) as $row):
                $array[] = $row;
            endforeach;
            return $array;
        } catch(PDOException $error) {
            return $error;
            exit();
        }
    }
    
    public function updateZonaPeligrosaCatv($conexion, $nodo, $troba, $amp, 
            $tap, $stateZona)
    {
        try {
            
            $sql = "UPDATE  ".$this->table." SET 
                    zona_peligrosa='".$stateZona."' 
                    WHERE nodo='".$nodo."' 
                    AND troba2='".$troba."' 
                    AND amp='".$amp."' 
                    AND tap='".$tap."'";
            $bind = $conexion->prepare($sql);
            $bind->execute();

        } catch(PDOException $error) {
            return $error;
            exit();
        }
    }
    
    public function updateXyTap($conexion, $nodo, $troba, $amp, $tap, $x, $y)
    {
        try {
            
            $sql = "UPDATE  ".$this->table." SET 
                x_coords = '".$x."', 
                y_coords = '".$y."' 
                WHERE nodo='".$nodo."'  
                AND troba2='".$troba."'  
                AND amp='".$amp."' 
                AND tap='".$tap."'";
            $bind = $conexion->prepare($sql);
            $bind->execute();

            return 1;
        } catch(PDOException $error) {
            return 0;
        }
    }
    
    public function updateXyTroba($conexion, $nodo, $troba, $x, $y)
    {
        try {
            
            $sql = "UPDATE  webunificada_fftt.fftt_trobas SET 
                x = '".$x."', 
                y = '".$y."' 
                WHERE nodo='".$nodo."'  
                AND troba='".$troba."'";
            $bind = $conexion->prepare($sql);
            $bind->execute();

            return 1;
        } catch(PDOException $error) {
            return 0;
        }
    }
    
}