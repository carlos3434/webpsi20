
<?php

/**
 * @package     class/data/
 * @name        class.ffttTerminales_smc.php
 * @category    Model
 * @author      Sergio Miranda C.
 * @copyright   2013 Telefonica del Peru
 * @version     1.0 - 2013/10/14
 */

//include_once "class.FfttTerminales.php";

class Data_FfttTerminales_Smc extends Data_FfttTerminales
{

    /* Funcion agregada por Sergio Miranda */
    /* 2013-10-14 */
    public function getTerminalesByZonalMdfTipoRedArmCable($conexion, 
            $zonal, $mdf, $tipoRed, $armario_cable)
    {
        try {
            $campo = $wstrnig = '';

            if (isset($zonal) && $zonal <> '') {
                //$wstrnig = " AND zonal = '" . $zonal . "' ";
            }

            if ($tipoRed == "F")
				$filtro_armario_cable = " AND armario = '".$armario_cable."'  ";
			else
				$filtro_armario_cable = " AND cable = '".$armario_cable."'  ";


            $sql = "SELECT caja FROM webunificada_fftt.fftt_terminales 
                    WHERE 1 = 1 AND mdf = '" . $mdf . "' ".$filtro_armario_cable  ;  

            $array = array();

            foreach ($conexion->query($sql) as $row):
                $array[] = $row;
            endforeach;
            //var_dump($array);
            return $array;
        } catch (PDOException $error) {
            return $error;
            exit();
        }
    }


public function getClientesPorTerminal($conexion, $zonal, $mdf, $cable, 
            $armario, $caja='', $tipoRed='' )
    {
        try {

            $strWhere .= "  ";

            if ($caja != '') {
                $strWhere .= " AND cli.Caja = '" . $caja . "' ";
            }

            $order = " 1 ";

            $sql = "SELECT cli.cliente, cli.direccion, cli.zonalgestel, cli.mdf, cli.inscripcion, 
            			   cli.`cable-armario` as armario_cable, cli.caja, cli.tipo  ";
            $sql .= " FROM webunificada_fftt.clientes_xy_terminal cli 
                    WHERE cli.zonalgestel = '" . $zonal . "' AND cli.mdf = '" . $mdf . "' AND cli.`cable-armario` = '" . $cable . "' 
                    AND cli.caja = '".$caja."' ORDER BY " . $order;

            $array = array();
            foreach ($conexion->query($sql) as $row):
                $array[] = $row;
            endforeach;
            return $array;
        } catch (PDOException $error) {
            return $error;
            exit();
        }
    }



 }
