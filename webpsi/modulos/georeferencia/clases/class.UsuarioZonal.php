<?php
/**
 *
 * [Web Unificada] :: Sistema Web Unificada
 *
 * PHP version 5
 *
 * Copyright (c) 2011 Planificacion y Sistemas Informaticos
 *
 * @author        Gonzalo Chacaltana Buleje <gchacaltana@gmd.com.pe>
 * @copyright     2011 � Planificacion y Soluciones Informaticas
 * [Descripcion] :: Clase UsuarioCapa.
 * [Fecha de Actualizacion] :: 21-10-2011
 */
 
 
class Data_UsuarioZonal
{
    protected $_idUsuarioZonal = 0;
    protected $_idUsuario='';
    protected $_idCapa = '';
    protected $_flag = 1;


    public function __construct()
    {
    }

    public function __get($propiedad)
    {
        $returnValue = (string) '';
        $returnValue = $this->$propiedad;
        return (string) $returnValue;
    }

    public function __set($propiedad, $valor)
    {
        $this->$propiedad = $valor;
    }
    
    /*public function save($conexion, $idUsuario, $idCapa,$flag)
    {
        try {
            $sql = "INSERT INTO ".$this->table." (idusuario,idcapa,flag) 
            VALUES (:idusuario,:idcapa,:flag)";
            $bind = $conexion->prepare($sql);
            $bind->bindParam(":idusuario", $idUsuario);
            $bind->bindParam(":idcapa", $idCapa);
            $bind->bindParam(":flag", $flag);
            $bind->execute();
        } catch(PDOException $error) {
            return $error;
            exit();
        }
    }*/

    public function listar($conexion,$idUsuario='')
    {
        try {
            $where='';
            if($idUsuario!='' && $idUsuario!='-1')
                $where.=' AND u.idusuario='.$idUsuario;
            $array = array();  
            $sql = "SELECT u.idusuario,u.zonal,u.status
            FROM ".$this->table." WHERE 1=1 ".$where." ORDER BY c.nombre ASC";
            
            foreach ($conexion->query($sql) as $row):
                if($row['idusuario_capa'] != ''):
                    $objUsuarioCapa = new Data_UsuarioCapa();
                    $objUsuarioCapa->__set('_idUsuarioCapa', $row['idusuario_capa']);
                    $objUsuarioCapa->__set('_idUsuario', $row['idusuario']);
                    $objUsuarioCapa->__set('_idCapa', $row['idcapa']);
                    $objUsuarioCapa->__set('_flag', $row['flag']);
                    $array[] = $objUsuarioCapa;
                endif;
            endforeach;
            return $array;
        } catch(PDOException $error) {
            return $error;
            exit();
        }
    }
    
    public function deleteXidusuario($conexion, $idUsuario)
    {
        try {
            $sql = "DELETE FROM ".$this->table." WHERE idusuario=:idusuario";
            $bind = $conexion->prepare($sql);
            $bind->bindParam(":idusuario", $idUsuario);
            $bind->execute();
        } catch(PDOException $error) {
            return $error;
            exit();
        }
    }
}