<?php

class GeoTerminald
{

    protected $_db = 'webpsi';
    protected $_table = 'geo_terminald';

    public function primerFiltro($conexion){
        try {
            $sql = "SELECT zonal nombre, zonal id 
                    FROM " . $this->_db . "." . $this->_table 
                    . " GROUP BY 1 ORDER BY 1";
            $bind = $conexion->prepare($sql);
            $bind->execute();

            $reporte = array();
            while ($data = $bind->fetch(PDO::FETCH_ASSOC)) {
                $reporte[] = $data;
            }
            return $reporte;
        } catch (PDOException $e) {
            exit();
        }
    }

    public function listarMdf($conexion, $arreglo=array()){
        try {
            $zonal = $arreglo[0];

            $sql = "SELECT mdf campo, mdf id 
                    FROM " 
                    . $this->_db 
                    . "." 
                    . $this->_table 
                    . " WHERE zonal='$zonal' GROUP BY 1 ORDER BY 1";
            $bind = $conexion->prepare($sql);
            $bind->execute();

            $reporte = array();
            while ($data = $bind->fetch(PDO::FETCH_ASSOC)) {
                $reporte[] = $data;
            }
            return $reporte;
        } catch (PDOException $e) {
            exit();
        }
    }

    public function listarCable($conexion, $arreglo=array()){
        try {
            $zonal = $arreglo[0];
            $mdf = $arreglo[1];

            $sql = "SELECT cable campo, cable id FROM " 
                    . $this->_db 
                    . "." 
                    . $this->_table 
                    . " WHERE zonal='$zonal' AND mdf='$mdf' GROUP BY 1 ORDER BY 1";
            $bind = $conexion->prepare($sql);
            $bind->execute();

            $reporte = array();
            while ($data = $bind->fetch(PDO::FETCH_ASSOC)) {
                $reporte[] = $data;
            }
            return $reporte;
        } catch (PDOException $e) {
            exit();
        }
    }

    public function listarTerminald($conexion, $arreglo=array()){
        try {
            $zonal = $arreglo[0];
            $mdf = $arreglo[1];
            $cable = $arreglo[2];

            $sql = "SELECT terminald campo, terminald id FROM " 
                    . $this->_db 
                    . "." 
                    . $this->_table 
                    . " WHERE zonal='$zonal' AND mdf='$mdf' and cable='$cable'" 
                    . " GROUP BY 1 ORDER BY 1";
            $bind = $conexion->prepare($sql);
            $bind->execute();

            $reporte = array();
            while ($data = $bind->fetch(PDO::FETCH_ASSOC)) {
                $reporte[] = $data;
            }
            return $reporte;
        } catch (PDOException $e) {
            exit();
        }
    }
    
    public function listar($conexion, $data = array()){
        try {
            $zonal  = $data[0];
            $mdf    = $data[1];
            $cable  = $data[2];
            $terminald = $data[3];

            $sql = "SELECT terminald id, terminald, coord_x, coord_y, direccion FROM " 
                    . $this->_db 
                    . "." 
                    . $this->_table 
                    . " WHERE zonal='$zonal' AND mdf='$mdf'" 
                    . " AND cable='$cable' AND terminald='$terminald'";

            $bind = $conexion->prepare($sql);
            $bind->execute();

            $reporte = array();
            while ($data = $bind->fetch(PDO::FETCH_ASSOC)) {
                $reporte[] = $data;
            }
            return $reporte;
        } catch (PDOException $e) {
            exit();
        }
    }

}