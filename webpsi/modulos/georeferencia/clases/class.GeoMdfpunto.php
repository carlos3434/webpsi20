<?php

class GeoMdfpunto
{

    protected $_db = 'webpsi';
    protected $_table = 'geo_mdfpunto';

    public function listar($conexion, $arreglo = array())
    {
        try {
            $zonal = $arreglo[0];
            $mdf = $arreglo[1];

            $sql = "SELECT zonal, mdf, coord_x, coord_y, direccion, orden  
                    FROM " . $this->_db . "." . $this->_table . " WHERE mdf<>'' ";

            $sql .= " AND zonal='$zonal' 
                      AND mdf='$mdf'";
            
            $sql .= " ORDER BY mdf, orden";
            $bind = $conexion->prepare($sql);
            $bind->execute();

            $reporte = array();
            while ($data = $bind->fetch(PDO::FETCH_ASSOC)) {
                $reporte[] = $data;
            }
            return $reporte;
        } catch (PDOException $error) {
            exit();
        }
    }

    public function primerFiltro($conexion){
        try {
            $sql = "SELECT zonal nombre, zonal id 
                    FROM " . $this->_db . "." . $this->_table 
                    . " GROUP BY 1 ORDER BY 1";
            $bind = $conexion->prepare($sql);
            $bind->execute();

            $reporte = array();
            while ($data = $bind->fetch(PDO::FETCH_ASSOC)) {
                $reporte[] = $data;
            }
            return $reporte;
        } catch (PDOException $e) {
            exit();
        }
    }

    public function listarMdfpunto($conexion, $arreglo=array()){
        try {
            $zonal = $arreglo[0];

            $sql = "SELECT mdf campo, mdf id 
                    FROM " . $this->_db . "." . $this->_table 
                    . " WHERE zonal='$zonal'"
                    . " GROUP BY 1 ORDER BY 1";
            $bind = $conexion->prepare($sql);
            $bind->execute();

            $reporte = array();
            while ($data = $bind->fetch(PDO::FETCH_ASSOC)) {
                $reporte[] = $data;
            }
            return $reporte;
        } catch (PDOException $e) {
            exit();
        }   
    }

}