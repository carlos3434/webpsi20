<?php

class GeoAmplificador
{

    protected $_db = 'webpsi';
    protected $_table = 'geo_amplificador';

    public function primerFiltro($conexion){
        try {
            $sql = "SELECT zonal nombre, zonal id 
                    FROM " . $this->_db . "." . $this->_table 
                    . " GROUP BY 1 ORDER BY 1";
            $bind = $conexion->prepare($sql);
            $bind->execute();

            $reporte = $bind->fetchAll(PDO::FETCH_ASSOC);            
            return $reporte;
        } catch (PDOException $e) {
            exit();
        }
    }

    public function listarNodo($conexion, $arreglo=array()){
        try {
            $zonal = $arreglo[0];

            $sql = "SELECT nodo campo, nodo id 
                    FROM " . $this->_db . "." . $this->_table 
                    . " WHERE zonal='$zonal'"
                    . " GROUP BY 1 ORDER BY 1";
            $bind = $conexion->prepare($sql);
            $bind->execute();

            $reporte = $bind->fetchAll(PDO::FETCH_ASSOC);
            return $reporte;
        } catch (PDOException $e) {
            exit();
        }   
    }

    public function listarTroba($conexion, $arreglo = array()){
        try {
            $zonal = $arreglo[0];
            $nodo = $arreglo[1];

            $sql = "SELECT troba campo, troba id 
                    FROM " . $this->_db . "." . $this->_table 
                    . " WHERE zonal='$zonal'"
                    . " AND nodo='$nodo'"
                    . " GROUP BY 1 ORDER BY 1";
            $bind = $conexion->prepare($sql);
            $bind->execute();

            $reporte = $bind->fetchAll(PDO::FETCH_ASSOC);
            return $reporte;
        } catch (PDOException $e) {
            exit();
        }   
    }

    public function listarAmplificador($conexion, $arreglo = array()){
        try {
            $zonal = $arreglo[0];
            $nodo = $arreglo[1];
            $troba = $arreglo[2];

            $sql = "SELECT amplificador campo, amplificador id 
                    FROM " . $this->_db . "." . $this->_table 
                    . " WHERE zonal='$zonal'"
                    . " AND nodo='$nodo'"
                    . " AND troba='$troba'"
                    . " GROUP BY 1 ORDER BY 1";
            $bind = $conexion->prepare($sql);
            $bind->execute();

            $reporte = $bind->fetchAll(PDO::FETCH_ASSOC);
            return $reporte;
        } catch (PDOException $e) {
            exit();
        }   
    }

    public function listar($conexion, $data = array())
    {
        try {
            $zonal = $data[0];
            $nodo = $data[1];
            $troba = $data[2];
            $amplificador = $data[3];

            $sql = "SELECT id, amplificador, coord_x, coord_y, orden  
                    FROM " . $this->_db . "." . $this->_table . " WHERE id<>'' ";

            $sql .= " AND zonal='$zonal' 
                      AND nodo='$nodo'
                      AND troba='$troba'
                      AND amplificador='$amplificador'";
            
            $sql .= "ORDER BY amplificador, orden";
            $bind = $conexion->prepare($sql);
            $bind->execute();

            $reporte = array();
            while ($data = $bind->fetch(PDO::FETCH_ASSOC)) {
                $reporte[] = $data;
            }
            return $reporte;
        } catch (PDOException $error) {
            exit();
        }
    }   


}