<?php
/**
 *
 * [WebPSI] :: Sistema Web PSI
 *
 * PHP version 5
 *
 * Copyright (c) 2011 Planificacion y Sistemas Informaticos
 *
 * @author              Cristian Villalta
 * @copyright           2014 © Planificacion y Soluciones Informaticas
 * @package             class/
 * @name                class.GeoTrama.php
 *
 * [Descripcion] :: Clase GeoTrama
 * [Fecha de Actualizacion] :: 06-08-2014
 */

class GeoTrama {

    protected $_db = 'webpsi';
    protected $_table = 'geo_tramos';
    private $_data = array();

    /**
     * 
     * @param type $dbh Conexion
     * @param type $distrito Cadena DEP(2) + PRO(2) + DIS(2)
     * @param type $calle Nombre de la via buscada 
     * @param type $numero Numero consultado
     * @param type $izq Lado izquierdo (true) Lado derecho (false)
     * @return type
     */
    public function buscarDireccion($dbh, $distrito, $calle, $numero, $izq) {
        $result = array();

        $dep = substr($distrito, 0, 2);
        $pro = substr($distrito, 2, 2);
        $dis = substr($distrito, 4, 2);

        try {
            //Iniciar transaccion
            $dbh->beginTransaction();

            $sql = "SELECT
                        COD_VIA, COD_TRAM, PKY_TRAMO, 
                        NUM_CUADRA, NOM_VIA_TR, 
                        COD_DPTO, COD_PROV, COD_DIST, 
                        NUM_IZQ_IN, NUM_IZQ_FI, NUM_DER_IN, NUM_DER_FI,
                        XA, YA, XB, YB, XC, YC, XD, YD
                    FROM 
                        $this->_db.geo_tramos 
                    WHERE 
                        NOM_VIA_TR LIKE '%$calle%'";

            //Limitar por distrito
            /*
            if ( $distrito != "" ) 
            {
                $sql .= " AND COD_DPTO='$dep'";
                $sql .= " AND COD_PROV='$pro'";
                $sql .= " AND COD_DIST='$dis'";
            }
            */
            if ( $dep != "00" ) 
            {
                $sql .= " AND COD_DPTO='$dep'";
            }
            if ( $pro != "00" ) 
            {
                $sql .= " AND COD_PROV='$pro'";
            }
            if ( $dis != "00" ) 
            {
                $sql .= " AND COD_DIST='$dis'";
            }

            //Numeracion izquierda o derecha
            if ( $numero != "" ) 
            {
                if ($izq) 
                {
                    $sql .= " AND $numero BETWEEN NUM_IZQ_IN AND NUM_IZQ_FI";
                } else {
                    $sql .= " AND $numero BETWEEN NUM_DER_IN AND NUM_DER_FI";
                }
            }

            //Orden por coordenadas
            $sql .= " ORDER BY XA";


            //Datos para la busqueda
            $bind = $dbh->prepare($sql);
            $bind->execute();
            
            $reporte = array();
            while ($data = $bind->fetch(PDO::FETCH_ASSOC)) {
                foreach ($data as $key=>$val) {
                    $data[$key] = utf8_encode($val);
                    $data["SEARCH"] = $calle . " " .  $numero;
                }
                $xDir = $data["XB"] + ( (- $data["NUM_IZQ_FI"] + $numero) * (- $data["XB"] + $data["XA"]) / (- $data["NUM_IZQ_FI"] + $data["NUM_DER_IN"]) );
                $yDir = $data["YB"] + ( (- $data["NUM_IZQ_FI"] + $numero) * (- $data["YB"] + $data["YA"]) / (- $data["NUM_IZQ_FI"] + $data["NUM_DER_IN"]) );
                $data["XY"] = $yDir . "," . $xDir;
                $reporte[] = $data;
            }

            $dbh->commit();
            return $reporte;
        } catch (PDOException $error) {
            $dbh->rollback();
            $result["estado"] = false;
            $result["msg"] = $error->getMessage();
            return $result;
        }
    }
    
    
    public function massiveAddress($dbh, $id, $distrito){
        $result = array();

        $dep = substr($distrito, 0, 2);
        $pro = substr($distrito, 2, 2);
        $dis = substr($distrito, 4, 2);
        
        try {
            //Iniciar transaccion
            $dbh->beginTransaction();

            $sql = "SELECT 
                            a.COD_VIA, a.COD_TRAM, a.PKY_TRAMO, 
                            a.NUM_CUADRA, a.NOM_VIA_TR, 
                            a.COD_DPTO, a.COD_PROV, a.COD_DIST, 
                            a.NUM_IZQ_IN, a.NUM_IZQ_FI, a.NUM_DER_IN, a.NUM_DER_FI,
                            a.XA, a.YA, a.XB, a.YB, a.XC, a.YC, a.XD, a.YD,
                            b.nombre, b.numero
                    FROM
                            webpsi.geo_tramos a, webpsi.geo_tramos_address b
                    WHERE
                            b.block_id='$id'
                            AND a.NOM_VIA_TR LIKE CONCAT('%', b.calle, '%') 
                            AND b.numero BETWEEN a.NUM_DER_IN AND a.NUM_IZQ_FI";
            
            if ( $dep != "00" ) 
            {
                $sql .= " AND a.COD_DPTO='$dep'";
            }
            if ( $pro != "00" ) 
            {
                $sql .= " AND a.COD_PROV='$pro'";
            }
            if ( $dis != "00" ) 
            {
                $sql .= " AND a.COD_DIST='$dis'";
            }

            //Datos para la busqueda
            $bind = $dbh->prepare($sql);
            $bind->execute();
            
            $reporte = array();
            while ($data = $bind->fetch(PDO::FETCH_ASSOC)) {
                foreach ($data as $key=>$val) {
                    $data[$key] = utf8_encode($val);
                }
                $reporte[] = $data;
            }

            $dbh->commit();
            return $reporte;
        } catch (PDOException $error) {
            $dbh->rollback();
            $result["estado"] = false;
            $result["msg"] = $error->getMessage();
            return $result;
        }
        
    }


}
