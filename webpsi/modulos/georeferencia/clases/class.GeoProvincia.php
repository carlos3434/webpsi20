<?php

class GeoProvincia
{

    protected $_db = 'webpsi';
    protected $_table = 'geo_provincia';

    public function listar($conexion, $data = array())
    {
        try {
            $departamento = $data[0];
            $provincia = $data[1];

            $sql = "SELECT provincia, coord_x, coord_y, orden  
                    FROM " . $this->_db . "." . $this->_table . " WHERE ubigeo<>'' ";

            $sql .= " AND departamento='$departamento' 
                      AND provincia='$provincia'";
            
            $sql .= "ORDER BY ubigeo, orden";
            $bind = $conexion->prepare($sql);
            $bind->execute();

            $reporte = array();
            while ($data = $bind->fetch(PDO::FETCH_ASSOC)) {
                $reporte[] = $data;
            }
            return $reporte;
        } catch (PDOException $error) {
            exit();
        }
    }

    public function primerFiltro($conexion){
        try {
            $sql = "SELECT departamento nombre, departamento id 
                    FROM " . $this->_db . "." . $this->_table 
                    . " GROUP BY 1 ORDER BY 1";
            $bind = $conexion->prepare($sql);
            $bind->execute();

            $reporte = array();
            while ($data = $bind->fetch(PDO::FETCH_ASSOC)) {
                $reporte[] = $data;
            }
            return $reporte;
        } catch (PDOException $e) {
            exit();
        }
    }

    public function listarProvincia($conexion, $arreglo=array()){
        try {
            $departamento = $arreglo[0];

            $sql = "SELECT provincia campo, provincia id 
                    FROM " . $this->_db . "." . $this->_table 
                    . " WHERE departamento='$departamento'"
                    . " GROUP BY 1 ORDER BY 1";
            $bind = $conexion->prepare($sql);
            $bind->execute();

            $reporte = array();
            while ($data = $bind->fetch(PDO::FETCH_ASSOC)) {
                $reporte[] = $data;
            }
            return $reporte;
        } catch (PDOException $e) {
            exit();
        }   
    }


}