<?php

/**
 * @package     class/data/
 * @name        class.GestionObraEdi.php
 * @category    Model
 * @author      Wilder Sandoval Huamani <wsandoval@gmd.com.pe>
 * @copyright   2012 Telefonica del Peru
 * @version     1.0 - 2012/04/16
 */

class Data_GestionObraEdi
{
    /**
     * Nombre de la tabla
     * @access protected
     * @var string
     */
    protected $_table = 'webunificada_fftt.edi_gestion_obra';
    
    /**
     * Id de la gestion de obra
     * @access protected
     * @var int
     */
    protected $_idGestionObra = '';
    
    /**
     * Nombre de la gestion de obra
     * @access protected
     * @var string
     */
    protected $_desGestionObra = '';
    
    /**
     * Indicador del estado (0: Inactivo - 1: Activo)
     * @access protected
     * @var int
     */
    protected $_flag = '';

    /**
     * Metodo constructor
     * @name __construct
     */
    public function __construct()
    {
        $this->_idGestionObra = 0;
        $this->_desGestionObra = '';
        $this->_flag = 1;
    }
    
    /**
     * @author Wilder Sandoval Huamani <wsandoval@gmd.com.pe>
     * @name __get
     * Obtener el valor de un atributo
     * @param string $propiedad Nombre del atributo a obtener
     * @return string $returnValue
     */
    public function __get($propiedad)
    {
        $returnValue = (string) '';
        $returnValue = $this->$propiedad;
        return (string) $returnValue;
    }

    /**
     * @author Wilder Sandoval Huamani <wsandoval@gmd.com.pe>
     * @name __set
     * Asignar un valor a un atributo
     * @param string $propiedad Nombre del atributo a setear
     * @param string $nombre Valor a asignar
     */
    public function __set($propiedad, $nombre)
    {
        $this->$propiedad = $nombre;
    }

    /**
     * @author Fernando Esteban Valerio <festeban@gmd.com.pe>
     * @name getIdGestObra
     * Obtener el valor del atributo '_idGestionObra'.
     * @return int
     */
    public function getIdGestObra()
    {
        return $this->_idGestionObra;
    }
    
    /**
     * @author Fernando Esteban Valerio <festeban@gmd.com.pe>
     * @name setIdGestObra
     * Asignar un valor al atributo '_idGestionObra'.
     * @param int $id Id de la gestion de obra.
     */
    public function setIdGestObra($id)
    {
        $this->_idGestionObra = $id;
    }
    
    /**
     * @author Fernando Esteban Valerio <festeban@gmd.com.pe>
     * @name getDesGestObra
     * Obtener el valor del atributo '_desGestionObra'.
     * @return string
     */
    public function getDesGestObra()
    {
        return $this->_desGestionObra;
    }
    
    /**
     * @author Fernando Esteban Valerio <festeban@gmd.com.pe>
     * @name setDesGestObra
     * Asignar un valor al atributo '_desGestionObra'.
     * @param string $nombre Nombre de la gestion de obra.
     */
    public function setDesGestObra($nombre)
    {
        $this->_desGestionObra = $nombre;
    }
    
    /**
     * @author Fernando Esteban Valerio <festeban@gmd.com.pe>
     * @name getFlag
     * Obtener el valor del atributo '_flag'.
     * @return int
     */
    public function getFlag()
    {
        return $this->_flag;
    }
    
    /**
     * @author Fernando Esteban Valerio <festeban@gmd.com.pe>
     * @name setFlag
     * Asignar un valor al atributo '_flag'.
     * @param string $flag Estado de la gestion de obra.
     */
    public function setFlag($flag)
    {
        $this->_flag = $flag;
    }

    /**
     * @author Fernando Esteban Valerio <festeban@gmd.com.pe>
     * @name registrar
     * Registrar los datos de la gestion de obra ingresada.
     * @global conexion $conexion Objeto de conexion a la BD creada con PDO.
     * @param array $arrParametros Arreglo que contiene los datos de la gestion
     * de obra.
     * @return boolean Resultado del registro/actualizacion (Con exito: true,
     * Sin exito: false).
     */
    public function registrar($arrParametros)
    {
        global $conexion;

        $sql = '';
        try {
            $id = $arrParametros['id'];
            $nombre = $arrParametros['desc'];
            $flag = $arrParametros['flag'];
            if ($id) {
                $sql = "UPDATE " . $this->_table. " SET "
                     . "des_gestion_obra = :nombre, flag = :flag "
                     . "WHERE idgestion_obra = :id";
            } else {
                $sql = "INSERT INTO " . $this->_table
                     . "(idgestion_obra,des_gestion_obra,flag) "
                     . "VALUES (:id,:nombre,:flag)";
            }
            $bind = $conexion->prepare($sql);
            $bind->bindParam(":id", $id, PDO::PARAM_INT);
            $bind->bindParam(":nombre", $nombre, PDO::PARAM_STR);
            $bind->bindParam(":flag", $flag, PDO::PARAM_INT);
            $bind->execute();
            return true;
        } catch (PDOException $error) {
            $error = "Ocurrio un error : " . $error->getMessage();
            return false;
        }
    }

    /**
     * @author Fernando Esteban Valerio <festeban@gmd.com.pe>
     * @name obtener
     * Obtener los datos de la gestion de obra que concuerde con el criterio
     * de busqueda ingresado: 
     * - Id : Id de la gestion de obra.
     * - Nombre : Nombre de la gestion de obra (Valor opcional).
     * @global conexion $conexion Objeto de conexion a la BD creada con PDO.
     * @param int $id Id de la gestion de obra.
     * @param string $nombre Nombre de la gestion de obra.
     * @return object Objeto de tipo Data_GestionObraEdi.
     */
    public function obtener($id, $nombre='')
    {
        global $conexion;

        $objeto = new Data_GestionObraEdi();
        try {
            $sql = "SELECT * FROM " . $this->_table . " WHERE 1 = 1 ";
            if ($id == 0) {
                if ($nombre != '') {
                    $sql .= "AND UPPER(des_gestion_obra) = :nombre";
                }
            } else {
                $sql .= "AND idgestion_obra = :id";
            }
            $bind = $conexion->prepare($sql);
            if ($nombre != '') {
                $bind->bindParam(":nombre", $nombre, PDO::PARAM_STR);
            } else {
                $bind->bindParam(":id", $id, PDO::PARAM_INT);
            }
            $bind->execute();
            $result = $bind->fetchAll(PDO::FETCH_ASSOC);
            if (is_array($result) && count($result)) {
                foreach ($result as $row) {
                    $objeto->setIdGestObra($row['idgestion_obra']);
                    $objeto->setDesGestObra($row['des_gestion_obra']);
                    $objeto->setFlag($row['flag']);
                }
            }
        } catch (PDOException $error) {
            $error = "Ocurrio un error : " . $error->getMessage();
        }
        return $objeto;
    }
    
    /**
     * @author Fernando Esteban Valerio <festeban@gmd.com.pe>
     * @name contar
     * Obtener el total de gestiones de obra que concuerde con el criterio de 
     * busqueda seleccionado:
     * - Nombre : Nombre de la gestion de obra (Valor opcional).
     * @global conexion $conexion Objeto de conexion a la BD creada con PDO.
     * @param string $nombre Nombre de la gestion de obra.
     * @return int Numero total de registros que concuerde con el criterio de 
     * busqueda.
     */
    public function contar($nombre='')
    {
        global $conexion;

        $totalRegistros = 0;
        try {
            $sql = "SELECT COUNT(1) AS 'Total' FROM " . $this->_table;
            if ($nombre != '') {
                $sql .= " WHERE des_gestion_obra LIKE :nombre";
            }
            $bind = $conexion->prepare($sql);
            if ($nombre != '') {
                $variable = '%' . $nombre . '%';
                $bind->bindParam(":nombre", $variable, PDO::PARAM_STR);
            }
            $bind->execute();
            $result = $bind->fetchAll(PDO::FETCH_ASSOC);
            if (is_array($result) && count($result)) {
                foreach ($result as $row) {
                    $totalRegistros = $row['Total'];
                }
            }
        } catch (PDOException $error) {
            $error = "Ocurrio un error : " . $error->getMessage();
            $totalRegistros = -1;
        }
        return $totalRegistros;
    }

    /**
     * @author Fernando Esteban Valerio <festeban@gmd.com.pe>
     * @name buscar
     * Obtener el total de gestiones de obra que concuerde con el criterio de 
     * busqueda seleccionado: 
     * - Id : Id de la gestion de obra.
     * - Nombre: Nombre de la gestion de obra (Valor opcional).
     * @global conexion $conexion Objeto de conexion a la BD creada con PDO
     * @param int $id Id de la gestion de obra.
     * @param string $nombre Nombre de la gestion de obra.
     * @return int Numero total de registros que concuerde con el criterio de 
     * busqueda.
     */
    public function buscar($id, $nombre='')
    {
        global $conexion;

        $numElementos = 0;
        try {
            $sql = "SELECT COUNT(*) AS total FROM " . $this->_table . " WHERE "
                 . "UPPER(des_gestion_obra) = '" . $nombre
                 . "' AND idgestion_obra <> " . $id;
            foreach ($conexion->query($sql) as $row) {
                $numElementos = $row['total'];
            }
        } catch (PDOException $error) {
            $error = "Ocurrio un error : " . $error->getMessage();
            $numElementos = -1;
        }
        return $numElementos;
    }
    
    /**
     * @author Fernando Esteban Valerio <festeban@gmd.com.pe>
     * @name filtrar
     * Obtener un arreglo de objetos de tipo de Data_GestionObraEdi que 
     * concuerde con el criterio de busqueda seleccionado: 
     * - Nombre : Nombre de la gestion de obra (Valor opcional).
     * @global conexion $conexion Objeto de conexion a la BD creada con PDO
     * @param int $pagina Numero de pagina.
     * @param string $nombre Nombre de la gestion de obra.
     * @return array Arreglo de objetos de tipo Data_GestionObraEdi.
     */
    public function filtrar($pagina=1, $nombre='')
    {
        global $conexion;

        $arrGestionObra = array();
        try {
            $sql = 'SELECT * FROM ' . $this->_table;
            if ($nombre != '') {
                $sql .= " WHERE des_gestion_obra LIKE '%" . $nombre . "%'";
            }
            $offset = ($pagina - 1) * TAM_PAG_LISTADO_MAP;
            $sql .= ' LIMIT ' . $offset . ',' . TAM_PAG_LISTADO_MAP;
            foreach ($conexion->query($sql) as $row) {
                $objeto = new Data_GestionObraEdi();
                $objeto->setIdGestObra($row['idgestion_obra']);
                $objeto->setDesGestObra($row['des_gestion_obra']);
                $objeto->setFlag($row['flag']);
                $arrGestionObra[] = $objeto;
            }
        } catch (PDOException $error) {
            $error = "Ocurrio un error : " . $error->getMessage();
        }
        return $arrGestionObra;
    }
    
    /**
     * @author Wilder Sandoval Huamani <wsandoval@gmd.com.pe>
     * @name listar
     * Obtener un arreglo de gestiones de obra que concuerde con el criterio de 
     * busqueda seleccionado: 
     * - Id : Id de la gestion de obra.
     * @param object $conexion Objeto de conexion a la BD creada con PDO.
     * @param int $idGestionObra Id de la gestion de obra.
     * @return array Arreglo de objetos de tipo Data_GestionObraEdi.
     */
    public function listar($conexion, $idGestionObra='')
    {
        $array = array();
        try {
            $strWhere = "";
            if ($idGestionObra != '') {
                $strWhere .= " AND idgestion_obra = '" . $idGestionObra . "' ";
            }
            $sql = "SELECT idgestion_obra, des_gestion_obra, flag "  
                 . "FROM " . $this->_table . " WHERE 1=1 " . $strWhere
                 . " AND flag = '1' ORDER BY idgestion_obra";            
            foreach ($conexion->query($sql) as $row) {
                $id = $row['idgestion_obra'];
                $desc = $row['des_gestion_obra'];
                $flag = $row['flag'];

                $objGestionObra = new Data_GestionObraEdi();
                $objGestionObra->__set('_idGestionObra', $id);
                $objGestionObra->__set('_desGestionObra', $desc);
                $objGestionObra->__set('_flag', $flag);
                $array[] = $objGestionObra;                
            }
        } catch (PDOException $error) {
            $error = "Ocurrio un error : " . $error->getMessage();
        }
        return $array;
    }
}