<?php

class GeoArmariopunto
{

    protected $_db = 'webpsi';
    protected $_table = 'geo_armariopunto';

    public function listar($conexion, $arreglo = array())
    {
        try {
            $zonal = $arreglo[0];
            $mdf = $arreglo[1];
            $armario = $arreglo[2];

            $sql = "SELECT id, armario campo, coord_x, coord_y  
                    FROM " . $this->_db . "." . $this->_table . " WHERE id<>'' ";

            $sql .= " AND zonal='$zonal' 
                      AND mdf='$mdf'
                      AND armario='$armario'";
            
            $sql .= " ORDER BY armario, orden";
            $bind = $conexion->prepare($sql);
            $bind->execute();

            $reporte = array();
            while ($data = $bind->fetch(PDO::FETCH_ASSOC)) {
                $reporte[] = $data;
            }
            return $reporte;
        } catch (PDOException $error) {
            exit();
        }
    }

    public function primerFiltro($conexion){
        try {
            $sql = "SELECT zonal nombre, zonal id 
                    FROM " . $this->_db . "." . $this->_table 
                    . " GROUP BY 1 ORDER BY 1";
            $bind = $conexion->prepare($sql);
            $bind->execute();

            $reporte = array();
            while ($data = $bind->fetch(PDO::FETCH_ASSOC)) {
                $reporte[] = $data;
            }
            return $reporte;
        } catch (PDOException $e) {
            exit();
        }
    }

    public function listarMdf($conexion, $arreglo=array()){
        try {
            $zonal = $arreglo[0];

            $sql = "SELECT mdf campo, mdf id 
                    FROM " . $this->_db . "." . $this->_table 
                    . " WHERE zonal='$zonal'"
                    . " GROUP BY 1 ORDER BY 1";
            $bind = $conexion->prepare($sql);
            $bind->execute();

            $reporte = array();
            while ($data = $bind->fetch(PDO::FETCH_ASSOC)) {
                $reporte[] = $data;
            }
            return $reporte;
        } catch (PDOException $e) {
            exit();
        }   
    }

    public function listarArmariopunto($conexion, $arreglo=array()){
        try {
            $zonal = $arreglo[0];
            $mdf = $arreglo[1];

            $sql = "SELECT armario campo, armario id 
                    FROM " . $this->_db . "." . $this->_table 
                    . " WHERE zonal='$zonal'"
                    . " AND mdf='$mdf'"
                    . " GROUP BY 1 ORDER BY 1";
            $bind = $conexion->prepare($sql);
            $bind->execute();

            $reporte = array();
            while ($data = $bind->fetch(PDO::FETCH_ASSOC)) {
                $reporte[] = $data;
            }
            return $reporte;
        } catch (PDOException $e) {
            exit();
        }   
    }

}