<?php

class GeoClienteanpunto
{

    protected $_db = 'webpsi';
    protected $_table = 'geo_clienteanpunto';

    public function listar($conexion, $arreglo = array())
    {
        try {
            $zonal = $arreglo[0];
            $tipo = $arreglo[1];
            $origen = $arreglo[2];
            $cliente = $arreglo[3];

            $sql = "SELECT id, cliente campo, coord_x, coord_y  
                    FROM " . $this->_db . "." . $this->_table . " WHERE id<>'' ";

            $sql .= " AND zonal='$zonal' 
                      AND tipo='$tipo'
                      AND origen='$origen'
                      AND cliente='$cliente'";
            
            //$sql .= " ORDER BY tipo, cliente";
            $bind = $conexion->prepare($sql);
            $bind->execute();

            $reporte = array();
            while ($data = $bind->fetch(PDO::FETCH_ASSOC)) {
                $reporte[] = $data;
            }
            return $reporte;
        } catch (PDOException $error) {
            exit();
        }
    }

    public function primerFiltro($conexion){
        try {
            $sql = "SELECT zonal nombre, zonal id 
                    FROM " . $this->_db . "." . $this->_table 
                    . " GROUP BY 1 ORDER BY 1";
            $bind = $conexion->prepare($sql);
            $bind->execute();

            $reporte = array();
            while ($data = $bind->fetch(PDO::FETCH_ASSOC)) {
                $reporte[] = $data;
            }
            return $reporte;
        } catch (PDOException $e) {
            exit();
        }
    }

    public function listarTipo($conexion, $arreglo=array()){
        try {
            $zonal = $arreglo[0];

            $sql = "SELECT tipo campo, tipo id 
                    FROM " . $this->_db . "." . $this->_table 
                    . " WHERE zonal='$zonal'"
                    . " GROUP BY 1 ORDER BY 1";
            $bind = $conexion->prepare($sql);
            $bind->execute();

            $reporte = array();
            while ($data = $bind->fetch(PDO::FETCH_ASSOC)) {
                $reporte[] = $data;
            }
            return $reporte;
        } catch (PDOException $e) {
            exit();
        }   
    }
    
    public function listarOrigen($conexion, $arreglo=array()){
        try {
            $zonal = $arreglo[0];
            $tipo = $arreglo[1];

            $sql = "SELECT origen campo, tipo id 
                    FROM " . $this->_db . "." . $this->_table 
                    . " WHERE zonal='$zonal'"
                    . " AND tipo='$tipo'"
                    . " GROUP BY 1 ORDER BY 1";
            $bind = $conexion->prepare($sql);
            $bind->execute();

            $reporte = array();
            while ($data = $bind->fetch(PDO::FETCH_ASSOC)) {
                $reporte[] = $data;
            }
            return $reporte;
        } catch (PDOException $e) {
            exit();
        }   
    }

    public function listarClienteanpunto($conexion, $arreglo=array()){
        try {
            $zonal = $arreglo[0];
            $tipo = $arreglo[1];
            $origen = $arreglo[2];

            $sql = "SELECT cliente campo, cliente id 
                    FROM " . $this->_db . "." . $this->_table 
                    . " WHERE zonal='$zonal'"
                    . " AND tipo='$tipo'"
                    . " AND origen='$origen'"
                    . " GROUP BY 1 ORDER BY 1";
            $bind = $conexion->prepare($sql);
            $bind->execute();

            $reporte = array();
            while ($data = $bind->fetch(PDO::FETCH_ASSOC)) {
                $reporte[] = $data;
            }
            return $reporte;
        } catch (PDOException $e) {
            exit();
        }   
    }

}