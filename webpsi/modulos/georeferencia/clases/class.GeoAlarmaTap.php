<?php

class GeoAlarmaTap
{

    protected $_db = 'webpsi';
    protected $_table = 'geo_alarma_tap';

    public function trobasAlarma($conexion, $data)
    {
        try {
            /*
            $sql = "SELECT zonal, nodo, troba, COUNT(codcli) n
                    FROM $this->_db.$this->_table 
                    GROUP BY zonal, nodo, troba
                    ORDER BY n, nodo, troba";
             * 
             */
            $sql = "SELECT 
                        a.zonal, a.nodo, a.troba, 
                        COUNT(a.codcli) n, b.troba alarma, 
                        b.color, b.opacidad, b.blink
                    FROM 
                        webpsi.geo_alarma_tap a 
                            INNER JOIN webpsi.trobas_alertadas b 
                            ON a.nodo=b.nodo AND a.troba=b.troba
                    WHERE 
                        a.troba <> '' ";
            
            if ( isset($data["zonal"]) )
            {
                $sql .= " AND a.zonal='{$data["zonal"]}'";
            }
            
            $sql .= "GROUP BY 
                        a.zonal, a.nodo, a.troba
                    ORDER BY 
                        color, a.nodo, a.troba, n";

            $bind = $conexion->prepare($sql);
            $bind->execute();

            $reporte = array();
            while ($data = $bind->fetch(PDO::FETCH_ASSOC)) {
                $reporte[] = $data;
            }
            return $reporte;
        } catch (PDOException $error) {
            exit();
        }
    }

    public function tapAlarma($conexion)
    {
        try {

            $sql = "SELECT 
                        b.coord_x, b.coord_y, 
                        a.zonal, a.nodo, a.troba, 
                        a.amplificador, a.tap
                    FROM
                        webpsi.geo_alarma_tap a, 
                        webpsi.geo_tap b
                    WHERE
                        a.zonal=b.zonal 
                        AND a.nodo=b.nodo 
                        AND a.troba=b.troba 
                        AND a.amplificador=b.amplificador 
                        AND a.tap=b.tap";
            $bind = $conexion->prepare($sql);
            $bind->execute();

            $reporte = array();
            while ($data = $bind->fetch(PDO::FETCH_ASSOC)) {
                $reporte[] = $data;
            }
            return $reporte;
        } catch (PDOException $error) {
            exit();
        }
    }
    
    public function tapAlarmaClientes($conexion, $zonal, $nodo, $troba)
    {
        try {

            $sql = "SELECT 
                        a.codcli, a.nombres, a.ipaddress, 
                        a.interface, a.detalle, a.amplificador, a.tap, 
                        b.coord_x, b.coord_y
                    FROM
                        webpsi.geo_alarma_tap a 
                            LEFT JOIN webpsi.geo_tap b
                                ON a.zonal=b.zonal 
                                AND a.nodo=b.nodo 
                                AND a.troba=b.troba 
                                AND a.amplificador=b.amplificador 
                                AND a.tap=b.tap
                    WHERE
                        a.zonal='$zonal' 
                        AND a.nodo='$nodo' 
                        AND a.troba='$troba'";
            $bind = $conexion->prepare($sql);
            $bind->execute();

            $reporte = array();
            while ($data = $bind->fetch(PDO::FETCH_ASSOC)) {
                $reporte[] = $data;
            }
            return $reporte;
        } catch (PDOException $error) {
            exit();
        }
    }
    
    public function ampAlarmaClientes($conexion, $zonal, $nodo, $troba)
    {
        try {

            $sql = "SELECT 
                        amplificador, coord_x, coord_y
                    FROM 
                        webpsi.geo_amplificador 
                    WHERE 
                        zonal='$zonal' 
                        AND nodo='$nodo' 
                        AND troba='$troba'";
            $bind = $conexion->prepare($sql);
            $bind->execute();

            $reporte = array();
            while ($data = $bind->fetch(PDO::FETCH_ASSOC)) {
                $reporte[] = $data;
            }
            return $reporte;
        } catch (PDOException $error) {
            exit();
        }
    }

}