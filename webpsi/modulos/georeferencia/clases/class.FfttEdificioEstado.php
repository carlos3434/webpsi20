<?php

/**
 * @package     class/data/
 * @name        class.ffttEdificioEstado.php
 * @category    Model
 * @author      Wilder Sandoval Huamani <wsandoval@gmd.com.pe>
 * @copyright   2011 Telefonica del Peru
 * @version     1.0 - 2012/01/09
 */

class Data_FfttEdificioEstado
{

    protected $_table = 'webunificada_fftt.fftt_edificios_estado';
    
    protected $_codEdificioEstado = '';
    protected $_desEstado = '';

    /**
     * Metodo constructor
     * @method __construct
     */
    public function __construct()
    {
    }
    
    /**
     * Obtiene el valor de un atributo
     *
     * @return string $returnValue
     * @param string $propiedad nombre del atributo a obtener
     */
    public function __get($propiedad)
    {
        $returnValue = (string) '';
        $returnValue = $this->$propiedad;
        return (string) $returnValue;
    }

    /**
     * Asigna un valor a un atributo
     *
     * @param string $propiedad nombre del atributo a setear
     * @param string $valor valor a asignar
     */
    public function __set($propiedad, $valor)
    {
        $this->$propiedad = $valor;
    }

    /**
     * Obtiene un listado de los estados de los edificios
     *
     * @return array $array[] array de objetos de tipo Data_ffttEdificioEstado
     * @param object $conexion Objeto de conexion a la BD creada con PDO
     * @param string $codEdificioEstado codigo del estado del edificio
     */
    public function obtenerEdificioEstado($conexion, $codEdificioEstado='')
    {
        try {
            $strWhere = "";
            if ($codEdificioEstado != '') {
                $strWhere = " AND codedificio_estado 
                = '" . $codEdificioEstado . "' ";
            }
            
            $sql = "SELECT codedificio_estado, des_estado 
                    FROM " . $this->_table . "   
                    WHERE 1=1 " . $strWhere . " 
                    AND flag = '1' 
                    ORDER BY codedificio_estado";
            $array = array();
            foreach ($conexion->query($sql) as $row):
                if($row['codedificio_estado'] != ''):
                    $objEdificioEstado = new Data_ffttEdificioEstado();
                    $objEdificioEstado->__set(
                        '_codEdificioEstado', $row['codedificio_estado']
                    );
                    $objEdificioEstado->__set('_desEstado', $row['des_estado']);
                    $array[] = $objEdificioEstado;
                endif;
            endforeach;
            
            return $array;
        } catch(PDOException $e) {
            throw $e;
        }
    }
}