<?php
class GeoElemento
{

    protected $_db = 'webpsi';
    protected $_table = 'geo_elementos';

    public function listar($conexion, $estado = false)
    {
        try {

            $sql = "SELECT id, nombre, descripcion, tipo 
                    FROM " . $this->_db . "." . $this->_table . " WHERE id <> '' ";

            if ( $estado === true ) {
                $sql .= " AND estado = 1 ";
            }
            
            $sql .= "ORDER BY nombre, orden";
            $bind = $conexion->prepare($sql);
            $bind->execute();

            $reporte = array();
            while ($data = $bind->fetch(PDO::FETCH_ASSOC)) {
                $reporte[] = $data;
            }
            return $reporte;
        } catch (PDOException $error) {
            exit();
        }
    }

}