<?php

class GeoProyecto {

    protected $_db = 'webpsi';
    protected $_table = 'geo_proyectos';
    private $_data = array();

    public function saveProject(
            $dbh, $proyecto, $capas, $usuario_id, $editar=array()) {
        $result = array();
        $capasArray = array();

        try {
            //Iniciar transaccion
            $dbh->beginTransaction();
                        
            //"Editar" proyecto
            if ($proyecto["keyId"] != "" and $proyecto["keyId"] > 0) {
                //Id del proyecto
                $id = $proyecto["keyId"];
                
                //Eliminar elementos del proyecto
                foreach ($capas as $idReg => $valReg) {
                    $layer = explode("|~", $valReg["layer"]);

                    $sql = "DELETE FROM "
                            . $this->_db
                            . ".geo_proyecto_elementos"
                            . " WHERE layer_id = '{$layer[1]}'"
                            . " AND geo_proyecto_id = '{$proyecto["keyId"]}'";
                    $dbh->exec($sql);
                }

                //Registrar elementos del proyecto
                foreach ($capas as $key => $val) {
                    $newLayer = false;
                    $layer = $val["layer"];
                    $arrLayer = explode("|~", $layer);
                    $layer_id = $arrLayer[1];
                    if ( strlen( trim( $layer_id ) ) < 32 ) {
                        $layer_id = md5( $proyecto["keyId"] . $arrLayer[0] );
                        $newLayer = true;
                    }
                    $tipo = $val["tipo"];
                    $origen = $val["origen"];
                    $datos = $val["datos"];
                    $datos = str_replace("chld=", "", $datos);
                    if ($tipo != "" and $origen != "" and $datos != "") {
                        $sql = "INSERT INTO "
                                . $this->_db
                                . ".geo_proyecto_elementos (geo_proyecto_id, 
                                    layer_id, layer, tipo, origen, datos)
                                VALUES 
                                    ({$proyecto["keyId"]}, '$layer_id', 
                                    '{$arrLayer[0]}', '$tipo', '$origen', 
                                    '$datos')";
                        $dbh->exec($sql);

                        if (array_key_exists($layer_id, $capasArray) === false) 
                        {
                            $estilo = explode("[{chars}]", $datos);

                            $capasArray[$layer_id]["layer_id"] = $layer_id;
                            $capasArray[$layer_id]["geo_proyecto_id"] = $id;
                            $capasArray[$layer_id]["layer"] = $arrLayer[0];
                            $capasArray[$layer_id]["tipo"] = $tipo;
                            $capasArray[$layer_id]["origen"] = $origen;
                            $capasArray[$layer_id]["estilo"] = $estilo[1];
                            $capasArray[$layer_id]["estilo_activo"] = 1;
                            $capasArray[$layer_id]["new_layer"] = $newLayer;
                        }
                    }
                }
                
                //Actualizar capas
                if (is_array($capasArray)) 
                {
                    foreach ($capasArray as $key => $val) {
                        if ( $val['new_layer'] ) 
                        {
                            $sql = "INSERT INTO " 
                                    . $this->_db 
                                    . ".geo_proyecto_capas (layer_id, 
                                    geo_proyecto_id, layer, tipo, origen, 
                                    estilo, estilo_activo) "
                                    . "VALUES ('{$val['layer_id']}', 
                                    '{$val['geo_proyecto_id']}', 
                                    '{$val['layer']}', '{$val['tipo']}', 
                                    '{$val['origen']}', '{$val['estilo']}', 
                                    '{$val['estilo_activo']}')";
                        } else {
                            $sql = "UPDATE " 
                                    . $this->_db . ".geo_proyecto_capas"
                                    . " SET estilo = '{$val['estilo']}', "
                                    . " estilo_activo = '1' "
                                    . " WHERE layer_id = '{$val['layer_id']}'"
                                    . " AND geo_proyecto_id = 
                                        '{$val['geo_proyecto_id']}'";
                        }                        
                        $dbh->exec($sql);
                    }
                }
            } else {

                //Registrar proyecto
                $nombre = $proyecto["nombre"];

                if ($nombre != "") {
                    $sql = "INSERT INTO " 
                            . $this->_db . "." . $this->_table 
                            . " (nombre, usuario_id, estado) 
                            VALUES (?, ?, ?)";
                    $this->_data = array();
                    $this->_data[] = $nombre;
                    $this->_data[] = $usuario_id;
                    $this->_data[] = 1;
                    $stmt = $dbh->prepare($sql);
                    $stmt->execute($this->_data);
                }

                //Ultimo ID registrado
                $id = $dbh->lastInsertId();
                
                //Proyecto y permisos de usuario
                $date = date("Y-m-d H:i:s");
                $sql = "INSERT INTO 
                            $this->_db.geo_proyecto_usuarios 
                            (geo_proyecto_id, usuario_id, editar, fec_registro) 
                        VALUES 
                            (?, ?, ?, ?)";
                $this->_data = array();
                $this->_data[] = $id;
                $this->_data[] = $usuario_id;
                $this->_data[] = 1;
                $this->_data[] = $date;
                $stmt = $dbh->prepare($sql);
                $stmt->execute($this->_data);

                //Registrar elementos del proyecto
                foreach ($capas as $key => $val) {
                    $layer = $val["layer"];
                    $arrLayer = explode("|~", $layer);
                    $layer_id = md5($id . $layer);
                    $tipo = $val["tipo"];
                    $origen = $val["origen"];
                    $datos = $val["datos"];
                    $datos = str_replace("chld=", "", $datos);
                    if ($tipo != "" and $origen != "" and $datos != "") {
                        $sql = "INSERT INTO "
                                . $this->_db
                                . ".geo_proyecto_elementos (geo_proyecto_id, 
                                    layer_id, layer, tipo, origen, datos)
                                VALUES (?, ?, ?, ?, ?, ?)";
                        $this->_data = array();
                        $this->_data[] = $id;
                        $this->_data[] = $layer_id;
                        $this->_data[] = $arrLayer[0];
                        $this->_data[] = $tipo;
                        $this->_data[] = $origen;
                        $this->_data[] = $datos;
                        $stmt = $dbh->prepare($sql);
                        $stmt->execute($this->_data);

                        if (array_key_exists($layer_id, $capasArray) === false) 
                        {
                            $estilo = explode("[{chars}]", $datos);

                            $capasArray[$layer_id]["layer_id"] = $layer_id;
                            $capasArray[$layer_id]["geo_proyecto_id"] = $id;
                            $capasArray[$layer_id]["layer"] = $arrLayer[0];
                            $capasArray[$layer_id]["tipo"] = $tipo;
                            $capasArray[$layer_id]["origen"] = $origen;
                            $capasArray[$layer_id]["estilo"] = $estilo[1];
                            $capasArray[$layer_id]["estilo_activo"] = 1;
                        }
                    }
                }

                //Registrar capas
                if (is_array($capasArray)) {
                    foreach ($capasArray as $key => $val) {
                        $sql = "INSERT INTO " 
                                . $this->_db . ".geo_proyecto_capas "
                                . "(layer_id, geo_proyecto_id, layer, tipo, "
                                . "origen, estilo, estilo_activo) "
                                . "VALUES (?, ?, ?, ?, ?, ?, ?)";
                        $this->_data = array();
                        $this->_data[] = $val['layer_id'];
                        $this->_data[] = $val['geo_proyecto_id'];
                        $this->_data[] = $val['layer'];
                        $this->_data[] = $val['tipo'];
                        $this->_data[] = $val['origen'];
                        $this->_data[] = $val['estilo'];
                        $this->_data[] = $val['estilo_activo'];
                        $stmt = $dbh->prepare($sql);
                        $stmt->execute($this->_data);
                    }
                }
            }

            $dbh->commit();
            $result["estado"] = true;
            $result["msg"] = "Proyecto registrado correctamente";
            $result["id"] = $id;
            return $result;
        } catch (PDOException $error) {
            $dbh->rollback();
            $result["estado"] = false;
            $result["msg"] = $error->getMessage();
            return $result;
            exit();
        }
    }

    public function doListLayer($conexion, $filter=array()) {
        try {

            $sql = "SELECT 
                        gp.id, gp.nombre, gp.fec_creacion, gu.editar
                    FROM 
                        $this->_db.geo_proyectos gp, 
                        $this->_db.geo_proyecto_usuarios gu
                    WHERE 
                        gp.id=gu.geo_proyecto_id AND gp.estado=1";
            
            if ( isset($filter['userid']) ) {
                $sql .= " AND gu.usuario_id=?";
                $this->_data[] = $filter['userid'];
            }
            
            $bind = $conexion->prepare($sql);
            $bind->execute($this->_data);

            $reporte = array();
            while ($data = $bind->fetch(PDO::FETCH_ASSOC)) {
                $reporte[] = $data;
            }
            return $reporte;
        } catch (PDOException $error) {
            exit();
        }
    }

    public function getProjectLayer($conexion, $id) {
        try {

            $sql = "SELECT layer_id, layer, tipo, origen, datos   
                    FROM "
                    . $this->_db . ".geo_proyecto_elementos"
                    . " WHERE layer_id='$id'";

            $bind = $conexion->prepare($sql);
            $bind->execute();

            $reporte = array();
            while ($data = $bind->fetch(PDO::FETCH_ASSOC)) {
                $reporte[] = $data;
            }
            return $reporte;
        } catch (PDOException $error) {
            exit();
        }
    }

    public function deleteProject($dbh, $id) {
        try {
            //Iniciar transaccion
            $dbh->beginTransaction();

            $sql = "DELETE FROM "
                    . $this->_db . "." . $this->_table
                    . " WHERE id=$id";
            $dbh->exec($sql);

            $dbh->commit();
            $result["estado"] = true;
            $result["msg"] = "Proyecto eliminado correctamente";
            return $result;
        } catch (PDOException $error) {
            $dbh->rollback();
            $result["estado"] = false;
            $result["msg"] = $error->getMessage();
            return $result;
            exit();
        }
    }

}
