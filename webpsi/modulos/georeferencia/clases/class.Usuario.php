<?php
/**
 *
 * [Web Unificada] :: Sistema Web Unificada
 *
 * PHP version 5
 *
 * Copyright (c) 2011 Planificacion y Sistemas Informaticos
 *
 * @author                Gonzalo Chacaltana Buleje <gchacaltana@gmd.com.pe>
 * @copyright           2011 � Planificacion y Soluciones Informaticas
 * @package             class/data/
 * @name                class.Usuario.phpf
 *
 * [Descripcion] :: Clase Usuario.
 * [Fecha de Actualizacion] :: 23-11-2011ultimoAccesoUsuario
 */
class Data_Usuario
{
    /**
     * Defincion de Propiedades
     * Ultima actualizacion: 23-11-2011 10:25:22 a.m
     * por @author Gonzalo Chacaltana Buleje <gchacaltana@gmd.com.pe>
     */
    
    /**
     * @property    $idusuario
     * @access      protected
     * ID del usuario
     */
    protected $_idUsuario = 0;
    
    /**
     * @property    $login
     * @access      protected
     * Login de acceso del usuario.
     */
    protected $_login = '';
    
    /**
     * @property $password
     * Password de acceso del usuario.
     */
    protected $_password = '';
    
    /**
     * @property    $appat
     * @access      protected
     * Apellido paterno del usuario.
     */
    protected $_appat = '';
    
    /**
     * @property    $apmat
     * @access      protected
     * Apellido Materno del usuario
     */
    protected $_apmat = '';
    
    /**
     * @property    $nombres
     * @access      protected
     * Nombre del usuario.
     */
    protected $_nombres = '';
    
    /**
     * @property    $idperfil
     * @access      protected
     * Id del Perfil del usuario.
     */
    protected $_idPerfil = '';
    
    /**
     * @property    $estado
     * @access      protected
     * Estado del usuario. Indica si un usuario esta habilitado o 
     * deshabilitado para acceder al Sistema.
     */
    protected $_estado='';
    
    /**
     * @property    $user_creator
     * @access      protected
     * Indica el Id del usuario quien lo creo.
     */
    protected $_userCreator=1;
    
    /**
     * @property    $dni
     * @access      protected
     * DNI del usuario. Unico en toda la tabla.
     */
    protected $_dni=0;
    
    /**
     * @property    $cip
     * @access      protected
     * Codigo CIP del usuario.
     */
    protected $_cip=0;
    
    /**
     * @property    $fecha_nac
     * @access      protected
     * Fecha de nacimiento del usuario.
     */
    protected $_fechaNac='';
    
    /**
     * @property    $correo
     * @access      protected
     * Correo electronico del usuario.
     */
    protected $_correo='';
    
    /**
     * @property    $celular
     * @access      protected
     * Numero celular del usuario.
     */
    protected $_celular='';
    
    /**
     * @property    $rpm
     * @access      protected
     * Numero RPM del usuario
     */
    protected $_rpm='';
    
    /**
     * @property    $idpool_atencion
     * @access      protected
     * ID del pool de atencion, solo en algunos usuarios.
     */
    protected $_idPoolAtencion='1';
    
    /**
     * @property    $idempresa
     * @access      protected
     * ID de la empresa de pertenencia del usuario.
     */
    protected $_idEmpresa='';
    
    /**
     * @property    $create_date
     * @access      protected
     * Fecha de creacion del usuario.
     */
    protected $_createDate='';
    
    /**
     * @property    $last_update_user
     * @access      protected
     * Id del ultimo usuario quien modifico informacion.
     */
    protected $_lastUpdateUser;
    
    /**
     * @property    $last_update_date
     * @access      protected
     * Ultima fecha de actualizacion.
     */
    protected $_lastUpdateDate='';
    
    /**
     * @property    $online_status
     * @access      protected
     * Indicador si el usuario esta conectado en el Sistema.
     */
    protected $_onlineStatus=0;
    
    /**
     * @property    $last_date_enter
     * @access      protected
     * Fecha del ultimo ingreso al sistema del usuario
     */
    protected $_lastDateEnter='NOW()';
    
    /**
     * @property    $last_date_session_activity
     * @access      protected
     * Ultima fecha de actividad del usuario en el Sistema.
     */
    protected $_lastDateSessionActivity='';
    
    /**
     * @property    $last_date_close_session
     * @access      protected
     * Fecha de la ultima vez que cerro session.
     */
    protected $_lastDateCloseSession='';
    
    /**
     * @property    $idarea
     * @access      protected
     * Id del area de pertenencia del usuario.
     */
    protected $_idArea;
    
    /**
     * @property    $idzonal
     * @access      protected
     * Id de la zonal de pertenencia del usuario
     */
    protected $_idZonal;
    
    /**
     * @property    $is_change_pass
     * @access      protected
     * Indicador utilizado para saber si el usuario debe actualizar su password.
     * 1 -> se le obliga a cambiar de password. Ocurre cuando se crea su cuenta 
     * o cuando se le resetea el password.
     * 2 -> se le obliga a cambiar de password. Ocurre cuando se verifica que 
     * el usuario no a actualizados su password por mas del tiempo establecido.
     */
    protected $_isChangePass;
    
    /**
     * @property    $last_update_pass_date
     * @access      protected
     * Ultima fecha de actualizacion de password por parte del usuario.
     */
    protected $_lastUpdatePassDate;
    
    /**
     * @property    $flagGeoRef
     * @access      protected
     * Array serializado de las opciones de Georeferencia.
     * Atributo agregado por wsandoval 27/06/2012
     */
    protected $_flagGeoRef;
    
    /**
     * Tabla a manipular
     * @access      public
     */
    protected $_table = 'webunificada.usuario';
    
    /*Constructor*/
    public function __construct()
    {        
    }

    public function __get($propiedad) 
    {
        $returnValue = (string) '';
        $returnValue = $this->$propiedad;
        return (string) $returnValue;
    }

    public function __set($propiedad, $valor)
    {
        $this->$propiedad = $valor;
    }
    
    /**
     * @author Gonzalo Chacaltana Buleje <gchacaltana@gmd.com.pe>
     * Metodo devuelve el objecto usuario por idusuario.
     * @method __offsetGet
     * @param object $conexion
     * @param int $idUsuario
     * @return object
     */
    public function offsetGet($conexion, $idUsuario)
    {
        if (empty($idUsuario)) {
            throw new PDOException("ID del usuario incorrecto.");
        }
        $sql = "SELECT idusuario, login, password, appat, apmat, nombres, 
        idperfil, estado, user_creator,dni,cip,fecha_nac,correo,celular,rpm,
        idpool_atencion,idempresa,create_date,last_update_user,last_update_date,
        online_status,idarea,idzonal,is_change_pass,flag_georef 
        FROM ".$this->_table." 
        WHERE idusuario='".$idUsuario."'";
            $objUsuario = new Data_Usuario();
            foreach ($conexion->query($sql) as $row):
                if($row['idusuario'] != ''):
                $objUsuario->__set('_idUsuario', $row['idusuario']);
                $objUsuario->__set('_login', $row['login']);
                $objUsuario->__set('_password', $row['password']);
                $objUsuario->__set('_appat', $row['appat']);
                $objUsuario->__set('_apmat', $row['apmat']);
                $objUsuario->__set('_nombres', $row['nombres']);
                $objUsuario->__set('_idPerfil', $row['idperfil']);
                $objUsuario->__set('_estado', $row['estado']);
                $objUsuario->__set('_userCreator', $row['user_creator']);
                $objUsuario->__set('_dni', $row['dni']);
                $objUsuario->__set('_cip', $row['cip']);
                $objUsuario->__set('_fechaNac', $row['fecha_nac']);
                $objUsuario->__set('_correo', $row['correo']);
                $objUsuario->__set('_celular', $row['celular']);
                $objUsuario->__set('_rpm', $row['rpm']);
                $objUsuario->__set('_idPoolAtencion', $row['idpool_atencion']);
                $objUsuario->__set('_idEmpresa', $row['idempresa']);
                $objUsuario->__set('_createDate', $row['create_date']);
                $objUsuario->__set('_lastUpdateUser', $row['last_update_user']);
                $objUsuario->__set('_lastUpdateDate', $row['last_update_date']);
                $objUsuario->__set('_onlineStatus', $row['online_status']);
                $objUsuario->__set('_idArea', $row['idarea']);
                $objUsuario->__set('_idZonal', $row['idzonal']);
                $objUsuario->__set('_isChangePass', $row['is_change_pass']);
                $objUsuario->__set('_flagGeoRef', $row['flag_georef']);
                endif;
            endforeach;
          return $objUsuario;
    }
    
    /**
     * @author Gonzalo Chacaltana Buleje <gchacaltana@gmd.com.pe>
     * Metodo devuelve el objecto usuario por login.
     * @method __offsetGetByLogin
     * @param object $conexion
     * @param string $login
     * @return object
     */
    public function offsetGetByLogin($conexion, $login)
    {
        if ($login!="") {
            
            $login=addslashes($login);
            
            $sql = "SELECT idusuario, login, appat, apmat, nombres, idperfil, 
            estado, user_creator,dni,cip,fecha_nac,correo,celular,rpm,
            idpool_atencion,idempresa,create_date,last_update_user,
            last_update_date,online_status,idarea,idzonal,is_change_pass 
            FROM ".$this->_table." WHERE login='".$login."'";
            $objUsuario = new Data_Usuario();
            foreach ($conexion->query($sql) as $row):
                if($row['idusuario'] != ''):
                $objUsuario->__set('_idUsuario', $row['idusuario']);
                $objUsuario->__set('_login', $row['login']);
                $objUsuario->__set('_appat', $row['appat']);
                $objUsuario->__set('_apmat', $row['apmat']);
                $objUsuario->__set('_nombres', $row['nombres']);
                $objUsuario->__set('_idPerfil', $row['idperfil']);
                $objUsuario->__set('_estado', $row['estado']);
                $objUsuario->__set('_userCreator', $row['user_creator']);
                $objUsuario->__set('_dni', $row['dni']);
                $objUsuario->__set('_cip', $row['cip']);
                $objUsuario->__set('_fechaNac', $row['fecha_nac']);
                $objUsuario->__set('_correo', $row['correo']);
                $objUsuario->__set('_celular', $row['celular']);
                $objUsuario->__set('_rpm', $row['rpm']);
                $objUsuario->__set('_idPoolAtencion', $row['idpool_atencion']);
                $objUsuario->__set('_idEmpresa', $row['idempresa']);
                $objUsuario->__set('_createDate', $row['create_date']);
                $objUsuario->__set('_lastUpdateUser', $row['last_update_user']);
                $objUsuario->__set('_lastUpdateDate', $row['last_update_date']);
                $objUsuario->__set('_onlineStatus', $row['online_status']);
                $objUsuario->__set('_idArea', $row['idarea']);
                $objUsuario->__set('_idZonal', $row['idzonal']);
                $objUsuario->__set('_isChangePass', $row['is_change_pass']);
                endif;
            endforeach;
          return $objUsuario;
            
        } else {
            throw new Exception("Login invalido");exit;
        }
    }
   
    public function save($conexion, $login, $password, $appat, $apmat, 
            $nombres, $idperfil, $userCreator)
    {
        try {
            $login = trim($login);
            $password = md5(trim($password));
            $appat = trim($appat);
            $apmat = trim($apmat);
            $nombres = trim($nombres);
 
            $sql = "INSERT INTO usuario (login,password,appat,apmat,nombres, 
            idperfil,estado,user_creator) VALUES (:login,:password,:appat,
            :apmat,:nombres,:idperfil,1,:user_creator)";
            $bind = $conexion->prepare($sql);
            $bind->bindParam(":login", $login);
            $bind->bindParam(":password", $password);
            $bind->bindParam(":appat", $appat);
            $bind->bindParam(":apmat", $apmat);
            $bind->bindParam(":nombres", $nombres);
            $bind->bindParam(":idperfil", $idperfil);
            $bind->bindParam(":user_creator", $userCreator);
            $bind->execute();
            return $conexion->lastInsertId();
        } catch(PDOException $error) {
            return $error;
            exit();
        }
    }

    public function saveUsuariosMasivo($conexion, $login, $password, $appat,
             $apmat, $nombres, $idPerfil, $userCreator, $dni, $cip, $fechaNac,
            $correo, $celular,$rpm)
    {
        try{
            $login = trim($login);
            $password = md5(trim($password));
            $appat = trim($appat);
            $apmat = trim($apmat);
            $nombres = trim($nombres);

            $dni=addslashes(trim($dni));
            $rpm=addslashes(trim($rpm));
            $estado="1";
            if ($rpm=='') {
                $rpm="-";
            }
            
            if ($celular=='') {
                $celular="-";
            }
            if ($cip=='') {
                $cip="-";
            }
            if ($dni=='00000000') {
                $dni="-";
            }

            $sql = "INSERT INTO usuario (login,password,appat,apmat,nombres,
            idperfil,estado,user_creator,dni,cip,fecha_nac,correo,celular,rpm) 
            VALUES ('$login','$password','$appat','$apmat','$nombres',
            '$idPerfil','$estado','$userCreator','$dni','$cip','$fechaNac',
            '$correo','$celular','$rpm')";
            $bind = $conexion->prepare($sql);
            $bind->execute();
            return $conexion->lastInsertId();
            
        }catch(PDOException $error){
            return $error;
            exit();
        }
    }
    
    /**
     * @author Gonzalo Chacaltana Buleje <gchacaltana@gmd.com.pe>
     * Metodo que registra valores del usuario con campos completos. 
     * Retorna el id del usuario registrado.
     * @method __save_Usuarios_Completo
     * @param object $conexion
     * @param string $login
     * @param string $password
     * @param string $appat
     * @param string $apmat
     * @param string $nombres
     * @param int $idperfil
     * @param int $user_creator
     * @param string $dni
     * @param string $cip
     * @param date $fecha_nac
     * @param string $correo
     * @param string $celular
     * @param string $rpm
     * @param int $idempresa_usuario
     * @param int $idarea_usuario
     * @param int $idzonal_usuario
     * @return int
     */
    public function saveUsuariosCompleto($conexion, $login, $password, 
            $appat, $apmat, $nombres, $idPerfil, $userCreator,$dni,$cip,
            $fechaNac,$correo,$celular,$rpm,$idEmpresaUsuario,$idAreaUsuario,
            $idzonalUsuario, $arrOpcGeoref=array())
    {
        try{
            $login = trim($login);
            $password = md5(trim($password));
            $appat = trim($appat);
            $apmat = trim($apmat);
            $nombres = trim($nombres);

            $dni=addslashes(trim($dni));
            $rpm=addslashes(trim($rpm));
            $estado="1";
            if ($rpm=='') {
                $rpm="-";
            }
            if ($celular=='') {
                $celular="-";
            }
            if ($cip=='') {
                $cip="-";
            }
            if ($dni=='00000000') {
                $dni="-";
            }
            $fechaActual=date('Y-m-d');
            $sql = "INSERT INTO usuario (login,password,appat,apmat,nombres,
            idperfil,estado,user_creator,dni,cip,fecha_nac,correo,celular,rpm,
            idempresa,create_date,last_update_user,last_update_date,idarea,
            idzonal, flag_georef) VALUES ('$login','$password','$appat',
            '$apmat','$nombres','$idPerfil','$estado','$userCreator','$dni',
            '$cip','$fechaNac','$correo','$celular','$rpm','$idEmpresaUsuario',
            '".$fechaActual."','$userCreator','".$fechaActual."',
            '".$idAreaUsuario."','".$idzonalUsuario."',
            '" . serialize($arrOpcGeoref) . "')";
           
            $bind = $conexion->prepare($sql);
            $bind->execute();
            return $conexion->lastInsertId();

        }catch(PDOException $error){
            return $error;
            exit();
        }
    }
    
    /**
     * @author Gonzalo Chacaltana Buleje <gchacaltana@gmd.com.pe>
     * Metodo que actualiza campos del usuario. [No usado].
     * @method __update
     * @param object $conexion
     * @param int $idUsuario
     * @param int $state
     */
    public function update($conexion, $idUsuario, $login, $password, $appat, 
            $apmat, $nombres, $idperfil)
    {
        try {
            $appat=trim($appat);
            $apmat=trim($apmat);
            $nombres=trim($nombres);

            $login=trim($login);
            $password=md5(trim($password));
            
            $sql = "UPDATE usuario SET login = :login, password = :password, 
            appat = :appat, apmat = :apmat, nombres = :nombres, 
            idperfil = :idperfil WHERE idusuario = :idusuario";
            $bind = $conexion->prepare($sql);
            $bind->bindParam(":idusuario", $idUsuario);
            $bind->bindParam(":login", $login);
            $bind->bindParam(":password", $password);
            $bind->bindParam(":appat", $appat);
            $bind->bindParam(":apmat", $apmat);
            $bind->bindParam(":nombres", $nombres);
            $bind->bindParam(":idperfil", $idperfil);
            $bind->execute();
        } catch(PDOException $error) {
            return $error;
            exit();
        }
    }
    
    /**
     * @author Gonzalo Chacaltana Buleje <gchacaltana@gmd.com.pe>
     * Metodo que actualiza campos del usuario. [No usado].
     * @method __updateCustomer
     * @param object $conexion
     * @param int $idUsuario
     * @param int $state
     */
     public function updateCustomer($conexion, $idUsuario, $appat, $apmat, 
             $nombres, $idperfil,$dni,$fecha,$cip,$celular,$rpm,$correo)
     {
        try {
            $appat=trim($appat);
            $apmat=trim($apmat);
            $nombres=trim($nombres);
            
            if ($fecha=='') {
                $fecha='-';
            }
            
            if ($celular=='') {
                $celular='-';
            }
            if ($rpm=='') { 
                $rpm='-';
            }
            if ($dni=='') {
                $dni='';
            }
            if ($cip=='') {
                $cip='-';
            }
            if ($correo=='') {
                $correo='-';
            }

            $sql = "UPDATE usuario SET appat ='".$appat."',
                    apmat ='".$apmat."', nombres ='".$nombres."', 
                    idperfil =".$idperfil.", dni='".$dni."', 
                    fecha_nac='".$fecha."', cip='".$cip."' , 
                    celular='".$celular."', rpm='".$rpm."', correo='".$correo."'
                    WHERE idusuario =".$idUsuario;
            
            $bind = $conexion->prepare($sql);
            $bind->execute();
        }catch(PDOException $error) {
            return $error;
            exit();
        }
     }
    
    /**
     * @author Gonzalo Chacaltana Buleje <gchacaltana@gmd.com.pe>
     * Metodo que actualiza el estado del usuario.
     * @method __updateStateUser
     * @param object $conexion
     * @param int $idUsuario
     * @param int $state
     */
    public function updateStateUser($conexion, $idUsuario, $state)
    {
        try {
            $sql = "UPDATE ".$this->_table." SET estado=:state 
            WHERE idusuario =:idusuario";
            $bind = $conexion->prepare($sql);
            $bind->bindParam(":idusuario", $idUsuario);
            $bind->bindParam(":state", $state);
            $bind->execute();
            
            //Limpiar los registros de intentos fallidos, para cuando se 
            //habilita al usuario.
            if ($state==1) {
                $objUsuario = null;
                $objUsuario = new Data_Usuario();
                $objUsuario = $objUsuario->offsetGet($conexion, $idUsuario);
                $objAuthenticacionFallida = null;
                $objAuthenticacionFallida = new Data_Autenticacion(
                    $conexion, $login = $objUsuario->__get('_login'), 
                    $password = ''
                );
                $objAuthenticacionFallida->cleanConnectionsFailed(
                    $conexion, $objUsuario->__get('_login'), date("Y-m-d")
                );
            }
            
        }catch(PDOException $error) {
            return $error;
            exit();
        }
    }
    
    /**
     * @author Wilder Sandoval Huamani <wsandoval@gmd.com.pe>
     * Metodo que actualiza el flag de georeferencia.
     * Fecha creacion: 28/06/2012
     * @method updateFlagGeoref
     * @param object $conexion
     * @param int $idUsuario
     * @param array $flagGeoref
     */
    public function updateFlagGeoref($conexion, $idUsuario, $flagGeoref)
    {
        try {
            $sql = "UPDATE ".$this->_table." SET flag_georef=:flag_georef
            WHERE idusuario =:idusuario";
            $bind = $conexion->prepare($sql);
            $bind->bindParam(":idusuario", $idUsuario);
            $bind->bindParam(":flag_georef", serialize($flagGeoref));
            $bind->execute();

        }catch(PDOException $e) {
            throw $e;
        }
    }
     public function updateFlagGeorefClonado($conexion, $idUsuario, $flagGeoref)
     {
        try {
            $sql = "UPDATE ".$this->_table." SET flag_georef=:flag_georef
            WHERE idusuario =:idusuario";
            $bind = $conexion->prepare($sql);
            $bind->bindParam(":idusuario", $idUsuario); 
            $bind->bindParam(":flag_georef", ($flagGeoref));
            $bind->execute();

        }catch(PDOException $e) {
            throw $e;
        }
     }
    
    /**
     * @author Gonzalo Chacaltana Buleje <gchacaltana@gmd.com.pe>
     * Metodo que actualiza el password del usuario.
     * @method __updatePasswordUser
     * @param object $conexion
     * @param int $idUsuario
     * @param string $pass
     * @return void
     */
    public function updatePasswordUser($conexion, $idUsuario, $pass)
    {
        try {
            $pass=md5($pass);
            $sql = "UPDATE ".$this->_table." SET password=:pass 
            WHERE idusuario=:idusuario";
            $bind = $conexion->prepare($sql);
            $bind->bindParam(":idusuario", $idUsuario);
            $bind->bindParam(":pass", $pass);
            $bind->execute();
        }catch(PDOException $error) {
            die("Error al actualizar password del usuario");
        }
    }
    
    /**
     * @author Gonzalo Chacaltana Buleje <gchacaltana@gmd.com.pe>
     * Metodo que actualiza el campo indicador si el usuario debe 
     * actualizar su password.
     * @method __updatePasswordUser
     * @param object $conexion
     * @param int $idUsuario
     * @param string $pass
     * @return void
     */
    public function updateIndicatorChangePass($conexion, $idUsuario,$isChange)
    {
        try {
            $sql = "UPDATE ".$this->_table." SET is_change_pass=:is_change_pass,
            last_update_pass_date=:last_update_pass_date 
            WHERE idusuario =:idusuario";
            $bind = $conexion->prepare($sql);
            $bind->bindParam(":idusuario", $idUsuario);
            $bind->bindParam(":is_change_pass", $isChange);
            $bind->bindParam(":last_update_pass_date", date("Y-m-d G:i:s"));
            $bind->execute();
        }catch(PDOException $error) {
            return $error;
            exit();
        }
    }
    
     /**
     * @author Gonzalo Chacaltana Buleje <gchacaltana@gmd.com.pe>
     * Metodo que actualiza los valores del usuario por campo.
     * @method __updateUserXcampo
     * @param object $conexion
     * @param int $idUsuario
     * @param string $campo
     * @param int $valor
     */
    public function updateUserXcampo($conexion, $idUsuario, $campo, $valor)
    {
        try {
            $fechaActual=date('Y-m-d');
            $sql = "UPDATE ".$this->_table." SET ".$campo."='".$valor."', 
            last_update_user='".$_SESSION['USUARIO']->__get('_idUsuario')."', 
            last_update_date='".$fechaActual."' 
            WHERE idusuario =".$idUsuario;
            $bind = $conexion->prepare($sql);
            $bind->execute();
        }catch(PDOException $error) {
            return $error;
            exit();
        }
    }
    
     /**
     * @author Gonzalo Chacaltana Buleje <gchacaltana@gmd.com.pe>
     * Metodo que devuelve lista de usuarios paginados.
     * @method listarAll
     * @param object $conexion
     * @param int $pagina
     * @param string $campoOrder
     * @param string $direction
     * @param array $filtroEmpresa
     * @param array array_area
     * @return array.
     */
    public function listarAll($conexion,$pagina='',$campoOrder='',$direction='',
            $filtroEmpresa='',$filtroArea='',$filtroPerfil="",$spvseg="")
    {
        try {
            
            $strLimite='';
            $strOrder='';
            $strWhere='';

            //Arreglo de campos de ordenamiento
           
            $arregloCampos=array();
            $arregloCampos[1]="u.login";
            $arregloCampos[2]="u.nombres";
            $arregloCampos[3]="u.idempresa";
            $arregloCampos[4]="u.idperfil";
            if($campoOrder) $campo=$arregloCampos[$campoOrder];
            
            //Armando cadena de Ordenamiento
            if ($campoOrder=='') {
                $strOrder.=' u.idusuario';
            } else {
                $strOrder.=$campo;
            }
            
            if ($direction=='') {
                $strOrder.=' ASC';
            } else {
                if ($direction=="asc") {
                    $strOrder.=' ASC';
                } else {
                    if ($direction=="desc") {
                        $strOrder.=' DESC';
                    }
                }
            }
            
            //Armando cadena de Limites de registros
            if ($pagina == '') {
                $pagina = 1;
            } else {
                $offset = ($pagina - 1) * TAM_PAG_LISTADO;
                $strLimite = " LIMIT " . $offset . "," . TAM_PAG_LISTADO;
            }

            //Armando cadena de Filtros de busqueda

            $whereEmpresa='';

            if ($filtroEmpresa !='' && $filtroEmpresa!='-1' && 
                    $filtroEmpresa!='0') {
                $strWhere.=' AND u.idempresa='.$filtroEmpresa;
            }
            
            if ($filtroPerfil !='' && $filtroPerfil!='-1' && 
                    $filtroPerfil!='0') {
                $strWhere.=' AND u.idperfil='.$filtroPerfil;
            }
            
            if ($spvseg == true) {
                $strWhere.=" AND idusuario<>1";
            }
            
            $sql = "SELECT u.idusuario,u.login,u.password,u.nombres,u.appat,
            u.apmat,u.estado,u.idperfil,u.idempresa,u.rpm,u.idpool_atencion,
            u.celular,u.user_creator,u.cip,u.correo,u.fecha_nac,u.idarea,u.dni 
            FROM ".$this->_table." u  WHERE 1=1 ".$strWhere." 
            ORDER BY ".$strOrder.$strLimite;
            
            $array = array();
        foreach ($conexion->query($sql) as $row):
            if($row['idusuario'] != ''):
                $objUsuario = new Data_Usuario();
                $objUsuario->__set('_idUsuario', $row['idusuario']);
                $objUsuario->__set('_login', $row['login']);
                $objUsuario->__set('_password', $row['password']);
                $objUsuario->__set('_appat', $row['appat']);
                $objUsuario->__set('_apmat', $row['apmat']);
                $objUsuario->__set('_nombres', $row['nombres']);
                $objUsuario->__set('_idPerfil', $row['idperfil']);
                $objUsuario->__set('_estado', $row['estado']);
                $objUsuario->__set('_userCreator', $row['user_creator']);
                $objUsuario->__set('_dni', $row['dni']);
                $objUsuario->__set('_cip', $row['cip']);
                $objUsuario->__set('_fechaNac', $row['fecha_nac']);
                $objUsuario->__set('_correo', $row['correo']);
                $objUsuario->__set('_celular', $row['celular']);
                $objUsuario->__set('_rpm', $row['rpm']);
                $objUsuario->__set('_idPoolAtencion', $row['idpool_atencion']);
                $objUsuario->__set('_idEmpresa', $row['idempresa']);
                $objUsuario->__set('_idArea', $row['idarea']);
                $array[] = $objUsuario;
            endif;
        endforeach;
            return $array;
        } catch(PDOException $error) {
            return $error;
            exit();
        }
    }
    
     /**
     * @author Gonzalo Chacaltana Buleje <gchacaltana@gmd.com.pe>
     * Metodo que devuelve lista de usuarios paginados.
     * @method listar
     * @param object $conexion
     * @param $pagina
     * @return array.
     */
    public function listar($conexion,$pagina='',$spvseg="")
    {
        try {
            
            // Cadena de delimitación de la búsqueda
            $strLimite = '';
            $strWhere = "";
            if ($pagina == '') {
                $pagina = 1;
            } else {
                $offset = ($pagina - 1) * TAM_PAG_LISTADO;
                $strLimite = " LIMIT " . $offset . "," . TAM_PAG_LISTADO;
            }
            if ($spvseg == true) {
                $strWhere.=" AND idusuario<>1";
            }
            
            
            $sql = "SELECT idusuario, login, password, appat, apmat, nombres, 
            idperfil, estado, user_creator,dni,cip,fecha_nac,correo,celular,rpm,
            idpool_atencion,idempresa,idarea,idzonal,is_change_pass 
            FROM ".$this->_table." WHERE 1=1 ".$strWhere.$strLimite;
            $array = array();
        foreach ($conexion->query($sql) as $row):
            if($row['idusuario'] != ''):
                $objUsuario = new Data_Usuario();
                $objUsuario->__set('_idUsuario', $row['idusuario']);
                $objUsuario->__set('_login', $row['login']);
                $objUsuario->__set('_password', $row['password']);
                $objUsuario->__set('_appat', $row['appat']);
                $objUsuario->__set('_apmat', $row['apmat']);
                $objUsuario->__set('_nombres', $row['nombres']);
                $objUsuario->__set('_idPerfil', $row['idperfil']);
                $objUsuario->__set('_estado', $row['estado']);
                $objUsuario->__set('_userCreator', $row['user_creator']);
                $objUsuario->__set('_dni', $row['dni']);
                $objUsuario->__set('_cip', $row['cip']);
                $objUsuario->__set('_fechaNac', $row['fecha_nac']);
                $objUsuario->__set('_correo', $row['correo']);
                $objUsuario->__set('_celular', $row['celular']);
                $objUsuario->__set('_rpm', $row['rpm']);
                $objUsuario->__set('_idPoolAtencion', $row['idpool_atencion']);
                $objUsuario->__set('_idEmpresa', $row['idempresa']);
                $objUsuario->__set('_idArea', $row['idarea']);
                $objUsuario->__set('_idZonal', $row['idzonal']);
                $objUsuario->__set('_isChangePass', $row['is_change_pass']);
                $array[] = $objUsuario;
            endif;
        endforeach;
            return $array;
        } catch(PDOException $error) {
            return $error;
            exit();
        }
    }
    
     /**
     * @author Gonzalo Chacaltana Buleje <gchacaltana@gmd.com.pe>
     * Metodo que devuelve numero total de usuarios.
     * @method listTotal
     * @param object $conexion
     * @return int.
     */
    public function listTotal($conexion)
    {
        try {

            $sql = "SELECT count(1) AS 'Total' FROM ".$this->_table;
            foreach ($conexion->query($sql) as $row):
                    $total = $row['Total'];
            endforeach;
            return $total;
        } catch(PDOException $error) {
            return $error;
            exit();
        }
    }
    /**
     * @author Gonzalo Chacaltana Buleje <gchacaltana@gmd.com.pe>
     * Metodo de validacion de login y password que devuelve un objeto 
     * de la clase usuario<br/>
     * en caso la validacion sea exitosa y devuelve un false en caso no lo sea.
     * @method validate
     * @param object $conexion
     * @param string $login
     * @param string $password
     * @return object / bool.
     */
    public function validate($conexion, $login, $password)
    {
        try {
            $sql = "SELECT idusuario, login, password, appat, apmat, nombres, 
            idperfil,estado, user_creator, dni, cip, fecha_nac, correo, 
            celular, rpm, idpool_atencion,idempresa,online_status,
            last_date_enter,last_date_session_activity,idarea,idzonal,
            is_change_pass FROM ".$this->_table." 
            WHERE login = '".$login."' AND password = '".md5($password)."'";
            $flag = false;

            foreach ($conexion->query($sql) as $row):
                if($row['login'] != '' && $row['password'] != ''):
                    $objUsuario = new Data_Usuario();
                    $objUsuario->__set('_idUsuario', $row['idusuario']);
                    $objUsuario->__set('_login', $row['login']);
                    $objUsuario->__set('_password', $row['password']);
                    $objUsuario->__set('_appat', $row['appat']);
                    $objUsuario->__set('_apmat', $row['apmat']);
                    $objUsuario->__set('_nombres', $row['nombres']);
                    $objUsuario->__set('_idPerfil', $row['idperfil']);
                    $objUsuario->__set('_estado', $row['estado']);
                    $objUsuario->__set('_userCreator', $row['user_creator']);
                    $objUsuario->__set('_dni', $row['dni']);
                    $objUsuario->__set('_cip', $row['cip']);
                    $objUsuario->__set('_fechaNac', $row['fecha_nac']);
                    $objUsuario->__set('_correo', $row['correo']);
                    $objUsuario->__set('_celular', $row['celular']);
                    $objUsuario->__set('_rpm', $row['rpm']);
                    $objUsuario->__set(
                        '_idPoolAtencion', $row['idpool_atencion']
                    );
                    $objUsuario->__set('_idEmpresa', $row['idempresa']);
                    $objUsuario->__set('_onlineStatus', $row['online_status']);
                    $objUsuario->__set(
                        '_lastDateEnter', $row['last_date_enter']
                    );
                    $objUsuario->__set(
                        '_lastDateSessionActivity',
                        $row['last_date_session_activity']
                    );
                    $objUsuario->__set('_idArea', $row['idarea']);
                    $objUsuario->__set('_idZonal', $row['idzonal']);
                    $objUsuario->__set('_isChangePass', $row['is_change_pass']);
                    $flag = $objUsuario;
                endif;
            endforeach;
            return $flag;
        } catch(PDOException $error) {
            return $error;
            exit();
        }
    }
    
    /**
     * @author Gonzalo Chacaltana Buleje <gchacaltana@gmd.com.pe>
     * @method __validateLogin
     * Valida login ingresado. Devuelve el id del usuario en caso el 
     * parametro $return_id = true,
     * de lo contrario solo trae el numero de registros encontrados.
     * @param object $conexion
     * @param string $login
     * @param bool $return_id
     * @return int / bool
     */
     public function validateLogin($conexion, $login,$returnId="")
     {
        try {
            $login=addslashes($login);
            (int) $total = 0;
            if (!empty($returnId)) {
                if ($returnId==true) {
                    $sql = "SELECT idusuario FROM usuario 
                    WHERE login = '".$login."'";
                }
            } else {
                    $sql = "SELECT count(1) AS 'TotalUser' 
                    FROM usuario WHERE login = '".$login."'";
            }
            foreach ($conexion->query($sql) as $row):
                    if (!empty ($returnId)) {
                        $total=$row['idusuario'];
                    } else {
                        $total=$row['TotalUser'];
                    }
            endforeach;
            return $total;
        } catch(PDOException $error) {
            return $error;
            exit();
        }
     }
     
    /**
     * @author Gonzalo Chacaltana Buleje <gchacaltana@gmd.com.pe>
     * @method __validateDNI
     * Valida el numero de DNI del usuario. Retorna el numero de 
     * registros encontrados.
     * @param object $conexion
     * @param string $dni
     * @return int
     */
    public function validateDNI($conexion, $dni)
    {
        try {
            $dni=addslashes($dni);
            $sql = "SELECT count(1) AS 'TotalUser' FROM usuario 
            WHERE dni = '".$dni."'";
            foreach ($conexion->query($sql) as $row):
                    $total=$row['TotalUser'];
            endforeach;
            return $total;
        } catch(PDOException $error) {
            return $error;
            exit();
        }
    }
    
    /**
     * @author Gonzalo Chacaltana Buleje <gchacaltana@gmd.com.pe>
     * @method __validateEmail
     * Valida el correo electronico del usuario. Retorna el numero de 
     * registros encontrados.
     * @param object $conexion
     * @param string $correo
     * @return int
     */
    public function validateEmail($conexion, $correo)
    {
        try {
            $sql = "SELECT count(1) AS 'TotalUser' FROM usuario 
            WHERE correo = '".$correo."'";
            foreach ($conexion->query($sql) as $row):
                    $total=$row['TotalUser'];
            endforeach;
            return $total;
        } catch(PDOException $error) {
            return $error;
            exit();
        }
    }
    
     /**
     * @author Gonzalo Chacaltana Buleje <gchacaltana@gmd.com.pe>
     * @method ultimoAcceso_Usuario
     * Devuelve el tiempo trascurrido desde su ultima fecha de ingreso 
     * en dias,horas y minutos.
     * @param object $conexion
     * @param int $idUsuario
     * @return string
     */
    public function ultimoAccesoUsuario($conexion,$idUsuario)
    {
        try{
            $fechaActual=date('Y-m-d');
            $horaActual=date("G:i:s");
            
            $fechaUltima = "";
            $horaUltima = "";
            
            $sql="SELECT uss.date_session AS 'Fecha',uss.time_session AS 'Hora'
            FROM usuario_session uss WHERE uss.idusuario=:idusuario 
            ORDER BY idusuario_session DESC LIMIT 1,1";
            
            $bind=$conexion->prepare($sql);
            $bind->bindParam(":idusuario", $idUsuario);
            $bind->execute();
            while ($data=$bind->fetch(PDO::FETCH_ASSOC)) {
                $fechaUltima=$data['Fecha'];
                $horaUltima=$data['Hora'];
            }
        if ($fechaUltima!="") {
        if ($fechaActual==$fechaUltima) {
        $hhA=intval(substr($horaActual, 0, 2));
        $hhU=intval(substr($horaUltima, 0, 2));
        $horasDif=$hhA-$hhU;

        if (intval($horasDif)==0) {
        $mmA=intval(substr($horaActual, 3, -3));
        $mmU=intval(substr($horaUltima, 3, -3));
        $minutosDif=$mmA-$mmU;
        if (intval($minutosDif==1)) {
        return "&Uacute;ltimo Acceso : Hace ".intval($minutosDif)." minuto";
        } else {
        return "&Uacute;ltimo Acceso : Hace ".intval($minutosDif)." minutos";
        }
        } else {
            if (intval($horasDif==1)) {
               return "&Uacute;ltimo Acceso : Hace ".intval($horasDif)." hora";
            } else {
               return "&Uacute;ltimo Acceso : Hace ".intval($horasDif)." horas";
            }
        }
        } else {
            $diasDiferencia=restarFechas($fechaActual, $fechaUltima);
            if ($diasDiferencia>1) {
                return "&Uacute;ltimo Acceso : Hace ".$diasDiferencia." 
                d&iacute;as";
            } else {
                return "&Uacute;ltimo Acceso : Ayer ";
            }
        }
        } else {
                return "Su primer acceso al Sistema";
        }

        }catch(PDOException $error){
            return $error;
            exit();
        }
    }
    
    /**
     * @author Gonzalo Chacaltana Buleje <gchacaltana@gmd.com.pe>
     * @method __modulos_xUsuario
     * Devuelve los modulos que tiene asignado un usuario por los submodulos 
     * relacionados.
     * @param object $conexion
     * @param int $idUsuario
     * @return array
     */
    public function modulosXusuario($conexion,$idUsuario,$isMovil = "")
    {
        try{
            $strWhere = "";
            if ($isMovil=="1") {
                $strWhere.=" AND sm.movil=1";
            } else {
                $strWhere.=" AND sm.web=1";
            }
            
            $sql="SELECT DISTINCT(mo.desc_modulo) AS 'Modulo', 
            mo.idmodulo AS 'idmodulo',mo.url_modulo AS 'url_modulo', 
            mo.detalle AS 'detalle' FROM modulos mo
            INNER JOIN submodulos sm ON mo.idModulo=sm.idModulo
            INNER JOIN usuario_submodulos usm ON usm.idsubmodulo=sm.idsubmodulo
            INNER JOIN usuario us ON us.idusuario=usm.idusuario
            WHERE  us.idusuario=:idusuario AND mo.flag_modulo=1 ".$strWhere." 
            ORDER BY mo.desc_modulo";
            $bind = $conexion->prepare($sql);
            $bind->bindParam(":idusuario", $idUsuario);
            $bind->execute();
            $modulos=array();
            while ($data=$bind->fetch(PDO::FETCH_ASSOC)) {
                $modulos[]=$data;
            }
            return $modulos;
        }catch(PDOException $error){
            return $error;
            exit();
        }
    }
    
    /**
     * Devuelve array de submodulos asignados que tiene el usuario por modulo.
     * @author Gonzalo Chacaltana Buleje <gchacaltana@gmd.com.pe>
     * @method __submodulos_xUsuario_x_Modulo
     * @param object $conexion
     * @param int $idUsuario
     * @param int $id_modulo
     * @return array
     */
    public function submodulosXusuarioXmodulo($conexion,$idUsuario,
            $idModulo,$isMovil = "", $all = "")
    {
        try{
            
            $strWhere = "";
            
            if (empty($all)) {
             
                if ($isMovil=="1") {
                    $strWhere.=" AND s.movil=1";
                } else {
                    $strWhere.=" AND s.web=1";
                }
                
            }
            
            $sql="SELECT DISTINCT(s.idsubmodulo) AS 'idsubmodulo',
            s.desc_submodulo,s.url_submodulo FROM submodulos s
            INNER JOIN modulos mo ON mo.idmodulo=s.idmodulo
            INNER JOIN usuario_submodulos usm ON usm.idsubmodulo=s.idsubmodulo
            WHERE  usm.idusuario=:idusuario AND 
            s.flag_submodulo=1 AND mo.idmodulo=:idmodulo ".$strWhere;

            $bind = $conexion->prepare($sql);
            $bind->bindParam(":idusuario", $idUsuario);
            $bind->bindParam(":idmodulo", $idModulo);
            $bind->execute();
            $submodulos=array();
            while ($data=$bind->fetch(PDO::FETCH_ASSOC)) {
                $submodulos[]=$data;
            }
            return $submodulos;
        }catch(PDOException $error){
            return $error;
            exit();
        }
    }
    
    /**
     * @author Gonzalo Chacaltana Buleje <gchacaltana@gmd.com.pe>
     * @method __submodulos_xUsuario
     * Devuelve array de submodulos asignados que tiene el usuario.
     * @param object $conexion
     * @param int $idUsuario
     * @return array
     */
    public function submodulosXusuario($conexion,$idUsuario)
    {
        try{
            $sql="SELECT DISTINCT(s.idsubmodulo) AS 'idsubmodulo'
            FROM submodulos s
            INNER JOIN modulos mo ON mo.idmodulo=s.idmodulo
            INNER JOIN usuario_submodulos usm ON usm.idsubmodulo=s.idsubmodulo
            WHERE  usm.idusuario=:idusuario AND s.flag_submodulo=1";

            $bind = $conexion->prepare($sql);
            $bind->bindParam(":idusuario", $idUsuario);
            $bind->execute();
            $submodulos=array();
            while ($data=$bind->fetch(PDO::FETCH_ASSOC)) {
                $submodulos[]=$data;
            }
            return $submodulos;
        }catch(PDOException $error){
            return $error;
            exit();
        }
    }
    
    /**
     * @author Gonzalo Chacaltana Buleje <gchacaltana@gmd.com.pe>
     * @method __usuarios_xAdministrador
     * Devuelve array de la lista de usuarios creados por un usuario 
     * administrador.
     * @param object $conexion
     * @param int $idUsuario
     * @param int $pagina
     * @param int $campoOrder;
     * @param string $direction
     * @param array $filtroEmpresa
     * @param array $filtroArea
     * @return array
     */
    public function usuariosXadministrador($conexion,$idUsuario,$pagina='',
            $campoOrder='',$direction='',$filtroEmpresa='',$filtroArea='')
    {
         try{
            
            $strLimite='';
            $strOrder='';
            $strWhere='';

            //Arreglo de campos de ordenamiento
            $arregloCampos=array();
            $arregloCampos[1]="u.login";
            $arregloCampos[2]="u.nombres";
            $arregloCampos[3]="u.idempresa";
            $arregloCampos[4]="u.codperfil";
            if ($campoOrder) {
                $campo=$arregloCampos[$campoOrder];
            }

            //Armando cadena de Limites de registros
            if ($pagina == '') {
                $pagina = 1;
            } else {
                $offset = ($pagina - 1) * TAM_PAG_LISTADO;
                $strLimite = " LIMIT " . $offset . "," . TAM_PAG_LISTADO;
            }

            //Armando cadena de Ordenamiento
            if ($campoOrder=='') {
                $strOrder.=' u.idusuario';
            } else {
                $strOrder.=$campo;
            }

            if ($direction=='') {
                $strOrder.=' ASC';
            } else {
                if ($direction=="asc") {
                    $strOrder.=' ASC';
                } else {
                    if ($direction=="desc") {
                        $strOrder.=' DESC';
                    }
                }
            }
            
            $sql="SELECT  u.idusuario,u.login,u.nombres,u.appat,u.apmat,
            u.estado,u.idperfil,u.idempresa,u.idarea FROM usuario u
            WHERE u.user_creator=:idusuario ORDER BY ".$strOrder.$strLimite;
            
            $bind = $conexion->prepare($sql);
            $bind->bindParam(":idusuario", $idUsuario);
            $bind->execute();
            $usuarios=array();
            while ($data=$bind->fetch(PDO::FETCH_ASSOC)) {
                    $objUsuario = new Data_Usuario();
                    $objUsuario->__set('_idUsuario', $data['idusuario']);
                    $objUsuario->__set('_login', $data['login']);
                    $objUsuario->__set('_appat', $data['appat']);
                    $objUsuario->__set('_apmat', $data['apmat']);
                    $objUsuario->__set('_nombres', $data['nombres']);
                    $objUsuario->__set('_idPerfil', $data['idperfil']);
                    $objUsuario->__set('_estado', $data['estado']);
                    $objUsuario->__set('_idArea', $data['idarea']);
                    $usuarios[] = $objUsuario;
            }
            return $usuarios;
         }catch(PDOException $error){
                return $error;
                exit();
         }
    }

    public function operadoresXarea($conexion,$filtroArea='', $campoOrden = '')
    {
         try{

            if($campoOrden=='') $campoOrden='login';
            $strOrder=' ORDER BY u.'.$campoOrden.' ASC ';
            $strWhere='';

            //Armando cadena de Filtros de busqueda

            $whereEmpresa='';
            $sqlInnerArea='';
            $whereArea='';

            if ($filtroArea !='') {
                $sqlInnerArea.=' INNER JOIN usuario_area ua 
                ON ua.idusuario=u.idusuario';
                $strWhere.=' AND ua.idarea='.$filtroArea;
            }


            $sql="SELECT  u.idusuario,u.login,u.nombres,u.appat,u.apmat,
            u.estado,u.idperfil,u.idempresa FROM usuario u 
            INNER JOIN usuario_area ua ON ua.idusuario=u.idusuario
            WHERE ua.idarea='$filtroArea' AND idperfil='4' 
            AND estado='1' ".$strOrder;

            $bind = $conexion->prepare($sql);
            $bind->execute();
            $usuarios=array();
            while ($data=$bind->fetch(PDO::FETCH_ASSOC)) {
                    $objUsuario = new Data_Usuario();
                    $objUsuario->__set('_idUsuario', $data['idusuario']);
                    $objUsuario->__set('_login', $data['login']);
                    $objUsuario->__set('_appat', $data['appat']);
                    $objUsuario->__set('_apmat', $data['apmat']);
                    $objUsuario->__set('_nombres', $data['nombres']);
                    $objUsuario->__set('_idPerfil', $data['idperfil']);
                    $objUsuario->__set('_estado', $data['estado']);
                    $usuarios[] = $objUsuario;
            }
            return $usuarios;
            
         }catch(PDOException $error){
            return $error;
            exit();
         }
    }

    public function listTotalUsuariosXadministrador($conexion,$idUsuario)
    {
         try{
             
            $sql="SELECT COUNT(1) AS 'Total'
                  FROM usuario u
                  WHERE u.user_creator=:idusuario";

            $bind = $conexion->prepare($sql);
            $bind->bindParam(":idusuario", $idUsuario);
            $bind->execute();
            while ($data=$bind->fetch(PDO::FETCH_ASSOC)) {
                $total=$data['Total'];
            }
            return $total;
         }catch(PDOException $error){
            return $error;
            exit();
         }
    }

    public function usuariosBusquedaXnombreCompleto($conexion,$filtro,
            $tipoUser,$idUsuario)
    {
         try{
            $sqlAdicional='';
            if ($tipoUser=="adm") {
                $sqlAdicional.=' AND u.user_creator='.$idUsuario;
            }
            if ($_SESSION['USUARIO_PERFIL']->__get('_codPerfil')=="spvseg") {
                $sqlAdicional.=" AND idusuario<>1";
            }
            $sql="SELECT u.idusuario,u.login,u.nombres,u.appat,u.apmat,
            u.estado,u.idperfil,u.idempresa FROM usuario u
            WHERE  CONCAT(u.nombres,' ',u.appat,' ',u.apmat) 
            LIKE '%".$filtro."%'".$sqlAdicional;

            $bind = $conexion->prepare($sql);
            $bind->execute();
            $usuarios=array();
            while ($data=$bind->fetch(PDO::FETCH_ASSOC)) {
                    $objUsuario = new Data_Usuario();
                    $objUsuario->__set('_idUsuario', $data['idusuario']);
                    $objUsuario->__set('_login', $data['login']);
                    $objUsuario->__set('_appat', $data['appat']);
                    $objUsuario->__set('_apmat', $data['apmat']);
                    $objUsuario->__set('_nombres', $data['nombres']);
                    $objUsuario->__set('_idPerfil', $data['idperfil']);
                    $objUsuario->__set('_estado', $data['estado']);
                    $objUsuario->__set('_idEmpresa', $data['idempresa']);
                    $usuarios[] = $objUsuario;
            }
            return $usuarios;
         }catch(PDOException $error){
            return $error;
            exit();
         }
    }

    public function usuariosBusquedaXempresaArea($conexion,$filtroEmpresa='',
            $filtroArea='')
    {
         try{
            $whereEmpresa='';
            $sqlInnerArea='';
            $whereArea='';
            $strWhere = "";
            if ($filtroArea !='' && $filtroArea!='-1') {
                $sqlInnerArea.=' INNER JOIN usuario_area ua 
                ON ua.idusuario=u.idusuario';
                $whereArea=' AND ua.idarea='.$filtroArea;
            }


            if ($filtroEmpresa !='' && $filtroEmpresa!='-1') {
                $whereEmpresa=' AND u.idempresa='.$filtroEmpresa;
            }
            
            if ($_SESSION['USUARIO_PERFIL']->__get('_codPerfil')=="spvseg") {
                $strWhere = " AND u.idusuario<>1";
            }

            $sql="SELECT u.idusuario,u.login,u.nombres,u.appat,u.apmat,
            u.estado,u.idperfil,u.idempresa FROM usuario u ".$sqlInnerArea." 
            WHERE 1=1 ".$whereEmpresa.$whereArea.$strWhere;


            $bind = $conexion->prepare($sql);
            $bind->execute();
            $usuarios=array();
            while ($data=$bind->fetch(PDO::FETCH_ASSOC)) {
                    $objUsuario = new Data_Usuario();
                    $objUsuario->__set('_idUsuario', $data['idusuario']);
                    $objUsuario->__set('_login', $data['login']);
                    $objUsuario->__set('_appat', $data['appat']);
                    $objUsuario->__set('_apmat', $data['apmat']);
                    $objUsuario->__set('_nombres', $data['nombres']);
                    $objUsuario->__set('_idPerfil', $data['idperfil']);
                    $objUsuario->__set('_estado', $data['estado']);
                    $objUsuario->__set('_idEmpresa', $data['idempresa']);
                    $usuarios[] = $objUsuario;
            }
            return $usuarios;
         }catch(PDOException $error){
            return $error;
            exit();
         }
    }
    
    public function usuariosBusquedaXcamposUsuario($conexion,$filtro,$tipoUser,
            $idUsuario,$tipoFiltro)
    {
         try{
             $sql='';
             $sqlAdicional='';
            if ($tipoUser=="adm") {
                $sqlAdicional.=' AND u.user_creator='.$idUsuario;
            }
            if ($_SESSION['USUARIO_PERFIL']->__get('_codPerfil')=="spvseg") {
                $sqlAdicional.="  AND idusuario!=1";
            }
            
            switch($tipoFiltro){
                case 'NombreCompleto':
                    $sql="SELECT u.idusuario,u.login,u.nombres,u.appat,u.apmat,
                    u.estado,u.idperfil,u.idempresa,u.idarea FROM usuario u
                    WHERE  CONCAT(u.nombres,' ',u.appat,' ',u.apmat) 
                    LIKE '%".$filtro."%'".$sqlAdicional;
                    break;
                case 'Dni':
                    $sql="SELECT u.idusuario,u.login,u.nombres,u.appat,u.apmat,
                    u.estado,u.idperfil,u.idempresa,u.idarea FROM usuario u
                    WHERE u.dni='".$filtro."' ".$sqlAdicional;
                    break;
                case 'Cip':
                    $sql="SELECT u.idusuario,u.login,u.nombres,u.appat,u.apmat,
                    u.estado,u.idperfil,u.idempresa,u.idarea FROM usuario u
                    WHERE u.cip='".$filtro."' ".$sqlAdicional;
                    break;
                case 'Login':
                    $sql="SELECT u.idusuario,u.login,u.nombres,u.appat,u.apmat,
                    u.estado,u.idperfil,u.idempresa,u.idarea FROM usuario u
                    WHERE u.login='".$filtro."' ".$sqlAdicional;
                    break;
                default:
                    exit();
            }
            
            $bind = $conexion->prepare($sql);
            $bind->execute();
            $usuarios=array();
            while ($data=$bind->fetch(PDO::FETCH_ASSOC)) {
                $objUsuario = new Data_Usuario();
                $objUsuario->__set('_idUsuario', $data['idusuario']);
                $objUsuario->__set('_login', $data['login']);
                $objUsuario->__set('_appat', $data['appat']);
                $objUsuario->__set('_apmat', $data['apmat']);
                $objUsuario->__set('_nombres', $data['nombres']);
                $objUsuario->__set('_idPerfil', $data['idperfil']);
                $objUsuario->__set('_estado', $data['estado']);
                $objUsuario->__set('_idEmpresa', $data['idempresa']);
                $objUsuario->__set('_idArea', $data['idarea']);
                $usuarios[] = $objUsuario;
            }
            return $usuarios;
         }catch(PDOException $error){
            return $error;
            exit();
         }
    }
    
    public function usuariosBusquedaXedad($conexion,$filtroOperador='',
            $filtroEdad='',$tipoUser='',$idUsuario='')
    {
         try{
             
              $sqlAdicional='';
            if ($tipoUser=="adm") {
                $sqlAdicional.=' AND u.user_creator='.$idUsuario;
            }
             
            $sql="SELECT u.idusuario,u.login,u.nombres,u.appat,u.apmat,u.estado,
            u.idperfil,u.idempresa,u.idarea FROM usuario u 
            WHERE fecha_nac IS NOT NULL AND YEAR(fecha_nac)<>2011 
            AND fecha_nac<>'0000-00-00' AND YEAR(NOW())-YEAR(fecha_nac)
            ".$filtroOperador." ".$filtroEdad.$sqlAdicional;
            
            $bind = $conexion->prepare($sql);
            $bind->execute();
            $usuarios=array();
            while ($data=$bind->fetch(PDO::FETCH_ASSOC)) {
                    $objUsuario = new Data_Usuario();
                    $objUsuario->__set('_idUsuario', $data['idusuario']);
                    $objUsuario->__set('_login', $data['login']);
                    $objUsuario->__set('_appat', $data['appat']);
                    $objUsuario->__set('_apmat', $data['apmat']);
                    $objUsuario->__set('_nombres', $data['nombres']);
                    $objUsuario->__set('_idPerfil', $data['idperfil']);
                    $objUsuario->__set('_estado', $data['estado']);
                    $objUsuario->__set('_idEmpresa', $data['idempresa']);
                    $objUsuario->__set('_idArea', $data['idarea']);
                    $usuarios[] = $objUsuario;
            }
            return $usuarios;
         }catch(PDOException $error){
            return $error;
            exit();
         }
    }
    
    public function listadoOrdenadoUsuario($conexion,$pagina='',$campoOrder='',
            $direction='',$filtroEmpresa='',$filtroArea='')
    {
        
        try{
            $strLimite='';
            $strOrder='';
            $strWhere='';

            //Arreglo de campos de ordenamiento
            $arregloCampos=array();
            $arregloCampos[1]="u.login";
            $arregloCampos[2]="u.nombres";
            $arregloCampos[3]="u.idempresa";
            $arregloCampos[4]="u.idperfil";
            if ($campoOrder) {
                $campo=$arregloCampos[$campoOrder];
            }
            
            //Armando cadena de Limites de registros
            if ($pagina == '') {
                $pagina = 1;
            } else {
                $offset = ($pagina - 1) * TAM_PAG_LISTADO;
                $strLimite = " LIMIT " . $offset . "," .TAM_PAG_LISTADO;
            }

            //Armando cadena de Ordenamiento
            if ($campoOrder=='') {
                $strOrder.=' u.idusuario';
            } else {
                $strOrder.=$campo;
            }

            if ($direction=='') {
                $strOrder.=' ASC';
            } else {
                if ($direction=="asc") {
                    $strOrder.=' ASC';
                } else {
                    if ($direction=="desc") {
                        $strOrder.=' DESC';
                    }
                }
            }

            //Armando cadena de Filtros de busqueda

            $whereEmpresa='';
            
            $sqlInnerArea='';
            
            $whereArea='';
            
            if ($filtroArea !='' && $filtroArea!='-1' && $filtroArea!='0') {
                $strWhere.=' AND u.idarea='.$filtroArea;
            }


            if ($filtroEmpresa !='' && $filtroEmpresa!='-1' && 
                    $filtroEmpresa!='0') {
                $strWhere.=' AND u.idempresa='.$filtroEmpresa;
            }
            
            $sqlAdicional='';
            
            if ($_SESSION['USUARIO_PERFIL']->__get('_codPerfil')=="adm") {
                $sqlAdicional=' AND 
                user_creator='.$_SESSION['USUARIO']->__get('_idUsuario');
            }
            
            if ($_SESSION['USUARIO_PERFIL']->__get('_codPerfil')=="spvseg") {
                $sqlAdicional.=" AND u.idusuario<>1";
            }
            
            $sql="SELECT u.idusuario,u.login,u.nombres,u.appat,u.apmat,u.estado,
            u.idperfil,u.idempresa,u.idarea FROM ".$this->_table." u  
            WHERE 1=1 ".$strWhere." ".$sqlAdicional." 
            ORDER BY ".$strOrder." ".$strLimite;
            
            $usuarios = array();
            foreach ($conexion->query($sql) as $row):
                    $objUsuario = new Data_Usuario();
                    $objUsuario->__set('_idUsuario', $row['idusuario']);
                    $objUsuario->__set('_login', $row['login']);
                    $objUsuario->__set('_appat', $row['appat']);
                    $objUsuario->__set('_apmat', $row['apmat']);
                    $objUsuario->__set('_nombres', $row['nombres']);
                    $objUsuario->__set('_idPerfil', $row['idperfil']);
                    $objUsuario->__set('_estado', $row['estado']);
                    $objUsuario->__set('_idEmpresa', $row['idempresa']);
                    $objUsuario->__set('_idArea', $row['idarea']);
                    $usuarios[] = $objUsuario;
            endforeach;
            
            return $usuarios;
            
        }catch(PDOException $error){
            die("Error al listar");
        }
    }
    
    public function listadoOrdenadoUsuarioTotal($conexion,$filtroEmpresa='',
            $filtroArea='')
    {
        try{

            $strWhere='';
            (int) $totalRegistros = 0;
            //Armando cadena de Filtros de busqueda
            $sqlInnerArea='';
            
            if ($filtroArea !='' && $filtroArea!='-1' && $filtroArea!='0') {
                $sqlInnerArea.=' INNER JOIN usuario_area ua 
                ON ua.idusuario=u.idusuario';
                $strWhere.=' AND ua.idarea='.$filtroArea;
            }

            if ($filtroEmpresa !='' && $filtroEmpresa!='-1' && 
                    $filtroEmpresa!='0') {
                $strWhere.=' AND u.idempresa='.$filtroEmpresa;
            }
            
            $sqlAdicional='';
            
            if ($_SESSION['USUARIO_PERFIL']->__get('_codPerfil')=="adm") {
                $sqlAdicional=' AND 
                u.user_creator='.$_SESSION['USUARIO']->__get('_idUsuario');
            }
            
            $sql="SELECT count(1) AS 'Total' 
            FROM usuario u ".$sqlInnerArea." 
            WHERE 1=1 ".$strWhere." ".$sqlAdicional;

            foreach ($conexion->query($sql) as $row):
                    $totalRegistros=$row['Total'];
            endforeach;
            return $totalRegistros;

        }catch(PDOException $error){
            die("Error al contar registros.");
        }
        
    }
    
    /**
     * @author      Gonzalo Chacaltana Buleje
     * @method      listadoUsuariosByFiltro
     * @param       object $conexion
     * @param       int $pagina
     * @param       int $filtroPerfil
     * @return      array
     */
    public function listadoUsuariosByFiltro($conexion,$pagina,$filtroPerfil)
    {
        
        try{
            $strLimite='';
            $strWhere='';

            //Armando cadena de Limites de registros
            if ($pagina == '') {
                $pagina = 1;
            } else {
                $offset = ($pagina - 1) * TAM_PAG_LISTADO;
                $strLimite = " LIMIT " . $offset . "," .TAM_PAG_LISTADO;
            }

            if ($filtroPerfil !='' && $filtroPerfil!='-1' && 
                    $filtroPerfil!='0') {
                $strWhere.=' AND u.idperfil='.$filtroPerfil;
            }
            
            $sql="SELECT u.idusuario,u.login,u.nombres,u.appat,u.apmat,u.estado,
            u.idperfil,u.idempresa,u.idarea FROM ".$this->_table." u  
            WHERE 1=1 ".$strWhere." 
            ORDER BY u.appat ".$strLimite;
            
            $usuarios = array();
            foreach ($conexion->query($sql) as $row):
                    $objUsuario = new Data_Usuario();
                    $objUsuario->__set('_idUsuario', $row['idusuario']);
                    $objUsuario->__set('_login', $row['login']);
                    $objUsuario->__set('_appat', $row['appat']);
                    $objUsuario->__set('_apmat', $row['apmat']);
                    $objUsuario->__set('_nombres', $row['nombres']);
                    $objUsuario->__set('_idPerfil', $row['idperfil']);
                    $objUsuario->__set('_estado', $row['estado']);
                    $objUsuario->__set('_idEmpresa', $row['idempresa']);
                    $objUsuario->__set('_idArea', $row['idarea']);
                    $usuarios[] = $objUsuario;
            endforeach;
            
            return $usuarios;
           
        }catch(PDOException $e){
            die("Error al listar");
        }
        
    }
    
    /**
     * @author      Gonzalo Chacaltana Buleje
     * @method      listadoUsuariosByFiltro_Total
     * @param       object $conexion
     * @param       int $filtroPerfil
     * @return      int
     */
    public function listadoUsuariosByFiltroTotal($conexion,$filtroPerfil)
    {
        try{
            (int) $totalRegistros = 0;
            $strWhere='';

            if ($filtroPerfil !='' && $filtroPerfil!='-1' && 
                    $filtroPerfil!='0') {
                $strWhere.=' AND idperfil='.$filtroPerfil;
            }
            
            $sql="SELECT count(1) AS 'Total' FROM usuario WHERE 1=1 ".$strWhere;
            
            foreach ($conexion->query($sql) as $row):
                    $totalRegistros=$row['Total'];
            endforeach;
            return $totalRegistros;

        }catch(PDOException $error){
            die("Error al contar registros");
        }
        
    }
    
    public function listadoUsuariosForReportePDF($conexion)
    {
        try{
            $sql='';
            
            if ($_SESSION['USUARIO_PERFIL']->__get('_codPerfil')=="spu") {
                
                $sql="SELECT TRIM(CONCAT(u.appat,' ',u.apmat,', ',u.nombres)) 
                AS 'NombreCompleto',u.login AS 'Login',p.perfil AS 'Perfil',
                um.login AS 'Manager',u.dni AS 'Dni',u.cip AS 'Cip',
                u.correo AS 'Correo',u.rpm AS 'Rpm',u.estado AS 'Estado', 
                e.desc_empresa AS 'Empresa' FROM usuario u 
                INNER JOIN perfiles p ON u.idperfil=p.idperfil 
                INNER JOIN usuario um ON u.user_creator=um.idusuario
                INNER JOIN empresas e ON u.idempresa=e.idempresa
                ORDER BY e.desc_empresa,u.appat ASC";
            } else {
                
                $sql="SELECT TRIM(CONCAT(u.appat,' ',u.apmat,', ',u.nombres)) 
                AS 'NombreCompleto',u.login AS 'Login',p.perfil AS 'Perfil',
                u.dni AS 'Dni',u.cip AS 'Cip',u.correo AS 'Correo',
                u.rpm AS 'Rpm',
                IF(u.estado=1,'Habilitado','Deshabilitado') AS 'Estado' 
                FROM usuario u 
                INNER JOIN perfiles p ON u.idperfil=p.idperfil
                INNER JOIN usuario um ON u.user_creator=um.idusuario
                AND u.user_creator=".$_SESSION['USUARIO']->__get('_idUsuario')."
                ORDER BY u.appat ASC";
            }
            
            $bind = $conexion->prepare($sql);
            $bind->execute();
            $usuarios=array();
            while ($data=$bind->fetch(PDO::FETCH_ASSOC)) {
                    $usuarios[] = $data;
            }
            return $usuarios;

        }catch(PDOException $error){
            return $error;
            exit();
        }
    }
    
    public function listadoUsuariosForReporteXLS($conexion)
    {
        try{
            $sql='';
            
            if ($_SESSION['USUARIO_PERFIL']->__get('_codPerfil')=="spu") {
                
                $sql="SELECT u.dni AS 'dni',u.nombres AS 'nombres',
                u.appat AS 'paterno',u.apmat AS 'materno',u.login AS 'login',
                p.perfil AS 'perfil',e.desc_empresa AS 'empresa',u.cip AS 'cip',
                u.correo AS 'correo',um.login AS 'manager',
                u.celular AS 'celular',u.rpm AS 'rpm',
                IF(u.estado=1,'Habilitado','Deshabilitado') AS 'estado'
                FROM usuario u
                INNER JOIN perfiles p ON u.idperfil=p.idperfil
                INNER JOIN usuario um ON u.user_creator=um.idusuario
                INNER JOIN empresas e ON u.idempresa=e.idempresa
                ORDER BY e.desc_empresa,u.appat";
            } else {
                
                $sql="SELECT u.dni AS 'dni',u.nombres AS 'nombres',
                u.appat AS 'paterno',u.apmat AS 'materno',u.login AS 'login',
                p.perfil AS 'perfil',u.cip AS 'cip',u.correo AS 'correo',
                u.celular AS 'celular',u.rpm AS 'rpm',
                IF(u.estado=1,'Habilitado','Deshabilitado') AS 'estado'
                FROM usuario u
                INNER JOIN perfiles p ON u.idperfil=p.idperfil
                WHERE 
                u.user_creator=".$_SESSION['USUARIO']->__get('_idUsuario')."
                ORDER BY u.appat";
            }
            
            $bind = $conexion->prepare($sql);
            $bind->execute();
            $usuarios=array();
            while ($data=$bind->fetch(PDO::FETCH_ASSOC)) {
                    $usuarios[] = $data;
            }
            return $usuarios;

        }catch(PDOException $error){
            return $error;
            exit();
        }
    }
    
    /**
     * @author      Gonzalo Chacaltana Buleje
     * Metodo que retorna lista de usuarios filtrados por perfil en reporte PDF
     * @method      listadoUsuariosForReportePDFByPerfil
     * @param       object $conexion
     * @param       int $perfil
     * @return      array
     */
    public function listadoUsuariosForReportePDFByPerfil($conexion,$perfil)
    {
        try{
        $sql='';
        $sql="SELECT 
        TRIM(CONCAT(u.appat,' ',u.apmat,', ',u.nombres)) AS 'NombreCompleto',
        u.login AS 'Login',p.perfil AS 'Perfil',um.login AS 'Manager',
        u.dni AS 'Dni',u.cip AS 'Cip',u.correo AS 'Correo',u.rpm AS 'Rpm',
        IF(u.estado=0,'Deshabilitado','Habilitado') AS 'Estado', 
        e.desc_empresa AS 'Empresa'
        FROM usuario u 
        INNER JOIN perfiles p ON u.idperfil=p.idperfil
        INNER JOIN usuario um ON u.user_creator=um.idusuario
        INNER JOIN empresas e ON u.idempresa=e.idempresa
        WHERE u.idperfil=".$perfil."
        ORDER BY u.appat ASC";
            
        $bind = $conexion->prepare($sql);
        $bind->execute();
        $usuarios=array();
        while ($data=$bind->fetch(PDO::FETCH_ASSOC)) {
                $usuarios[] = $data;
        }
        return $usuarios;

        }catch(PDOException $error){
            return $error;
            exit();
        }
    }
    /**
     * @author      Gonzalo Chacaltana Buleje
     * Metodo que retorna lista de usuarios filtrados por perfil en reporte XLS
     * @method      listadoUsuariosForReportePDFByPerfil
     * @param       object $conexion
     * @param       int $perfil
     * @return      array
     */
    public function listadoUsuariosForReporteXLSByPerfil($conexion,$perfil)
    {
        try{
            $sql='';
            $sql="SELECT u.dni AS 'dni',u.nombres AS 'nombres',
            u.appat AS 'paterno',u.apmat AS 'materno',u.login AS 'login',
            p.perfil AS 'perfil',e.desc_empresa AS 'empresa',u.cip AS 'cip',
            u.correo AS 'correo',um.login AS 'manager',u.celular AS 'celular',
            u.rpm AS 'rpm',
            IF(u.estado=1,'Habilitado','Deshabilitado') AS 'estado'
            FROM usuario u
            INNER JOIN perfiles p ON u.idperfil=p.idperfil
            INNER JOIN usuario um ON u.user_creator=um.idusuario
            INNER JOIN empresas e ON u.idempresa=e.idempresa
            WHERE u.idperfil=".$perfil."
            ORDER BY u.appat";
                
            $bind = $conexion->prepare($sql);
            $bind->execute();
            $usuarios=array();
            
            while ($data=$bind->fetch(PDO::FETCH_ASSOC)) {
                    $usuarios[] = $data;
            }
            return $usuarios;

        }catch(PDOException $error){
            return $error;
            exit();
        }
    }
    
    /**
     * @author      Gonzalo Chacaltana Buleje
     * Metodo que retorna lista de usuarios filtrados por perfil en reporte PDF
     * @method      listadoUsuariosForReportePDFByChk
     * @param       object $conexion
     * @param       int $filtroChk
     * @return      array
     */
    public function listadoUsuariosForReportePDFByChk($conexion,$filtroChk)
    {
        try{
            if ($filtroChk) {
                $sql = "SELECT 
                TRIM(CONCAT(u.appat,' ',u.apmat,', ',u.nombres)) 
                AS 'NombreCompleto',u.login AS 'Login',p.perfil AS 'Perfil',
                um.login AS 'Manager',u.dni AS 'Dni',u.cip AS 'Cip',
                u.correo AS 'Correo',u.rpm AS 'Rpm',
                IF(u.estado=0,'Deshabilitado','Habilitado') AS 'Estado', 
                e.desc_empresa AS 'Empresa'
                FROM usuario u 
                INNER JOIN perfiles p ON u.idperfil=p.idperfil
                INNER JOIN usuario um ON u.user_creator=um.idusuario
                INNER JOIN empresas e ON u.idempresa=e.idempresa";
                
                switch($filtroChk){
                    
                    case 'enabled':
                        //Filtra listado de usuarios por usuarios 
                        //deshabilitados.
                         $sql .= " WHERE u.estado=0";
                        break;
                    case '90':
                        //Filtra listado de usuarios, quienes no hayan 
                        //ingresado al sistemas hace mas de 90 dias.
                        $sql .= " WHERE 
                        TIMESTAMPDIFF(DAY,u.last_date_enter,NOW())>90";
                   case 'never':
                       //Filtra listado de usuarios , quienes nunca han 
                       //ingresado al sistema.
                       $sql .= " WHERE u.last_date_enter IS NULL 
                       AND u.last_date_session_activity IS NULL";
                }
                $sql .=" ORDER BY u.appat";
                
                $bind = $conexion->prepare($sql);
                $bind->execute();
                $usuarios=array();
                while ($data=$bind->fetch(PDO::FETCH_ASSOC)) {
                        $usuarios[] = $data;
                }
                return $usuarios;
                
            } else {
                throw new Exception("Parametro invalido");exit;
            }
        }catch(PDOException $error){
            return $error;
            exit();
        }
    }
    
    public function changeStatusOnline($conexion, $idUsuario, $status)
    {
        try {
            if ($status==1) {
                $sql = "UPDATE ".$this->_table." 
                SET online_status = :status,last_date_enter=NOW(),
                last_date_close_session=NULL, last_date_session_activity=NULL 
                WHERE idusuario = :idusuario";
            } else if ($status==0) {
                $sql = "UPDATE ".$this->_table." 
                SET online_status = :status,last_date_close_session=NOW() 
                WHERE idusuario = :idusuario";
            }
            
            $bind = $conexion->prepare($sql);
            $bind->bindParam(":idusuario", $idUsuario);
            $bind->bindParam(":status", $status);
            $bind->execute();
        } catch(PDOException $error) {
            return $error;
            exit();
        }
    }
    public function changeDateSessionActivity($conexion, $idUsuario)
    {
        try {

            $sql = "UPDATE ".$this->_table." 
            SET last_date_session_activity=NOW()
            WHERE idusuario = :idusuario";
            $bind = $conexion->prepare($sql);
            $bind->bindParam(":idusuario", $idUsuario);
            $bind->execute();
        } catch(PDOException $error) {
            return $error;
            exit();
        }
    }
    public function changeDateCloseSession($conexion, $idUsuario)
    {
        try {

            $sql = "UPDATE ".$this->_table." 
            SET last_date_close_session=NOW() WHERE idusuario = :idusuario";
            $bind = $conexion->prepare($sql);
            $bind->bindParam(":idusuario", $idUsuario);
            $bind->execute();
        } catch(PDOException $error) {
            return $error;
            exit();
        }
    }
    
    public function clearDateCloseSession($conexion, $idUsuario)
    {
        try {

            $sql = "UPDATE ".$this->_table." 
            SET last_date_close_session=NULL WHERE idusuario = :idusuario";
            $bind = $conexion->prepare($sql);
            $bind->bindParam(":idusuario", $idUsuario);
            $bind->execute();
        } catch(PDOException $error) {
            return $error;
            exit();
        }
    }
  
    public function diffTimeSessionActivity($conexion, $idUsuario)
    {
        try {
            (int) $minutosInactividad = 0;
            $sql = "SELECT 
            TIMESTAMPDIFF(MINUTE,last_date_session_activity,NOW()) 
            AS minutos_inactividad FROM webunificada.usuario 
            WHERE idusuario =:idusuario";
            $bind = $conexion->prepare($sql);
            $bind->bindParam(":idusuario", $idUsuario);
            $bind->execute();
            while ($data=$bind->fetch(PDO::FETCH_ASSOC)) {
                $minutosInactividad=$data['minutos_inactividad'];
            }
            if ($minutosInactividad<10) {
                return true;
            } else {
                return false;
            }
            
        } catch(PDOException $error) {
            return $error;
            exit();
        }
    }
    
    public function getLastIpConnection($conexion, $idUsuario)
    {
        try {

            $sql = "SELECT ip_session FROM usuario_session 
            WHERE idusuario =:idusuario 
            ORDER BY idusuario_session 
            DESC LIMIT 1";
            $bind = $conexion->prepare($sql);
            $bind->bindParam(":idusuario", $idUsuario);
            $bind->execute();
            while ($data=$bind->fetch(PDO::FETCH_ASSOC)) {
                $ip=$data['ip_session'];
            }
            return $ip;
            
        } catch(PDOException $error) {
            return $error;
            exit();
        }
    }
    
    public function getManagerByEmpresa($conexion, $idEmpresa)
    {
        try {

            $sql = "SELECT idusuario,login,nombres,appat,apmat 
            FROM ".$this->_table." WHERE idperfil=2 
            AND idempresa =:idempresa ORDER BY appat";
            $bind = $conexion->prepare($sql);
            $bind->bindParam(":idempresa", $idEmpresa);
            $bind->execute();
            $array=array();
            while ($data=$bind->fetch(PDO::FETCH_ASSOC)) {
                $array[]=$data;
            }
            return $array;
            
        } catch(PDOException $error) {
            return $error;
            exit();
        }
    }
    public function getLastDateActivity($conexion, $idUsuario)
    {
        try {
            (string) $lastDateActivity = "";
            $sql = "SELECT last_date_session_activity 
            FROM usuario 
            WHERE idusuario =:idusuario";
            $bind = $conexion->prepare($sql);
            $bind->bindParam(":idusuario", $idUsuario);
            $bind->execute();
            while ($data=$bind->fetch(PDO::FETCH_ASSOC)) {
                $lastDateActivity=$data['last_date_session_activity'];
            }
            return $lastDateActivity;
            
        } catch(PDOException $error) {
            return $error;
            exit();
        }
    }
    
    /**
     * @author Gonzalo Chacaltana Buleje <gchacaltana@gmd.com.pe>
     * [Descripcion ] :: Metodo que devuelve un array de las empresas por 
     * los perfiles de usuarios registrados(idempresa,desc_empresa).
     * 
     * @param string $conexion
     */
    public function getEmpresasXusuariosPerfil($conexion,$arrayPerfiles)
    {
        try{
            $strWhere='';
            if (!empty($arrayPerfiles) && is_array($arrayPerfiles)) {
                if (count($arrayPerfiles)>1) {
                    $con=0;
                    $strWhere.=" AND u.idperfil IN (";
                    foreach ($arrayPerfiles as $idPerfil) {
                        $con++;
                        if (count($arrayPerfiles)==$con) {
                            $strWhere.=$idPerfil.")";
                        } else {
                            $strWhere.=$idPerfil.",";
                        }
                    }
                } else {
                    $strWhere.=" AND u.idperfil='".$arrayPerfiles[0]."'";
                }
            }
            $sql='SELECT DISTINCT(e.idempresa) AS idempresa,
            e.desc_empresa AS desc_empresa FROM usuario u 
            INNER JOIN empresas e ON e.idempresa=u.idempresa
            WHERE 1=1 '.$strWhere.'
            ORDER BY 2';
            $empresas = array();
            $stmSql = $conexion->prepare($sql);
            //$stm_sql->bindParam(":idempresa", $idEmpresa);
            $stmSql->execute();
            if ($stmSql) {
                while ($data = $stmSql->fetch(PDO::FETCH_ASSOC)) {
                    $empresas[]=$data;
                }
            return $empresas;
            }
            unset ($sql,$stmSql,$data);
        }catch(PDOException $e){ 
            return $e->getMessage();exit(0);
        }
    }
    /**
     * @author Gonzalo Chacaltana Buleje <gchacaltana@gmd.com.pe>
     * [Descripcion ] :: Metodo que devuelve un array de las empresas 
     * registradas en los usuarios(idempresa,desc_empresa).
     * 
     * @param string $conexion
     */
    public function getEmpresasXusuarios($conexion)
    {
        try{
            $sql='SELECT DISTINCT(e.idempresa) AS idempresa,
            e.desc_empresa AS desc_empresa FROM usuario u
            INNER JOIN empresas e ON u.idempresa=e.idempresa
            ORDER BY 2';
            $empresas = array();
            $stmSql = $conexion->prepare($sql);
            $stmSql->execute();
            if ($stmSql) {
                while ($data = $stmSql->fetch(PDO::FETCH_ASSOC)) {
                    $empresas[]=$data;
                }
            return $empresas;
            }
            unset ($sql,$stmSql,$data);
        }catch(PDOException $e){ 
            return $e->getMessage();exit(0);
        }
    }
    /**
     * @author Gonzalo Chacaltana Buleje <gchacaltana@gmd.com.pe>
     * [Descripcion ] :: Metodo que devuelve un array de las zonales 
     * registradas en los usuarios(idzonal,desc_zonal).
     * 
     * @param string $conexion
     */
    public function getZonalesXusuariosEmpresa($conexion,$arrayEmpresas,
            $arrayPerfiles)
    {
        try{
            $strWhere='';
            //filtrando por perfiles
            if (!empty($arrayPerfiles) && is_array($arrayPerfiles)) {
                if (count($arrayPerfiles)>1) {
                    $con=0;
                    $strWhere.=" AND u.idperfil IN (";
                    foreach ($arrayPerfiles as $idPerfil) {
                        $con++;
                        if (count($arrayPerfiles)==$con) {
                            $strWhere.=$idPerfil.")";
                        } else {
                            $strWhere.=$idPerfil.",";
                        }
                    }
                } else {
                    $strWhere.=" AND u.idperfil='".$arrayPerfiles[0]."'";
                }
            }
            
            //filtrando por empresas
            if (!empty($arrayEmpresas) && is_array($arrayEmpresas)) {
                if (count($arrayEmpresas)>1) {
                    $con=0;
                    $strWhere.=" AND u.idempresa IN (";
                    foreach ($arrayEmpresas as $idEmpresa) {
                        $con++;
                        if (count($arrayEmpresas)==$con) {
                            $strWhere.=$idEmpresa.")";
                        } else {
                            $strWhere.=$idEmpresa.",";
                        }
                    }
                } else {
                    $strWhere.=" AND u.idempresa='".$arrayEmpresas[0]."'";
                }
            }

            $sql='SELECT DISTINCT(z.idzonal) AS idzonal,
            z.desc_zonal AS desc_zonal FROM usuario u
            INNER JOIN zonal z ON u.idzonal=z.idzonal
            WHERE 1=1 '.$strWhere.'
            ORDER BY 2';
            $zonales = array();
            $stmSql = $conexion->prepare($sql);
            
            $stmSql->execute();
            if ($stmSql) {
                while ($data = $stmSql->fetch(PDO::FETCH_ASSOC)) {
                    $zonales[]=$data;
                }
            return $zonales;
            }
            unset ($sql,$stmSql,$data);
        } catch(PDOException $e) {
            return $e->getMessage();exit(0);
        }
    }
    /**
     * @author Gonzalo Chacaltana Buleje <gchacaltana@gmd.com.pe>
     * [Descripcion ] :: Metodo que devuelve un array de las areas registradas 
     * en los usuarios por empresa y zonal(idarea,desc_area).
     * 
     * @param string $conexion
     */
    public function getAreasXusuariosXzonalesXempresa($conexion,
            $arrayEmpresas, $arrayZonales, $arrayPerfiles)
    {
        try{
            
            $strWhere='';
            
            //filtrando por perfiles
            if (!empty($arrayPerfiles) && is_array($arrayPerfiles)) {
                if (count($arrayPerfiles)>1) {
                    $con=0;
                    $strWhere.=" AND u.idperfil IN (";
                    foreach ($arrayPerfiles as $idPerfil) {
                        $con++;
                        if (count($arrayPerfiles)==$con) {
                            $strWhere.=$idPerfil.")";
                        } else {
                            $strWhere.=$idPerfil.",";
                        }
                    }
                } else {
                    $strWhere.=" AND u.idperfil='".$arrayPerfiles[0]."'";
                }
            }
            
            //filtrando por empresas
            if (!empty($arrayEmpresas) && is_array($arrayEmpresas)) {
                if (count($arrayEmpresas)>1) {
                    $con=0;
                    $strWhere.=" AND u.idempresa IN (";
                    foreach ($arrayEmpresas as $idEmpresa) {
                        $con++;
                        if (count($arrayEmpresas)==$con) {
                            $strWhere.=$idEmpresa.")";
                        } else {
                            $strWhere.=$idEmpresa.",";
                        }
                    }
                } else {
                    $strWhere.=" AND u.idempresa='".$arrayEmpresas[0]."'";
                }
            }
            
            //filtrando por zonales
            if (!empty($arrayZonales) && is_array($arrayZonales)) {
                if (count($arrayZonales)>1) {
                    $con=0;
                    $strWhere.=" AND u.idzonal IN (";
                    foreach ($arrayZonales as $idZonal) {
                        $con++;
                        if (count($arrayZonales)==$con) {
                            $strWhere.=$idZonal.")";
                        } else {
                            $strWhere.=$idZonal.",";
                        }
                    }
                } else {
                    $strWhere.=" AND u.idzonal='".$arrayZonales[0]."'";
                }
            }
            
            $sql='SELECT DISTINCT(a.idarea) AS idarea,a.desc_area AS desc_area 
            FROM usuario u
            INNER JOIN areas a ON u.idarea=a.idarea
            WHERE 1=1 '.$strWhere.'
            ORDER BY 2';
            $areas = array();
            $stmSql = $conexion->prepare($sql);
            $stmSql->execute();
            if ($stmSql) {
                while ($data = $stmSql->fetch(PDO::FETCH_ASSOC)) {
                    $areas[]=$data;
                }
            return $areas;
            }
            unset ($sql,$stmSql,$data);
        }catch(PDOException $e){ 
            return $e->getMessage();exit(0);
        }
    }
    /**
     * @author Gonzalo Chacaltana Buleje <gchacaltana@gmd.com.pe>
     * @method __getUsuariosConnectOnline
     * [Descripcion ] :: Metodo que devuelve lista de usuarios conectados 
     * en este momento.
     * 
     * @param string $conexion
     * @param int $idEmpresa
     * @param int $idzonal
     * @param int $idArea
     * @return array
     */
    public function getUsuariosConnectOnline($conexion,$idEmpresa,$idzonal,
            $idArea)
    {
        try{
            $strWhere = "";
            
            if (!empty ($idEmpresa)) {
                $strWhere.=" AND idempresa=".$idEmpresa;
            }
            
            if (!empty ($idzonal)) {
                $strWhere.=" AND idzonal=".$idzonal;
            }
            
            if (!empty ($idArea)) {
                $strWhere.=" AND idarea=".$idArea;
            }
            
            $sql = "SELECT idusuario,login,
            CONCAT(nombres,' ',appat,' ',apmat) AS usuario,last_date_enter,
            last_date_session_activity FROM usuario 
            WHERE online_status=1 ".$strWhere;
            
            $usuariosConectados = array();
            $stmSql = $conexion->prepare($sql);
            $stmSql->execute();
            if ($stmSql) {
                while ($data = $stmSql->fetch(PDO::FETCH_ASSOC)) {
                    $usuariosConectados[]=$data;
                }
                return $usuariosConectados;
            } else {
                return 0;
            }
        }catch(PDOException $e){
            return $e->getMessage();exit(0);
        }
    }
}
