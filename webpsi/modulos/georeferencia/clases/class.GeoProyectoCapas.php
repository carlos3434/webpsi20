<?php

class GeoProyectoCapas {

    protected $_db = 'webpsi';
    protected $_table = 'geo_proyecto_capas';

    public function getProjectLayer($conexion, $id) {
        try {

            $sql = "SELECT 
                        id, geo_proyecto_id, layer, layer_id,
                        tipo, origen, estilo, estilo_activo   
                    FROM "
                    . $this->_db 
                    . "."
                    . $this->_table
                    . " WHERE geo_proyecto_id=$id";

            $bind = $conexion->prepare($sql);
            $bind->execute();

            $reporte = array();
            while ($data = $bind->fetch(PDO::FETCH_ASSOC)) {
                $reporte[] = $data;
            }
            return $reporte;
        } catch (PDOException $error) {
            exit();
        }
    }

}
