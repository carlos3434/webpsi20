<?php

/**
 * @package     class/data/
 * @name        class.RegistroTrabajoPlanta.php
 * @category    Model
 * @author      Wilder Sandoval Huamani <wsandoval@gmd.com.pe>
 * @copyright   2011 Telefonica del Peru
 * @version     1.0 - 2011/10/15
 */

class Data_DunaAdsl
{
    protected $_table = 'webunificada_fftt.duna_adsl';   
    protected $_idDuna           = 0;
    protected $_item             = 0;
    protected $_zonal            = '';
    protected $_departamento     = '';
    protected $_provincia        = '';
    protected $_ciudad           = '';
    protected $_distrito         = '';
    protected $_direccion        = '';
    protected $_fftt             = '';
    protected $_direccionReferencial    = '';
    protected $_telefonoReferencial     = '';
    protected $_telefonoDetectado       = '';
    protected $_filtroLinea     = '';
    protected $_codigo           = '';
    protected $_estatus          = '';
    protected $_tipo             = 'EMISOR';
    protected $_expediente       = '';
    protected $_version          = '';
    protected $_fechaDeteccion  = '';
    protected $_fechaExpediente = '';
    protected $_asociado         = '';
    protected $_fotoCorrecta    = '';
    protected $_estado           = '';
    protected $_observaciones    = '';
    protected $_fechaInsert     = '';
    protected $_usuarioInsert   = 0;
    protected $_fechaUpdate     = '';
    protected $_usuarioUpdate   = 0;
    protected $_indDuna         = '';
    protected $_estadoDuna      = '';
    
    public function __construct()
    {
    }

    public function __get($propiedad)
    {
        $returnValue = (string) '';
        $returnValue = $this->$propiedad;
        return (string) $returnValue;
    }

    public function __set($propiedad, $valor)
    {
        $this->$propiedad = $valor;
    }
    
    public function listarDepartamentos($conexion)
    {
        try {

            $sql = "SELECT DISTINCT departamento FROM " . $this->_table . " 
                    ORDER BY departamento";
            $array = array();
            foreach ($conexion->query($sql) as $row):
                if($row['departamento'] != ''):
                    $objDuna = new Data_DunaAdsl();
                    $objDuna->__set('_departamento', $row['departamento']);
                    $array[] = $objDuna;
                endif;
            endforeach;
            
            return $array;
        } catch(PDOException $e) {
            throw $e;
        }
    }
    
    public function listarProvincias($conexion, $departamento)
    {
        try {

            $sql = "SELECT DISTINCT provincia FROM " . $this->_table . " 
                    WHERE departamento = '" . $departamento . "' 
                    ORDER BY provincia";
            $array = array();
            foreach ($conexion->query($sql) as $row):
                if($row['provincia'] != ''):
                    $objDuna = new Data_DunaAdsl();
                    $objDuna->__set('_provincia', $row['provincia']);
                    $array[] = $objDuna;
                endif;
            endforeach;
            
            return $array;
        } catch(PDOException $e) {
            throw $e;
        }
    }
    
    public function listarDistritos($conexion, $departamento, $provincia)
    {
        try {

            $sql = "SELECT DISTINCT distrito FROM " . $this->_table . " 
                    WHERE departamento = '" . $departamento . "' 
                    AND provincia = '" . $provincia . "' 
                    ORDER BY distrito";
            $array = array();
            foreach ($conexion->query($sql) as $row):
                if($row['distrito'] != ''):
                    $objDuna = new Data_DunaAdsl();
                    $objDuna->__set('_distrito', $row['distrito']);
                    $array[] = $objDuna;
                endif;
            endforeach;
            
            return $array;
        } catch(PDOException $e) {
            throw $e;
        }
    }
    
    public function buscarPirataPorXY($conexion, $x, $y, $distancia=10, 
            $nroPiratas=10)
    {
        try {
            
            $sql = "SELECT duna.idduna, duna.item, duna.departamento, 
                    duna.provincia, duna.ciudad, duna.distrito, 
                    duna.direccion, duna.direccion_referencial, duna.foto1, 
                    duna.telefono_referencial, duna.filtro_linea, 
                    duna.telefono_detectado, duna.tipo, 
                    duna.fftt, duna.codigo, duna.estatus, duna.x, duna.y, 
                    duna.ind_duna, estado.nombre as estado_pirata, 
                    ( 6371 * ACOS( COS( RADIANS(" . $y. ") ) * COS( RADIANS( duna.y ) ) * 
                    COS( RADIANS( duna.x ) - RADIANS(" . $x. ") ) + SIN( RADIANS(" . $y. ") ) * 
                    SIN( RADIANS( duna.y ) ) ) ) AS distance 
                    FROM " . $this->_table . " duna, 
                    webunificada_fftt.duna_estado estado
                    WHERE duna.estado = estado.idestado 
                    AND duna.ind_duna = 'S' 
                    AND estado.ind_estado = 'S' 
                    HAVING distance < " . $distancia . " 
                    ORDER BY distance LIMIT 0," . $nroPiratas . "";
            $array = array();
            foreach ($conexion->query($sql) as $row):
                $array[] = $row;
            endforeach;
            return $array;
        } catch (PDOException $e) {
            throw $e;
        }    
    }
    
    public function listarDunaAdslMapa($conexion, $departamento, $provincia='',
            $distrito='', $datosXY='')
    {
        try {
            $strWhere = "";
            if ( $provincia != '' ) {
                $strWhere .= " AND duna.provincia = '" . $provincia . "' ";
            }
            if ( $distrito!= '' ) {
                $strWhere .= " AND duna.distrito = '" . $distrito . "' ";
            }
            if ( $datosXY != '' ) {
                if ( $datosXY == 'conXY' ) {
                    $strWhere .= " AND duna.x IS NOT NULL AND duna.y IS NOT NULL ";
                } elseif ( $datosXY == 'sinXY' ) {
                    $strWhere .= " AND duna.x IS NULL AND duna.y IS NULL ";
                }
            }

            $sql = "SELECT duna.idduna, duna.item, duna.departamento, 
                    duna.provincia, duna.ciudad, duna.distrito, duna.direccion, 
                    duna.direccion_referencial, duna.telefono_referencial, 
                    duna.filtro_linea, duna.telefono_detectado, duna.tipo, 
                    duna.fftt, duna.codigo, duna.estatus, duna.x, duna.y, 
                    duna.ind_duna, duna.estado, estado.nombre as estado_pirata 
                    FROM " . $this->_table . " duna, 
                    webunificada_fftt.duna_estado estado
                    WHERE duna.estado = estado.idestado 
                    AND duna.departamento = '" . $departamento . "' 
                    " . $strWhere . " 
                    AND duna.filtro_linea != '0' 
                    AND duna.ind_duna = 'S' 
                    AND estado.ind_estado = 'S' 
                    ORDER BY duna.codigo";
            $array = array();
            foreach ($conexion->query($sql) as $row):
                if($row['idduna'] != ''):
                    $array[] = $row;
                endif;
            endforeach;
            
            return $array;
        } catch(PDOException $e) {
            throw $e;
        }
    }
    
    public function updateXYDunaAdsl($conexion, $idDuna, $x, $y, $usuarioUpdate)
    {
        try {

            $sql = "UPDATE " . $this->_table .  "  
                    SET x = :x, 
                    y = :y, 
                    usuario_update = :usuario_update, 
                    fecha_update = now() 
                    WHERE idduna = :idduna";
            $bind = $conexion->prepare($sql);
            $bind->bindParam(":x", $x);
            $bind->bindParam(":y", $y);
            $bind->bindParam(":usuario_update", $usuarioUpdate);
            $bind->bindParam(":idduna", $idDuna);
            $bind->execute();
            
            return 1;
        }catch(PDOException $e) {
            return 0;
        }
    }
    
    public function updateEstadoDunaAdsl($conexion, $idDuna, $estado, $usuarioUpdate)
    {
        try {

            $sql = "UPDATE " . $this->_table .  "  
                    SET estado = :estado, 
                    usuario_update = :usuario_update, 
                    fecha_update = now() 
                    WHERE idduna = :idduna";
            $bind = $conexion->prepare($sql);
            $bind->bindParam(":estado", $estado);
            $bind->bindParam(":usuario_update", $usuarioUpdate);
            $bind->bindParam(":idduna", $idDuna);
            $bind->execute();
            
            return 1;
        }catch(PDOException $e) {
            throw $e;
        }
    }
    
    public function listarDunaAdsl($conexion, $pagina='', $indDuna='', $arrFiltro=array())
    {
        try {
            $strLimite = '';
            $strWhere = " AND filtro_linea != '0' ";
            if ($pagina == '') {
                $pagina = 1;
            } else {
                $offset = ($pagina - 1) * TAM_PAG_LISTADO;
                $strLimite = " LIMIT " . $offset . "," . TAM_PAG_LISTADO;
            }
            
            if ($indDuna != '') {
                $strWhere .= " AND duna.ind_duna = '".$indDuna."'";
            }
            
            //datos del filtro
            if ( !empty($arrFiltro) ) {
                if ( isset($arrFiltro['tipo']) && $arrFiltro['tipo'] != '' ) {
                    $strWhere .= " AND duna.tipo = '" . $arrFiltro['tipo'] . "' ";
                }
                if ( isset($arrFiltro['estado']) && $arrFiltro['estado'] != '' ) {
                    $strWhere .= " AND duna.estado = '" . $arrFiltro['estado'] . "' ";
                }
                if ( isset($arrFiltro['foto']) && $arrFiltro['foto'] != '' ) {
                    if ( $arrFiltro['foto'] == 'S' ) {
                        $strWhere .= " AND duna.foto1 != '' ";
                    } elseif ( $arrFiltro['foto'] == 'N' ) {
                        $strWhere .= " AND (duna.foto1 IS NULL OR duna.foto1 = '') ";
                    }
                }
                if ( isset($arrFiltro['campo_filtro']) && $arrFiltro['campo_filtro'] != '' ) {
                    if ( $arrFiltro['campo_filtro'] == 'f_direccion' ) {
                        if ( isset($arrFiltro['campo_valor']) && $arrFiltro['campo_valor'] != '' ) {
                            $strWhere .= " AND duna.direccion LIKE '%" . $arrFiltro['campo_valor'] . "%' ";
                        }
                    }
                }
                
                if ( isset($arrFiltro['departamento']) && $arrFiltro['departamento'] != '' ) {
                    $strWhere .= " AND duna.coddpto = '" . $arrFiltro['departamento'] . "' ";
                }
                if ( isset($arrFiltro['provincia']) && $arrFiltro['provincia'] != '' ) {
                    $strWhere .= " AND duna.codprov = '" . $arrFiltro['provincia'] . "' ";
                }
                if ( isset($arrFiltro['distrito']) && $arrFiltro['distrito'] != '' ) {
                    $strWhere .= " AND duna.coddist = '" . $arrFiltro['distrito'] . "' ";
                }
            }

            $sql = "SELECT duna.*, est.nombre as estado_duna 
                    FROM " . $this->_table . " duna, 
                    webunificada_fftt.duna_estado est
                    WHERE duna.estado = est.idestado  
                    " . $strWhere . " 
                    ORDER BY duna.fecha_insert DESC " . $strLimite;
            $array = array();
            foreach ($conexion->query($sql) as $row):
                if($row['idduna'] != ''):
                    $objDuna = new Data_DunaAdsl();
                    $objDuna->__set('_idDuna', $row['idduna']);
                    $objDuna->__set('_item', $row['item']);
                    $objDuna->__set('_zonal', $row['zonal']);
                    $objDuna->__set('_departamento', $row['departamento']);
                    $objDuna->__set('_provincia', $row['provincia']);
                    $objDuna->__set('_ciudad', $row['ciudad']);
                    $objDuna->__set('_distrito', $row['distrito']);
                    $objDuna->__set('_direccion', $row['direccion']);
                    $objDuna->__set('_direccionReferencial', $row['direccion_referencial']);
                    $objDuna->__set('_telefonoDetectado', $row['telefono_detectado']);
                    $objDuna->__set('_filtroLinea', $row['filtro_linea']);
                    $objDuna->__set('_codigo', $row['codigo']);
                    $objDuna->__set('_estatus', $row['estatus']);
                    $objDuna->__set('_tipo', $row['tipo']);
                    $objDuna->__set('_x', $row['x']);
                    $objDuna->__set('_y', $row['y']);
                    $objDuna->__set('_foto1', $row['foto1']);
                    $objDuna->__set('_fechaInsert', $row['fecha_insert']);
                    $objDuna->__set('_asociado', $row['asociado']);
                    $objDuna->__set('_estado', $row['estado']);
                    $objDuna->__set('_estadoDuna', $row['estado_duna']);
                    $array[] = $objDuna;
                endif;
            endforeach;
            
            return $array;
        } catch(PDOException $e) {
            throw $e;
        }
    }
    
    public function totalDunaAdsl($conexion, $arrFiltro=array())
    {
        try {
 
            $strWhere = " AND filtro_linea != '0' ";
            
            //datos del filtro
            if ( !empty($arrFiltro) ) {
                if ( isset($arrFiltro['tipo']) && $arrFiltro['tipo'] != '' ) {
                    $strWhere .= " AND tipo = '" . $arrFiltro['tipo'] . "' ";
                }
                if ( isset($arrFiltro['estado']) && $arrFiltro['estado'] != '' ) {
                    $strWhere .= " AND estado = '" . $arrFiltro['estado'] . "' ";
                }
                if ( isset($arrFiltro['foto']) && $arrFiltro['foto'] != '' ) {
                    if ( $arrFiltro['foto'] == 'S' ) {
                        $strWhere .= " AND foto1 != '' ";
                    } elseif ( $arrFiltro['foto'] == 'N' ) {
                        $strWhere .= " AND (foto1 IS NULL OR foto1 = '') ";
                    }
                }
                if ( isset($arrFiltro['campo_filtro']) && $arrFiltro['campo_filtro'] != '' ) {
                    if ( $arrFiltro['campo_filtro'] == 'f_direccion' ) {
                        if ( isset($arrFiltro['campo_valor']) && $arrFiltro['campo_valor'] != '' ) {
                            $strWhere .= " AND direccion LIKE '%" . $arrFiltro['campo_valor'] . "%' ";
                        }
                    }
                }
                
                if ( isset($arrFiltro['departamento']) && $arrFiltro['departamento'] != '' ) {
                    $strWhere .= " AND coddpto = '" . $arrFiltro['departamento'] . "' ";
                }
                if ( isset($arrFiltro['provincia']) && $arrFiltro['provincia'] != '' ) {
                    $strWhere .= " AND codprov = '" . $arrFiltro['provincia'] . "' ";
                }
                if ( isset($arrFiltro['distrito']) && $arrFiltro['distrito'] != '' ) {
                    $strWhere .= " AND coddist = '" . $arrFiltro['distrito'] . "' ";
                }
            }
            
            $sql = "SELECT count(1) AS 'Total' 
                    FROM " . $this->_table . " 
                    WHERE 1=1 " . $strWhere . "";
            foreach ($conexion->query($sql) as $row):
                $totalRegistros=$row['Total'];
            endforeach;
            
            return $totalRegistros;
        } catch(PDOException $e) {
            throw $e;
        }
    }
    
    public function obtenerDunaAdsl($conexion, $idDuna)
    {
        try {

            $sql = "SELECT * 
                    FROM " . $this->_table . " 
                    WHERE idduna = '" . $idDuna . "'";
            $array = array();
            foreach ($conexion->query($sql) as $row):
                if($row['idduna'] != ''):
                    $objDuna = new Data_DunaAdsl();
                    $objDuna->__set('_idDuna', $row['idduna']);
                    $objDuna->__set('_item', $row['item']);
                    $objDuna->__set('_zonal', $row['zonal']);
                    $objDuna->__set('_departamento', $row['departamento']);
                    $objDuna->__set('_provincia', $row['provincia']);
                    $objDuna->__set('_ciudad', $row['ciudad']);
                    $objDuna->__set('_distrito', $row['distrito']);
                    $objDuna->__set('_coddpto', $row['coddpto']);
                    $objDuna->__set('_codprov', $row['codprov']);
                    $objDuna->__set('_coddist', $row['coddist']);
                    $objDuna->__set('_direccion', $row['direccion']);
                    $objDuna->__set('_direccionReferencial', $row['direccion_referencial']);
                    $objDuna->__set('_telefonoDetectado', $row['telefono_detectado']);
                    $objDuna->__set('_filtroLinea', $row['filtro_linea']);
                    $objDuna->__set('_codigo', $row['codigo']);
                    $objDuna->__set('_estatus', $row['estatus']);
                    $objDuna->__set('_tipo', $row['tipo']);
                    $objDuna->__set('_fftt', $row['fftt']);
                    $objDuna->__set('_x', $row['x']);
                    $objDuna->__set('_y', $row['y']);
                    $objDuna->__set('_foto1', $row['foto1']);
                    $objDuna->__set('_foto2', $row['foto2']);
                    $objDuna->__set('_foto3', $row['foto3']);
                    $objDuna->__set('_foto4', $row['foto4']);
                    $objDuna->__set('_fechaInsert', $row['fecha_insert']);
                    $objDuna->__set('_asociado', $row['asociado']);
                    $objDuna->__set('_estado', $row['estado']);
                    $array[] = $objDuna;
                endif;
            endforeach;
            
            return $array;
        } catch(PDOException $e) {
            throw $e;
        }
    }
    
    public function eliminarFotoPirata($conexion, $idDuna, $foto1, $foto2, 
            $foto3, $foto4, $usuarioUpdate)
    {
        try {

            $sql = "UPDATE " . $this->_table . " SET 
                    foto1 = :foto1,
                    foto2 = :foto2,
                    foto3 = :foto3,
                    foto4 = :foto4,
                    fecha_update = now(), 
                    usuario_update = :usuario_update 
                    WHERE idduna = :idduna";
            $bind = $conexion->prepare($sql);
            $bind->bindParam(":foto1", $foto1);
            $bind->bindParam(":foto2", $foto2);
            $bind->bindParam(":foto3", $foto3);
            $bind->bindParam(":foto4", $foto4);
            $bind->bindParam(":idduna", $idDuna);
            $bind->bindParam(":usuario_update", $usuarioUpdate);
            $bind->execute();
            
            return 1;
        }catch(PDOException $e) {
            throw $e;
        }
    }
    
    public function insertarFotoPirata($conexion, $idDuna, $foto1, $foto2,
            $foto3, $foto4, $usuarioUpdate)
    {
        try {

            $sql = "UPDATE " . $this->_table . " SET 
                    foto1 = :foto1,
                    foto2 = :foto2,
                    foto3 = :foto3,
                    foto4 = :foto4,
                    fecha_update = now(), 
                    usuario_update = :usuario_update 
                    WHERE idduna = :idduna";
            $bind = $conexion->prepare($sql);
            $bind->bindParam(":foto1", $foto1);
            $bind->bindParam(":foto2", $foto2);
            $bind->bindParam(":foto3", $foto3);
            $bind->bindParam(":foto4", $foto4);
            $bind->bindParam(":idduna", $idDuna);
            $bind->bindParam(":usuario_update", $usuarioUpdate);
            $bind->execute();
            
            return 1;
        }catch(PDOException $e) {
            throw $e;
        }
    }
    
    public function insertarDunaAdsl($conexion, $arrDatos, $usuarioInsert)
    {
        try {
            //Estados duna adsl
            //01 DETECTADO RECEPTOR
            //03 DETECTADO EMISOR CON FOTO
            //04 DETECTADO EMISOR SIN FOTO
            
            $sql = "INSERT INTO " . $this->_table .  " (foto1, foto2, foto3, foto4, 
                    coddpto, codprov, coddist, direccion, 
                    x, y,
                    direccion_referencial, tipo, estado, ind_duna,
                    fecha_insert, usuario_insert, filtro_linea) 
                    VALUES(:foto1, :foto2, :foto3, :foto4, 
                    :coddpto, :codprov, :coddist, :direccion, 
                    :x, :y,
                    :direccion_referencial, :tipo, :estado, 'S', 
                    now(), :usuario_insert, '1')";
            $bind = $conexion->prepare($sql);
            $bind->bindParam(":foto1", $arrDatos['foto1']);
            $bind->bindParam(":foto2", $arrDatos['foto2']);
            $bind->bindParam(":foto3", $arrDatos['foto3']);
            $bind->bindParam(":foto4", $arrDatos['foto4']);
            $bind->bindParam(":coddpto", $arrDatos['departamento']);
            $bind->bindParam(":codprov", $arrDatos['provincia']);
            $bind->bindParam(":coddist", $arrDatos['distrito']);
            $bind->bindParam(":direccion", $arrDatos['direccion']);
            $bind->bindParam(":x", $arrDatos['x']);
            $bind->bindParam(":y", $arrDatos['y']);
            $bind->bindParam(":direccion_referencial", $arrDatos['direccion_referencial']);
            $bind->bindParam(":tipo", $arrDatos['tipo_antena']);
            $bind->bindParam(":estado", $arrDatos['estado']);
            $bind->bindParam(":usuario_insert", $usuarioInsert);
            $bind->execute();
            
            return 1;
        }catch(PDOException $e) {
            throw $e;
        }
    }
    
}