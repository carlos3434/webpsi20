<?php
/**
 * Telefonica del Peru S.A.A
 * [Web Unificada] :: Clase UsuarioSession
 * @package     class/data/
 * @name        class.UsuarioSession.php
 * @author      Gonzalo Chacaltana Buleje <gchacaltanab@gmd.com.pe>
 * @copyright   2011 Supervision de Planificacion y Soluciones Informaticas.
 */
class Data_Zonal
{
    protected $_idZonal = 0;
    protected $_abvZonal = '';
    protected $_descZonal = '';
    protected $_flag = 1;
    protected $_x;
    protected $_y;

    protected $_table ='webunificada.zonal';

    public function __construct()
    {
    }

    public function __get($propiedad)
    {
        $returnValue = (string) '';
        $returnValue = $this->$propiedad;
        return (string) $returnValue;
    }

    public function __set($propiedad, $valor)
    {
        $this->$propiedad = $valor;
    }

    public function save($conexion, $abvZonal, $descZonal, $flag)
    {
        try {
            $sql = "INSERT INTO ".$this->_table." 
            (abv_zonal, desc_zonal, flag) 
            VALUES (:abv_zonal,:desc_zonal,:flag)";
            $bind = $conexion->prepare($sql);
            $bind->bindParam(":abv_zonal", $abvZonal);
            $bind->bindParam(":desc_zonal", $descZonal);
            $bind->bindParam(":flag", $flag);
            $bind->execute();
        } catch(PDOException $error) {
            return $error;
            exit();
        }
    }

    public function update($conexion, $idZonal, $abvZonal, $descZonal, $flag)
    {
        try {
            $sql = "UPDATE ".$this->_table." SET abv_zonal = :abv_zonal, 
            desc_zonal = :desc_zonal, flag=:flag WHERE idzonal = :idzonal";
            $bind = $conexion->prepare($sql);
            $bind->bindParam(":idzonal", $idZonal);
            $bind->bindParam(":abv_zonal", $abvZonal);
            $bind->bindParam(":desc_zonal", $descZonal);
            $bind->bindParam(":flag", $flag);
            $bind->execute();
        } catch(PDOException $error) {
            return $error;
            exit();
        }
    }

    public function offsetGet($conexion, $idZonal)
    {
        $objZonal = new Data_Zonal();
        $arrayZonal = $objZonal->listar($conexion);
        foreach($arrayZonal as $objeto):
            if($objeto->__get('_idZonal') == $idZonal):
                return $objeto;
            endif;
        endforeach;
        return false;
    }

    public function offsetGetXCodigo($conexion, $codZonal)
    {
        $objZonal = new Data_Zonal();
        $arrayZonal = $objZonal->listar($conexion);
        foreach($arrayZonal as $objeto):
            if($objeto->__get('_abvZonal') == $codZonal):
                return $objeto;
            endif;
        endforeach;
        return false;
    }

    public function listar($conexion,$idZonal='',$abvZonal='',$flag='')
    {
        try {
            $where='';
            if ($idZonal != '' && $idZonal != '-1') {
                $where.=" AND idzonal = ".$idZonal;
            }
            if ($abvZonal != '' && $abvZonal != '-1') {
                $where.=" AND abv_zonal = '".$abvZonal . "' ";
            }

            $sql = "SELECT idzonal, abv_zonal, desc_zonal, flag, x,y 
            FROM ".$this->_table." WHERE flag=1 ".$where;
            echo $sql;
            $array = array();
            foreach ($conexion->query($sql) as $row):
                if($row['idzonal'] != ''):
                    $objZonal = new Data_Zonal();
                    $objZonal->__set('_idZonal', $row['idzonal']);
                    $objZonal->__set('_abvZonal', $row['abv_zonal']);
                    $objZonal->__set('_descZonal', $row['desc_zonal']);
                    $objZonal->__set('_flag', $row['flag']);
                    $objZonal->__set('_x', $row['x']);
                    $objZonal->__set('_y', $row['y']);
                    $array[] = $objZonal;
                endif;
            endforeach;
            return $array;
        } catch(PDOException $error) {
            return $error;
            exit();
        }
    }
    
    
    /* E.JAVIER */
    public function listarCanal($conexion, $canal)
    {
        $segmento = "";
        $idCanal = "";
        $idZonal = "";
        $idGrupo = "";
        
        if ($canal!='') {
            $whereCanal=" and id_canal=".$canal;
        } else {
            $whereCanal="";
        }                    
        try {
            $query="select segmento, idcanal, idzonal, idgrupo 
            from usuario_res_canal_zonal 
            where idusuario=".$_SESSION['USUARIO']->__get('_idUsuario').";";

            foreach ($conexion->query($query) as $row):
                    $segmento=$row['segmento'];
                    $idCanal=$row['idcanal'];
                    $idZonal=$row['idzonal'];
                    $idGrupo=$row['idgrupo'];
            endforeach;
            
            if ($idCanal!=0 && $idZonal!=0) {
                $sql = "SELECT idzonal,des_zonal FROM canales_punto_venta 
                where 1=1".$whereCanal." and id_canal in 
                (select idcanal from usuario_res_canal_zonal 
                where idusuario=".$_SESSION['USUARIO']->__get('_idUsuario').")
                and idzonal in (select idzonal from usuario_res_canal_zonal 
                where idusuario=".$_SESSION['USUARIO']->__get('_idUsuario').")
                group by 2";
                
            } elseif ($idCanal==0 && $idZonal!=0) {
                $sql = "SELECT idzonal,des_zonal FROM canales_punto_venta 
                where 1=1".$whereCanal." and idzonal in (select idzonal 
                from usuario_res_canal_zonal 
                where idusuario=".$_SESSION['USUARIO']->__get('_idUsuario').")
                group by 2";
            } elseif ($idCanal!=0 && $idZonal==0) {
                $sql = "SELECT idzonal,des_zonal FROM canales_punto_venta 
                where 1=1".$whereCanal." and id_canal in (select idcanal 
                from usuario_res_canal_zonal 
                where idusuario=".$_SESSION['USUARIO']->__get('_idUsuario').") 
                and idgrupo_canal in (select idgrupo 
                from usuario_res_canal_zonal 
                where idusuario=".$_SESSION['USUARIO']->__get('_idUsuario').")
                group by 2";
            } else {
                $sql = "SELECT idzonal,des_zonal 
                FROM canales_punto_venta where 1=1".$whereCanal." 
                group by 1;";
            }
            $array = array();
            foreach ($conexion->query($sql) as $row):
                if($row['idzonal'] != ''):
                    $objZonal = new data_PuntoVenta();
                    $objZonal->__set('_idZonal', $row['idzonal']);
                    $objZonal->__set('_desZonal', $row['des_zonal']);
                    $array[] = $objZonal;
                endif;
            endforeach;
            return $array;
        } catch(PDOException $error) {
            return $error;
            exit();
        }
    }    
    
    
}