<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 18/11/14
 * Time: 12:08 PM
 */

class GeoMovilidad {

    protected $_db = 'webpsi';
    protected $_table = 'geo_movilidad';

    public function listar($conexion, $arreglo = array())
    {
        try {
            $zonal = $arreglo[0];
            $proyecto = $arreglo[1];
            $movil = $arreglo[2];

            $sql = "SELECT id, movil campo, coord_x, coord_y
                    FROM " . $this->_db . "." . $this->_table . " WHERE id<>'' ";

            $sql .= " AND zonal='$zonal'
                      AND proyecto='$proyecto'
                      AND movil='$movil'";

            $sql .= " ORDER BY movil, orden";
            $bind = $conexion->prepare($sql);
            $bind->execute();

            $reporte = array();
            while ($data = $bind->fetch(PDO::FETCH_ASSOC)) {
                $reporte[] = $data;
            }
            return $reporte;
        } catch (PDOException $error) {
            exit();
        }
    }

    public function primerFiltro($conexion){
        try {
            $sql = "SELECT zonal nombre, zonal id
                    FROM " . $this->_db . "." . $this->_table
                . " GROUP BY 1 ORDER BY 1";
            $bind = $conexion->prepare($sql);
            $bind->execute();

            $reporte = array();
            while ($data = $bind->fetch(PDO::FETCH_ASSOC)) {
                $reporte[] = $data;
            }
            return $reporte;
        } catch (PDOException $e) {
            exit();
        }
    }

    public function listarProyecto($conexion, $arreglo=array()){
        try {
            $zonal = $arreglo[0];

            $sql = "SELECT proyecto campo, proyecto id
                    FROM " . $this->_db . "." . $this->_table
                . " WHERE zonal='$zonal'"
                . " GROUP BY 1 ORDER BY 1";
            $bind = $conexion->prepare($sql);
            $bind->execute();

            $reporte = array();
            while ($data = $bind->fetch(PDO::FETCH_ASSOC)) {
                $reporte[] = $data;
            }
            return $reporte;
        } catch (PDOException $e) {
            exit();
        }
    }

    public function listarMovil($conexion, $arreglo=array()){
        try {
            $zonal = $arreglo[0];
            $proyecto = $arreglo[1];

            $sql = "SELECT movil campo, movil id
                    FROM " . $this->_db . "." . $this->_table
                . " WHERE zonal='$zonal'"
                . " AND proyecto='$proyecto'"
                . " GROUP BY 1 ORDER BY 1";
            $bind = $conexion->prepare($sql);
            $bind->execute();

            $reporte = array();
            while ($data = $bind->fetch(PDO::FETCH_ASSOC)) {
                $reporte[] = $data;
            }
            return $reporte;
        } catch (PDOException $e) {
            exit();
        }
    }

} 