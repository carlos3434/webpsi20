<?php

class GeoAntenacircle
{

    protected $_db = 'webpsi';
    protected $_table = 'geo_antenacircle';

    public function primerFiltro($conexion){
        try {
            $sql = "SELECT departamento nombre, departamento id 
                    FROM " . $this->_db . "." . $this->_table 
                    . " GROUP BY 1 ORDER BY 1";
            $bind = $conexion->prepare($sql);
            $bind->execute();

            $reporte = array();
            while ($data = $bind->fetch(PDO::FETCH_ASSOC)) {
                $reporte[] = $data;
            }
            return $reporte;
        } catch (PDOException $e) {
            exit();
        }
    }

    public function listarProvincia($conexion, $arreglo=array()){
        try {
            $departamento = $arreglo[0];

            $sql = "SELECT provincia campo, provincia id 
                    FROM " . $this->_db . "." . $this->_table 
                    . " WHERE departamento='$departamento'"
                    . " GROUP BY 1 ORDER BY 1";
            $bind = $conexion->prepare($sql);
            $bind->execute();

            $reporte = array();
            while ($data = $bind->fetch(PDO::FETCH_ASSOC)) {
                $reporte[] = $data;
            }
            return $reporte;
        } catch (PDOException $e) {
            exit();
        }   
    }

    public function listarDistrito($conexion, $arreglo = array()){
        try {
            $departamento = $arreglo[0];
            $provincia = $arreglo[1];

            $sql = "SELECT distrito campo, distrito id 
                    FROM " . $this->_db . "." . $this->_table 
                    . " WHERE departamento='$departamento'"
                    . " AND provincia='$provincia'"
                    . " GROUP BY 1 ORDER BY 1";
            $bind = $conexion->prepare($sql);
            $bind->execute();

            $reporte = array();
            while ($data = $bind->fetch(PDO::FETCH_ASSOC)) {
                $reporte[] = $data;
            }
            return $reporte;
        } catch (PDOException $e) {
            exit();
        }   
    }
    
    public function listarTecnologia($conexion, $arreglo = array()){
        try {
            $departamento = $arreglo[0];
            $provincia = $arreglo[1];
            $distrito = $arreglo[2];

            $sql = "SELECT tecnologia campo, tecnologia id 
                    FROM " . $this->_db . "." . $this->_table 
                    . " WHERE departamento='$departamento'"
                    . " AND provincia='$provincia'"
                    . " AND distrito='$distrito'"
                    . " GROUP BY 1 ORDER BY 1";
            $bind = $conexion->prepare($sql);
            $bind->execute();

            $reporte = array();
            while ($data = $bind->fetch(PDO::FETCH_ASSOC)) {
                $reporte[] = $data;
            }
            return $reporte;
        } catch (PDOException $e) {
            exit();
        }   
    }
    
    public function listarAntenacircle($conexion, $arreglo = array()){
        try {
            $departamento   = $arreglo[0];
            $provincia      = $arreglo[1];
            $distrito       = $arreglo[2];
            $tecnologia     = $arreglo[3];

            $sql = "SELECT estacion campo, estacion id 
                    FROM " . $this->_db . "." . $this->_table 
                    . " WHERE departamento='$departamento'"
                    . " AND provincia='$provincia'"
                    . " AND distrito='$distrito'"
                    . " AND tecnologia='$tecnologia'"
                    . " GROUP BY 1 ORDER BY 1";
            $bind = $conexion->prepare($sql);
            $bind->execute();

            $reporte = array();
            while ($data = $bind->fetch(PDO::FETCH_ASSOC)) {
                $reporte[] = $data;
            }
            return $reporte;
        } catch (PDOException $e) {
            exit();
        }   
    }

    public function listar($conexion, $data = array())
    {
        try {
            $departamento = $data[0];
            $provincia = $data[1];
            $distrito = $data[2];
            $tecnologia = $data[3];
            $estacion = $data[4];

            $sql = "SELECT estacion id, estacion, coord_x, coord_y, radio  
                    FROM " . $this->_db . "." . $this->_table . " WHERE radio<>'' ";

            $sql .= " AND departamento='$departamento' 
                      AND provincia='$provincia'
                      AND distrito='$distrito'
                      AND tecnologia='$tecnologia'
                      AND estacion='$estacion'";
            
            $sql .= "ORDER BY id";
            $bind = $conexion->prepare($sql);
            $bind->execute();

            $reporte = array();
            while ($data = $bind->fetch(PDO::FETCH_ASSOC)) {
                $reporte[] = $data;
            }
            return $reporte;
        } catch (PDOException $error) {
            exit();
        }
    }   


}