<?php

/**
 * @package     class/data/
 * @name        class.FfttCapasArea.php
 * @category    Model
 * @author      Wilder Sandoval Huamani <wsandoval@gmd.com.pe>
 * @copyright   2011 Telefonica del Peru
 * @version     1.0 - 2011/08/09
 */

class Data_FfttCapasArea
{

    protected $_table = 'webunificada_fftt.poligonos';

    public function existsElemento($conexion, $tipoCapa, $elemento)
    {
        try {
            $sql = "SELECT DISTINCT tipocapa, elemento FROM 
                    " . $this->_table . " 
                    WHERE tipocapa = '" . trim($tipoCapa) . "' 
                    AND elemento = '" . trim($elemento) . "'";
            $bind = $conexion->prepare($sql);
            $bind->execute();

            $data = $bind->fetch(PDO::FETCH_ASSOC);

            $flag = 0;
            if (!empty($data)) {
                $flag = 1;
            }

            return $flag;
        } catch (PDOException $error) {
            return $error;
            exit();
        }
    }

    public function getTipoCapas($conexion)
    {
        try {
            $sql = "SELECT DISTINCT tipocapa FROM " . $this->_table . " 
                    ORDER BY tipocapa;";
            $bind = $conexion->prepare($sql);
            $bind->execute();

            $reporte = array();
            while ($data = $bind->fetch(PDO::FETCH_ASSOC)) {
                if ($data['tipocapa'] != '') {
                    $reporte[] = $data;
                }
            }
            return $reporte;
        } catch (PDOException $error) {
            return $error;
            exit();
        }
    }

    public function getCapasByTipoCapa($conexion, $tipoCapa)
    {
        try {
            $sql = "SELECT DISTINCT elemento 
                    FROM " . $this->_table . " 
                    WHERE tipocapa = '" . $tipoCapa . "' 
                    ORDER BY elemento";
            $bind = $conexion->prepare($sql);
            $bind->execute();

            $reporte = array();
            while ($data = $bind->fetch(PDO::FETCH_ASSOC)) {
                if ($data['elemento'] != '') {
                    $reporte[] = $data;
                }
            }
            return $reporte;
        } catch (PDOException $error) {
            return $error;
            exit();
        }
    }

    public function getXysByTipoCapaAndElemento($conexion, $tipoCapa, $elemento,
            $flagElemento=0)
    {
        try {

            $strWhere = "";
            if ($tipoCapa == 'MDF' || $tipoCapa == 'NODO') {
                if ($flagElemento == 1) {
                    $strWhere .= " AND elemento = '" . $elemento . "' ";
                } else {
                    $strWhere .= " AND elemento LIKE '" . $elemento . "-%' ";
                }
            } else {
                $strWhere .= " AND elemento = '" . $elemento . "' ";
            }

            $sql = "SELECT tipocapa, elemento, x, y 
                    FROM " . $this->_table . "  
                    WHERE tipocapa = '" . $tipoCapa . "' 
                    " . $strWhere . " 
                    ORDER BY orden";
            $bind = $conexion->prepare($sql);
            $bind->execute();

            $reporte = array();
            while ($data = $bind->fetch(PDO::FETCH_ASSOC)) {
                if ($data['x'] != '' && $data['y'] != '') {
                    $reporte[] = $data;
                }
            }
            return $reporte;
        } catch (PDOException $error) {
            return $error;
            exit();
        }
    }

    public function savePoligono(
        $conexion, $tipoCapa, $elemento, $orden, $x, $y
    )
    {
        try {

            $sql = "INSERT INTO " . $this->_table . " 
                    (tipocapa, elemento, orden, x, y) 
                    VALUES(:tipocapa, :elemento, :orden, :x, :y)";
            $bind = $conexion->prepare($sql);
            $bind->bindParam(":tipocapa", trim($tipoCapa));
            $bind->bindParam(":elemento", trim($elemento));
            $bind->bindParam(":orden", $orden);
            $bind->bindParam(":x", $x);
            $bind->bindParam(":y", $y);
            $bind->execute();

            return 1;
        } catch (PDOException $error) {
            return 0;
        }
    }

}