<?php

/**
 * @package     class/data/
 * @name        class.EdificioFotos.php
 * @category    Model
 * @author      Wilder Sandoval Huamani <wsandoval@gmd.com.pe>
 * @copyright   2011 Telefonica del Peru
 * @version     1.0 - 2012/04/10
 */

class Data_FfttEdificioFotos
{

    protected $_table = 'webunificada_fftt.fftt_edificios_foto';
    
    protected $_idEdificioFoto = 0;
    protected $_idEdificio = '';
    protected $_estado = '';
    protected $_desEstado = '';
    protected $_fechaFoto = '';
    protected $_observaciones = '';
    protected $_foto1 = '';
    protected $_foto2 = '';
    protected $_foto3 = '';
    protected $_foto4 = '';
    protected $_fechaInsert = '';
    protected $_userInsert = 0;
    protected $_fechaUpdate = '';
    protected $_userUpdate = 0;
    protected $_flag = 0;
    protected $_ingreso = 0;

    
    /**
     * Metodo constructor
     * @method __construct
     */
    public function __construct()
    {
    }

    /**
     * Obtiene el valor de un atributo
     *
     * @return string $returnValue
     * @param string $propiedad nombre del atributo a obtener
     */
    public function __get($propiedad)
    {
        $returnValue = (string) '';
        $returnValue = $this->$propiedad;
        return (string) $returnValue;
    }

    /**
     * Asigna un valor a un atributo
     *
     * @param string $propiedad nombre del atributo a setear
     * @param string $valor valor a asignar
     */
    public function __set($propiedad, $valor)
    {
        $this->$propiedad = $valor;
    }

    /**
     * Obtiene un listado de registro de Fotos de un Edificio
     *
     * @return array $array[] array de objetos de tipo Data_FfttEdificioFotos
     * @param object $conexion Objeto de conexion a la BD creada con PDO
     * @param int $idEdificio id del edificio
     */
    public function obtenerFotosEdificio($conexion, $idEdificio)
    {
        try {
            $sql = "SELECT edi.idedificios_foto, edi.idedificio, 
                    edi.fecha_foto,
                    edi.estado, edi.observaciones, edi.foto1, edi.foto2, 
                    edi.foto3, edi.foto4, edi.fecha_insert, edi.user_insert, 
                    edi.fecha_update, edi.user_update, edi.flag, edi.ingreso, 
                    est.des_estado as des_estado 
                    FROM " . $this->_table . " edi 
                        LEFT JOIN webunificada_fftt.fftt_edificios_estado est  
                    ON edi.estado = est.codedificio_estado  
                    WHERE edi.idedificio = '" . $idEdificio . "' 
                    AND edi.flag = '1' 
                    ORDER BY edi.fecha_foto DESC";
            $array = array();
            foreach ($conexion->query($sql) as $row):
                if($row['idedificio'] != ''):
                    $objEdificioFotos = new Data_FfttEdificioFotos();
                    $objEdificioFotos->__set(
                        '_idEdificioFoto', $row['idedificios_foto']
                    );
                    $objEdificioFotos->__set('_idEdificio', $row['idedificio']);
                    $objEdificioFotos->__set(
                        '_fechaFoto', $row['fecha_foto']
                    );
                    $objEdificioFotos->__set('_estado', $row['estado']);
                    $objEdificioFotos->__set('_desEstado', $row['des_estado']);
                    $objEdificioFotos->__set(
                        '_observaciones', $row['observaciones']
                    );
                    $objEdificioFotos->__set('_foto1', $row['foto1']);
                    $objEdificioFotos->__set('_foto2', $row['foto2']);
                    $objEdificioFotos->__set('_foto3', $row['foto3']);
                    $objEdificioFotos->__set('_foto4', $row['foto4']);
                    $objEdificioFotos->__set(
                        '_fechaInsert', $row['fecha_insert']
                    );
                    $objEdificioFotos->__set(
                        '_userInsert', $row['user_insert']
                    );
                    $objEdificioFotos->__set(
                        '_fechaUpdate', $row['fecha_update']
                    );
                    $objEdificioFotos->__set(
                        '_userUpdate', $row['user_update']
                    );
                    $objEdificioFotos->__set('_flag', $row['flag']);
                    $objEdificioFotos->__set('_ingreso', $row['ingreso']);
                    
                    $array[] = $objEdificioFotos;
                endif;
            endforeach;
            
            return $array;
        } catch(PDOException $e) {
            throw $e;
        }
    }
    
    /**
     * Obtiene el detalle un registro de Fotos de un Edificio
     *
     * @return array $array[] array de objetos de tipo Data_FfttEdificioFotos
     * @param object $conexion Objeto de conexion a la BD creada con PDO
     * @param int $idEdificiosFoto id de registro de fotos del edificio
     */
    public function obtenerFotosDetalle($conexion, $idEdificiosFoto)
    {
        try {
            $sql = "SELECT edi.idedificios_foto, edi.idedificio,
                    edi.fecha_foto,
                    edi.estado, edi.observaciones, edi.foto1, edi.foto2,
                    edi.foto3, edi.foto4, edi.fecha_insert, edi.user_insert,
                    edi.fecha_update, edi.user_update, edi.flag, edi.ingreso, 
                    est.des_estado as des_estado
                    FROM " . $this->_table . " edi
                        LEFT JOIN webunificada_fftt.fftt_edificios_estado est
                    ON edi.estado = est.codedificio_estado
                    WHERE edi.idedificios_foto = '" . $idEdificiosFoto . "' 
                    AND edi.flag = '1' ";
            $array = array();
            foreach ($conexion->query($sql) as $row):
                if($row['idedificios_foto'] != ''):
                    $objEdificioFotos = new Data_FfttEdificioFotos();
                    $objEdificioFotos->__set(
                        '_idEdificioFoto', $row['idedificios_foto']
                    );
                    $objEdificioFotos->__set('_idEdificio', $row['idedificio']);
                    $objEdificioFotos->__set(
                        '_fechaFoto', $row['fecha_foto']
                    );
                    $objEdificioFotos->__set('_estado', $row['estado']);
                    $objEdificioFotos->__set('_desEstado', $row['des_estado']);
                    $objEdificioFotos->__set(
                        '_observaciones', $row['observaciones']
                    );
                    $objEdificioFotos->__set('_foto1', $row['foto1']);
                    $objEdificioFotos->__set('_foto2', $row['foto2']);
                    $objEdificioFotos->__set('_foto3', $row['foto3']);
                    $objEdificioFotos->__set('_foto4', $row['foto4']);
                    $objEdificioFotos->__set(
                        '_fechaInsert', $row['fecha_insert']
                    );
                    $objEdificioFotos->__set(
                        '_userInsert', $row['user_insert']
                    );
                    $objEdificioFotos->__set(
                        '_fechaUpdate', $row['fecha_update']
                    );
                    $objEdificioFotos->__set(
                        '_userUpdate', $row['user_update']
                    );
                    $objEdificioFotos->__set('_flag', $row['flag']);
                    $objEdificioFotos->__set('_ingreso', $row['ingreso']);
                    
                    $array[] = $objEdificioFotos;
                endif;
            endforeach;

            return $array;
        } catch(PDOException $e) {
            throw $e;
        }
    }
    
    /**
     * Inserta un nuevo registro de foto para edificios
     *
     * @return boolean 1 si lleva a cabo la actualizacion,
     * sino devuelve la exception
     * @param object $conexion Objeto de conexion a la BD creada con PDO
     * @param array $arrDatos array asociativo con los datos a insertar
     * @param int $usuarioIinsert id de usuario que realiza la actualizacion
     */
    public function insertarFotoEdificio($conexion, $arrDatos, $usuarioInsert)
    {
        try {
            $sql = "INSERT INTO " . $this->_table 
                    .  " (idedificio, estado, fecha_foto, 
                    observaciones, foto1, foto2, foto3, foto4, fecha_insert,
                    user_insert, ingreso) 
                    VALUES(:idedificio, :estado, :fecha_foto,  
                    :observaciones, :foto1, :foto2, :foto3, :foto4, now(),
                    :user_insert, :ingreso)";
            $bind = $conexion->prepare($sql);
            $bind->bindParam(":idedificio", $arrDatos['idEdificio']);
            $bind->bindParam(":estado", $arrDatos['estado']);
            $bind->bindParam(":fecha_foto", $arrDatos['fecha_foto']);
            $bind->bindParam(":observaciones", $arrDatos['observaciones']);
            $bind->bindParam(":foto1", $arrDatos['foto_edificio1']);
            $bind->bindParam(":foto2", $arrDatos['foto_edificio2']);
            $bind->bindParam(":foto3", $arrDatos['foto_edificio3']);
            $bind->bindParam(":foto4", $arrDatos['foto_edificio4']);
            $bind->bindParam(":user_insert", $usuarioInsert);
            $bind->bindParam(":ingreso", $arrDatos['ingreso']);
            $bind->execute();
            
            return 1;
        }catch(PDOException $e) {
            throw $e;
        }
    }
}