<?php

/**
 * @package     class/data/
 * @name        Data_PoligonoTipo
 * @category    Model
 * @author      Wilder Sandoval Huamani <wsandoval@gmd.com.pe>
 * @copyright   2011 Telefonica del Peru
 * @version     1.0 - 2011/11/21
 */

class Data_PoligonoTipo
{

    protected $_table = 'webunificada_fftt.poligono_tipo';

    public function listar($conexion, $tipo='', $pagina='', $flag='')
    {
        try {

            $strLimite = '';
            $strWhere = '';
            if ($pagina == '') {
                $pagina = 1;
            } else {
                $offset = ($pagina - 1) * TAM_PAG_LISTADO;
                $strLimite = " LIMIT " . $offset . "," . TAM_PAG_LISTADO;
            }

            if ($tipo != '') {
                $strWhere.=" AND tipo='" . $tipo . "'";
            }

            if ($flag != '') {
                $strWhere.=' AND flag=' . $flag;
            }

            $sql = "SELECT idpoligono_tipo, nombre, tipo, flag 
                    FROM " . $this->_table . " 
                    WHERE 1=1 " . $strWhere . " 
                    ORDER BY nombre";
			//echo $sql;
            $bind = $conexion->prepare($sql);
            $bind->execute();

            $reporte = array();
            while ($data = $bind->fetch(PDO::FETCH_ASSOC)) {
                if ($data['idpoligono_tipo'] != '') {
                    $reporte[] = $data;
                }
            }
            return $reporte;
        } catch (PDOException $error) {
            exit();
        }
    }

    public function listTotal($conexion)
    {
        try {
            $sql = "SELECT count(1) as 'Total' FROM " . $this->_table;
            foreach ($conexion->query($sql) as $row):
                $totalRegistros = $row['Total'];
            endforeach;
            return $totalRegistros;
        } catch (PDOException $error) {
            exit();
        }
    }

    public function listPoligonosxPoligonoTipo($conexion, $idPoligonoTipo)
    {
        try {

            $sql = "SELECT COUNT(idpoligono_tipo) AS 'Total' 
                    FROM webunificada_fftt.poligono 
                    WHERE idpoligono_tipo = " . $idPoligonoTipo;
            foreach ($conexion->query($sql) as $row):
                $totalRegistros = $row['Total'];
            endforeach;
            return $totalRegistros;
        } catch (PDOException $error) {
            exit();
        }
    }

    public function getPoligonoTipo($conexion, $idPoligonoTipo)
    {
        try {
            $sql = "SELECT idpoligono_tipo, nombre 
                    FROM " . $this->_table . " 
                    WHERE idpoligono_tipo = '" . $idPoligonoTipo . "'";
            $bind = $conexion->prepare($sql);
            $bind->execute();

            $reporte = array();
            while ($data = $bind->fetch(PDO::FETCH_ASSOC)) {
                if ($data['nombre'] != '') {
                    $reporte[] = $data;
                }
            }
            return $reporte;
        } catch (PDOException $error) {
            exit();
        }
    }

    public function updateCampoPoligonoTipo($conexion, $idPoligonoTipo, $campo,
            $nvoValor)
    {
        try {
            $sql = "UPDATE " . $this->_table . " 
                    SET " . $campo . "='" . $nvoValor . "' 
                    WHERE idpoligono_tipo =" . $idPoligonoTipo;
            $bind = $conexion->prepare($sql);
            $bind->execute();
        } catch (PDOException $error) {
            exit();
        }
    }

    public function updateStatePoligonoTipo($conexion, $idPoligonoTipo, $flag)
    {
        try {
            $sql = "UPDATE " . $this->_table . " 
                    SET flag=:flag 
                    WHERE idpoligono_tipo =:idpoligono_tipo";
            $bind = $conexion->prepare($sql);
            $bind->bindParam(":idpoligono_tipo", $idPoligonoTipo);
            $bind->bindParam(":flag", $flag);
            $bind->execute();
        } catch (PDOException $error) {
            exit();
        }
    }

    public function insertPoligonoTipo($conexion, $nombre, $tipo, $userInsert, 
            $flag)
    {
        try {
            $sql = "INSERT INTO " . $this->_table . "(nombre, tipo, 
                    fecha_insert, user_insert, flag) 
                    VALUES(:nombre, :tipo, now(), :user_insert, :flag)";
            $bind = $conexion->prepare($sql);
            $bind->bindParam(":nombre", $nombre);
            $bind->bindParam(":tipo", $tipo);
            $bind->bindParam(":user_insert", $userInsert);
            $bind->bindParam(":flag", $flag);
            $bind->execute();

            return $conexion->lastInsertId();
        } catch (PDOException $error) {
            exit();
        }
    }

}