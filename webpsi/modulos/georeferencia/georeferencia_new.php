<?php

define("APP_FDR", "webpsi/");
define("APP_DIR", $_SERVER['DOCUMENT_ROOT'] . "/" . APP_FDR);
define("APP_URL", "http://" . $_SERVER['SERVER_NAME'] . APP_FDR);

require_once 'autoload.php';
?>
<!DOCTYPE HTML>
<html>
	<head>
		<title>Web PSI</title>
		<meta http-equiv="content-type" content="text/html; charset=utf-8" />
		<meta name="description" content="" />
		<meta name="keywords" content="" />
		<link href="http://fonts.googleapis.com/css?family=Ubuntu+Condensed" rel="stylesheet">
		<script src="js/jquery.min.js"></script>
		<script src="js/config.js"></script>
		<script src="js/skel.min.js"></script>
		<script src="js/skel-panels.min.js"></script>
    <script src="js/jquery.magnific-popup.min.js"></script>
    <script src="js/easyResponsiveTabs.js"></script>

    <script src="../../js/jquery-ui-1.8.2.custom.min.js"></script>
		<noscript>
			<link rel="stylesheet" href="css/skel-noscript.css" />
			<link rel="stylesheet" href="css/style.css" />
			<link rel="stylesheet" href="css/style-desktop.css" />
		</noscript>

    <link rel="stylesheet" href="css/magnific-popup.css" />
    <link rel="stylesheet" href="css/easy-responsive-tabs.css" />
    <link href='../../js/jqueryui_1.8.2/css/redmond/jquery-ui-1.8.1.custom.css' rel="Stylesheet" />

		<style>
	      #map-canvas {
	        height: 500px;
	        width: 100%;
	      }
        #map-over {
          position: absolute; 
          top: 40px; 
          left: 80px; 
          z-index: 999;
          background-color: #FFFFFF;
        }
        #map-wrapper { position: relative; }
        .white-popup {
          position: relative;
          background: #FFF;
          padding: 20px;
          width: auto;
          max-width: 580px;
          margin: 20px auto;
        }
        .map-ctrl-container {
            margin: 2px auto;
            position: relative;
            overflow: auto;
        }
        .map-ctrl-block {
            background: #DFDBD4;
            height: 25px;
            width: 100px;
            float: left;
            margin: 3px;
            color: #000000;
            text-align: center;
            vertical-align: middle;
        }
        #map-ctrl-inherit select option {
            font-size: 10pt;
        }
        .itemSelect {
            background-color: #e1e3ed;
            margin: 5px 2px 0px 2px;
            padding: 5px;
        }
        .departamento {background-color: #00527A; color: #FFFFFF; margin: 5px, 0px, 5px, 0px; padding: 7px;}
        .provincia {background-color: #0085C7; margin: 5px, 0px, 5px, 0px; padding: 7px;}
        .distrito {background-color: #FFFFFF; color: #00527A; margin: 5px, 0px, 5px, 0px; padding: 7px;}
        .itemSelected {background-color: #C76300; color: #FFFFFF;}

        .geo_item {display: none};
        .geo_item_active {display: block};
        
	  </style>
	  <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=true"></script>
	  <script>
    // Note: This example requires that you consent to location sharing when
    // prompted by your browser. If you see a blank space instead of the map, this
    // is probably because you have denied permission for location sharing.

    var map;
    var data_polygon = "";
    var polygonCoords = [];
    var markers = [];
    var bounds;

    function initialize() {
        bounds = new google.maps.LatLngBounds();
      var mapOptions = {
        zoom: 10
      };
      map = new google.maps.Map(document.getElementById('map-canvas'),
          mapOptions);

      // Try HTML5 geolocation
      if(navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(function(position) {
          var pos = new google.maps.LatLng(position.coords.latitude,
                                           position.coords.longitude);
          /*
          var infowindow = new google.maps.InfoWindow({
            map: map,
            position: pos,
            content: 'Location found using HTML5.'
          });
        */
          map.setCenter(pos);
        }, function() {
          handleNoGeolocation(true);
        });
      } else {
        // Browser doesn't support Geolocation
        handleNoGeolocation(false);
      }
    }

    function handleNoGeolocation(errorFlag) {
      if (errorFlag) {
        var content = 'Error: The Geolocation service failed.';
      } else {
        var content = 'Error: Your browser doesn\'t support geolocation.';
      }

      var options = {
        map: map,
        position: new google.maps.LatLng(60, 105),
        content: content
      };

      var infowindow = new google.maps.InfoWindow(options);
      map.setCenter(options.position);
    }

    google.maps.event.addDomListener(window, 'load', initialize);

    $(document).ready(function (){
        $('.open-map-ctrl').magnificPopup({
          type:'inline',
          midClick: true // Allow opening popup on middle mouse click. Always set it to true if you don't provide alternative source in href.
        });
        $('#map-tab').easyResponsiveTabs();

        $('#cascade1').hide();

        data_content = "action=geoElementos";
        $.ajax({
            type:   "POST",
            url:    "georeferencia.request.php",
            data:   data_content,
            dataType: 'json',
            success: function(datos) {
                $.each(datos, function() {
                    $('<option>').val(this.id).text(this.nombre).appendTo('#geo_elemento');
                });
            }
        });

        //Listas, seleccion de elementos y dependencias
        $("#geo_elemento").change(function (){
            var origen = $("#geo_elemento option:selected").text();
            var dependencia = "";
            data_content = "action=getGeoDedependencia&elemento_id=" + $("#geo_elemento").val();
            $.ajax({
                type:   "POST",
                url:    "georeferencia.request.php",
                data:   data_content,
                dataType: 'json',
                success: function(datos) {
                    $(".geo_item_active").attr("class", "geo_item");
                    $.each(datos, function() {
                        //obtener opciones del primer elemento:orden = 1
                        if ( this.orden == 1 ) {
                            var campo = this.campo;
                            data_content = "action=getGeoItemIni&item=" + this.campo + "&origen=" + origen;
                            $.ajax({
                                type:   "POST",
                                url:    "georeferencia.request.php",
                                data:   data_content,
                                dataType: 'json',
                                success: function(datos) {
                                    $.each(datos, function() {
                                        $('<option>').val(this.id).text(this.nombre).appendTo('#geo_' + campo);
                                    });
                                }
                            });
                        }
                        //elimina options
                        //$(".geo_item select option").remove();                        

                        //mostrar elemento
                        $("#geo_" + this.campo).attr("class", "geo_item_active");
                        dependencia += "_" + this.campo
                    });
                    $(".geo_item_active").attr("title", dependencia.substring(1));
                    $("#geo_elemento").attr("title", dependencia.substring(1));

                    //Seleccionando dependencias
                    $(".geo_item_active").change(function (){
                        var title = $("#geo_elemento").attr("title");
                        var items = title.split("_");
                        $.each(items, function(key, value){
                            if($("#geo_" + value + " option").length == 1 ) {

                                data_content = "action=geoItem&item=" + value;
                                $.ajax({
                                    type:   "POST",
                                    url:    "georeferencia.request.php",
                                    data:   data_content,
                                    dataType: 'json',
                                    success: function(datos) {
                                       
                                    }
                                });

                                return false;
                            }
                        });
                    });

                }
            });
        });

        

        //Bloque de funciones del módulo
        $(".tipoElemento").click(function(event) {
            event.preventDefault();
            var elemento = $(this).attr("href");

            if (elemento=="mdf") {            
                data_content = "action=geoSearch&element=" + elemento;
                $.ajax({
                    type:   "POST",
                    url:    "georeferencia.request.php",
                    data:   data_content,
                    dataType: 'json',
                    success: function(datos) {
                        $.each(datos, function() {
                            $('#inherit1').append("<div class='itemSelect' title='" + this.nombre + "'>" + this.nombre + "</div>");
                        });

                        $('#inherit1 div').click(function (){
                            $( this ).toggleClass( "itemSelected" );
                        });
                    }
                });
            }

            if (elemento=="distrito") {
                data_content = "action=geoSearch&element=" + elemento;
                $.ajax({
                    type:   "POST",
                    url:    "georeferencia.request.php",
                    data:   data_content,
                    success: function(datos) {
                        
                        $('#inherit1').html(datos);                        

                        $(".departamento").click(function (){
                            var depa = $(this).attr("id");

                            $('div[title="' + depa + '"]').each(function() {
                                var cssDisplay = $(this).css("display");
                                if (cssDisplay == "none") {
                                    $(this).css("display", "block");
                                } else {
                                    $(this).css("display", "none");
                                }
                            });

                        });

                        $(".provincia").click(function (event){
                            event.stopPropagation();
                            var prov = $(this).attr("id");

                            $('div[title="' + prov + '"]').each(function() {
                                var cssDisplay = $(this).css("display");
                                if (cssDisplay == "none") {
                                    $(this).css("display", "block");
                                } else {
                                    $(this).css("display", "none");
                                }
                            });
                            
                        });

                        $('.distrito').click(function (event){
                            event.stopPropagation();
                            $( this ).toggleClass( "itemSelected" );
                        });
                    }
                });
            }

            if (elemento=="terminal") {
                data_content = "action=geoSearch&element=" + elemento;
                $.ajax({
                    type:   "POST",
                    url:    "georeferencia.request.php",
                    data:   data_content,
                    success: function(datos) {
                        
                        $('#inherit1').html(datos);
                        
                        $(".tipored").click(function (){
                            var depa = $(this).attr("id");

                            $('div[title="' + depa + '"]').each(function() {
                                var cssDisplay = $(this).css("display");
                                if (cssDisplay == "none") {
                                    $(this).css("display", "block");
                                } else {
                                    $(this).css("display", "none");
                                }
                            });

                        });

                        $(".zonal").click(function (event){
                            event.stopPropagation();
                            var prov = $(this).attr("id");

                            $('div[title="' + prov + '"]').each(function() {
                                var cssDisplay = $(this).css("display");
                                if (cssDisplay == "none") {
                                    $(this).css("display", "block");
                                } else {
                                    $(this).css("display", "none");
                                }
                            });
                            
                        });

                        $('.mdf').click(function (event){
                            event.stopPropagation();
                            var mdf = $(this).attr("id");

                            //Nueva peticion recupera cable/armario
                            data_content = "action=geoSearch&element=lista_terminal&dato=" + mdf;
                            $.ajax({
                                type:   "POST",
                                url:    "georeferencia.request.php",
                                data:   data_content,
                                success: function(datos) {console.log(datos);
                                    
                                    $( "#" + mdf ).append(datos);

                                    $('.cabarm').click(function (event){
                                        event.stopPropagation();
                                        $( this ).toggleClass( "itemSelected" );
                                    });
                                    
                                }
                            });

                            //$( this ).toggleClass( "itemSelected" );
                        });
                    }
                });
            }
        });

        $('#btnDraw').click(function(){
            $( ".itemSelected" ).each(function( index ) {
                
                var title = $( this ).attr("title");
                title = title.split("-");
                data_polygon += "'" + title[0] + "',"

                //Objeto poligono
                var polygon;

                //Enviar peticion y respuesta json
                data_polygon = "action=drawPolygon&poligono_mdf=" + title[0];
                $.ajax({
                    type:   "POST",
                    url:    "georeferencia.request.php",
                    data:   data_polygon,
                    dataType: 'json',
                    success: function(datos) {
                        //console.log(this.coord_x);
                        $.each(datos, function() {
                            var pt = new google.maps.LatLng(this.coord_y, this.coord_x);
                            polygonCoords.push( pt );
                        });

                        // Construct the polygon.
                        polygon = new google.maps.Polygon({
                            paths: polygonCoords,
                            strokeColor: '#FF0000',
                            strokeOpacity: 0.8,
                            strokeWeight: 2,
                            fillColor: '#FF0000',
                            fillOpacity: 0.35
                        });

                        polygon.setMap(map);

                        polygon = null;
                        polygonCoords = [];                
                    }
                });
            });
            $.magnificPopup.close();
        });

        $('#btnDrawDist').click(function(){
            $( ".itemSelected" ).each(function( index ) {
                
                var title = $( this ).attr("id");
                data_polygon += "'" + title + "',"

                //Objeto poligono
                var polygon;

                //Enviar peticion y respuesta json
                data_polygon = "action=drawPolygon&poligono_distrito=" + title;
                $.ajax({
                    type:   "POST",
                    url:    "georeferencia.request.php",
                    data:   data_polygon,
                    dataType: 'json',
                    success: function(datos) {
                        console.log(datos);
                        $.each(datos, function() {
                            var pt = new google.maps.LatLng(this.coord_y, this.coord_x);
                            bounds.extend(pt);
                            polygonCoords.push( pt );
                        });

                        // Construct the polygon.
                        polygon = new google.maps.Polygon({
                            paths: polygonCoords,
                            strokeColor: '#FF0000',
                            strokeOpacity: 0.8,
                            strokeWeight: 2,
                            fillColor: '#FF0000',
                            fillOpacity: 0.35
                        });

                        polygon.setMap(map);
                        map.fitBounds(bounds);

                        polygon = null;
                        polygonCoords = [];              
                    }
                });
            });
            $.magnificPopup.close();
        });

        $('#btnDrawTerm').click(function(){
            $( ".itemSelected" ).each(function( index ) {
                
                var title = $( this ).attr("id");
                data_polygon += "'" + title + "',"

                //Objeto poligono
                var polygon;

                //Enviar peticion y respuesta json
                data_polygon = "action=drawMarker&single_terminal=" + title;
                $.ajax({
                    type:   "POST",
                    url:    "georeferencia.request.php",
                    data:   data_polygon,
                    dataType: 'json',
                    success: function(datos) {
                        //console.log(this.coord_x);
                        $.each(datos, function() {
                            var pt = new google.maps.LatLng(this.coord_y, this.coord_x);
                            bounds.extend(pt);
                            markers.push(pt);

                            var marker = new google.maps.Marker({
                                position: pt,
                                map: map
                            });

                            marker.setMap(map);
                        });
                        map.fitBounds(bounds);
                    }
                });
            });
            $.magnificPopup.close();
        });
    });
    </script>

		<!--[if lte IE 9]><link rel="stylesheet" href="css/ie9.css" /><![endif]-->
		<!--[if lte IE 8]><script src="js/html5shiv.js"></script><![endif]-->
	</head>
	<body>
	<!-- ********************************************************* -->
		<div id="header-wrapper">
			<div class="container">
				<div class="row">
					<div class="12u">
						
						<header id="header">
							<h1><a href="#" id="logo">Web PSI</a></h1>
							<nav id="nav">
								<a href="index.html" class="current-page-item">Homepage</a>
								<a href="twocolumn1.html">Two Column #1</a>
								<a href="twocolumn2.html">Two Column #2</a>
								<a href="onecolumn.html">One Column</a>
								<a href="threecolumn.html">Three Column</a>
							</nav>
						</header>
					
					</div>
				</div>
			</div>
		</div>
    <div id="map-wrapper">
        <div id="map-canvas"></div>
        <div id="map-over">
          <a href="#map-ctrl" class="open-map-ctrl">Controles</a>
        </div>
        <div id="map-ctrl" class="white-popup mfp-hide">
            <div id="map-ctrl-element">
                <div id="map-tab">
                    <ul class="resp-tabs-list">
                        <li>Buscar elemento</li>
                        <li>Crear capa</li>
                        <li>Responsive Tab 3</li>
                    </ul>
                    <div class="resp-tabs-container">
                        <div>
                            <div class="map-ctrl-container">
                                <div>
                                    <span float="left">Elemento</span>
                                    <div id="map-ctrl-elementos">
                                        <div class="geo_item_init">
                                            <select name="geo_elemento" id="geo_elemento" title="">
                                                <option value=""> - Seleccione - </option>
                                            </select>
                                        </div>
                                        <div>
                                            <select class="geo_item" title="" name="geo_departamento" id="geo_departamento">
                                                <option value="">-Departamento-</option>
                                            </select>
                                        </div>
                                        <div>
                                            <select class="geo_item" title="" name="geo_provincia" id="geo_provincia">
                                                <option value="">-Provincia-</option>
                                            </select>
                                        </div>
                                        <div>
                                            <select class="geo_item" title="" name="geo_distrito" id="geo_distrito">
                                                <option value="">-Distrito-</option>
                                            </select>
                                        </div>
                                        <div>
                                            <select class="geo_item" title="" name="geo_zonal" id="geo_zonal">
                                                <option value="">-Zonal-</option>
                                            </select>
                                        </div>
                                        <div>
                                            <select class="geo_item" title="" name="geo_mdf" id="geo_mdf">
                                                <option value="">-Mdf-</option>
                                            </select>
                                        </div>
                                        <div>
                                            <select class="geo_item" title="" name="geo_nodo" id="geo_nodo">
                                                <option value="">-Nodo-</option>
                                            </select>
                                        </div>
                                        <div>
                                            <select class="geo_item" title="" name="geo_armario" id="geo_armario">
                                                <option value="">-Armario-</option>
                                            </select>
                                        </div>
                                        <div>
                                            <select class="geo_item" title="" name="geo_cable" id="geo_cable">
                                                <option value="">-Cable-</option>
                                            </select>
                                        </div>
                                        <div>
                                            <select class="geo_item" title="" name="geo_troba" id="geo_troba">
                                                <option value="">-Troba-</option>
                                            </select>
                                        </div>
                                        <div>
                                            <select class="geo_item" title="" name="geo_terminald" id="geo_terminald">
                                                <option value="">-Terminal D.-</option>
                                            </select>
                                        </div>
                                        <div>
                                            <select class="geo_item" title="" name="geo_terminalf" id="geo_terminalf">
                                                <option value="">-Terminal F.-</option>
                                            </select>
                                        </div>
                                        <div>
                                            <select class="geo_item" title="" name="geo_tap" id="geo_tap">
                                                <option value="">-Tap-</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="map-ctrl-block"><a href="distrito" class="tipoElemento">Distrito</a></div>
                                <div class="map-ctrl-block"><a href="zonal" class="tipoElemento">Zonal</a></div>
                                <div class="map-ctrl-block"><a href="mdf" class="tipoElemento">Mdf</a></div>
                                <div class="map-ctrl-block"><a href="nodo" class="tipoElemento">Nodo</a></div>
                                <div class="map-ctrl-block"><a href="armario" class="tipoElemento">Armario</a></div>
                                <div class="map-ctrl-block"><a href="terminal" class="tipoElemento">Terminal</a></div>
                                <!--
                                <div class="map-ctrl-block">6. name of the company</div>
                                <div class="map-ctrl-block">7. name of the company</div>
                                <div class="map-ctrl-block">8. name of the company</div>
                                -->
                            </div>
                            <div id="map-ctrl-msg"></div>
                            <div id="map-ctrl-inherit">
                                <div id="inherit1" style="max-height: 200px; overflow: auto"></div>
                                <select name="cascade1" id="cascade1"></select>
                                <input type="button" name="btnDraw" id="btnDraw" value="DrawMdf">
                                <input type="button" name="btnDrawDist" id="btnDrawDist" value="DrawDist">
                                <input type="button" name="btnDrawTerm" id="btnDrawTerm" value="DrawTerm">
                            </div>
                        </div>
                        <div>
                            <p>This tab has icon in it.</p>
                        </div>
                        <div>
                            <p>Suspendisse blandit velit Integer laoreet placerat suscipit. Sed sodales scelerisque commodo. Nam porta cursus lectus. Proin nunc erat, gravida a facilisis quis, ornare id lectus. Proin consectetur nibh quis Integer laoreet placerat suscipit. Sed sodales scelerisque commodo. Nam porta cursus lectus. Proin nunc erat, gravida a facilisis quis, ornare id lectus. Proin consectetur nibh quis urna gravid urna gravid eget erat suscipit in malesuada odio venenatis.</p>
                        </div>
                    </div>
                </div>
                
            </div>
            <div id="map-ctrl-response">

            </div>
            
        </div>
    </div>				
		<div id="footer-wrapper">
			<div class="container">
				<div class="row">
					<div class="8u">
						
						<section>
							<h2>How about a truckload of links?</h2>
							<div>
								<div class="row">
									<div class="3u">
										<ul class="link-list">
											<li><a href="#">Sed neque nisi consequat</a></li>
											<li><a href="#">Dapibus sed mattis blandit</a></li>
											<li><a href="#">Quis accumsan lorem</a></li>
											<li><a href="#">Suspendisse varius ipsum</a></li>
											<li><a href="#">Eget et amet consequat</a></li>
										</ul>
									</div>
									<div class="3u">
										<ul class="link-list">
											<li><a href="#">Quis accumsan lorem</a></li>
											<li><a href="#">Sed neque nisi consequat</a></li>
											<li><a href="#">Eget et amet consequat</a></li>
											<li><a href="#">Dapibus sed mattis blandit</a></li>
											<li><a href="#">Vitae magna sed dolore</a></li>
										</ul>
									</div>
									<div class="3u">
										<ul class="link-list">
											<li><a href="#">Sed neque nisi consequat</a></li>
											<li><a href="#">Dapibus sed mattis blandit</a></li>
											<li><a href="#">Quis accumsan lorem</a></li>
											<li><a href="#">Suspendisse varius ipsum</a></li>
											<li><a href="#">Eget et amet consequat</a></li>
										</ul>
									</div>
									<div class="3u">
										<ul class="link-list">
											<li><a href="#">Quis accumsan lorem</a></li>
											<li><a href="#">Sed neque nisi consequat</a></li>
											<li><a href="#">Eget et amet consequat</a></li>
											<li><a href="#">Dapibus sed mattis blandit</a></li>
											<li><a href="#">Vitae magna sed dolore</a></li>
										</ul>
									</div>
								</div>
							</div>
						</section>
					
					</div>
					<div class="4u">

						<section>
							<h2>Something of interest</h2>
							<p>Duis neque nisi, dapibus sed mattis quis, rutrum accumsan sed. 
							Suspendisse eu varius nibh. Suspendisse vitae magna eget odio amet 
							mollis justo facilisis quis. Sed sagittis mauris amet tellus gravida
							lorem ipsum dolor sit amet consequat blandit.</p>
							<footer class="controls">
								<a href="#" class="button">Oh, please continue ....</a>
							</footer>
						</section>

					</div>
				</div>
				<div class="row">
					<div class="12u">

						<div id="copyright">
							&copy; Untitled. All rights reserved. | Design: <a href="http://html5up.net">HTML5 UP</a> | Images: <a href="http://fotogrph.com">fotogrph</a>
						</div>

					</div>
				</div>
			</div>
		</div>
	<!-- ********************************************************* -->
	</body>
</html>