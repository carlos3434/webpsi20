<?php
require_once("../../cabecera.php");
$_SESSION["proyecto"] = array();
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />

        <title></title>

        <?php include ("../../includes.php") ?>    


        <link type="text/css" href='css/jquery.multiselect.css' rel="Stylesheet" />
        <link type="text/css" href='css/jquery.multiselect.filter.css' rel="Stylesheet" />
        <link rel="stylesheet" href="css/magnific-popup.css" />
        <link rel="stylesheet" href="css/easy-responsive-tabs.css" />

        <link rel="stylesheet" type="text/css" href="css/geo.css" />
        <link rel="stylesheet" type="text/css" href="css/colpick.css" />
        <link href='css/jquery-ui.css' rel="Stylesheet" />
        <link href='css/jquery-ui.theme.css' rel="Stylesheet" />
        <link href='css/flexigrid.pack.css' rel="Stylesheet" />

        <script type="text/javascript" src="js/jquery.multiselect.js"></script>
        <script type="text/javascript" src="js/jquery.multiselect.filter.js"></script>
        <script src="js/jquery.magnific-popup.min.js"></script>
        <script src="js/easyResponsiveTabs.js"></script>
        <script src="js/jquery.simple-color.min.js"></script>
        <script src="js/json2.js"></script>

        <script src="https://maps.googleapis.com/maps/api/js?libraries=geometry&v=3.exp&sensor=true"></script>
        <script src="js/geo.js"></script>
        <script src="js/geo_direcciones.js"></script>
        <script src="js/colpick.js"></script>
        <script src="js/jquery.md5.js"></script>
        <script src="js/flexigrid.pack.js"></script>

        <script src="js/geo_alarmas.js"></script>
    </head>

    <body>

        <input type="hidden" value="<?php echo $IDUSUARIO ?>" name="txt_idusuario" id="txt_idusuario"/>

        <?php echo pintar_cabecera(); ?>

        <div id="geo_wrapper">
            <div id="geo_tools">
                <!--
                <a href="" id="hideTools">
                    <img src="images/exit_panel.png" />
                </a>
                -->
                <div style="text-align: center; padding: 5px;">
                    <input type="button" id="hideToolsAlarma" class="btnMain" value="Ocultar" />
                    <input type="button" id="clearAll" class="btnMain" value="Limpiar Mapa" />
                    <input type="button" id="btnMain03" class="btnMain" value="Boton 03" />
                </div>

                <!-- Opciones del modulo -->
                <div id="accordion">
                    <h3 style="text-align: center">BUSCAR ELEMENTO</h3>
                    <div>
                        <div style="display: table">
                            <div style="display: table-row">
                                <div style="display: table-cell">
                                    <p>&nbsp;</p>
                                </div>
                            </div>
                            <div style="display: table-row">
                                <div style="display: table-cell">
                                    <select name="geo_elemento" id="geo_elemento" class="ui-selectmenu-button ui-widget ui-state-default ui-corner-all" style="width: 100%">
                                        <option value="">-Seleccione-</option>
                                    </select>
                                    <div id="geo_pattern"></div>
                                    <div id="geo_response"></div>
                                </div>
                            </div>
                            <div style="display: table-row">
                                <div style="display: table-cell">
                                    <!-- mostrar capas creadas -->
                                    <div style="display: table" id="layerCreated">
                                        
                                    </div>
                                </div>
                            </div>
                            <div style="display: table-row">
                                <div style="display: table-cell">
                                    <fieldset style="padding: 10px; display: none" class="estiloItem" id="estilo_poligono">
                                        <legend>Pol&iacute;gono</legend>
                                        <div style="float: right; position: relative; border-color: #008000" id="polyDemoCreateMapx" class="demoPolygon">
                                            <img src="images/geoPolyImageConstruct_80x80.png" id="polyDemoCreateMap" style="; z-index: 0; position: absolute" class="demoPolygon" />
                                            <p id="polyDemoCreate" style="background-color: #008000; z-index: 1;" class="demoPolygon"></p>
                                        </div>
                                        <p>Color fondo <input type="text" title="polyDemoCreate" id="pickerPolyBack" class="color-picker" value="008000" /></p>
                                        <p>Color linea <input type="text" title="polyDemoCreate" id="pickerPolyLine" class="color-picker" value="008000" /></p>
                                        <p>
                                            <label for="polyOpacCreate">Opacidad</label>
                                            <div id="sliderPolyOpac" title="polyDemoCreate" style="width: 250px"></div>
                                        </p>
                                        <p>
                                            Linea 
                                            <select name="polyLineCreate" id="polyLineCreate">
                                                <?php
                                                for ($i = 0; $i <= 10; $i++) {
                                                    echo "<option value=\"$i\">$i</option>";
                                                }
                                                ?>
                                            </select>
                                            <div></div>
                                        </p>

                                    </fieldset>

                                    <fieldset style="padding: 10px; display: none" class="estiloItem" id="estilo_punto">
                                        <legend>Punto</legend>
                                        <div class="newMarkerBackground">
                                            <img src="images/transparent_marker.png" style="; z-index: 1; position: absolute" />
                                        </div>
                                        <p>
                                            <input type="radio" name="iconSelect" value="default" checked="checked" /> Por defecto
                                        </p>
                                        <p>
                                            <input type="radio" name="iconSelect" value="numeric" /> Numerico
                                            Color fondo <input type="text" id="pickerMarkBack" class="color-picker" value="008000" />
                                        </p>
                                    </fieldset>
                                </div>
                            </div>
                            <div style="display: table-row" class="btn_drawSet">
                                <div style="display: table-cell">
                                    <div style="display: table">
                                        <div style="display: table-row">
                                            <div style="display: table-cell">
                                                Capa
                                            </div>
                                            <div style="display: table-cell">
                                                <input type="text" name="newLayerName" id="newLayerName" />
                                                <div id="newLayerNameError" style="color: #FF0000; display: none; float: right">
                                                    Ingrese nombre para la capa.
                                                </div>
                                            </div>
                                            <div style="display: table-cell">
                                                <input type="button" name="draw" id="draw" value="Dibujar Capa" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" />
                                            </div>
                                        </div>
                                        <div style="display: table-row">
                                            <div style="display: table-cell">
                                                Proyecto
                                            </div>
                                            <div style="display: table-cell">
                                                <input type="text" name="proyecto" id="proyecto" />
                                                <div id="newProjectNameError" style="color: #FF0000; display: none; float: right">
                                                    Ingrese nombre para el proyecto.
                                                </div>
                                            </div>
                                            <div style="display: table-cell">
                                                <input type="button" name="saveProject" id="saveProject" value="Guardar Proyecto" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div style="display: table-row" class="btn_drawSet">
                                <div style="display: table-cell; margin: auto">                                    
                                    <!--<input type="button" name="clearAll" id="clearAll" value="Limpiar Mapa" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" />-->
                                    <input type="button" name="newLayer" id="newLayer" value="Nueva Capa" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" />
                                </div>
                            </div>
                            <div style="display: table-row">
                                <div style="display: table-cell">
                                    <p>&nbsp;</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <h3 style="text-align: center">PROYECTOS</h3>
                    <div>
                        <div id="tabs">
                            <ul>
                                <li>
                                    <a href="#tabs-1">Lista</a>
                                </li>
                                <li>
                                    <a href="#tabs-2" class="currentProject">Capas</a>
                                </li>
                            </ul>
                            <div id="tabs-1">
                                <table class="flexme4" style="display: none"></table>
                            </div>
                            <div id="tabs-2">
                                <div id="currentProject"></div>
                            </div>
                        </div>
                    </div>
                    <h3 style="text-align: center">ALARMAS TAPS</h3>
                    <div>
                        <p>
                            <input type="checkbox" name="show_alarma_tap" id="show_alarma_tap" value="ok" />
                            Mostrar Alarma
                        </p>
                        <p>
                            <input type="checkbox" name="interval_alarma_tap" id="interval_alarma_tap" value="ok" />
                            Actualizar cada 3 minutos.
                        </p>
                        <p>&nbsp;</p>
                        <!--
                        <h3>Configuraci&oacute;n</h3>
                        <p>1 - 2 clientes por troba (verde)</p>
                        <p>3 - 5 clientes por troba (amarillo)</p>
                        <p>6 - 9 clientes por troba (naranja)</p>
                        <p>10+ clientes por troba (rojo)</p>
                        -->
                        <hr />
                        
                        <div id="listaTroba" style="max-height: 200px; overflow: auto">
                            <h4>Lista de trobas</h4>
                        </div>
                        <div id="detalleTroba" style="max-height: 200px; overflow: auto">
                            <h4>Detalle de troba</h4>
                        </div>
                    </div>
                    <!--
                    <h3 style="text-align: center">CAPAS</h3>
                    <div>
                        <p>Sed non urna. Donec et ante. Phasellus eu ligula. Vestibulum sit amet purus. Vivamus hendrerit, dolor at aliquet laoreet, mauris turpis porttitor velit, faucibus interdum tellus libero ac justo. Vivamus non quam. In suscipit faucibus urna. </p>
                    </div>
                    <h3 style="text-align: center">DIRECCIONES</h3>
                    <div>
                        <div id="addressTabs">
                            <ul>
                                <li>
                                    <a href="#addressTabs-1">B&uacute;squeda individual</a>
                                </li>
                                <li>
                                    <a href="#addressTabs-2" class="currentProject">B&uacute;squeda masiva</a>
                                </li>
                                <li>
                                    <a href="#addressTabs-3" class="currentProject">Tramos</a>
                                </li>
                            </ul>
                            <div id="addressTabs-1">
                                <p>Nombre, Direcci&oacute;n, distrito, ciudad, pais</p>
                                <input type="text" name="address" id="address" size="40" />
                                <input type="button" name="buscadir" id="buscadir" value="Buscar" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" />
                            </div>
                            <div id="addressTabs-2">
                                <input type="file" name="dirfile" id="dirfile" />
                                <input type="button" name="updirfile" id="updirfile" value="Buscar" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" />
                                <div id="addressControl"></div>
                                <div id="address_result" style="margin: 8px; max-height: 250px; overflow: auto"></div>
                            </div>
                            <div id="addressTabs-3">
                                <form name="geo_trama_form" id="geo_trama_form" action="geo.main.php" method="post">
                                <div style="display: table">
                                    <div style="display:table-row">
                                        <div style="display: table-cell">
                                            Distrito
                                        </div>
                                        <div style="display: table-cell">
                                            Calle / Avenida / Jir&oacute;n
                                        </div>
                                        <div style="display: table-cell">
                                            N&uacute;mero
                                        </div>
                                    </div>
                                    <div style="display:table-row">
                                        <div style="display: table-cell">
                                            <select name="geo_trama_distrito" id="geo_trama_distrito">
                                                <option value="150100"> - Todo Lima - </option>
                                                <option value="150101">Lima Cercado</option>
                                                <option value="150102">Ancon</option>
                                                <option value="150103">Ate</option>
                                                <option value="150104">Barranco</option>
                                                <option value="150105">Breña</option>
                                                <option value="150106">Carabayllo</option>
                                                <option value="150107">Chaclacayo</option>
                                                <option value="150108">Chorrillos</option>
                                                <option value="150109">Cieneguilla</option>
                                                <option value="150110">Comas</option>
                                                <option value="150111">El Agustino</option>
                                                <option value="150112">Independencia</option>
                                                <option value="150113">Jesus María</option>
                                                <option value="150114">La Molina</option>
                                                <option value="150115">La Victoria</option>
                                                <option value="150116">Lince</option>
                                                <option value="150117">Los Olivos</option>
                                                <option value="150118">Lurigancho</option>
                                                <option value="150119">Lurin</option>
                                                <option value="150120">Magdalena del Mar</option>
                                                <option value="150121">Pueblo Libre</option>
                                                <option value="150122">Miraflores</option>
                                                <option value="150123">Pachacamac</option>
                                                <option value="150124">Pucusana</option>
                                                <option value="150125">Puente Piedra</option>
                                                <option value="150126">Punta Hermosa</option>
                                                <option value="150127">Punta Negra</option>
                                                <option value="150128">Rimac</option>
                                                <option value="150129">San Bartolo</option>
                                                <option value="150130">San Borja</option>
                                                <option value="150131">San Isidro</option>
                                                <option value="150132">San Juan De Lurigancho</option>
                                                <option value="150133">San Juan de Miraflores</option>
                                                <option value="150134">San Luis</option>
                                                <option value="150135">San Martin de Porres</option>
                                                <option value="150136">San Miguel</option>
                                                <option value="150137">Santa Anita</option>
                                                <option value="150138">Santa Maria del Mar</option>
                                                <option value="150139">Santa Rosa</option>
                                                <option value="150140">Santiago de Surco</option>
                                                <option value="150141">Surquillo</option>
                                                <option value="150142">Villa el Salvador</option>
                                                <option value="150143">Villa Maria del Triunfo</option>
                                                <option value="070100"> - Todo Callao - </option>
                                                <option value="070101">Callao</option>
                                                <option value="070102">Bellavista</option>
                                                <option value="070103">Carmen de la Legua Reynoso</option>
                                                <option value="070104">La Perla</option>
                                                <option value="070105">La Punta</option>
                                                <option value="070106">Ventanilla</option>
                                            </select>
                                        </div>
                                        <div style="display: table-cell">
                                            <input type="text" name="geo_trama_calle" id="geo_trama_calle" size="12" />
                                        </div>
                                        <div style="display: table-cell">
                                            <input type="text" name="geo_trama_numero" id="geo_trama_numero" size="12" />
                                        </div>
                                    </div>
                                </div>                                    
                                <table>
                                    <tr>
                                        <td>
                                            Color l&iacute;nea
                                        </td>
                                        <td>
                                            <input type="text" title="polyDemoCreate" id="pickerAddressLine" class="color-picker" value="008000" />
                                        </td
>                                    </tr>
                                    <tr>
                                        <td>
                                            L&iacute;nea
                                        </td>
                                        <td>
                                            <select name="polyLineAddress" id="polyLineAddress">
                                            <?php
                                            for ($i = 2; $i <= 8; $i++) {
                                                echo "<option value=\"$i\">$i</option>";
                                            }
                                            ?>
                                            </select>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                            <input type="checkbox" name="showPathMarker" id="showPathMarker" value="ok" />
                                            Mostrar inicio y fin del tramo
                                        </td>
                                    </tr>
                                </table>
                                <input type="submit" name="trama_buscar" id="trama_buscar" value="Buscar direcci&oacute;n" style="width: 100%" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" />
                                </form>
                                
                                <p>
                                    <form action="" name="faddressmass" id="faddressmass" method="post">
                                        <input type="file" name="adrsfile" id="adrsfile" />
                                        <input type="submit" name="adrs_buscar" id="adrs_buscar" value="Buscar direcciones" style="width: 100%" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" />
                                        <div id="adrsControl"></div>
                                        <div id="adrs_result" style="margin: 8px; max-height: 250px; overflow: auto"></div>
                                    </form>
                                </p>
                            </div>
                        </div>
                    </div>
                    <h3 style="text-align: center">KML / KMZ</h3>
                    <div>
                        <p>
                            <a title="file0" href="http://zoolbox.com/kmlkmz/_punto.kml" class="getFileLayer">_punto</a>
                        </p>
                        <p>
                            <a title="file1" href="http://zoolbox.com/kmlkmz/_Resto_2014.kml" class="getFileLayer">_Resto_2014</a>
                        </p>
                        <p>
                            <a title="file2" href="http://zoolbox.com/kmlkmz/_Trobas_M1_93_2014.kml" class="getFileLayer">_Trobas_M1_93_2014</a>
                        </p>
                        <p>
                            <a title="file3" href="http://zoolbox.com/kmlkmz/_Trobas_M1_94_2014.kml" class="getFileLayer">_Trobas_M1_94_2014</a>
                        </p>
                        <p>
                            <a title="file4" href="http://zoolbox.com/kmlkmz/_Trobas_M1_resto_de_las_457.kml" class="getFileLayer">_Trobas_M1_resto_de_las_457</a>
                        </p>
                        <p>
                            <a title="file5" href="http://zoolbox.com/kmlkmz/_uranac.kml" class="getFileLayer">_uranac</a>
                        </p>
                        <p>
                            <a title="file6" href="http://zoolbox.com/kmlkmz/_uraNac.kmz" class="getFileLayer">_uraNac</a>
                        </p>
                        <p>
                            <a title="file7" href="http://zoolbox.com/kmlkmz/_Trobas_Lima.kml" class="getFileLayer">_Trobas_Lima</a>
                        </p>
                        <p>
                            <a title="file8" href="http://zoolbox.com/kmlkmz/_Trobas_M1_2013.kml" class="getFileLayer">_Trobas_M1_2013</a>
                        </p>
                        <p>
                            <a title="file9" href="http://zoolbox.com/kmlkmz/_Zona_HFC.kml" class="getFileLayer">_Zona_HFC</a>
                        </p>
                    </div>
                    -->
                </div>

            </div>
            <div id="geo_map"></div>
            <div id="btn_tools">
                <a href="" id="showToolsAlarma">
                    <img src="images/map_edit.png" />
                </a>
            </div>
        </div>
        
        <!-- ID de proyecto actual -->
        <input type="hidden" name="proIdEdit" id="proIdEdit" value="" />
        
        <!-- Modal loading effect -->
        <div class="modalPop"></div>
        
        <!-- Herramientas de estilo como cuadros de dialogo -->
        <fieldset style="padding: 10px; display: none" class="estiloItem" id="estilo_poligono_edit" title="Estilo pol&iacute;gono">
            <div style="float: right; position: relative; border-color: #008000" id="polyDemoCreateMapx_edit" class="demoPolygon">
                <img src="images/geoPolyImageConstruct_80x80.png" id="polyDemoCreateMap_edit" style="; z-index: 0; position: absolute" class="demoPolygon" />
                <p id="polyDemoCreate_edit" style="background-color: #008000; z-index: 1;" class="demoPolygon"></p>
            </div>
            <p>Color fondo <input type="text" title="polyDemoCreate_edit" id="pickerPolyBack_edit" class="color-picker" value="008000" /></p>
            <p>Color linea <input type="text" title="polyDemoCreate" id="pickerPolyLine_edit" class="color-picker" value="008000" /></p>
            <p>
                <label for="polyOpacCreate">Opacidad</label>
                <div id="sliderPolyOpac_edit" title="polyDemoCreate_edit" style="width: 250px"></div>
            </p>
            <p>
                Linea 
                <select name="polyLineCreate_edit" id="polyLineCreate_edit">
                    <?php
                    for ($i = 0; $i <= 10; $i++) {
                        echo "<option value=\"$i\">$i</option>";
                    }
                    ?>
                </select>
                <div></div>
            </p>

        </fieldset>

        <fieldset style="padding: 10px; display: none" class="estiloItem" id="estilo_punto_edit" title="Estilo punto">
            <div class="newMarkerBackground">
                <img src="images/transparent_marker.png" style="; z-index: 1; position: absolute" />
            </div>
            <p>
                <input type="radio" name="iconSelect_edit" value="default" checked="checked" /> Por defecto
            </p>
            <p>
                <input type="radio" name="iconSelect_edit" value="numeric" /> Numerico
                Color fondo <input type="text" id="pickerMarkBack_edit" class="color-picker" value="008000" />
            </p>
        </fieldset>
        
        

    </body>
</html>