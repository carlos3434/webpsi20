<?php 
require_once("../../cabecera.php");
$_SESSION["proyecto"] = array();

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />

<title></title>

<?php include ("../../includes.php") ?>    


<link type="text/css" href='../../css/jquery.multiselect.css' rel="Stylesheet" />
<link rel="stylesheet" href="css/magnific-popup.css" />
<link rel="stylesheet" href="css/easy-responsive-tabs.css" />
<link href='../../js/jqueryui_1.8.2/css/redmond/jquery-ui-1.8.1.custom.css' rel="Stylesheet" />


<script type="text/javascript" src="../../js/jquery.multiselect.js"></script>
<script src="js/jquery.magnific-popup.min.js"></script>
<script src="js/easyResponsiveTabs.js"></script>
<script src="js/jquery.simple-color.min.js"></script>
<script src="js/json2.js"></script>



    <style>
        body, html {
            height: 100%;
            width: 100%;
        }
        #map-canvas {
            width: 100%; height: 100%;
        }
        #map-over {
            position: absolute; 
            top: 40px; 
            left: 80px; 
            z-index: 999;
            background-color: #FFFFFF;
        }
        #map-wrapper { position: relative; height: 100%}
        .white-popup {
            position: relative;
            background: #FFF;
            padding: 20px;
            width: auto;
            max-width: 580px;
            margin: 20px auto;
        }
        .map-ctrl-container {
            margin: 2px auto;
            position: relative;
        }
        .map-ctrl-block {
            background: #DFDBD4;
            height: 25px;
            width: 100px;
            float: left;
            margin: 3px;
            color: #000000;
            text-align: center;
            vertical-align: middle;
        }
        #map-ctrl-inherit select option { font-size: 10pt; }
        .itemSelect {
            background-color: #e1e3ed;
            margin: 5px 2px 0px 2px;
            padding: 5px;
        }
        .departamento {background-color: #00527A; color: #FFFFFF; margin: 5px, 0px, 5px, 0px; padding: 7px;}
        .provincia {background-color: #0085C7; margin: 5px, 0px, 5px, 0px; padding: 7px;}
        .distrito {background-color: #FFFFFF; color: #00527A; margin: 5px, 0px, 5px, 0px; padding: 7px;}
        .itemSelected {background-color: #C76300; color: #FFFFFF;}

        /*.geo_item {display: none; float: left};*/
        .geo_item_active {display: block; float: left};

        #slider { width: 200px; }

        .simpleColorDisplay {
            width: 100px;
            height: 30px;
        }
        
      </style>
      <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=true"></script>
      <script>
    // Note: This example requires that you consent to location sharing when
    // prompted by your browser. If you see a blank space instead of the map, this
    // is probably because you have denied permission for location sharing.

    var map;
    var data_polygon = "";
    var polygonCoords = [];
    var markers = [];
    var markerArray = [];
    var polygonArray = [];
    var bounds;
    var capasArray = [];
    var polygonObject;
    var mapObjects = new Array();
    var projectLayers = new Array();

    //Variables funcionales
    var geoVal;

    //Tamaño de objeto javascript
    Object.size = function(obj) {
        var size = 0, key;
        for (key in obj) {
            if (obj.hasOwnProperty(key)) size++;
        }
        return size;
    };

    //Genera y obtiene color aleatorio
    function getRandomColor () {
        var hex = Math.floor(Math.random() * 0xFFFFFF);
        return "#" + ("000000" + hex.toString(16)).substr(-6);
    }

    //Inicializar Google Maps
    function initialize() {
        bounds = new google.maps.LatLngBounds();
      var mapOptions = {
        zoom: 10
      };
      map = new google.maps.Map(document.getElementById('map-canvas'),
          mapOptions);

      // Try HTML5 geolocation
      if(navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(function(position) {
          var pos = new google.maps.LatLng(position.coords.latitude,
                                           position.coords.longitude);
          /*
          var infowindow = new google.maps.InfoWindow({
            map: map,
            position: pos,
            content: 'Location found using HTML5.'
          });
        */
          map.setCenter(pos);
        }, function() {
          handleNoGeolocation(true);
        });
      } else {
        // Browser doesn't support Geolocation
        handleNoGeolocation(false);
      }
    }


    function handleNoGeolocation(errorFlag) {
      if (errorFlag) {
        var content = 'Error: The Geolocation service failed.';
      } else {
        var content = 'Error: Your browser doesn\'t support geolocation.';
      }

      var options = {
        map: map,
        position: new google.maps.LatLng(60, 105),
        content: content
      };

      var infowindow = new google.maps.InfoWindow(options);
      map.setCenter(options.position);
    }

    function polygonDo(polygon){
        polygonObject = polygon;
        //Opciones de edicion poligono
        $("#colorPickerPoligonoEdit").val(polygon.fillColor);
        
        $.magnificPopup.open({
            items: {
                src: '#polygonOptions', // can be a HTML string, jQuery object, or CSS selector
                type: 'inline'
            }
        });
        //Caracteristicas del color picker
        $(".simpleColorDisplay").css("width", "100px");
        $(".simpleColorDisplay").css("height", "30px");
        $(".simpleColorDisplay").css("background-color", polygon.fillColor);
        $(".simpleColorDisplay").html(polygon.fillColor);
        $("#box").css("background-color", polygon.fillColor);

    }

    google.maps.event.addDomListener(window, 'load', initialize);

    $(document).ready(function (){
        $('.open-map-ctrl').magnificPopup({
          type:'inline',
          midClick: true // Allow opening popup on middle mouse click. Always set it to true if you don't provide alternative source in href.
        });
        $('.polygon-options').magnificPopup({
          type:'inline'
        });
        $('#map-tab').easyResponsiveTabs();

        $("#map-ctrl").mouseover(function (){
            $(this).css("opacity", 1);
        });
        $("#map-ctrl").mouseout(function (){
            $(this).css("opacity", 0.2); 
        });

        $('#cascade1').hide();

        $('.simple_color_color_code').simpleColor({ displayColorCode: true });

        //Guardar cambios realizados al poligono
        $("#savePolygon").click(function (){
            polygonObject.setMap(null);
            //Color de fondo
            polygonObject.fillColor = $("#colorPickerPoligonoEdit").val();
            polygonObject.strokeColor = $("#colorPickerPoligonoEdit").val();
            //Opacidad
            polygonObject.fillOpacity = $(".simpleColorDisplay").css("opacity");
            //Dibujar en mapa
            polygonObject.setMap(map);
        });

        data_content = "action=geoElementos";
        $.ajax({
            type:   "POST",
            url:    "georeferencia.request.php",
            data:   data_content,
            dataType: 'json',
            success: function(datos) {
                $.each(datos, function() {
                    $('<option>').val(this.id + "_" + this.tipo + "_" + this.nombre).text(this.descripcion).appendTo('#geo_elemento');
                });
            }
        });

        //Listas, seleccion de elementos y dependencias
        $("#geo_elemento").change(function (){
            //var origen = $("#geo_elemento option:selected").text();
            var dependencia = "";
            geoVal = $("#geo_elemento").val().split("_");
            var origen = geoVal[2];

            //Ocultar o mostrar bloque de estilos
            $(".estiloItem").hide();
            $("#estilo_" + geoVal[1]).show();

            data_content = "action=getGeoDedependencia&elemento_id=" + geoVal[0] + "&origen=" + origen;
            $.ajax({
                type:   "POST",
                url:    "georeferencia.request.php",
                data:   data_content,
                success: function(datos) {
                    $("#geo_response").html(datos);

                    $(".geo_item").change(function (){
                        var title = $(this).attr("title");
                        var call = title.split("_");
                        var dep = "";
                        var itemId;

                        if ( $(this).val() != "" ) {
                            itemId = $(this).attr("id");

                            var itemIndex = call.indexOf( itemId.substring(4) );

                            //$(".ui-multiselect").remove();
                            
                            $.each(call, function(id, val){
                                if (id > itemIndex) {
                                    $('#geo_' + val).find('option').remove()
                                }
                            });

                            $.each(call, function(id, val){
                                
                                if ( $("#geo_" + val + " option").length > 0 ) {
                                    dep += "___" + $("#geo_" + val).val();
                                }

                                if ( $("#geo_" + val + " option").length == 0 ) {

                                    dep = dep.substring(3);
                                    data_content = "action=getGeoItem&element=" 
                                                    + val 
                                                    + "&dependencia=" 
                                                    + dep 
                                                    + "&origen=" 
                                                    + origen;
                                    $.ajax({
                                        type:   "POST",
                                        url:    "georeferencia.request.php",
                                        data:   data_content,
                                        success: function(dato) {
                                            $("#geo_" + val).html(dato);
                                            if (val == call[ call.length -1 ]) {
                                                
                                                $("#geo_" + val).multiselect();
                                                $("#geo_" + val).multiselect('refresh');
                                            }
                                        }
                                    });

                                    return false;
                                }
                            });
                        }
                    });
                }
            });
        });        

        //Dibujar poligono o punto
        $("#btnDrawItem").click(function (){
            geoVal = $("#geo_elemento").val().split("_");
            //var origen = $("#geo_elemento option:selected").text();
            var origen = geoVal[2];
            
            var title = $("#geo_" + origen).attr("title");
            var call = title.split("_");
            var items = "";
            var dep = "";

            //geoVal = $("#geo_elemento").val().split("_");

            //Color fijo o aleatorio
            var colorType = $('input[name=colorSelect]:checked').val();
            var colorSelected;

            var icontype = $('input[name=iconSelect]:checked').val();
            var iconSelected;

            var colorLineSelected;

            var timeStamp = new Date().getTime();

            
            $.each(call, function(id, val){

                if (val != call[ call.length -1 ]) {
                    dep += "___" + $.trim( $("#geo_" + val).val() );
                }
                  
            });
            dep = dep.substring(3);

            //Objeto poligono
            var polygon;
            var idFirst;
            var idLast;
            var idCount;

            items = $("#geo_" + origen).multiselect("getChecked").map(function(){
                       return this.value;
                    }).get();

            if ( geoVal[1] == 'poligono' ) {
                $.each(items, function(key, value){
                    //Enviar peticion y respuesta json
                    data_polygon = "action=drawPolygon&origen=" + origen + "&data=" + dep + "&item=" + value;
                    $.ajax({
                        type:   "POST",
                        url:    "georeferencia.request.php",
                        data:   data_polygon,
                        dataType: 'json',
                        success: function(datos) {
                            
                            idCount = 1;
                            $.each(datos, function() {
                                if (idCount == 1) {
                                    idFirst = this.id;
                                } else {
                                    idLast = this.id;
                                }

                                var pt = new google.maps.LatLng(this.coord_y, this.coord_x);
                                bounds.extend(pt);
                                polygonCoords.push( pt );
                                idCount++;
                            });

                            //Color de fondo seleccionado
                            if (colorType == "static") {
                                colorSelected = $("#colorPickerPoligono").val();
                            } else if (colorType == "random") {
                                colorSelected = getRandomColor();
                            }

                            //Color de linea
                            if ($('#colorLine').is(':checked')) {
                                colorLineSelected = $("#colorPickerLine").val();
                            } else {
                                //console.log("KO Checkbox");
                                colorLineSelected = colorSelected;
                            }

                            // Construct the polygon.
                            polygon = new google.maps.Polygon({
                                paths: polygonCoords,
                                strokeColor: colorLineSelected,
                                strokeOpacity: 0.8,
                                strokeWeight: 2,
                                fillColor: colorSelected,
                                fillOpacity: $(".simpleColorDisplay").css("opacity")
                            });
                            
                            var valueToPush = {};
                            //valueToPush["tipo"]     = "poligono";
                            //valueToPush["origen"]   = origen;
                            //valueToPush["dep"]      = dep;
                            //valueToPush["name"]     = value;                            
                            valueToPush["objeto"]   = polygon;
                            mapObjects[timeStamp + "___poli___" + origen + "|^" + dep + "___" + value] = polygon;

                            //Agregar capa al proyecto
                            projectLayers[timeStamp + "___poli___" + origen + "|^" + dep] = origen + " en " + dep;

                            //console.log(mapObjects);
                            valueToPush = null;
                            /*
                            var dataObject = "action=saveObject&dep=" 
                                            + dep 
                                            + "&origen=" + origen 
                                            + "&item=" + value 
                                            + "&tipo=poligono&obj=" + JSON.stringify(polygon)
                            $.ajax({
                                type: "POST",
                                url: "georeferencia.request.php",
                                data: dataObject,
                                dataType: 'json',
                                success: function(datos){
                                    console.log(datos);
                                }
                            });
                            */
                            google.maps.event.addListener(polygon, 'click', function() {
                                polygonDo(this);
                            });

                            polygon.setMap(map);
                            map.fitBounds(bounds);

                            polygonArray.push(polygon);
                            
                            polygon = null;
                            polygonCoords = [];

                            capasArray.push(
                                "poligono," 
                                + origen 
                                + "," + value 
                                + "," + colorSelected 
                                + "," + idFirst 
                                + "," + idLast
                            );                            
                        }
                    });
                });
            } else if ( geoVal[1] == 'punto' ) {
                var markerNumber = 1;
                $.each(items, function(key, value){
                    //Enviar peticion y respuesta json
                    data_polygon = "action=drawMarker&origen=" + origen + "&data=" + dep + "&item=" + value;
                    $.ajax({
                        type:   "POST",
                        url:    "georeferencia.request.php",
                        data:   data_polygon,
                        dataType: 'json',
                        success: function(datos) {
                            //console.log(this.coord_x);
                            $.each(datos, function() {
                                var pt = new google.maps.LatLng(this.coord_y, this.coord_x);
                                bounds.extend(pt);
                                markers.push(pt);

                                if (icontype == "default") {
                                    iconSelected = "";
                                } else if (icontype == "numeric") {
                                    iconSelected = "http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld="+markerNumber + "|" + $("#colorPickerPunto").val().substring(1) + "|000000";
                                }

                                var marker = new google.maps.Marker({
                                    position: pt,
                                    map: map,
                                    icon: iconSelected
                                });
                                marker.metadata = {type: "point", id: markerNumber};

                                var valueToPush = {};
                                //valueToPush["tipo"]     = "poligono";
                                //valueToPush["origen"]   = origen;
                                //valueToPush["dep"]      = dep;
                                //valueToPush["name"]     = value;                            
                                //valueToPush["objeto"]   = polygon;
                                mapObjects[timeStamp + "___punto___" + origen + "|^" + dep + "___" + value] = marker;

                                //Agregar capa al proyecto
                                projectLayers[timeStamp + "___punto___" + origen + "|^" + dep] = origen + " en " + dep;

                                //console.log(mapObjects);
                                valueToPush = null;


                                /*
                                var dataObject = "action=saveObject&dep=" 
                                                + dep 
                                                + "&origen=" + origen 
                                                + "&item=" + value 
                                                + "&tipo=punto&obj=" + JSON.stringify(polygon)
                                $.ajax({
                                    type: "POST",
                                    url: "georeferencia.request.php",
                                    data: dataObject,
                                    dataType: 'json',
                                    success: function(datos){
                                        console.log(datos);
                                    }
                                });
                                */
                                
                                markerArray.push(marker);
                                marker.setMap(map);
                                markerNumber++;
                            });
                            map.fitBounds(bounds);
                        }
                    });
                });
            }
            //console.log(mapObjects);
        });

        $("#btnSaveLayout").click(function (event){

            //Validar existencia de elementos en el mapa
            if ( Object.size( mapObjects ) == 0 || Object.size( projectLayers ) == 0 ) {
                alert("El mapa no contiene elementos.");
                return false;
            }

            //Validar campo de nombre de proyecto no vacío
            if ( $.trim( $("#proyecto").val() ) == "" ) {
                alert("Debe ingresar un nombre para el proyecto.");
                $("#proyecto").val("");
                $("#proyecto").css("background-color", "#ffcccc");
                $("#proyecto").focus();
                return false;
            }
            

            var capas = "";
            var itemSet = "";
            var timeKey;
            for (key in mapObjects) {
                if (mapObjects.hasOwnProperty(key)) {

                    var ext = key.split("___");
                    if (ext[1] == "poli") {
                        itemSet = mapObjects[key].fillColor 
                                  + "___" 
                                  + mapObjects[key].fillOpacity
                                  + "___"
                                  + mapObjects[key].strokeColor
                    } else if (ext[1] == "punto") {
                        var iconSet = mapObjects[key].icon.split("&");
                        itemSet = iconSet[1];
                    }

                    capas += "," + key + "[{chars}]" + itemSet;
                }
            }


            var dataObject = "action=saveObject&proyecto=" 
                              + $("#proyecto").val() 
                              + "&capas=" + capas.substring(1)
                              + "&keyProject=" + $("#proIdEdit").val();
            $.ajax({
                type: "POST",
                url: "georeferencia.request.php",
                data: dataObject,
                dataType: 'json',
                success: function(datos){
                    alert( datos.msg );
                }
            });
        });

        //Elimina todos los elementos del mapa y de memoria (script)
        $("#btnClearItem").click(function (){
            for (var i = 0; i < markerArray.length; i++) {
                markerArray[i].setMap(null);
            }
            for (var i = 0; i < polygonArray.length; i++) {
                polygonArray[i].setMap(null);
            }
            //Limpiar limites del mapa
            bounds          = new google.maps.LatLngBounds();
            //Limpiar elementos del mapa
            mapObjects      = new Array();
            //Limpiar elementos del proyecto
            projectLayers   = new Array();
            //Limpiar campo de nombre del proyecto
            $("#proyecto").val("");
            //Limpiar proyecto
            $("#proIdEdit").val(0);
            //Ocultar bloques de edicion
            $("#estilo_punto_edit").hide();
            $("#estilo_poligono_edit").hide();
            //ELiminar titulo del proyecto
            $(".projectTitle").html("");
        });

        $("#btnLayout").click(function (){

            //limpiar layout
            $("#layoutList").html("");

            //mostrar capas generadas
            for (var i=0; i<capasArray.length; i++) {
                $("#layoutList").append( "<div id=\"capa_" + i + "\" title=\""+i + "," + capasArray[i]+"\"><a href=\"" 
                    + i  
                    + "\" class=\"actionLayout\" title=\"edit\">[E]</a><a href=\"" 
                    + i  
                    + "\" class=\"actionLayout\" title=\"drop\">[X]</a>" 
                    + capasArray[i] + "</div>" );
            }

            $(".actionLayout").click(function (event){
                event.preventDefault();
                var href = $(this).attr("href");
                var title = $("#capa_" + href).attr("title");
                var datos = title.split(",");
                var action = $(this).attr("title");
                
                //eliminar elemento del mapa
                if (action=="drop") {
                    polygonArray[ datos[0] ].setMap(null);
                    //eliminar elemento del DOM
                    $("#capa_" + datos[0]).remove();
                    //eliminar elemento del array
                    capasArray.splice(datos[0], 1);
                    //polygonArray.splice(datos[0], 1);
                } else if (action=="edit") {

                }
            });
        });

        //Muestra lista de Proyectos almacenados en DB
        $(".projectDbList").click(function (event){
            var dataObject = "action=doListLayer"
            $.ajax({
                type: "POST",
                url: "georeferencia.request.php",
                data: dataObject,
                success: function(datos){
                    
                    //Mostrar lista de proyectos contenidos en "datos"
                    $("#layoutList").html(datos);

                    //Eliminar proyecto
                    $(".deleteProject").click(function (event){
                        event.preventDefault();
                        //Project Key
                        var idProject = $(this).attr("href");

                        var conf = confirm(
                                        "¿Desea eliminar el proyecto: "
                                        + $(".loadProject[href=" + idProject + "]").html()
                                        + "?"
                                    );

                        if (conf) {
                            dataObject = "action=deleteProject&keyProject=" + idProject;
                            $.ajax({
                                type: "POST",
                                url: "georeferencia.request.php",
                                data: dataObject,
                                dataType: 'json',
                                success: function(datos){
                                    alert( datos.msg );
                                    //Recargar lista de proyectos
                                    $(".projectDbList").click();
                                }
                            });
                        }
                        
                    });

                    //Al hacer click en un proyecto generar elementos
                    $(".loadProject").click(function (event){
                        //Invocar al evento que elimina todo del mapa y memoria
                        $("#btnClearItem").click();
                        //Poblar caja de texto con nombre del proyecto
                        $("#proyecto").val( $(this).html() );
                        //Ocultar bloques de edicion
                        $("#estilo_punto_edit").hide();
                        $("#estilo_poligono_edit").hide();

                        event.preventDefault();
                        //Project Key
                        var idProject = $(this).attr("href");
                        //Poblar Key proyecto
                        $("#proIdEdit").val(idProject);
                        //Nombre del proyecto en el bloque de edición
                        $(".projectTitle").html( $(".loadProject[href=" + idProject + "]").html() );

                        //recuperar datos de puntos y/o poligonos
                        var dataObjects = "action=getProjectLayer&id=" + idProject
                        $.ajax({
                            type: "POST",
                            url: "georeferencia.request.php",
                            data: dataObjects,
                            dataType: 'json',
                            success: function(datos){
                                polygonCoords = [];
                                //bounds (limites del mapa por coordenadas)
                                $.each(datos, function(){
                                    //Si el elemento generado es un poligono
                                    if (this.tipo == "poli") {

                                        var xy = this.coords;

                                        //draw Polygon
                                        $.each(xy, function() {
                                            var pt = new google.maps.LatLng(this.coord_y, this.coord_x);
                                            bounds.extend(pt);
                                            polygonCoords.push( pt );
                                        });
                                        var poliColor = this.chars.split("___");

                                        // Construct the polygon.
                                        polygon = new google.maps.Polygon({
                                            paths: polygonCoords,
                                            strokeColor: poliColor[2],
                                            strokeOpacity: 0.8,
                                            strokeWeight: 2,
                                            fillColor: poliColor[0],
                                            fillOpacity: poliColor[1]
                                        });
                                        
                                        //Agregar capa al mapa
                                        mapObjects[this.layer 
                                                    + "___" 
                                                    + this.tipo 
                                                    + "___" 
                                                    + this.origen 
                                                    + "|^" 
                                                    + this.datos 
                                                ] = polygon;
                                        //Agregar capa al proyecto
                                        projectLayers[this.layer 
                                                    + "___" 
                                                    + this.tipo 
                                                    + "___" 
                                                    + this.origen 
                                                    + "|^" 
                                                    + this.capa
                                                ] = this.origen + " en " + this.capa;
                                        
                                        google.maps.event.addListener(polygon, 'click', function() {
                                            polygonDo(this);
                                        });

                                        polygon.setMap(map);
                                        map.fitBounds(bounds);

                                        polygonArray.push(polygon);

                                        polygon = null;
                                        polygonCoords = [];

                                    } else if (this.tipo == "punto") {
                                        var xy = this.coords;
                                        var itemIcon = this.chars;

                                        var puntoOrigen = this.origen;
                                        var puntoLayer = this.layer;
                                        var puntoDatos = this.datos;
                                        var puntoCapa = this.capa;
                                        var puntoExt = this.chars.split("|");

                                        markerNumber = 1;
                                        $.each(xy, function() {
                                            var pt = new google.maps.LatLng(this.coord_y, this.coord_x);
                                            bounds.extend(pt);
                                            markers.push(pt);

                                            if (itemIcon == "undefined") {
                                                iconSelected = "";
                                            } else {
                                                iconSelected = "http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=" + itemIcon;
                                            }

                                            var marker = new google.maps.Marker({
                                                position: pt,
                                                map: map,
                                                icon: iconSelected
                                            });
                                            //if ( itemIcon == "undefined" ) {
                                                marker.metadata = {type: "point", id: puntoExt[0]};
                                            //}

                                            //Agregar capa al mapa
                                            mapObjects[puntoLayer 
                                                        + "___" 
                                                        + "punto" 
                                                        + "___" 
                                                        + puntoOrigen 
                                                        + "|^" 
                                                        + puntoDatos 
                                                    ] = marker;

                                            //Agregar capa al proyecto
                                            projectLayers[puntoLayer 
                                                        + "___" 
                                                        + "punto" 
                                                        + "___" 
                                                        + puntoOrigen 
                                                        + "|^" 
                                                        + puntoCapa
                                                    ] = puntoOrigen + " en " + puntoCapa;

                                                markerArray.push(marker);                                            
                                                marker.setMap(map);
                                                marker = null;
                                                markerNumber++;

                                        });
                                        map.fitBounds(bounds);

                                    }
                                });
                            }
                        });

                    });
                }
            });
        });

        $(".currentProject").click(function(event){
            $("#currentProject").html("");

            //Dividiendo informacion
            var pjLayers = new Array();
            var pjOrigenData;
            var pjOrigen;
            var pjData;
            var pjLayerArray;
            var pjLayer;
            var htmlProject = "<div style=\"display: table;\">"
                                    + "<div style=\"display: table-row;\">"
                                        + "<div style=\"display: table-cell; text-align: center\">Capa</div>"
                                        + "<div style=\"display: table-cell; text-align: center\">Eliminar</div>"
                                        + "<div style=\"display: table-cell; text-align: center\">Editar</div>"
                                    + "</div>";

            for (key in projectLayers) {
                if (projectLayers.hasOwnProperty(key)) {

                    var formattedLayer = projectLayers[key];
                    formattedLayer=formattedLayer.replace("___", ", ");

                    htmlProject += "<div style=\"display: table-row;\">"
                                        + "<div id=\"" + key + "\" style=\"display: table-cell;\">"
                                            + formattedLayer 
                                        + "</div>"
                                        + "<div style=\"display: table-cell; text-align: center\">"
                                            + "<a href=\"drop\" title=\"" + key + "\" class=\"onProjectLayer\">[X]</a>"
                                        + "</div>"
                                        + "<div style=\"display: table-cell; text-align: center\">"
                                            + "<a href=\"edit\" title=\"" + key + "\" class=\"onProjectLayer\">[E]</a>"
                                        + "</div>"
                                    + "</div>";
                    /*
                    $("#currentProject").append(
                                    "<div id=\"" + key + "\">" 
                                    + projectLayers[key] 
                                    + "<a href=\"drop\" title=\"" + key + "\" class=\"onProjectLayer\">[X]</a>" 
                                    + "<a href=\"edit\" title=\"" + key + "\" class=\"onProjectLayer\">[E]</a>"
                                    + "</div>");*/
                }
            }
            htmlProject += "</div>";
            $("#currentProject").append(htmlProject);

            $(".onProjectLayer").click(function (event){
                event.preventDefault();
                var action = $(this).attr("href");
                var keyLayer = $(this).attr("title");

                $("#keyEdit").val( keyLayer );

                if ( action == "drop" ) {
                    
                    for (key in mapObjects) {
                        if (mapObjects.hasOwnProperty(key)) {
                            //Keys coinciden, eliminar elemento
                            if( key.substring(0, keyLayer.length) == keyLayer ) {
                                mapObjects[ key ].setMap(null);

                                //Eliminando del arreglo que pinta en mapa
                                delete mapObjects[key];

                                //Elimina del arreglo que guarda las capas
                                delete projectLayers[keyLayer];
                            } else {
                                console.log(mapObjects);
                            }
                        }
                    }
                    //Elimina elemento HTML
                    $("div#" + keyLayer).remove();
                    $(".currentProject").click();

                } else if ( action == "edit" ) {
                    //Editar layer
                    var tipo = keyLayer.split("___");
                    var editZone = "";

                    $("#estilo_punto_edit").hide();
                    $("#estilo_poligono_edit").hide();

                    //Edicion por tipo de elemento
                    if ( tipo[1] == "punto" ) {
                        $("#estilo_punto_edit").show();
                    } else if ( tipo[1] == "poli" ) {
                        $("#estilo_poligono_edit").show();
                    }                    
                }
            });

        });

        //Guardar cambios en los poligonos modificados
        $("#savePoli").click(function(event){
            var colorFondo;
            var opacidad;
            var colorLinea;
            var grupo = $("#keyEdit").val();

            //Color de fondo
            $.each($(".simpleColorDisplay"), function(a, b){
                if (a == 3) {
                    colorFondo = $(this).html();
                }
            });

            //Color de linea
            if ($('#colorLineEdit').is(':checked')) {
                //Personalizado
                colorLinea = $("#colorPickerLineEdit").val();
            } else {
                //Igual al color de fondo
                colorLinea = colorFondo;
            }

            //Opacidad
            opacidad = $(".sliderEdit a").css("left");
            opacidad = Number( opacidad.substring(0, (opacidad.length - 2)) );
            opacidad /= 100;

            //Objetos del mapa
            for (key in mapObjects) {
                if (mapObjects.hasOwnProperty(key)) {
                    //Keys coinciden, eliminar elemento
                    if( key.substring(0, grupo.length) == grupo ) {
                        //Eliminar objeto del mapa
                        mapObjects[ key ].setMap(null);

                        //Cambiar propiedades
                        mapObjects[ key ].fillColor     = colorFondo;
                        mapObjects[ key ].fillOpacity   = opacidad;
                        mapObjects[ key ].strokeColor   = colorLinea;

                        //Agregar objeto al mapa
                        mapObjects[ key ].setMap(map);
                    }
                }
            }

            /*
            //Objetos del proyecto
            for (key in projectLayers) {
                if (projectLayers.hasOwnProperty(key)) {
                    //Keys coinciden, eliminar elemento
                    if( key.substring(0, grupo.length) == grupo ) {

                        //Cambiar propiedades
                        projectLayers[ key ].fillColor     = colorFondo;
                        projectLayers[ key ].opacity       = opacidad;
                        projectLayers[ key ].strokeColor   = colorLinea;
                    }
                }
            }
            */

        });

        //Guardar cambios en los puntos modificados
        $("#savePunto").click(function(event){
            var grupo = $("#keyEdit").val();
            var iconSelected;
            var icontype = $('input[name=iconSelectEdit]:checked').val();
            markerNumber = 1;

            //Color de fondo
            $.each($(".simpleColorDisplay"), function(a, b){
                if (a == 3) {
                    colorFondo = $(this).html();
                }
            });

            //Objetos del mapa
            for (key in mapObjects) {
                if (mapObjects.hasOwnProperty(key)) {
                    //Keys coinciden, eliminar elemento
                    if( key.substring(0, grupo.length) == grupo ) {
                        //Icono del elemento
                        if (icontype == "default") {
                            iconSelected = "";
                        } else if (icontype == "numeric") {
                            //Numeración para marcadores
                            mapObjects[ key ].metadata.id = markerNumber;
                            markerNumber++;
                            
                            iconSelected = "http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=" 
                                         + mapObjects[ key ].metadata.id 
                                         + "|" 
                                         + $("#colorPickerPuntoEdit").val().substring(1) 
                                         + "|000000";
                        }

                        //Eliminar objeto del mapa
                        mapObjects[ key ].setMap(null);

                        //Cambiar propiedades
                        mapObjects[ key ].icon = iconSelected;

                        //Agregar objeto al mapa
                        mapObjects[ key ].setMap(map);
                    }
                }
            }
            /*
            //Objetos del proyecto
            for (key in projectLayers) {
                if (projectLayers.hasOwnProperty(key)) {
                    //Keys coinciden, eliminar elemento
                    if( key.substring(0, grupo.length) == grupo ) {

                        //Cambiar propiedades
                        projectLayers[ key ].fillColor     = colorFondo;
                        projectLayers[ key ].opacity       = opacidad;
                        projectLayers[ key ].strokeColor   = colorLinea;
                    }
                }
            }
            */
        });

        //Boton de guardar cambios en la edicion de poligonos y puntos
        $(".saveAllEditLayer").click(function (event){
            $("#btnSaveLayout").click();
        });

        $('.slider').slider({ min: 0, max: 1, step: 0.05, value: 1 })
            .bind("slidechange", function() {
                //get the value of the slider with this call
                var o = $(this).slider('value');
                $(".simpleColorDisplay").first().css('opacity', o)
            });

        $('.sliderEdit').slider({ min: 0, max: 1, step: 0.05, value: 1 })
            .bind("slidechange", function() {
                //get the value of the slider with this call
                var o = $(this).slider('value');
                $.each($(".simpleColorDisplay"), function(a, b){
                    if (a == 3) {
                        $(this).css('opacity', o);
                    }
                });
            });
        
    });
    </script>

</head>

<body>

<input type="hidden" value="<?php echo $IDUSUARIO ?>" name="txt_idusuario" id="txt_idusuario"/>

<?php echo pintar_cabecera(); ?>




<div id="map-wrapper">
        <div id="map-canvas"></div>
        <div id="map-over">
          <a href="#map-ctrl" class="open-map-ctrl">Controles</a>
        </div>
        <div id="map-ctrl" class="white-popup mfp-hide">
            <div id="map-ctrl-element">
                <div id="map-tab">
                    <ul class="resp-tabs-list">
                        <li class="searchElementPanel">Buscar elemento</li>
                        <li class="projectDbList">Proyectos</li>
                        <li class="currentProject">Editar proyecto</li>                        
                    </ul>
                    <div class="resp-tabs-container">
                        <div>
                            <div class="map-ctrl-container">
                                <div>
                                    <span float="left">Elemento</span>
                                    <div id="map-ctrl-elementos">
                                        <div class="geo_item_init">
                                            <select name="geo_elemento" id="geo_elemento" title="">
                                                <option value=""> - Seleccione - </option>
                                            </select>
                                        </div>
                                        <div id="geo_response"></div>
                                    </div>
                                </div>
                                
                            </div>
                            <div id="map-ctrl-msg"></div>
                            <div id="map-ctrl-inherit">
                                <div id="inherit1" style="max-height: 200px; overflow: auto"></div>
                                <select name="cascade1" id="cascade1"></select>
                                
                                <fieldset style="padding: 10px; display: none" class="estiloItem" id="estilo_poligono">
                                    <legend>Pol&iacute;gono</legend>
                                    <div style="display: table;">
                                        <div style="display: table-row;">
                                            <div style="display: table-cell;">
                                                <input type="radio" name="colorSelect" value="static" checked="checked"> Color &uacute;nico
                                            </div>
                                            <div style="display: table-cell;">
                                                <input class="simple_color_color_code" id="colorPickerPoligono" value="#0066cc"/>
                                            </div>
                                        </div>
                                        <div style="display: table-row;">
                                            <div style="display: table-cell;">
                                                <input type="radio" name="colorSelect" value="random"> Color aleatorio
                                            </div>
                                            <div style="display: table-cell;">
                                                <div class="slider" data-wjs-element="box" style="width: 100px"></div>
                                            </div>
                                        </div>
                                        <div style="display: table-row;">
                                            <div style="display: table-cell;">
                                                <input type="checkbox" name="colorLine" id="colorLine" value="color_line">
                                                Color de l&iacute;nea
                                            </div>
                                            <div style="display: table-cell;">
                                                <input class="simple_color_color_code" id="colorPickerLine" value="#0066cc"/>
                                            </div>
                                        </div>
                                    </div>
                                </fieldset>
                                
                                <fieldset style="padding: 10px; display: none" class="estiloItem" id="estilo_punto">
                                    <legend>Punto</legend>
                                    <div style="display: table;">
                                        <div style="display: table-row;">
                                            <div style="display: table-cell;">
                                                <input type="radio" name="iconSelect" value="default" checked="checked"> Por defecto
                                            </div>
                                            <div style="display: table-cell;">
                                                
                                            </div>
                                        </div>
                                        <div style="display: table-row;">
                                            <div style="display: table-cell;">
                                                <input type="radio" name="iconSelect" value="numeric"> Numerico
                                            </div>
                                            <div style="display: table-cell;">
                                                <input class="simple_color_color_code" id="colorPickerPunto" value="#0066cc"/>
                                            </div>
                                        </div>
                                        <!--
                                        <div style="display: table-row;">
                                            <div style="display: table-cell;">
                                                <input type="radio" name="iconSelect" value="numeric"> Personalizado
                                            </div>
                                            <div style="display: table-cell;">
                                                
                                            </div>
                                        </div>
                                        -->
                                    </div>
                                </fieldset>                                
                                
                                <input type="button" name="btnDrawItem" id="btnDrawItem" value="Dibujar">

                                <input type="button" name="btnClearItem" id="btnClearItem" value="Borrar todo / Nuevo proyecto">

                                <input type="button" name="btnLayout" id="btnLayout" value="Capas">

                                <hr>

                                Nombre del proyecto <input type="text" name="proyecto" id="proyecto">
                                <input type="button" name="btnSaveLayout" id="btnSaveLayout" value="Guardar cambios">                                
                            </div>
                        </div>
                        <div>
                            <!-- Capas generadas -->
                            <!-- <input type="button" name="btnUpdateLayer" id="btnUpdateLayer" value="Actualizar"> -->
                            <div id="layoutList"></div>
                        </div>
                        <div>
                            <!-- Current project -->
                            <h3 class="projectTitle" style="text-align: center"></h3>
                            <div style="display: table; width: 100%">
                                <div style="display: table-row">
                                    <div style="display: table-cell" id="currentProject"></div>
                                    <div style="display: table-cell; width: 45%" id="editZone">
                                        <input type="hidden" name="keyEdit" id="keyEdit">
                                        <input type="hidden" name="proIdEdit" id="proIdEdit" value="0">
                                        
                                        <fieldset style="padding: 10px; display: none" id="estilo_poligono_edit">
                                            <legend>Pol&iacute;gono</legend>
                                            <div style="display: table;">
                                                <div style="display: table-row;">
                                                    <div style="display: table-cell;">
                                                        <input type="radio" name="colorSelectEdit" value="static" checked="checked"> Color &uacute;nico
                                                    </div>
                                                    <div style="display: table-cell;">
                                                        <input class="simple_color_color_code" id="colorPickerPoligonoEdit" value="#0066cc"/>
                                                    </div>
                                                </div>
                                                <div style="display: table-row;">
                                                    <div style="display: table-cell;">
                                                        <input type="radio" name="colorSelectEdit" value="random"> Color aleatorio
                                                    </div>
                                                    <div style="display: table-cell;">
                                                        <div class="sliderEdit" data-wjs-element="box" style="width: 100px"></div>
                                                    </div>
                                                </div>
                                                <div style="display: table-row;">
                                                    <div style="display: table-cell;">
                                                        <input type="checkbox" name="colorLineEdit" id="colorLineEdit" value="color_line">
                                                        Color de l&iacute;nea
                                                    </div>
                                                    <div style="display: table-cell;">
                                                        <input class="simple_color_color_code" id="colorPickerLineEdit" value="#0066cc"/>
                                                    </div>
                                                </div>
                                                <div style="display: table-row;">
                                                    <div style="display: table-cell;">
                                                        <input type="button" id="savePoli" value="Aplicar cambios" >
                                                    </div>
                                                    <div style="display: table-cell;">
                                                        <input type="button" class="saveAllEditLayer" value="Guardar cambios" >
                                                    </div>
                                                </div>
                                            </div>
                                        </fieldset>
                                        
                                        <fieldset style="padding: 10px; display: none" id="estilo_punto_edit">
                                            <legend>Punto</legend>
                                            <div style="display: table;">
                                                <div style="display: table-row;">
                                                    <div style="display: table-cell;">
                                                        <input type="radio" name="iconSelectEdit" value="default" checked="checked"> Por defecto
                                                    </div>
                                                    <div style="display: table-cell;">
                                                        
                                                    </div>
                                                </div>
                                                <div style="display: table-row;">
                                                    <div style="display: table-cell;">
                                                        <input type="radio" name="iconSelectEdit" value="numeric"> Numerico
                                                    </div>
                                                    <div style="display: table-cell;">
                                                        <input class="simple_color_color_code" id="colorPickerPuntoEdit" value="#0066cc"/>
                                                    </div>
                                                </div>
                                                <div style="display: table-row;">
                                                    <div style="display: table-cell;">
                                                        <input type="button" id="savePunto" value="Aplicar cambios" >
                                                    </div>
                                                    <div style="display: table-cell;">
                                                        <input type="button" class="saveAllEditLayer" value="Guardar cambios" >
                                                    </div>
                                                </div>
                                                <!--
                                                <div style="display: table-row;">
                                                    <div style="display: table-cell;">
                                                        <input type="radio" name="iconSelect" value="numeric"> Personalizado
                                                    </div>
                                                    <div style="display: table-cell;">
                                                        
                                                    </div>
                                                </div>
                                                -->
                                            </div>
                                        </fieldset>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                    </div>
                </div>
                
            </div>
            <div id="map-ctrl-response">

            </div>
            
        </div>
    </div>

    <div class="white-popup mfp-hide" id="polygonOptions">
        <h3>Polygon Options</h3>
        <input class="simple_color_color_code" id="colorPickerPoligonoEdit" value=""/>
        <div class="slider" data-wjs-element="box" style="width: 100px"></div>
        <input type="button" name="savePolygon" id="savePolygon" value="Guardar Cambios">
    </div>


<!--
<iframe width="100%" height="500" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://www.google.com/maps/ms?msa=0&amp;msid=210123400359188756570.0004f68b97fd91c29c3f3&amp;ie=UTF8&amp;ll=-11.97997,-77.088042&amp;spn=0.17509,0.117252&amp;t=m&amp;output=embed"></iframe><br /><small>Ver <a href="https://www.google.com/maps/ms?msa=0&amp;msid=210123400359188756570.0004f68b97fd91c29c3f3&amp;ie=UTF8&amp;ll=-11.97997,-77.088042&amp;spn=0.17509,0.117252&amp;t=m&amp;source=embed" style="color:#0000FF;text-align:left">Nuevas trobas</a> en un mapa más grande</small>
-->

</body>
</html>