<?php
require_once "clases/class.ConexionMapas.php";
/**
require_once "clases/class.FfttTerminales.php";
require_once "clases/class.FfttCapas.php";
require_once "clases/class.FfttCapasArea.php";
require_once "clases/class.FfttEdificioEstado.php";
require_once "clases/class.FfttCms.php";
require_once "clases/class.Poligono.php";
require_once "clases/class.PoligonoTipo.php";
require_once "clases/class.Ubigeo.php";
require_once "clases/class.TipoPedido.php";
require_once "clases/class.Usuario.php";
require_once "clases/class.UsuarioCapa.php";
require_once "clases/class.GestionObraEdi.php";
require_once "clases/class.SubmodulosAccesos.php";
require_once "clases/class.RegionZonalEdi.php";

require_once "clases/class.DunaAdsl.php";
require_once "clases/class.Zonal.php";

require_once "clases/class.FfttTerminales_smc.php";
*/
require_once "clases/class.GeoElemento.php";
require_once "clases/class.GeoDependencia.php";
require_once "clases/class.GeoMdf.php";
require_once "clases/class.GeoProvincia.php";
require_once "clases/class.GeoDistrito.php";
require_once "clases/class.GeoTerminald.php";
require_once "clases/class.GeoTerminalf.php";
require_once "clases/class.GeoTroba.php";
require_once "clases/class.GeoArmariopoligono.php";
require_once "clases/class.GeoArmariopunto.php";
require_once "clases/class.GeoAmplificador.php";
require_once "clases/class.GeoProyecto.php";
require_once "clases/class.GeoTap.php";
require_once "clases/class.GeoNodopoligono.php";
require_once "clases/class.GeoProyectoCapas.php";
require_once "clases/class.GeoAntenacircle.php";
require_once "clases/class.GeoDistritopunto.php";
require_once "clases/class.GeoMdfpunto.php";
require_once "clases/class.GeoClienteanpunto.php";
require_once "clases/class.GeoAddress.php";
require_once "clases/class.GeoAlarmaTap.php";

function __autoload($className)
{
    $className = 'class.'.$className;
    $arrayClassName= explode("_", $className);
    $elemUlt = $arrayClassName[(count($arrayClassName)-1)];
    unset($arrayClassName[(count($arrayClassName)-1)]);

    $base = strtolower(implode("/", $arrayClassName));
	
	$ruta = "clases.".$elemUlt . '.php';
	
	echo $ruta;
	
    if(file_exists($ruta))
        require_once  APP_DIR  . $base ."/class.".$elemUlt . '.php';
}

# In PHP 5.2 or higher we don't need to bring this in
if (!function_exists('json_encode')) {
    require_once 'jsonwrapper_inner.php';
} 


