<?php
session_start();
include_once 'autoload.php';

$cnxObj = new ConexionMapas();
$conexion = $cnxObj->conectarBD();


/**
 * Upload icono personalizado
 */

//Debe existir la siguiente carpeta
$upload_folder = 'images/tmp';

$nombre_archivo = $_FILES['archivo']['name'];
$ext_archivo = end((explode(".", $nombre_archivo)));

$tipo_archivo = $_FILES['archivo']['type'];

$tamano_archivo = $_FILES['archivo']['size'];

$tmp_archivo = $_FILES['archivo']['tmp_name'];

//Nuevo nombre de archivo MD5
$archivo_nuevo = md5(date("YmdHis") . $nombre_archivo) . "." . $ext_archivo;

//$archivador = $upload_folder . '/' . $nombre_archivo;
$archivador = $upload_folder . '/' . $archivo_nuevo;

if (!move_uploaded_file($tmp_archivo, $archivador)) {
    $return = array(
        'upload' => FALSE, 
        'msg' => "Ocurrio un error al subir el archivo. No pudo guardarse.", 
        'error' => $_FILES['archivo']
    );
} else {
    $return = array(
        'upload' => TRUE, 
        'file' => $archivador
    );
}

echo json_encode($return);
