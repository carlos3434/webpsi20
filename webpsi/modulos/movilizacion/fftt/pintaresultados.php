<?php
require_once("../../../clases/class.FacilidadesTecnicas.php");

if ($_REQUEST["zonal"]!='0') {
	$zonal = $_REQUEST['zonal'];
	$mdf = $_REQUEST['mdf'];
	$tipo_red = $_REQUEST['tipo_red'];
	$cable_armario = $_REQUEST['cable_armario'];
	$caja_terminal = $_REQUEST['caja_terminal'];
	$minDistancia = $_REQUEST["minDistancia"];	
	$maxDistancia = $_REQUEST["maxDistancia"];
	$maxElementos = $_REQUEST["maxElementos"];
	$mostrarTaps = $_REQUEST["mostrarTaps"];
	
}
else
{
	$zonal = "LIM";
	$mdf = "MI";
	$tipo_red = "D";
	$cable_armario = "D/01";
	$caja_terminal = "015";
	$mostrarTaps = 0;
}


$objFFTT = new FacilidadesTecnicas();

$arrTerminal = $objFFTT->getTerminalXY($zonal, $mdf, $tipo_red, $cable_armario, $caja_terminal ) ;
foreach ($arrTerminal as $rowTerminal ) {
	$cajaOrigen_terminalX = $rowTerminal['X'];
	$cajaOrigen_terminalY = $rowTerminal["Y"];
	
}

$arrCajasCercanas = $objFFTT->getTerminalCercanosXY($zonal, $mdf, $cajaOrigen_terminalX, $cajaOrigen_terminalY, 
			$minDistancia, $maxDistancia, $maxElementos) ;

?>

<?php

$arrCajaT = $objFFTT->getTerminalDatos($zonal, $mdf, $tipo_red, $cable_armario, $caja_terminal ) ;


foreach ($arrCajaT as $fila) {
	$direccionCajaT = $fila["direccion"];
}

?>

<script type="text/javascript" src="../js/jquery-1.7.1.min.js"></script>

<script type="text/javascript">

	$(document).ready(function(){
	
		var zonal = $("#txtZonal" ).val();
		
		var mdf = $("#txtMdf").val();
		var tipoRed = $("#txtTipoRed").val();
		var cable_armario = $("#txtCableArmario").val();
		var caja_terminal = $("#txtTerminal").val();

		var minDistancia = $("#txtMinDistancia").val();
		var maxDistancia = $("#txtMaxDistancia").val();
		var maxElementos = $("#txtMaxElementos").val();
		var mostrarTaps = $("#txtMostrarTaps").val();

		var pag = "mapa_fftt.php?coordX="+<?=$cajaOrigen_terminalX?>+"&coordY="+<?=$cajaOrigen_terminalY?>;
		pag = pag+"&zonal="+zonal+"&mdf="+mdf+"&tipo_red="+tipoRed+"&cable_armario="+cable_armario;
		pag = pag+"&caja_terminal="+caja_terminal;
		pag = pag+"&minDistancia="+minDistancia;
		pag = pag+"&maxDistancia="+maxDistancia;
		pag = pag+"&maxElementos="+maxElementos;
		pag = pag+"&mostrarTaps="+mostrarTaps;
		//alert(pag);

		window.parent.$("#frame_mapa").show();
		window.parent.$("#frame_mapa").attr('src', pag);
	
	});
	
	
	function mostrarParesClientes(terminal, tipored, cable_armario) {

		var zonal = $("#cmb_zonales", window.parent.document).val();
		var mdf =   $("#cmb_mdfs", window.parent.document).val();
		//var cable_armario = $("#cmb_cable_armario").val();
		//var terminal = $("#cmb_caja_terminal").val();
		//var llave = $("#txtLlave").val();
		//var tipored = $("#cmb_tipored").val();

		//alert("caja = "+terminal+", tipored= "+tipored+" , armario = "+cable_armario);
		/*$( "#divParesClientes" ).load(
			"pintarclientes.php",
			{
				zonal: zonal,
				mdf: mdf,	
				cable_armario: cable_armario,	
				terminal: terminal,	
				tipored: tipored								
			}
		);
		*/
		
		var pag = "pintarclientes.php";
		pag = pag+"?zonal="+zonal;
		pag = pag+"&mdf="+mdf;
		pag = pag+"&cable_armario="+cable_armario;
		pag = pag+"&terminal="+terminal;
		pag = pag+"&tipored="+tipored;
		
		window.parent.$("#iframeParesClientes").show();
		window.parent.$("#iframeParesClientes").attr('src', pag);
	}
	
	
</script>

<link rel="stylesheet" type="text/css" href="estilo.css">


<table class="tablaClonar" >
<tr>	
	<td class="celda_titulo" style="width: 18%;">Direccion de CT:</td>
	<td class="celda_res"><?=$direccionCajaT?></td>
</tr>
</table>

<br/>
<?php 
//echo "Direccion de Caja Terminal: ".$direccionCajaT;
//echo "<br/>X/Y De Caja Terminal: X=".$caja_terminalX." / "."Y=".$caja_terminalY;

?>

<input type="hidden" value="<?=$caja_terminalX?>" id="txtCoordX" name="txtCoordX" />
<input type="hidden" value="<?=$caja_terminalY?>" id="txtCoordY" name="txtCoordY" />

<input type="hidden" value="" id="txtCoordXY_Mapa" name="txtCoordXY_Mapa" class="caja_texto1" />

<input type="hidden" value="<?=$zonal?>" id="txtZonal" name="txtZonal" />
<input type="hidden" value="<?=$mdf?>" id="txtMdf" name="txtMdf" />
<input type="hidden" value="<?=$tipo_red?>" id="txtTipoRed" name="txtTipoRed" />
<input type="hidden" value="<?=$cable_armario?>" id="txtCableArmario" name="txtCableArmario" />
<input type="hidden" value="<?=$caja_terminal?>" id="txtTerminal" name="txtTerminal" />
<input type="hidden" value="<?=$mostrarTaps?>" id="txtMostrarTaps" name="txtMostrarTaps" />

<input type="hidden" value="<?=$minDistancia?>" id="txtMinDistancia" name="txtMinDistancia" />
<input type="hidden" value="<?=$maxDistancia?>" id="txtMaxDistancia" name="txtMaxDistancia" />
<input type="hidden" value="<?=$maxElementos?>" id="txtMaxElementos" name="txtMaxElementos" />

<form name="form_cajas" id="form_cajas" action="">
<table class="tablaClonar" >
<thead>
	<th class="celda_titulo" style="width: 1px">#</th>
	<th class="celda_titulo">FFTT Terminal</th>
	<th class="celda_titulo">Data</th>
	<!--<th class="celda_titulo">Recomendacion Caja</th>-->
	<!--<th class="celda_titulo">Max Velocidad</th>
	-->
	<th class="celda_titulo">Direccion CT</th>
</thead>

<?php

$i=1;
$j=0;


foreach ($arrCajasCercanas as $row ) {
	$nombretxtCaja = "txtXY_Cliente".$j;
	$nombretxtCaja2 = "txtInscripcion_Cliente".$j;
	$nombretxtCaja3 = "txtZonal_Cliente".$j;

	if ($row['qparlib']>2) 
		$alarma = "bgVerde";
	else if ($row['qparlib']<=2 and $row['qparlib']>0 )
		$alarma = "bgAmarillo";
	else 
		$alarma = "bgRojo";

	if (strtoupper($row["tipoDslam"])=="DI")
		$dslam = "dslamDI";
	else
		$dslam = "dslamDO";

	$tipoRed_Row = $row["tipo_red"];


	if (strtoupper($tipoRed_Row)=="D") 
		$cable_armario_Row = $row["cable"];
	else
		$cable_armario_Row = $row["armario"];

	$cajaRow = $row["caja"];

	?> 
	<tr>
		<td class="celda_res <?=$alarma?>" style="width: 5%; "><a href="javascript: void(0)" onclick="mostrarParesClientes('<?=$cajaRow?>','<?=$tipoRed_Row?>','<?=$cable_armario_Row?>')"><? echo $i ?></a>
			<input type="hidden" id="txtLlave" name="txtLlave" value="<?php echo $row['llave']?>" /> 		
		</td>
		<td class="celda_res <?=$dslam?>" style="width: 20%; "><?php echo $row['zonal']." ".$row["mdf"]." ".$row["cable"]." ".$row["armario"]." ".$row["caja"]." - ".$row["tipoDslam"] ; ?>
		</td>		
		<td class="celda_res" style="width: 12%; text-align: left;"><?php echo round($row['distancia'], 0)." mts.<br/>".$row['qparlib']." P.libres<br/>".$row['sugerencia'];?></td>
		<!--<td class="celda_res" style="width: 10%; "><?php echo $row['qparlib']; ?></td>-->
		<!--<td class="celda_res" style="width: 10%; "><?php echo $row['sugerencia']; ?></td>-->
		
		<!--<td class="celda_res" style="width: 10%; "><?php echo $row["SugerenciaCaja"]?></td>-->
		<td class="celda_res" style="width: 63%; "><?php  echo $row['direccion']."<br/>".$row["x"]." ".$row["y"]; ?></td>
	</tr>	
	<?php
	$i++;
	$j++;
}
?>
</table>
</form>

</iframe>


<br/>
<br/>


