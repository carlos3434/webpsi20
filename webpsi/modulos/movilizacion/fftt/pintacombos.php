<?php
require_once("../../../clases/class.FacilidadesTecnicas.php");

$tipo_combo = $_REQUEST["tipo_combo"];

function comboMdfs($zonal) {
	$objZonales = new FacilidadesTecnicas();
	$arrMdfs = $objZonales->getMdfsxZonal($zonal);
	
	$x = "<select name='cmb_mdfs' id='cmb_mdfs' onchange='changeMdf();'>";
	$x .= "<option value='0' >Seleccione...</option>";
	foreach ($arrMdfs as $fila) {
		$mdf = $fila['mdf'];
		$x .= "<option value=".$mdf.">".$mdf."</option>";
	}	
	$x .= "</select>";

	return $x;
}

function comboTipoRed() {
	$objZonales = new FacilidadesTecnicas();
	$arrMdfs = $objZonales->getTipoRed();

	$x = "<select name='cmb_tipored' id='cmb_tipored' onchange='changeTipoRed();' >";
	$x .= "<option value='0' >Seleccione...</option>";
	foreach ($arrMdfs as $fila) {
		$tipo_red = $fila['tipo_red'];
		$tipo_red_desc = $fila['descripcion'];
		$x .= "<option value=".$tipo_red.">".$tipo_red_desc."</option>";
	}	
	$x .= "</select>";

	return $x;
}


function comboCableArmario($zonal, $mdf, $tipo_red) {
	$objZonales = new FacilidadesTecnicas();
	$arrArmCable = $objZonales->getCableArmario($zonal, $mdf, $tipo_red) ;

	$x = "<select name='cmb_cable_armario' id='cmb_cable_armario' onchange='changeCableArmario();' >";
	$x .= "<option value='0' >Seleccione...</option>";
	foreach ($arrArmCable as $fila) {
		$cableArmario = $fila['cable_armario'];
		$x .= "<option value=".$cableArmario.">".$cableArmario."</option>";
	}	
	$x .= "</select>";

	return $x;
}


function comboTerminales($zonal, $mdf, $tipo_red, $cableArmario) {
	$objZonales = new FacilidadesTecnicas();
	$arrCajas = $objZonales->getTerminales($zonal, $mdf, $tipo_red, $cableArmario );
	//var_dump($arrCajas);

	$x = "<select name='cmb_caja_terminal' id='cmb_caja_terminal' >";
	$x .= "<option value='0' >Seleccione...</option>";
	foreach ($arrCajas as $fila) {
		$cajaTerminal = $fila['caja'];
		$x .= "<option value=".$cajaTerminal.">".$cajaTerminal."</option>";
	}	
	$x .= "</select>";

	return $x;
}


if ($tipo_combo=="changeZonal")
{
	$zonal = $_REQUEST['zonal'];
	echo comboMdfs($zonal);
}
else if ($tipo_combo=="changeMdf")
{
	$zonal = $_REQUEST['zonal'];
	$mdf = $_REQUEST['mdf'];
	echo comboTipoRed();
}
else if ($tipo_combo=="changeTipoRed")
{
	$zonal = $_REQUEST['zonal'];
	$mdf = $_REQUEST['mdf'];
	$tipo_red = $_REQUEST['tipo_red'];

	echo comboCableArmario($zonal, $mdf, $tipo_red);
}
else if ($tipo_combo=="changeCableArmario")
{
	$zonal = $_REQUEST['zonal'];
	$mdf = $_REQUEST['mdf'];
	$tipo_red = $_REQUEST['tipo_red'];
	$cable_armario = $_REQUEST['cable_armario'];

	echo comboTerminales($zonal, $mdf, $tipo_red, $cable_armario);
}




?>