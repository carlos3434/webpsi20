<?php

include ("../../../includes.php") ; 

require_once("../../../clases/class.FacilidadesTecnicas.php");
require_once("../../../clases/class.LBS.php");

$direccionEnMapa = "";

$cajaOrigen_terminalX = $_REQUEST['coordX'];
$cajaOrigen_terminalY = $_REQUEST['coordY'];

$zonal = $_REQUEST['zonal'];
$mdf = $_REQUEST['mdf'];
$nodo = $mdf;

$tipo_red = $_REQUEST['tipo_red'];
$cable_armario = $_REQUEST['cable_armario'];
$caja_terminal = $_REQUEST['caja_terminal'];

$minDistancia = $_REQUEST['minDistancia'];
$maxDistancia = $_REQUEST['maxDistancia'];
$maxElementos = $_REQUEST['maxElementos'];

if (isset($_REQUEST["mostrarTaps"]))
	$mostrarTaps = $_REQUEST['mostrarTaps'];
else
	$mostrarTaps = 0;

//var_dump($_REQUEST);

$objFFTT = new FacilidadesTecnicas();

$arrCajasCercanas = $objFFTT->getTerminalCercanosXY($zonal, $mdf, $cajaOrigen_terminalX, $cajaOrigen_terminalY, 
		$minDistancia, $maxDistancia, $maxElementos );


if ($mostrarTaps==1)
	$arrTapsCercanos = $objFFTT->getTapsCercanosXY($cajaOrigen_terminalX, $cajaOrigen_terminalY, 
			$minDistancia, $maxDistancia, $maxElementos );
else
	$arrTapsCercanos = array();

//echo "TAPS CERCANOS = $mostrarTaps / ".var_dump($arrTapsCercanos);

//var_dump($arrCajasCercanas);
//var_dump($arrTapsCercanos);

/*EAHF - llamamos al metodo que busca los tecnicos mas cercanos a la caja terminal en referencia*/
$objTecnicos = new LBS();

$arrTecnicosCercanos = $objTecnicos->getTecnicosCercanos($cajaOrigen_terminalX, $cajaOrigen_terminalY, 
				$minDistancia, $maxDistancia) ;

$sumaX = 0;
$sumaY = 0;
$contarXY = 0;

foreach ($arrCajasCercanas as $row ) {
	if ($row["x"]!='') {
		$sumaX += $row["x"];
		$sumaY += $row["y"];
		$contarXY++;
	}
}

$promedioX = $sumaX / $contarXY;
$promedioY = $sumaY / $contarXY;
//echo $promedioX. " / ".$promedioY." / ".$contarXY; 


?>

<script type='text/javascript'>
<?php
    $js_array = json_encode($arrCajasCercanas);
    echo "var cajasCercanas = ". $js_array . ";\n";

    $js_arraytec = json_encode($arrTecnicosCercanos);
    echo "var tecnicosCercanos = ". $js_arraytec . ";\n";

    $js_arraytaps = json_encode($arrTapsCercanos);
    echo "var tapsCercanos = ". $js_arraytaps . ";\n";  

?>

//alert(cajasCercanas[0]["llavexy"]);

</script>



<!DOCTYPE HTML PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>

<style type="text/css">
#divModalMapa {
	font-family: "Tahoma";
	font-size: 12px;
	border-top-width: 0px;
	border-right-width: 0px;
	border-bottom-width: 0px;
	border-left-width: 0px;
	border-top-style: none;
	border-right-style: none;
	border-bottom-style: none;
	border-left-style: none;
	letter-spacing: normal;
	text-align: left;
	vertical-align: middle;
	word-spacing: normal;
}

</style>

<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false"></script>

<script type="text/javascript">

	function makeMapListener(window, map, markers) {
	  return function() { window.open(map, markers); };
	}
	
	function initialize() {	
		var coordXInicial = $("#txtCoordX").val();
		var coordYInicial = $("#txtCoordY").val();
		//alert(cajasCercanas[0]["y"]);


		//alert(coordXInicial);
	    var latlng = new google.maps.LatLng(coordYInicial, coordXInicial );

	    var myOptions = {
	        zoom: 18,
	        center: latlng
	    }

	    var map = new google.maps.Map(document.getElementById("mapa"), myOptions);
	    //var image = 'img/terminal-2.gif';
	    	    //alert("tamaño array = "+actuaciones.length);
	    var image="";
	    var j=1;
	    var marker = new Array();
	    var marker_posX = new Array();
	    var infowindow = new Array();
	    var actuacion ;
	    var contentString;
	    var imageX;

	    //alert(cajasCercanas.length);
		for (var i = 0; i < cajasCercanas.length; i++) {
			
			if (i!=0) {
				if (cajasCercanas[i]['qparlib']>2) 
					imageX = "../img/icos/verde/"+j+".png";
				else if (cajasCercanas[i]['qparlib']<=2 && cajasCercanas[i]['qparlib']>0 )
					imageX = "../img/icos/amarillo/"+j+".png";
				else 
					imageX = "../img/icos/red/"+j+"-lv.png";
			}
			else
			{
				imageX = "../img/icos/azul.png";
			}
			actuacion = cajasCercanas[i]["mtgespktrm"];

	        marker[i] = new google.maps.Marker({
	             position: new google.maps.LatLng(cajasCercanas[i]["y"], cajasCercanas[i]["x"]),
	             map: map,
	             icon: imageX,
	             size: new google.maps.Size(10, 22),
	             title: cajasCercanas[i]["mtgespktrm"],
	             zIndex: i
	         });

			contentString = "<div style='width: 250px'>";
			contentString = contentString+"<span style='font-size: 9px; font-weight: bold;'>"+cajasCercanas[i]["zonal"]+" "+cajasCercanas[i]["mdf"]+" "+cajasCercanas[i]["cable"];
			contentString = contentString+cajasCercanas[i]["armario"]+" "+cajasCercanas[i]["caja"]+"</span> /  ";
			contentString = contentString+"<span style='font-size: 9px;'>"+cajasCercanas[i]["direccion"]+"</span><br/>";
			contentString = contentString+"<span style='font-size: 9px; font-weight:bold;'>Pares Libres: </span>";
			contentString = contentString+"<span style='font-size: 9px;'>"+cajasCercanas[i]["qparlib"]+" </span>"
			contentString = contentString+"<span style='font-size: 9px; font-weight:bold;'>Distancia: </span>";
			contentString = contentString+"<span style='font-size: 9px;'>"+parseFloat(cajasCercanas[i]["distancia"]).toFixed(2)+" </span>"
			contentString = contentString+"<span style='font-size: 9px; font-weight:bold;'>Sugerencia: </span>";
			contentString = contentString+"<span style='font-size: 9px;'>"+cajasCercanas[i]["sugerencia"]+" </span>"
			contentString = contentString+"</div>";
	        infowindow[i] = new google.maps.InfoWindow({
			      content: contentString
			});
			
			google.maps.event.addListener(marker[i], 'click', makeMapListener(infowindow[i], map, marker[i]));			


	        j++;
	     }


	    // TAPS CERCANOS
	    j = 1;
	   	var markerTaps = new Array();
	    var infowindowTaps = new Array();
	    var imageTap = "";

		for (var i = 0; i < tapsCercanos.length; i++) {
				
			imageTap = "../img/icos/taps-morados_20/number_"+j+".png";
			actuacion = tapsCercanos[i]["mtgespktrm"];

	        markerTaps[i] = new google.maps.Marker({
	             position: new google.maps.LatLng(tapsCercanos[i]["y"], tapsCercanos[i]["x"]),
	             map: map,
	             icon: imageTap,
	             size: new google.maps.Size(10, 22),
	             title: tapsCercanos[i]["mtgespktrm"],
	             zIndex: i
	         });

			contentString = "<div style='width: 250px'>";
			contentString = contentString+"<span style='font-size: 9px; font-weight: bold;'>"+tapsCercanos[i]["nodo"]+" "+tapsCercanos[i]["troba"]+" ";
			contentString = contentString+tapsCercanos[i]["amplificador"]+" "+tapsCercanos[i]["tap"]+"</span> /  ";
			contentString = contentString+"<span style='font-size: 9px;'>"+tapsCercanos[i]["direccion"]+"</span><br/>";
			contentString = contentString+"<span style='font-size: 9px; font-weight:bold;'>Distancia: </span>";
			contentString = contentString+"<span style='font-size: 9px;'>"+parseFloat(tapsCercanos[i]["distancia"]).toFixed(2)+" </span>"
			contentString = contentString+"</div>";
	        infowindowTaps[i] = new google.maps.InfoWindow({
			      content: contentString
			});
			
			google.maps.event.addListener(markerTaps[i], 'click', makeMapListener(infowindowTaps[i], map, markerTaps[i]));			


	        j++;
	     }		     

	    var image="";
	    var j=1;
	    var marker = new Array();
	    var marker_posX = new Array();
	    var infowindow = new Array();
	    var actuacion ;
	    var contentString;
	    var imageX;

		for (var i = 0; i < tecnicosCercanos.length; i++) {
			
			/*
			if (i!=0) {
				if (cajasCercanas[i]['qparlib']>2) 
					imageX = "../img/icos/verde/"+j+".png";
				else if (cajasCercanas[i]['qparlib']<=2 && cajasCercanas[i]['qparlib']>0 )
					imageX = "../img/icos/amarillo/"+j+".png";
				else 
					imageX = "../img/icos/red/"+j+"-lv.png";
			}
			else
			{
				imageX = "../img/icos/azul.png";
			} */

			imageX = "../img/icos/tecnico.gif";

			actuacion = tecnicosCercanos[i]["celular"];

	        marker[i] = new google.maps.Marker({
	             position: new google.maps.LatLng(tecnicosCercanos[i]["lat_y"], tecnicosCercanos[i]["lon_x"]),
	             map: map,
	             icon: imageX,
	             size: new google.maps.Size(10, 22),
	             title: tecnicosCercanos[i]["nombre"],
	             zIndex: i
	         });

			contentString = "<div style='width: 250px'>";
			contentString = contentString+"<span style='font-size: 9px; font-weight: bold;'>Tecnico : "+tecnicosCercanos[i]["nombre"]+" "+tecnicosCercanos[i]["celular"]+"</span> <br>  ";
			contentString = contentString+"<span style='font-size: 9px;'>"+tecnicosCercanos[i]["lugar"]+"</span><br/>";
			contentString = contentString+"<span style='font-size: 9px; font-weight:bold;'>Fecha ult. posición: </span>";
			contentString = contentString+"<span style='font-size: 9px;'>"+tecnicosCercanos[i]["fecha"]+" </span>"
			contentString = contentString+"</div>";
	        infowindow[i] = new google.maps.InfoWindow({
			      content: contentString
			});
			
			google.maps.event.addListener(marker[i], 'click', makeMapListener(infowindow[i], map, marker[i]));			


	        j++;
	     }
	}


google.maps.event.addDomListener(window, 'load', initialize);

</script>
<body >
	<input type="hidden" value="<?=$promedioX?>" id="txtCoordX" name="txtCoordX" />
	<input type="hidden" value="<?=$promedioY?>" id="txtCoordY" name="txtCoordY" />
	<input type="hidden" value="<?=$direccionEnMapa?>" id="txtDireccionEnMapa" name="txtDireccionEnMapa" />
	<!--<input type="button" value="buscar" onclick="buscarXY();" />-->

<div id="mapa" style="width: 100%; height: 500px; line-height:normal;"></div>


</body>
