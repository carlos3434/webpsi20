<?php
/**
 * Asignacion de XY a clientes de una misma Caja Terminal
 *
 * @package    geoAsignaXY_clientes.php
 * @author     Sergio Miranda C.
 * @copyright  2013
 */


require_once("../../../cabecera.php");
require_once("../../../clases/class.FacilidadesTecnicas.php");

$idusuario_modelo = $_REQUEST["idusuario"];

function comboZonales() {
	$objZonales = new FacilidadesTecnicas();
	$arrZonales = $objZonales->getZonales();

	$x = "<select name='cmb_zonales' id='cmb_zonales'  >";
	$x .= "<option value='0' >Seleccione...</option>";
	foreach ($arrZonales as $fila) {
		$idZonal = $fila['zonal'];
		$zonal = $fila['zonal'];
		$x .= "<option value=".$idZonal.">".$zonal."</option>";
	}	
	$x .= "</select>";

	return $x;
}

function comboMostrarTaps() {
	$x = "<select name='cmb_mostrar_taps' id='cmb_mostrar_taps'  >";
	$x .= "<option value='0' >NO</option>";
	$x .= "<option value='1' >SI</option>";
	$x .= "</select>";

	return $x;
}

?>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
    <head>
        <title>PSI - Web SMS - Cambio de Clave</title>
        <meta http-equiv="content-type" content="text/html; charset=iso-8859-1">
            <meta name="author" content="Sergio MC" />
            <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">

            <?php include ("../../../includes.php") ?>                

<script type="text/javascript">
 $(document).ready(function () {
	$("#txtMaxElementos").forceNumeric();
	$("#txtMaxDistancia").forceNumeric();

 });


 // forceNumeric() plug-in implementation
 jQuery.fn.forceNumeric = function () {

     return this.each(function () {
         $(this).keydown(function (e) {
             var key = e.which || e.keyCode;

             if (!e.shiftKey && !e.altKey && !e.ctrlKey &&
             // numbers   
                 key >= 48 && key <= 57 ||
             // Numeric keypad
                 key >= 96 && key <= 105 ||
             // comma, period and minus, . on keypad
                key == 190 || key == 188 || key == 109 || key == 110 ||
             // Backspace and Tab and Enter
                key == 8 || key == 9 || key == 13 ||
             // Home and End
                key == 35 || key == 36 ||
             // left and right arrows
                key == 37 || key == 39 ||
             // Del and Ins
                key == 46 || key == 45)
                 return true;

             return false;
         });
     });
 }
</script>



<script type="text/javascript">




function validar_requerido(field,alerttxt)
{
	var value = $(field).val();
	if (value==null||value==""||value==0)
		{alert(alerttxt);return false}
	else {return true}
}

function changeMdf() {

	$( "#div_tipo_red" ).load(
		"pintacombos.php",
		{
			tipo_combo: 'changeMdf',
			zonal: $('#cmb_zonales').val(),
			mdf: $('#cmb_mdfs').val()
		}
	);

}

function changeTipoRed() {

	$( "#div_cable_armario" ).load(
		"pintacombos.php",
		{
			tipo_combo: 'changeTipoRed',
			zonal: $('#cmb_zonales').val(),
			mdf: $('#cmb_mdfs').val(),
			tipo_red: $('#cmb_tipored').val()
		}
	);

}


function changeCableArmario() {

	$( "#div_caja_terminal" ).load(
		"pintacombos.php",
		{
			tipo_combo: 'changeCableArmario',
			zonal: $('#cmb_zonales').val(),
			mdf: $('#cmb_mdfs').val(),
			tipo_red: $('#cmb_tipored').val(),
			cable_armario: $('#cmb_cable_armario').val()
		}
	);

}

$(document).ready(function(){

	/*$('#cmb_zonales').val("LIM");
	$('#cmb_mdfs').val("MI");
	$('#cmb_tipored').val("D");
	$('#cmb_cable_armario').val();
	$('#cmb_caja_terminal').val();
*/
	$('#txtMinDistancia').val(0);
	$('#txtMaxElementos').val(20);

	$('#btnsalir').click(function(){
        $("#childModal").dialog('close');
    });


	$('#cmb_zonales').change(function(){
		$( "#div_combo_mdf" ).load(
				"pintacombos.php",
				{
					tipo_combo: 'changeZonal',
					zonal: $(this).val()
				}
			);
    });

	
	$('#btnGuardarCambios').click(function(){

    	if (validar_requerido($("#cmb_zonales"),"Eliga una Zonal.")==false)
        	{$("#cmb_zonales").focus();return false}
    	if (validar_requerido($("#cmb_mdfs"),"Eliga un MDF.")==false)
        	{$("#cmb_mdfs").focus();return false}
    	if (validar_requerido($("#cmb_tipored"),"Eliga el Tipo de Red.")==false)
        	{$("#cmb_tipored").focus();return false}        
    	if (validar_requerido($("#cmb_cable_armario"),"Eliga un Cable/Armario.")==false)
        	{$("#cmb_cable_armario").focus();return false}
    	if (validar_requerido($("#cmb_caja_terminal"),"Eliga un Cable/Armario.")==false)
        	{$("#cmb_caja_terminal").focus();return false}

		if ( $('#txtMaxElementos').val()>20 ) {
			alert("No se puede superar la cantidad maxima de elementos, que es 20.")
			return false;
		}

		if ( $('#txtMinDistancia').val()< 0 || $('#txtMinDistancia').val()==null || $('#txtMinDistancia').val()=='' ) {
			alert("La distancia minima tiene un valor incorrecto.")
			return false;
		}

		if ( $('#txtMaxDistancia').val()< 1 || $('#txtMaxDistancia').val()==null || $('#txtMaxDistancia').val()=='' ) {
			alert("La distancia maxima no puede ser 0.")
			return false;
		}

		//alert($('#txtMaxDistancia').val());
		if (  parseInt($('#txtMaxDistancia').val()) < parseInt($('#txtMinDistancia').val()) ) {
			alert("La distancia minima no puede ser mayor que la distancia maxima.");
			//alert($('#txtMaxDistancia').val());
			return false;
		}

		var pag = "pintaresultados.php";
		pag = pag+"?zonal="+$('#cmb_zonales').val();
		pag = pag+"&mdf="+$('#cmb_mdfs').val();
		pag = pag+"&cable_armario="+$('#cmb_cable_armario').val();
		pag = pag+"&caja_terminal="+$('#cmb_caja_terminal').val();
		pag = pag+"&tipo_red="+$('#cmb_tipored').val();
		
		pag = pag+"&minDistancia="+$('#txtMinDistancia').val();
		pag = pag+"&maxDistancia="+$('#txtMaxDistancia').val();
		pag = pag+"&maxElementos="+$('#txtMaxElementos').val();
		pag = pag+"&mostrarTaps="+$('#cmb_mostrar_taps').val();
		
	
		$("#iframeRes").show();
		$("#iframeRes").attr('src', pag);


		/*
		$( "#div_resultados" ).load(
			"pintaresultados.php",
			{
				zonal: $('#cmb_zonales').val(),
				mdf: $('#cmb_mdfs').val(),
				tipo_red: $('#cmb_tipored').val(),
				cable_armario: $('#cmb_cable_armario').val(),
				caja_terminal: $('#cmb_caja_terminal').val(),
				minDistancia: $('#txtMinDistancia').val(),
				maxDistancia: $('#txtMaxDistancia').val(),
				maxElementos: $('#txtMaxElementos').val(),
				mostrarTaps: $('#cmb_mostrar_taps').val()
			},
			function() {
				var cx = $("#txtCoordX").val();
				var cy = $("#txtCoordY").val();
				//alert("X = "+cx+"  Y = "+cy);

				var zonal =  $('#cmb_zonales').val();
				var mdf = $('#cmb_mdfs').val();
				var tipo_red =  $('#cmb_tipored').val();
				var cable_armario = $('#cmb_cable_armario').val();
				var caja_terminal =  $('#cmb_caja_terminal').val();			

				
				// var zonal = "LIM";
				// var mdf = "MI";
				// var tipo_red = "D";
				// var cable_armario = "D/01";
				// var caja_terminal = "011";
				// cx = '-77.06876';
				// cy = '-12.04404';				
			

				//alert(cx);
				//$("#frame_mapa").attr('src', "mapa_fftt.php?buscarCoordenadas=1&coordx="+cx+"&coordy="+cy+"&zonal="+zonal+"&mdf="+mdf+"&tipo_red="+tipo_red+"&cable_armario="+cable_armario+"&caja_terminal="+caja_terminal);

			}			
		);
		*/
	});

});
        

         
</script>

<!--<link rel="stylesheet" type="text/css" href="../../estilos.css">-->
<link rel="stylesheet" type="text/css" href="estilo.css">
<link rel="stylesheet" type="text/css" href="buttons.css">
</head>

<body>
<?php echo pintar_cabecera(); ?>
<p>
	<br/>
</p>
<input type="hidden" value="<?php echo $IDUSUARIO?>" name="txt_idusuario" id="txt_idusuario"/>
	
<div id="divMain" class="div_main" style="width: 90%;  ">

	<div id="div_Clonar" class="divClonar" 
		style="float:left; margin-right: 0px; margin-top:0; width: 45%; padding-right: 20px; border: 0px red solid; ">

	<table class="tablaClonar" style="width: 100%;"  >
	<tr>
		<td class="celda_titulo">Zonal:</td>
		<td class="celda_res"  colspan="1">
			<?php echo comboZonales() ?>
		</td>
		<td class="celda_titulo">Mdf:</td>
		<td class="celda_res"  colspan="1"><div id="div_combo_mdf">
			<select name='cmb_mdfs' id='cmb_mdfs' ><option value=0>Seleccione ...</option></select></div>
		</td>
	</tr>
	<tr>
		<td class="celda_titulo">Tipo de Red:</td>
		<td class="celda_res"  colspan="1"><div id="div_tipo_red">
			<select name='cmb_tipored' id='cmb_tipored' ><option value=0>Seleccione ...</option></select></div>
		</td>
		<td class="celda_titulo">Cable / Armario:</td>
		<td class="celda_res"  colspan="1"><div id="div_cable_armario">
			<select name='cmb_cable_armario' id='cmb_cable_armario' ><option value=0>Seleccione ...</option></select></div>
		</td>
	</tr>	
	<tr>
		<td class="celda_titulo">Caja Terminal:</td>
		<td class="celda_res"  colspan="1"><div id="div_caja_terminal">
			<select name='cmb_caja_terminal' id='cmb_caja_terminal' ><option value=0>Seleccione ...</option></select></div>
		</td>
		<td class="celda_titulo">Min/Max Distancia (metros)</td>
		<td class="celda_res"  colspan="1">
			<input type="text" id="txtMinDistancia" name="txtMinDistancia" class="cajaTexto" style="width: 30px" />
			<input type="text" id="txtMaxDistancia" name="txtMaxDistancia" class="cajaTexto" style="width: 30px" />
		</td>		
	</tr>
	<tr>

		<td class="celda_titulo">Max Terminales</td>
		<td class="celda_res"  colspan="1">
			<input type="text" id="txtMaxElementos" name="txtMaxElementos" class="cajaTexto" style="width: 30px" />
		</td>		

		<td class="celda_res"  align="center" colspan="2">
			<button id="btnGuardarCambios" class="action blue" title="Buscar Terminales">
				<span class="label">Buscar Terminales</span>
			</button>
		</td>
	</tr>
	<tr>
		<td class="celda_titulo">Mostrar TAPS CATV:</td>
		<td class="celda_res"  colspan="3">
		<?php echo comboMostrarTaps() ?>
		</td>

	</tr>
	</table>


	<div id="div_resultados" class="divResultados" 
		style="position:relative; float:left;  width: 100%; " >
		<iframe src=""  id="iframeRes" name="iframeRes" 
			width="100%" height="200px" style="display:none"  ></iframe>
	</div>
	
	<div id="divParesClientes">
	<iframe src=""  id="iframeParesClientes" name="iframeParesClientes" 
		width="100%" height="200px" style="display:none"  ></iframe>
	</div>

	
	<div id="divmapa" class="divResultados">
	<iframe src="" id="frame_mapa" width="100%" height="510px" style="display:none" ></iframe> 
	</div>


	</div>


	<br/>
	<br/>
	<br/>
	<br/>

</div>

</body>
</html>
