// Funciones de los botones

function revisar(){
	$("#resultado").html("<br /><br /><span>Realizando busqueda <img src=\"img/loading.gif\" /></span> ");
	
	var mdf = $("#cmbMdf").val();
	var nodo = $("#cmbNodo").val();

	$("#resultado").load(
		"pintarIndicadores1.php",
		{
			mdf:mdf,
			nodo:nodo
		}
	);

}

function revisar2(){
	//var mdf = $("#cmbMdf").val();
	//var nodo = $("#cmbNodo").val();

	var mdf = $("#cmbMdf").multiselect("getChecked").map(function(){
	   return this.value;    
	}).get();

	var nodo = $("#cmbNodo").multiselect("getChecked").map(function(){
	   return this.value;    
	}).get();

	if (mdf.length==0) {
		alert("Seleccione al menos un MDF");
		return false;
	}

	if (nodo.length==0) {
		alert("Seleccione al menos un NODO");
		return false;
	}

	if (mdf.length > 0 && nodo.length > 0) {
		$("#resultado").html("<br /><br /><span>Realizando busqueda <img src=\"img/loading.gif\" /></span> ");

		$("#resultado").load(
			"pintarIndicadores11.php",
			{
				mdf:mdf,
				nodo:nodo
			}
		);

	}

}

//funcion para el boton actualizar
function actualizar(){
	$("#resultado").html("<br /><br /><span>Realizando prueba <img src=\"img/loading.gif\" /></span> ");
	
	var mdf = $("#cmbMdf").val();
	var nodo = $("#cmbNodo").val();

	$("#resultado").load(
		"pintarIndicadores3.php",
		{
			mdf:mdf,
			nodo:nodo
		}
	);

}

function reporteador(){
	$('#cronometro_prueba').hide();
	window.open("reporteExcel.php");

}

function verDetalle(tipoDetalle, tipoActuacion, negocio) {
	//verDetalle('porvencer','provision','catv')
	
	//var mdf = $("#cmbMdf").val();
	//var nodo = $("#cmbNodo").val();
	var mdf = $("#cmbMdf").multiselect("getChecked").map(function(){
	   return this.value;    
	}).get();

	var nodo = $("#cmbNodo").multiselect("getChecked").map(function(){
	   return this.value;    
	}).get();


	$( "#divDetalle1" ).load(
		"pintarIndicadores2.php",
		{
			mdf:mdf,
			nodo:nodo,
			tipoDetalle: tipoDetalle,
			tipoActuacion: tipoActuacion,
			negocio: negocio
		}
	);

}

function verDetalle2(tipoDetalle, tipoActuacion, negocio, ConSinTecnico) {
	//verDetalle('porvencer','provision','catv')
	
	//var mdf = $("#cmbMdf").val();
	//var nodo = $("#cmbNodo").val();

	var mdf = $("#cmbMdf").multiselect("getChecked").map(function(){
	   return this.value;    
	}).get();

	var nodo = $("#cmbNodo").multiselect("getChecked").map(function(){
	   return this.value;    
	}).get();

	$( "#divDetalle1" ).load(
		"pintarIndicadores2.php",
		{
			mdf:mdf,
			nodo:nodo,
			tipoDetalle: tipoDetalle,
			tipoActuacion: tipoActuacion,
			negocio: negocio,
			ConSinTecnico: ConSinTecnico
		}
	);

}

function verDetalleActuacion(actuacion,tipoDetalle, tipoActuacion, negocio) {

	$( "#divDetalleActuacion" ).load(
		"detalleActuacion.php",
		{
			actuacion:actuacion,
			tipoDetalle: tipoDetalle,
			tipoActuacion: tipoActuacion,
			negocio: negocio
		}
	);

}