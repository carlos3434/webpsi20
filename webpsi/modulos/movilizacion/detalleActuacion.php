<?php
header('Content-Type: text/html; charset=UTF-8');
header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache");


//include ("../../clases/class.Indicadores.php");

include("funciones.php");

$hoy = date("Y-m-d H:i:s");
$hoyFormato = date("d-m-Y H:i:s");

$Indicadores = new Indicadores();

$actuacion = $_POST["actuacion"];
$tipoDetalle = $_POST["tipoDetalle"];
$tipoActuacion = $_POST["tipoActuacion"];
$negocio = $_POST["negocio"];

$arrResultado = detalleActuacion($actuacion, $tipoDetalle, $tipoActuacion, $negocio);

//var_dump($arrResultado);


?>
<table class="table">
<thead>
<th colspan="6" style="background: #FFDE40; color: #17688B;">
	<?php 
	switch ($tipoDetalle) {
		case 'porvencer':
			$tipoDetalle = "Por Vencer";
			$tipoDetalleOriginal = "porvencer";
			break;	
		default:
			$tipoDetalleOriginal = $tipoDetalle;
			# code...
			break;
	}
	echo strtoupper($tipoActuacion)." / ".strtoupper($negocio)." - ".strtoupper($tipoDetalle) ;
	?>
</th>
</thead>
<tr>
	

</tr>	

<?php
$i=1;
foreach ($arrResultado as $fila) {
	?>
	<tr>
		<td class="thDetalleActuacion">Actuacion</td>
		<td class="celdaRes"><?=$fila["xactuacion"];?></td>
		<td width="20%" class="thDetalleActuacion">FFTT</td>
		<td class="celdaRes" colspan="3"><?=$fila["xmdf"]." ".$fila["xarmario"]." ".$fila["xcable"]." ".$fila["xterminal"];?></td>
		
	</tr>	
	<tr>
		<td class="thDetalleActuacion">Telefono/Cod Cliente</td>
		<td class="celdaRes"><?=$fila["xtelefono"];?></td>
		<td width="20%" class="thDetalleActuacion">Cliente</td>
		<td class="celdaRes" colspan="3"><?=$fila["xcliente"];?></td>
		
	</tr>
	<tr>
		<td width="15%" class="thDetalleActuacion">Direccion / Distrito</td>
		<td class="celdaRes" colspan="5"><?=$fila["xdireccion"]." ".$fila["xdistrito"];?></td>

	</tr>
	<tr>
		<td class="thDetalleActuacion">Fecha Registro</td>
		<td class="celdaRes"><?=$fila["xfecreg"];?></td>
		<td width="20%" class="thDetalleActuacion">Area</td>
		<td class="celdaRes"><?=$fila["xarea"];?></td>
		<td class="thDetalleActuacion">Segmento</td>
		<td class="celdaRes"><?=$fila["xsegmento1"]." ".$fila["xsegmento2"];?></td>
	</tr>
	<tr>
		<td class="thDetalleActuacion">Fecha Instalacion</td>
		<td class="celdaRes" colspan="1" style="text-align: left">&nbsp;<?php if (isset($fila["fechaInstalacion"])) {echo $fila["fechaInstalacion"];}?></td>
		<td class="thDetalleActuacion">Reiteradas</td>
		<td class="celdaRes" colspan="1" style="text-align: left">&nbsp;<?php if (isset($fila["reiteradaLegado"])) {echo $fila["reiteradaLegado"];}?></td>		
		<td class="thDetalleActuacion">Recomendacion Caja</td>
		<td class="celdaRes" colspan="1" style="text-align: left">&nbsp;<?php if (isset($fila["sugerenciaCaja"])) {echo $fila["sugerenciaCaja"];}?></td>		
	</tr>	

	<?php
	$i++;
}
?>
</table>	
