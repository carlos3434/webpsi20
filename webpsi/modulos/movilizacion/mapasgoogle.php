<?php

//include ("../../clases/class.Indicadores.php");
include("funciones.php");

require_once("../../clases/class.LBS.php");
/*
function analizarArreglo($tipoDetalle, $tipoActuacion, $negocio, $mdf, $nodo) {

	if ($tipoDetalle=="Por Vencer")
		$tipoDetalle = "porvencer";
	//echo $tipoDetalle;
	$Indicadores = new Indicadores();
	$arrDetalle = "";

	if ($tipoDetalle=="pendiente" && $tipoActuacion=="averias") {
		//echo "OK";
		switch ($negocio) {
			case 'tba':
				$arrDetalle= $Indicadores->getDetallePendAveriasTba($mdf);			
				break;
			case 'adsl':
				$arrDetalle= $Indicadores->getDetallePendAveriasAdsl($mdf);			
				break;			
			case 'catv':
				$arrDetalle= $Indicadores->getDetallePendAveriasCatv($nodo);			
				break;		
		}
	}
	else if ($tipoDetalle=="pendiente" && $tipoActuacion=="provision") {
		//echo "K2";
		switch ($negocio) {
			case 'tba':
				$arrDetalle= $Indicadores->getDetallePendProvisionTba($mdf);			
				break;
			case 'adsl':
				$arrDetalle= $Indicadores->getDetallePendProvisionAdsl($mdf);			
				break;			
			case 'catv':
				$arrDetalle= $Indicadores->getDetallePendProvisionCatv($nodo);			
				break;		
		}
	}
	else if ($tipoDetalle=="liquidadas" && $tipoActuacion=="provision") {
		//echo "K2";
		switch ($negocio) {
			case 'tba':
				$arrDetalle= $Indicadores->getDetalleLiqDiaProvisionTba($mdf,$hoyFecha);			
				break;
			case 'adsl':
				$arrDetalle= $Indicadores->getDetalleLiqDiaProvisionAdsl2($mdf, $hoyFecha);			
				break;			
			case 'catv':
				$arrDetalle= $Indicadores->getDetalleLiqDiaProvisionCatv($nodo, $hoyFecha);			
				break;		
		}
	}
	else if ($tipoDetalle=="liquidadas" && $tipoActuacion=="averias") {
		//echo "K2";
		switch ($negocio) {
			case 'tba':
				$arrDetalle= $Indicadores->getDetalleLiqDiaAveriasTba($mdf,$hoyFecha);			
				break;
			case 'adsl':
				$arrDetalle= $Indicadores->getDetalleLiqDiaAveriasAdsl($mdf, $hoyFecha);			
				break;			
			case 'catv':
				$arrDetalle= $Indicadores->getDetalleLiqDiaAveriasCatv($nodo, $hoyFecha);			
				break;		
		}
	}
	else if ($tipoDetalle=="porvencer" && $tipoActuacion=="provision") {
		//echo "K2";
		switch ($negocio) {
			case 'tba':
				$arrDetalle= $Indicadores->getDetallePorVencerProvisionTba($mdf,$hoyFecha);			
				break;
			case 'adsl':
				$arrDetalle= $Indicadores->getDetallePorVencerProvisionAdsl($mdf, $hoyFecha);			
				break;			
			case 'catv':
				$arrDetalle= $Indicadores->getDetallePorVencerProvisionCatv($nodo, $hoyFecha);			
				break;		
		}
	}
	else if ($tipoDetalle=="porvencer" && $tipoActuacion=="averias") {
		//echo "K2";
		switch ($negocio) {
			case 'tba':
				$arrDetalle= $Indicadores->getDetallePorVencerAveriasTba($mdf,$hoyFecha);			
				break;
			case 'adsl':
				$arrDetalle= $Indicadores->getDetallePorVencerAveriasAdsl($mdf, $hoyFecha);			
				break;			
			case 'catv':
				$arrDetalle= $Indicadores->getDetallePorVencerAveriasCatv($nodo, $hoyFecha);			
				break;		
		}
	}
	else if ($tipoDetalle=="vencidas" && $tipoActuacion=="provision") {
		//echo "K2";
		switch ($negocio) {
			case 'tba':
				$arrDetalle= $Indicadores->getVencidasDetalleProvisionTba($mdf,$hoyFecha);			
				break;
			case 'adsl':
				$arrDetalle= $Indicadores->getVencidasDetalleProvisionAdsl($mdf, $hoyFecha);			
				break;			
			case 'catv':
				$arrDetalle= $Indicadores->getVencidasDetalleProvisionCatv($nodo, $hoyFecha);			
				break;		
		}
	}
	else if ($tipoDetalle=="vencidas" && $tipoActuacion=="averias") {
		//echo "K2";
		switch ($negocio) {
			case 'tba':
				$arrDetalle= $Indicadores->getVencidasDetalleAveriasTba($mdf,$hoyFecha);			
				break;
			case 'adsl':
				$arrDetalle= $Indicadores->getVencidasDetalleAveriasAdsl($mdf, $hoyFecha);			
				break;			
			case 'catv':
				$arrDetalle= $Indicadores->getVencidasDetalleAveriasCatv($nodo, $hoyFecha);			
				break;		
		}
	}
	//var_dump($arrDetalle);
	return $arrDetalle;
}
*/

$direccionEnMapa = "";

$coordx = "-77.01557636260986";
$coordy = "-12.109214826084697";

$mdf = trim($_REQUEST["mdf"]);
$nodo = trim($_REQUEST["nodo"]);
$tipoDetalle = trim($_REQUEST["tipoDetalle"]);

$tipoActuacion = trim($_REQUEST["tipoActuacion"]);
$negocio = trim($_REQUEST["negocio"]);

$ConSinTecnico = $_REQUEST['cst'];

//var_dump($_REQUEST);

$arrResultado = analizarArreglo($tipoDetalle, $tipoActuacion, $negocio, $mdf, $nodo, $ConSinTecnico);

$sumaX = 0;
$sumaY = 0;
$contarXY = 0;

foreach ($arrResultado as $row ) {
	if ($row["coordX"]!='') {
		$sumaX += $row["coordX"];
		$sumaY += $row["coordY"];
		$contarXY++;
	}
}

$promedioX = $sumaX / $contarXY;
$promedioY = $sumaY / $contarXY;

/*EAHF - llamamos al metodo que busca los tecnicos mas cercanos a la caja terminal en referencia*/
$objTecnicos = new LBS();

$arrTecnicosCercanos = $objTecnicos->getTecnicosCercanos($promedioX, $promedioY, 
				0, 1000) ;

?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>

<!--<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false&language=es"></script>-->

<script type='text/javascript'>
<?php
    $js_array = json_encode($arrResultado);
    echo "var actuaciones = ". $js_array . ";\n";
    $js_arraytec = json_encode($arrTecnicosCercanos);
    echo "var tecnicosCercanos = ". $js_arraytec . ";\n";

    echo "var promedioX = ". $promedioX . ";\n";
    echo "var promedioY = ". $promedioY . ";\n";
?>

//alert(actuaciones[0]["llavexy"]);

</script>


<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false"></script>
<script type="text/javascript" src="js/jquery-1.7.1.min.js"></script>

<style type="text/css">
#divModalMapa {
	font-family: "Tahoma";
	font-size: 12px;
	border-top-width: 0px;
	border-right-width: 0px;
	border-bottom-width: 0px;
	border-left-width: 0px;
	border-top-style: none;
	border-right-style: none;
	border-bottom-style: none;
	border-left-style: none;
	letter-spacing: normal;
	text-align: left;
	vertical-align: middle;
	word-spacing: normal;
}
#map_canvas { height: 100% }

</style>

<script type="text/javascript">

function makeMapListener(window, map, markers) {
  return function() { window.open(map, markers); };
}

	function initialize() {
		var coordXInicial;
		var coordYInicial;
		//alert(actuaciones[0]["coordY"]);
		/*
		if (actuaciones[0]["coordY"]==null || actuaciones[0]["coordX"]==null) {
			coordXInicial = actuaciones[1]["coordX"];
			coordYInicial = actuaciones[1]["coordY"];
		}
		else
		{
			coordXInicial = actuaciones[0]["coordX"];
			coordYInicial = actuaciones[0]["coordY"];			
		}*/
		//alert(coordXInicial);
	    var latlng = new google.maps.LatLng(promedioY, promedioX );

	    var myOptions = {
	        zoom: 14,
	        center: latlng
	    }

	    var map = new google.maps.Map(document.getElementById("mapa"), myOptions);
	    //var image = 'img/terminal-2.gif';
	    	    //alert("tamaño array = "+actuaciones.length);
	    var image="";
	    var j=1;
	    var marker = new Array();
	    var marker_posX = new Array();
	    var infowindow = new Array();
	    var actuacion ;
	    var contentString;
	    var imageX;

		for (var i = 0; i < actuaciones.length; i++) {
			
			if (actuaciones[i]["marca"]==null || actuaciones[i]["marca"]=='' )
				imageX = "img/icos/verde/"+j+".png";
			else
				imageX = "img/icos/red/"+j+"-lv.png";
			
			//alert(actuaciones[i]["actuacion"]+" marca = "+actuaciones[i]["marca"]);
			actuacion = actuaciones[i]["actuacion"];

	        marker[i] = new google.maps.Marker({
	             position: new google.maps.LatLng(actuaciones[i]["coordY"], actuaciones[i]["coordX"]),
	             map: map,
	             icon: imageX,
	             size: new google.maps.Size(10, 22),
	             title: actuaciones[i]["actuacion"],
	             zIndex: i
	         });

			contentString = "Actuacion "+actuacion;

	        infowindow[i] = new google.maps.InfoWindow({
			      content: contentString
			});
			
			google.maps.event.addListener(marker[i], 'click', makeMapListener(infowindow[i], map, marker[i]));			


	        j++;
	     }

	    var image="";
	    var j=1;
	    var marker = new Array();
	    var marker_posX = new Array();
	    var infowindow = new Array();
	    var actuacion ;
	    var contentString;
	    var imageX;

		for (var i = 0; i < tecnicosCercanos.length; i++) {
			
			/*
			if (i!=0) {
				if (cajasCercanas[i]['qparlib']>2) 
					imageX = "../img/icos/verde/"+j+".png";
				else if (cajasCercanas[i]['qparlib']<=2 && cajasCercanas[i]['qparlib']>0 )
					imageX = "../img/icos/amarillo/"+j+".png";
				else 
					imageX = "../img/icos/red/"+j+"-lv.png";
			}
			else
			{
				imageX = "../img/icos/azul.png";
			} */

			imageX = "img/icos/tecnico.gif";

			actuacion = tecnicosCercanos[i]["celular"];

	        marker[i] = new google.maps.Marker({
	             position: new google.maps.LatLng(tecnicosCercanos[i]["lat_y"], tecnicosCercanos[i]["lon_x"]),
	             map: map,
	             icon: imageX,
	             size: new google.maps.Size(10, 22),
	             title: tecnicosCercanos[i]["nombre"],
	             zIndex: i
	         });

			contentString = "<div style='width: 250px'>";
			contentString = contentString+"<span style='font-size: 9px; font-weight: bold;'>Tecnico : "+tecnicosCercanos[i]["nombre"]+" "+tecnicosCercanos[i]["celular"]+"</span> <br>  ";
			contentString = contentString+"<span style='font-size: 9px;'>"+tecnicosCercanos[i]["lugar"]+"</span><br/>";
			contentString = contentString+"<span style='font-size: 9px; font-weight:bold;'>Fecha ult. posición: </span>";
			contentString = contentString+"<span style='font-size: 9px;'>"+tecnicosCercanos[i]["fecha"]+" </span>"
			contentString = contentString+"</div>";
	        infowindow[i] = new google.maps.InfoWindow({
			      content: contentString
			});
			
			google.maps.event.addListener(marker[i], 'click', makeMapListener(infowindow[i], map, marker[i]));			


	        j++;
	     }


	}

google.maps.event.addDomListener(window, 'load', initialize);



</script>

<body>

	<input type="hidden" value="<?=$promedioX?>" id="txtCoordX" name="txtCoordX" />
	<input type="hidden" value="<?=$promedioY?>" id="txtCoordY" name="txtCoordY" />
	<input type="hidden" value="<?=$direccionEnMapa?>" id="txtDireccionEnMapa" name="txtDireccionEnMapa" />
	<!--<input type="button" value="buscar" onclick="buscarXY();" />-->

<div id="mapa" style="width: 650px; height: 500px; line-height:normal;"></div>


</body>
