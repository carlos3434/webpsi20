<?php
include ("../../clases/class.Indicadores.php");

function analizarArreglo($tipoDetalle, $tipoActuacion, $negocio, $mdf, $nodo, $cst=null, $hoyFecha=null) {

	if ($tipoDetalle=="Por Vencer")
		$tipoDetalle = "porvencer";
	//echo $tipoDetalle;
	$Indicadores = new Indicadores();
	$arrDetalle = "";

	if ($tipoDetalle=="pendiente" && $tipoActuacion=="averias") {
		//echo "OK";
		switch ($negocio) {
			case 'tba':
				$arrDetalle= $Indicadores->getDetallePendAveriasTba($mdf, $cst);			
				break;
			case 'adsl':
				$arrDetalle= $Indicadores->getDetallePendAveriasAdsl($mdf, $cst);			
				break;			
			case 'catv':
				$arrDetalle= $Indicadores->getDetallePendAveriasCatv($nodo);			
				break;		
		}
	}
	else if ($tipoDetalle=="pendiente" && $tipoActuacion=="provision") {
		//echo "K2";
		switch ($negocio) {
			case 'tba':
				$arrDetalle= $Indicadores->getDetallePendProvisionTba($mdf, $cst);			
				break;
			case 'adsl':
				$arrDetalle= $Indicadores->getDetallePendProvisionAdsl($mdf, $cst);			
				break;			
			case 'catv':
				$arrDetalle= $Indicadores->getDetallePendProvisionCatv($nodo);			
				break;		
		}
	}
	else if ($tipoDetalle=="liquidadas" && $tipoActuacion=="provision") {
		//echo "K2";
		switch ($negocio) {
			case 'tba':
				$arrDetalle= $Indicadores->getDetalleLiqDiaProvisionTba($mdf,$hoyFecha);			
				break;
			case 'adsl':
				$arrDetalle= $Indicadores->getDetalleLiqDiaProvisionAdsl2($mdf, $hoyFecha);			
				break;			
			case 'catv':
				$arrDetalle= $Indicadores->getDetalleLiqDiaProvisionCatv($nodo, $hoyFecha);			
				break;		
		}
	}
	else if ($tipoDetalle=="liquidadas" && $tipoActuacion=="averias") {
		//echo "K2";
		switch ($negocio) {
			case 'tba':
				$arrDetalle= $Indicadores->getDetalleLiqDiaAveriasTba($mdf,$hoyFecha);			
				break;
			case 'adsl':
				$arrDetalle= $Indicadores->getDetalleLiqDiaAveriasAdsl($mdf, $hoyFecha);			
				break;			
			case 'catv':
				$arrDetalle= $Indicadores->getDetalleLiqDiaAveriasCatv($nodo, $hoyFecha);			
				break;		
		}
	}
	else if ($tipoDetalle=="porvencer" && $tipoActuacion=="provision") {
		//echo "K2";
		switch ($negocio) {
			case 'tba':
				$arrDetalle= $Indicadores->getDetallePorVencerProvisionTba($mdf,$hoyFecha, $cst);			
				break;
			case 'adsl':
				$arrDetalle= $Indicadores->getDetallePorVencerProvisionAdsl($mdf, $hoyFecha, $cst);			
				break;			
			case 'catv':
				$arrDetalle= $Indicadores->getDetallePorVencerProvisionCatv($nodo, $hoyFecha, $cst);			
				break;		
		}
	}
	else if ($tipoDetalle=="porvencer" && $tipoActuacion=="averias") {
		//echo "K2";
		switch ($negocio) {
			case 'tba':
				$arrDetalle= $Indicadores->getDetallePorVencerAveriasTba($mdf,$hoyFecha, $cst);			
				break;
			case 'adsl':
				$arrDetalle= $Indicadores->getDetallePorVencerAveriasAdsl($mdf, $hoyFecha, $cst);			
				break;			
			case 'catv':
				$arrDetalle= $Indicadores->getDetallePorVencerAveriasCatv($nodo, $hoyFecha, $cst);			
				break;		
		}
	}
	else if ($tipoDetalle=="vencidas" && $tipoActuacion=="provision") {
		//echo "K2";
		switch ($negocio) {
			case 'tba':
				$arrDetalle= $Indicadores->getVencidasDetalleProvisionTba($mdf,$hoyFecha, $cst);			
				break;
			case 'adsl':
				$arrDetalle= $Indicadores->getVencidasDetalleProvisionAdsl($mdf, $hoyFecha, $cst);			
				break;			
			case 'catv':
				$arrDetalle= $Indicadores->getVencidasDetalleProvisionCatv($nodo, $hoyFecha);			
				break;		
		}
	}
	else if ($tipoDetalle=="vencidas" && $tipoActuacion=="averias") {
		//echo "K2";
		switch ($negocio) {
			case 'tba':
				$arrDetalle= $Indicadores->getVencidasDetalleAveriasTba($mdf,$hoyFecha, $cst);			
				break;
			case 'adsl':
				$arrDetalle= $Indicadores->getVencidasDetalleAveriasAdsl($mdf, $hoyFecha, $cst);			
				break;			
			case 'catv':
				$arrDetalle= $Indicadores->getVencidasDetalleAveriasCatv($nodo, $hoyFecha);			
				break;		
		}
	}
	//var_dump($arrDetalle);
	return $arrDetalle;
}




function detalleActuacion($actuacion, $tipoDetalle, $tipoActuacion, $negocio) {

	//$arr = array ($actuacion, $tipoDetalle, $tipoActuacion, $negocio);

	$Indicadores = new Indicadores();
	$arrDetalle = "";

	if ($tipoDetalle=="pendiente" && $tipoActuacion=="averias") {
		//echo "OK";
		switch ($negocio) {
			case 'tba':
				$arrDetalle= $Indicadores->getActuacionPendienteDetalle_AveriasTba($actuacion);			
				break;
			case 'adsl':
				$arrDetalle= $Indicadores->getActuacionPendienteDetalle_AveriasAdsl($actuacion);			
				break;			
			case 'catv':
				$arrDetalle= $Indicadores->getActuacionPendienteDetalle_AveriasCatv($actuacion);			
				break;		
		}
	}
	else if ($tipoDetalle=="pendiente" && $tipoActuacion=="provision") {
		//echo "K2";
		switch ($negocio) {
			case 'tba':
				$arrDetalle= $Indicadores->getActuacionPendienteDetalle_ProvisionTba($actuacion);			
				break;
			case 'adsl':
				$arrDetalle= $Indicadores->getActuacionPendienteDetalle_ProvisionAdsl($actuacion);			
				break;			
			case 'catv':
				$arrDetalle= $Indicadores->getActuacionPendienteDetalle_ProvisionCatv	($actuacion);			
				break;		
		}
	}
	else if ($tipoDetalle=="liquidadas" && $tipoActuacion=="provision") {
		//echo "K2";
		switch ($negocio) {
			case 'tba':
				$arrDetalle= $Indicadores->getActuacionLiqDetalle_ProvisionTba($mdf,$hoyFecha);			
				break;
			case 'adsl':
				$arrDetalle= $Indicadores->getActuacionLiqDetalle_ProvisionTba($mdf, $hoyFecha);			
				break;			
			case 'catv':
				$arrDetalle= $Indicadores->getActuacionLiqDetalle_ProvisionTba($nodo, $hoyFecha);			
				break;		
		}
	}
	else if ($tipoDetalle=="liquidadas" && $tipoActuacion=="averias") {
		//echo "K2";
		switch ($negocio) {
			case 'tba':
				$arrDetalle= $Indicadores->getActuacionLiqDetalle_AveriasTba($mdf,$hoyFecha);			
				break;
			case 'adsl':
				$arrDetalle= $Indicadores->getActuacionLiqDetalle_AveriasTba($mdf, $hoyFecha);			
				break;			
			case 'catv':
				$arrDetalle= $Indicadores->getActuacionLiqDetalle_AveriasTba($nodo, $hoyFecha);			
				break;		
		}
	}
	else if ($tipoDetalle=="porvencer" && $tipoActuacion=="provision") {
		//echo "K2";
		switch ($negocio) {
			case 'tba':
				$arrDetalle= $Indicadores->getActuacionPendienteDetalle_ProvisionTba($actuacion );			
				break;
			case 'adsl':
				$arrDetalle= $Indicadores->getActuacionPendienteDetalle_ProvisionAdsl($actuacion );			
				break;			
			case 'catv':
				$arrDetalle= $Indicadores->getActuacionPendienteDetalle_ProvisionCatv($actuacion );			
				break;		
		}
	}
	else if ($tipoDetalle=="porvencer" && $tipoActuacion=="averias") {
		//echo "K2";
		switch ($negocio) {
			case 'tba':
				$arrDetalle= $Indicadores->getActuacionPendienteDetalle_AveriasTba($actuacion);			
				break;
			case 'adsl':
				$arrDetalle= $Indicadores->getActuacionPendienteDetalle_AveriasAdsl($actuacion);			
				break;			
			case 'catv':
				$arrDetalle= $Indicadores->getActuacionPendienteDetalle_AveriasCatv($actuacion);			
				break;	
		}
	}
	else if ($tipoDetalle=="vencidas" && $tipoActuacion=="provision") {
		//echo "K2";
		switch ($negocio) {
			case 'tba':
				$arrDetalle= $Indicadores->getActuacionPendienteDetalle_ProvisionTba($actuacion);			
				break;
			case 'adsl':
				$arrDetalle= $Indicadores->getActuacionPendienteDetalle_ProvisionAdsl($actuacion);			
				break;			
			case 'catv':
				$arrDetalle= $Indicadores->getActuacionPendienteDetalle_ProvisionCatv($actuacion);			
				break;		
		}
	}
	else if ($tipoDetalle=="vencidas" && $tipoActuacion=="averias") {
		//echo "K2";
		switch ($negocio) {
			case 'tba':
				$arrDetalle= $Indicadores->getActuacionPendienteDetalle_AveriasTba($actuacion);			
				break;
			case 'adsl':
				$arrDetalle= $Indicadores->getActuacionPendienteDetalle_AveriasAdsl($actuacion);			
				break;			
			case 'catv':
				$arrDetalle= $Indicadores->getActuacionPendienteDetalle_AveriasCatv($actuacion);			
				break;	
		}
	}	

	return $arrDetalle;

}


?>