<?php
header('Content-Type: text/html; charset=UTF-8');
header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache");


include ("../../clases/class.Indicadores.php");


$hoy = date("Y-m-d H:i:s");
$hoyFormato = date("d-m-Y H:i:s");
$hoyFecha = date("Y-m-d");

$Indicadores = new Indicadores();

//$mdf = trim($_POST["mdf"]);
//$nodo = trim($_POST["nodo"]);
$tipoDetalle = trim($_POST["tipoDetalle"]);
$tipoActuacion = trim($_POST["tipoActuacion"]);
$negocio = trim($_POST["negocio"]);
if (isset($_POST["ConSinTecnico"])){
	$ConSinTecnico = trim($_POST["ConSinTecnico"]);
} else {
	$ConSinTecnico = "";
}

//Multiple MDF/NODO
$mdf = "";
$nodo = "";
if ( isset($_POST["mdf"]) ) {
	foreach ($_POST["mdf"] as $key=>$val) {
		$mdf .= "'$val', ";
	}
	$mdf = substr($mdf, 0, -2);
}
if ( isset($_POST["nodo"]) ) {
	foreach ($_POST["nodo"] as $key=>$val) {
		$nodo .= "'$val', ";
	}
	$nodo = substr($nodo, 0, -2);
}

//echo "tipoDetalle = ".$tipoDetalle.", tipoActuacion=".$tipoActuacion.", negocio=".$negocio;

if ($tipoDetalle=="pendiente" && $tipoActuacion=="averias") {
	//echo "OK";
	if ($ConSinTecnico=="1") { 
		switch ($negocio) {
			case 'tba':
				$arrDetalle= $Indicadores->getDetallePendAveriasTba($mdf, $_POST["ConSinTecnico"]);			
				break;
			case 'adsl':
				$arrDetalle= $Indicadores->getDetallePendAveriasAdsl($mdf, $_POST["ConSinTecnico"]);			
				break;			
			case 'catv':
				$arrDetalle= $Indicadores->getDetallePendAveriasCatv_ConSinTecnico($nodo,1);			
				break;		
		}
	}
	else
	{
		switch ($negocio) {
			case 'tba':
				$arrDetalle= $Indicadores->getDetallePendAveriasTba($mdf, $_POST["ConSinTecnico"]);			
				break;
			case 'adsl':
				$arrDetalle= $Indicadores->getDetallePendAveriasAdsl($mdf, $_POST["ConSinTecnico"]);			
				break;			
			case 'catv':
				$arrDetalle= $Indicadores->getDetallePendAveriasCatv_ConSinTecnico($nodo, 0);			
				break;		
		}
	
	}
}
else if ($tipoDetalle=="pendiente" && $tipoActuacion=="provision") {
	//echo "K2";
	switch ($negocio) {
		case 'tba':
			$arrDetalle= $Indicadores->getDetallePendProvisionTba($mdf, $_POST["ConSinTecnico"]);			
			break;
		case 'adsl':
			$arrDetalle= $Indicadores->getDetallePendProvisionAdsl($mdf, $_POST["ConSinTecnico"]);			
			break;			
		case 'catv':
			$arrDetalle= $Indicadores->getDetallePendProvisionCatv($nodo);			
			break;		
	}
}
else if ($tipoDetalle=="liquidadas" && $tipoActuacion=="provision") {
	//echo "K2";
	switch ($negocio) {
		case 'tba':
			$arrDetalle= $Indicadores->getDetalleLiqDiaProvisionTba($mdf,$hoyFecha);			
			break;
		case 'adsl':
			$arrDetalle= $Indicadores->getDetalleLiqDiaProvisionAdsl2($mdf, $hoyFecha);			
			break;			
		case 'catv':
			$arrDetalle= $Indicadores->getDetalleLiqDiaProvisionCatv($nodo, $hoyFecha);			
			break;		
	}
}
else if ($tipoDetalle=="liquidadas" && $tipoActuacion=="averias") {
	//echo "K2";
	switch ($negocio) {
		case 'tba':
			$arrDetalle= $Indicadores->getDetalleLiqDiaAveriasTba($mdf,$hoyFecha);			
			break;
		case 'adsl':
			$arrDetalle= $Indicadores->getDetalleLiqDiaAveriasAdsl($mdf, $hoyFecha);			
			break;			
		case 'catv':
			$arrDetalle= $Indicadores->getDetalleLiqDiaAveriasCatv($nodo, $hoyFecha);			
			break;		
	}
}
else if ($tipoDetalle=="porvencer" && $tipoActuacion=="provision") {
	//echo "K2";
	switch ($negocio) {
		case 'tba':
			$arrDetalle= $Indicadores->getDetallePorVencerProvisionTba($mdf,$hoyFecha, $_POST["ConSinTecnico"]);			
			break;
		case 'adsl':
			$arrDetalle= $Indicadores->getDetallePorVencerProvisionAdsl($mdf, $hoyFecha, $_POST["ConSinTecnico"]);			
			break;			
		case 'catv':
			$arrDetalle= $Indicadores->getDetallePorVencerProvisionCatv($nodo, $hoyFecha);			
			break;		
	}
}
else if ($tipoDetalle=="porvencer" && $tipoActuacion=="averias" ) {
	//echo "K2";
	if ($ConSinTecnico=="1") {
		switch ($negocio) {
			case 'tba':
				$arrDetalle= $Indicadores->getDetallePorVencerAveriasTba($mdf,$hoyFecha, $_POST["ConSinTecnico"]);			
				break;
			case 'adsl':
				$arrDetalle= $Indicadores->getDetallePorVencerAveriasAdsl($mdf, $hoyFecha, $_POST["ConSinTecnico"]);			
				break;			
			case 'catv':
				$arrDetalle= $Indicadores->getDetallePorVencerAveriasCatv_ConSinTecnico($nodo, $hoyFecha, 1);			
				break;		
		}
	}
	else {
		switch ($negocio) {
			case 'tba':
				$arrDetalle= $Indicadores->getDetallePorVencerAveriasTba($mdf,$hoyFecha);			
				break;
			case 'adsl':
				$arrDetalle= $Indicadores->getDetallePorVencerAveriasAdsl($mdf, $hoyFecha);			
				break;			
			case 'catv':
				$arrDetalle= $Indicadores->getDetallePorVencerAveriasCatv_ConSinTecnico($nodo, $hoyFecha, 0);			
				break;		
		}
	}
	
}
else if ($tipoDetalle=="vencidas" && $tipoActuacion=="provision") {
	//echo "K2";
	switch ($negocio) {
		case 'tba':
			$arrDetalle= $Indicadores->getVencidasDetalleProvisionTba($mdf,$hoyFecha, $_POST["ConSinTecnico"]);			
			break;
		case 'adsl':
			$arrDetalle= $Indicadores->getVencidasDetalleProvisionAdsl($mdf, $hoyFecha, $_POST["ConSinTecnico"]);			
			break;			
		case 'catv':
			$arrDetalle= $Indicadores->getVencidasDetalleProvisionCatv($nodo, $hoyFecha, $_POST["ConSinTecnico"]);			
			break;		
	}
}
else if ($tipoDetalle=="vencidas" && $tipoActuacion=="averias") {
	//echo "K2";
	switch ($negocio) {
		case 'tba':
			$arrDetalle= $Indicadores->getVencidasDetalleAveriasTba($mdf,$hoyFecha, $_POST["ConSinTecnico"]);			
			break;
		case 'adsl':
			$arrDetalle= $Indicadores->getVencidasDetalleAveriasAdsl($mdf, $hoyFecha, $_POST["ConSinTecnico"]);			
			break;			
		case 'catv':
			$arrDetalle= $Indicadores->getVencidasDetalleAveriasCatv($nodo, $hoyFecha, $_POST["ConSinTecnico"]);			
			break;		
	}
}
else if ($tipoDetalle=="agendas" && $tipoActuacion=="provision") {
	//echo "K2";
	switch ($negocio) {
		case 'tba':
			$arrDetalle= $Indicadores->getAgendasDetalleProvisionTba($mdf,$hoyFecha);			
			break;
		case 'adsl':
			$arrDetalle= $Indicadores->getAgendasDetalleProvisionTba($mdf, $hoyFecha);			
			break;			
		case 'catv':
			$arrDetalle= $Indicadores->getAgendasDetalleProvisionTba($nodo, $hoyFecha);			
			break;		
	}
}
else if ($tipoDetalle=="agendas" && $tipoActuacion=="averias") {
	//echo "K2";
	switch ($negocio) {
		case 'tba':
			$arrDetalle= $Indicadores->getAgendasDetalleAverias($mdf,$hoyFecha, 'tba');			
			break;
		case 'adsl':
			$arrDetalle= $Indicadores->getAgendasDetalleAverias($mdf, $hoyFecha, 'adsl');			
			break;			
		case 'catv':
			$arrDetalle= $Indicadores->getAgendasDetalleAverias($nodo, $hoyFecha, 'catv');			
			break;		
	}
}

	echo "<input type='hidden' id='txtTipoDetalle' value='$tipoDetalle' />";
	echo "<input type='hidden' id='txtTipoActuacion' value='$tipoActuacion' />";
	echo "<input type='hidden' id='txtNegocio' value='$negocio' />";
	echo "<input type='hidden' id='txtMdf' value='$mdf' />";
?>

<script>
	function descargaExcel() {
		var tipoDetalle = $("#txtTipoDetalle").val();
		var tipoActuacion = $("#txtTipoActuacion").val();
		var negocio = $("#txtNegocio").val();
		
		var mdf = $("#txtMdf").val();
		
		//alert("tipo detalle="+tipoDetalle+", tipo actuacion="+tipoActuacion+", negocio="+negocio);
		var pagina="descargarExcel.php?tipoDetalle="+tipoDetalle+"&tipoActuacion="+tipoActuacion+"&negocio="+negocio+"&mdf="+mdf;
		
		window.open(pagina);
		
	}
</script>

<br/>


	
<table class="table">
<thead>
<th colspan="8" style="height: 24px; background: #FFDE40; color: #17688B; vertical-align:middle">
	<?php 

	//var_dump($arrDetalle);

	switch ($tipoDetalle) {
		case 'porvencer':
			$tipoDetalle = "Por Vencer";
			$tipoDetalleOriginal = "porvencer";
			break;	
		default:
			$tipoDetalleOriginal = $tipoDetalle;
			# code...
			break;
	}
	echo strtoupper($tipoActuacion)." / ".strtoupper($negocio)." - ".strtoupper($tipoDetalle) ;
	
	?>
	<a href='#' onclick="descargaExcel()"  >
		<img src='img/excel_icon.gif' alt='descarga excel' title='descarga excel' width='16' height='16'
		style='padding: 3px;'/>
	</a>
	
	
</th>
</thead>
<thead>
	<th colspan='1' class="thDetalle">#</th>
	<th colspan='1' width="15%" class="thDetalle">Actuacion</th>
	<th colspan='1' width="15%" class="thDetalle">Mdf<br/>Nodo</th>
	<th colspan='1' width="7%" class="thDetalle">SEG</th>	
	<th colspan='1' width="15%" class="thDetalle">Tecnico</th>	
	<th colspan='1' width="5%"  class="thDetalle">Fecha<br/>Registro</th>
	<th colspan='1' class="thDetalle">Estado</th>
	<th colspan='1' class="thDetalle">Marca</th>
	<!--<th colspan='1' class="thDetalle">X/Y</th>-->
</thead>		

<?php
$i=1;
//var_dump($arrDetalle);
if (isset($arrDetalle) and !empty($arrDetalle)) {
	foreach ($arrDetalle as $fila) {
		?>
		<tr>
			<td class="celdaRes">
			<?php 
				/*
				if ($fila["coordX"]=='' or $fila["coordY"]=='')
					$colorI = "<span style='color: red;'>".$i."</span>";
				else
					$colorI = $i;
				echo $colorI;
				*/
				echo $i;
			?>
			</td>
			<td class="celdaRes">
			<?php 
				//$actuacionMostrar = "<a href='javascript:void(0);' onclick=\"void verDetalleActuacion('".$fila["actuacion"]."', '$tipoDetalleOriginal', '$tipoActuacion', '$negocio');\">";
				$actuacionMostrar = "<a href='#targeDetalle' onclick=\"void verDetalleActuacion('".$fila["actuacion"]."', '$tipoDetalleOriginal', '$tipoActuacion', '$negocio');\">";
				$actuacionMostrar .= $fila["actuacion"]."</a>";
				$actuacionMostrar .= " / ";
				if ($negocio=="catv")
						$actuacionMostrar .= $fila["inscripcion"];
				else 
						$actuacionMostrar .= $fila["telefono"];
				echo $actuacionMostrar;
			?>
			</td>

			<td class="celdaRes"><!--<?=$fila["fftt"]." ".$fila["coordX"]." ".$fila["coordY"];?>--><?php if (isset($fila["mdf"])){echo $fila["mdf"];}?></td>
			<td class="celdaRes"><!--<?=$fila["segmentoY"]?>--></td>
			<td class="celdaRes"><?php if (isset($fila["cod_tecnico"])) { echo $fila["cod_tecnico"];}?></td>
			<td class="celdaRes"><?=$fila["fecreg"]?></td>
			<td class="celdaRes"><?php if ( isset($fila["estado"]) ) { echo $fila["estado"];}?></td>
			<td class="celdaRes"><!--<?=$fila["marca"]." / A:".$fila["fec_agenda"]?>--><?php echo " / A:";//.$fila["fec_agenda"]?></td>
			<?php /*<td class="celdaRes"><?=$fila["coordX"]." / ".$fila["coordY"]?></td>*/ ?>
		</tr>
		<?php
		$i++;
	}
}
?>
</table>



<br/>

<div id="xx1" class="divResMain" style="border: 1px ; display: table;">
	<a name="targeDetalle" ></a>

<div id="divDetalleActuacion"></div>

<br/>
<div id="divMapa" class="mapa" >
	<iframe src="mapasgoogle.php?mdf=<?=$mdf?>&nodo=<?=$nodo?>&tipoDetalle=<?=$tipoDetalle?>&tipoActuacion=<?=$tipoActuacion?>&negocio=<?=$negocio?>&cst=<?php echo $ConSinTecnico;?>" 
			id="frame_mapa" style="width: 100%; height: 100%;" ></iframe> 
</div>



<br/>
</table>


<br/>


