<?php
session_start();

require_once("../../cabecera.php");


/* DETECTAR SI ES DISPOSITIVO MOVIL */
include '../../mobile_device_detect.php';
$is_movil = mobile_device_detect();
if( $is_movil ) {
	$_SESSION['IS_MOBILE'] = 1;
	$_SESSION['PREF_MOBILE'] = "m.";
	header ("Location: m.movilizacion.php");
}else {
	
	$_SESSION['IS_MOBILE'] = 0;
	$_SESSION['PREF_MOBILE'] = "";
	
	echo "NO ES MOVIL";
	header ("Location: movilizacion.php");

}

//@ if logoff
if(!empty($_GET['logoff'])) { $_SESSION = array(); }


?>
