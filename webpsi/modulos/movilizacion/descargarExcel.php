<?php
/*header("Content-Type:   application/vnd.ms-excel; charset=utf-8");
header("Content-Disposition: attachment; filename=abc.xls");  //File name extension was wrong
header("Expires: 0");
header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
header("Cache-Control: private",false);
*/

header('Content-type: application/ms-excel');
header('Content-Disposition: attachment; filename=reporte.xls');

include ("../../clases/class.Indicadores.php");

$tipoDetalle = $_REQUEST["tipoDetalle"];
$tipoActuacion = $_REQUEST["tipoActuacion"];
$negocio = $_REQUEST["negocio"];
$mdf = $_REQUEST["mdf"];

$Indicadores = new Indicadores();


//echo "mdf=".$mdf.", tipoDetalle = ".$tipoDetalle.", tipoActuacion=".$tipoActuacion.", negocio=".$negocio."<br/>";

if ($tipoDetalle=="pendiente" && $tipoActuacion=="averias") {
	//echo "OK";
	if ($ConSinTecnico=="1") { 
		switch ($negocio) {
			case 'tba':
				$arrDetalle= $Indicadores->getDetallePendAveriasTba($mdf);			
				break;
			case 'adsl':
				$arrDetalle= $Indicadores->getDetallePendAveriasAdsl($mdf);			
				break;			
			case 'catv':
				$arrDetalle= $Indicadores->getDetallePendAveriasCatv_ConSinTecnico($nodo,1);			
				break;		
		}
	}
	else
	{
		switch ($negocio) {
			case 'tba':
				$arrDetalle= $Indicadores->getDetallePendAveriasTba($mdf);			
				break;
			case 'adsl':
				$arrDetalle= $Indicadores->getDetallePendAveriasAdsl($mdf);			
				break;			
			case 'catv':
				$arrDetalle= $Indicadores->getDetallePendAveriasCatv_ConSinTecnico($nodo, 0);			
				break;		
		}
	
	}
}
else if ($tipoDetalle=="pendiente" && $tipoActuacion=="provision") {
	//echo "K2";
	switch ($negocio) {
		case 'tba':
			$arrDetalle= $Indicadores->getDetallePendProvisionTba($mdf);			
			break;
		case 'adsl':
			$arrDetalle= $Indicadores->getDetallePendProvisionAdsl($mdf);			
			break;			
		case 'catv':
			$arrDetalle= $Indicadores->getDetallePendProvisionCatv($nodo);			
			break;		
	}
}
else if ($tipoDetalle=="liquidadas" && $tipoActuacion=="provision") {
	//echo "K2";
	switch ($negocio) {
		case 'tba':
			$arrDetalle= $Indicadores->getDetalleLiqDiaProvisionTba($mdf,$hoyFecha);			
			break;
		case 'adsl':
			$arrDetalle= $Indicadores->getDetalleLiqDiaProvisionAdsl2($mdf, $hoyFecha);			
			break;			
		case 'catv':
			$arrDetalle= $Indicadores->getDetalleLiqDiaProvisionCatv($nodo, $hoyFecha);			
			break;		
	}
}
else if ($tipoDetalle=="liquidadas" && $tipoActuacion=="averias") {
	//echo "K2";
	switch ($negocio) {
		case 'tba':
			$arrDetalle= $Indicadores->getDetalleLiqDiaAveriasTba($mdf,$hoyFecha);			
			break;
		case 'adsl':
			$arrDetalle= $Indicadores->getDetalleLiqDiaAveriasAdsl($mdf, $hoyFecha);			
			break;			
		case 'catv':
			$arrDetalle= $Indicadores->getDetalleLiqDiaAveriasCatv($nodo, $hoyFecha);			
			break;		
	}
}
else if ($tipoDetalle=="porvencer" && $tipoActuacion=="provision") {
	//echo "K2";
	switch ($negocio) {
		case 'tba':
			$arrDetalle= $Indicadores->getDetallePorVencerProvisionTba($mdf,$hoyFecha);			
			break;
		case 'adsl':
			$arrDetalle= $Indicadores->getDetallePorVencerProvisionAdsl($mdf, $hoyFecha);			
			break;			
		case 'catv':
			$arrDetalle= $Indicadores->getDetallePorVencerProvisionCatv($nodo, $hoyFecha);			
			break;		
	}
}
else if ($tipoDetalle=="porvencer" && $tipoActuacion=="averias" ) {
	//echo "K2";
	if ($ConSinTecnico=="1") {
		switch ($negocio) {
			case 'tba':
				$arrDetalle= $Indicadores->getDetallePorVencerAveriasTba($mdf,$hoyFecha);			
				break;
			case 'adsl':
				$arrDetalle= $Indicadores->getDetallePorVencerAveriasAdsl($mdf, $hoyFecha);			
				break;			
			case 'catv':
				$arrDetalle= $Indicadores->getDetallePorVencerAveriasCatv_ConSinTecnico($nodo, $hoyFecha, 1);			
				break;		
		}
	}
	else {
		switch ($negocio) {
			case 'tba':
				$arrDetalle= $Indicadores->getDetallePorVencerAveriasTba($mdf,$hoyFecha);			
				break;
			case 'adsl':
				$arrDetalle= $Indicadores->getDetallePorVencerAveriasAdsl($mdf, $hoyFecha);			
				break;			
			case 'catv':
				$arrDetalle= $Indicadores->getDetallePorVencerAveriasCatv_ConSinTecnico($nodo, $hoyFecha, 0);			
				break;		
		}
	}
	
}
else if ($tipoDetalle=="vencidas" && $tipoActuacion=="provision") {
	//echo "K2";
	switch ($negocio) {
		case 'tba':
			$arrDetalle= $Indicadores->getVencidasDetalleProvisionTba($mdf,$hoyFecha);			
			break;
		case 'adsl':
			$arrDetalle= $Indicadores->getVencidasDetalleProvisionAdsl($mdf, $hoyFecha);			
			break;			
		case 'catv':
			$arrDetalle= $Indicadores->getVencidasDetalleProvisionCatv($nodo, $hoyFecha);			
			break;		
	}
}
else if ($tipoDetalle=="vencidas" && $tipoActuacion=="averias") {
	//echo "K2";
	switch ($negocio) {
		case 'tba':
			$arrDetalle= $Indicadores->getVencidasDetalleAveriasTba($mdf,$hoyFecha);			
			break;
		case 'adsl':
			$arrDetalle= $Indicadores->getVencidasDetalleAveriasAdsl($mdf, $hoyFecha);			
			break;			
		case 'catv':
			$arrDetalle= $Indicadores->getVencidasDetalleAveriasCatv($nodo, $hoyFecha);			
			break;		
	}
}
else if ($tipoDetalle=="agendas" && $tipoActuacion=="provision") {
	//echo "K2";
	switch ($negocio) {
		case 'tba':
			$arrDetalle= $Indicadores->getAgendasDetalleProvisionTba($mdf,$hoyFecha);			
			break;
		case 'adsl':
			$arrDetalle= $Indicadores->getAgendasDetalleProvisionTba($mdf, $hoyFecha);			
			break;			
		case 'catv':
			$arrDetalle= $Indicadores->getAgendasDetalleProvisionTba($nodo, $hoyFecha);			
			break;		
	}
}
else if ($tipoDetalle=="agendas" && $tipoActuacion=="averias") {
	//echo "K2";
	switch ($negocio) {
		case 'tba':
			$arrDetalle= $Indicadores->getAgendasDetalleAverias($mdf,$hoyFecha, 'tba');			
			break;
		case 'adsl':
			$arrDetalle= $Indicadores->getAgendasDetalleAverias($mdf, $hoyFecha, 'adsl');			
			break;			
		case 'catv':
			$arrDetalle= $Indicadores->getAgendasDetalleAverias($nodo, $hoyFecha, 'catv');			
			break;		
	}
}


//var_dump($arrDetalle);
echo "<table cellpadding='0' cellspacing='0' border='1'>";

echo "<th>Actuacion</th>";
echo "<th>Mdf</th>";
echo "<th>SEG</th>";	
echo "<th>Tecnico</th>";
echo "<th>Fecha</th>";
echo "<th>Estado</th>";
echo "<th>Marca</th>";

foreach($arrDetalle as $fila) {
	echo "<tr>";
	
	if ($negocio=="catv")
		$codCliente = $fila["inscripcion"];
	else 
		$codCliente = $fila["telefono"];

	echo "<td>".$fila["actuacion"]." / ".$codCliente."</td>";

	echo "<td>".$fila["fftt"]." ".$fila["coordX"]." ".$fila["coordY"]."</td>";
	echo "<td>".$fila["segmentoY"]."</td>";
	echo "<td>".$fila["cod_tecnico"]."</td>";
	echo "<td>".$fila["fecreg"]."</td>";
	echo "<td>".$fila["estado"]."</td>";
	echo "<td>".$fila["marca"]." / A:".$fila["fec_agenda"]."</td>";
	
	echo "</tr>";
		
}


echo "</table>";	
