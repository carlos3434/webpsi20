<?php
header('Content-Type: text/html; charset=UTF-8');
header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache");


include ("../../clases/class.Indicadores.php");

$hoy = date("Y-m-d H:i:s");
$hoyFormato = date("d-m-Y H:i:s");

$Indicadores = new Indicadores();

//$mdf = $_POST["mdf"];
//$nodo = $_POST["nodo"];

//Multiple MDF/NODO
$mdf = "";
$nodo = "";
if ( isset($_POST["mdf"]) ) {
	foreach ($_POST["mdf"] as $key=>$val) {
		$mdf .= "'$val', ";
	}
	$mdf = substr($mdf, 0, -2);
}
if ( isset($_POST["nodo"]) ) {
	foreach ($_POST["nodo"] as $key=>$val) {
		$nodo .= "'$val', ";
	}
	$nodo = substr($nodo, 0, -2);
}

echo "<b>".$mdf." / ".$nodo."</b><br/>";

//echo $Indicadores->getFechaUpdateBasica();

/** PROVISION **/
// TBA BASICA
$pendTbaProvisionCT = 0;
$arrPendTbaProvisionCT= $Indicadores->getPendProvisionTba_ConSinTenico($mdf,1);
if (!empty($arrPendTbaProvisionCT)) {
	foreach ($arrPendTbaProvisionCT as $key ) {
		$pendTbaProvisionCT += $key[2];
	}
}
$pendTbaProvisionCT = "<a href='#targetListado' onclick=\"verDetalle2('pendiente','provision','tba', '1')\" >".$pendTbaProvisionCT."</a>";

$pendTbaProvisionST = 0;
$arrPendTbaProvisionST= $Indicadores->getPendProvisionTba_ConSinTenico($mdf,0);
foreach ($arrPendTbaProvisionST as $key ) {
	$pendTbaProvisionST += $key[2];
}
$pendTbaProvisionST = "<a href='#targetListado' onclick=\"verDetalle2('pendiente','provision','tba', '0')\" >".$pendTbaProvisionST."</a>";

// ADSL
$pendADSLProvisionCT = 0;
$arrPendAdslProvisionCT = $Indicadores->getPendProvisionAdsl_ConSinTenico($mdf,1);
foreach ($arrPendAdslProvisionCT as $key ) {
	$pendADSLProvisionCT += $key[2];
}
$pendADSLProvisionCT = "<a href='#targetListado' onclick=\"verDetalle2('pendiente','provision','adsl', '1')\">".$pendADSLProvisionCT."</a>";

$pendADSLProvisionST = 0;
$arrPendAdslProvisionST = $Indicadores->getPendProvisionAdsl_ConSinTenico($mdf,0);
foreach ($arrPendAdslProvisionST as $key ) {
	$pendADSLProvisionST += $key[2];
}
$pendADSLProvisionST = "<a href='#targetListado' onclick=\"verDetalle2('pendiente','provision','adsl', '0')\">".$pendADSLProvisionST."</a>";


// CATV
$pendCATVProvision = 0;
$arrPendCatvProvision = $Indicadores->getPendProvisionCatv($nodo);
foreach ($arrPendCatvProvision as $key ) {
	$pendCATVProvision += $key[1];
}
$pendCATVProvision = "<a href='#targetListado' onclick=\"verDetalle('pendiente','provision','catv')\">".$pendCATVProvision."</a>";




/** AVERIAS **/
// BASICA
$pendTbaAveriasCT = 0;
$arrPendTbaAveriasCT = $Indicadores->getPendAveriasTba_ConSinTenico($mdf, 1);
if (is_array($arrPendTbaAveriasCT) and !empty($arrPendTbaAveriasCT)) {
	foreach ($arrPendTbaAveriasCT as $key ) {
		$pendTbaAveriasCT += $key[1];
	}
	if (count($arrPendTbaAveriasCT) == 0) {
		$pendTbaAveriasCT = 0;
	}

	$pendTbaAveriasCT = "<a href='#targetListado' onclick=\"verDetalle2('pendiente','averias','tba', '1')\">".$pendTbaAveriasCT."</a>";
}

$pendTbaAveriasST = 0;
$arrPendTbaAveriasST= $Indicadores->getPendAveriasTba_ConSinTenico($mdf, 0);
if (count($arrPendTbaAveriasST) == 0)
	$pendTbaAveriasST = 0;
else {
	foreach ($arrPendTbaAveriasST as $key ) {
		$pendTbaAveriasST += $key[1];
	}
}

$pendTbaAveriasST = "<a href='#targetListado' onclick=\"verDetalle2('pendiente','averias','tba', '0')\">".$pendTbaAveriasST."</a>";


// ADSL
$pendADSLAveriasCT = 0;
$arrPendAdslAveriasCT = $Indicadores->getPendAveriasAdsl_ConSinTenico($mdf, 1);
if (count($arrPendAdslAveriasCT) == 0)
	$pendADSLAveriasCT = 0;
else {
	foreach ($arrPendAdslAveriasCT as $key ) {
		$pendADSLAveriasCT += $key[1];
	}
}
$pendADSLAveriasCT = "<a href='#targetListado' onclick=\"verDetalle2('pendiente','averias','adsl', '1')\">".$pendADSLAveriasCT."</a>";

$pendADSLAveriasST = 0;
$arrPendAdslAveriasST = $Indicadores->getPendAveriasAdsl_ConSinTenico($mdf, 0);
if (count($arrPendAdslAveriasST) == 0)
	$pendADSLAveriasST = 0;
else {
	foreach ($arrPendAdslAveriasST as $key ) {
		$pendADSLAveriasST += $key[1];
	}
}
$pendADSLAveriasST = "<a href='#targetListado' onclick=\"verDetalle2('pendiente','averias','adsl', '0')\">".$pendADSLAveriasST."</a>";


// CATV
$pendCATVAveriasCT = 0;
$arrPendCatvAveriasCT= $Indicadores->getPendAveriasCatv_ConSinTenico($nodo, 1);
if (is_array($arrPendCatvAveriasCT) and !empty($arrPendCatvAveriasCT)) {
	foreach ($arrPendCatvAveriasCT as $key ) {
		$pendCATVAveriasCT += $key[1];
	}
	$pendCATVAveriasCT = "<a href='#targetListado' onclick=\"verDetalle2('pendiente','averias','catv', 1)\">".$pendCATVAveriasCT."</a>";
}

$pendCATVAveriasST = 0;
$arrPendCatvAveriasST= $Indicadores->getPendAveriasCatv_ConSinTenico($nodo, 0);
if (is_array($arrPendCatvAveriasST) and !empty($arrPendCatvAveriasST)) {
	foreach ($arrPendCatvAveriasST as $key ) {
		$pendCATVAveriasST += $key[1];
	}
	$pendCATVAveriasST = "<a href='#targetListado' onclick=\"verDetalle2('pendiente','averias','catv', 0)\">".$pendCATVAveriasST."</a>";
}


/** POR VENCER PROVISION **/
// TBA BASICA
$pendTbaProvisionVencerCT = $Indicadores->getPorVencerProvisionTba_ConSinTenico($mdf, 1);
$pendTbaProvisionVencerCT = "<a href='#targetListado' onclick=\"verDetalle2('porvencer','provision','tba', '1')\"  >".$pendTbaProvisionVencerCT."</a>";

$pendTbaProvisionVencerST = $Indicadores->getPorVencerProvisionTba_ConSinTenico($mdf, 0);
$pendTbaProvisionVencerST = "<a href='#targetListado' onclick=\"verDetalle2('porvencer','provision','tba', '0')\"  >".$pendTbaProvisionVencerST."</a>";


// ADSL
$pendAdslProvisionVencerCT = $Indicadores->getPorVencerProvisionAdsl_ConSinTenico($mdf, 1);
$pendAdslProvisionVencerCT = "<a href='#targetListado' onclick=\"verDetalle2('porvencer','provision','adsl', '1')\">".$pendAdslProvisionVencerCT."</a>";

$pendAdslProvisionVencerST = $Indicadores->getPorVencerProvisionAdsl_ConSinTenico($mdf, 0);
$pendAdslProvisionVencerST = "<a href='#targetListado' onclick=\"verDetalle2('porvencer','provision','adsl', '0')\">".$pendAdslProvisionVencerST."</a>";


$pendCatvProvisionVencer = $Indicadores->getPorVencerProvisionCatv($nodo);
$pendCatvProvisionVencer = "<a href='#targetListado' onclick=\"verDetalle('porvencer','provision','catv')\">".$pendCatvProvisionVencer."</a>";
/******/


/** POR VENCER AVERIAS **/
// BASICA
$pendTbaAveriasVencerCT = $Indicadores->getPorVencerAveriasTba_ConSinTenico($mdf,1);
$pendTbaAveriasVencerCT = "<a href='#targetListado' onclick=\"verDetalle2('porvencer','averias','tba', '1')\"  >".$pendTbaAveriasVencerCT."</a>";

$pendTbaAveriasVencerST = $Indicadores->getPorVencerAveriasTba_ConSinTenico($mdf,0);
$pendTbaAveriasVencerST = "<a href='#targetListado' onclick=\"verDetalle2('porvencer','averias','tba', '0')\"  >".$pendTbaAveriasVencerST."</a>";


// ADSL
$pendAdslAveriasVencerCT = $Indicadores->getPorVencerAveriasAdsl_ConSinTenico($mdf,1);
$pendAdslAveriasVencerCT = "<a href='#targetListado' onclick=\"verDetalle2('porvencer','averias','adsl', '1')\">".$pendAdslAveriasVencerCT."</a>";

$pendAdslAveriasVencerST = $Indicadores->getPorVencerAveriasAdsl_ConSinTenico($mdf,0);
$pendAdslAveriasVencerST = "<a href='#targetListado' onclick=\"verDetalle2('porvencer','averias','adsl', '0')\">".$pendAdslAveriasVencerST."</a>";


// CATV
$pendCatvAveriasVencerCT = $Indicadores->getPorVencerAveriasCatv_ConSinTecnico($nodo,1);
$pendCatvAveriasVencerCT = "<a href='#targetListado' onclick=\"verDetalle2('porvencer','averias','catv', 1)\">".$pendCatvAveriasVencerCT."</a>";

$pendCatvAveriasVencerST = $Indicadores->getPorVencerAveriasCatv_ConSinTecnico($nodo,0);
$pendCatvAveriasVencerST = "<a href='#targetListado' onclick=\"verDetalle2('porvencer','averias','catv', 0)\">".$pendCatvAveriasVencerST."</a>";

/******/


/** VENCIDAS PROVISION**/
// BASICA
$pendTbaProvisionVencidasCT = $Indicadores->getVencidasProvisionTba_ConSinTenico($mdf, 1);
$pendTbaProvisionVencidasCT = "<a href='#targetListado'  onclick=\"verDetalle2('vencidas','provision','tba', '1')\" >".$pendTbaProvisionVencidasCT."</a>";

$pendTbaProvisionVencidasST = $Indicadores->getVencidasProvisionTba_ConSinTenico($mdf, 0);
$pendTbaProvisionVencidasST = "<a href='#targetListado'  onclick=\"verDetalle2('vencidas','provision','tba', '0')\" >".$pendTbaProvisionVencidasST."</a>";

//ADSL
$pendAdslProvisionVencidasCT = $Indicadores->getVencidasProvisionAdsl_ConSinTenico($mdf, 1);
$pendAdslProvisionVencidasCT = "<a href='#targetListado' onclick=\"verDetalle2('vencidas','provision','adsl', '1')\">".$pendAdslProvisionVencidasCT."</a>";

$pendAdslProvisionVencidasST = $Indicadores->getVencidasProvisionAdsl_ConSinTenico($mdf, 0);
$pendAdslProvisionVencidasST = "<a href='#targetListado' onclick=\"verDetalle2('vencidas','provision','adsl', '0')\">".$pendAdslProvisionVencidasST."</a>";

//CATV
$pendCatvProvisionVencidas = $Indicadores->getVencidasProvisionCatv($nodo);
$pendCatvProvisionVencidas = "<a href='#targetListado' onclick=\"verDetalle2('vencidas','provision','catv')\">".$pendCatvProvisionVencidas."</a>";

/******/

/** VENCIDAS AVERIAS**/
// BASICA
$vencidasTbaAveriasCT = $Indicadores->getVencidasAveriasTba_ConSinTenico($mdf, 1);
$vencidasTbaAveriasCT = "<a href='#targetListado'  onclick=\"verDetalle2('vencidas','averias','tba', '1')\" >".$vencidasTbaAveriasCT."</a>";

$vencidasTbaAveriasST = $Indicadores->getVencidasAveriasTba_ConSinTenico($mdf, 0);
$vencidasTbaAveriasST = "<a href='#targetListado'  onclick=\"verDetalle2('vencidas','averias','tba', '0')\" >".$vencidasTbaAveriasST."</a>";


// ADSL
$vencidasAdslAveriasCT = $Indicadores->getVencidasAveriasAdsl_ConSinTenico($mdf, 1);
$vencidasAdslAveriasCT = "<a href='#targetListado' onclick=\"verDetalle2('vencidas','averias','adsl', '1')\">".$vencidasAdslAveriasCT."</a>";

$vencidasAdslAveriasST = $Indicadores->getVencidasAveriasAdsl_ConSinTenico($mdf, 0);
$vencidasAdslAveriasST = "<a href='#targetListado' onclick=\"verDetalle2('vencidas','averias','adsl', '0')\">".$vencidasAdslAveriasST."</a>";


// CATV
$vencidasCatvAveriasCT = $Indicadores->getVencidasAveriasCatv_ConSinTecnico($nodo,1);
$vencidasCatvAveriasCT = "<a href='#targetListado' onclick=\"verDetalle2('vencidas','averias','catv', '1')\">".$vencidasCatvAveriasCT."</a>";

$vencidasCatvAveriasST = $Indicadores->getVencidasAveriasCatv_ConSinTecnico($nodo,0);
$vencidasCatvAveriasST = "<a href='#targetListado' onclick=\"verDetalle2('vencidas','averias','catv', '0')\">".$vencidasCatvAveriasST."</a>";

/******/


/** LIQUIDADAS DEL DIA **/
$hoySoloFecha = date("Y-m-d");
// PROVISION
// BASICA
$liqDiaTbaProvisionCT = $Indicadores->getLiqDiaProvisionTba_ConSinTenico($mdf, 1, $hoySoloFecha);
$liqDiaTbaProvisionCT = "<a href='#targetListado' onclick=\"verDetalle2('liquidadas','provision','tba', '1')\">".$liqDiaTbaProvisionCT."</a>";

$liqDiaTbaProvisionST = $Indicadores->getLiqDiaProvisionTba_ConSinTenico($mdf, 0, $hoySoloFecha);
$liqDiaTbaProvisionST = "<a href='#targetListado' onclick=\"verDetalle2('liquidadas','provision','tba', '0')\">".$liqDiaTbaProvisionST."</a>";

// ADSL
$liqDiaAdslProvisionST = $Indicadores->getLiqDiaProvisionAdsl2_ConSinTenico($mdf, 0, $hoySoloFecha);
$liqDiaAdslProvisionST = "<a href='#targetListado' onclick=\"verDetalle2('liquidadas','provision','adsl', '0')\">".$liqDiaAdslProvisionST."</a>";

$liqDiaAdslProvisionCT = $Indicadores->getLiqDiaProvisionAdsl2_ConSinTenico($mdf, 1, $hoySoloFecha);
$liqDiaAdslProvisionCT = "<a href='#targetListado' onclick=\"verDetalle2('liquidadas','provision','adsl', '1')\">".$liqDiaAdslProvisionCT."</a>";


// CATV
$liqDiaCatvProvision = $Indicadores->getLiqDiaProvisionCatv($nodo, $hoySoloFecha);
$liqDiaCatvProvision = "<a href='#targetListado' onclick=\"verDetalle('liquidadas','provision','catv')\">".$liqDiaCatvProvision."</a>";


// AVERIAS

//BASICA
$liqDiaTbaAveriasCT = $Indicadores->getLiqDiaAveriasTba_ConSinTenico($mdf, 1, $hoySoloFecha);
$liqDiaTbaAveriasCT = "<a href='#targetListado' onclick=\"verDetalle2('liquidadas','averias','tba', '1')\">".$liqDiaTbaAveriasCT."</a>";

$liqDiaTbaAveriasST = $Indicadores->getLiqDiaAveriasTba_ConSinTenico($mdf, 0, $hoySoloFecha);
$liqDiaTbaAveriasST = "<a href='#targetListado' onclick=\"verDetalle2('liquidadas','averias','tba', '0')\">".$liqDiaTbaAveriasST."</a>";


//ADSL
$liqDiaAdslAveriasCT = $Indicadores->getLiqDiaAveriasAdsl_ConSinTecnico($mdf, 1, $hoySoloFecha);
$liqDiaAdslAveriasCT = "<a href='#targetListado' onclick=\"verDetalle2('liquidadas','averias','adsl', '1')\">".$liqDiaAdslAveriasCT."</a>";

$liqDiaAdslAveriasST = $Indicadores->getLiqDiaAveriasAdsl_ConSinTecnico($mdf, 0, $hoySoloFecha);
$liqDiaAdslAveriasST = "<a href='#targetListado' onclick=\"verDetalle2('liquidadas','averias','adsl', '0')\">".$liqDiaAdslAveriasST."</a>";


//CATV
$liqDiaCatvAverias = $Indicadores->getLiqDiaAveriasCatv($nodo, $hoySoloFecha);
$liqDiaCatvAverias = "<a href='#targetListado' onclick=\"verDetalle('liquidadas','averias','catv')\">".$liqDiaCatvAverias."</a>";
/******/


// AGENDAS AVERIAS
$arrAgendasDiaTbaAverias = $Indicadores->getAgendasDiaAveriasTba($mdf, $hoySoloFecha);
$arrAgendasDiaAdslAverias = $Indicadores->getAgendasDiaAveriasAdsl($mdf, $hoySoloFecha);
$arrAgendasDiaCatvAverias = $Indicadores->getAgendasDiaAveriasCatv($nodo, $hoySoloFecha);

/*****************/

// AGENDAS PROVISION
$arrAgendasDiaAdslProvision = $Indicadores->getAgendasDiaProvisionAdsl($mdf, $hoySoloFecha);
$arrAgendasDiaTbaProvision = $Indicadores->getAgendasDiaProvisionTba($mdf, $hoySoloFecha);
$arrAgendasDiaCatvProvision = $Indicadores->getAgendasDiaProvisionCatv($nodo, $hoySoloFecha);

/*****************/

echo "<br/>";

?>

<div id="submain1" class="divResMain3">
	<div id="res1" class="divResInternoRight" >
		<table class="table">
		<thead>
			<th colspan='5'>Pendientes</th>
		</thead>		
		<tr>
			<th class="celdaTitulo">Tipo Actuacion</th>
			<th class="celdaTitulo" colspan="2" align='center'>Provision</th>
			<th class="celdaTitulo" colspan="2" align='center'>Averias</th>
		</tr>
		<tr>
			<th class="celdaTitulo">&nbsp;</th>
			<th class="celdaTitulo">Con Tecnico</th>
			<th class="celdaTitulo">Sin Tecnico</th>
			<th class="celdaTitulo">Con Tecnico</th>	
			<th class="celdaTitulo">Sin Tecnico</th>			
		</tr>		
		<tr>
			<td class="celdaRes">TBA:</td><td class="celdaRes"><?=$pendTbaProvisionCT?></td><td class="celdaRes"><?=$pendTbaProvisionST?></td>
			<td class="celdaRes"><?php if(isset($pendTbaAveriasCT)){echo $pendTbaAveriasCT;}?></td>
			<td class="celdaRes"><?php if(isset($pendTbaAveriasST)){echo $pendTbaAveriasST;}?></td>
		</tr>
		<tr>
			<td class="celdaRes">ADSL:</td><td class="celdaRes"><?=$pendADSLProvisionCT?></td><td class="celdaRes"><?=$pendADSLProvisionST?></td>
			<td class="celdaRes"><?=$pendADSLAveriasCT?></td>
			<td class="celdaRes"><?=$pendADSLAveriasST?></td>
		</tr>
		<tr>
			<td class="celdaRes">CATV:</td><td class="celdaRes" colspan="2"><?=$pendCATVProvision?></td>
			<td class="celdaRes"><?php if(isset($pendCATVAveriasCT)){echo $pendCATVAveriasCT;}?></td><td class="celdaRes" colspan="2"><?php if(isset($pendCATVAveriasST)){echo $pendCATVAveriasST;}?></td>
		</tr>

		</table>
	</div>

	<div id="res3" class="divResInternoRight">
		<table class="table">
		<thead>
			<th colspan='5' align='center'>Liquidadas del dia:</th>
		</thead>
		<tr>
			<th class="celdaTitulo">Tipo Actuacion</th>
			<th class="celdaTitulo" colspan="2" align='center'>Provision</th>
			<th class="celdaTitulo" colspan="2" align='center'>Averias</th>
		</tr>
		<tr>
			<th class="celdaTitulo">&nbsp;</th>
			<th class="celdaTitulo">Con Tecnico</th>
			<th class="celdaTitulo">Sin Tecnico</th>
			<th class="celdaTitulo">Con Tecnico</th>	
			<th class="celdaTitulo">Sin Tecnico</th>			
		</tr>			
		<tr>
			<td class="celdaRes">TBA:</td>
			<td class="celdaRes"><?=$liqDiaTbaProvisionCT?></td>
			<td class="celdaRes"><?=$liqDiaTbaProvisionST?></td>
			<td class="celdaRes"><?=$liqDiaTbaAveriasCT?></td><td class="celdaRes"><?=$liqDiaTbaAveriasST?></td>
		</tr>
		<tr>
			<td class="celdaRes">ADSL:</td>
			<td class="celdaRes"><?=$liqDiaAdslProvisionCT?></td>
			<td class="celdaRes"><?=$liqDiaAdslProvisionST?></td>
			<td class="celdaRes"><?=$liqDiaAdslAveriasCT?></td><td class="celdaRes"><?=$liqDiaAdslAveriasST?></td>
		</tr>
		<tr>
			<td class="celdaRes">CATV:</td><td class="celdaRes" colspan="2"><?=$liqDiaCatvProvision?></td>
			<td class="celdaRes" colspan="2"><?=$liqDiaCatvAverias?></td>
		</tr>

		</table>
	</div>
</div>
<br/>

<div id="submain1" class="divResMain3" >	
	<div id="res22" class="divResInternoRight" >
		<table class="table">
		<thead>
			<th colspan='5'>Por Vencer</th>
		</thead>		
		<tr>
			<th class="celdaTitulo">Tipo Actuacion</th>
			<th class="celdaTitulo" colspan="2" align='center'>Provision < 7 días </th>
			<th class="celdaTitulo" colspan="2" align='center'>Averias < 24 horas</th>
		</tr>
		<tr>
			<th class="celdaTitulo">&nbsp;</th>
			<th class="celdaTitulo">Con Tecnico</th>
			<th class="celdaTitulo">Sin Tecnico</th>
			<th class="celdaTitulo">Con Tecnico</th>	
			<th class="celdaTitulo">Sin Tecnico</th>			
		</tr>			
		<tr>
			<td class="celdaRes" >TBA:</td>
			<td class="celdaRes"><?=$pendTbaProvisionVencerCT?></td><td class="celdaRes"><?=$pendTbaProvisionVencerST?></td>
			<td class="celdaRes"><?=$pendTbaAveriasVencerCT?></td>
			<td class="celdaRes"><?=$pendTbaAveriasVencerST?></td>
		</tr>
		<tr>
			<td class="celdaRes">ADSL:</td>
			<td class="celdaRes"><?=$pendAdslProvisionVencerCT?></td><td class="celdaRes"><?=$pendAdslProvisionVencerST?></td>
			<td class="celdaRes"><?=$pendAdslAveriasVencerCT?></td><td class="celdaRes"><?=$pendAdslAveriasVencerST?></td>
		</tr>
		<tr>
			<td class="celdaRes">CATV:</td>
			<td class="celdaRes" colspan="2"><?=$pendCatvProvisionVencer?></td>
			<td class="celdaRes" ><?=$pendCatvAveriasVencerCT?></td><td class="celdaRes" colspan="2"><?=$pendCatvAveriasVencerST?></td>
		</tr>

		</table>
	</div>
	<div id="res23" class="divResInternoRight">
		<table class="table">
		<thead>
			<th colspan='5'>Vencidas</th>
		</thead>		
		<tr>
			<th class="celdaTitulo">Tipo Actuacion</th>
			<th class="celdaTitulo" colspan="2" align='center'>Provision > 7 días </th>
			<th class="celdaTitulo" colspan="2" align='center'>Averias > 24 horas</th>
		</tr>
		<tr>
			<th class="celdaTitulo">&nbsp;</th>
			<th class="celdaTitulo">Con Tecnico</th>
			<th class="celdaTitulo">Sin Tecnico</th>
			<th class="celdaTitulo">Con Tecnico</th>	
			<th class="celdaTitulo">Sin Tecnico</th>			
		</tr>			
		<tr>
			<td class="celdaRes">TBA:</td>
			<td class="celdaRes"><?=$pendTbaProvisionVencidasCT?></td><td class="celdaRes"><?=$pendTbaProvisionVencidasST?></td>
			<td class="celdaRes"><?=$vencidasTbaAveriasCT?></td><td class="celdaRes"><?=$vencidasTbaAveriasST?></td>
		</tr>
		<tr>
			<td class="celdaRes">ADSL:</td>
			<td class="celdaRes"><?=$pendAdslProvisionVencidasCT?></td><td class="celdaRes"><?=$pendAdslProvisionVencidasST?></td>
			<td class="celdaRes"><?=$vencidasAdslAveriasCT?></td><td class="celdaRes"><?=$vencidasAdslAveriasST?></td>
		</tr>
		<tr>
			<td class="celdaRes">CATV:</td>
			<td class="celdaRes" colspan="2"><?=$pendCatvProvisionVencidas?></td>
			<td class="celdaRes"><?=$vencidasCatvAveriasCT?></td><td class="celdaRes" colspan="2"><?=$vencidasCatvAveriasST?></td>
		</tr>

		</table>
	</div>	

</div>


<div id="submain1" class="divResMain3">
	<div id="res2" class="divResInternoRight">
		<table class="table">
		<thead>
			<th colspan='5'>Agendas</th>
		</thead>		
		<tr>
			<th class="celdaTitulo">Tipo Actuacion</th>
			<th class="celdaTitulo" colspan="2" align='center'>Provision</th>
			<th class="celdaTitulo" colspan="2" align='center'>Averias</th>
		</tr>
		<tr>
			<th class="celdaTitulo">&nbsp;</th>
			<th class="celdaTitulo">Con Tecnico</th>
			<th class="celdaTitulo">Sin Tecnico</th>
			<th class="celdaTitulo">Con Tecnico</th>	
			<th class="celdaTitulo">Sin Tecnico</th>			
		</tr>			
		<tr>
			<td class="celdaRes">TBA:</td>
			<td class="celdaRes">
				<?php  // Provision Agendas
				$x = "";
				if (count($arrAgendasDiaTbaProvision)>0 ) {				
					foreach ($arrAgendasDiaTbaProvision as $fila) {
						$x .= "T".$fila[0].":".$fila[1];
						$x .= ", ";
					}
					echo "<a href='#' onclick=\"verDetalle('agendas','provision','tba')\">".$x."</a>";
				}
				else
					echo "0";
				?>
			</td>			
			<td class="celdaRes">
				<?php  // Averias Agendas
				$x = "";
				if (count($arrAgendasDiaTbaAverias)>0 ) {
					foreach ($arrAgendasDiaTbaAverias as $fila) {
						$x .= "T".$fila[0].":".$fila[1];
						$x .= ", ";
					}
					echo "<a href='#' onclick=\"verDetalle('agendas','averias','tba')\">".$x."</a>";
				}
				else
					echo "0";
				?>
			</td>
		</tr>
		<tr>
			<td class="celdaRes">ADSL:</td>
			<td class="celdaRes">
				<?php  // Provision Agendas
				$x = "";
				if (count($arrAgendasDiaAdslProvision)>0 ) {
					foreach ($arrAgendasDiaAdslProvision as $fila) {
						$x .= "T".$fila[0].":".$fila[1];
						$x .= ", ";
					}
					echo "<a href='#' onclick=\"verDetalle('agendas','provision','adsl')\">".$x."</a>";
				}
				else
					echo "0";				
				?>
			</td>		
			<td class="celdaRes">
				<?php  // Averias Agendas
				$x = "";
				if (count($arrAgendasDiaAdslAverias)>0 ) {
					foreach ($arrAgendasDiaAdslAverias as $fila) {
						$x .= "T".$fila[0].":".$fila[1];
						$x .= ", ";
					}
					echo "<a href='#' onclick=\"verDetalle('agendas','averias','adsl')\">".$x."</a>";
				}
				else
					echo "0";				
				?>
			</td>
		</tr>
		<tr>
			<td class="celdaRes">CATV:</td>
			<td class="celdaRes" colspan="2">
				<?php  // Provision Agendas
				$x = "";
				if (count($arrAgendasDiaCatvProvision)>0 ) {				
					foreach ($arrAgendasDiaCatvProvision as $fila) {
						$x .= "T".$fila[0].":".$fila[1];
						$x .= ", ";
					}
					echo "<a href='#' onclick=\"verDetalle('agendas','provision','catv')\">".$x."</a>";
				}
				else
					echo "0";
				?>
			</td>			
			<td class="celdaRes" colspan="2">
				<?php  // Averias Agendas
				$x = "";
				if (count($arrAgendasDiaCatvAverias)>0 ) {				
					foreach ($arrAgendasDiaCatvAverias as $fila) {
						$x .= "T".$fila[0].":".$fila[1];
						$x .= ", ";
					}
					echo "<a href='#' onclick=\"verDetalle('agendas','averias','catv')\">".$x."</a>";
				}
				else
					echo "0";
				?>
			</td>
		</tr>

		</table>
	</div>	
</div>



<div id="divDetalle1" class="divResMain3"></div>


