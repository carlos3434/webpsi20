<?php
header('Content-Type: text/html; charset=UTF-8');
header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache");


include ("../../clases/class.Indicadores.php");

$hoy = date("Y-m-d H:i:s");
$hoyFormato = date("d-m-Y H:i:s");

$Indicadores = new Indicadores();

$mdf = $_POST["mdf"];
$nodo = $_POST["nodo"];

echo "<b>".$mdf." / ".$nodo."</b><br/>";

echo $Indicadores->getFechaUpdateBasica();

/** PROVISION **/
$arrPendTbaProvision= $Indicadores->getPendProvisionTba($mdf);
foreach ($arrPendTbaProvision as $key ) {
	$pendTbaProvision = $key[1];
}
$pendTbaProvision = "<a href='#targetListado' onclick=\"verDetalle('pendiente','provision','tba')\" >".$pendTbaProvision."</a>";

$arrPendAdslProvision = $Indicadores->getPendProvisionAdsl2($mdf);
foreach ($arrPendAdslProvision as $key ) {
	$pendADSLProvision = $key[1];
}
$pendADSLProvision = "<a href='#targetListado' onclick=\"verDetalle('pendiente','provision','adsl')\">".$pendADSLProvision."</a>";

$arrPendCatvProvision = $Indicadores->getPendProvisionCatv($nodo);
foreach ($arrPendCatvProvision as $key ) {
	$pendCATVProvision = $key[1];
}
$pendCATVProvision = "<a href='#targetListado' onclick=\"verDetalle('pendiente','provision','catv')\">".$pendCATVProvision."</a>";


/** AVERIAS **/
$arrPendTbaAverias= $Indicadores->getPendAveriasTba($mdf);
foreach ($arrPendTbaAverias as $key ) {
	$pendTbaAverias = $key[1];
}
$pendTbaAverias = "<a href='#targetListado' onclick=\"verDetalle('pendiente','averias','tba')\">".$pendTbaAverias."</a>";

$arrPendAdslAverias= $Indicadores->getPendAveriasAdsl($mdf);
foreach ($arrPendAdslAverias as $key ) {
	$pendADSLAverias = $key[1];
}
$pendADSLAverias = "<a href='#targetListado' onclick=\"verDetalle('pendiente','averias','adsl')\">".$pendADSLAverias."</a>";

$arrPendCatvAverias= $Indicadores->getPendAveriasCatv($nodo);
foreach ($arrPendCatvAverias as $key ) {
	$pendCATVAverias = $key[1];
}
$pendCATVAverias = "<a href='#targetListado' onclick=\"verDetalle('pendiente','averias','catv')\">".$pendCATVAverias."</a>";



/** POR VENCER PROVISION **/
$pendTbaProvisionVencer = $Indicadores->getPorVencerProvisionTba($mdf);
$pendTbaProvisionVencer = "<a href='#targetListado' onclick=\"verDetalle('porvencer','provision','tba')\"  >".$pendTbaProvisionVencer."</a>";

$pendAdslProvisionVencer = $Indicadores->getPorVencerProvisionAdsl($mdf);
$pendAdslProvisionVencer = "<a href='#targetListado' onclick=\"verDetalle('porvencer','provision','adsl')\">".$pendAdslProvisionVencer."</a>";

$pendCatvProvisionVencer = $Indicadores->getPorVencerProvisionCatv($nodo);
$pendCatvProvisionVencer = "<a href='#targetListado' onclick=\"verDetalle('porvencer','provision','catv')\">".$pendCatvProvisionVencer."</a>";
/******/


/** POR VENCER AVERIAS **/
$pendTbaAveriasVencer = $Indicadores->getPorVencerAveriasTba($mdf);
$pendTbaAveriasVencer = "<a href='#targetListado' onclick=\"verDetalle('porvencer','averias','tba')\"  >".$pendTbaAveriasVencer."</a>";

$pendAdslAveriasVencer = $Indicadores->getPorVencerAveriasAdsl($mdf);
$pendAdslAveriasVencer = "<a href='#targetListado' onclick=\"verDetalle('porvencer','averias','adsl')\">".$pendAdslAveriasVencer."</a>";

$pendCatvAveriasVencer = $Indicadores->getPorVencerAveriasCatv($nodo);
$pendCatvAveriasVencer = "<a href='#targetListado' onclick=\"verDetalle('porvencer','averias','catv')\">".$pendCatvAveriasVencer."</a>";
/******/


/** VENCIDAS PROVISION**/
$pendTbaProvisionVencidas = $Indicadores->getVencidasProvisionTba($mdf);
$pendTbaProvisionVencidas = "<a href='#targetListado'  onclick=\"verDetalle('vencidas','provision','tba')\" >".$pendTbaProvisionVencidas."</a>";

$pendAdslProvisionVencidas = $Indicadores->getVencidasProvisionAdsl($mdf);
$pendAdslProvisionVencidas = "<a href='#targetListado' onclick=\"verDetalle('vencidas','provision','adsl')\">".$pendAdslProvisionVencidas."</a>";

$pendCatvProvisionVencidas = $Indicadores->getVencidasProvisionCatv($nodo);
$pendCatvProvisionVencidas = "<a href='#targetListado' onclick=\"verDetalle('vencidas','provision','catv')\">".$pendCatvProvisionVencidas."</a>";

/******/

/** VENCIDAS AVERIAS**/
$vencidasTbaAverias = $Indicadores->getVencidasAveriasTba($mdf);
$vencidasTbaAverias = "<a href='#targetListado'  onclick=\"verDetalle('vencidas','averias','tba')\" >".$vencidasTbaAverias."</a>";

$vencidasAdslAverias = $Indicadores->getVencidasAveriasAdsl($mdf);
$vencidasAdslAverias = "<a href='#targetListado' onclick=\"verDetalle('vencidas','averias','adsl')\">".$vencidasAdslAverias."</a>";

$vencidasCatvAverias = $Indicadores->getVencidasAveriasCatv($nodo);
$vencidasCatvAverias = "<a href='#targetListado' onclick=\"verDetalle('vencidas','averias','catv')\">".$vencidasCatvAverias."</a>";

/******/


/** LIQUIDADAS DEL DIA **/
$hoySoloFecha = date("Y-m-d");
// PROVISION
$liqDiaTbaProvision = $Indicadores->getLiqDiaProvisionTba($mdf, $hoySoloFecha);
$liqDiaTbaProvision = "<a href='#targetListado' onclick=\"verDetalle('liquidadas','provision','tba')\">".$liqDiaTbaProvision."</a>";

$liqDiaAdslProvision = $Indicadores->getLiqDiaProvisionAdsl2($mdf, $hoySoloFecha);
$liqDiaAdslProvision = "<a href='#targetListado' onclick=\"verDetalle('liquidadas','provision','adsl')\">".$liqDiaAdslProvision."</a>";

$liqDiaCatvProvision = $Indicadores->getLiqDiaProvisionCatv($nodo, $hoySoloFecha);
$liqDiaCatvProvision = "<a href='#targetListado' onclick=\"verDetalle('liquidadas','provision','catv')\">".$liqDiaCatvProvision."</a>";

// AVERIAS
$liqDiaTbaAverias = $Indicadores->getLiqDiaAveriasTba($mdf, $hoySoloFecha);
$liqDiaTbaAverias = "<a href='#targetListado' onclick=\"verDetalle('liquidadas','averias','tba')\">".$liqDiaTbaAverias."</a>";

$liqDiaAdslAverias = $Indicadores->getLiqDiaAveriasAdsl($mdf, $hoySoloFecha);
$liqDiaAdslAverias = "<a href='#targetListado' onclick=\"verDetalle('liquidadas','averias','adsl')\">".$liqDiaAdslAverias."</a>";

$liqDiaCatvAverias = $Indicadores->getLiqDiaAveriasCatv($nodo, $hoySoloFecha);
$liqDiaCatvAverias = "<a href='#targetListado' onclick=\"verDetalle('liquidadas','averias','catv')\">".$liqDiaCatvAverias."</a>";
/******/


// AGENDAS AVERIAS
$arrAgendasDiaTbaAverias = $Indicadores->getAgendasDiaAveriasTba($mdf, $hoySoloFecha);
$arrAgendasDiaAdslAverias = $Indicadores->getAgendasDiaAveriasAdsl($mdf, $hoySoloFecha);
$arrAgendasDiaCatvAverias = $Indicadores->getAgendasDiaAveriasCatv($nodo, $hoySoloFecha);

/*****************/

// AGENDAS PROVISION
$arrAgendasDiaAdslProvision = $Indicadores->getAgendasDiaProvisionAdsl($mdf, $hoySoloFecha);
$arrAgendasDiaTbaProvision = $Indicadores->getAgendasDiaProvisionTba($mdf, $hoySoloFecha);
$arrAgendasDiaCatvProvision = $Indicadores->getAgendasDiaProvisionCatv($nodo, $hoySoloFecha);

/*****************/

echo "<br/>";

?>

<div id="submain1" class="divResMain1">
	<div id="res1" class="divResInternoRight">
		<table class="table">
		<thead>
			<th colspan='3'>Pendientes</th>
		</thead>		
		<tr>
			<th class="celdaTitulo">Tipo Actuacion</th><th class="celdaTitulo">Provision</th><th class="celdaTitulo">Averias</th>
			<?php /*<td>TBA (<?=$pendTbaProvision?>:<?=$pendTbaAverias?>)</td>
			<td>ADSL (<?=$pendADSLProvision?>:<?=$pendADSLAverias?>)</td>
			<td>CATV (<?=$pendCATVProvision?>:<?=$pendCATVAverias?>)</td>
			*/ ?>
		</tr>
		<tr>
			<td class="celdaRes">TBA:</td><td class="celdaRes"><?=$pendTbaProvision?></td><td class="celdaRes"><?=$pendTbaAverias?></td>
		</tr>
		<tr>
			<td class="celdaRes">ADSL:</td><td class="celdaRes"><?=$pendADSLProvision?></td><td class="celdaRes"><?=$pendADSLAverias?></td>
		</tr>
		<tr>
			<td class="celdaRes">CATV:</td><td class="celdaRes"><?=$pendCATVProvision?></td><td class="celdaRes"><?=$pendCATVAverias?></td>
		</tr>

		</table>
	</div>

	<div id="res3" class="divResInternoRight">
		<table class="table">
		<thead>
			<th colspan='3'>Liquidadas del dia:</th>
		</thead>
		<tr>
			<th class="celdaTitulo">Tipo Actuacion</th><th class="celdaTitulo">Provision</th><th class="celdaTitulo">Averias</th>
			<?php /*<td>TBA (<?=$pendTbaProvision?>:<?=$pendTbaAverias?>)</td>
			<td>ADSL (<?=$pendADSLProvision?>:<?=$pendADSLAverias?>)</td>
			<td>CATV (<?=$pendCATVProvision?>:<?=$pendCATVAverias?>)</td>
			*/ ?>
		</tr>
		<tr>
			<td class="celdaRes">TBA:</td><td class="celdaRes"><?=$liqDiaTbaProvision?></td><td class="celdaRes"><?=$liqDiaTbaAverias?></td>
		</tr>
		<tr>
			<td class="celdaRes">ADSL:</td><td class="celdaRes"><?=$liqDiaAdslProvision?></td><td class="celdaRes"><?=$liqDiaAdslAverias?></td>
		</tr>
		<tr>
			<td class="celdaRes">CATV:</td><td class="celdaRes"><?=$liqDiaCatvProvision?></td><td class="celdaRes"><?=$liqDiaCatvAverias?></td>
		</tr>

		</table>
	</div>
</div>
<br/>

<div id="submain1" class="divResMain1" >	
	<div id="res22" class="divResInternoRight" >
		<table class="table">
		<thead>
			<th colspan='3'>Por Vencer</th>
		</thead>		
		<tr>
			<th class="celdaTitulo">Tipo Actuacion</th><th class="celdaTitulo">Provision < 7 días </th><th class="celdaTitulo">Averias < 24 horas</th>
			<?php /*<td>TBA (<?=$pendTbaProvision?>:<?=$pendTbaAverias?>)</td>
			<td>ADSL (<?=$pendADSLProvision?>:<?=$pendADSLAverias?>)</td>
			<td>CATV (<?=$pendCATVProvision?>:<?=$pendCATVAverias?>)</td>
			*/ ?>
		</tr>
		<tr>
			<td class="celdaRes" >TBA:</td>
			<td class="celdaRes"><?=$pendTbaProvisionVencer?></td>
			<td class="celdaRes"><?=$pendTbaAveriasVencer?></td>
		</tr>
		<tr>
			<td class="celdaRes">ADSL:</td>
			<td class="celdaRes"><?=$pendAdslProvisionVencer?></td>
			<td class="celdaRes"><?=$pendAdslAveriasVencer?></td>
		</tr>
		<tr>
			<td class="celdaRes">CATV:</td>
			<td class="celdaRes"><?=$pendCatvProvisionVencer?></td>
			<td class="celdaRes"><?=$pendCatvAveriasVencer?></td>
		</tr>

		</table>
	</div>
	<div id="res23" class="divResInternoRight">
		<table class="table">
		<thead>
			<th colspan='3'>Vencidas</th>
		</thead>		
		<tr>
			<th class="celdaTitulo">Tipo Actuacion</th><th class="celdaTitulo">Provision > 7 días </th><th class="celdaTitulo">Averias > 24 horas</th>
			<?php /*<td>TBA (<?=$pendTbaProvision?>:<?=$pendTbaAverias?>)</td>
			<td>ADSL (<?=$pendADSLProvision?>:<?=$pendADSLAverias?>)</td>
			<td>CATV (<?=$pendCATVProvision?>:<?=$pendCATVAverias?>)</td>
			*/ ?>
		</tr>
		<tr>
			<td class="celdaRes">TBA:</td>
			<td class="celdaRes"><?=$pendTbaProvisionVencidas?></td>
			<td class="celdaRes"><?=$vencidasTbaAverias?></td>
		</tr>
		<tr>
			<td class="celdaRes">ADSL:</td>
			<td class="celdaRes"><?=$pendAdslProvisionVencidas?></td>
			<td class="celdaRes"><?=$vencidasAdslAverias?></td>
		</tr>
		<tr>
			<td class="celdaRes">CATV:</td>
			<td class="celdaRes"><?=$pendCatvProvisionVencidas?></td>
			<td class="celdaRes"><?=$vencidasCatvAverias?></td>
		</tr>

		</table>
	</div>	

</div>


<div id="submain1" class="divResMain1">
	<div id="res2" class="divResInternoRight">
		<table class="table">
		<thead>
			<th colspan='3'>Agendas</th>
		</thead>		
		<tr>
			<th class="celdaTitulo">Tipo Actuacion</th><th class="celdaTitulo">Provision</th><th class="celdaTitulo">Averias</th>
			<?php /*<td>TBA (<?=$pendTbaProvision?>:<?=$pendTbaAverias?>)</td>
			<td>ADSL (<?=$pendADSLProvision?>:<?=$pendADSLAverias?>)</td>
			<td>CATV (<?=$pendCATVProvision?>:<?=$pendCATVAverias?>)</td>
			*/ ?>
		</tr>
		<tr>
			<td class="celdaRes">TBA:</td>
			<td class="celdaRes">
				<?php  // Provision Agendas
				$x = "";
				if (count($arrAgendasDiaTbaProvision)>0 ) {				
					foreach ($arrAgendasDiaTbaProvision as $fila) {
						$x .= "T".$fila[0].":".$fila[1];
						$x .= ", ";
					}
					echo "<a href='#' onclick=\"verDetalle('agendas','provision','tba')\">".$x."</a>";
				}
				else
					echo "0";
				?>
			</td>			
			<td class="celdaRes">
				<?php  // Averias Agendas
				$x = "";
				if (count($arrAgendasDiaTbaAverias)>0 ) {
					foreach ($arrAgendasDiaTbaAverias as $fila) {
						$x .= "T".$fila[0].":".$fila[1];
						$x .= ", ";
					}
					echo "<a href='#' onclick=\"verDetalle('agendas','averias','tba')\">".$x."</a>";
				}
				else
					echo "0";
				?>
			</td>
		</tr>
		<tr>
			<td class="celdaRes">ADSL:</td>
			<td class="celdaRes">
				<?php  // Provision Agendas
				$x = "";
				if (count($arrAgendasDiaAdslProvision)>0 ) {
					foreach ($arrAgendasDiaAdslProvision as $fila) {
						$x .= "T".$fila[0].":".$fila[1];
						$x .= ", ";
					}
					echo "<a href='#' onclick=\"verDetalle('agendas','provision','adsl')\">".$x."</a>";
				}
				else
					echo "0";				
				?>
			</td>		
			<td class="celdaRes">
				<?php  // Averias Agendas
				$x = "";
				if (count($arrAgendasDiaAdslAverias)>0 ) {
					foreach ($arrAgendasDiaAdslAverias as $fila) {
						$x .= "T".$fila[0].":".$fila[1];
						$x .= ", ";
					}
					echo "<a href='#' onclick=\"verDetalle('agendas','averias','adsl')\">".$x."</a>";
				}
				else
					echo "0";				
				?>
			</td>
		</tr>
		<tr>
			<td class="celdaRes">CATV:</td>
			<td class="celdaRes">
				<?php  // Provision Agendas
				$x = "";
				if (count($arrAgendasDiaCatvProvision)>0 ) {				
					foreach ($arrAgendasDiaCatvProvision as $fila) {
						$x .= "T".$fila[0].":".$fila[1];
						$x .= ", ";
					}
					echo "<a href='#' onclick=\"verDetalle('agendas','provision','catv')\">".$x."</a>";
				}
				else
					echo "0";
				?>
			</td>			
			<td class="celdaRes">
				<?php  // Averias Agendas
				$x = "";
				if (count($arrAgendasDiaCatvAverias)>0 ) {				
					foreach ($arrAgendasDiaCatvAverias as $fila) {
						$x .= "T".$fila[0].":".$fila[1];
						$x .= ", ";
					}
					echo "<a href='#' onclick=\"verDetalle('agendas','averias','catv')\">".$x."</a>";
				}
				else
					echo "0";
				?>
			</td>
		</tr>

		</table>
	</div>	
</div>



<div id="divDetalle1" class="divResMain1"></div>


