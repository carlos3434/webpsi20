<?php
header('Content-Type: text/html; charset=UTF-8');
header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache");


include ("../../clases/class.IndicadoresFechas.php");

$hoy = date("Y-m-d H:i:s");
$hoyFormato = date("d-m-Y H:i:s");

$Indicadores = new IndicadoresFechas();

$mdf = $_POST["mdf"];
$nodo = $_POST["nodo"];

echo "<b>".$mdf." / ".$nodo."</b><br/>";

echo $Indicadores->getFechaUpdateBasica();

/*------------------------------------------------------------Pendientes-----------------------------------------------------------------**/
/** PROVISION **/
//$PendAdslPro= $Indicadores->getFechaUpdateBasica($mdf);

$pendTbaProv = $Indicadores->getFechaUpdateProvTba($mdf);


$PendAdslProv = $Indicadores->getFechaUpdateProvAdsl($mdf);
//$pendADSLProvision = $pendADSLProvision;

$PendCatvProv = $Indicadores->getFechaUpdateProvCatv($nodo);
//$pendCATVProvision = $pendCATVProvision;


/** AVERIAS **/

$PendTbaAve= $Indicadores->getFechaUpdateAveTba($mdf);

//$pendTbaAverias = $pendTbaAverias;

$PendAdslAve= $Indicadores->getFechaUpdateAveAdsl($mdf);

//$pendADSLAverias = $pendADSLAverias;

$PendCatvAve= $Indicadores->getFechaUpdateAveCatv($nodo);
//$pendCATVAverias = $pendCATVAverias;


/**---------------------------------------------------------------------------POR VENCER---------------------------------------------------*/
/** POR VENCER PROVISION **/
$pendTbaProvisionVencer = $Indicadores->getFechaUpdatePorVencer_Prov_Tba($mdf);
$pendTbaProvisionVencer = $pendTbaProvisionVencer;

$pendAdslProvisionVencer = $Indicadores->getFechaUpdatePorVencer_Prov_Adsl($mdf);
$pendAdslProvisionVencer = $pendAdslProvisionVencer;

$pendCatvProvisionVencer = $Indicadores->getFechaUpdatePorVencer_Prov_Catv($nodo);
$pendCatvProvisionVencer = $pendCatvProvisionVencer;
/******/


/** POR VENCER AVERIAS **/
$pendTbaAveriasVencer = $Indicadores->getFechaUpdatePorVencer_Ave_Tba($mdf);
$pendTbaAveriasVencer = $pendTbaAveriasVencer;

$pendAdslAveriasVencer = $Indicadores->getFechaUpdatePorVencer_Ave_Adsl($mdf);
$pendAdslAveriasVencer = $pendAdslAveriasVencer;

$pendCatvAveriasVencer = $Indicadores->getFechaUpdatePorVencer_Ave_Catv($nodo);
$pendCatvAveriasVencer = $pendCatvAveriasVencer;
/******/

/**-------------------------------------------------------------------Vencidas ------------------------------------------------------------*/
/** VENCIDAS PROVISION**/
$pendTbaProvisionVencidas = $Indicadores->getFechaUpdateVencidas_Prov_Tba($mdf);
$pendTbaProvisionVencidas = $pendTbaProvisionVencidas;

$pendAdslProvisionVencidas = $Indicadores->getFechaUpdateVencidas_Prov_Adsl($mdf);
$pendAdslProvisionVencidas = $pendAdslProvisionVencidas;

$pendCatvProvisionVencidas = $Indicadores->getFechaUpdateVencidas_Prov_Catv($nodo);
$pendCatvProvisionVencidas = $pendCatvProvisionVencidas;

/******/

/** VENCIDAS AVERIAS**/
$vencidasTbaAverias = $Indicadores->getFechaUpdateVencidas_Ave_Tba($mdf);
$vencidasTbaAverias = $vencidasTbaAverias;

$vencidasAdslAverias = $Indicadores->getFechaUpdateVencidas_Ave_Adsl($mdf);
$vencidasAdslAverias = $vencidasAdslAverias;

$vencidasCatvAverias = $Indicadores->getFechaUpdateVencidas_Ave_Catv($nodo);
$vencidasCatvAverias = $vencidasCatvAverias;

/******/



/** --------------------------------------------------------------------------LIQUIDADAS DEL DIA --------------------------------------------**/
$hoySoloFecha = date("Y-m-d");
// PROVISION
$liqDiaTbaProvision = $Indicadores->getFechaUpdateLiqui_Prov_Tba($mdf, $hoySoloFecha);
$liqDiaTbaProvision = $liqDiaTbaProvision;

$liqDiaAdslProvision = $Indicadores->getFechaUpdateLiqui_Prov_Adsl($mdf, $hoySoloFecha);
$liqDiaAdslProvision = $liqDiaAdslProvision;

$liqDiaCatvProvision = $Indicadores->getFechaUpdateLiqui_Prov_Catv($nodo, $hoySoloFecha);
$liqDiaCatvProvision = $liqDiaCatvProvision;

// AVERIAS
$liqDiaTbaAverias = $Indicadores->getFechaUpdateLiqui_Ave_Tba($mdf, $hoySoloFecha);
$liqDiaTbaAverias = $liqDiaTbaAverias;

$liqDiaAdslAverias = $Indicadores->getFechaUpdateLiqui_Ave_Adsl($mdf, $hoySoloFecha);
$liqDiaAdslAverias = $liqDiaAdslAverias;

$liqDiaCatvAverias = $Indicadores->getFechaUpdateLiqui_Ave_Catv($nodo, $hoySoloFecha);
$liqDiaCatvAverias = $liqDiaCatvAverias;
/******/

/**----------------------------------------------------------------------------Agendas-----------------------------------------------------------*/

// AGENDAS PROVISION
$AgendasDiaTbaProvision = $Indicadores->getFechaUpdateAgendas_Prov_Tba($mdf, $hoySoloFecha);//getFechaUpdateAgendas_Prov_Tba
$AgendasDiaTbaProvision = $AgendasDiaTbaProvision;
$AgendasDiaAdslProvision = $Indicadores->getFechaUpdateAgendas_Prov_Adsl($mdf, $hoySoloFecha);//getFechaUpdateAgendas_Prov_Adsl
$AgendasDiaAdslProvision = $AgendasDiaAdslProvision;
$AgendasDiaCatvProvision = $Indicadores->getFechaUpdateAgendas_Prov_Catv($nodo, $hoySoloFecha);//getFechaUpdateAgendas_Prov_Catv
$AgendasDiaCatvProvision = $AgendasDiaCatvProvision;
/*****************/

// AGENDAS AVERIAS
$AgendasDiaTbaAverias = $Indicadores->getFechaUpdateAgendas_Ave_Tba($mdf, $hoySoloFecha);//getFechaUpdateAgendas_Ave_Tba
$AgendasDiaTbaAverias = $AgendasDiaTbaAverias;
$AgendasDiaAdslAverias = $Indicadores->getFechaUpdateAgendas_Ave_Adsl($mdf, $hoySoloFecha);//getFechaUpdateAgendas_Ave_Adsl
$AgendasDiaAdslAverias = $AgendasDiaAdslAverias;
$AgendasDiaCatvAverias = $Indicadores->getFechaUpdateAgendas_Ave_Catv($nodo, $hoySoloFecha);//getFechaUpdateAgendas_Ave_Catv
$AgendasDiaCatvAverias = $AgendasDiaCatvAverias;
/*****************/

echo "<br/>";


?>


<style >

.table1{ 
	width: 95%; /*175px*/
	/*max-width: 350px;*/
	font: 70% Verdana, Arial, Helvetica, sans-serif;
	color: #000;
	text-align: left;
	border-collapse:collapse;
   
	border: 1px solid #000000;	
    /*border-spacing:  35px;*/
	/*background: #BEEEFA;*/
	background: #FFFFFF;
	/*border-radius: 5px;*/
}

/**CELDA DE TITULO DE PINTAR INDICADORES 3*/
.celdaTitulo1 { 
  font: normal 10px Arial, Helvetica, sans-serif;
  color: #FFFFFF  ;
  background: #5595B1;/*#5595B1*/
  height:25px;
  /*font-weight: bold;*/
  width: 10%;
  padding: 8px;
  border-right: 1px solid #000000;	
  border-bottom: 1px solid #000000;	  

}


/*---------------------CELDAS DE PINTAR INDICADORES 3*/
.celdaRes1 {
	font: normal 9px Arial, Helvetica, sans-serif;
	color: #000000 ;
	background: #FFFFFF ;
	height:20px;
	/*border-spacing:15px;*/
	/*font-weight: bold;*/
    width: 30px;
	border-right: 1px solid #000000;	
	border-bottom: 1px solid #000000;	
	text-align: center;
	/*margin: 5px; 
	padding: 5px;*/

}
/**----------------------------------------------------------------------------------------*/


.divResInternoDerecho {

	/*position:relative; */
	float:left;
	width:33%; 
	padding: 20px;
   }

}


</style>

<div id="submain1" class="divResMain1">


	<div id="res" class="divResInternoDerecho">
		<table class="table1" CELLSPACING="0" >
		<thead>
			<th colspan='3' cellspacing="5" >Pendientes <?php ?></th>
		</thead>		
		<tr>
			<th class="celdaTitulo1">Tipo Actuacion</th><th class="celdaTitulo1">Provision</th><th class="celdaTitulo1">Averias</th>
			<?php /*<td>TBA (<?=$pendTbaProvision?>:<?=$pendTbaAverias?>)</td>
			<td>ADSL (<?=$pendADSLProvision?>:<?=$pendADSLAverias?>)</td>
			<td>CATV (<?=$pendCATVProvision?>:<?=$pendCATVAverias?>)</td>
			*/?>
		</tr>
		<tr>
			<td class="celdaRes1">TBA:</td>
			<td class="celdaRes1" height:"28px"; width: "50%";><?php echo $pendTbaProv?></td>
  			<td class="celdaRes1"><?php echo $PendTbaAve?></td>
		</tr>
		<tr>
			<td class="celdaRes1">ADSL:</td>
			<td class="celdaRes1" ><?php echo $PendAdslProv?></td>
			<td class="celdaRes1"><?php echo $PendAdslAve?></td>
		</tr>
		<tr>
			<td class="celdaRes1">CATV:</td>
			<td class="celdaRes1" ><?php echo $PendCatvProv?></td>
			<td class="celdaRes1"><?php echo $PendCatvAve?></td>
		</tr>

		</table>
	</div>

	<div id="res0" class="divResInternoDerecho">
		<table class="table1">
		<thead>
			<th colspan='3' >Liquidadas del dia:</th>
		</thead>
		<tr>
			<th class="celdaTitulo1">Tipo Actuacion</th><th class="celdaTitulo1">Provision</th><th class="celdaTitulo1">Averias</th>
			<?php /*<td>TBA (<?=$pendTbaProvision?>:<?=$pendTbaAverias?>)</td>
			<td>ADSL (<?=$pendADSLProvision?>:<?=$pendADSLAverias?>)</td>
			<td>CATV (<?=$pendCATVProvision?>:<?=$pendCATVAverias?>)</td>
			*/ ?>
		</tr>
		<tr>
			<td class="celdaRes1" style="width:400px">TBA:</td>
			<td class="celdaRes1"><?php echo $liqDiaTbaProvision?></td>
			<td class="celdaRes1"><?php echo $liqDiaTbaAverias?></td>
		</tr>
		<tr>
			<td class="celdaRes1">ADSL:</td>
			<td class="celdaRes1"><?php echo $liqDiaAdslProvision?></td>
			<td class="celdaRes1"><?php echo $liqDiaAdslAverias?></td>
		</tr>
		<tr>
			<td class="celdaRes1">CATV:</td>
			<td class="celdaRes1"><?php echo $liqDiaCatvProvision?></td>
			<td class="celdaRes1"><?php echo $liqDiaCatvAverias?></td>
		</tr>

		</table>
	</div>
</div>
<br/>

<div id="submain1" class="divResMain1" >	
	<div id="res1" class="divResInternoDerecho" >
		<table class="table1">
		<thead>
			<th colspan='3'>Por Vencer</th>
		</thead>		
		<tr>
			<th class="celdaTitulo1">Tipo Actuacion</th><th class="celdaTitulo1">Provision < 7 días </th><th class="celdaTitulo1">Averias < 24 horas</th>
			<?php /*<td>TBA (<?=$pendTbaProvision?>:<?=$pendTbaAverias?>)</td>
			<td>ADSL (<?=$pendADSLProvision?>:<?=$pendADSLAverias?>)</td>
			<td>CATV (<?=$pendCATVProvision?>:<?=$pendCATVAverias?>)</td>
			*/ ?>
		</tr>
		<tr>
			<td class="celdaRes1" >TBA:</td>
			<td class="celdaRes1"><?php echo $pendTbaProvisionVencer?></td>
			<td class="celdaRes1"><?php echo $pendTbaAveriasVencer?></td>
		</tr>
		<tr>
			<td class="celdaRes1">ADSL:</td>
			<td class="celdaRes1"><?php echo $pendAdslProvisionVencer?></td>
			<td class="celdaRes1"><?php echo $pendAdslAveriasVencer?></td>
		</tr>
		<tr>
			<td class="celdaRes1">CATV:</td>
			<td class="celdaRes1"><?php echo $pendCatvProvisionVencer?></td>
			<td class="celdaRes1"><?php echo $pendCatvAveriasVencer?></td>
		</tr>

		</table>
	</div>
	<div id="res2" class="divResInternoDerecho">
		<table class="table1" >
		<thead>
			<th colspan='3'>Vencidas</th>
		</thead>		
		<tr>
			<th class="celdaTitulo1">Tipo Actuacion</th><th class="celdaTitulo1">Provision > 7 días </th><th class="celdaTitulo1">Averias > 24 horas</th>
			<?php /*<td>TBA (<?=$pendTbaProvision?>:<?=$pendTbaAverias?>)</td>
			<td>ADSL (<?=$pendADSLProvision?>:<?=$pendADSLAverias?>)</td>
			<td>CATV (<?=$pendCATVProvision?>:<?=$pendCATVAverias?>)</td>
			*/ ?>
		</tr>
		<tr>
			<td class="celdaRes1">TBA:</td> 
			<td class="celdaRes1"><?php echo $pendTbaProvisionVencidas?></td>
			<td class="celdaRes1"><?php echo $vencidasTbaAverias?></td>
		</tr>
		<tr>
			<td class="celdaRes1">ADSL:</td>
			<td class="celdaRes1"><?php echo $pendAdslProvisionVencidas?></td>
			<td class="celdaRes1"><?php echo $vencidasAdslAverias?></td>
		</tr>
		<tr>
			<td class="celdaRes1">CATV:</td>
			<td class="celdaRes1"><?php echo $pendCatvProvisionVencidas?></td>
			<td class="celdaRes1"><?php echo $vencidasCatvAverias?></td>
		</tr>

		</table>
	</div>	

</div>
<div id="re3" class="divResInternoDerecho">
		<table class="table1" >
		<thead>
			<th colspan='3'>Agendas</th>
		</thead>		
		<tr>
			<th class="celdaTitulo1">Tipo Actuacion</th><th class="celdaTitulo1">Provision > 7 días </th><th class="celdaTitulo1">Averias > 24 horas</th>
			<?php /*<td>TBA (<?=$pendTbaProvision?>:<?=$pendTbaAverias?>)</td>
			<td>ADSL (<?=$pendADSLProvision?>:<?=$pendADSLAverias?>)</td>
			<td>CATV (<?=$pendCATVProvision?>:<?=$pendCATVAverias?>)</td>
			*/ ?>
		</tr>
		<tr>
			<td class="celdaRes1">TBA:</td>
			<td class="celdaRes1"><?php echo $AgendasDiaTbaProvision?></td>
			<td class="celdaRes1"><?php echo $AgendasDiaTbaAverias?></td>
		</tr>
		<tr>
			<td class="celdaRes1">ADSL:</td>
			<td class="celdaRes1"><?php echo $AgendasDiaAdslProvision?></td>
			<td class="celdaRes1"><?php echo $AgendasDiaAdslAverias?></td>
		</tr>
		<tr>
			<td class="celdaRes1">CATV:</td>
			<td class="celdaRes1"><?php echo $AgendasDiaCatvProvision?></td>
			<td class="celdaRes1"><?php echo $AgendasDiaCatvAverias?></td>
		</tr>

		</table>








