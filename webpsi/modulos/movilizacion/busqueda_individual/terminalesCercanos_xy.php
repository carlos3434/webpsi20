<?php
require_once("../../../clases/class.FacilidadesTecnicas.php");

if ($_REQUEST["zonal"]!='0') {
	$posX = $_REQUEST['posX'];
	$posY = $_REQUEST['posY'];
	$minDistancia = $_REQUEST["minDistancia"];	
	$maxDistancia = $_REQUEST["maxDistancia"];
	$maxElementos = $_REQUEST["maxElementos"];
}


//var_dump($_REQUEST);

$objFFTT = new FacilidadesTecnicas();

$cajaOrigen_terminalX = $posX;
$cajaOrigen_terminalY = $posY;

$arrCajasCercanas = $objFFTT->getTerminalCercanosXY_porXY($posX, $posY, $minDistancia, $maxDistancia, $maxElementos ) ;

?>

<script type="text/javascript">
	/*
	var zonal = $("#txtZonal").val();
	var mdf = $("#txtMdf").val();
	var tipoRed = $("#txtTipoRed").val();
	var cable_armario = $("#txtCableArmario").val();
	var caja_terminal = $("#txtTerminal").val();

	var minDistancia = $("#txtMinDistancia").val();
	var maxDistancia = $("#txtMaxDistancia").val();
	var maxElementos = $("#txtMaxElementos").val();

	var pag = "mapa_fftt.php?coordX="+<?=$cajaOrigen_terminalX?>+"&coordY="+<?=$cajaOrigen_terminalY?>;
	pag = pag+"&zonal="+zonal+"&mdf="+mdf+"&tipo_red="+tipoRed+"&cable_armario="+cable_armario;
	pag = pag+"&caja_terminal="+caja_terminal;
	pag = pag+"&minDistancia="+minDistancia;
	pag = pag+"&maxDistancia="+maxDistancia;
	pag = pag+"&maxElementos="+maxElementos;
	//alert(pag);
	*/

	//$("#frame_mapa").attr('src', pag);
</script>

            <?php include ("../../../includes.php") ?>     

<link type="text/css" href='../css/estilo.css' rel="Stylesheet" />
<link type="text/css" href="../css/estilosBusqueda.css" rel="Stylesheet" />
<link rel="stylesheet" type="text/css" href="../fftt/estilo.css">

</head>

<body>
<table class="tablaClonar" >
<tr>	
	<td class="celda_titulo" style="width: 18%;">Pos. X/Y:</td>
	<td class="celda_res"><?=$posX.", ".$posY?></td>
</tr>
</table>

<br/>
<?php 
//echo "Direccion de Caja Terminal: ".$direccionCajaT;
//echo "<br/>X/Y De Caja Terminal: X=".$caja_terminalX." / "."Y=".$caja_terminalY;

?>

<input type="hidden" value="<?=$caja_terminalX?>" id="txtCoordX" name="txtCoordX" />
<input type="hidden" value="<?=$caja_terminalY?>" id="txtCoordY" name="txtCoordY" />

<input type="hidden" value="" id="txtCoordXY_Mapa" name="txtCoordXY_Mapa" class="caja_texto1" />

<input type="hidden" value="<?=$zonal?>" id="txtZonal" name="txtZonal" />
<input type="hidden" value="<?=$mdf?>" id="txtMdf" name="txtMdf" />
<input type="hidden" value="<?=$tipo_red?>" id="txtTipoRed" name="txtTipoRed" />
<input type="hidden" value="<?=$cable_armario?>" id="txtCableArmario" name="txtCableArmario" />
<input type="hidden" value="<?=$caja_terminal?>" id="txtTerminal" name="txtTerminal" />



<form name="form_cajas" id="form_cajas" action="">
<table class="tablaClonar" >
<thead>
	<th class="celda_titulo" style="width: 1px">#</th>
	<th class="celda_titulo">FFTT Terminal</th>
	<th class="celda_titulo">Data</th>
	<!--<th class="celda_titulo">Pares Libres</th>	-
	<th class="celda_titulo">Max Velocidad</th>
	-->
	<th class="celda_titulo">Direccion CT</th>
</thead>

<?php

$i=1;
$j=0;


foreach ($arrCajasCercanas as $row ) {
	$nombretxtCaja = "txtXY_Cliente".$j;
	$nombretxtCaja2 = "txtInscripcion_Cliente".$j;
	$nombretxtCaja3 = "txtZonal_Cliente".$j;

	if ($row['qparlib']>2) 
		$alarma = "bgVerde";
	else if ($row['qparlib']<=2 and $row['qparlib']>0 )
		$alarma = "bgAmarillo";
	else 
		$alarma = "bgRojo";

	if (strtoupper($row["tipoDslam"])=="DI")
		$dslam = "dslamDI";
	else
		$dslam = "dslamDO";

	$tipoRed_Row = $row["tipo_red"];


	if (strtoupper($tipoRed_Row)=="D") 
		$cable_armario_Row = $row["cable"];
	else
		$cable_armario_Row = $row["armario"];

	$cajaRow = $row["caja"];

	?> 
	<tr>
		<td class="celda_res <?=$alarma?>" style="width: 5%; "><a href="javascript: void(0)" onclick="mostrarParesClientes('<?=$cajaRow?>','<?=$tipoRed_Row?>','<?=$cable_armario_Row?>')"><? echo $i ?></a>
			<input type="hidden" id="txtLlave" name="txtLlave" value="<?php echo $row['llave']?>" /> 		
		</td>
		<td class="celda_res <?=$dslam?>" style="width: 20%; "><? echo $row['zonal']." ".$row["mdf"]." ".$row["cable"]." ".$row["armario"]." ".$row["caja"]." - ".$row["tipoDslam"] ; ?>
		</td>		
		<td class="celda_res" style="width: 12%; text-align: left;"><? echo round($row['distancia'], 0)." mts.<br/>".$row['qparlib']." P.libres<br/>".$row['sugerencia'];?></td>
		<!--<td class="celda_res" style="width: 10%; "><? echo $row['qparlib']; ?></td>-->
		<!--<td class="celda_res" style="width: 10%; "><? echo $row['sugerencia']; ?></td>-->
		<td class="celda_res" style="width: 63%; "><? echo $row['direccion']."<br/>".$row["x"]." ".$row["y"]; ?></td>
	</tr>	
	<?php
	$i++;
	$j++;
}
?>
</table>
</form>

<br/>
<br/>
</body>