<?php 
require_once("../../../cabecera.php");
set_time_limit(0);

include ("../../../clases/class.UsuarioFftt.php");

//echo "idusuario = ".$IDUSUARIO;

function comboTipoActuacion($idusuario) {
	$html = "";
	$Fftt = new UsuarioFftt();
	$arr = $Fftt->listarTipoActuacionUsuario($idusuario);

    $html.='<select name="cmbTipoActuacion" class="texto_simple2" 
        id="cmbTipoActuacion" onchange="">';
    foreach ($arr as $row ) {
		$html.='<option value="'.$row[1].'">'.$row[1].'</option>';
    }

    $html.='<option value=99>Posicion X/Y'.'</option>';
    $html.='</select>';
    echo $html;
}

function comboNegocios() {
	$html = "";

    $html.='<select name="cmbNegocios" class="texto_simple2" 
        id="cmbNegocios" >';
    $arr = array( array(0, 'Seleccione...'),
    			  array(1, 'TBA'),
    			  array(2, 'ADSL'),
    			  array(3, 'CATV')
     );

    foreach ($arr as $row ) {
		$html.='<option value="'.$row[1].'">'.$row[1].'</option>';
    }
    $html.='</select>';
    echo $html;
}

function comboBusqueda() {
	$html = "";
    $html.='<select name="cmbBusqueda" class="texto_simple2" 
        id="cmbBusqueda" onchange="">';
    $arr = array( array(0, 'Seleccione...'),
    			  array("PETICION", 'Peticion'),
    			  array("AVERIA", 'Averia'),
    			  array("TELEFONO", 'Telefono/Cod Cliente')
     );

    foreach ($arr as $row ) {
		$html.='<option value="'.$row[0].'">'.$row[1].'</option>';
    }
    $html.='</select>';
    echo $html;
}

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />

<title></title>

<?php include ("../../../includes.php") ?>    


<link type="text/css" href="../css/redmond/jquery-ui-1.8.17.custom.css" rel="Stylesheet" />
<link type="text/css" href='../css/estilo.css' rel="Stylesheet" />
<link type="text/css" href="estilosBusqueda.css" rel="Stylesheet" />
<script type="text/javascript" src="../js/jquery-1.7.1.min.js"></script>
<script type="text/javascript" src="../js/jquery-ui-1.8.17.custom.min.js"></script>
<script type="text/javascript" src="../js/cronometro.js"></script>

<script type="text/javascript">

function validar_requerido(field,alerttxt)
{
	var value = $(field).val();
	if (value==null||value==""||value==0)
		{alert(alerttxt);return false}
	else {return true}
}


$(document).ready(function(){
	document.getElementById('cmbTipoActuacion').selectedIndex = 1;
	document.getElementById('cmbBusqueda').selectedIndex = 1;
	$("#txtBusqueda").focus();
	
	//revisar();

	$('#cmbTipoActuacion').change(function(){

		$("#cmbNegocios").attr("disabled", false);
		$("#cmbBusqueda").attr("disabled", false);

		if($(this).val()=='99') {
			alert("Escribir el punto X/Y en formato Longitud, Latitud");
			$("#cmbNegocios").val(0);
			$("#cmbBusqueda").val(0);
			$("#cmbNegocios").attr("disabled", true);
			$("#cmbBusqueda").attr("disabled", true);
			$("#txtBusqueda").val('');
			$("#txtBusqueda").focus();
		}
		else if($(this).val()=='Averias')
			document.getElementById('cmbNegocios').selectedIndex = 2;
		else
			document.getElementById('cmbBusqueda').selectedIndex = 1;
		//document.getElementById('cmbBusqueda').selectedIndex = 2;
	});



	$('#btnBuscar').click(function(){
        var cmbTipoActuacion = $("#cmbTipoActuacion").val();  
        var cmbBusqueda = $("#cmbBusqueda").val();  
        var txtBusqueda = $("#txtBusqueda").val();

        //alert(cmbBusqueda);

    	if (validar_requerido($("#cmbTipoActuacion"),"Eliga el tipo de actuacion.")==false)
        	{$("#cmbTipoActuacion").focus();return false}

    	if (validar_requerido($("#txtBusqueda"),"Escriba la actuacion.")==false)
        	{$("#txtBusqueda").focus();return false}        

 		//alert("tipo actuacion = "+cmbTipoActuacion+", cmbBusqueda="+cmbBusqueda+", txtBusqueda="+txtBusqueda);	

 		if ( $("#cmbTipoActuacion").val()!="99" ) {

	    	if (validar_requerido($("#cmbNegocios"),"Eliga el negocio.")==false)
	        	{$("#cmbNegocios").focus();return false}        
	    	if (validar_requerido($("#cmbBusqueda"),"Eliga el tipo de busqueda.")==false)
	        	{$("#cmbBusqueda").focus();return false}

	 		$( "#resultado" ).load("pintarbusqueda1.php",
				{
					cmbTipoActuacion: $('#cmbTipoActuacion').val(),
					cmbNegocios: $("#cmbNegocios").val(),
					cmbBusqueda: $('#cmbBusqueda').val(),
					txtBusqueda: $('#txtBusqueda').val()
				},
				function() {
					var cx = $("#txtCoordX").val();
					var cy = $("#txtCoordY").val();
					//alert("X = "+cx+"  Y = "+cy);

					var zonal =  $('#cmb_zonales').val();
					var mdf = $('#cmb_mdfs').val();
					var tipo_red =  $('#cmb_tipored').val();
					var cable_armario = $('#cmb_cable_armario').val();
					var caja_terminal =  $('#cmb_caja_terminal').val();			
				}			
			);
		}
		else
		{
	 		$( "#resultado" ).load("pintarbusqueda_xy.php",
				{
					txtBusqueda: $('#txtBusqueda').val()
				},
				function() {
					var cx = $("#txtCoordX").val();
					var cy = $("#txtCoordY").val();
					//alert("X = "+cx+"  Y = "+cy);

					var zonal =  $('#cmb_zonales').val();
					var mdf = $('#cmb_mdfs').val();
					var tipo_red =  $('#cmb_tipored').val();
					var cable_armario = $('#cmb_cable_armario').val();
					var caja_terminal =  $('#cmb_caja_terminal').val();			
				}			
			);			
		}


    });	

});

</script>

</head>

<body>

<input type="hidden" value="<?php echo $IDUSUARIO ?>" name="txt_idusuario" id="txt_idusuario"/>

<?php echo pintar_cabecera(); ?>

<br/>

<div id="div_bus" class="divBusqueda" style="width: 620px">
	<table class="tablaBusqueda" style="width:100%">
	<form id="form" name="form" method="POST" >
	<tr class="tr_busqueda" style="height: 30px;">
		<td style="width: 20%; padding; 1px; border: 0px solid ;" >
			<label style="padding: 1px;">Tipo:</label><?php echo comboTipoActuacion($IDUSUARIO); ?>
		</td>
		<td style="width: 25%; padding; 2px; border: 0px solid ;">
			<label style="padding: 5px;">Negocio:</label><?php echo comboNegocios(); ?>
		</td>		
		<td style="width: 20%; padding; 2px; border: 0px solid ; ">
			<label style="padding: 2px;">Busq:</label><?php echo comboBusqueda(); ?>
		</td>
		<td style="width: 15%; padding; 2px; border: 0px solid ; ">
			<input type="text" id="txtBusqueda" name="txtBusqueda" style="width: 90px; border: 1px solid; padding: 1px;" >
		</td>
		<td align="left" style="width: 15%; border: 0px solid ;">
			<input type="button" id="btnBuscar" name="btnBuscar" 
				value="Revisar" style="width: 80px; font-weight: bold;" />
		</td>
	</tr>
	</form>
	</table>
</div>


<div id="div_cronometro" class="div_busqueda">
<b><span id="cronometro_prueba"></span></b>
</div>

<br/>

<div id="pp1" class="divResMain" style="border: 1px ; display: table;">
	<a name="targetListado" ></a>


<div id="resultado" class="divResMain" style="border: 1px;"></div> 

</div> 
</body>
</html>