<?php
header('Content-Type: text/html; charset=UTF-8');
header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache");


include ("../../../clases/class.Indicadores.php");


$hoy = date("Y-m-d H:i:s");
$hoyFormato = date("d-m-Y H:i:s");
$hoyFecha = date("Y-m-d");

$Indicadores = new Indicadores();

$cmbTipoActuacion = trim(strtoupper($_POST["cmbTipoActuacion"]));
$cmbNegocios = trim(strtoupper($_POST["cmbNegocios"]));
$cmbBusqueda = trim(strtoupper($_POST["cmbBusqueda"]));
$actuacion = trim(strtoupper($_POST["txtBusqueda"]));


//echo "cmbTipoActuacion = ".$cmbTipoActuacion.", cmbNegocios = ".$cmbNegocios.", cmbBusqueda=".$cmbBusqueda.", txtBusqueda=".$actuacion;

if ($cmbTipoActuacion=="PROVISION" && $cmbBusqueda=="PETICION") {
	switch ($cmbNegocios) {
		case 'TBA':
			echo "TBA PROVISION";
			$arrDetalle= $Indicadores->getActuacionPendienteDetalle_ProvisionTba($actuacion);			
			break;
		case 'ADSL':
			echo "ADSL PROVISION";
			$arrDetalle= $Indicadores->getActuacionPendienteDetalle_ProvisionAdsl($actuacion);			
			break;			
		case 'CATV':
			$arrDetalle= $Indicadores->getActuacionPendienteDetalle_ProvisionTba($actuacion);			
			break;		
	}
}
else if ($cmbTipoActuacion=="AVERIAS" && $cmbBusqueda=="AVERIA") {
	switch ($cmbNegocios) {
		case 'TBA':
			echo "TBA AVERIAS";
			$arrDetalle= $Indicadores->getActuacionPendienteDetalle_AveriasTba($actuacion);			
			break;
		case 'ADSL':
			echo "ADSL AVERIAS";
			$arrDetalle= $Indicadores->getActuacionPendienteDetalle_AveriasAdsl($actuacion);			
			break;			
		case 'CATV':
			$arrDetalle= $Indicadores->getActuacionPendienteDetalle_AveriasTba($actuacion);			
			break;		
	}
}
else if ($cmbTipoActuacion=="AVERIAS" && $cmbBusqueda=="TELEFONO") {
	switch ($cmbNegocios) {
		case 'TBA':
			//echo "TBA AVERIAS x Telefono";
			$arrDetalle= $Indicadores->getActuacionPendienteDetalle_AveriasTba_xFono($actuacion);			
			break;
		case 'ADSL':
			//echo "ADSL AVERIAS x Telefono";
			$arrDetalle= $Indicadores->getActuacionPendienteDetalle_AveriasAdsl_xFono($actuacion);			
			break;			
		case 'CATV':
			echo "CATV AVERIAS x Cod Cliente";
			$arrDetalle= $Indicadores->getActuacionPendienteDetalle_AveriasCatv_xFono($actuacion);			
			break;		
	}
}


//getActuacionPendienteDetalle_AveriasTba

//var_dump($arrDetalle);

?>

<br/>

<table class="table" style="width: 500px;">
<thead>
<th colspan="6" style="background: #FFDE40; color: #17688B;">
	<?php 
	switch ($tipoDetalle) {
		case 'porvencer':
			$tipoDetalle = "Por Vencer";
			$tipoDetalleOriginal = "porvencer";
			break;	
		default:
			$tipoDetalleOriginal = $tipoDetalle;
			# code...
			break;
	}
	echo strtoupper($cmbTipoActuacion)." / ".strtoupper($cmbNegocios) ;
	?>
</th>
</thead>

<?php
$i=1;

if (count($arrDetalle)< 1 ) {
	echo "No se encontraron datos.";
}

foreach ($arrDetalle as $fila) {
	?>
	<tr>
		<td class="thDetalleActuacion">Actuacion:</td>
		<td class="celdaRes"><?=$fila["xactuacion"];?></td>
		<td width="20%" class="thDetalleActuacion">FFTT</td>
		<td class="celdaRes" colspan="3"><?=$fila["xmdf"]." ".$fila["xarmario"]." ".$fila["xcable"]." ".$fila["xterminal"];?></td>
		
	</tr>	
	<tr>
		<td class="thDetalleActuacion">Telefono/Cod Cliente</td>
		<td class="celdaRes"><?=$fila["xtelefono"];?></td>
		<td width="20%" class="thDetalleActuacion">Cliente</td>
		<td class="celdaRes" colspan="3"><?=$fila["xcliente"];?></td>
		
	</tr>
	<tr>
		<td width="15%" class="thDetalleActuacion">Direccion / Distrito</td>
		<td class="celdaRes" colspan="5"><?=$fila["xdireccion"]." ".$fila["xdistrito"];?></td>

	</tr>
	<tr>
		<td class="thDetalleActuacion">Fecha Registro</td>
		<td class="celdaRes"><?=$fila["xfecreg"];?></td>
		<td width="20%" class="thDetalleActuacion">Area</td>
		<td class="celdaRes"><?=$fila["xarea"];?></td>
		<td class="thDetalleActuacion">Segmento</td>
		<td class="celdaRes"><?=$fila["xsegmento1"]." ".$fila["xsegmento2"];?></td>
	</tr>	

	<?php
	$i++;
}
?>
</table>	

<?php
$mdf = $fila["xmdf"];

if ($cmbNegocios!="CATV") {
	if ($fila["xarmario"]=="" or $fila["xarmario"]==null) {
		$tipo_red = "D";
	}
	else {
		$tipo_red = "F";
		$cable_armario = $fila["xarmario"];
	}
}
else {
	$tipo_red = "CATV";
	$cable_armario = $fila["xarmario"];
}


$caja_terminal = $fila["xterminal"];
$caja_terminal = $fila["xterminal"];
$minDistancia = 0;
$maxDistancia = 200;
$maxElementos = 20;
$coordX = $fila["coordX"];
$coordY = $fila["coordY"];

//echo "coordX = ".$coordX;

?>

<br/>


<div id="divTerminalesCercanos" class="width: 500px; height: 600px;" >
<?php
	if ($tipo_red!="CATV")
		$pag1 = "terminalesCercanos.php?zonal=$zonal&mdf=$mdf&tipo_red=$tipo_red&cable_armario=$cable_armario&caja_terminal=$caja_terminal&minDistancia=$minDistancia&maxDistancia=$maxDistancia&maxElementos=$maxElementos" ;
	else
		$pag1 = "tapsCercanos.php?nodo=$mdf&troba=$cable_armario&tap=$caja_terminal&minDistancia=$minDistancia&maxDistancia=$maxDistancia&maxElementos=$maxElementos" ;
	//echo $pag1;
?>

	<!--<iframe src="terminalesCercanos.php?zonal=<?=$zonal?>&mdf=<?=$mdf?>&tipo_red=<?=$tipo_red?>&cable_armario=<?=$cable_armario?>&caja_terminal=<?=$caja_terminal?>&minDistancia=<?=$minDistancia?>&maxDistancia=<?=$maxDistancia?>&maxElementos=<?=$maxElementos?>" 
			id="frame_cercanos" style="width: 100%; height: 500px;" ></iframe> 
	-->
	<iframe src="<?php echo $pag1?>"
			id="frame_cercanos" style="width: 100%; height: 500px;" ></iframe> 

</div>

<br/>



<div id="divMapa" class="mapa" >
<?php
$pagMapa = "../fftt/mapa_fftt.php?coordX=$coordX&coordY=$coordY&zonal=$zonal&mdf=$mdf&tipo_red=$tipo_red&cable_armario=$cable_armario&caja_terminal=$caja_terminal&minDistancia=$minDistancia&maxDistancia=$maxDistancia&maxElementos=$maxElementos";

//echo $pagMapa;
?>
<iframe src="<?php echo $pagMapa ?>"
			id="frame_mapa" style="width: 100%; height: 100%;" ></iframe> 
</div>



<br/>
</table>


<br/>


