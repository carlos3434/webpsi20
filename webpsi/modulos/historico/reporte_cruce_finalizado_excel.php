<?php
header("Content-Type:  application/x-msexcel");
header("Expires: 0"); 
header("Cache-Control: must-revalidate, post-check=0, pre-check=0"); 
header("Content-Disposition: attachment; filename=cruce_finalizado.xls");
header("Expires: 0");


include ("../../clases/class.Conexion.php");

print "\xEF\xBB\xBF"; // UTF-8 BOM



function limpia_cadena_rf($value) {
        $input = trim( preg_replace( '/\s+/', ' ', $value ) );
        return $input;
}

function limpia_campo_mysql_text($text)
{
    $text = preg_replace('/<br\\\\s*?\\/??>/i', "\\n", $text);
    return str_replace("<br />","\n",$text);
}

function writeRow($val) {
    echo '<td>'.limpia_campo_mysql_text($val).'</td>';              
}
function limpia_cadena($value)
{
   $nopermitidos = array("'",'\\','<','>',"\"",";"); 
   $nueva_cadena = str_replace($nopermitidos, "", $value); 
   return $nueva_cadena;
}

$objCnx = new Conexion();

$cnx = $objCnx->conectarPDO();


$fini=$_GET["fecIni"];
$ffin=$_GET["fecFin"];

//$cad = "SELECT * FROM webpsi_coc.averias_criticos_final ORDER BY 1 ASC ";
$cad = "SELECT * FROM (
		SELECT gc.id,gc.id_atc,e.estado,IFNULL(substr(t.paso,6),'') paso
			,(	select fecha_recepcion
				from webpsi_officetrack.tareas
				where task_id=t.task_id
				and cod_tecnico= t.cod_tecnico
				and paso='0001-Inicio'
				order by id desc limit 1
			) f_inicio
			,(	select fecha_recepcion
				from webpsi_officetrack.tareas
				where task_id=t.task_id
				and cod_tecnico= t.cod_tecnico
				and paso='0002-Supervision'
				order by id desc limit 1
			) f_supervision
			,(	select fecha_recepcion
				from webpsi_officetrack.tareas
				where task_id=t.task_id
				and cod_tecnico= t.cod_tecnico
				and paso='0003-Cierre'			
				order by id desc limit 1
			) f_cierre
			,IF(gc.tipo_actividad='Averias',(SELECT averia FROM webpsi_criticos.gestion_averia WHERE id_gestion=gc.id)
				,IF(gc.tipo_actividad='Provision',(SELECT codigo_req FROM webpsi_criticos.gestion_provision WHERE id_gestion=gc.id)
					,IF(gc.tipo_actividad='Manual',(SELECT averia FROM webpsi_criticos.gestion_rutina_manual WHERE id_gestion=gc.id)
						,(SELECT codigo_req FROM webpsi_criticos.gestion_rutina_manual_provision WHERE id_gestion=gc.id)
						)
					)
			) as averia, gc.tipo_actividad
			,IF(gc.tipo_actividad='Averias',(SELECT tipo_averia FROM webpsi_criticos.gestion_averia WHERE id_gestion=gc.id)
				,IF(gc.tipo_actividad='Provision',(SELECT origen FROM webpsi_criticos.gestion_provision WHERE id_gestion=gc.id)
					,IF(gc.tipo_actividad='Manual',(SELECT tipo_averia FROM webpsi_criticos.gestion_rutina_manual WHERE id_gestion=gc.id)
						,(SELECT origen FROM webpsi_criticos.gestion_rutina_manual_provision WHERE id_gestion=gc.id)
						)
					)
			) as tipo_averia
			,(SELECT fecha_agenda 
				FROM webpsi_criticos.gestion_movimientos 
				WHERE id_gestion=gc.id 
				AND id_estado=1 
				ORDER BY id DESC LIMIT 1) fecha_agenda
			,(SELECT h.horario 
				FROM webpsi_criticos.gestion_movimientos m 
				INNER JOIN webpsi_criticos.horarios h ON h.id=m.id_horario
				WHERE m.id_gestion=gc.id 
				AND m.id_estado=1 
				ORDER BY m.id DESC LIMIT 1) hora_agenda
			,replace(gm.fecha_consolidacion,'0000-00-00','') fecha_consilidacion
			,gm.observacion,gm.tecnico,concat(u.apellido,', ',u.nombre) usuario
		FROM webpsi_criticos.gestion_criticos gc
		INNER JOIN webpsi_criticos.gestion_movimientos gm on gc.id=gm.id_gestion
		INNER JOIN webpsi.tb_usuario u ON gm.id_usuario=u.id 
		INNER JOIN webpsi_criticos.horarios h on gm.id_horario=h.id
		INNER JOIN webpsi_criticos.estados e on gm.id_estado=e.id
		LEFT JOIN webpsi_officetrack.tareas t on (gc.id=t.task_id AND t.id=(SELECT id 
																			FROM webpsi_officetrack.tareas 
																			WHERE task_id=gc.id 
																			ORDER BY id DESC LIMIT 1
																			) 
												 )
		WHERE gm.id=(	SELECT id 
						FROM webpsi_criticos.gestion_movimientos 
						WHERE id_gestion=gc.id 
						ORDER BY id DESC LIMIT 1
					)
		AND gc.n_evento=1
		AND gm.id_estado!='21' ) reporte
		WHERE reporte.fecha_agenda BETWEEN '$fini' AND '$ffin'
		ORDER BY reporte.fecha_agenda";
$res = $cnx->query($cad);
$array = $res->fetchAll(PDO::FETCH_ASSOC);

foreach($array as $row){
	foreach($row as $key=>$val){
		if(!in_array($key, $h)){
			$h[] = $key;   
		}
	}
}
echo "<table border='1' cellpadding='0' cellspacing='0'><tr>";

foreach($h as $key) {
	$key = ucwords($key);
	echo '<th>'.$key.'</th>';
}
echo '</tr>';


foreach($array as $row){
	echo '<tr>';
	foreach($row as $val)
		writeRow($val);   
}
echo '</tr>';

echo '</table>';

?>
