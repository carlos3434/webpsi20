<?php
require_once("../../cabecera.php");
require_once('clases/quiebre.php');

$ob_quiebre = new Quiebre();
$quiebre = $ob_quiebre->getQuiebresAllOfficeTrack();

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
        <title>PSI - Web SMS - Clientes Críticos</title>
        <!--<meta http-equiv="content-type" content="text/html; charset=iso-8859-1">-->
        <meta http-equiv="content-type" content="text/html; charset=utf8">
        <meta name="author" content="" />

        <?php include ("../../includes.php") ?>

		<script type="text/javascript">

		function GenerarActQuiebre(){
			var inputFile = document.getElementById("file_tmp");
		    var file = inputFile.files[0];
		    var data = new FormData();
		    var url = "actualiza_quiebre_masivo_ajax.php";

		    data.append('archivoTmp', file);
		    data.append('quiebre', $("#slct_quiebre").val());

		    if( $("#file_tmp").val()!='' && $("#slct_quiebre").val()!='' ){
			    $.ajax({
			        url: url,
			        type: 'POST',
			        data: data,
			        contentType: false,
			        processData: false,
			        cache: false,
			        dataType: "json",
			        success: function(datos) {
			        	if(datos.estado){
			        		alert('Se realizó con éxito');	
			        	}
			        	else{
			        		alert('No se encontro el archivo. Favor de volver buscarlo y cargarlo');
			        	}
			            
			        },
			        error: function(datos) {
			            console.log(datos);
			        }
			    });
			}
			else if($("#slct_quiebre").val()==''){
				alert('Seleccione un quiebre');
			}
			else{
				alert('Busque y seleccione un archivo');
			}
			
		}
		</script>
		<link type="text/css" href='css/estilo.css' rel="Stylesheet" />
</head>

<body>
<?php echo pintar_cabecera(); ?>

<div class="bandeja_clientes">
	<div class="filtro_criticos">
		<div class="filtro_clientes">
		<form action="" method="POST" name="frmQA" id="frmQA" enctype="multipart/form-data">
		  	<fieldset>
			<legend class="title">Actualización masiva de quiebre:</legend>
				<div class="toolBox">
	                <label>Quiebre</label>
	                <div>
	                    <select id="slct_quiebre" name="slct_quiebre">
	                    	<option value="">--Seleccione--</option>
	                    	<?php
	                    		foreach ($quiebre as $r) {
	                    			echo "<<option value='$r[id_quiebre]'>$r[nombre]</option>";
	                    		}
	                    	?>
	                    </select>
	                </div>
	            </div>
	            <br>
				<div class="toolBox">
	                <label>Archivo</label>
	                <div>
	                    <input type="file" name="file_tmp" id="file_tmp" />
	                </div>
	            </div> 
		  	</fieldset>
		  	<br>
		  	<div class="filtroxcampo asignar">
			    <span id="generar_act_quiebre" onClick="GenerarActQuiebre();" class="btn_buscar">Generar Actualización</span>
			</div>
		</form>
		</div>
	</div>
	<div class="listado_clientes">
	</div>
</div>