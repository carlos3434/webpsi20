<?php
require_once("../../cabecera.php");
require_once('clases/quiebre.php');

//$ob_quiebre = new Quiebre();
//$quiebre = $ob_quiebre->getQuiebresAll();

//Abriendo la conexion
$db = new Conexion();
$cnx = $db->conectarPDO();

$ob_quiebre = new Quiebre();
$quiebre = $ob_quiebre->getQuiebre($cnx,$IDUSUARIO);
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
        <title>PSI - Web SMS - Clientes Críticos</title>
        <!--<meta http-equiv="content-type" content="text/html; charset=iso-8859-1">-->
        <meta http-equiv="content-type" content="text/html; charset=utf8">
        <meta name="author" content="" />

        <?php include ("../../includes.php") ?>
        <link type="text/css" rel="stylesheet" href="css/actualiza_quiebre_individual.css" />
        <link type="text/css" href='css/estilo.css' rel="Stylesheet" />
</head>

<body>
    <?php echo pintar_cabecera(); ?>

    <div class="bandeja_clientes" >
        <div class="filtro_criticos">
            <div class="filtro_clientes">
                <form action="" method="POST" name="frmQA" id="frmQA" enctype="multipart/form-data">
                    <fieldset>
                    <legend class="title">Actualización individual de quiebre:</legend>
                        <div class="toolBox">
                            <label>Quiebre</label>
                            <div>
                                <select id="slct_quiebre" name="slct_quiebre">
                                    <option value="">--Seleccione--</option>
                                    <?php
                                        foreach ($quiebre as $r) {
                                            echo "<option value='$r[id]'>$r[nombre]</option>";
                                        }
                                    ?>
                                </select>
                            </div>
                        </div>
                        <br />
                        <div class="toolBox">
                            <label>Archivo</label>
                            <div>
                                <input type="file" accept="text/plain" name="file_tmp" id="file_tmp" onchange="visualizarFiles(this.files)"/>
                            </div>
                            <br />
                            <div id='actividad'></div>
                            <br />
                            <div id="fileOutput" ></div>
                        </div> 
                    </fieldset>
                    <br>
                    <div class="filtroxcampo asignar">
                        <span id="generar_act_quiebre" onClick="GenerarActQuiebre();" class="btn_buscar">Generar Actualización</span>
                    </div>
                </form>
            </div>
        </div>
        <div class="listado_clientes"></div>
    </div>
    <script type="text/javascript" src="js/actualiza_quiebre_individual.js"></script>
</body>