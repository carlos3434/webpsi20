<?php 
require_once("../../cabecera.php");

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />

<title>WEBPSI - Criticos - Reportes</title>

<?php include ("../../includes.php") ?>    

<script type="text/javascript" src="../../js2/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="../../js2/jquery-ui-1.10.3.custom/js/jquery-ui-1.10.3.custom.min.js"></script>
<link type="text/css" href="../../js2/jquery-ui-1.10.3.custom/css/redmond/jquery-ui-1.10.3.custom.css" rel="Stylesheet" />

<link type="text/css" href="css/reporteador.css" rel="Stylesheet" />
<link type="text/css" href='css/botones.css' rel="Stylesheet" />

<style type="text/css">
input[type="text"], select {
	/*border: 1px solid #000000;	*/
	border:1px solid #6297BC;
	padding: 1px;
	font-family:tahoma, arial, sans-serif;
	font-size: 11px;
}
</style>

<script type="text/javascript">
	
	$(document).ready(function(){

        $("#txt_fecha_ini").datepicker({
            yearRange:'0:+1',
            maxDate:'0D',
            changeYear: true,
            numberOfMonths: 1,
            dateFormat: 'yy-mm-dd'
        });
		
        $("#txt_fecha_fin").datepicker({
            yearRange:'0:+1',
			maxDate:'0D',
            changeYear: true,
            numberOfMonths: 1,
            dateFormat: 'yy-mm-dd'
        });
		
		
		var dlg=$('#register').dialog({
			title: 'Actualizacion',
			resizable: false,
			autoOpen:false,
			modal: true,
			hide: 'fade',
			width:450,
			height:350
		});		
	
		
	});
	


function reporte() {
	var fecIni = $("#txt_fecha_ini").val();
	var fecFin = $("#txt_fecha_fin").val();
	var pag = "reporte_cruce_finalizado_excel.php?fecIni="+fecIni+"&fecFin="+fecFin;

	window.open(pag);
}

</script>
</head>

<body>

<input type="hidden" value="<?php echo $IDUSUARIO ?>" name="txt_idusuario" id="txt_idusuario"/>

<?php echo pintar_cabecera(); ?>

<br/>



<div id="div_bus2" class="divBusqueda" style="width: 750px" >

	<table class="tablaBusqueda">
		<thead>
			<th colspan='3'><font size='+1'>Reporte de Cruce Finalizado - Officetrack VS Liquidados </font></th>
		</thead>
		
	<tr class="tr_busqueda">
		<td style="width: 120px">Fecha Inicial:</td >
		<td style="background-color:white; padding: 5px;" >
			<input type="text" id="txt_fecha_ini"
			   name="txt_fecha_ini" readonly="readonly"
			   value = "<?php echo date("Y-m-d")?>"/>
		</td >
	</tr>
	<tr class="tr_busqueda">
		<td style="width: 120px" >Fecha Final:</td >
		<td style="background-color:white; color: red; padding: 5px">
			<input type="text" id="txt_fecha_fin"
			   name="txt_fecha_fin" readonly="readonly"
			   value = "<?php echo date("Y-m-d")?>"/>
		</td >
	</tr>
	<tr>
		<td style="background-color:white; color: red; padding: 5px" colspan="2">                
			<div id="divBoton" style="padding-top: 3px; padding-bottom: 3px; width: 100px;">
					<a href="javascript:void(0)" onclick="reporte()" class="botonAzul">Reporte</a>
			</div>
		</td>
	</tr>

	</table>
</div>

<div id="register"></div>
	
</body>
</html>