<?php

class gestionMovimientos{

	function addGestionMovimientos($cnx,$id_gestion,$id_empresa,$id_zonal,$fecha_agenda
        	,$id_horario,$id_dia,$id_motivo,$id_submotivo,$id_estado,$tecnicos_asignados,$observacion,$id_usuario
        	,$fecha,$tecnico,$fecha_consolidacion,$idtecnico,$coordinado="0"){
		
			$sql = "INSERT INTO webpsi_criticos.gestion_movimientos values  ('',$id_gestion,$id_empresa,$id_zonal,'$fecha_agenda',$id_horario,$id_dia,$id_motivo,$id_submotivo,$id_estado,$tecnicos_asignados,
			'$observacion',$id_usuario,'$fecha','$tecnico','$fecha_consolidacion','$coordinado','$idtecnico')";
			//echo $sql;
			$res = $cnx->exec($sql);

			$id = $cnx->lastInsertId();

			//echo $sql;
			

			$sql2 = "UPDATE webpsi_criticos.gestion_criticos gc, (SELECT gc.id, COUNT(gm.id) AS nmov FROM webpsi_criticos.gestion_criticos gc, webpsi_criticos.gestion_movimientos gm
					 WHERE gc.id=gm.id_gestion AND gc.id=$id_gestion GROUP BY gc.id) t 
					 SET gc.nmov = t.nmov WHERE gc.id=t.id";
			$res = $cnx->exec($sql2);
        	return $res;
        
	}

	function getGestionMovimiento($cnx,$id){
		
		$cnx->exec("set names utf8");
		$sql = "SELECT c.observacion,c.id_gestion,tipo_actividad,nombre_cliente_critico,telefono_cliente_critico,celular_cliente_critico,
				c.id_horario,c.id_estado,c.id_dia,e.nombre,z.zonal,z.abreviatura,c.fecha_agenda,h.horario,m.motivo,s.submotivo,
				m.id as 'm_id',s.id as 's_id',es.estado,tec.nombre_tecnico tecnico,fecha_movimiento,t.usuario 
				FROM webpsi_criticos.gestion_movimientos c
				INNER JOIN webpsi_criticos.gestion_criticos cri ON c.id_gestion=cri.id 
				INNER JOIN webpsi_criticos.empresa e ON e.id=c.id_empresa
				INNER JOIN webpsi_criticos.horarios h ON h.id=c.id_horario
				INNER JOIN webpsi_criticos.zonales z ON z.id=c.id_zonal 
				INNER JOIN webpsi_criticos.motivos m ON m.id=c.id_motivo 
				INNER JOIN webpsi_criticos.submotivos s ON s.id=c.id_submotivo
				INNER JOIN webpsi_criticos.estados es ON es.id=c.id_estado 
				INNER JOIN webpsi.tb_usuario t ON c.id_usuario=t.id
				LEFT JOIN webpsi_criticos.tecnicos tec ON tec.id=c.idtecnico
				WHERE c.id_gestion='$id'
				ORDER BY c.id DESC";

		$arr = array();
		$res = $cnx->query($sql); 
        while ($row = $res->fetch(PDO::FETCH_ASSOC))
        {
            $arr[] = $row;
        }
        return $arr;
        
	}

	function getTecnicoUltimoMovimiento($cnx,$id_gestion){
		
		$cnx->exec("set names utf8");
		$sql = "SELECT tecnico FROM webpsi_criticos.gestion_criticos c,webpsi_criticos.gestion_movimientos mov 
				where c.id=mov.id_gestion and c.id=$id_gestion and 
				mov.fecha_movimiento=(SELECT MAX(mov2.fecha_movimiento) FROM webpsi_criticos.gestion_movimientos mov2
  								WHERE mov2.id_gestion=$id_gestion)";
		
		$arr = array();
		$res = $cnx->query($sql); 
        $row = $res->fetch(PDO::FETCH_ASSOC);
        return $row["tecnico"];
        
	}

	function getTecnico_UltimoMovimiento($cnx,$id_gestion){
		
		$cnx->exec("set names utf8");
		$sql = "SELECT t.id,t.nombre_tecnico tecnico,t.officetrack
				FROM webpsi_criticos.gestion_criticos c
				inner join webpsi_criticos.gestion_movimientos mov on mov.id_gestion=c.id
				INNER JOIN webpsi_criticos.tecnicos t ON t.id=mov.idtecnico 
				where c.id=$id_gestion and 
				mov.id=(	SELECT MAX(mov2.id) 
							FROM webpsi_criticos.gestion_movimientos mov2
							WHERE mov2.id_gestion=$id_gestion)";
		
		$arr = array();
		$res = $cnx->query($sql); 
        $row = $res->fetch(PDO::FETCH_ASSOC);
        return $row;
        
	}

	function getCriticoMovimiento($cnx,$emp,$reporte,$fecha_ini,$fecha_fin,$averia,$chk){
                               
       $cnx->exec("set names utf8");
       
       $filtroa="";
       $filtrop="";
       $ordenar="id_atc,id";
        if($chk=="1|1" or $chk=="1|"){
	       if($fecha_ini!="" && $fecha_fin!=""){
				if($reporte=="act"){			       
		        	$filtroa .=" AND DATE(a.fecha_registro) BETWEEN '$fecha_ini' AND '$fecha_fin'";
		        	$filtrop .=" AND DATE(p.fecha_Reg) BETWEEN '$fecha_ini' AND '$fecha_fin'";
				}
				elseif($reporte=="mov"){
					$ordenar=" tecnico,fecha_movimiento";
					$filtroa .=" AND DATE(c.fecha_movimiento) BETWEEN '$fecha_ini' AND '$fecha_fin'";
		        	$filtrop .=" AND DATE(c.fecha_movimiento) BETWEEN '$fecha_ini' AND '$fecha_fin'";
				}
				else{			     	
		        	$filtroa .=" AND DATE(cri.fecha_creacion) BETWEEN '$fecha_ini' AND '$fecha_fin'";
		        	$filtrop .=" AND DATE(cri.fecha_creacion) BETWEEN '$fecha_ini' AND '$fecha_fin'";			        
				}
			}
		}
		
		if($chk=="1|1" or $chk=="|1"){
			$averia=str_replace(",","','",$averia);
			$filtrop.=" AND p.codigo_req IN ('$averia')";
			$filtroa.=" AND a.averia IN ('$averia')";
		}

       $sql = "	SELECT * FROM(
					
								SELECT c.id,cri.id_atc,e.nombre eecc_final
									,(CASE c.id WHEN (	SELECT MAX(mov2.id) 
														FROM webpsi_criticos.gestion_movimientos mov2
														WHERE mov2.id_gestion=c.id_gestion
													 ) 
									  THEN 'X' ELSE '' END) 'ultimo_movimiento',tipo_actividad,a.averia,a.codmotivo_req_catv
								,a.quiebre,nombre_cliente_critico,telefono_cliente_critico,celular_cliente_critico,c.observacion
								,c.fecha_agenda,horario,d.dias, e.nombre,z.zonal,m.motivo,s.submotivo,es.estado,tec.nombre_tecnico tecnico
								,a.fecha_registro,fecha_creacion, fecha_movimiento,t.usuario,es.id AS 'e_id',c.fecha_consolidacion
								,tipo_averia,averia_m1,l.penalizable,a.mdf
								,(SELECT fecha_cambio 
									FROM webpsi_coc.`averias_criticos_final_historico` ach 
									WHERE ach.averia=a.`averia` 
								 ) AS fecha_cambio
								FROM webpsi_criticos.gestion_averia a 
								INNER JOIN webpsi_criticos.gestion_criticos cri ON a.id_gestion=cri.id 
								INNER JOIN webpsi_criticos.gestion_movimientos c ON c.id_gestion=cri.id 
								LEFT JOIN webpsi_criticos.tecnicos tec ON tec.id=c.idtecnico
								INNER JOIN webpsi_criticos.empresa e ON e.id=c.id_empresa 
								INNER JOIN webpsi_criticos.horarios h ON h.id=c.id_horario 
								INNER JOIN webpsi_criticos.zonales z ON z.id=c.id_zonal 
								INNER JOIN webpsi_criticos.dias d ON d.id=c.id_dia 
								INNER JOIN webpsi_criticos.motivos m ON m.id=c.id_motivo 
								INNER JOIN webpsi_criticos.submotivos s ON s.id=c.id_submotivo 
								INNER JOIN webpsi_criticos.estados es ON es.id=c.id_estado 
								INNER JOIN webpsi.tb_usuario t ON c.id_usuario=t.id 
								LEFT JOIN webpsi_criticos.liquidados l on c.id_gestion=l.id_gestion
								WHERE 	(l.id in (	SELECT max(l2.id) 
												 	FROM webpsi_criticos.liquidados l2 
												 	WHERE l2.id_gestion=c.id_gestion
											  	 )
										OR l.id IS NULL
										)
								$filtroa 
						
				UNION ALL
						
								SELECT c.id,cri.id_atc,e.nombre eecc_final
									,(CASE c.id WHEN (	SELECT MAX(mov2.id) 
														FROM webpsi_criticos.gestion_movimientos mov2
														WHERE mov2.id_gestion=c.id_gestion
													 ) 					
									 THEN  'X' ELSE '' END) 'ultimo_movimiento',tipo_actividad,p.codigo_req AS 'averia'
								,p.tipo_motivo as 'codmotivo_req_catv',p.quiebre,nombre_cliente_critico,telefono_cliente_critico
								,celular_cliente_critico,c.observacion, c.fecha_agenda,horario,d.dias, e.nombre,z.zonal,m.motivo
								,s.submotivo,es.estado,tec.nombre_tecnico tecnico,p.fecha_Reg AS 'fecha_registro',fecha_creacion,fecha_movimiento,t.usuario
								,es.id AS 'e_id',c.fecha_consolidacion,origen AS 'tipo_averia',aver_m1 AS 'averia_m1',l.penalizable,p.mdf
								,(SELECT fecha_cambio 
								 FROM webpsi_coc.`tmp_provision_historico` pch 
								 WHERE pch.codigo_req=p.codigo_req ) AS fecha_cambio
								FROM webpsi_criticos.gestion_provision p 
								INNER JOIN webpsi_criticos.gestion_criticos cri ON p.id_gestion=cri.id 
								INNER JOIN webpsi_criticos.gestion_movimientos c ON c.id_gestion=cri.id 
								LEFT JOIN webpsi_criticos.tecnicos tec ON tec.id=c.idtecnico 
								INNER JOIN webpsi_criticos.empresa e ON e.id=c.id_empresa 
								INNER JOIN webpsi_criticos.horarios h ON h.id=c.id_horario 
								INNER JOIN webpsi_criticos.zonales z ON z.id=c.id_zonal 
								INNER JOIN webpsi_criticos.dias d ON d.id=c.id_dia 
								INNER JOIN webpsi_criticos.motivos m ON m.id=c.id_motivo 
								INNER JOIN webpsi_criticos.submotivos s ON s.id=c.id_submotivo 
								INNER JOIN webpsi_criticos.estados es ON es.id=c.id_estado 
								INNER JOIN webpsi.tb_usuario t ON c.id_usuario=t.id 
								LEFT JOIN	webpsi_criticos.liquidados l on c.id_gestion=l.id_gestion
								WHERE (l.id in (	SELECT max(l2.id) 
											 		FROM webpsi_criticos.liquidados l2 
													WHERE l2.id_gestion=c.id_gestion
											   )
										OR l.id IS NULL
									  )
								$filtrop
						
				UNION ALL
						
								SELECT c.id,cri.id_atc,e.nombre eecc_final
									,(CASE c.id WHEN (	SELECT MAX(mov2.id) 
														FROM webpsi_criticos.gestion_movimientos mov2
														WHERE mov2.id_gestion=c.id_gestion
													 ) 
									THEN 'X' ELSE '' END) 'ultimo_movimiento',tipo_actividad,a.averia,a.codmotivo_req_catv,a.quiebre
								,nombre_cliente_critico,telefono_cliente_critico,celular_cliente_critico,c.observacion,c.fecha_agenda
								,horario,d.dias, e.nombre,z.zonal,m.motivo,s.submotivo,es.estado,tec.nombre_tecnico tecnico,a.fecha_registro,fecha_creacion
								,fecha_movimiento,t.usuario,es.id AS 'e_id',c.fecha_consolidacion,tipo_averia,averia_m1,l.penalizable,a.mdf
								,'' AS fecha_cambio
								FROM webpsi_criticos.gestion_rutina_manual a 
								INNER JOIN webpsi_criticos.gestion_criticos cri ON a.id_gestion=cri.id 
								INNER JOIN webpsi_criticos.gestion_movimientos c ON c.id_gestion=cri.id 
								LEFT JOIN webpsi_criticos.tecnicos tec ON tec.id=c.idtecnico 
								INNER JOIN webpsi_criticos.empresa e ON e.id=c.id_empresa 
								INNER JOIN webpsi_criticos.horarios h ON h.id=c.id_horario 
								INNER JOIN webpsi_criticos.zonales z ON z.id=c.id_zonal 
								INNER JOIN webpsi_criticos.dias d ON d.id=c.id_dia 
								INNER JOIN webpsi_criticos.motivos m ON m.id=c.id_motivo 
								INNER JOIN webpsi_criticos.submotivos s ON s.id=c.id_submotivo 
								INNER JOIN webpsi_criticos.estados es ON es.id=c.id_estado 
								INNER JOIN webpsi.tb_usuario t ON c.id_usuario=t.id 
								LEFT JOIN webpsi_criticos.liquidados l on c.id_gestion=l.id_gestion
								WHERE (l.id in (	SELECT max(l2.id) 
													FROM webpsi_criticos.liquidados l2 
													WHERE l2.id_gestion=c.id_gestion
											   )
										OR l.id IS NULL
									  )
								$filtroa
						
				UNION ALL 
						
								SELECT c.id,cri.id_atc,e.nombre eecc_final
									,(CASE c.id WHEN (	SELECT MAX(mov2.id) 
														FROM webpsi_criticos.gestion_movimientos mov2
														WHERE mov2.id_gestion=c.id_gestion
													 ) 					
									 THEN  'X' ELSE '' END) 'ultimo_movimiento',tipo_actividad,p.codigo_req AS 'averia'
								,p.tipo_motivo as 'codmotivo_req_catv',p.quiebre,nombre_cliente_critico,telefono_cliente_critico
								,celular_cliente_critico,c.observacion, c.fecha_agenda,horario,d.dias, e.nombre,z.zonal,m.motivo
								,s.submotivo,es.estado,tec.nombre_tecnico tecnico,p.fecha_Reg AS 'fecha_registro',fecha_creacion,fecha_movimiento,t.usuario
								,es.id AS 'e_id',c.fecha_consolidacion,origen AS 'tipo_averia',aver_m1 AS 'averia_m1',l.penalizable,p.mdf
								,(SELECT fecha_cambio 
								 FROM webpsi_coc.`tmp_provision_historico` pch 
								 WHERE pch.codigo_req=p.codigo_req ) AS fecha_cambio
								FROM webpsi_criticos.gestion_rutina_manual_provision p 
								INNER JOIN webpsi_criticos.gestion_criticos cri ON p.id_gestion=cri.id 
								INNER JOIN webpsi_criticos.gestion_movimientos c ON c.id_gestion=cri.id 
								LEFT JOIN webpsi_criticos.tecnicos tec ON tec.id=c.idtecnico 
								INNER JOIN webpsi_criticos.empresa e ON e.id=c.id_empresa 
								INNER JOIN webpsi_criticos.horarios h ON h.id=c.id_horario 
								INNER JOIN webpsi_criticos.zonales z ON z.id=c.id_zonal 
								INNER JOIN webpsi_criticos.dias d ON d.id=c.id_dia 
								INNER JOIN webpsi_criticos.motivos m ON m.id=c.id_motivo 
								INNER JOIN webpsi_criticos.submotivos s ON s.id=c.id_submotivo 
								INNER JOIN webpsi_criticos.estados es ON es.id=c.id_estado 
								INNER JOIN webpsi.tb_usuario t ON c.id_usuario=t.id 
								LEFT JOIN	webpsi_criticos.liquidados l on c.id_gestion=l.id_gestion
								WHERE (l.id in (	SELECT max(l2.id) 
											 		FROM webpsi_criticos.liquidados l2 
													WHERE l2.id_gestion=c.id_gestion
											   )
										OR l.id IS NULL
									  )
								$filtrop
						
				)AS t1  
				ORDER BY $ordenar ";

//echo $sql;
       $arr = array();
       $res = $cnx->query($sql); 
        while ($row = $res->fetch(PDO::FETCH_ASSOC))
        {
            $arr[] = $row;
        }
        return $arr;
        
                }


	function updateTecnicoMovimientos($cnx,$tecnico,$codigos,$idtecnico){
		
            $cnx->exec("set names utf8");

			//ya que solo quiero actualizar el ultimo movimiento y no se puede hacer una subconsulta en el where
			//de un update lo hago por separado
			$cad1 = "SELECT max(fecha_movimiento) 'fechas' FROM webpsi_criticos.gestion_movimientos
					 group by id_gestion having id_gestion in ($codigos)";
					 
			$res1 = $cnx->query($cad1);
			$fechas = "";
			while ($row = $res1->fetch(PDO::FETCH_ASSOC))
	        {
	            $fechas .= "'".$row["fechas"]."',";
	        }
	        $fechas = substr($fechas,0,strlen($fechas)-1);
			$cad = "update webpsi_criticos.`gestion_movimientos` set 
					tecnico='$tecnico',idtecnico='$idtecnico' where fecha_movimiento in ($fechas)";
			$res = $cnx->exec($cad);

            return $res;
	}

	function updateEmpresaMovimientos($cnx,$codigos,$empresa,$tecnico,$idtecnico){
		
            $cnx->exec("set names utf8");
			$cad = "update webpsi_criticos.`gestion_movimientos` 
					set id_empresa='$empresa'
					,tecnico='$tecnico'
					,idtecnico='$idtecnico' 
					where id_gestion in($codigos)";
			$res = $cnx->exec($cad);			

            return $res;
	}

}

?>
