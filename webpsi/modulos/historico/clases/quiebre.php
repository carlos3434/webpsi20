<?php

$PATH = $_SERVER['DOCUMENT_ROOT'] . "/webpsi/";
require_once($PATH . '/clases/class.Conexion.php');


class Quiebre{

    protected $cnx;

    public function __construct() {
        $db = new Conexion();
        $cnx = $db->conectarPDO();
        $this->cnx = $cnx;
    }

	function getQuiebre($cnx,$usu){
        $cnx->exec("set names utf8");

        $sql=" select *
                from tb_usuario_quiebre
                where id_usuario='$usu'
                and cestado='1'";
        $res = $cnx->query($sql);
        $r=array();

        while ($row = $res->fetch(PDO::FETCH_ASSOC))
        {
            $r[] = $row;
        }

            if(count($r)>0){
                $sql = "select q.apocope as id,q.nombre
                        from tb_usuario_quiebre uq 
                        inner join tb_quiebre q on uq.id_quiebre=q.id_quiebre
                        WHERE uq.id_usuario='$usu'
                        and q.cestado='1'
                        and uq.cestado='1'
                        order by 1";
            }
            else{
                $sql = "select apocope as id,nombre
                        from tb_quiebre 
                        where cestado='1'
                        order by 1";
            }
        
        
        $res = $cnx->query($sql);

        while ($row = $res->fetch(PDO::FETCH_ASSOC))
        {
            $arr[] = $row;
        }
        return $arr;
	}

    /*
     * Todos los quiebres que estan como activos
     */
    public function getQuiebresAll()
    {
        $this->cnx->exec("set names utf8");

        $sql=" select *  from webpsi.tb_quiebre where cestado = 1";

        $res = $this->cnx->query($sql);
        while ($row = $res->fetch(PDO::FETCH_ASSOC))
        {
            $arr[] = $row;
        }

        return $arr;
    }


    public function getQuiebresAllOfficeTrack()
    {
        $this->cnx->exec("set names utf8");

        $sql="  SELECT GROUP_CONCAT(CONCAT(q.apocope,'*',a.nombre)) id_quiebre,
                CONCAT(q.nombre,' - (',GROUP_CONCAT(a.nombre SEPARATOR ','),')') nombre
                from webpsi.tb_quiebre q
                INNER JOIN webpsi_criticos.actividad_quiebre aq ON (q.id_quiebre=aq.id_quiebre)
                INNER JOIN webpsi_criticos.actividad a ON (a.id_actividad=aq.id_actividad)
                WHERE q.cestado = 1
                AND aq.estado=1
                AND a.estado=1
                GROUP BY q.id_quiebre
                ORDER BY nombre";

        $res = $this->cnx->query($sql);
        while ($row = $res->fetch(PDO::FETCH_ASSOC))
        {
            $arr[] = $row;
        }

        return $arr;
    }

    /*
     * Optiene todos los quiebres activos o inactivos
     */
    public function getQuiebres()
    {
        $this->cnx->exec("set names utf8");

        $sql="select q.* , aq.actividades  from webpsi.tb_quiebre q
                LEFT JOIN
                (select GROUP_CONCAT(id_actividad) actividades , id_quiebre from webpsi_criticos.actividad_quiebre
                GROUP BY id_quiebre)
                 aq on aq.id_quiebre = q.id_quiebre";

        $res = $this->cnx->query($sql);
        while ($row = $res->fetch(PDO::FETCH_ASSOC))
        {
            $arr[] = $row;
        }

        return $arr;
    }


    public function quiebresPorTecnico($idtecnico = "")
    {
        if(empty($idtecnico))
            return false;

        $this->cnx->exec("set names utf8");

        $sql=" select idquiebre from webpsi_criticos.tecnico_quiebre
              where idtecnico = '$idtecnico' and estado = 1";

        $res = $this->cnx->query($sql);
        while ($row = $res->fetch(PDO::FETCH_ASSOC))
        {
            $arr[] = $row["idquiebre"];
        }

        if(count($arr)<= 0){
            return false;
        }

        return $arr;
    }

    public function comboQuiebres($idtecnico = "")
    {
        $quiebres = $this->getQuiebresAll();

        $options = "";
        $selected_quiebres = $this->quiebresPorTecnico($idtecnico);


        foreach($quiebres as $quiebre)
        {
            if($selected_quiebres)
            {   $selected = false;
                if(in_array($quiebre["id_quiebre"] ,$selected_quiebres )){
                    $selected = true;
                }
            }
            $deb = 1;
            $options.= $this->comboQuiebreOption($quiebre["id_quiebre"],$quiebre["nombre"],$selected);
        }
        return $options;
    }

    public function comboQuiebreOption($key,$value,$selected = false)
    {
        $selected  = ($selected)?"selected": "";
        return "<option value='$key' $selected>" . $value . "</option>";

    }

    public function UpdateEstado($id_quiebre , $estado){

        try{

            $this->cnx->beginTransaction();

            $rows = $this->cnx->exec("UPDATE webpsi.tb_quiebre SET cestado=$estado where id_quiebre = $id_quiebre");
            $deb  = 1;
            $this->cnx->commit();
            return $rows;

        }catch (PDOException $e){
            $this->cnx->rollBack();
            return $e->getMessage();
        }


    }

    public  function CrearQuiebre($nombre, $apocope,$estado,$actividades = "")
    {
        try{

            $this->cnx->beginTransaction();

            $this->cnx->exec("Insert into  webpsi.tb_quiebre SET cestado=$estado , nombre = '$nombre' , apocope ='$apocope' ");
            $insertId = $this->cnx->lastInsertId();

            //Agregando actividades
            if($actividades != ""){
                $acti  = explode(",",$actividades);
                foreach($acti as $value){
                    $this->cnx->exec("Insert into webpsi_criticos.actividad_quiebre SET estado=1 ,id_quiebre = $insertId , id_actividad = $value ");
                }
            }

            $this->cnx->commit();

            return 1;

        }catch (PDOException $e){
            $this->cnx->rollBack();
            return $e->getMessage();
        }
    }

    public  function EditarQuiebre($id , $nombre, $apocope,$estado,$actividades = "")
    {
        try{

            $this->cnx->beginTransaction();

            $this->cnx->exec("Update   webpsi.tb_quiebre SET cestado=$estado , nombre = '$nombre' , apocope ='$apocope' where id_quiebre = $id ");


            //Agregando actividades
            $this->cnx->exec("Delete from webpsi_criticos.actividad_quiebre where id_quiebre = $id ");
            if($actividades != ""){
                $acti  = explode(",",$actividades);
                foreach($acti as $value){
                    $this->cnx->exec("Insert into webpsi_criticos.actividad_quiebre SET estado=1 ,id_quiebre = $id , id_actividad = $value ");
                }
            }

            $this->cnx->commit();

            return 1;

        }catch (PDOException $e){
            $this->cnx->rollBack();
            return $e->getMessage();
        }
    }

}

?>