/**
 * Created by root on 02/12/14.
 */

//EVENTOS JAVASCRIPT SOLO PARA GESTION DIGITALIZACION

$(function(){

    //FIL CELULA DENTRO DE GESTION DIGITALIZACION SOLO

    $(".tecnico").multiselect()
    $(".tecnico").multiselect().multiselectfilter();
    $("#tecnico").multiselect("uncheckAll");

    $("#slct_estado_coordinado").multiselect({
        multiple: false,
        header: "Seleccione opcion",
        noneSelectedText: "Select an Option",
        selectedList: 1
    });
    $("#slct_estado_legado").multiselect({
        multiple: false,
        header: "Seleccione opcion",
        noneSelectedText: "Select an Option",
        selectedList: 1
    });



    $("#empresa").multiselect({
        click: function(event, ui) {
            var empresa = ui.value;
            var estado = ui.checked;
            var self = this

            if(estado === true){
                //se agregan las celulas de la empresa
                AgregarCelulas(empresa)
            }else{
                QuitarCelulas(empresa)
            }
            AgregarTecnicosPorCelulasHabilitadas()
            $("#fil_celula").multiselect("refresh")

        },
        checkAll: function(e){
            $("#fil_celula option").remove();
            AgregarCelulasPorDefecto();
            $("#fil_celula").multiselect("refresh")
            AgregarTecnicosPorCelulasHabilitadas()

        },
        uncheckAll: function(e){

            $("#fil_celula option").remove();
            $("#fil_celula").multiselect("refresh");
            AgregarTecnicosPorCelulasHabilitadas()
        }
    });

    //$(".fil_celula").multiselect()
    $(".fil_celula").multiselect({
        click: function(event, ui) {
            var celula = ui.value;
            var estado = ui.checked;
            var self = this

            //if(estado === true){
            //    //se agregan las celulas de la empresa
            //    AgregarTecnicos(celula)
            //
            //}else{
            //    QuitarTecnicos(celula)
            //}

            AgregarTecnicosDeCelulasSeleccionadas()
            $("#tecnico").multiselect("refresh")

        },
        checkAll: function(e){
            //$("#tecnico option").remove();
            //AgregarTecnicosPorSeleccionMultiple()
            AgregarTecnicosDeCelulasSeleccionadas();


        },
        uncheckAll: function(e){


            AgregarTecnicosPorCelulasHabilitadas()

        }
    }).multiselectfilter();

    AgregarDatosPorDefecto();

});


function AgregarDatosPorDefecto(){

    AgregarCelulasPorDefecto()
    AgregarTecnicosPorCelulasHabilitadas();
}

function AgregarCelulasPorDefecto(){

    var empresas_selectes = $("#empresa").multiselect("getChecked").map(function(){   return this.value;    }).get();

    _.each( empresas_selectes , function(item){
        AgregarCelulas(item);
    });

    $("#fil_celula").multiselect("refresh")
}

function AgregarCelulas(empresa){
    var celulas = _.where(Data.celulas,{empresa:empresa})

    _.each(celulas, function(item ){
        tr = "<option value='"+ item.id+"' emp='"+item.empresa+"'>"+ item.nombre + "</option>";
        $("#fil_celula").append(tr);
    })

}

function QuitarCelulas(empresa){

    $("#fil_celula option[emp='"+empresa+"']").remove()
    $("#fill_celula").multiselect("refresh")


}

function AgregarTecnicos(celula){

    var tecnicos = _.where(Data.tecnicos,{idcedula:celula})

    _.each(tecnicos, function( item ){
        tr = "<option value='"+ item.carnet_critico+"' cel='"+item.idcedula+"'>"+ item.carnet_critico +' '+ item.nombre_tecnico + "</option>";
        $("#tecnico").append(tr);
    })

}

function QuitarTecnicos(celula){

    $("#tecnico option[cel='"+celula+"']").remove()
    $("#tecnico").multiselect("refresh");
}

function AgregarTecnicosPorSeleccionMultiple(){

 var celulas_selected = $("#fil_celula").multiselect("getChecked").map(function(){   return this.value;    }).get();
    _.each(celulas_selected, function(item){        AgregarTecnicos(item)        });
    $("#tecnico").multiselect("refresh");

}

function AgregarTecnicosPorCelulasHabilitadas(){
    $("#tecnico option").remove();
    var celulas_selected = $("#fil_celula option").not(":selected").map(function(){  return $(this).val() }).get()

    _.each(celulas_selected, function(item){        AgregarTecnicos(item)        });
    ElimiarDuplicados()
    $("#tecnico").multiselect("refresh")

}


function AgregarTecnicosDeCelulasSeleccionadas(){

    $("#tecnico option").remove();
    var celulas_selected = $("#fil_celula").multiselect("getChecked").map(function(){   return this.value;    }).get();
    _.each(celulas_selected, function(item){        AgregarTecnicos(item)        });
    ElimiarDuplicados()
    $("#tecnico").multiselect("refresh");
}

function ElimiarDuplicados(){
    $("#tecnico option").each(function(){  if( $("#tecnico option[value='"+  $(this).val().trim()+"']").length > 1 ){$(this).remove() } })
}