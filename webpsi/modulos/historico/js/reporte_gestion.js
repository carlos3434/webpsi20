var url_ajax2 = "reporte_gestion_ajax.php";

$(document).ready(function() {
    var oTable = $('#clienteCriticos').dataTable({
        aaSorting:[],
        aoColumnDefs: [
          {
             bSortable: false,
             aTargets: [ 0 ]
          }
        ],                  
        "iDisplayLength": 10,
        "bPaginate": true,
        "aLengthMenu": [[50, 100, 200, -1], [50, 100, 200, "All"]],
        "sDom": 'T<"clear">lfrtip',
        "dom": 'T<"clear">lfrtip',
        "tableTools": {
            "sSwfPath": "swf/copy_csv_xls_pdf.swf"
        }
    });

    if($( "#fechaIni" ).length == 1){
        $( "#fechaIni" ).datepicker({maxDate: "+0D"});
        $( "#fechaIni" ).datepicker("option", "dateFormat", "dd/mm/yy");
        $("#fechaIni").val(fecha.diaHoy);
        $( "#fechaFin" ).datepicker({maxDate: "+0D"});
        $( "#fechaFin" ).datepicker("option", "dateFormat", "dd/mm/yy");
        $("#fechaFin").val(fecha.diaHoy);

        var wigdet_fechaIniAgenda =  $( "#fechaIniAgenda" );
        wigdet_fechaIniAgenda.datepicker({maxDate: "+0D"});
        wigdet_fechaIniAgenda.datepicker("option", "dateFormat", "dd/mm/yy");
        wigdet_fechaIniAgenda.val(fecha.diaMesFirst);

    }

    $("#mostrar").click(function(){

        var fechaIni = $("#fechaIni").val();
        var fechaFin = $("#fechaFin").val();
        var fechaIniAgenda = $("#fechaIniAgenda").val();
        var fechaFinAgenda = $("#fechaFinAgenda").val();

        $.ajax({
            type: "POST",
            url: url_ajax2,
            data: {
                accion:"gestion_criticos_date",
                fechaIni:fechaIni,
                fechaFin:fechaFin
            },
            dataType: "Json",
            beforeSend: function(){
                $('.modalPop').show();  
            },
            complete: function(){
                $('.modalPop').hide();
            },
            success: function (data) {
                //if(data!==null){
                    oTable.fnClearTable();
                    
                    var tabla = "";
                    $.each(data, function (i, info) {

                        if(info.fecha_reg!==''){
                            fecha_reg = info.fecha_reg.substr(8,2)+"-"+info.fecha_reg.substr(5,2)+"-"+info.fecha_reg.substr(0,4);
                            fecha_reg += "<br>"+info.fecha_reg.substr(11,8);
                        }else{
                            fecha_reg = "";
                        }
                        if(info.fecha_agenda!==''){
                            fecha = info.fecha_agenda.substr(8,2)+"-"+info.fecha_agenda.substr(5,2)+"-"+info.fecha_agenda.substr(0,4);
                        }else{
                            fecha = "";
                        }
                        if(info.codigo_estado=="1" || info.codigo_estado=="9" || info.codigo_estado=="10" || info.codigo_estado=="20" || info.codigo_estado=="8"){
                            fecha_horario = fecha+"<br>"+info.horario;
                        }else{
                            fecha_horario = "";
                        }
                        pai="";
                        if(info.area2=="PAI"){
                            pai = '<img src="img/info_1.png" alt="PAI" title="PAI" />';
                        }
                        img_Averia = '<img src="img/averia.png" alt="Mostrar Avería" title="Mostrar Avería" />';
                        existe='';
                        if(info.estado!="Temporal"  && info.tipo_actividad!="Manual"){
                            if(info.existe=="No"){
                                existe = '<img src="img/info_2.png" alt="No esta Pendiente" title="No esta Pendiente" />';
                            }
                            img_mov = '<img src="img/mov.jpg" alt="Mostrar Movimiento" title="Mostrar Movimiento" />';
                            
                        }else if(info.estado!="Temporal"  && info.tipo_actividad=="Manual"){
                                img_mov = '<img src="img/mov.jpg" alt="Mostrar Movimiento" title="Mostrar Movimiento" />';
                        }else{
                            img_mov = "";
                        }
                        if(info.codigo_estado!="21"){
                            img_gestion = '<img src="img/gestionar.png" alt="Gestionar" title="Gestionar" />';
                        }else{
                            img_gestion = "";
                        }           
                        //Flag tecnico
                        if(info.codigo_estado!="21"){
                            if ( info.flag_tecnico=="Tecnico Asignado" ) {
                                imgTecnicoEstado = '<img src="img/user-yellow.png" alt="Tecnico Asignado" title="Tecnico Asignado" />';
                            } else if (info.flag_tecnico=="Tecnico Entregado" ) {
                                imgTecnicoEstado = '<img src="img/user-green.png" alt="Tecnico Entregado" title="Tecnico Entregado" />';
                            }else{
                                imgTecnicoEstado = '<img title="Sin tecnico" alt="Sin tecnico" src="img/user-clean.png">';
                            }
                        }else{
                            imgTecnicoEstado = "";
                        }
                        var imgn_evento='';
                        var imagen_evento="";
                        if(info.n_evento=="1"){
                            if($.trim(info.estado_evento)!==''){
                                if(info.estado_evento.split("-")[0]=='0001'){
                                    imagen_evento='<img src="img/verde.jpg" alt="'+info.estado_evento.split("-")[1]+'" />';
                                }
                                else if(info.estado_evento.split("-")[0]=='0002'){
                                    imagen_evento='<img src="img/amarillo.jpg" alt="'+info.estado_evento.split("-")[1]+'" />';
                                }
                                else if(info.estado_evento.split("-")[0]=='0003'){
                                    imagen_evento='<img src="img/azul.jpg" alt="'+info.estado_evento.split("-")[1]+'" />';
                                }
                            }
                            imgn_evento=imagen_evento+'<img src="img/cel1.png" alt="Transmision de informacion" />';
                        }

                        rtn = oTable.fnAddData([  info.averia, info.id_atc, info.tipo_actividad,
                            '<span style="display: none;">' + info.fecha_reg + '</span>'+fecha_reg,
                            info.quiebres, $.trim(info.averia_m1).replace("MOVISTAR UNO","M1"), info.empresa,
                            info.telefono_cliente_critico, fecha_horario, info.estado, info.tecnico, /*info.mdf,
                            info.microzona,*/ info.distrito, img_mov + '<span class="nmov'+info.id+'">(' + info.nmov + ')',
                            imgn_evento, info.tipo_averia, info.horas_averia, info.fecha_registro, info.ciudad,
                            info.codigo_averia, info.inscripcion, info.fono1, info.telefono,info.mdf, info.observacion_102,
                            info.segmento, info.area_, info.direccion_instalacion, info.codigo_distrito, info.nombre_cliente,
                            info.orden_trabajo, info.veloc_adsl, info.clase_servicio_catv, info.codmotivo_req_catv,
                            info.total_averias_cable, info.total_averias_cobre, info.total_averias, info.fftt,info.llave,
                            info.dir_terminal, info.fonos_contacto, info.contrata, info.zonal, info.quiebre, info.lejano,
                            info.distrito, info.eecc_final, info.zona_movistar_uno, info.paquete, info.data_multiproducto,
                            info.averia_m1, info.fecha_data_fuente, info.telefono_codclientecms, info.rango_dias, info.sms1,
                            info.sms2, info.area2, info.microzona, info.tecnico], false);

                        var oSettings = oTable.fnSettings();
                        var nTr = oSettings.aoData[ rtn[0] ].nTr;
                        nTr.setAttribute( 'data-indice', i );

                        $('td', nTr)[0].className = 'faveria_ini'+i;
                        $('td', nTr)[2].className = 'factividad'+i;
                        $('td', nTr)[5].className = 'fm1'+i;
                        $('td', nTr)[6].className = 'fempresa'+info.id;
                        
                        var ind = (info.id!=='')? info.id:i;
                        $('td', nTr)[7].className = 'ftele'+ind;
                        $('td', nTr)[8].className = 'fagenda'+info.id;
                        $('td', nTr)[9].className = 'festado'+info.id;
                        $('td', nTr)[10].className = 'ftecnico'+info.id;
                        
                        if(info.estado!="Temporal"){
                            $('td', nTr)[12].className = 'mostrar_mov';
                        }
                        $('td', nTr)[12].setAttribute('data-id', info.id );
                        $('td', nTr)[12].setAttribute('data-indice', i );
 
                        $('td', nTr)[13].className = 'transmision'+info.id+' transmision';
                        $('td', nTr)[13].setAttribute( 'data-id', info.id );
                        $('td', nTr)[13].setAttribute('data-actividad', info.tipo_actividad );


                        $('td', nTr)[14].style.display = 'none';                      
                        $('td', nTr)[15].style.display = 'none';
                                                $('td', nTr)[16].style.display = 'none';                      
                        $('td', nTr)[17].style.display = 'none';
                        $('td', nTr)[18].style.display = 'none';
                        $('td', nTr)[19].style.display = 'none';
                        $('td', nTr)[20].style.display = 'none';
                        $('td', nTr)[21].style.display = 'none';
                        $('td', nTr)[22].style.display = 'none';
                        $('td', nTr)[23].style.display = 'none';
                        $('td', nTr)[24].style.display = 'none';
                        $('td', nTr)[25].style.display = 'none';
                        $('td', nTr)[26].style.display = 'none';
                        $('td', nTr)[27].style.display = 'none';
                        $('td', nTr)[28].style.display = 'none';
                        $('td', nTr)[29].style.display = 'none';
                        $('td', nTr)[30].style.display = 'none';
                        $('td', nTr)[31].style.display = 'none';
                        $('td', nTr)[32].style.display = 'none';
                        $('td', nTr)[33].style.display = 'none';
                        $('td', nTr)[34].style.display = 'none';
                        $('td', nTr)[35].style.display = 'none';
                        $('td', nTr)[36].style.display = 'none';
                        $('td', nTr)[37].style.display = 'none';
                        $('td', nTr)[38].style.display = 'none';
                        $('td', nTr)[39].style.display = 'none';
                        $('td', nTr)[40].style.display = 'none';
                        $('td', nTr)[41].style.display = 'none';
                        $('td', nTr)[42].style.display = 'none';
                        $('td', nTr)[43].style.display = 'none';
                        $('td', nTr)[44].style.display = 'none';
                        $('td', nTr)[45].style.display = 'none';
                        $('td', nTr)[46].style.display = 'none';
                        $('td', nTr)[47].style.display = 'none';
                        $('td', nTr)[48].style.display = 'none';
                        $('td', nTr)[49].style.display = 'none';
                        $('td', nTr)[50].style.display = 'none';
                        $('td', nTr)[51].style.display = 'none';
                        $('td', nTr)[52].style.display = 'none';
                        $('td', nTr)[53].style.display = 'none';
                        $('td', nTr)[54].style.display = 'none';
                        $('td', nTr)[55].style.display = 'none';
                        $('td', nTr)[56].style.display = 'none';
                        $('td', nTr)[57].style.display = 'none';
                        //$('td', nTr)[58].style.display = 'none';
                        //$('td', nTr)[59].style.display = 'none';

                    });
                    oTable.fnDraw();
                    $("#seleccion_general").removeAttr('checked');//para el check de la lista

                    $('#clienteCriticos').delegate('td.registro_criticos','click', function(){
                    
                        var fonoBus = $(this).attr("data-telefono");
                        var indice = $(this).attr("data-indice");
                        var actividad = $(this).attr("data-actividad");
                        var averia_ini = $(".faveria_ini"+indice).html();
                        var url;
                        
                        if(actividad=='Provision'){
                            url = "registro_clientes_criticos_provision.php?averia_ini="+averia_ini+"&actividad="+actividad+"&indice="+indice;
                        }else{
                            url = "registro_clientes_criticos.php?averia_ini="+averia_ini+"&actividad="+actividad+"&indice="+indice;
                        }
                        var pagina = '<iframe style="border: 0px; " src="' + url + '" width="100%" height="100%"></iframe>';
                        $("#dialog-criticos").html(pagina);
                        $("#dialog-criticos").dialog({
                             autoOpen: false,
                             modal: true,
                             height: 600,
                             width: 700,
                        });

                        $("#dialog-criticos").dialog( "open" );                 
                    });

                    $('#clienteCriticos').delegate('td.sinacceso','click', function(){
                        var quiebre = $(this).attr("data-quiebre");
                        alert('No cuenta con permisos para el quiebre: '+quiebre);                  
                    });
                /*}else{
                    alert("No se ha especificado un dato de consulta")
                }*/         
                    ePrevious=$("#clienteCriticos_previous").attr("class");
                    eNext=$("#clienteCriticos_next").attr("class");     
            },
            error: function () {
                alert("No se ha especificado un dato de consulta");
            }
        });
    });
});