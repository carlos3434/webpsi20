function visualizarFiles(files){
    document.getElementById("fileOutput").style.display = "none";
    var file = files[0];
    var reader = new FileReader();
    var cabecerasProvision = ["fechorreg","codreq","codcli","codnod","indvip","codordtrab","codctr","codofcadm","desdtt","coddtt","codpvc","coddpt","tipreq","codmotv","nroplano"];
    var cabecerasAveria =["fechorreg","codreqatn","codcli","codnod","destipvia","desnomvia","numvia","destipurb","desurb","despis","desint","desmzn","deslot","coddtt","codordtrab","codclasrv","tipreq","codmotv","nroplano","desnomctr","codofcadm","desdtt"];
    
    reader.onload = function (event) {
        var textFile = event.target;
        var output = document.getElementById("fileOutput");
        var archivo = textFile.result;
        var arrayArchivo= archivo.split("\n");
        var cabecera = arrayArchivo[0].split("\t");
        var registro = arrayArchivo[1].split("\t");
        var datos = {};
        var buscador=0, index,i;
        servicio='provision';
        for (i = cabecerasProvision.length - 1; i >= 0; i--) {
            index = cabecerasProvision[i];
            buscador = cabecera.indexOf(index,0);
            if ( buscador>= 0) {
                datos[cabecerasProvision[i]] = registro[buscador];
            }
            else{
                servicio='averia';
                datos = {};
                break;
            }
        }
        if (servicio == 'averia') {
            for (i = cabecerasAveria.length - 1; i >= 0; i--) {
                index = cabecerasAveria[i];
                buscador = cabecera.indexOf(index,0);
                if (buscador >= 0) {
                    datos[cabecerasAveria[i]] = registro[buscador];
                }else{
                    servicio='';
                    datos = {};
                    break;
                }
            }
        }
        if (servicio === '') {
            alert('archivo no valido');
            return;
        }
        var codsrv=registro[cabecera.indexOf('codsrv')];
        HTMLcabecera = "";
        HTMLregistro = "";
        for(var indice in datos) {
            HTMLcabecera += "<th class='tabla_th'>"+indice+"</th>";
            HTMLregistro += "<td class='tabla_td'>"+datos[indice]+"</td>";
        }
        var form = new FormData();
        var url = "actualiza_quiebre_individual_ajax.php";
        var quiebre = $("#slct_quiebre").val(); //para validar si selecciono de acuerdo al archivo que subio
        form.append('codcli', datos.codcli);
        form.append('codsrv', codsrv);
        form.append('servicio', servicio);
        
        $.ajax({
            url: url,
            type: 'POST',
            data: form,
            contentType: false,
            processData: false,
            cache: false,
            dataType: "json",
            success: function(respAjax) {
                if(respAjax){
                    var strDatos = JSON.stringify(datos);
                    var apellidos = (respAjax.apellidos)? respAjax.apellidos:'';
                    var nombre = (respAjax.nombre)? respAjax.nombre:'';
                    var tipocalle = (respAjax.tipocalle)? respAjax.tipocalle:'';
                    var nomcalle = (respAjax.nomcalle)? respAjax.nomcalle:'';
                    var numcalle = (respAjax.numcalle)? respAjax.numcalle:'';
                        
                    HTML = "<table class='table_carga_indiv'><thead><tr>";
                    HTML +="<th class='tabla_th'> Apellidos y nombres </th>";
                    if (servicio=='provision')   HTML +="<th class='tabla_th'>direccion </th>";
                    HTML +=HTMLcabecera;
                    HTML +="</tr></thead>";
                    HTML +="<tbody><tr>";
                    HTML +="<td style='display: none' id='data' data='"+strDatos+"'></td>";
                    HTML +="<td class='tabla_td'>";
                    HTML +="<label class='etiqueta'>Ap.</label><input id='apellidos' value='"+apellidos+"'><br>";
                    HTML +="<label class='etiqueta'>Nom.</label><input id='nombre' value='"+nombre+"'>";
                    HTML +="</td>";
                    
                    if (servicio=='provision') {
                        HTML +="<td class='tabla_td'>";
                        HTML +="<label class='etiqueta'>Vía</label><input id='tipocalle' class='numero' value='"+ tipocalle +"'>";
                        HTML +="<label class='etiqueta'>Nom. </label><input id='nomcalle' value='"+ nomcalle +"'><br>";
                        HTML +="<label class='etiqueta'>N°</label><input id='numcalle' class='numero' value='"+ numcalle +"'>";
                        HTML +="<label class='etiqueta'>Piso</label><input id='piso' class='numero'>";
                        HTML +="<label class='etiqueta'>Int.</label><input id='interior' class='numero'>";
                        HTML +="<label class='etiqueta'>Mzn.</label><input id='manzana' class='numero'>";
                        HTML +="<label class='etiqueta'>Lote</label><input id='lote' class='numero'>";
                        HTML +="</td>";
                    }
                    HTML +=HTMLregistro;
                    HTML +="</tr></tbody></table>";
                    output.innerHTML = HTML;
                    $("#actividad").html("<p style='color:#FF0000; text-align: center; background: #EEF6FD; width: 100px; height: 20px; margin: auto; border-radius: 5px;'>"+servicio+"</p>");
                }
                else{
                    alert('ocurrio un error en la carga');
                }
            },
            error: function(respAjax) {
                console.log(respAjax);
            }
        });
    };
    reader.readAsText(file);
    document.getElementById("fileOutput").style.display = "block";
}
function GenerarActQuiebre(){
    if( $("#data").attr("data")!=='' && $("#slct_quiebre").val()!=='' && $("#file_tmp").val()!==''){
        var form = new FormData();
        var url = "actualiza_quiebre_individual_ajax.php";

        if(servicio=='provision'){
            form.append('tipocalle', $("#tipocalle").val() );
            form.append('nomcalle', $("#nomcalle").val() );
            form.append('numcalle', $("#numcalle").val() );
            form.append('piso', $("#piso").val() );
            form.append('interior', $("#interior").val() );
            form.append('manzana', $("#manzana").val() );
            form.append('lote', $("#lote").val() );
        }
        form.append('data', $("#data").attr("data") );
        form.append('apellidos', $("#apellidos").val() );
        form.append('nombre', $("#nombre").val() );
        form.append('quiebre',$("#slct_quiebre").val() );
        form.append('servicio',servicio );

        $.ajax({
            url: url,
            type: 'POST',
            data: form,
            contentType: false,
            processData: false,
            cache: false,
            dataType: "json",
            success: function(respAjax) {
                if(respAjax.estado){
                    alert('Se realizó con éxito');  
                }
                else{
                    alert('ocurrio un error en la carga');
                }
            },
            error: function(respAjax) {
                console.log(respAjax);
                alert('no se cargo archivo');
            }
        });
    }else if($("#slct_quiebre").val()===''){
        alert('Seleccione un quiebre');
    }
    else{
        alert('Busque y seleccione un archivo');
    }
}