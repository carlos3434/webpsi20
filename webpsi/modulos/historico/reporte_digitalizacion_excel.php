<?php
header("Content-Type:  application/x-msexcel");
header("Expires: 0"); 
header("Cache-Control: must-revalidate, post-check=0, pre-check=0"); 
header("Content-Disposition: attachment; filename=digitalizacion.xls");
header("Expires: 0");


include ("../../clases/class.Conexion.php");

$objCnx = new Conexion();

$cnx = $objCnx->conectarPDO();

$where="";
if($_GET["tipo"]=="0"){
$where=" WHERE contrata!='357'";
}
elseif($_GET["tipo"]=="357"){
$where=" WHERE contrata='357'";
}


//$grupo=array("contrata","d_nodo","d_troba");
if($_GET["grupo"]==""){
$_GET["grupo"]="''";
}
$grupo=explode(",",$_GET["grupo"]);
$dgrupo="contrata,".implode(",",$grupo);




//$cad = "SELECT * FROM webpsi_coc.averias_criticos_final ORDER BY 1 ASC ";
$cad = "SELECT $dgrupo,CONCAT($dgrupo) id,estado,count(contrata) cant, if(contrata='357','Post Digitalizacion','Digitalizacion') pos
		FROM webpsi_coc.pendientes_digitalizacion
		$where 
		GROUP BY $dgrupo,estado
		ORDER BY pos,$dgrupo,estado";
//echo $cad;
$res = $cnx->query($cad);
$array = $res->fetchAll(PDO::FETCH_ASSOC);

echo "<h2 style='text-align:center'><b>Reporte conteo de Post Digitalizacion VS Digitalizacion </b></h2>";
echo "<br>";
echo "<table border='1' cellpadding='0' cellspacing='0'>";

$htmlgrupo="";
$valor["P"]=0;
$valor["D"]=0;
$valor["S"]=0;
$valor["TP"]=0;
$valor["TD"]=0;
$valor["TS"]=0;

echo "<tr>";
echo "<th>Contrata</th>";
foreach ($grupo as $r) {
	echo "<th>".$r."</th>";
}
	echo "<th>Pendientes</th>";
	echo "<th>Devueltas</th>";
	echo "<th>Asignadas</th>";
	echo "<th>Tipo</th>";
echo "</tr>";

$id="";
echo "<tr>";

$auxcampo="";
$auxpos="";
foreach($array as $r){	
		if($id==''){
			$id=$r["id"];
			$auxpos="<td>".$r["pos"]."</td>";
			$auxcampo.="<td>".$r["contrata"]."</td>";
			foreach ($grupo as $d) {
				$auxcampo.="<td>".$r[$d]."</td>";								
			}
		}

		if($id!=$r["id"]){
			echo $auxcampo;
			echo "<td>".$valor["P"]."</td>";
			echo "<td>".$valor["D"]."</td>";
			echo "<td>".$valor["S"]."</td>";
			echo $auxpos;
			echo "</tr>";
			echo "<tr>";
			$auxcampo="";
			$auxpos="<td>".$r["pos"]."</td>";
			$auxcampo.="<td>".$r["contrata"]."</td>";
			foreach ($grupo as $d) {
				$auxcampo.="<td>".$r[$d]."</td>";				
			}
			$valor["P"]=0;
			$valor["D"]=0;
			$valor["S"]=0;
			$id=$r["id"];
		}

		$valor[strtoupper($r["estado"])]=$r["cant"];
		$valor["T".strtoupper($r["estado"])]+=$r["cant"];
		
}
	echo $auxcampo;
	echo "<td>".$valor["P"]."</td>";
	echo "<td>".$valor["D"]."</td>";
	echo "<td>".$valor["S"]."</td>";
	echo $auxpos;
	echo "</tr>";

	echo "<td colspan='".(count($grupo)+1)."' style='text-align:right'><b>Total</b></td>";
	echo "<td>".$valor["TP"]."</td>";
	echo "<td>".$valor["TD"]."</td>";
	echo "<td>".$valor["TS"]."</td>";
	echo "<td>&nbsp</td>";
	echo "</tr>";

echo '</table>';

?>
