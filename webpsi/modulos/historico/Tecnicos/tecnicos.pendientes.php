<?php
//ini_set('display_errors', 'On');
//error_reporting(E_ALL);
require_once("../../../cabecera.php");
require_once("../clases/class.TecnicosCriticos.php");
require_once('../clases/empresa.php');
include_once "../../../clases/class.Conexion.php";
require_once('../clases/cedula.php');
require_once '../../officetrack/clases/class.Usuario_eecc.php';

$db = new Conexion();
$cnx = $db->conectarPDO();
$Usuario_eecc =  new Usuario_eecc();
$empresa = new Empresa();
$cedula = new Cedula();
$tecnico = new TecnicosCriticos();

//ID usuario
$idUser = $_SESSION["exp_user"]["id"];

//Lista empresas
//$empArray = $Empresa->getEmpresa($db, "", "1");
$empArray = $Usuario_eecc->getEeccUser($cnx, $idUser);

$empresa->setCnx($cnx);
$empresa_options = $empresa->getEmpresaAllSelectOptions($idempresa);

$cedula->setCnx($cnx);
$cedula->setIdempresa($idempresa);
$celulas_options = $cedula->getCedulaAllByEmpresaSelectOptions();

$json_tecnicos_ot= $tecnico->TecnicosOfficetrackAll();

?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
    <title>WebPSI - Pendientes por Tecnico</title>
    <meta charset="utf-8">
    <meta name="author" content="Sergio MC"/>
    <?php include("../../../includes.php") ?>
    <script type="text/javascript" src="../js/js.js"></script>
    <link rel="stylesheet" type="text/css" href="../../../estilos.css">
    <link rel="stylesheet" type="text/css" href="../asistencia/asistenciaTecnicos.css">

    <script type="text/javascript" src="../js/jquery.filter_input.js"></script>
    <script type="text/javascript" src="../js/prettify.js"></script>
    <script type="text/javascript" src="../js/jquery.multiselect.min.js"></script>
    <script type="text/javascript" src="../js/jquery.multiselect.filter.js"></script>
    <link type="text/css" href='../css/jquery.multiselect.css' rel="Stylesheet" />
    <link type="text/css" href='../css/jquery.multiselect.filter.css' rel="Stylesheet" />



    <script src="../js/json2.js"></script>
    <script src="../js/underscore-min.js"></script>
    <script src="../js/backbone-min.js"></script>

    <script src="tecnicos.pendientes.js"></script>
<!--    <script src="../asistencia/asistenciaTecnicos.js"></script>-->
    <script>
        //cargo todos los tecnicos officetrack en json
        window.tecnicosOT = {}
        tecnicosOT = <?=$json_tecnicos_ot;?>;
        window.fecha = {};
        fecha.diaHoy = "<?=date("d/m/Y")?>";
        fecha.diaMesFirst = "<?=date("1/m/Y")?>";
    </script>
</head>
<body>

<input type="hidden" value="<?php echo $IDUSUARIO ?>" name="txt_idusuario" id="txt_idusuario"/>

<div id="page-wrap">
    <?php echo pintar_cabecera(); ?>    <br/>
    <div id="div_res_grupal" class="div_res_grupal"
         style="border: 1px solid #304B73; padding-top: 0px; float:left;
			 width: 100%;">


        <div id="filtros" class="form-group">


            <div>
                <div id="filtro_empresa" class="filtro-item" >
                    <div>
                    <label class="control-label">Empresa:</label>
                    <span>
                        <select class="fil_empresa" id="fil_empresa" name="fil_empresa" class="form-control">
                            <option value=''>-- Seleccione --</option>
                            <?php
                            foreach ($empArray["data"] as $key=>$val) {
                                $id = $val["ideecc"];
                                $nom = $val["eecc"];
                                echo "<option value=\"$id\">$nom</option>";
                            }
                            ?>

                        </select>
                    </span></div>
                    <div>
                    <label class="control-label">Celula:</label>
                    <span>
                        <select class="fil_celula" id="fil_celula" name="fil_celula" class="form-control">
                            <option value=''>-- Seleccione --</option>
                            <?= $celulas_options ?>

                        </select>
                    </span>
                        </div>
                    <div>
                    <label class="control-label">Estados</label>
                    <span>
                        <select name="tipo_estado" id="tipo_estado" multiple style="display: none">
                            <option value="28" selected="selected">Planificado</option>
                            <option value="2" selected="selected">Pendiente</option>
                            <option value="1" selected="selected" >Agendado con t&eacute;cnico</option>
                            <option value="8" selected="selected">Agendado sin t&eacute;cnico</option>
                            <option value="9,10,20" selected="selected">T&eacute;cnico en sitio</option>
                            <option value="21" >Cancelado</option>
                            <option value="4,5,6" >Devuelto</option>
                            <option value="27">Pre Liquidado</option>
                            <option value="3,19">Liquidado</option>
                        </select>
                    </span>
                        </div>
                    <div>
                        <h3>Fecha  Agendamiento</h3>
                        <label class="control-label">Desde</label>
                        <span><input type="text" class="fecha" id="fechaIniAgenda" /></span>


                    </div>
                </div>

                <div>

                    <button id="mostrar" style="padding: 5px;margin-left: 10px">Mostrar </button>
                    <button id="reiniciarFiltros" style="padding: 5px;margin-left: 10px">Reiniciar Filtros</button>
                    <span><a href="#" id="ExportExcel">
                            <img src="../img/excel2007.png" alt="" width="20px" style="vertical-align: bottom;"/>
                            Exportar a excel</a></span>
                </div>
            </div>

        </div>

        <div id="ListaTecnicosPendientes">
        <table id="table-tec">
            <tr class="cabecera">
                <th >Carnet</th>
                <th >Tecnico</th>
                <th   colspan="3">cantidad</th>
                <th  >Ver en Mapa</th>
            </tr>
            <tr>
                <th></th>
                <th></th>
                <th> < H</th>
                <th>H</th>
                <th>  H > </th>
                <th></th>
            </tr>
        </table>

        </div>


    </div>

    <div id="parentModal" style="display: none;">
        <div id="childModal" style="background: #fff;"></div>
        <div id="childModal_nuevo" style="background: #fff;"></div>


        <style>
            #table-tec {
                width: 100%;
                text-align: center;
            }
            #table-tec th {
                background: #ccc;
            }
            #table-tec td {
                border: 1px solid #ccc;
            }
            .detalle {
                cursor: pointer;
                padding: 10px;
            }
            .row-tec:hover {
                background: #aaccdd;
            }
            .row-tec.tareas{
                background: #ddffdf;
            }
            .active{
                font-weight: bold;
            }
            td.active {
                background: #eeffaa;
            }
            button.ui-multiselect, #tecnicos_ot {
                width: 200px !important;
            }
            .pen-0.active,
            .pen-0.tareas{
                background:#ffb1b1 ;
            }
            .pen-1.active,
            .pen-1.tareas{
                background:#f5ffb3 ;
            }
            .pen-2.active,
            .pen-2.tareas{
                background:#cdffb3   ;
            }
        </style>

</body>
</html>
