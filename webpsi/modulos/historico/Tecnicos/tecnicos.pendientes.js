var url_ajax2 = "tecnicos.pendientes.ajax.php";
var url2 = "tecnicos.pendientes.php";
$().ready(function(){

    $("#tipo_estado").multiselect({});

    if($("#fil_empresa").val() == ""){
        $("#fil_celula").attr("disabled","disabled");
    }else{
        $("#fil_celula").attr("disabled","");
    }

    $("#fil_empresa").change(function(){

        if($(this).val() == ""){
            $("#fil_celula").attr("disabled",true);
        }else{
            $("#fil_celula").attr("disabled",false);
        }
    });

    var wigdet_fechaIniAgenda =  $( "#fechaIniAgenda" )
    wigdet_fechaIniAgenda.datepicker({maxDate: "+0D"});
    wigdet_fechaIniAgenda.datepicker("option", "dateFormat", "dd/mm/yy");
    wigdet_fechaIniAgenda.val(fecha.diaMesFirst);



    $("#mostrar").click(function(){

       var idemp = $("#fil_empresa").val();
       var idcel = $("#fil_celula").val();
        var estados = "";
        if($("#tipo_estado").val() != null){
            estados = $("#tipo_estado").val().join();
        }

        //TECNICOS A MOSTRAR
        var tec = _.where(tecnicosOT,{idcedula:idcel});
        var carnets = _.map(tec , function(item){ return item.carnet_critico }).join("','");

        var fechaIniAgenda = $("#fechaIniAgenda").val();

         $.ajax({
                type: "POST",
                url: url_ajax2,
                dataType : "json",
                data: {
                    accion:"MostrarReportePendientes",
                    idemp:idemp,
                    idcel:idcel,
                    estados:estados,
                    fechaIni:fechaIniAgenda,
                    carnets : carnets
                },
                success: function (obj) {
                    $(".row-tec").remove();
                    if(obj != null){
                        if(obj.estado == false ||  obj.data.length ==0 ){
                            this.error()
                        }else{
                            mostrarTabla(obj.data);
                        }
                    }else{
                        this.error()
                    }

                },
                error: function () {
                    alert("No se encontraron datos a mostrar");
                    $(".row-tec").remove();
                }
            });




    });

   verDetalle();

    //FILTRO EMPRESA
    $("#fil_empresa").change(function () {
        var idempresa = $(this).val();
        $("#fil_celula option").hide();
        $("#fil_celula option").attr("selected",false);
        if(idempresa != ""){
            $("#fil_celula option[idemp="+idempresa+"]").show();
            $("#fil_celula option").eq(0).show()
            //$("#fil_celula option").eq(0).text("Todos " + $("#fil_empresa option:selected").text());
            //actualizamos tecnicos
            $("#tecnicos_ot option").remove();

        }else{
            //$("#fil_celula option").eq(0).text(" -- Todos --");
            $("#fil_celula option").show();

            //actualizamos tecnicos
            $("#tecnicos_ot option").remove();

        }
    });


    $("#fil_celula").change(function () {
        var idcelula = $(this).val();

        if(idcelula!= ""){
            //removemos todos los tecnicos
            $("#tecnicos_ot option").remove();
            //agregamos los tecnicos correspondientes

        }else{
            //revisamos si empresaa estaba seleccionado
            var idempresa = $("#fil_empresa").val();
            if(idempresa != ""){

            }else{

            }

        }

    });

    $("#reiniciarFiltros").click(function(){
        $("#fil_empresa").val("")
        $("#fil_empresa").trigger("change");
        $("#asistenciaFecha").val(fecha.diaHoy);
        $("#asistenciaTecnicos").html("")
    });


    $("#ExportExcel").click(function(){
        //EXPORTAR EXCEL
        //DATOS A ENVIAR
        var idemp = $("#fil_empresa").val();
        var idcel = $("#fil_celula").val();
        var estados = "";
        if($("#tipo_estado").val()!= null){
            estados = $("#tipo_estado").val().join();
        }

        //TECNICOS A MOSTRAR
        var tec = _.where(tecnicosOT,{idcedula:idcel});
        var carnets = _.map(tec , function(item){ return item.carnet_critico }).join("','");

        var fechaIniAgenda = $("#fechaIniAgenda").val();


        if(idemp!="" && idcel != "" && estados != "" ) {

            var accion = "ExportExcel"
            window.open(url_ajax2+"?" +
                "accion="+accion
                + "&excel=1"
                + "&idemp="+idemp
                + "&idcel="+idcel
                + "&estados="+estados
                + "&fechaIni="+fechaIniAgenda
                + "&carnets="+carnets
            );

        }else{
            alert("Debe seleccionar una contrata y Celula");
        }




    });



});

function mostrarTabla(data){

    $(".row-tec").remove();
    var tecnicos = _.groupBy(data,"carnet_critico");
    var tr = "";

    var estados = "";
    if($("#tipo_estado").val()!= null){
        estados = $("#tipo_estado").val().join();
    }

   //MOSTRAMOS LISTADO DE TECNICOS
    _.each(tecnicos,function(tareas, key, list){

        tr = "<tr class='row-tec  tec main-"+key+"' key='"+key+"'>" +
            "<td>" + key  + "</td>" +
            "<td>" + tareas[0].tecnico  + "</td>" +
            "<td class='detalle pen-0' key='"+key+"' pendiente='0'>"+ _.where(tareas,{pendiente:"0"}).length +"</td>" +
            "<td class='detalle pen-1' key='"+key+"' pendiente='1'>"+ _.where(tareas,{pendiente:"1"}).length +"</td>" +
            "<td class='detalle pen-2' key='"+key+"' pendiente='2'>"+ _.where(tareas,{pendiente:"2"}).length +"</td>" +
            "<td> <span class='mapa'> " +
        "<a class='ver-mapa' key='todos' href='#'>Todos</a> " +
        "<a class='ver-mapa' key='pasados' href='#'>Pasados</a> " +
        "<a class='ver-mapa' key='hoy' href='#'>Hoy</a> " +
        "<a class='ver-mapa' key='futuros' href='#'>Futuros</a> " +
        "</span> </td>" +
        "</tr>";

        tareas.forEach(function(value){

            var todo =  _.reduce(value, function(memo, num){ return memo + " , "+num; });
            tr += "<tr class='row-tec tareas  sub-"+key+" pen-"+value.pendiente+" pendiente-"+value.pendiente+"' tarea='"+value.id_atc+"'  style='display:none'>" +
                "<td>" +
            "<a target='_blank' " +
            "href='/webpsi/modulos/public/rutaTecnico/?cc="+key+"&tareas="+value.id_atc+"&estados="+estados+"&sin_fecha=1'>"+
            value.id_atc+"</a> </td>" +
                "<td>"+ value.fecha_agenda + "/"+value.horario +"</td>" +
                "<td colspan='3'>"+ value.estado +"</td>" +
                "<td>" + todo +" </td>" +
            "</tr>";

        });

        $("#table-tec").append(tr);
    });
    verDetalle();



}

function verDetalle(){

    $(".detalle").toggle(function(){

        $(this).addClass("active")
        var key = $(this).attr("key").trim();
        var pendiente = $(this).attr("pendiente").trim();

        $(".sub-"+key+".pendiente-"+pendiente).show("fast");
    },function(){
        $(this).removeClass("active");
        var key = $(this).attr("key").trim();
        var pendiente = $(this).attr("pendiente").trim();

        $(".sub-"+key+".pendiente-"+pendiente).hide("fast");
    });

    //MOSTRAR MAPA SEGUN GRUPO
    $(".ver-mapa").click(function(){
        var key = $(this).attr("key");
        var carnet = $(this).parent().parent().parent().attr("key");
        var estados = "";
        if($("#tipo_estado").val()!= null){
            estados = $("#tipo_estado").val().join();
        }
        var sin_fecha = 1;
        var tareas = []
        var busqueda = ""
        if(key=="todos"){
           busqueda = ".sub-"+carnet;
        }else if(key=="hoy"){
            busqueda = ".sub-"+carnet+".pen-1";
        }else if(key=="pasados"){
            busqueda = ".sub-"+carnet+".pen-0";
        }else if(key=="futuros"){
            busqueda = ".sub-"+carnet+".pen-2";
        }

        $(busqueda).each(function(value){
            tareas.push( $(this).attr("tarea") )
        });
        tareas  = tareas.join();

        var url = "/webpsi/modulos/public/rutaTecnico/?" +
            "cc="+carnet
            + "&tareas="+tareas
            + "&estados="+estados
            + "&sin_fecha=1"
            + "&gestionar=1"

        window.open(url,'_blank');

    });



}