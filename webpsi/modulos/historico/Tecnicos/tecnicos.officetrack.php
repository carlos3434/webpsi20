<?php
//ini_set('display_errors', 'On');
//error_reporting(E_ALL);
require_once("../../../cabecera.php");
include_once "../../../clases/class.Conexion.php";
require_once '../../officetrack/clases/class.Usuario_eecc.php';
require_once("../clases/class.TecnicosCriticos.php");
require_once('../clases/empresa.php');
require_once('../clases/cedula.php');

$db = new Conexion();
$cnx = $db->conectarPDO();
$Usuario_eecc =  new Usuario_eecc();
$empresa = new Empresa();
$cedula = new Cedula();
$tecnico = new TecnicosCriticos();
$json_tecnicos_ot= $tecnico->TecnicosOfficetrackAll();
//ID usuario
$idUser = $_SESSION["exp_user"]["id"];

//Lista empresas
//$empArray = $Empresa->getEmpresa($db, "", "1");
$empArray = $Usuario_eecc->getEeccUser($cnx, $idUser);

$empresa->setCnx($cnx);
$empresa_options = $empresa->getEmpresaAllSelectOptions($idempresa);

$cedula->setCnx($cnx);
$cedula->setIdempresa($idempresa);
$celulas_options = $cedula->getCedulaAllByEmpresaSelectOptions();

?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
    <title>WebPSI - Pendientes por Tecnico</title>
    <meta charset="utf-8">
    <meta name="author" content="Sergio MC"/>
    <?php include("../../../includes.php") ?>
    <script type="text/javascript" src="../js/js.js"></script>
    <link rel="stylesheet" type="text/css" href="../../../estilos.css">
    <link rel="stylesheet" type="text/css" href="../asistencia/asistenciaTecnicos.css">

    <script type="text/javascript" src="../js/jquery.filter_input.js"></script>
    <script type="text/javascript" src="../js/prettify.js"></script>
    <script type="text/javascript" src="../js/jquery.multiselect.min.js"></script>
    <script type="text/javascript" src="../js/jquery.multiselect.filter.js"></script>
    <link type="text/css" href='../css/jquery.multiselect.css' rel="Stylesheet" />
    <link type="text/css" href='../css/jquery.multiselect.filter.css' rel="Stylesheet" />
    <link rel="stylesheet" href="static/tecnico.css" type="text/css"/>



    <script src="../js/json2.js"></script>
    <script src="../js/underscore-min.js"></script>
    <script src="../js/backbone-min.js"></script>
    <script src="../js/handlebars.js"></script>
    <script src="tecnicos.officetrack.js"></script>
    <script src="static/app.TecOff.js"></script>
    <script>
        window.tecnicosOT = {}
        tecnicosOT = <?=$json_tecnicos_ot;?>;
        window.fecha = {};
        fecha.diaHoy = "<?=date("d/m/Y")?>";
        fecha.diaMesFirst = "<?=date("1/m/Y")?>";
    </script>
</head>
<body>

<input type="hidden" value="<?php echo $IDUSUARIO ?>" name="txt_idusuario" id="txt_idusuario"/>

<div id="page-wrap">
    <?php echo pintar_cabecera(); ?>    <br/>
    <div id="div_res_grupal" class="div_res_grupal"
         style="border: 1px solid #304B73; padding-top: 0px; float:left;
			 width: 100%;">


        <div id="filtros" class="form-group">


            <div>
                <div id="filtro_empresa" class="filtro-item" >
                    <div>
                        <label class="control-label">Empresa:</label>
                    <span>
                        <select class="fil_empresa" id="fil_empresa" name="fil_empresa" class="form-control">
                            <option value=''>-- Seleccione --</option>
                            <?php
                            foreach ($empArray["data"] as $key=>$val) {
                                $id = $val["ideecc"];
                                $nom = $val["eecc"];
                                echo "<option value=\"$id\">$nom</option>";
                            }
                            ?>

                        </select>
                    </span></div>
                    <div>
                        <label class="control-label">Celula:</label>
                    <span>
                        <select class="fil_celula" id="fil_celula" name="fil_celula" class="form-control">
                            <option value=''>-- Seleccione --</option>
                            <?= $celulas_options ?>

                        </select>
                    </span>
                    </div>

<!--                    estados Actividades Pendiets-->
                    <div>
                        <label class="control-label">Estados</label>
                    <span>
                        <select name="tipo_estado" id="tipo_estado" multiple style="display: none">
                            <option value="2" selected="selected">Pendiente</option>
                            <option value="28" selected="selected">Planificado</option>
                            <option value="8" selected="selected">Agendado sin t&eacute;cnico</option>
                            <option value="1" selected="selected" >Agendado con t&eacute;cnico</option>
                            <option value="9,10,20" selected="selected">T&eacute;cnico en sitio</option>
                            <option value="21">Cancelado</option>
                            <option value="4,5,6" >Devuelto</option>
                            <option value="27">Pre Liquidado</option>
                            <option value="3,19">Liquidado</option>
                        </select>
                    </span>
                    </div>
<!--                    fin estados actividades pendientes -->
                    <div>
                        <h3>Fecha  Agendamiento</h3>
                        <label class="control-label">Desde</label>
                        <span><input type="text" class="fecha" id="fechaIniAgenda" /></span>


                    </div>
                    <div>
                        <h3>Fecha Recepcion Officetrack</h3>
                        <label class="control-label">Ini</label>
                        <span><input type="text" class="fecha" id="fechaIni" readonly/></span>

                    <span  class="fechaFin">
                        <label class="control-label"> Final:</label>
                        <span><input type="text" class="fecha" id="fechaFin" readonly/></span>
                    </span>
                    </div>
                </div>

                <div>

                    <button id="mostrar" style="padding: 5px;margin-left: 10px">Mostrar </button>
                    <button id="reiniciarFiltros" style="padding: 5px;margin-left: 10px">Reiniciar Filtros</button>

                </div>
            </div>

        </div>
        <br/>
        <hr/>
        <br/>
        <span><a href="#" id="ExportExcelPendientes">
                <img src="../img/excel2007.png" alt="" width="20px" style="vertical-align: bottom;"/>
                Exportar a excel</a></span>
        <!--        lista tecnicos pendientes-->
        <div id="ListaTecnicosPendientes">
            <table id="table-tec">
                <tr class="cabecera">
                    <th >Carnet</th>
                    <th >Tecnico</th>
                    <th   colspan="3">cantidad</th>
                    <th  >Ver en Mapa</th>
                </tr>
                <tr>
                    <th></th>
                    <th></th>
                    <th> < H</th>
                    <th>H</th>
                    <th>  H > </th>
                    <th></th>
                </tr>
            </table>

        </div>
        <!--        fin lista tecnicos pendientes-->
        <br/>
        <hr/>
        <br/>
         <span><a href="#" id="ExportExcelOfficetrack">
                 <img src="../img/excel2007.png" alt="" width="20px" style="vertical-align: bottom;"/>
                 Exportar a excel</a></span>
<!--        lista tecnicos estados officetrack-->
        <div id="ListaTecnicosOfficetrack">
            <table id="table-tec">
                <tr class="cabecera">
                    <td colspan="3">DATOS TECNICO</td>
                    <td colspan="3">PASOS</td>
                    <td colspan="8">Estados Officetrack</td>
                </tr>
                <tr class="cabecera">
                    <th rowspan="2" >Carnet</th>
                    <th rowspan="2" colspan="2" >Tecnico</th>
                    <th rowspan="2" >Inicio</th>
                    <th rowspan="2" >Supervicion</th>
                    <th rowspan="2" >Cerrado</th>
                    <th class="estado-ot">Atendido</th>
                    <th class="estado-ot">En procesos</th>
                    <th class="estado-ot">Inefectiva</th>
                    <th class="estado-ot">No deja</th>
                    <th class="estado-ot">No desea</th>
                    <th class="estado-ot">Ausente</th>
                    <th class="estado-ot">Otros</th>
                    <th class="estado-ot">Transferido</th>
                </tr>
            </table>
        </div>
<!--fin listado tecnicos estados officetrack-->




    </div>


<?php  include "static/templates.php"; ?>

</body>
</html>
