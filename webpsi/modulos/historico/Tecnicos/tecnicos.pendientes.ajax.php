<?php
//ini_set('display_errors', 'On');
//error_reporting(E_ALL);
require_once '../../../clases/class.Conexion.php';
require_once("../../../cabecera.php");
require_once("../clases/class.TecnicosCriticos.php");
require_once '../../officetrack/clases/class.Tecnico.php';
require_once '../../officetrack/clases/class.Location.php';
require_once '../../officetrack/clases/class.Tareas.php';
require "underscore.php";
//uso __::groupBy($data,"cod_tecnico")


$Conexion       = new Conexion();
$db = $Conexion->conectarPDO();

$accion = $_REQUEST["accion"];

$Tecnico        = new Tecnico();
$Location       = new Location();
$Tareas         = new Tareas();

$excelTareasOT = $_GET["excelTareasOT"];

$mostrarEnExcel = $_GET["excel"]; // tecnicos.pendientes.php


if($mostrarEnExcel == 1 ) {

    header("Content-Type:  application/x-msexcel");
    header("Expires: 0");
    header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
    header("Content-Disposition: attachment; filename=PendientesTecnicos-" . time() .".xls");


    $idEmp = $_REQUEST["idemp"];
    $idCel = $_REQUEST["idcel"];
    $estados = $_REQUEST["estados"];

    $carnets = $_REQUEST["carnets"];

    $fechaIni = $_REQUEST["fechaIni"];

    list($dia,$mes,$anio) = explode("/",$fechaIni);
    $fechaIni = $anio . "-". $mes . "-" . $dia;




    //OBTENGO AVERIAS DEL TECNICO
//    $pendientes = $Location->getAgendasTodos($db, $idEmp, $idCel, "", "" , "", $estados);
    $pendientes = $Location->getAgendasTodos($db, "" , "", $fechaIni, ""  , "", $estados, "" , "", $carnets);


    $data = $pendientes["data"];
    $trs = "";
    //acumulados
    $acumulados_tecnico = array();
    foreach($data as $key => $value){
        $acumulados_tecnico[$value["carnet_critico"]][$value["pendiente"]][] = $value;
    }
    $debugger= 1;

    foreach($data as $key => $value){
        $trs .= "<tr>";
        $trs .= "<td>" .$value["eecc"] . "</td>"; // empresa
        $trs .= "<td>" .$value["nombre"] . "</td>";//cedula
        $trs .= "<td>" .$value["carnet_critico"] . "</td>";
        $trs .= "<td>" .$value["tecnico"] . "</td>";

        $trs .= "<td>" .count($acumulados_tecnico[$value["carnet_critico"]][0]) . "</td>";
        $trs .= "<td>" .count($acumulados_tecnico[$value["carnet_critico"]][1]) . "</td>";
        $trs .= "<td>" .count($acumulados_tecnico[$value["carnet_critico"]][2]) . "</td>";


        $trs .= "<td>" .$value["programados"] . "</td>";

        $trs .= "<td>" .$value["codactu"] . "</td>";
        $trs .= "<td>" .$value["id_atc"] . "</td>";
        $trs .= "<td>" .$value["fecha_agenda"]."/ ".$value["horario"] . "</td>";
        $trs .= "<td>" .$value["estado"] . "</td>";
        $trs .= "<td>" .implode(" , ",$value) . "</td>";
        $trs .= "<td>" .$value["supervisor"] . "</td>";
        $trs .= "</tr>";
    }



    ?>
    <table id="table-tec">
        <tr class="cabecera">
            <th>Empresa</th>
            <th>Celula</th>
            <th>Carnet</th>
            <th>Tecnico</th>

            <th>Cant Pasados</th>
            <th>Cant Hoy</th>
            <th>Cant Futuros</th>

            <th>Programado</th>

            <th>COD ACTUACION</th>
            <th>ATC</th>
            <th>Fecha</th>
            <th>Estado</th>
            <th>Descripcion</th>
            <th>Supervisor</th>
        </tr>
        <?php print $trs; ?>
    </table>
<?php


}elseif($excelTareasOT == 1){

    header("Content-Type:  application/x-msexcel");
    header("Expires: 0");
    header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
    header("Content-Disposition: attachment; filename=TareasPorEstadoOfficetrack-" . time() .".xls");


    $idEmp = $_REQUEST["idemp"];
    $idCel = $_REQUEST["idcel"];
    $fechaIni = $_REQUEST["fechaIni"];
    $fechaFin = $_REQUEST["fechaFin"];
    $empresa_text = $_REQUEST["empresa_text"];
    $celula_text = $_REQUEST["celula_text"];

    list($dia,$mes,$anio) = explode("/",$fechaIni);
    $fechaIni = $anio . "-". $mes . "-" . $dia;
    list($dia,$mes,$anio) = explode("/",$fechaFin);
    $fechaFin = $anio . "-". $mes . "-" . $dia;
    $result = $Tareas->getTareas($fechaIni,$fechaFin . " 23:59:59 ",$idEmp , $idCel);

    $data  = $result["data"];

    $acumulados_tecnico = array();
    foreach($data as $key => $value){
        $acumulados_tecnico[$value["cod_tecnico"]][$value["paso"]][] = $value;
    }

    //mostrando grupos de grupos para estados officetrawck
    foreach($acumulados_tecnico as $carnet => $tecnico_data){
        foreach($tecnico_data as $paso =>  $value){
         $acu_estado[$carnet][$paso] =__::groupBy($value,"estado");
        }
    }




    $trs = "";
    $numeracion = 0;
    $anterior_cod = "";
    foreach($data as $key => $value){

        if($anterior_cod == $value["cod_tecnico"] || $anterior_cod == ""){
            $numeracion ++ ;
        }else{
            $numeracion = 1;
        }
        $anterior_cod = $value["cod_tecnico"];

        $deb = 1;
        $trs .= "<tr>";
        $trs .= "<td>" .$numeracion . "</td>"; // numeracion por grupo de tecnicos
        $trs .= "<td>" .$empresa_text . "</td>"; // empresa
        $trs .= "<td>" .$celula_text . "</td>";//cedula
        $trs .= "<td>" .$value["cod_tecnico"] . "</td>";
        $trs .= "<td>" .$value["nombre_tecnico"] . "</td>";

        $trs .= "<td>" .count($acumulados_tecnico[$value["cod_tecnico"]]["0001-Inicio"]) . "</td>";
        $trs .= "<td>" .count($acumulados_tecnico[$value["cod_tecnico"]]["0002-Supervision"]) . "</td>";
        $trs .= "<td>" .count($acumulados_tecnico[$value["cod_tecnico"]]["0003-Cierre"]) . "</td>";

        //ESTADOS OFFICETRACK
        $trs .="<td> ".count($acu_estado[$value["cod_tecnico"]]["0003-Cierre"]["Atendido"])  ." </td>";
        $trs .="<td> ".count($acu_estado[$value["cod_tecnico"]]["0003-Cierre"]["En procesos"])  ." </td>";
        $trs .="<td> ".count($acu_estado[$value["cod_tecnico"]]["0003-Cierre"]["Inefectiva"])  ." </td>";
        $trs .="<td> ".count($acu_estado[$value["cod_tecnico"]]["0003-Cierre"]["No Deja"])  ." </td>";
        $trs .="<td> ".count($acu_estado[$value["cod_tecnico"]]["0003-Cierre"]["No Desea"])  ." </td>";
        $trs .="<td> ".count($acu_estado[$value["cod_tecnico"]]["0003-Cierre"]["No Ubica"])  ." </td>";
        $trs .="<td> ".count($acu_estado[$value["cod_tecnico"]]["0003-Cierre"]["Otros"])  ." </td>";
        $trs .="<td> ".count($acu_estado[$value["cod_tecnico"]]["0003-Cierre"]["Transferido"])  ." </td>";



        $trs .= "<td>" .$value["paso"] . "</td>";

        $trs .= "<td>" .$value["id_atc"] . "</td>";
        $trs .= "<td>" .$value["estado"] . "</td>";
        $trs .= "<td>" .$value["observacion"] . "</td>";
        $trs .= "<td>" .$value["fecha_recepcion"] . "</td>";
        $trs .= "<td>" .$value["fecha_agenda"] . "</td>";


        //__::groupBy($acumulados_tecnico["LA5378"]["0003-Cierre"],"estado")
        $trs .= "</tr>";
    }
    ?>
    <table id="table-tec">
        <tr class="cabecera">
            <th>Numeracion</th>
            <th>Empresa</th>
            <th>Celula</th>
            <th>Carnet</th>
            <th>Tecnico</th>



            <th>Paso 1 - Inicio</th>
            <th>Paso 2 - Supervision</th>
            <th>Paso 3 - Cerrado</th>


            <th>Atendido</th>
            <th>En procesos</th>
            <th>Inefectiva</th>
            <th>No deja</th>
            <th>No desea</th>
            <th>No ubica</th>
            <th>Otros</th>
            <th>Transferido</th>



            <th>paso</th>

            <th>ATC</th>
            <th>Estados Officetrack</th>
            <th>observacion</th>
            <th>Recepcion</th>
            <th>Fecha agenda</th>
        </tr>
        <?php print $trs; ?>
    </table>
<?php


}

if($accion == "MostrarReportePendientes")
{
    $idEmp = $_POST["idemp"];
    $idCel = $_POST["idcel"];
    $estados = $_POST["estados"];
    $carnets = $_POST["carnets"];

    //fechas de rango de agendas
    $fechaIni = $_POST["fechaIni"];


    list($dia,$mes,$anio) = explode("/",$fechaIni);
    $fechaIni = $anio . "-". $mes . "-" . $dia;



    $tecnico = $Tecnico->getTecnico($db, $idEmp, "", "", $idCel);
    if ( $tecnico["estado"] and !empty($tecnico["data"]) ){
        //OBTENGO AVERIAS DEL TECNICO // $db, $empresa, $celula, $fini, $fend, $carnet, $estado , $task_id = ""
        $pendientes = $Location->getAgendasTodos($db, "" , "", $fechaIni, ""  , "", $estados, "" , "", $carnets);
    }
    print json_encode($pendientes); exit();

}elseif($accion == "MostrarTecnicosOfficetrack")
{
    $idEmp = $_POST["idemp"];
    $idCel = $_POST["idcel"];
    $fechaIni = $_POST["fechaIni"];
    $fechaFin = $_POST["fechaFin"];

    list($dia,$mes,$anio) = explode("/",$fechaIni);
    $fechaIni = $anio . "-". $mes . "-" . $dia;
    list($dia,$mes,$anio) = explode("/",$fechaFin);
    $fechaFin = $anio . "-". $mes . "-" . $dia;


    $result = $Tareas->getTareas($fechaIni,$fechaFin . " 23:59:59 ",$idEmp , $idCel);
    print json_encode($result); exit();
}