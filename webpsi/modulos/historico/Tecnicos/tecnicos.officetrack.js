
//DATA CONFIG
var url_ajax2 = "tecnicos.pendientes.ajax.php";
var url2 = "tecnicos.officetrack.php";
var tablaOfficetrack = "#ListaTecnicosOfficetrack";
var tablaPendientes = "#ListaTecnicosPendientes";

var dataAjax ={}
dataAjax.officetrack = {}
dataAjax.pendientes = {}

window.dataAjax = dataAjax
$().ready(function(){

    //ESTADOS PARA LISTADO DE TAREAS PENDIENTES
    $("#tipo_estado").multiselect({});

    if($("#fil_empresa").val() == ""){   $("#fil_celula").attr("disabled","disabled");
    }else{        $("#fil_celula").attr("disabled","");    }

    $("#fil_empresa").change(function(){
        if($(this).val() == ""){            $("#fil_celula").attr("disabled",true);
        }else{            $("#fil_celula").attr("disabled",false);        }
    });

    //TECNICOS OFFICETRACK
    if($( "#fechaIni" ).length == 1){
        $( "#fechaIni" ).datepicker({maxDate: "+0D"});
        $( "#fechaIni" ).datepicker("option", "dateFormat", "dd/mm/yy");
        $("#fechaIni").val(fecha.diaHoy);

        $( "#fechaFin" ).datepicker({maxDate: "+0D"});
        $( "#fechaFin" ).datepicker("option", "dateFormat", "dd/mm/yy");
        $("#fechaFin").val(fecha.diaHoy);


        var wigdet_fechaIniAgenda =  $( "#fechaIniAgenda" )
        wigdet_fechaIniAgenda.datepicker({maxDate: "+0D"});
        wigdet_fechaIniAgenda.datepicker("option", "dateFormat", "dd/mm/yy");
        wigdet_fechaIniAgenda.val(fecha.diaMesFirst);





    }



    $("#mostrar").click(function(){

        var idemp = $("#fil_empresa").val();
        var idcel = $("#fil_celula").val();
        var fechaIni = $("#fechaIni").val();
        var fechaFin = $("#fechaFin").val();
        //fechas para agendamiento
        var fechaIniAgenda = $("#fechaIniAgenda").val();
        var fechaFinAgenda = $("#fechaFinAgenda").val();

        var estados = "";
        if($("#tipo_estado").val() != null){
            estados = $("#tipo_estado").val().join();
        }

        //TECNICOS A MOSTRAR
        var tec = _.where(tecnicosOT,{idcedula:idcel});
        var carnets = _.map(tec , function(item){ return item.carnet_critico }).join("','")

        //LISTADO DE TAREAS ENVIADAS A OFFICETRACK
        $.ajax({
            type: "POST", url: url_ajax2, dataType : "json",
            data: {
                accion:"MostrarTecnicosOfficetrack",
                idemp:idemp,
                idcel:idcel,
                fechaIni:fechaIni,
                fechaFin:fechaFin,
                carnets : carnets
            },
            success: function (obj) {
                $(tablaOfficetrack + " .row-tec").remove();
                if(obj != null){
                    if(obj.estado == false ||  obj.data.length ==0 ){
                        this.error()
                    }else{
                        dataAjax.officetrack = obj.data;
                        mostrarTabla(obj.data);
                    }
                }else{
                    this.error()
                }

            },
            error: function () {
                alert("No se encontraron datos a mostrar en estados Officetrack");
                $(tablaOfficetrack + " .row-tec").remove();
            }
        });



        //LISTADO DE TAREAS PENDIENTES DEL TECNICO
        $.ajax({
            type: "POST",
            url: url_ajax2,
            dataType : "json",
            data: {
                accion:"MostrarReportePendientes",
                idemp:idemp,
                idcel:idcel,
                estados:estados,
                carnets : carnets,
                fechaIni:fechaIniAgenda,
                fechaFin:""
            },
            success: function (obj) {
                $(tablaPendientes + ".row-tec").remove();
                if(obj != null){
                    if(obj.estado == false ||  obj.data.length ==0 ){
                        this.error()
                    }else{
                        dataAjax.pendientes = obj.data
                        mostrarTablaPendientes(obj.data);
                    }
                }else{
                    this.error()
                }

            },
            error: function () {
                $(tablaPendientes + " .row-tec").remove();
                alert("No se encontraron datos a mostrar en Pendeintes");

            }
        });




    });


    //MOSTRAR  webpsi vs officetrack
    $("#mostrarVs").click(function(){

        var idemp = $("#fil_empresa").val();
        var idcel = $("#fil_celula").val();
        var fechaIni = $("#fechaIni").val();
        var fechaFin = $("#fechaFin").val();
        //fechas para agendamiento
        var fechaIniAgenda = $("#fechaIniAgenda").val();
        var fechaFinAgenda = $("#fechaFinAgenda").val();

        var estados = "";
        if($("#tipo_estado").val() != null){
            estados = $("#tipo_estado").val().join();
        }

        //TECNICOS A MOSTRAR
        var tec = _.where(tecnicosOT,{idcedula:idcel});
        var carnets = _.map(tec , function(item){ return item.carnet_critico }).join("','");



        //LISTADO DE TAREAS ENVIADAS A OFFICETRACK
        $.ajax({
            type: "POST", url: url_ajax2, dataType : "json",async:false ,
            data: {
                accion:"MostrarTecnicosOfficetrack",
                idemp:idemp,
                idcel:idcel,
                fechaIni:fechaIni,
                fechaFin:fechaFin,
                carnets : carnets
            },
            success: function (obj) {
                $(tablaOfficetrack + " .row-tec").remove();
                if(obj != null){
                    if(obj.estado == false ||  obj.data.length ==0 ){
                        this.error()
                    }else{
                        dataAjax.officetrack = obj.data;

                    }
                }else{
                    this.error()
                }

            },
            error: function () {
                alert("No se encontraron datos a mostrar en estados Officetrack");
                $(tablaOfficetrack + " .row-tec").remove();
            }
        });



        //LISTADO DE TAREAS PENDIENTES DEL TECNICO
        $.ajax({
            type: "POST",
            url: url_ajax2,
            dataType : "json",async:false ,
            data: {
                accion:"MostrarReportePendientes",
                idemp:idemp,
                idcel:idcel,
                estados:estados,
                carnets : carnets,
                fechaIni:fechaIniAgenda,
                fechaFin:""
            },
            success: function (obj) {
                $(tablaPendientes + ".row-tec").remove();
                if(obj != null){
                    if(obj.estado == false ||  obj.data.length ==0 ){
                        this.error()
                    }else{
                        dataAjax.pendientes = obj.data
                    }
                }else{
                    this.error()
                }

            },
            error: function () {
                $(tablaPendientes + " .row-tec").remove();
                alert("No se encontraron datos a mostrar en Pendeintes");

            }
        });


        mostrarTablaVs();



    })




    verDetalle();verDetallePendiente();

    //FILTRO EMPRESA
    $("#fil_empresa").change(function () {
        var idempresa = $(this).val();
        $("#fil_celula option").hide();
        $("#fil_celula option").attr("selected",false);
        if(idempresa != ""){
            $("#fil_celula option[idemp="+idempresa+"]").show();
            $("#fil_celula option").eq(0).show()
        }else{
            $("#fil_celula option").show();
        }
    });

    $("#reiniciarFiltros").click(function(){
        $("#fil_empresa").val("")
        $("#fil_empresa").trigger("change");
        $("#asistenciaFecha").val(fecha.diaHoy);
        $("#asistenciaTecnicos").html("")
    });

    $("#ExportExcelOfficetrack").click(function(){
        var idemp = $("#fil_empresa").val();
        var idcel = $("#fil_celula").val();
        var fechaIni = $("#fechaIni").val();
        var fechaFin = $("#fechaFin").val();
        if(idemp!="" && idcel != ""  ) {
            var accion = "ExportExcel"
            window.open(url_ajax2+"?" +
                "accion="+accion
                + "&excelTareasOT=1"
                + "&idemp="+idemp
                + "&idcel="+idcel
                + "&fechaIni="+fechaIni
                + "&fechaFin="+fechaFin
                + "&empresa_text="+$("#fil_empresa option:selected").text()
                + "&celula_text="+$("#fil_celula option:selected").text()
            );
        }else{
            alert("Debe seleccionar una contrata y Celula");
        }
    });

    $("#ExportExcelPendientes").click(function(){
        //EXPORTAR EXCEL
        //DATOS A ENVIAR
        var idemp = $("#fil_empresa").val();
        var idcel = $("#fil_celula").val();
        var estados = "";
        if($("#tipo_estado").val()!= null){
            estados = $("#tipo_estado").val().join();
        }
        if(idemp!="" && idcel != "" && estados != "" ) {
            var accion = "ExportExcel"
            window.open(url_ajax2+"?" +
                "accion="+accion
                + "&excel=1"
                + "&idemp="+idemp
                + "&idcel="+idcel
                + "&estados="+estados
            );
        }else{
            alert("Debe seleccionar una contrata y Celula");
        }




    });

    //MOSTRAR POR ESTADOS
    activarVerDetalleGrupo();


});

function activarVerDetalleGrupo(){

    $(".AccionGrupo").click(function(){

        var activar = true; // para saber si mostrar o ocultar
        if($(this).hasClass("activo")){ // si estaba activo al hacer click , es para desactivar
            activar = false;
        }


        var tec = $(this).parent().attr("carnet");
        var es_pendiente = $(this).hasClass("pendiente");
        var grupo = $(this).attr("key");


        $("#main-detalle-"+ tec + " #detalleGrupo tr.detalle-grupo").remove();

        var tecOfficetrack = _.groupBy(dataAjax.officetrack,"cod_tecnico");
        var tecPendientes = _.groupBy(dataAjax.pendientes,"carnet_critico");

        var tareas = {} // se llenara con las tareas a mostrar
        var tipo_lista = "Pendiente" // si es una lista de pendientes o officetrack

        if(es_pendiente){
            if(grupo === "g-pasado"){
                 tareas  = _.where(tecPendientes[tec],{pendiente:"0"})
            }else if(grupo === "g-hoy"){
                tareas  = _.where(tecPendientes[tec],{pendiente:"1"})
            }else if(grupo === "g-futuro"){
                tareas  = _.where(tecPendientes[tec],{pendiente:"2"})
            }
        }else{
            if(grupo === "g-p1"){
                tareas  = _.where(tecOfficetrack[tec],{paso:"0001-Inicio"})
                tipo_lista= "OT"
            }else if(grupo === "g-p2"){
                tareas  = _.where(tecOfficetrack[tec],{paso:"0002-Supervision"})
                tipo_lista= "OT"
            }else if(grupo === "g-p3"){
                tareas  = _.where(tecOfficetrack[tec],{paso:"0003-Cierre"})
                tipo_lista= "OT"
            }
        }

        if(activar){
            //DE TAREAS MOSTRAR EL DETALLE DEL GRUPO
            MostrarDetalleGrupo(tec , tareas , tipo_lista)
            $("#main-detalle-"+tec).show();
            $(this).addClass("activo")
        }else{
            OcultarDetalleGrupo(tec)
            $(this).removeClass("activo");
        }

    });

}

function OcultarDetalleGrupo(tec){
    //Eliminiar filas
    $(".detalle-"+tec).remove();
    //OCULTAR TABLA
    $("#main-detalle-"+tec).hide();

}

/* Muestra las tareas enviadas al hacer click en cada cuadrito*/
function MostrarDetalleGrupo(tec , list, tipo_lista){

    //Eliminiar filas
    $(".detalle-"+tec).remove();

    _.each(list,function(tarea, key, list){

        if(tipo_lista === "Pendiente"){
            //OBTENEMOS LOS REGISTROS PARA CONVINARLOS
            var tareaPE = _.where(dataAjax.pendientes, {id:tarea.id})[0];
            var tareaOT = _.where(dataAjax.officetrack, {task_id:tarea.id})[0];
        }else{
            var tareaPE = _.where(dataAjax.pendientes, {id:tarea.task_id})[0];
            var tareaOT = _.where(dataAjax.officetrack, {task_id:tarea.task_id})[0];
        }

        var tr = Templates.detalleGrupo({
            carnet:tec,
            atc:tareaPE ? tareaPE.id_atc  :"",
            cod_req:tareaPE ? tareaPE.codactu: "",
            estado_webpsi:tareaPE ? tareaPE.estado: "",
            paso:tareaOT ? tareaOT.paso : "",
            estado_ot: tareaOT ? tareaOT.estado: "",
            FechaAgenda:tareaOT ? tareaOT.fecha_agenda: "",
            FechaOfficetrack:tareaOT ? tareaOT.fecha_recepcion: "",
            observacion:tareaOT ? tareaOT.observacion: ""

        });


        $("#main-detalle-"+tec + " #detalleGrupo").append(tr);
    });
}

/*  Muestra la tabla principal de webpsi vs officetrack*/
function mostrarTablaVs(){
    try{
        //TECNICOS A MOSTRAR
        var tec = _.where(tecnicosOT,{idcedula:$("#fil_celula").val()});
        var carnets = _.map(tec , function(item){ return item.carnet_critico });


        $( "#ListaTecnicosVs" + " .row-tec").remove();
        var tecOfficetrack = _.groupBy(dataAjax.officetrack,"cod_tecnico");
        var tecPendientes = _.groupBy(dataAjax.pendientes,"carnet_critico");

        //MOSTRAMOS LISTADO DE TECNICOS
        var actual_carnet = ""
        _.each(carnets,function(carnet, key, list){
            //imprime la fila de acumulado sumatoria
            actual_carnet = carnet
            var tec_nombre = ""
            if( tecPendientes[actual_carnet]){
                tec_nombre = tecPendientes[actual_carnet][0].tecnico
            } else {
                tec_nombre = tecOfficetrack[actual_carnet][0].nombre_tecnico
            }

            tr = Templates.trTareasVs({
                carnet:carnet ,
                tec_nombre : tec_nombre ,
                pasado:_.where(tecPendientes[carnet],{pendiente:"0"}).length,
                hoy:_.where(tecPendientes[carnet],{pendiente:"1"}).length,
                futuro:_.where(tecPendientes[carnet],{pendiente:"2"}).length,
                cant_paso1 :_.where(tecOfficetrack[carnet],{paso:"0001-Inicio"}).length ,
                cant_paso2: _.where(tecOfficetrack[carnet],{paso:"0002-Supervision"}).length
                , cant_paso3 :_.where(tecOfficetrack[carnet],{paso:"0003-Cierre"}).length
                , cant_atendido :_.where(tecOfficetrack[carnet],{estado:"Atendido"}).length
                , cant_enproceso :_.where(tecOfficetrack[carnet],{estado:"En Proceso"}).length
                , cant_inefectiva :_.where(tecOfficetrack[carnet],{estado:"Inefectiva"}).length
                , cant_nodeja :_.where(tecOfficetrack[carnet],{estado:"No Deja"}).length
                , cant_nodesea :_.where(tecOfficetrack[carnet],{estado:"No Desea"}).length
                , cant_noubica :_.where(tecOfficetrack[carnet],{estado:"Ausente"}).length
                , cant_otros :_.where(tecOfficetrack[carnet],{estado:"Otros"}).length
                , cant_transferidos :_.where(tecOfficetrack[carnet],{estado:"Transferido"}).length
            });

            $("#ListaTecnicosVs" + " #table-tec").append(tr);
        });

        activarVerDetalleGrupo();
    }catch(err){

    }

}




//tabla de estados officetrack de tareas recepcionadas del OT
function mostrarTabla(data){


    $(tablaOfficetrack + " .row-tec").remove();
    var tecnicos = _.groupBy(data,"cod_tecnico");
    var tr = "";

    var estados = "";


    //MOSTRAMOS LISTADO DE TECNICOS
    _.each(tecnicos,function(tareas, key, list){
        //imprime la fila de acumulado sumatoria

        tr = Templates.trTareas({carnet:key ,
            tec_nombre : tareas[0].nombre_tecnico ,
            cant_paso1 :_.where(tareas,{paso:"0001-Inicio"}).length ,
            cant_paso2: _.where(tareas,{paso:"0002-Supervision"}).length
            , cant_paso3 :_.where(tareas,{paso:"0003-Cierre"}).length
            , cant_atendido :_.where(tareas,{estado:"Atendido"}).length
            , cant_enproceso :_.where(tareas,{estado:"En Proceso"}).length
            , cant_inefectiva :_.where(tareas,{estado:"Inefectiva"}).length
            , cant_nodeja :_.where(tareas,{estado:"No Deja"}).length
            , cant_nodesea :_.where(tareas,{estado:"No Desea"}).length
            , cant_noubica :_.where(tareas,{estado:"Ausente"}).length
            , cant_otros :_.where(tareas,{estado:"Otros"}).length
            , cant_transferidos :_.where(tareas,{estado:"Transferido"}).length
        })


        tr += "<tr class='tec detalle-main-"+key+"' style='display:none'> <td colspan='20'>" +
        "<table></table>" +
        "</td></tr>";

        tareas.forEach(function(value){
           //imprime el detalle oculto
            tr +=Templates.trTareasDetalle({carnet:key ,  grupo : value.paso ,    task_id : value.task_id ,   atc : value.id_atc ,
                recepcion : value.fecha_recepcion ,     agenda : value.fecha_agenda , estado : value.estado , obs:value.observacion  })

        });

        $(tablaOfficetrack + " #table-tec").append(tr);
    });


    verDetalle();verDetallePendiente();



}


function mostrarTablaPendientes(data){

    $(tablaPendientes + " .row-tec").remove();
    var tecnicos = _.groupBy(data,"carnet_critico");
    var tr = "";

    var estados = "";
    if($("#tipo_estado").val()!= null){
        estados = $("#tipo_estado").val().join();
    }

    //MOSTRAMOS LISTADO DE TECNICOS
    _.each(tecnicos,function(tareas, key, list){

        tr = "<tr class='row-tec  tec main-"+key+"' key='"+key+"'>" +
        "<td>" + key  + "</td>" +
        "<td>" + tareas[0].tecnico  + "</td>" +
        "<td class='detalle pen-0' key='"+key+"' pendiente='0'>"+ _.where(tareas,{pendiente:"0"}).length +"</td>" +
        "<td class='detalle pen-1' key='"+key+"' pendiente='1'>"+ _.where(tareas,{pendiente:"1"}).length +"</td>" +
        "<td class='detalle pen-2' key='"+key+"' pendiente='2'>"+ _.where(tareas,{pendiente:"2"}).length +"</td>" +
        "<td> <span class='mapa'> " +
        "<a class='ver-mapa' key='todos' href='#'>Todos</a> " +
        "<a class='ver-mapa' key='pasados' href='#'>Pasados</a> " +
        "<a class='ver-mapa' key='hoy' href='#'>Hoy</a> " +
        "<a class='ver-mapa' key='futuros' href='#'>Futuros</a> " +
        "</span> </td>" +
        "</tr>";

        tareas.forEach(function(value){

            var todo =  _.reduce(value, function(memo, num){ return memo + " , "+num; });
            tr += "<tr class='row-tec tareas  sub-"+key+" pen-"+value.pendiente+" pendiente-"+value.pendiente+"' tarea='"+value.id_atc+"'  style='display:none'>" +
            "<td>" +
            "<a target='_blank' " +
            "href='/webpsi/modulos/public/rutaTecnico/?cc="+key+"&tareas="+value.id_atc+"&estados="+estados+"&sin_fecha=1'>"+
            value.id_atc+"</a> </td>" +
            "<td>"+ value.fecha_agenda + "/"+value.horario +"</td>" +
            "<td colspan='3'>"+ value.estado +"</td>" +
            "<td>" + todo +" </td>" +
            "</tr>";

        });

        $(tablaPendientes + " #table-tec").append(tr);
    });
    verDetalle();verDetallePendiente();



}


function verDetalle(){
    $(tablaOfficetrack + " .detalle").toggle(function(){
        $(this).addClass("active")
        var key = $(this).attr("key").trim();
        var pendiente = $(this).attr("pendiente").trim();
        $(tablaOfficetrack + " .sub-"+key+".pendiente-"+pendiente).show("fast");
    },function(){
        $(this).removeClass("active");
        var key = $(this).attr("key").trim();
        var pendiente = $(this).attr("pendiente").trim();
        $(tablaOfficetrack + " .sub-"+key+".pendiente-"+pendiente).hide("fast");
    });
}




function verDetallePendiente(){

    $(tablaPendientes + " .detalle").toggle(function(){

        $(this).addClass("active")
        var key = $(this).attr("key").trim();
        var pendiente = $(this).attr("pendiente").trim();

        $(tablaPendientes + " .sub-"+key+".pendiente-"+pendiente).show("fast");
    },function(){
        $(this).removeClass("active");
        var key = $(this).attr("key").trim();
        var pendiente = $(this).attr("pendiente").trim();

        $(tablaPendientes + " .sub-"+key+".pendiente-"+pendiente).hide("fast");
    });

    //MOSTRAR MAPA SEGUN GRUPO
    $(".ver-mapa").click(function(){
        var key = $(this).attr("key");
        var carnet = $(this).parent().parent().parent().attr("key");
        var estados = "";
        if($("#tipo_estado").val()!= null){
            estados = $("#tipo_estado").val().join();
        }
        var sin_fecha = 1;
        var tareas = []
        var busqueda = ""
        if(key=="todos"){
            busqueda = ".sub-"+carnet;
        }else if(key=="hoy"){
            busqueda = ".sub-"+carnet+".pen-1";
        }else if(key=="pasados"){
            busqueda = ".sub-"+carnet+".pen-0";
        }else if(key=="futuros"){
            busqueda = ".sub-"+carnet+".pen-2";
        }

        $(tablaPendientes + " " +busqueda).each(function(value){
            tareas.push( $(this).attr("tarea") )
        });
        tareas  = tareas.join();

        var url = "/webpsi/modulos/public/rutaTecnico/?" +
            "cc="+carnet
            + "&tareas="+tareas
            + "&estados="+estados
            + "&sin_fecha=1"
            + "&gestionar=1"

        window.open(url,'_blank');

    });



}

/*
Muestra el detalle de la fila principal acumulada
 */
function MostrarDetalleTr( carnet , grupo ){

}

function OcultarDetalleTr( carnet , grupo ){

}