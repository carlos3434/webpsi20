<?php 
require_once("../../cabecera.php");

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />

<title>WEBPSI - Criticos - Reportes</title>

<?php include ("../../includes.php") ?>    

<script type="text/javascript" src="../../js2/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="../../js2/jquery-ui-1.10.3.custom/js/jquery-ui-1.10.3.custom.min.js"></script>
<link type="text/css" href="../../js2/jquery-ui-1.10.3.custom/css/redmond/jquery-ui-1.10.3.custom.css" rel="Stylesheet" />

<link type="text/css" href="css/reporteador.css" rel="Stylesheet" />
<link type="text/css" href='css/botones.css' rel="Stylesheet" />
<link type="text/css" href='css/jquery.multiselect.css' rel="Stylesheet" />

<script type="text/javascript" src="js/jquery.multiselect.min.js"></script>

<style type="text/css">
input[type="text"], select {
	/*border: 1px solid #000000;	*/
	border:1px solid #6297BC;
	padding: 1px;
	font-family:tahoma, arial, sans-serif;
	font-size: 11px;
}
</style>

<script type="text/javascript">
	
	$(document).ready(function(){

        $("#txt_fecha_ini").datepicker({
            yearRange:'0:+1',
            maxDate:'0D',
            changeYear: true,
            numberOfMonths: 1,
            dateFormat: 'yy-mm-dd'
        });
		
        $("#txt_fecha_fin").datepicker({
            yearRange:'0:+1',
			maxDate:'0D',
            changeYear: true,
            numberOfMonths: 1,
            dateFormat: 'yy-mm-dd'
        });
		
		$("#filtro_grupo").multiselect();
		$("#filtro_grupo").multiselect("checkAll");

		
	});

function mostrar(){
	if($("#filtro_grupo").val()!=null){
		pag = "?grupo="+$("#filtro_grupo").val().join(",")+"&tipo="+$("#filtro_digi").val();	
	}
	else{
		pag = "?grupo="+"&tipo="+$("#filtro_digi").val();
	}

	$.ajax({
        type: "POST",
            url: "controladorHistorico/historicoController.php"+pag,
            data: {digitalizacion:'digitalizacion'},
            dataType: "html",
            success: function (data) {
				$("#listadoDigitalizacion").html(data.split("|").join("/"));
            },
            error: function () {
                alert("Error");
            }
        });
}

function reporte() {
	var pag="";
	if($("#filtro_grupo").val()!=null){
		pag = "reporte_digitalizacion_excel.php?grupo="+$("#filtro_grupo").val().join(",")+"&tipo="+$("#filtro_digi").val();	
	}
	else{
		pag = "reporte_digitalizacion_excel.php?grupo="+"&tipo="+$("#filtro_digi").val();
	}
	window.open(pag);	
}

</script>
</head>

<body>

<input type="hidden" value="<?php echo $IDUSUARIO ?>" name="txt_idusuario" id="txt_idusuario"/>

<?php echo pintar_cabecera(); ?>

<br/>



<div id="div_bus2" class="divBusqueda" style="width: 750px" >

	<table class="tablaBusqueda">
		<thead>
			<th colspan='3'><font size='+1'>Reporte de Digitalización </font></th>
		</thead>
	<tr class="tr_busqueda">
		<td style="width: 50px">Agrupaciones:</td >
		<td style="background-color:white; padding: 5px;" >
			<select class="filtro_grupo" id="filtro_grupo" multiple="multiple" name="filtro_grupo[]" >
		  		<option value="d_nodo">Nodo</option>
		  		<option value="d_troba">Troba</option>
			</select
		</td >
	</tr>
	<tr>
		<td style="width: 50px">Tipo:</td >
		<td style="background-color:white; padding: 5px;" >
			<select class="filtro_digi" id="filtro_digi" name="filtro_digi" >
		  		<option value="" selected>Todos</option>
		  		<option value="0">Digitalizacion</option>
		  		<option value="357">Post Digitalizacion</option>
			</select
		</td >
	</tr>
	<tr>
		<td style="background-color:white; color: red; padding: 5px" colspan="2">                
			<span id="divBoton" style="padding-top: 3px; padding-bottom: 3px; width: 100px;">
					<a href="javascript:void(0)" onclick="mostrar()" class="botonAzul">Mostrar</a>
			</span>
			<span id="divBoton" style="padding-top: 3px; padding-bottom: 3px; width: 100px;">
					<a href="javascript:void(0)" onclick="reporte()" class="botonAzul">Exportar</a>
			</span>
		</td>
	</tr>

	</table>

	<br><br>
	<div id="listadoDigitalizacion" style="overflow: auto;height: 500px">
	</div>
</div>

<div id="register"></div>
	
</body>
</html>