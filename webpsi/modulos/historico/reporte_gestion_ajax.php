<?php

require_once '../../clases/class.Conexion.php';
require_once("../../cabecera.php");
require_once("clases/gestionCriticos.php");

$accion = $_REQUEST["accion"];


if($accion == "gestion_criticos_date")
{
    $fechaIni = $_POST["fechaIni"];
    $fechaFin = $_POST["fechaFin"];

    list($dia,$mes,$anio) = explode("/",$fechaIni);
    $fechaIni = $anio . "-". $mes . "-" . $dia;
    list($dia,$mes,$anio) = explode("/",$fechaFin);
    $fechaFin = $anio . "-". $mes . "-" . $dia;
    $db = new Conexion();
    $cnx = $db->conectarPDO();
    $ob_critico = new gestionCriticos();
    $critico = $ob_critico->getGestionCriticosForDate($cnx,$fechaIni,$fechaFin);

    echo str_replace("\\u0000","",json_encode($critico)); exit();

    //$result = $Tareas->getTareas($fechaIni,$fechaFin . " 23:59:59 ",$idEmp , $idCel);
    //print json_encode($result); exit();
}