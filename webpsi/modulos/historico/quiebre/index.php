<?php
require_once("../../../cabecera.php");
require_once("QuiebreController.php");

$Quiebre = new QuiebreController();

//DATA A MOSTRAR
$arr = $Quiebre->quiebreModel->getQuiebres();

?>
<!DOCTYPE html>
<html>
<head>
<title>WEBPSI - Mantenimiento de Quiebres</title>
<meta charset="UTF-8">
<?php include("../../../includes.php") ?>

<link type="text/css" href='../css/jquery.multiselect.css' rel="Stylesheet" />
<script type="text/javascript" src="../js/jquery.filter_input.js"></script>
<script type="text/javascript" src="../js/prettify.js"></script>
<script type="text/javascript" src="../js/jquery.multiselect.min.js"></script>

<script type="text/javascript" src="../js/js.js"></script>
<link rel="stylesheet" type="text/css" href="../../../css/estiloAdmin.css">
<link rel="stylesheet" type="text/css" href="../../../css/buttons.css">
<script src="../js/json2.js"></script>
<script src="../js/underscore-min.js"></script>
<link rel="stylesheet" type="text/css" href="../../../estilos.css">
    <link rel="stylesheet" href="static/quiebre.css"/>
    <script src="static/quiebre.js"></script>
</head>

<body>

<input type="hidden" value="<?php echo $IDUSUARIO ?>" name="txt_idusuario" id="txt_idusuario"/>

<div id="page-wrap">
<?php echo pintar_cabecera(); ?>

<br/>

<div id="div_res_grupal" class="div_res_grupal" style="border: 1px solid #304B73; padding-top: 0px; float:left; overflow-y: auto;
			height: 500px; width: 780px;">
    <span style="padding: 20px" class="nueva"><a href="#" onclick="Crear();">[ Agregar Quiebre ]</a> </span>



    <table class="tabla_res_grupal" style="width: 100%;">
        <thead>
        <tr>

            <th class="th_res_grupal2">Nombre
            </th>
            <th class="th_res_grupal2">Apocope
            </th>

            <th class="th_res_grupal2">Acciones
            </th>
        </tr>
        </thead>
        <?php
        $i = 1;

        foreach ($arr as $fila) {
            $cbox = "";
            ?>
            <tr>
                <td style="display: none;" class="td-data" data='<?= json_encode($fila); ?>'></td>

                <td class="td_res_grupal2" style="width:120px"><?php echo $fila["nombre"] ?></td>
                <td class="td_res_grupal2"
                    style="width:120px"><?php echo $fila["apocope"] ?></td>

                <td class="td_res_grupal2" style="width:150px">
                    <a href="#" onclick="Editar(this)">
                        <img src="../../../img/pencil_16.png" alt="Editar " title="Editar ">
                    </a>

                    <?php
                    $imagen = "estado_deshabilitado.png";
                    $title = " Deshabilitado";
                    $alt = "Deshabilitado";
                    if ($fila["cestado"]) {
                        $imagen = "estado_habilitado.png";
                        $title = " Habilitado";
                        $alt = "Habilitado";

                    }
                    ?>
                    <a href="#" onclick="EditarEstado(this)">
                        <img src="../../../img/<?= $imagen; ?>" alt="<?= $alt; ?>" title="<?= $title; ?>">
                    </a>


                </td>
            </tr>
            <?php

            $i++;
        }

        ?>
    </table>

</div>

    <?php  include"static/templates.php" ?>
</body>
</html>
