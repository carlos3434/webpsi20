
<div id="parentModal" style="display: none;">
    <div id="childModal" style="background: #fff;"></div>
</div>
<script id="formTemplate" type="text/template">
    <div id="div_Clonar" class="divClonar">
        <table class="tablaClonar">
            <tr style="display: none">
<!--            <tr >-->
                <td class="celda_titulo">id:</td>
                <td class="celda_res" colspan="2">
                    <input type="text" name="id" id="id" class="id" value=""/>
                </td>
            </tr>
            <tr>
                <td class="celda_titulo">Nombre:</td>
                <td class="celda_res" colspan="2">
                    <input type="text" name="nombre" id="nombre" class="nombre" value=""/>
                </td>
            </tr>
            <tr>
                <td class="celda_titulo">Apocope:</td>
                <td class="celda_res" colspan="2">
                    <input type="text" name="apocope" id="apocope" class="apocope" value=""/>
                </td>
            </tr>

            <tr>
                <td class="celda_titulo">Estado:</td>
                <td class="celda_res" colspan="2">
                    <select class="slct_estado" id="slct_estado" name="slct_estado">
                        <option value='1'>Activo</option>
                        <option value='0'>Inactivo</option>
                    </select>
                </td>
            </tr>
            <tr>
                <td class="celda_titulo">Actividad Officetrack:</td>
                <td class="celda_res" colspan="2">
                    <select class="slct_actividad" id="slct_actividad" name="slct_actividad" multiple style="display: none">
                        <option value='1'>Averia</option>
                        <option value='2'>Provision</option>
                        <option value='3'>Manual</option>
                        <option value='4'>Manual Provision</option>
                    </select>
                </td>
            </tr>

            <tr>
                <td class="celda_res" colspan="3" align="center">
                    <button id="guardar" class="action blue" title="">
                        <span class="label">Guardar </span>
                    </button>
                    &nbsp; &nbsp;
                    <button id="Cancelar" class="action blue" title="">
                        <span class="label">Cancelar </span>
                    </button>
                </td>
            </tr>
        </table>
    </div>
</script>