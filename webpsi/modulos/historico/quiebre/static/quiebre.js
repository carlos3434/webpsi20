/**
 * Created by root on 17/11/14.
 */

$().ready(function () {

    //Uso de templates de underscore
    _.templateSettings = {interpolate: /\{\{(.+?)\}\}/g, evaluate: /\{!(.+?)!\}/g};
    window.templates = {};
    templates.formTemplate = _.template($("#formTemplate").html());

    $("#slct_actividad").multiselect({})
});

function Crear(){
    OpenDialog("Crear Quiebre")
}

function OpenDialog(title){
    $("#childModal").html(templates.formTemplate())
    $("#slct_actividad").multiselect({});
    $("#Cancelar").click(function(){
        $("#childModal").dialog("close")
    });


    $("#guardar").click(function(){

        var validacion = validarFormulario("#childModal");
        if(validacion == true){

            //DATOS
            var id          = $("#id").val();
            var nombre          = $("#nombre").val();
            var apocope          = $("#apocope").val();
            var estado          = $("#slct_estado").val();
            var actividades  = "";

            if($("#slct_actividad").val()  != null){
                actividades = $("#slct_actividad").val().join(",")
            }

            //GET TIPO DE ACCION
            var accion = "EditarQuiebre"
            if($("#id").val() == ""){
                accion = "CrearQuiebre";
            }

            //ENVIAR DATOS
            $.ajax({
                type: "POST",
                url: "quiebre.ajax.php",
                data: {
                    accion:accion,
                    id:id,
                    nombre:nombre,
                    apocope:apocope,
                    estado:estado,
                    actividades:actividades

                },
                success: function (data) {

                    if( data == null|| data == undefined || data != "1"){
                        alert("Hubo un problema al registrar los datos " + data)
                    }else{
                        alert("Los datos se registraron correctamente")
                    }

                    location.reload();
                }
            });
        }
    });

    $("#childModal").dialog({
        modal: true,
        width: '45%',
        title: title,
        close: function( event, ui ) {
            $("#childModal").html("");
        }
    });


}



function Editar(e)
{

    var element = $(e)
        .parent()
        .parent()
        .find(".td-data").attr("data");
    var data = JSON.parse(element);


    //Cargar Modal
    OpenDialog("Editar Quiebre");
    //Agregar Valores
    $("#id").val(data.id_quiebre);
    $("#nombre").val(data.nombre);
    $("#apocope").val(data.apocope);
    $("#slct_estado").val(data.cestado);
    $("#slct_actividad").val(data.actividades.split(","));
    $("#slct_actividad").multiselect("refresh");



}

function EditarEstado(e) {
    var element = $(e)
        .parent()
        .parent()
        .find(".td-data").attr("data");
    var data = JSON.parse(element);

    var nuevo_estado = 0;
    if (data.cestado == 0) {
        nuevo_estado = 1;
    }

    $.ajax({
        type: "POST",
        url: "quiebre.ajax.php",
        data: {
            accion:"EditarEstado",
            estado: nuevo_estado,
            idquiebre:data.id_quiebre

        },
        success: function (response) {
            location.reload();
        }
    });


}


validarFormulario  = function(idparent){
    var error = 0;

    $(idparent + " input").each(function(i,el){

        if( $(el).attr("id") != "id"){

            var value  = $(el).val();
            if(value == ""){
                $(el).css("border-color","red");
                alert("Debe llenar todos los datos");
                console.log($(el))
                error++;
                return false;
            }else{
                $(el).css("border-color","");
            }
        }


    });
    if(error > 0 ){
        return false;
    }

    return true;
}
