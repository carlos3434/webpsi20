<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 17/11/14
 * Time: 11:40 AM
 */
require_once "../../../clases/class.Conexion.php";
require_once('../clases/quiebre.php');

class QuiebreController {


    protected $cnx ;
    public $quiebreModel;

    public function __construct()
    {
        $db = new Conexion();
        $cnx = $db->conectarPDO();

        $this->cnx = $cnx;

        $this->quiebreModel = new Quiebre();

    }


} 