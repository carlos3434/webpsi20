<?php

require_once("../../cabecera.php");
include_once "../../clases/class.Conexion.php";
require_once '../officetrack/clases/class.Usuario_eecc.php';
require_once("clases/class.TecnicosCriticos.php");
require_once('clases/empresa.php');
require_once('clases/cedula.php');

$db = new Conexion();
$cnx = $db->conectarPDO();
$Usuario_eecc =  new Usuario_eecc();
$empresa = new Empresa();
$cedula = new Cedula();
$tecnico = new TecnicosCriticos();
$json_tecnicos_ot= $tecnico->TecnicosOfficetrackAll();
//ID usuario
$idUser = $_SESSION["exp_user"]["id"];

//Lista empresas
//$empArray = $Empresa->getEmpresa($db, "", "1");
$empArray = $Usuario_eecc->getEeccUser($cnx, $idUser);

$empresa->setCnx($cnx);
$empresa_options = $empresa->getEmpresaAllSelectOptions($idempresa);

$cedula->setCnx($cnx);
$cedula->setIdempresa($idempresa);
$celulas_options = $cedula->getCedulaAllByEmpresaSelectOptions();

?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
    <title>WebPSI - Pendientes por Tecnico</title>
    <meta charset="utf-8">

    <link type="text/css" href='css/estilo.css' rel="Stylesheet" />
    <?php include("../../includes.php") ?>

    <link rel="stylesheet" type="text/css" href="asistencia/asistenciaTecnicos.css">
    <link type="text/css" href='css/demo_table.css' rel="Stylesheet" />
    <link type="text/css" href='css/TableTools.css' rel="Stylesheet" />
    <link type="text/css" href='css/prettify.css' rel="Stylesheet" />
    <link type="text/css" href='css/jquery.multiselect.css' rel="Stylesheet" />
    <link rel="stylesheet" type="text/css" href="css/dataTables.tableTools.css">
    <script type="text/javascript" src="js/jquery.filter_input.js"></script>
    <script type="text/javascript" src="js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" language="javascript" src="js/dataTables.tableTools.js"></script>
    <script type="text/javascript" src="js/reporte_gestion.js"></script>
    <script type="text/javascript" src="js/json2.js"></script>
    <script type="text/javascript" src="js/underscore-min.js"></script>
    <script type="text/javascript" src="js/backbone-min.js"></script>
    <script type="text/javascript" src="js/handlebars.js"></script>
    <script>
        window.fecha = {};
        fecha.diaHoy = "<?=date("d/m/Y")?>";
        fecha.diaMesFirst = "<?=date("1/m/Y")?>";
    </script>
    <style>
        #clienteCriticos_wrapper{
            width: auto;
        }
    </style>
</head>
<body>
<div class="modalPop"></div>
<input type="hidden" value="<?php echo $IDUSUARIO ?>" name="txt_idusuario" id="txt_idusuario"/>

<div id="page-wrap">
    <?php echo pintar_cabecera(); ?>    <br/>
    <div id="div_res_grupal" class="div_res_grupal" style="border: 1px solid #304B73; padding-top: 0px; float:left; width: 100%;">
        <div id="filtros" class="form-group">
            <div>
                <div id="filtro_empresa" class="filtro-item" >
                    <div>
                        <h3>Fecha</h3>
                        <label class="control-label">Ini</label>
                        <span><input type="text" class="fecha" id="fechaIni" readonly/></span>
                        <span  class="fechaFin">
                        <label class="control-label"> Final:</label>
                        <span><input type="text" class="fecha" id="fechaFin" readonly/></span>
                    </span>
                    </div>
                </div>
                <div>
                    <button id="mostrar" style="padding: 5px;margin-left: 10px">Mostrar </button>
                </div>
            </div>
        </div>
        <div class="listado_clientes" style="width: 1220px; margin: auto;">
            
            <table cellpadding="0" cellspacing="0" border="0" class="display" id="clienteCriticos" >
                <thead>
                    <tr>
                        <th>Averias</th>
                        <th>Codigo Atencion</th>
                        <th>Tipo Actividad</th>
                        <th>Fecha Registro</th>
                        <th>Quiebre</th>
                        <th>M1</th>
                        <th>Empresa</th>
                        <th>Telefono</th>
                        <th>Fecha Agenda</th>
                        <th>Estado</th>
                        <th>Tecnico</th>
                        <!--<th>MDF</th>
                        <th title="Microzona">Mcz.</th>-->
                        <th>Distrito</th>
                        <th>Mov.</th>
                        <th>Trans<br>Info</th>
                        <th style="display:none">Tipo_Averia</th>
                        <th style="display:none">Horas_Averia</th>
                        <th style="display:none">Fecha_Reporte</th>
                        <th style="display:none">Ciudad</th>
                        <th style="display:none">Averia</th>
                        <th style="display:none">Inscripcion</th>
                        <th style="display:none">Fono1</th>
                        <th style="display:none">Telefono</th>
                        <th style="display:none">MDF</th>
                        <th style="display:none">Observacion_102</th>
                        <th style="display:none">Segmento</th>
                        <th style="display:none">Area_</th>
                        <th style="display:none">Direccion_Instalacion</th>
                        <th style="display:none">Codigo_Distrito</th>
                        <th style="display:none">Nombre_Cliente</th>
                        <th style="display:none">Orden_Trabajo</th>
                        <th style="display:none">Veloc_Adsl</th>
                        <th style="display:none">Clase_Servicio_Catv</th>
                        <th style="display:none">Codmotivo_req_catv</th>
                        <th style="display:none">Total_Averias_Cable</th>
                        <th style="display:none">Total_Averias_Cobre</th>
                        <th style="display:none">Total_Averias</th>
                        <th style="display:none">fftt</th>
                        <th style="display:none">llave</th>
                        <th style="display:none">dir_terminal</th>
                        <th style="display:none">Fonos_Contacto</th>
                        <th style="display:none">Contrata</th>
                        <th style="display:none">Zonal</th>
                        <th style="display:none">Quiebre</th>
                        <th style="display:none">Lejano</th>
                        <th style="display:none">Distrito</th>
                        <th style="display:none">eecc_zona</th>
                        <th style="display:none">Zona_Movistar_Uno</th>
                        <th style="display:none">Paquete</th>
                        <th style="display:none">Data_Multiproducto</th>
                        <th style="display:none">Averia_m1</th>
                        <th style="display:none">Fecha_Data_Fuente</th>
                        <th style="display:none">Telefono_Codclientecms</th>
                        <th style="display:none">Rango_Dias</th>
                        <th style="display:none">Sms1</th>
                        <th style="display:none">Sms2</th>
                        <th style="display:none">Area2</th>
                        <th style="display:none">Microzona</th>
                        <th style="display:none">Tecnico</th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
    </div>
<?php  include "Tecnicos/static/templates.php"; ?>

</body>
</html>