<?php
session_start();
$PATH =  $_SERVER['DOCUMENT_ROOT']."/webpsi/";
require_once ($PATH."clases/class.Conexion.php");

$db = new Conexion();
$cnx = $db->conectarPDO();

$usuario = $_SESSION["exp_user"]["id"];

//visualizarFiles
if ( isset($_POST["codsrv"]) and isset($_POST["codcli"]) and isset($_POST["servicio"]) ) {

    $codsrv=$_POST["codsrv"];
    $codcli=$_POST["codcli"];
    $servicio=$_POST["servicio"]; //provision o averia
    

    $sql="	SELECT CONCAT(appater, ' ',apmater) apellidos, nombre 
			FROM webpsi_coc.tb_lineas_servicio_total 
			WHERE codclicms='$codcli' LIMIT 1";
    $res = $cnx->query($sql);
    $query = $res->fetch(PDO::FETCH_ASSOC);
    $apellidos=$query['apellidos'];
    $nombre=$query['nombre'];

    if ($servicio=='provision') {
        $sq2="	SELECT tipocalle, nomcalle, numcalle 
				FROM webpsi_coc.tb_lineas_servicio_total 
				WHERE codclicms='$codcli' AND codservcms='$codsrv' LIMIT 1";
        unset($res);
        $res = $cnx->query($sq2);

        $query = $res->fetch(PDO::FETCH_ASSOC);

        $tipocalle=$query['tipocalle'];
        $nomcalle=$query['nomcalle'];
        $numcalle=$query['numcalle'];

        echo json_encode(array('apellidos'=>$apellidos,'nombre'=>$nombre,'tipocalle'=>$tipocalle,'nomcalle'=>$nomcalle,'numcalle'=>$numcalle));
    } elseif ($servicio=='averia') {

        echo json_encode(array('apellidos'=>$apellidos,'nombre'=>$nombre));
    }
//GenerarActQuiebre
} elseif ( isset($_POST['data']) ) {

    $data = json_decode($_POST['data']);
    $servicio = $_POST['servicio'];
    $quiebre = $_POST['quiebre'];
    $apellidos = $_POST['apellidos'];
    $nombre = $_POST['nombre'];

    $fechorreg=$data->fechorreg;
    $var = explode("/", substr($fechorreg, 0, 10));
    $var=array($var['1'],$var['0'],$var['2']);
    $fechorreg = implode("/", $var).substr($fechorreg, 10);
    $date = new DateTime($fechorreg);
    $fechorreg = $date->format('Y-m-d h:i:s');

    $codcli=$data->codcli;
    $codnod=$data->codnod;
    $coddtt=$data->coddtt;
    $nombre_cliente = $apellidos.', '.$nombre;
    $codordtrab = $data->codordtrab;
    $tipreq=$data->tipreq;
    $codmotv = $data->codmotv;
    $nroplano = $data->nroplano;
    $codofcadm=$data->codofcadm;
    $desdtt=$data->desdtt;

    $sql="	SELECT eecc_critico
			FROM webpsi_fftt.nodos_eecc_regiones 
			WHERE nodo='$codnod' LIMIT 1";
    $res = $cnx->query($sql);
    $query = $res->fetch(PDO::FETCH_ASSOC);
    $eecc_critico=$query['eecc_critico'];

    if ($servicio == 'averia') {
        
        $codreqatn=$data->codreqatn;
        
        $destipvia=$data->destipvia;
        $desnomvia=$data->desnomvia;
        $numvia=$data->numvia;
        $destipurb=$data->destipurb;
        $desurb=$data->desurb;
        $despis=$data->despis;
        $desint=$data->desint;
        $desmzn=$data->desmzn;
        $deslot=$data->deslot;
        $codclasrv=$data->codclasrv;
        
        $codmotivo_req_catv=$tipreq.$codmotv;
        
        $fftt=$codnod.$nroplano;
        $desnomctr=$data->desnomctr;

        $direccion_instalacion = $destipvia.' '.$desnomvia.' '.$numvia.' '.$destipurb.' '.$desurb.', Piso: '.$despis.' Int: '.$desint.' Mzn: '.$desmzn.' Lt: '.$deslot;
        
        $sql="INSERT INTO webpsi_coc.averias_criticos_final 
        (tipo_averia,horas_averia,fecha_registro,ciudad,averia,inscripcion,fono1,telefono,
        mdf,observacion_102,segmento,area_,direccion_instalacion,codigo_distrito,
        nombre_cliente,orden_trabajo,veloc_adsl,clase_servicio_catv,codmotivo_req_catv,
        total_averias_cable,total_averias_cobre,total_averias,fftt,llave,dir_terminal,
        fonos_contacto,contrata,zonal,wu_nagendas,wu_nmovimientos,wu_fecha_ult_agenda,
        total_llamadas_tecnicas,total_llamadas_seguimiento,llamadastec15dias,llamadastec30dias,
        quiebre,lejano,distrito,eecc_zona,zona_movistar_uno,paquete,data_multiproducto,
        averia_m1,fecha_data_fuente,telefono_codclientecms,rango_dias,sms1,sms2,area2,
        velocidad_caja_recomendada,tipo_servicio,tipo_actuacion,eecc_final,microzona) 
        VALUES 
        ('aver-catv-pais',TIMESTAMPDIFF(HOUR, DATE_FORMAT('$fechorreg','%Y-%m-%d %h:%i:%s'), NOW()),DATE_FORMAT('$fechorreg','%Y-%m-%d %h:%i:%s'),'','$codreqatn','$codcli','','',
        '$codnod','','','','$direccion_instalacion','$coddtt',
        '$nombre_cliente','$codordtrab','','$codclasrv','$codmotivo_req_catv',
        '','','','$fftt','',NULL,
        '','$desnomctr','$codofcadm','','','',
        NULL,NULL,'','',
        '$quiebre','','$desdtt','','','','',
        '','','$codcli','','','','',
        NULL,'catv','AVERIA','$eecc_critico','') ";

        $sql2="INSERT INTO webpsi_coc.averias_criticos_final_historico 
        (tipo_averia,horas_averia,fecha_registro,ciudad,averia,inscripcion,fono1,telefono,
        mdf,observacion_102,segmento,area_,direccion_instalacion,codigo_distrito,
        nombre_cliente,orden_trabajo,veloc_adsl,clase_servicio_catv,codmotivo_req_catv,
        total_averias_cable,total_averias_cobre,total_averias,fftt,llave,dir_terminal,
        fonos_contacto,contrata,zonal,wu_nagendas,wu_nmovimientos,wu_fecha_ult_agenda,
        total_llamadas_tecnicas,total_llamadas_seguimiento,llamadastec15dias,llamadastec30dias,
        quiebre,lejano,distrito,eecc_zona,zona_movistar_uno,paquete,data_multiproducto,
        averia_m1,fecha_data_fuente,telefono_codclientecms,rango_dias,sms1,sms2,area2,
        velocidad_caja_recomendada,tipo_servicio,tipo_actuacion,eecc_final,microzona,
        fecha_cambio, fecha_subida) 
        VALUES 
        ('aver-catv-pais',TIMESTAMPDIFF(HOUR, DATE_FORMAT('$fechorreg','%Y-%m-%d %h:%i:%s'), NOW()),DATE_FORMAT('$fechorreg','%Y-%m-%d %h:%i:%s'),'','$codreqatn','$codcli','','',
        '$codnod','','','','$direccion_instalacion','$coddtt',
        '$nombre_cliente','$codordtrab','','$codclasrv','$codmotivo_req_catv',
        '','','','$fftt','',NULL,
        '','$desnomctr','$codofcadm','','','',
        NULL,NULL,'','',
        '$quiebre','','$desdtt','','','','',
        '','','$codcli','','','','',
        NULL,'catv','AVERIA','$eecc_critico','',DATE_FORMAT('$fechorreg','%Y-%m-%d %h:%i:%s'),DATE_FORMAT('$fechorreg','%Y-%m-%d %h:%i:%s')) ";

    } elseif ($servicio=='provision') {
        //$apellidos = $_POST['apellidos'];
        //$nombre = $_POST['nombre'];
        $tipocalle = $_POST['tipocalle'];
        $nomcalle = $_POST['nomcalle'];
        $numcalle = $_POST['numcalle'];
        $piso = $_POST['piso'];
        $interior = $_POST['interior'];
        $manzana = $_POST['manzana'];
        $lote = $_POST['lote'];
        
        $coddpt = $data->coddpt;
        $codpvc = $data->codpvc;
        $codreq = $data->codreq;
        $indvip = $data->indvip;
        $codctr = $data->codctr;

        $distrito = $coddtt.$codpvc.$coddpt;

        $direccion = $tipocalle.' '.$nomcalle.' '.$numcalle.', Piso: '.$piso.' Int: '.$interior.' Mzn: '.$manzana.' Lt: '.$lote;
        
        $tipo_motivo = $tipreq.$codmotv;

        $fftt= $codnod.$nroplano;

        $sql="	INSERT INTO webpsi_coc.tmp_provision 
		(origen, horas_pedido, fecha_Reg, ciudad, codigo_req, codigo_del_cliente, fono1, telefono,
		 mdf, obs_dev, codigosegmento, estacion, direccion, distrito, nomcliente, orden, veloc_adsl,
		 servicio, tipo_motivo, tot_aver_cab, tot_aver_cob, tot_averias, fftt, llave, dir_terminal,
		 fonos_contacto, contrata, zonal, wu_nagendas, wu_nmovimient, wu_fecha_ult_age, tot_llam_tec,
		 tot_llam_seg, llamadastec15d, llamadastec30d, quiebre, lejano, des_distrito, eecc_zon,
		 zona_movuno, paquete, data_multip, aver_m1, fecha_data_fuente, telefono_codclientecms, rango_dias,
		 sms1, sms2, area2, veloc_caja_recomen, tipo_servicio, tipo_actuacion, eecc_final, microzona) 
		VALUES  
		('pen_prov_catv',TIMESTAMPDIFF(HOUR, DATE_FORMAT('$fechorreg','%Y-%m-%d %h:%i:%s'), NOW()), DATE_FORMAT('$fechorreg','%Y-%m-%d %h:%i:%s'), '', '$codreq', '$codcli', '', '',
		 '$codnod', '', '$indvip', 'TELEDIF', '$direccion', '$distrito', '$nombre_cliente', '$codordtrab', '',
		 '', '$tipo_motivo', NULL, NULL, NULL, '$fftt', '', NULL,
		 '', '$codctr', '$codofcadm', NULL, NULL, '', NULL, NULL,
		 NULL, NULL, '$quiebre', '', '$desdtt', '',
		 NULL, NULL, NULL, '', NULL, '$codcli', '',
		 '', '', 'EN CAMPO', NULL, NULL, '', '$eecc_critico', '')	";
    }
    unset($res);
    $res = $cnx->exec($sql);
    unset($res);
    $res = $cnx->exec($sql2);

    echo json_encode(array('estado'=>true,'data'=>''));
}
else
    echo json_encode(null);
