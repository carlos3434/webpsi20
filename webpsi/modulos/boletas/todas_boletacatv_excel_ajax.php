<?php 
require_once("clases/class.Boletas.php");
    // obtenemos los datos del archivo
        $anio = $_GET["anio"];
	$mes = $_GET["mes"];
        $contrata = $_GET["contrata"];
	
	$boleta = new  Boletas();

    $c = $boleta->todas_boletas_excel_catv($anio,$mes,$contrata);
	
        $file = $anio.'-'.$mes.'-'.$contrata.".xls";
        ob_start();
    if (count($c)!=0) {
	?>

	<table class="tablaClonar" >
    <tr>
                <td class="celda_titulo"> GRUPO </td>
                <td class="celda_titulo"> TIPO </td>
                <td class="celda_titulo"> MOTIVO </td>
                <td class="celda_titulo"> CLIENTE </td>
                <td class="celda_titulo"> APELLIDO PATERNO </td> 
                <td class="celda_titulo"> APELLIDO MATERNO </td>
                <td class="celda_titulo"> NOMBRES </td>
                <td class="celda_titulo"> INDICADOR </td>
                <td class="celda_titulo"> CATEGORIA </td>
                <td class="celda_titulo"> SERVICIO </td>
                <td class="celda_titulo"> OFICINA </td> 
                <td class="celda_titulo"> DEPARTAMENTO </td>
                <td class="celda_titulo"> PROVINCIA </td>
                <td class="celda_titulo"> DISTRITO </td>
                <td class="celda_titulo"> VIA </td> 
                <td class="celda_titulo"> FECHA </td>
                <td class="celda_titulo"> NODO </td>
                <td class="celda_titulo"> CONTRATA </td>
	</tr>
	<?php
	foreach ($c as $fila) { 
	?>
	<tr>
		<td class="celda_res"><?php echo $fila["grupo"]?></td>
		<td class="celda_res"><?php echo $fila["tipo"]?></td>
		<td class="celda_res"><?php echo $fila["motivo"]?></td>
		<td class="celda_res"><?php echo $fila["cliente"]?></td>
		<td class="celda_res"><?php echo $fila["apellidopaterno"]?></td>
		<td class="celda_res"><?php echo $fila["apellidomaterno"]?></td>
		<td class="celda_res"><?php echo $fila["nombres"]?></td>
		<td class="celda_res"><?php echo $fila["indicador"]?></td>
		<td class="celda_res"><?php echo $fila["categoria"]?></td>
		<td class="celda_res"><?php echo $fila["servicio"]?></td>
		<td class="celda_res"><?php echo $fila["oficina"]?></td>
		<td class="celda_res"><?php echo $fila["departamento"]?></td>
		<td class="celda_res"><?php echo $fila["provincia"]?></td>
		<td class="celda_res"><?php echo $fila["distrito"]?></td>
		<td class="celda_res"><?php echo $fila["via"]?></td>
		<td class="celda_res"><?php echo $fila["fecha"]?></td>
	        <td class="celda_res"><?php echo $fila["nodo"]?></td>
		<td class="celda_res"><?php echo $fila["contrata"]?></td>
	</tr>
	<?php
	}    
    ?>
	</table>

	 <?php
}
        $contenido = ob_get_contents();
        ob_end_clean();
        header("Expires : 0");
        header("Content-Type: application/vnd.ms-excel");
        header("Content-disposition: attachment; filename=$file");
        echo $contenido;

?>