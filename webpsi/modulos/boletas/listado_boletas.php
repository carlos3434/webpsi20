<?php
//require_once("clases/class.ConexionSigas.php");
require_once("../../cabecera.php");


?>
<html>
<head>
<title>Pruebas</title>
<?php include ("../../includes.php") ?> 
<link rel="stylesheet" href="jquery-ui-1.10.3/themes/base/jquery.ui.all.css" />
<link rel="stylesheet" type="text/css" href="../../estilos.css">
<link rel="stylesheet" type="text/css" href="estiloAdmin.css">
<link rel="stylesheet" type="text/css" href="buttons.css">

             <script src="jquery-ui-1.10.3/jquery-1.9.1.js"></script>
             <script src="jquery-ui-1.10.3/ui/jquery-ui.js"></script>
			 <script type="text/javascript">
	 
	                function listar()
                {
			        var vista = $("#vista").val();   
					var campo = $("#campo").val();
					if(vista==2 && campo==1)
					{
					$.ajax({
                        type: "POST",
                        url: "vista_basica_orden_ajax.php"
                    }).done(function( msg ) {
						//alert(msg);
						$("#lista").html(msg);				
                    });
					}
					else if(vista==2 && campo==2)
					{
					$.ajax({
                        type: "POST",
                        url: "vista_basica_solicitud_ajax.php",
                        data: { 
                            buscar_basica: '1'
                        }
                    }).done(function( msg ) {
						//alert(msg);
						$("#lista").html(msg);				
                    });
					}
					else if(vista==2 && campo==3)
					{
					$.ajax({
                        type: "POST",
                        url: "vista_basica_telefono_ajax.php",
                        data: { 
                            buscar_basica: '1'
                        }
                    }).done(function( msg ) {
						//alert(msg);
						$("#lista").html(msg);				
                    });
					}
                } 

                function descarga_01()
                {
				alert("Descarga Iniciada");
				}				
						
</script>
</head>
<body background="imagenes/fondo.gif">
<?php
echo pintar_cabecera(); 
?>
  <div id="div_Clonar" class="divClonar" style="width:300px;">
  <br>
  <table class="tablaClonar">
             <thead>
             <tr>
             <th class="celda_titulo" colspan="9">Consulta de Boletas</th>
             </tr>	
             </thead>
  <tbody>
  <tr>
 <td class="celda_titulo">
	Vista
	</td>
	<td>
		<select id="vista" name="vista" class="caja_texto3">
			<option value="0">Selecciona</option>
			<option value="1">VISTA 1</option>
			<option value="2">VISTA 2</option>
			<option value="3">VISTA 3</option>
			<option value="4">VISTA 4</option>
		</select>
	</td>
  <td class="celda_titulo">
	Campo
	</td>
	<td>
		<select id="campo" name="campo" class="caja_texto3">
			<option value="0">Selecciona</option>
			<option value="1">ORDEN</option>
			<option value="2">SOLICITUD</option>
			<option value="3">TELEFONO</option>
		</select>
	</td>
   <td class="celda_res" >    
    <a href="#"><img src="imagenes/boton-consultar.jpg" onclick="listar();"/></a>
	</td>
  </tr>
  </tbody>
  </table>
  </div>
  <div id="lista">
  </div>
  <br>
  <div id="footer">
                <center>      2013 PSI - Planificacion de Soluciones Informaticas
 </div>
</body>
</html>