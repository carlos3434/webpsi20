<?php

include_once "class.ConexionRss.php";

class Boletas {

                public function RetornaCATV($orden,$cliente,$requerimiento) {
		$db = new ConexionRss();
		$cnx = $db->conectarBD();
		
		if($orden!='')
		{
		$fil_orden = "orden='".$orden."'";
		}
		else
		{
		$fil_orden = "";
		}
		
		if($cliente!='')
		{
		$fil_cliente = "cliente='".$cliente."'";
		}
		else
		{
		$fil_cliente = "";
		}
		
		if($requerimiento!='')
		{
		$fil_requerimiento = "requerimiento='".$requerimiento."'";
		}
		else
		{
		$fil_requerimiento = "";
		}
		
		if($orden!='' && $cliente!='')
		{
		$fil_and_oc = " and " ;
		}
		else
		{
		$fil_and_oc = "";
		}
		
		if($cliente!='' && $requerimiento!='')
		{
		$fil_and_cr = " and " ;
		}
		else
		{
		$fil_and_cr = "";
		}
		
		$cad = "SELECT orden, cliente, requerimiento, fecha, zonal, contrata, url FROM `tb_CATV` WHERE $fil_orden $fil_and_oc $fil_cliente $fil_and_cr $fil_requerimiento";
		
		$arr = array();
		
		$res = mysql_query($cad, $cnx) or die(mysql_error());
		while ($row = mysql_fetch_array($res))
		{
			$arr[] = $row;
		}
		return $arr;
	}
	
	public function RetornaSPEEDY($orden,$telefono,$inscripcion) {
	
		$db = new ConexionRss();
		$cnx = $db->conectarBD();
		
		
		
		if($orden!='')
		{
		$fil_orden = "orden='".$orden."'";
		}
		else
		{
		$fil_orden = "";
		}
		
		if($telefono!='')
		{
		$fil_telefono = "telefono='".$telefono."'";
		}
		else
		{
		$fil_telefono = "";
		}
		
		if($inscripcion!='')
		{
		$fil_inscripcion = "inscripcion='".$inscripcion."'";
		}
		else
		{
		$fil_inscripcion = "";
		}
		
		if($orden!='' && $telefono!='')
		{
		$fil_and_ot = " and " ;
		}
		else
		{
		$fil_and_ot = "";
		}
		
		if($telefono!='' && $inscripcion!='')
		{
		$fil_and_ti = " and " ;
		}
		else
		{
		$fil_and_ti = "";
		}

		$cad = "SELECT orden, telefono, inscripcion, fecha, zonal, contrata, url 
		FROM `tb_SPEEDY` WHERE $fil_orden $fil_and_ot $fil_telefono $fil_and_ti $fil_inscripcion";
		
		echo $cad;
		$arr = array();
		
		$res = mysql_query($cad, $cnx) or die(mysql_error());
		while ($row = mysql_fetch_array($res))
		{
			$arr[] = $row;
		}
		return $arr;
	}
	
		public function RetornaTUP($orden,$telefono,$pedido) {
		$db = new ConexionRss();
		$cnx = $db->conectarBD();
		
		if($orden!='')
		{
		$fil_orden = "orden='".$orden."'";
		}
		else
		{
		$fil_orden = "";
		}
		
		if($telefono!='')
		{
		$fil_telefono = "telefono='".$telefono."'";
		}
		else
		{
		$fil_telefono = "";
		}
		
		if($pedido!='')
		{
		$fil_pedido = "pedido='".$pedido."'";
		}
		else
		{
		$fil_pedido = "";
		}
		
		if($orden!='' && $telefono!='')
		{
		$fil_and_ot = " and " ;
		}
		else
		{
		$fil_and_ot = "";
		}
		
		if($telefono!='' && $pedido!='')
		{
		$fil_and_tp = " and " ;
		}
		else
		{
		$fil_and_tp = "";
		}

		$cad = "SELECT pedido, orden, telefono,fecha, zonal, contrata, url FROM `tb_TUP` WHERE $fil_orden $fil_and_ot $fil_telefono $fil_and_tp $fil_pedido";
		
		$arr = array();
		
		$res = mysql_query($cad, $cnx) or die(mysql_error());
		while ($row = mysql_fetch_array($res))
		{
			$arr[] = $row;
		}
		return $arr;
	}
	
	public function RetornaBASICA($solicitud,$orden,$telefono) {
		$db = new ConexionRss();
		$cnx = $db->conectarBD();
		
		if($solicitud!='')
		{
		$fil_solicitud = "solicitud='".$solicitud."'";
		}
		else
		{
		$fil_solicitud = "";
		}
		
		if($orden!='')
		{
		$fil_orden = "orden='".$orden."'";
		}
		else
		{
		$fil_orden = "";
		}
		
		if($telefono!='')
		{
		$fil_telefono = "telefono='".$telefono."'";
		}
		else
		{
		$fil_telefono = "";
		}
		
		if($solicitud!='' && $orden!='')
		{
		$fil_and_so = " and " ;
		}
		else
		{
		$fil_and_so = "";
		}
		
		if($orden!='' && $telefono!='')
		{
		$fil_and_ot = " and " ;
		}
		else
		{
		$fil_and_ot = "";
		}
		
		$cad = "SELECT solicitud, orden, telefono, fecha, zonal, contrata, url FROM `tb_BASICA` WHERE $fil_solicitud $fil_and_so $fil_orden $fil_and_ot $fil_telefono";
		
		$arr = array();
		
		$res = mysql_query($cad, $cnx) or die(mysql_error());
		while ($row = mysql_fetch_array($res))
		{
			$arr[] = $row;
		}
			
		return $arr;
	}
	
		public function Solo_boletas($anio,$mes) {
		$db = new ConexionRss();
		$cnx = $db->conectarBD();
		
		$cad = "SELECT `eecc` AS contrata,
            COUNT(*) AS cantidad,
            'BASICA' AS tipo
            FROM 
            `facturas_2013`.`basica_x_orden_boletas`
            WHERE YEAR(`FechaLiquidacion`) = '".$anio."' AND MONTH(`FechaLiquidacion`) = '".$mes."'
            GROUP BY eecc
            UNION
            SELECT `eecc` AS contrata,
            COUNT(*) AS cantidad,
            'SPEEDY' AS tipo
            FROM 
	   `facturas_2013`.`speedy_x_orden_boletas`
            WHERE YEAR(`FechaLiquidacion`) = '".$anio."' AND MONTH(`FechaLiquidacion`) = '".$mes."'
            GROUP BY eecc
            UNION
            SELECT b.descripcion AS contrata,
            COUNT(*) AS cantidad,
            'CATV' AS tipo
            FROM 
	   `facturas_2013`.`catv_x_orden_boletas` AS a JOIN `facturas_2013`.`tb_contratas` AS b
            ON a.Contrata=b.codigo
            WHERE YEAR(a.FechaLiquidacion) = '".$anio."' AND MONTH(a.FechaLiquidacion) = '".$mes."'
            GROUP BY contrata";
		
		$arr = array();
		
		$res = mysql_query($cad, $cnx) or die("boletas".mysql_error());
		while ($row = mysql_fetch_array($res))
		{
			$arr[] = $row;
		}
			
		return $arr;
	}
	
	public function Solo_noboletas($anio,$mes) {
		$db = new ConexionRss();
		$cnx = $db->conectarBD();
		
		$cad = "SELECT `eecc` AS contrata,
                COUNT(*) AS cantidad,
                'BASICA' AS tipo
                FROM 
                `facturas_2013`.`basica_x_orden_noboletas`
                WHERE YEAR(FechaLiquidacion) = '".$anio."' AND MONTH(FechaLiquidacion) = '".$mes."'
                AND eecc != ' '
                GROUP BY eecc
                UNION
                SELECT 	`eecc` AS contrata,
                COUNT(*) AS cantidad,
                'SPEEDY' AS tipo
                FROM 
                `facturas_2013`.`speedy_x_orden_noboletas`
                WHERE YEAR(FechaLiquidacion) = '".$anio."' AND MONTH(FechaLiquidacion) = '".$mes."'
                AND eecc != ' '
                GROUP BY eecc
                UNION
                SELECT  b.descripcion AS contrata,
                COUNT(*) AS cantidad,
                'CATV' AS tipo
                FROM
                `facturas_2013`.`catv_x_orden_noboletas` AS a JOIN `facturas_2013`.`tb_contratas` AS b
                ON a.Contrata=b.codigo
                WHERE YEAR(a.FechaLiquidacion) = '".$anio."' AND MONTH(a.FechaLiquidacion) = '".$mes."'
                GROUP BY b.descripcion";
		
		$arr = array();
		
		$res = mysql_query($cad, $cnx) or die("noboletas".mysql_error());
		while ($row = mysql_fetch_array($res))
		{
			$arr[] = $row;
		}
			
		return $arr;
	}
	
	public function todas_boletas($anio,$mes) {
		$db = new ConexionRss();
		$cnx = $db->conectarBD();
		
		$cad = "SELECT eecc AS contrata,
                COUNT(*) AS cantidad,
                'BASICA' AS tipo
                FROM 
                `facturas_2013`.`basica_todas_boletas`
                WHERE YEAR(FechaLiquidacion) = '".$anio."' AND MONTH(FechaLiquidacion) = '".$mes."'
                GROUP BY eecc
                UNION
                SELECT 	`eecc` AS contrata,
                COUNT(*) AS cantidad,
                'SPEEDY' AS tipo
                FROM 
                `facturas_2013`.`speedy_todas_boletas` 
                WHERE YEAR(FechaLiquidacion) = '".$anio."' AND MONTH(FechaLiquidacion) = '".$mes."'
                GROUP BY eecc
                UNION 
                SELECT  b.descripcion AS contrata,
                COUNT(*) AS cantidad,
                'CATV' AS tipo
                FROM 
                `facturas_2013`.`catv_todas_boletas` AS a JOIN `facturas_2013`.`tb_contratas` AS b
                ON a.Contrata=b.codigo
                WHERE YEAR(a.FechaLiquidacion) = '".$anio."' AND MONTH(a.FechaLiquidacion) = '".$mes."'
                GROUP BY b.descripcion";
		
		$arr = array();
		
		$res = mysql_query($cad, $cnx) or die("boletas".mysql_error());
		while ($row = mysql_fetch_array($res))
		{
			$arr[] = $row;
		}
			
		return $arr;
	}
	
	public function Solo_contratas($anio,$mes) {
		$db = new ConexionRss();
		$cnx = $db->conectarBD();
		
		$cad = "SELECT DISTINCT contrata FROM    
                (SELECT `eecc` AS contrata,
                COUNT(*) AS cantidad,
                'BASICA' AS tipo
                FROM 
               `facturas_2013`.`basica_x_orden_boletas`
                WHERE YEAR(`FechaLiquidacion`) = '".$anio."' AND MONTH(`FechaLiquidacion`) = '".$mes."'
                GROUP BY eecc
                UNION
                SELECT `eecc` AS contrata,
                COUNT(*) AS cantidad,
                'SPEEDY' AS tipo
                FROM 
               `facturas_2013`.`speedy_x_orden_boletas`
                WHERE YEAR(`FechaLiquidacion`) = '".$anio."' AND MONTH(`FechaLiquidacion`) = '".$mes."'
                GROUP BY eecc
                UNION
                SELECT b.descripcion AS contrata,
                COUNT(*) AS cantidad,
                'CATV' AS tipo
                FROM 
               `facturas_2013`.`catv_x_orden_boletas` AS a JOIN `facturas_2013`.`tb_contratas` AS b
                ON a.Contrata=b.codigo
                WHERE YEAR(a.FechaLiquidacion) = '".$anio."' AND MONTH(a.FechaLiquidacion) = '".$mes."'
                GROUP BY contrata) AS tabla;";
		
		$arr = array();
		
		$res = mysql_query($cad, $cnx) or die("contratas".mysql_error());
		while ($row = mysql_fetch_array($res))
		{
			$arr[] = $row;
		}
			
		return $arr;
	}
	
	public function Solo_contratas2($anio,$mes) {
		$db = new ConexionRss();
		$cnx = $db->conectarBD();
		
		$cad = "SELECT DISTINCT contrata FROM
                (SELECT `eecc` AS contrata,
                COUNT(*) AS cantidad,
                'BASICA' AS tipo
                FROM 
                `facturas_2013`.`basica_x_orden_noboletas`
                WHERE YEAR(FechaLiquidacion) = '".$anio."' AND MONTH(FechaLiquidacion) = '".$mes."'
                AND eecc != ' '
                GROUP BY eecc
                UNION
                SELECT 	`eecc` AS contrata,
                COUNT(*) AS cantidad,
                'SPEEDY' AS tipo
                FROM 
                `facturas_2013`.`speedy_x_orden_noboletas`
                WHERE YEAR(FechaLiquidacion) = '".$anio."' AND MONTH(FechaLiquidacion) = '".$mes."'
                AND eecc != ' '
                GROUP BY eecc
                UNION
                SELECT  b.descripcion AS contrata,
                COUNT(*) AS cantidad,
                'CATV' AS tipo
                FROM
                `facturas_2013`.`catv_x_orden_noboletas` AS a JOIN `facturas_2013`.`tb_contratas` AS b
                ON a.Contrata=b.codigo
                WHERE YEAR(a.FechaLiquidacion) = '".$anio."' AND MONTH(a.FechaLiquidacion) = '".$mes."'
                GROUP BY b.descripcion) AS contratas;";
		
		$arr = array();
		
		$res = mysql_query($cad, $cnx) or die("contratas".mysql_error());
		while ($row = mysql_fetch_array($res))
		{
			$arr[] = $row;
		}
			
		return $arr;
	}
	
	public function Solo_contratas3($anio,$mes) {
		$db = new ConexionRss();
		$cnx = $db->conectarBD();
		
		$cad = "SELECT DISTINCT contrata FROM
                (SELECT eecc AS contrata,
                COUNT(*) AS cantidad,
                'BASICA' AS tipo
                FROM 
                `facturas_2013`.`basica_todas_boletas`
                WHERE YEAR(FechaLiquidacion) = '".$anio."' AND MONTH(FechaLiquidacion) = '".mes."'
                GROUP BY eecc
                UNION
                SELECT 	`eecc` AS contrata,
                COUNT(*) AS cantidad,
                'SPEEDY' AS tipo
                FROM 
                `facturas_2013`.`speedy_todas_boletas` 
                WHERE YEAR(FechaLiquidacion) = '".$anio."' AND MONTH(FechaLiquidacion) = '".$mes."'
                GROUP BY eecc
                UNION 
                SELECT  b.descripcion AS contrata,
                COUNT(*) AS cantidad,
                'CATV' AS tipo
                FROM 
                `facturas_2013`.`catv_todas_boletas` AS a JOIN `facturas_2013`.`tb_contratas` AS b
                ON a.Contrata=b.codigo
                WHERE YEAR(a.FechaLiquidacion) = '".$anio."' AND MONTH(a.FechaLiquidacion) = '".$mes."'
                GROUP BY b.descripcion) AS contratas";
		
		$arr = array();
		
		$res = mysql_query($cad, $cnx) or die("contratas".mysql_error());
		while ($row = mysql_fetch_array($res))
		{
			$arr[] = $row;
		}
			
		return $arr;
	}
	
	public function boletas_excel_basica($anio,$mes,$contrata) {
		$db = new ConexionRss();
		$cnx = $db->conectarBD();
		
		$cad = "SELECT 	`Ciudad`, 
                `Orden`, 
                `Solicitud`, 
                `Telefono`, 
                `Servicio`, 
                `Movimiento`, 
                `Fecha`, 
                `Estado`, 
                `FechaEjecucion`, 
                `FechaReformulacion`, 
                `FechaLiquidacion`, 
                `FechaReal`, 
                `Inscripcion`, 
                `Direccion Postal`, 
                `Direccion Instalacion`, 
                `mdf`, 
                `eecc`
                FROM 
                `facturas_2013`.`basica_x_orden_boletas`
                WHERE YEAR(`FechaLiquidacion`) = '".$anio."' AND MONTH(`FechaLiquidacion`) = '".$mes."' AND eecc = '".$contrata."'";
		
		$arr = array();
		
		$res = mysql_query($cad, $cnx) or die("contratas".mysql_error());
		while ($row = mysql_fetch_array($res))
		{
			$arr[] = $row;
		}
			
		return $arr;
	}
	
	public function boletas_excel_speedy($anio,$mes,$contrata) {
		$db = new ConexionRss();
		$cnx = $db->conectarBD();
		
		$cad = "SELECT 	`Ciudad` AS ciudad, 
                `Orden` AS orden, 
                `Solicitud` AS solicitud, 
                `Telefono` AS telefono, 
                `Servicio` AS servicio, 
                `Movimiento` AS movimiento, 
                `Fecha` AS fecha, 
                `Estado` AS estado, 
                `FechaEjecucion` AS fechaejecucion, 
                `FechaReformulacion` AS fechareformulacion, 
                `FechaLiquidacion` AS fechaliquidacion, 
                `FechaReal` AS fechareal, 
                `Inscripcion` AS inscripcion, 
                `Direccion Postal` AS direccionpostal, 
                `Direccion Instalacion` AS direccioninstalacion, 
                `mdf` AS mdf, 
                `eecc` AS contrata
                FROM 
                `facturas_2013`.`speedy_x_orden_boletas`
                 WHERE YEAR(`FechaLiquidacion`) = '".$anio."' AND MONTH(`FechaLiquidacion`) = '".$mes."' AND eecc = '".$contrata."';";
		
		$arr = array();
		
		$res = mysql_query($cad, $cnx) or die("contratas".mysql_error());
		while ($row = mysql_fetch_array($res))
		{
			$arr[] = $row;
		}
			
		return $arr;
	}
	
       public function boletas_excel_catv($anio,$mes,$contrata) {
		$db = new ConexionRss();
		$cnx = $db->conectarBD();
		
		$cad = "SELECT 	a.Grupo AS grupo, 
                a.Tipo AS tipo, 
                a.Motivo AS motivo, 
                a.Cliente AS cliente, 
                a.ApellidoPaterno AS apellidopaterno, 
                a.ApellidoMaterno AS apellidomaterno, 
                a.Nombres AS nombres, 
                a.Indicador AS indicador, 
                a.Categoria AS categoria, 
                a.Servicio AS servicio, 
                a.Oficina AS oficina, 
                a.Departamento AS departamento, 
                a.Provincia AS provincia, 
                a.Distrito AS distrito, 
                a.Via AS via, 
                a.FechaLiquidacion AS fecha, 
                a.Nodo AS nodo, 
                b.descripcion AS contrata
                FROM 
                `facturas_2013`.`catv_x_orden_boletas` AS a JOIN `tb_contratas` AS b
                ON a.Contrata=b.codigo
                WHERE YEAR(a.FechaLiquidacion) = '".$anio."' AND MONTH(a.FechaLiquidacion) = '".$mes."' AND b.descripcion = '".$contrata."';";
		
		$arr = array();
		
		$res = mysql_query($cad, $cnx) or die("contratas".mysql_error());
		while ($row = mysql_fetch_array($res))
		{
			$arr[] = $row;
		}
			
		return $arr;
	}
                  
        public function noboletas_excel_basica($anio,$mes,$contrata) {
		$db = new ConexionRss();
		$cnx = $db->conectarBD();
		
		$cad = "SELECT 	`Ciudad` AS ciudad, 
                `Orden` AS orden, 
                `Solicitud` AS solicitud, 
                `Telefono` AS telefono, 
                `Servicio` AS servicio, 
                `Movimiento` AS movimiento, 
                `Fecha` AS fecha, 
                `Estado` AS estado, 
                `FechaEjecucion` AS fechaejecucion, 
                `FechaReformulacion` AS fechareformulacion, 
                `FechaLiquidacion` AS fechaliquidacion, 
                `FechaReal` AS fechareal, 
                `Inscripcion` AS inscripcion, 
                `DireccionPostal` AS direccionpostal, 
                `DireccionInstalacion` AS direccioninstalacion, 
                `mdf` AS mdf, 
                `eecc` AS contrata
                FROM 
                `facturas_2013`.`basica_x_orden_noboletas` 
                WHERE YEAR(FechaLiquidacion) = '".$anio."' AND MONTH(FechaLiquidacion) = '".$mes."' AND eecc = '".$contrata."';";
		
		$arr = array();
		
		$res = mysql_query($cad, $cnx) or die("contratas".mysql_error());
		while ($row = mysql_fetch_array($res))
		{
			$arr[] = $row;
		}
			
		return $arr;
	}
        
        public function noboletas_excel_speedy($anio,$mes,$contrata) {
		$db = new ConexionRss();
		$cnx = $db->conectarBD();
		
		$cad = "SELECT 	`Ciudad` AS ciudad, 
                `Orden` AS orden, 
                `Solicitud` AS solicitud, 
                `Telefono` AS telefono, 
                `Servicio` AS servicio, 
                `Movimiento` AS movimiento, 
                `Fecha` AS fecha, 
                `Estado` AS estado, 
                `FechaEjecucion` AS fechaejecucion, 
                `FechaReformulacion` AS fechareformulacion, 
                `FechaLiquidacion` AS fechaliquidacion, 
                `FechaReal` AS fechareal, 
                `Inscripcion` AS inscripcion, 
                `DireccionPostal` AS direccionpostal, 
                `DireccionInstalacion` AS direccioninstalacion, 
                `mdf` AS mdf, 
                `eecc` AS contrata
                FROM 
                `facturas_2013`.`speedy_x_orden_noboletas` 
                WHERE YEAR(FechaLiquidacion) = '".$anio."' AND MONTH(FechaLiquidacion) = '".$mes."' AND eecc = '".$contrata."';";
		
		$arr = array();
		
		$res = mysql_query($cad, $cnx) or die("contratas".mysql_error());
		while ($row = mysql_fetch_array($res))
		{
			$arr[] = $row;
		}
			
		return $arr;
	}
        
      public function noboletas_excel_catv($anio,$mes,$contrata) {
		$db = new ConexionRss();
		$cnx = $db->conectarBD();
		
		$cad = "SELECT 	a.Grupo AS grupo, 
                a.Tipo AS tipo, 
                a.Motivo AS motivo, 
                a.Cliente AS cliente, 
                a.ApellidoPaterno AS apellidopaterno,
                a.ApellidoMaterno AS apellidomaterno,
                a.Nombres AS nombres, 
                a.Indicador AS indicador, 
                a.Categoria AS categoria, 
                a.Servicio AS servicio, 
                a.Oficina AS oficina, 
                a.Departamento AS departamento, 
                a.Provincia AS provincia, 
                a.Distrito AS distrito, 
                a.Via AS via, 
                a.FechaLiquidacion AS fechaliquidacion, 
                a.Nodo AS nodo, 
                b.descripcion as contrata
                FROM 
                `facturas_2013`.`catv_x_orden_noboletas` AS a JOIN `facturas_2013`.`tb_contratas` AS b
                ON a.Contrata=b.codigo
                WHERE YEAR(FechaLiquidacion) = '".$anio."' AND MONTH(FechaLiquidacion) = '".$mes."' AND b.descripcion = '".$contrata."';";
		
		$arr = array();
		
		$res = mysql_query($cad, $cnx) or die("contratas".mysql_error());
		while ($row = mysql_fetch_array($res))
		{
			$arr[] = $row;
		}
			
		return $arr;
	}
        
        public function todas_boletas_excel_basica($anio,$mes,$contrata) {
		$db = new ConexionRss();
		$cnx = $db->conectarBD();
		
		$cad = "SELECT 	`Ciudad`, 
                `Orden`, 
                `Solicitud`, 
                `Telefono`, 
                `Servicio`, 
                `Movimiento`, 
                `Fecha`, 
                `Estado`, 
                `FechaEjecucion`, 
                `FechaReformulacion`, 
                `FechaLiquidacion`, 
                `FechaReal`, 
                `Inscripcion`, 
                `DireccionPostal`, 
                `DireccionInstalacion`, 
                `mdf`, 
                `eecc`
                FROM 
                `facturas_2013`.`basica_todas_boletas` 
                WHERE YEAR(FechaLiquidacion) = '".$anio."' AND MONTH(FechaLiquidacion) = '".$mes."' AND eecc = '".$contrata."';";
		
		$arr = array();
		
		$res = mysql_query($cad, $cnx) or die("contratas".mysql_error());
		while ($row = mysql_fetch_array($res))
		{
			$arr[] = $row;
		}
			
		return $arr;
	}
        
         public function todas_boletas_excel_speedy($anio,$mes,$contrata) {
		$db = new ConexionRss();
		$cnx = $db->conectarBD();
		
		$cad = "SELECT 	`Ciudad`, 
                `Orden`, 
                `Solicitud`, 
                `Telefono`, 
                `Servicio`, 
                `Movimiento`, 
                `Fecha`, 
                `Estado`, 
                `FechaEjecucion`, 
                `FechaReformulacion`, 
                `FechaLiquidacion`, 
                `FechaReal`, 
                `Inscripcion`, 
                `DireccionPostal`, 
                `DireccionInstalacion`, 
                `mdf`, 
                `eecc`
                FROM 
                `facturas_2013`.`speedy_todas_boletas` 
                WHERE YEAR(FechaLiquidacion) = '".$anio."' AND MONTH(FechaLiquidacion) = '".$mes."' AND eecc = '".$contrata."';";
		
		$arr = array();
		
		$res = mysql_query($cad, $cnx) or die("contratas".mysql_error());
		while ($row = mysql_fetch_array($res))
		{
			$arr[] = $row;
		}
			
		return $arr;
	}
        
        public function todas_boletas_excel_catv($anio,$mes,$contrata) {
		$db = new ConexionRss();
		$cnx = $db->conectarBD();
		
		$cad = "SELECT 	a.Grupo, 
                a.Tipo as tipo, 
                a.Motivo as motivo, 
                a.Cliente as cliente, 
                a.ApellidoPaterno as apellidopaterno, 
                a.ApellidoMaterno as apellidomaterno, 
                a.Nombres as nombres, 
                a.Indicador as indicador, 
                a.Categoria as categoria, 
                a.Servicio as servicio, 
                a.Oficina as oficina, 
                a.Departamento as departamento, 
                a.Provincia as provincia, 
                a.Distrito as distrito, 
                a.Via as via, 
                a.FechaLiquidacion as fechaliquidacion, 
                a.Nodo as nodo, 
                b.descripcion as contrata
                FROM 
                `facturas_2013`.`catv_todas_boletas` AS a JOIN `facturas_2013`.`tb_contratas` AS b
                ON a.Contrata=b.codigo
                WHERE YEAR(FechaLiquidacion) = '".$anio."' AND MONTH(FechaLiquidacion) = '".$mes."' AND b.descripcion = '".$contrata."';";
		
		$arr = array();
		
		$res = mysql_query($cad, $cnx) or die("contratas".mysql_error());
		while ($row = mysql_fetch_array($res))
		{
			$arr[] = $row;
		}
	
		return $arr;
	}
}

    
	
	