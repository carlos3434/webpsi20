<?php 
require_once("clases/class.Boletas.php");

    // obtenemos los datos del archivo
    $anio = $_GET["anio"];
    $mes = $_GET["mes"];
    $contrata = $_GET["contrata"];
	
    $boleta = new  Boletas();

    $b = $boleta->boletas_excel_speedy($anio,$mes,$contrata);

        $file = $anio.'-'.$mes.'-'.$contrata.".xls";
        ob_start();

	if (count($b)!=0) {
	?>

	<table class="tablaClonar" >
    <tr>
                <td class="celda_titulo"> CIUDAD </td>
                <td class="celda_titulo"> ORDEN </td>
                <td class="celda_titulo"> SOLICITUD </td>
                <td class="celda_titulo"> TELEFONO </td>
                <td class="celda_titulo"> SERVICIO </td> 
                <td class="celda_titulo"> MOVIMIENTO </td>
                <td class="celda_titulo"> FECHA </td>
                <td class="celda_titulo"> ESTADO </td>
                <td class="celda_titulo"> FECHA EJECUCION </td>
                <td class="celda_titulo"> FECHA REFORMULACION </td>
                <td class="celda_titulo"> FECHA LIQUIDACION </td> 
                <td class="celda_titulo"> FECHA REAL </td>
                <td class="celda_titulo"> INSCRIPCION </td>
                <td class="celda_titulo"> DIRECCION POSTAL </td>
                <td class="celda_titulo"> DIRECCION INSTALACION </td> 
                <td class="celda_titulo"> MDF </td>
                <td class="celda_titulo"> CONTRATA </td>
	</tr>
	<?php
	foreach ($b as $fila) { 
	?>
	<tr>
		<td class="celda_res"><?php echo $fila["ciudad"]?></td>
		<td class="celda_res"><?php echo $fila["orden"]?></td>
		<td class="celda_res"><?php echo $fila["solicitud"]?></td>
		<td class="celda_res"><?php echo $fila["telefono"]?></td>
		<td class="celda_res"><?php echo $fila["servicio"]?></td>
		<td class="celda_res"><?php echo $fila["movimiento"]?></td>
		<td class="celda_res"><?php echo $fila["fecha"]?></td>
		<td class="celda_res"><?php echo $fila["estado"]?></td>
		<td class="celda_res"><?php echo $fila["fechaejecucion"]?></td>
		<td class="celda_res"><?php echo $fila["fechareformulacion"]?></td>
		<td class="celda_res"><?php echo $fila["fechaliquidacion"]?></td>
		<td class="celda_res"><?php echo $fila["fechareal"]?></td>
		<td class="celda_res"><?php echo $fila["inscripcion"]?></td>
		<td class="celda_res"><?php echo $fila["direccionpostal"]?></td>
		<td class="celda_res"><?php echo $fila["direccioninstalacion"]?></td>
		<td class="celda_res"><?php echo $fila["mdf"]?></td>
		<td class="celda_res"><?php echo $fila["contrata"]?></td>
	</tr>
	<?php
	}    
    ?>
	</table>

	 <?php
} 

        $contenido = ob_get_contents();
        ob_end_clean();
        header("Expires : 0");
        header("Content-Type: application/vnd.ms-excel");
        header("Content-disposition: attachment; filename=$file");
        echo $contenido;
?>