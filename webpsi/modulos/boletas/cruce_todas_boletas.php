<?php
//require_once("clases/class.ConexionSigas.php");
require_once("../../cabecera.php");

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
    <head>
        <title>Boletas</title>
        <meta http-equiv="content-type" content="text/html; charset=iso-8859-1">
            <meta name="author" content="Sergio MC" />
            <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">

<?php include ("../../includes.php") ?> 

<script src="jquery-ui-1.10.3/jquery-1.9.1.js"></script>

<script type="text/javascript">
function consulta_01()
{
    var anio = $("#anio").val();   
	var mes = $("#mes").val();

    if (anio != 0 && mes !=0){
	$.ajax({
        type: "POST",
        url: "cruce_todas_boletas_ajax.php",
        data: {
            todas_boletas: '1',
            anio: anio,
			mes: mes
        }
    }).done(function( msg ) {
		//alert(msg);
		$("#resultado").html(msg);					
    });
	}
	else {
	alert("Seleccione los criterios de busqueda");
	}    
		
}	
			
</script>

<link rel="stylesheet" type="text/css" href="estiloAdmin.css">
<link rel="stylesheet" type="text/css" href="buttons.css">

</head>
<body>


<?php
echo pintar_cabecera(); 
?>

<br/>

<div id="div_Clonar" class="divClonar">

<div id="div_Clonar" class="divClonar" style="width:300px;">
<table class="tablaClonar" >
<tbody>
<tr>
    <td class="celda_titulo">
      A�o
    </td>
    <td class="celda_res" >
        <select id="anio" name="anio" class="caja_texto3">
			<option value="0">Selecciona</option>
			<option value="2009">2009</option>
			<option value="2010">2010</option>
			<option value="2011">2011</option>
			<option value="2012">2012</option>
			<option value="2013">2013</option>
		</select>
    </td>
</tr>
<tr>
    <td class="celda_titulo">Mes</td>
    <td class="celda_res" >
        <select id="mes" name="mes" class="caja_texto3">
			<option value="0">Selecciona</option>
			<option value="1">Enero</option>
			<option value="2">Febrero</option>
			<option value="3">Marzo</option>
			<option value="4">Abril</option>
			<option value="5">Mayo</option>
			<option value="6">Junio</option>
			<option value="7">Julio</option>
			<option value="8">Agosto</option>
			<option value="9">Setiembre</option>
			<option value="10">Octubre</option>
			<option value="11">Noviembre</option>
			<option value="12">Diciembre</option>
		</select>
    </td>
</tr>
<tr>
    <td class="celda_titulo">&nbsp;</td>
    <td class="celda_res" >    
    <a href="#"><img src="imagenes/boton-consultar.jpg" onclick="consulta_01();"/></a>
	</td>
</tr>
</tbody>
</table>
</div>

</div>

<br/>

<div id="resultado"><div>

 <div id="footer">
                <center>      2013 PSI - Planificacion de Soluciones Informaticas
 </div>
</body>
</html>