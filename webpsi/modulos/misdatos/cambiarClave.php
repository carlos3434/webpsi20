<?php
/**
 * Short description for file
 *
 * Long description for file (if any)...
 *
 * @package    cambiarClave.php
 * @author     Sergio Miranda C.
 * @copyright  2013

 */


require_once("../../cabecera.php");
require_once("../../clases/class.Usuario.php");

$usuario = new Usuario();

$idusuario_modelo = $_REQUEST["idusuario"];

//echo $idusuario;

?>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
    <head>
        <title>PSI - Web SMS - Cambio de Clave</title>
        <meta http-equiv="content-type" content="text/html; charset=iso-8859-1">
            <meta name="author" content="Sergio MC" />
            <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">

                <?php include ("../../includes.php") ?>                

<!--<script type="text/javascript" src="../js/js.js"></script>-->




<script type="text/javascript">

function randompass(length){
var letras = new Array();
letras[1]="a";
letras[2]="b";
letras[3]="c";
letras[4]="d";
letras[5]="e";
letras[6]="f";
letras[7]="g";
letras[8]="h";
letras[9]="i";
letras[10]="j";
letras[11]="k";
letras[12]="l";
letras[13]="m";
letras[14]="n";
letras[15]="o";
letras[16]="p";
letras[17]="q";
letras[18]="r";
letras[19]="s";
letras[20]="t";
letras[21]="u";
letras[22]="w";
var pass = "";
var numero = 0;
for(var i=1;i<length;i++){
    numero = Math.floor(Math.random()*(22-1))+1;
    if(i==1){
        pass+=numero;
    }else{
        pass+=letras[numero];
    }        
}
numero = Math.floor(Math.random()*(22-1))+1;
pass+=numero;
return pass;
}

function validar_requerido(field,alerttxt)
{
	var value = $(field).val();
	if (value==null||value==""||value==0)
		{alert(alerttxt);return false}
	else {return true}
}



$(document).ready(function(){

	$("#txtActualPasswd").focus(); //
	
	$('#btnsalir').click(function(){
        $("#childModal").dialog('close');
    });
	
	$('#btnGuardarCambios').click(function(){
        var passwdActual = $("#txtActualPasswd");
		var passwdNueva1 = $("#txtNuevaPasswd1");
		var passwdNueva2 = $("#txtNuevaPasswd2");
		var idUsuario 	 = $("#txt_idusuario");

		if (validar_requerido(passwdActual,"Falta llenar el campo Password Actual.")==false)
		  { passwdActual.focus();return false}
		if (validar_requerido(passwdNueva1,"Falta llenar el campo Nueva Password.")==false)
		  {passwdNueva1.focus();return false}
		if (validar_requerido(passwdNueva2,"Falta llenar el campo Nueva Password (repeticion).")==false)
		  {passwdNueva2.focus();return false}

		if (passwdNueva1.val().length < 6 ) {
			alert("Las nueva contrase"+'\u00f1'+"a debe tener minimo 6 caracteres.");
			return false;
		}


		if (passwdNueva1.val()!=passwdNueva2.val()) {
			alert("Las nuevas contrase"+'\u00f1'+"as no coinciden. Revisar.");
			return false;
		}

		// todo conforme, se procede a clonar usuario
		var pagina="receptor_cambiarClave.php";
		$.ajax({
	        type: "POST",
	        url: pagina,
	        data: {
				action: 'cambiarClave',
				idUsuario: idUsuario.val(),
				passwdActual: passwdActual.val(),
				passwdNueva1: passwdNueva1.val(),
				passwdNueva2: passwdNueva2.val()
			},
	        success: function(html){
				alert(html);
	        }
	    });

		
		
			
		  
    });	
	

});
                    

                 
</script>

<link rel="stylesheet" type="text/css" href="../../estilos.css">
<link rel="stylesheet" type="text/css" href="estiloAdmin.css">
<link rel="stylesheet" type="text/css" href="buttons.css">
</head>

<body>
	<?php echo pintar_cabecera(); ?>
	<p>
		<br/>
	</p>
	<input type="hidden" value="<?php echo $IDUSUARIO?>" name="txt_idusuario" id="txt_idusuario"/>
	
	<div id="div_Clonar" class="divClonar">
	<table class="tablaClonar"  >
	<tr>
		<td class="celda_titulo">Password actual:</td>
		<td class="celda_res"  colspan="1">
			<input type="password" name="txtActualPasswd" id="txtActualPasswd" class="caja_texto3" maxlength="8" />
		</td>
	</tr>
	<tr>
		<td class="celda_titulo">Nueva Password:</td>
		<td class="celda_res"  colspan="1">
			<input type="password" name="txtNuevaPasswd1" id="txtNuevaPasswd1" class="caja_texto3" maxlength="8" />
		</td>
	</tr>
	<tr>
		<td class="celda_titulo">Repetir Nueva Password:</td>
		<td class="celda_res"  colspan="1">
			<input type="password" name="txtNuevaPasswd2" id="txtNuevaPasswd2" class="caja_texto3" maxlength="8" />
		</td>
	</tr>

	<tr>
		<td class="celda_res" colspan="3" align="center">
        
		<button id="btnGuardarCambios" class="action blue" title="Guardar Cambios">
			<span class="label">Guardar Cambios</span>
		</button>
		<button id="btnsalir" class="action red" title="Cancelar">
			<span class="label">Salir</span>
		</button>
		
		</td>
	</tr>

	</table>
		
	</div>

</body>
</html>
