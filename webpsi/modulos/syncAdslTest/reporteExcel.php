<?php
/** PHPExcel */
error_reporting(E_ALL);
ini_set('display_errors', TRUE);
ini_set('display_startup_errors', TRUE);

/** Include PHPExcel */
require_once("../../clases/PHPExcel/PHPExcel.php");
require_once("../../clases/class.Conexion.php");

$objCnx = new Conexion();
$db = $objCnx->conectarBD();

$reqExcel = "SELECT codigo_prueba,
			codArea,
			telefono,
			horaInicio,
			horaTermino,
			solId,
			pruebaEstado,
			pruebaDescripcion,
			perfilEstado,
			perfilDescripcion,
			velocidadActualSubidaEstado,
			velocidadActualSubidaDescripcion,
			velocidadActualBajadaEstado,
			velocidadActualBajadaDescripcion,
			velocidadMaximaSubidaDescripcion,
			velocidadMaximaBajadaDescripcion,
			senalRuidoSubidaEstado,
			senalRuidoSubidaDescripcion,
			senalRuidoBajadaEstado,
			senalRuidoBajadaDescripcion,
			atenuacionUpStreamDescripcion,
			atenuacionDownStreamDescripcion,
			ocupacionUpStreamEstado,
			ocupacionUpStreamDescripcion,
			ocupacionDownStreamEstado,
			ocupacionDownStreamDescripcion,
			vendorDescripcion,
			dslamDescripcion,
			posicionDescripcion,
			mdfDescripcion,
			portTypeDescripcion,
			callingStationIdDescripcion,
			vpiVciModemDescripcion,
			vpiVciRedDescripcion,
			userNameDescripcion,
			ispLoginDescripcion,
			ispPasswordDescripcion,
			nasIpAddressERXDescripcion,
			numeroInscripcionDescripcion,
			estadoCuentaDescripcion,
			nasPortIdDescripcion,
			codigoProductoDescripcion,
			descripcionServicioDescripcion,
			virtualRouterEstado,
			virtualRouterDescripcion,
			resultado,
			resumen,
			resultadoHtml,
			tiempoTranscurrido,
			b.usuario,
			CONCAT(b.apellido, ' ', b.nombre) as nombre_usuario
			FROM pcba.pruebas_internetsocial a, envio_sms.tb_usuario b WHERE b.id=a.idusuario ; ";
			
//$reqExcel = "SELECT * FROM agenda_empresas.contratas ; ";
$resultat = mysql_query($reqExcel, $db) or die(mysql_error($db));
$numoffields=mysql_num_fields($resultat);
$entete_champs =1;


function col2chr($a) {
	$a=$a+1;
	$b=0;

	if($a<27){

		return strtoupper(chr($a+96));  

	} else {
		//echo "FF";
		while($a > 26){
			$b++;
			$a = $a-26;              
		}	                
		$b = strtoupper(chr($b+96));  
		$a = strtoupper(chr($a+96));              
		return $b.$a;
	}		
}

 

// Create new PHPExcel object
$objPHPExcel = new PHPExcel();

// Set properties
$objPHPExcel->getProperties()->setCreator("Sistemas")
			->setLastModifiedBy("Sistemas")
			->setTitle("Office 2007 XLSX Test Document")
			->setSubject("Office 2007 XLSX Test Document")
			->setDescription("Test document for Office 2007 XLSX.")
			->setKeywords("office 2007 openxml")
			->setCategory("File") ;

		
 // Add some data
for ($entete_champs=0; $entete_champs<$numoffields;$entete_champs++) {
	$objPHPExcel->setActiveSheetIndex(0)->setCellValueByColumnAndRow($entete_champs,1, mysql_field_name($resultat,$entete_champs));
	$lettre_colonne = col2chr($entete_champs); //."1";
	$lettre_colonne = $lettre_colonne.'1';
	$objPHPExcel->getActiveSheet()->getStyle($lettre_colonne)->getFont()->setBold(true);
	$objPHPExcel->getActiveSheet()->getStyle($lettre_colonne)->getFont()->getColor()->setARGB(PHPExcel_Style_Color::COLOR_BLUE);
}


$i = 2; 
while($row = mysql_fetch_array($resultat))
{
	for ($j=0; $j<$numoffields;$j++) {
		$objPHPExcel->setActiveSheetIndex(0)->setCellValueByColumnAndRow($j,$i, $row[$j]);  
	}
	$i++;
}

 

// Rename sheet
$objPHPExcel->getActiveSheet()->setTitle('ReporteIS');


// Set active sheet index to the first sheet, so Excel opens this as the first sheet
$objPHPExcel->setActiveSheetIndex(0);

/*
// Redirect output to a client’s web browser (Excel5)
header('Content-Type: application/vnd.ms-excel');
header('Content-Disposition: attachment;filename="reporte_IS.xls"');
header('Cache-Control: max-age=0');

$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter->save('php://output');

exit;
*/

// Redirect output to a client’s web browser (Excel2007)
header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="reporte_IS.xlsx"');
header('Cache-Control: max-age=0');

$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
$objWriter->save('php://output');
exit;


?>

