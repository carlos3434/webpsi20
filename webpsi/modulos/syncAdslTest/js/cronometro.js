
var CronoID = null;
var CronoEjecutandose = false;
var decimas, segundos, minutos

function DetenerCrono (){
   	if(CronoEjecutandose)
   		clearTimeout(CronoID);
   	CronoEjecutandose = false;
}

function InicializarCrono () {
	//inicializa contadores globales
  
	decimas = 0;
	segundos = 0;
	minutos = 0;
	
	//pone a cero los marcadores
	$('#cronometro_prueba').html('Tiempo transcurrido: 00:00:0');
	
}

function MostrarCrono () {
	       
   	//incrementa el crono
   	decimas++;
	if ( decimas > 9 ) {
		decimas = 0;
		segundos++;
		if ( segundos > 59 ) {
			segundos = 0;
			minutos++;
			if ( minutos > 99 ) {
				
				DetenerCrono();
				return true;
			}
		}
	}

	//configura la salida
	var ValorCrono = ""
	ValorCrono = (minutos < 10) ? "0" + minutos : minutos;
	ValorCrono += (segundos < 10) ? ":0" + segundos : ":" + segundos;
	ValorCrono += ":" + decimas	;
			
  	$('#cronometro_prueba').html('Tiempo transcurrido: '+ValorCrono);

  	CronoID = setTimeout("MostrarCrono()", 100);
	CronoEjecutandose = true;
	return true
}

function IniciarCrono () {
 	DetenerCrono();
 	InicializarCrono();
	MostrarCrono();
}

function ObtenerParcial() {
	//obtiene cuenta parcial
	//document.crono.parcial.value = document.crono.display.value;
}

