<?php
require_once("../../clases/class.Conexion.php");

include 'funciones.php';

session_start();
//@ is authorized?
if (empty($_SESSION['exp_user']) || @$_SESSION['exp_user']['expires'] < time()) {
	$RUTA = "/webpsi/";
    header("location:".$RUTA."login/login.html"); //@ redirect
} else {
    $_SESSION['exp_user']['expires'] = time() + (45 * 60); //@ renew 45 minutes
}

$IDUSUARIO = $_SESSION['exp_user']['id'];
$USUARIO = $_SESSION['exp_user']['usuario'];

$statusPrueba = "0";


function cambio_fecha2($date)   // de fechas YYYYmmdd pasar a YYYY-mm-dd
{
	$year = substr($date, 0,4) ;
	$mes = substr($date, 4,2) ;
	$dia = substr($date,6,2) ;
	
	return $year."-".$mes."-".$dia;

}

if($_POST["codArea"]!="" && $_POST["telefono"]!="")
{


	//$url="http://172.28.11.66/PCBASINAPPLET/ws_pruebas_ver_online.php?codArea=".$_POST["codArea"]."&telefono=".$_POST["telefono"];
	$url="http://172.28.13.119/amfphp/services/ws_pdm_request.php?CodArea=".$_POST["codArea"]."&Telefono=".$_POST["telefono"];
	//echo $url;

	$_SESSION["xml"] = new DOMDocument();
	$_SESSION["xml"] -> load($url);

	$dom = $_SESSION["xml"] -> getElementsByTagName("PRUEBAS_PCBA");
	
	

	$valor1 = leerxmldatos("Area")  ; 
	$valor2 = leerxmldatos("Telefono")  ; 
	$valor3 = leerxmldatos("Inicio")  ; 
	$valor4 = leerxmldatos("Termino")  ; 
	$valor5 = leerxmldatos("SolId")  ; 
	$valor6 = leerxmlprueba("PruebaDslam","EstadoPrueba","Estado")  ; 
	$valor7 = leerxmlprueba("PruebaDslam","EstadoPrueba","Descripcion")  ; 
	$valor8 = leerxmlprueba("PruebaDslam","PerfilConfigurado","Estado")  ; 
	$valor9 = leerxmlprueba("PruebaDslam","PerfilConfigurado","Descripcion")  ; 
	$valor10 = leerxmlprueba("PruebaDslam","VelocidadActualSubida","Estado")  ; 
	$valor11 = leerxmlprueba("PruebaDslam","VelocidadActualSubida","Descripcion")  ; 
	$valor12 = leerxmlprueba("PruebaDslam","VelocidadActualBajada","Estado")  ; 
	$valor13 = leerxmlprueba("PruebaDslam","VelocidadActualBajada","Descripcion")  ; 
	$valor14 = leerxmlprueba("PruebaDslam","VelocidadMaximaSubida","Descripcion")  ; 
	$valor15 = leerxmlprueba("PruebaDslam","VelocidadMaximaBajada","Descripcion")  ; 
	$valor16 = leerxmlprueba("PruebaDslam","SenalRuidoSubida","Estado")  ; 
	$valor17 = leerxmlprueba("PruebaDslam","SenalRuidoSubida","Descripcion")  ; 
	$valor18 = leerxmlprueba("PruebaDslam","SenalRuidoBajada","Estado")  ; 
	$valor19 = leerxmlprueba("PruebaDslam","SenalRuidoBajada","Descripcion")  ; 
	$valor20 = leerxmlprueba("PruebaDslam","AtenuacionUpstream","Descripcion")  ; 
	$valor21 = leerxmlprueba("PruebaDslam","AtenuacionDownstream","Descripcion")  ; 
	$valor22 = leerxmlprueba("PruebaDslam","OcupacionUpstream","Estado")  ; 
	$valor23 = leerxmlprueba("PruebaDslam","OcupacionUpstream","Descripcion")  ; 
	$valor24 = leerxmlprueba("PruebaDslam","OcupacionDownstream","Estado")  ; 
	$valor25 = leerxmlprueba("PruebaDslam","OcupacionDownstream","Descripcion")  ; 
	$valor26 = leerxmlprueba("PruebaDslam","Vendor","Descripcion")  ; 
	$valor27 = leerxmlprueba("PruebaDslam","DSLAM","Descripcion")  ; 
	$valor28 = leerxmlprueba("PruebaDslam","Posicion","Descripcion")  ; 
	$valor29 = leerxmlprueba("PruebaDslam","MDF","Descripcion")  ; 
	$valor30 = leerxmlprueba("PruebaDslam","PortType","Descripcion")  ; 
	$valor31 = leerxmlprueba("PruebaDslam","CallingStationId","Descripcion")  ; 
	$valor32 = leerxmlprueba("PruebaDslam","VpiVciModem","Descripcion")  ; 
	$valor33 = leerxmlprueba("PruebaDslam","VpiVciRed","Descripcion")  ; 
	$valor34 = leerxmlprueba("PruebaRadian","Username","Descripcion")  ; 
	$valor35 = leerxmlprueba("LDAP","ISPLogin","Descripcion")  ; 
	$valor36 = leerxmlprueba("LDAP","ISPPassword","Descripcion")  ; 
	$valor37 = leerxmlprueba("LDAP","NASIpAddressERX","Descripcion")  ; 
	$valor38 = leerxmlprueba("LDAP","NumeroInscripcion","Descripcion")  ; 
	$valor39 = leerxmlprueba("LDAP","EstadoCuenta","Descripcion")  ; 
	$valor40 = leerxmlprueba("LDAP","NASPortId","Descripcion")  ; 
	$valor41 = leerxmlprueba("LDAP","CodigoProducto","Descripcion")  ; 
	$valor42 = leerxmlprueba("LDAP","DescripcionServicio","Descripcion")  ; 
	$valor43 = leerxmlprueba("LDAP","VirtualRouter","Estado")  ; 
	$valor44 = leerxmlprueba("LDAP","VirtualRouter","Descripcion")  ; 
	$valor45 = resultado(1)  ; 
	$valor46 = resultado(2)  ; 
	$valor47 = resultado(3)   ; 
	$valor48 = $IDUSUARIO ; 

	$objCnx = new Conexion();
	$cnx = $objCnx->conectarPDO();



	try {

		
		$cnx->beginTransaction();   
		
		$query = "INSERT INTO `pcba`.`pruebas_internetsocial`
		(codArea,
		telefono,
		horaInicio,
		horaTermino,
		solId,
		pruebaEstado,
		pruebaDescripcion,
		perfilEstado,
		perfilDescripcion,
		velocidadActualSubidaEstado,
		velocidadActualSubidaDescripcion,
		velocidadActualBajadaEstado,
		velocidadActualBajadaDescripcion,
		velocidadMaximaSubidaDescripcion,
		velocidadMaximaBajadaDescripcion,
		senalRuidoSubidaEstado,
		senalRuidoSubidaDescripcion,
		senalRuidoBajadaEstado,
		senalRuidoBajadaDescripcion,
		atenuacionUpStreamDescripcion,
		atenuacionDownStreamDescripcion,
		ocupacionUpStreamEstado,
		ocupacionUpStreamDescripcion,
		ocupacionDownStreamEstado,
		ocupacionDownStreamDescripcion,
		vendorDescripcion,
		dslamDescripcion,
		posicionDescripcion,
		mdfDescripcion,
		portTypeDescripcion,
		callingStationIdDescripcion,
		vpiVciModemDescripcion,
		vpiVciRedDescripcion,
		userNameDescripcion,
		ispLoginDescripcion,
		ispPasswordDescripcion,
		nasIpAddressERXDescripcion,
		numeroInscripcionDescripcion,
		estadoCuentaDescripcion,
		nasPortIdDescripcion,
		codigoProductoDescripcion,
		descripcionServicioDescripcion,
		virtualRouterEstado,
		virtualRouterDescripcion,
		resultado,
		resumen,
		resultadoHtml, 
		idusuario  ) 
		VALUES (
		:valor1,
		:valor2,
		:valor3,
		:valor4,
		:valor5,
		:valor6,
		:valor7,
		:valor8,
		:valor9,
		:valor10,
		:valor11,
		:valor12,
		:valor13,
		:valor14,
		:valor15,
		:valor16,
		:valor17,
		:valor18,
		:valor19,
		:valor20,
		:valor21,
		:valor22,
		:valor23,
		:valor24,
		:valor25,
		:valor26,
		:valor27,
		:valor28,
		:valor29,
		:valor30,
		:valor31,
		:valor32,
		:valor33,
		:valor34,
		:valor35,
		:valor36,
		:valor37,
		:valor38,
		:valor39,
		:valor40,
		:valor41,
		:valor42,
		:valor43,
		:valor44,
		:valor45,
		:valor46,
		:valor47,
		:valor48  ); " ;
		
		$res = $cnx->prepare($query) ;
			
		$res->bindParam(':valor1', $valor1 ) ; 
		$res->bindParam(':valor2', $valor2 ) ; 
		$res->bindParam(':valor3', $valor3 ) ; 
		$res->bindParam(':valor4', $valor4 ) ; 
		$res->bindParam(':valor5', $valor5 ) ; 
		$res->bindParam(':valor6', $valor6 ) ; 
		$res->bindParam(':valor7', $valor7 ) ; 
		$res->bindParam(':valor8', $valor8 ) ; 
		$res->bindParam(':valor9', $valor9 ) ; 
		$res->bindParam(':valor10', $valor10 ) ; 
		$res->bindParam(':valor11', $valor11 ) ; 
		$res->bindParam(':valor12', $valor12 ) ; 
		$res->bindParam(':valor13', $valor13 ) ; 
		$res->bindParam(':valor14', $valor14 ) ; 
		$res->bindParam(':valor15', $valor15 ) ; 
		$res->bindParam(':valor16', $valor16 ) ; 
		$res->bindParam(':valor17', $valor17 ) ; 
		$res->bindParam(':valor18', $valor18 ) ; 
		$res->bindParam(':valor19', $valor19 ) ; 
		$res->bindParam(':valor20', $valor20 ) ; 
		$res->bindParam(':valor21', $valor21 ) ; 
		$res->bindParam(':valor22', $valor22 ) ; 
		$res->bindParam(':valor23', $valor23 ) ; 
		$res->bindParam(':valor24', $valor24 ) ; 
		$res->bindParam(':valor25', $valor25 ) ; 
		$res->bindParam(':valor26', $valor26 ) ; 
		$res->bindParam(':valor27', $valor27 ) ; 
		$res->bindParam(':valor28', $valor28 ) ; 
		$res->bindParam(':valor29', $valor29 ) ; 
		$res->bindParam(':valor30', $valor30 ) ; 
		$res->bindParam(':valor31', $valor31 ) ; 
		$res->bindParam(':valor32', $valor32 ) ; 
		$res->bindParam(':valor33', $valor33 ) ; 
		$res->bindParam(':valor34', $valor34 ) ; 
		$res->bindParam(':valor35', $valor35 ) ; 
		$res->bindParam(':valor36', $valor36 ) ; 
		$res->bindParam(':valor37', $valor37 ) ; 
		$res->bindParam(':valor38', $valor38 ) ; 
		$res->bindParam(':valor39', $valor39 ) ; 
		$res->bindParam(':valor40', $valor40 ) ; 
		$res->bindParam(':valor41', $valor41 ) ; 
		$res->bindParam(':valor42', $valor42 ) ; 
		$res->bindParam(':valor43', $valor43 ) ; 
		$res->bindParam(':valor44', $valor44 ) ; 
		$res->bindParam(':valor45', $valor45 ) ; 
		$res->bindParam(':valor46', $valor46 ) ; 
		$res->bindParam(':valor47', $valor47 ) ; 
		$res->bindParam(':valor48', $valor48 ) ; 

		//$exec_sql_codcontrol=mysql_query($exec_sql_codcontrol, $cnx) or die ("Error el ingresar datos ".$sql_codcontrol.mysql_error($cnx));

		$res->execute();
		
		$idPrueba = $cnx->lastInsertId();
		$anyoPrueba = date("Y");
		$codPrueba = "PS-".$anyoPrueba."-".str_pad( $idPrueba, 6, "0", STR_PAD_LEFT);
		
		
		$sql2 = "UPDATE `pcba`.`pruebas_internetsocial` SET codigo_prueba=:codPrueba WHERE id=:id ";
		$res2 = $cnx->prepare($sql2);
		$res2->bindParam(":codPrueba", $codPrueba);
		$res2->bindParam(":id", $idPrueba);
		$res2->execute();

		$cnx->commit();
		$statusPrueba = "1";
		
	}
	catch (PDOException $e)
	{
		$cnx->rollBack();
		print ("Error: No se pudo registrar la prueba -- ".$e->getMessage () . "\n");
		$statusPrueba = "0";
	}

	$cnx = NULL;
	$db = NULL;

	echo "<br/>";
	?>
	&nbsp;
	
	<?php
		echo "<div>";
		
		if ($statusPrueba == "1") 
			echo "<br/>Codigo de Prueba: <span style=' font-weight: bold; font-size: 14px; color: red; '>".$codPrueba." </span>";
		else
			echo " -- Ocurrio un error, no se pudo realizar la prueba correctamente.";
		echo "</div>";
	?>
	<br>
	<span><b>
	Pruebas Sincronismo ADSL:
	</span></b>
	</div>


	<br>
	<br>
	<div id="divmain" class="div_main">
	<span><b>Datos Entrada:</span></b>
	<br>
		<div id="divmain1" class="div_main">
		  <table class='table'>
			<tr>
			  <td class="celda_titulo" >Area</td>
			  <td class="celda_res" >
			  <?php echo leerxmldatos("Area"); ?>
			  </td>
			  <td class="celda_titulo" >Telefono</td>
			  <td class="celda_res" >
			  <?php echo leerxmldatos("Telefono"); ?>
			  </td>
			</tr>
			<tr>
			  <td class="celda_titulo" >Inicio</td>
			  <td class="celda_res">
			  <?php echo cambio_fecha_human(leerxmldatos("Inicio")); ?>
			  </td>
			  <td class="celda_titulo">Termino</td>
			  <td class="celda_res">
			  <?php echo cambio_fecha_human(leerxmldatos("Termino")); ?>
			  </td>
			</tr>
		  </table>
		</div> 
		<br> 
		<br>

		<span><b>
		Prueba DSLAM:
		</span></b>

		<br>
		<br>
		<div id="divmain2" class="div_main">
		  <table class='table' >
			<tr>
			<?php 
			  if(leerxmlprueba("PruebaDslam","EstadoPrueba","Estado") == "OK" ){
			  echo "<td class=\"celda_titulo\">Estado Prueba</td>";
			  echo "<td class=\"celda_res\">";
			  echo leerxmlprueba("PruebaDslam","EstadoPrueba","Estado"); 
			   }
			   else
			   {
			   echo "<td class=\"celda_titulo\">Estado Prueba</td>";
			  echo "<td class=\"celda_res\">";
			  echo leerxmlprueba("PruebaDslam","EstadoPrueba","Estado"); 
			   }
			?>
			  <td class="celda_titulo">Descripcion</td>
			  <td class="celda_res">
				<?php echo leerxmlprueba("PruebaDslam","EstadoPrueba","Descripcion"); ?>
			  </td>
			</tr>
			<tr>
			  <td class="celda_titulo">Perfil Configurado</td>
			  <td class="celda_res">
			  <?php echo leerxmlprueba("PruebaDslam","PerfilConfigurado","Estado"); ?>
			  </td>
			  <td class="celda_titulo">VELOCIDAD CONFIGURADA</td>
			  <td class="celda_res">
			  <?php echo leerxmlprueba("PruebaDslam","PerfilConfigurado","Descripcion"); ?>
			  </td>
			</tr>
				<tr>
			  <td class="celda_titulo">VELOCIDAD SINCRONIZADA</td>
			  <td class="celda_res">
			  <?php echo leerxmlprueba("PruebaDslam","VelocidadActualBajada","Estado"); ?>
			  </td>
			  <td class="celda_titulo">Descripcion</td>
			  <td class="celda_res">
			  <?php echo leerxmlprueba("PruebaDslam","VelocidadActualBajada","Descripcion"); ?>
			  </td>
			</tr>

		  </table>
		</div>


	</div>

<?php
}
else{
	
	echo "<span>Por favor, verifique datos.</span>";

}

?>
  
  
