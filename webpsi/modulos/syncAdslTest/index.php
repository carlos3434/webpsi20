<?php 
require_once("../../cabecera.php");
set_time_limit(0);



?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />

<title></title>

<?php include ("../../includes.php") ?>    


<link type="text/css" href="css/redmond/jquery-ui-1.8.17.custom.css" rel="Stylesheet" />
<link type="text/css" href='css/estilo.css' rel="Stylesheet" />
<!--<script type="text/javascript" src="js/jquery-1.7.1.min.js"></script>-->
<script type="text/javascript" src="js/jquery-1.10.2.min.js"></script>

<script type="text/javascript" src="js/jquery-ui-1.8.17.custom.min.js"></script>
<script type="text/javascript" src="js/cronometro.js"></script>
<script type="text/javascript" src="js/pcba.js"></script>

	<script type="text/javascript">

	$(document).ready(function(){
	
		$("#telefono").keydown(function(event) {
			if ( event.keyCode == 46 || event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 27 || event.keyCode == 13 || 
				(event.keyCode == 67 && event.ctrlKey === true) || 
				(event.keyCode == 86 && event.ctrlKey === true) || 
				(event.keyCode == 88 && event.ctrlKey === true) || 
				(event.keyCode >= 35 && event.keyCode <= 39)) {
					 return;
			}
			else {
				if (event.shiftKey || (event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105 )) {
					event.preventDefault(); 
				}   
			}
		});
		
		$("#codArea").keydown(function(event) {
			if ( event.keyCode == 46 || event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 27 || event.keyCode == 13 || 
				(event.keyCode == 67 && event.ctrlKey === true) || 
				(event.keyCode == 86 && event.ctrlKey === true) || 
				(event.keyCode == 88 && event.ctrlKey === true) || 
				(event.keyCode >= 35 && event.keyCode <= 39)) {
					 return;
			}
			else {
				if (event.shiftKey || (event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105 )) {
					event.preventDefault(); 
				}   
			}
		});
	
	});
	
	</script>

</head>

<body>

<input type="hidden" value="<?php echo $IDUSUARIO ?>" name="txt_idusuario" id="txt_idusuario"/>

<?php echo pintar_cabecera(); ?>



<?php

if(!isset($_POST['codArea']) and !isset($_POST['telefono'])){
?>

<br/>

<div id="div_bus" class="div_busqueda" style="width: 750px">
	<table class="tabla">
	<form id="form" name="form" method="POST" >
	<tr class="tr_busqueda">
	<td style="width: 220px; padding; 5px">
		<label style="padding: 20px;">Codigo de Area :</label><input type="text" name="codArea" id="codArea" maxlength="2" style="width: 50px"  />
	</td>
	<td style="width: 220px; padding; 10px">
		<label style="padding: 10px;">Telefono : </label>
		<input type="text" name="telefono" id="telefono" maxlength="7" style="width: 100px; border: 1px" />
	</td>
	<td align="center" style="width: 400px">
		<input type="button" id="btn_pcba" value="PCBA" onclick="pcba()" style="width: 80px; font-weight: bold;" />
		<input type="button" id="btn_historico" value="Historico" onclick="historico()" style="width: 80px; font-weight: bold;"  />
		<input type="button" id="btn_reportes" value="Reportes" onclick="reporteador()" style="width: 80px; font-weight: bold;"  />
	</td>
	</tr>
	</form>
	</table>
</div>


<div id="div_cronometro" class="div_busqueda">
<b><span id="cronometro_prueba"></span></b>
</div>

<br>


<div id="resultadoPrueba" class="div_resultado"></div> 

 <?php
}
 ?>



</body>
</html>