<?php 
session_start();

$isLogin = 0;
$dirModulo = '';
$_SESSION['IS_MOBILE'] = 1;
$_SESSION['PREF_MOBILE'] = "m.";
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML Mobile 1.0//EN" "http://www.wapforum.org/DTD/xhtml-mobile10.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
  <!--<meta id="viewport" name="viewport" content="width=320; initial-scale=1.0; maximum-scale=1.0; user-scalable=0;" />-->
  <meta name="viewport" content="width=device-width; height=device-height; maximum-scale=1.4; initial-scale=1.0; user-scalable=yes"/>
  <title>Webunificada - Movil</title>
  <script type="text/javascript" src="js/jquery/jquery-latest.js"></script>
  <link href="css/movil.css" rel="stylesheet" type="text/css" />
</head>
<body>

<?php include 'm.header.php';?>

<form id="login" name="login" method="post" action="control.php">
<table id="tblPrincipal" class="principal" width="100%">
<tr>
    <td class="titulo">Ingreso de Usuarios</td>
</tr>
</table>
<div style="height:20px;"></div>

<table id="tblPrincipal" border="0" style="width: 200px; margin:auto;">
<tr>
    <td>
    <span class="msgErrorLogin">
    <?php
        if ( isset($_REQUEST['error']) && $_REQUEST['error'] == "ERROR_LOGIN" ) {
            echo 'Usuario y password incorrectos';
        } elseif ( isset($_REQUEST['error']) && $_REQUEST['error'] == "ERROR_ESTADO" ) {
            echo 'Su cuenta se encuentra deshabilitada';
        } elseif ( isset($_REQUEST['error']) && $_REQUEST['error'] == "IS_CONNECTED" ) {
            echo 'Su cuenta se encuentra en linea en el Sistema';
        }
    ?>
    </span>
    </td>
</tr>
<tr>
    <td style="color:#006699; font-size: 16px; font-weight:bold;">Usuario:</td>
</tr>
<tr>
    <td><input type="text" id="username" name="username" maxlength="20" style="font-size:20px; height:28px; width:180px;"></td>
</tr>
<tr>
    <td style="color:#006699; font-size: 16px; font-weight:bold;">Password:</td>
</tr>
<tr>
    <td><input type="password" id="password" name="password" maxlength="10" style="font-size:20px; height:28px; width:180px;"></td>
</tr>
<tr>
    <td><input type="submit" id="Entrar" name="Entrar" value="Entrar" class="searchsubmit" style="font-weight:bold; font-size:16px;"></td>
</tr>
<tr>
    <td>
        <div style="margin-top: 40px;"><a href="login.php">Ir a la Web Unificada</a></div>
    </td>
</tr>
</table>
</form>



</body>
</html>