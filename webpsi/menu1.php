<head>
<script type="text/javascript" src="js/jquery-1.7.2.min.js"></script>
<script type='text/javascript' src="js/jquery.hoverIntent.minified.js"></script>
<script type='text/javascript' src="js/jquery.dcmegamenu.1.3.3.js"></script>

<link rel="stylesheet" href="css/menucss.css" type="text/css" media="screen" />

<script type="text/javascript">

jQuery(document).ready(function($) {
    jQuery('#mega-menu').dcMegaMenu();
});
    
</script>
</head>

<body>
	
<ul id="mega-menu">
<li><a href="#">Home</a></li>
<li><a href="#">Products</a>
<ul>
    <li><a href="#">Mobile Phones &#038; Accessories</a>
    <ul>
        <li><a href="#">Product 1</a></li>
        <li><a href="#">Product 2</a></li>
        <li><a href="#">Product 3</a></li>
 
    </ul>
</li>
    <li><a href="#">Desktop</a>
    <ul>
        <li><a href="#">Product 4</a></li>
        <li><a href="#">Product 5</a></li>
        <li><a href="#">Product 6</a></li>
 
        <li><a href="#">Product 7</a></li>
    </ul>
</li>
    <li><a href="#">Laptop</a>
    <ul>
        <li><a href="#">Product 8</a></li>
        <li><a href="#">Product 9</a></li>
 
        <li><a href="#">Product 10</a></li>
    </ul>
</li>
</ul>
</li>
<li><a href="#">Sale</a>
<ul>
    <li><a href="#">Special Offers</a>
    <ul>
        <li><a href="#">Offer 1</a></li>
        <li><a href="#">Offer 2</a></li>
        <li><a href="#">Offer 3</a></li>
    </ul>
</li>
    <li><a href="#">Reduced Price</a>
    <ul>
        <li><a href="#">Offer 4</a></li>
        <li><a href="#">Offer 5</a></li>
        <li><a href="#">Offer 6</a></li>
    </ul>
</li>
    <li><a href="#">Clearance Items</a>
    <ul>
        <li><a href="#">Offer 7</a></li>
        <li><a href="#">Offer 8</a></li>
        <li><a href="#">Offer 9</a></li>
 
    </ul>
</li>
</ul>
</li>
<li><a href="#">Contact us</a></li>
</ul>			

</body>