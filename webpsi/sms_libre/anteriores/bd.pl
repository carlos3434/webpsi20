#!/usr/bin/perl
use DBI;
use strict;
use CGI;

# Conectar a Base de datos.
# Sintaxis
# $dbh = DBI->connect('dbi:mysql:DATABASE_NAME', USERNAME, PASSWORD)
my $dbh = DBI->connect('dbi:mysql:sergio','smiranda','sergio2011') 
	or die "Connection Error: $DBI::errstr\n";
$dbh->{AutoCommit} = 0;
###

## Ejecutar sentecia SQL ##
# my $sql = "select * from EECC limit 10 ";
# my $sth = $dbh->prepare($sql);
# $sth->execute or die "SQL Error: $DBI::errstr\n";
# ###

# while (my @row = $sth->fetchrow_array) {
	# print "@row\n";
# } 

## Obtener IP
my $q=CGI->new();
#my $ip = $q->header().$q->remote_addr();
my $ip = $q->remote_addr();
##########

## Obtener fecha y hora actual
my ($sec,$min,$hour,$mday,$mon,$year,$wday,$yday,$isdst) = localtime(time);
$year += 1900;
$mon++;
my $hoy =  "$year-$mon-$mday $hour:$min:$sec";
##########


## Forma de insertar en TRANSACCIONES en Base de Datos.
my $sql_ins = 'INSERT INTO tabla1 (ip, fecha_envio) VALUES (?,?)';
my $res_ins = $dbh->prepare_cached($sql_ins);

my $sql_ins2 = 'INSERT INTO tabla2 (ip, fecha_envio) VALUES (?,?)';
my $res_ins2 = $dbh->prepare_cached($sql_ins2);

die "No se puedo realizar el PREPARE del query. Terminando." unless defined $res_ins && defined $res_ins2 ;

my $success = 1;
$success &&= $res_ins->execute($ip, $hoy) ;
$success &&= $res_ins2->execute($ip, $hoy) ;

my $result = ($success ? $dbh->commit : $dbh->rollback);
unless ($result) { 
	die "No se pudo realizar transaccion por error: " . $dbh->errstr 
}

print "Resultado = ".$success;
print "\n";



