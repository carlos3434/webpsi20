<?php
include("conexion.php");

session_start();
//@ is authorized?
if(empty($_SESSION['exp_user']) || @$_SESSION['exp_user']['expires'] < time()) {
    header("location:login/login.html");	//@ redirect
} else {
    $_SESSION['exp_user']['expires'] = time()+(45*60);	//@ renew 45 minutes
}


?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Envio de SMS</title>


        <link rel="stylesheet" type="text/css" href="css/main.css" />
        <link rel="stylesheet" type="text/css" href="css/estilos.css" />
        <link rel="stylesheet" type="text/css" href="css/body.css" />


        <script type="text/javascript" src="js/jquery-1.7.2.min.js"></script>
        <script type="text/javascript">

                function sms_libre(){
                    $.post("enviosms_formulario.php", {modo: 1}, function (data){
                        $("#rightcolumn").html(data);
                    });	
                }

                function sms_individual(){
                    $.post("enviosms_formulario.php", {modo: 2}, function (data){
                        $("#rightcolumn").html(data);
                    });	
                }

            </script>
    </head>

    <body>

    <!-- Begin Wrapper -->
    <div id="wrapper">

        <!-- Begin Header -->
        <div id="header">

            <table>
                <thead>
                    <tr class="odd">
                        <th scope="col" abbr="Home" colspan="5">&nbsp;Envio de SMS</th>
                    </tr>	
                </thead>
                <tbody>
                    <tr>
                        <td width="22%"><b>Bienvenido(a): </b></td>
                        <td width="22%"><b>Celular: </b></td>
                        <td width="22%"><b>Area: </b></td>
                        <td width="22%"><b>DNI: </b></td>
                        <th scope="row" class="column1" rowspan="2" width="12%" >
                            <b><a href="cerrar_sesion.php?logout=555">
                                    &nbsp;Cerrar sesion
                                </a></b>
                        </th>
                        </tr>
                        <tr>
                            <td><b>
<?php
//echo "<br/>".var_dump($_SESSION["exp_user"]);

$query = "SELECT nombre,apellido,id_perfil,dni FROM tb_usuario 
                                                            WHERE id = '" . $_SESSION['exp_user']['id'] . "'";
$exec_query = mysql_query($query);
while ($row = mysql_fetch_array($exec_query)) {
    echo $row["nombre"] . " " . $row["apellido"];
    $perfil = $row["id_perfil"];
    $dni = $row["dni"];
}
?>
            </b></td>
        <td><b>
<?php
$query = "SELECT contacto FROM tb_persona_contacto WHERE id_persona = '" . $_SESSION['exp_user']['persona'] . "'";
$exec_query = mysql_query($query);
while ($row = mysql_fetch_array($exec_query)) {
    echo $row["contacto"] . " ";
}
?>
                                </b></td>
                            <td><b>
<?php
$query = "SELECT id_area FROM tb_usuario WHERE id = '" . $_SESSION['exp_user']['usuario'] . "'";
$exec_query = mysql_query($query);
while ($row = mysql_fetch_array($exec_query)) {
    $query = "SELECT area,estado FROM tb_area WHERE id = '" . $row['exp_user']['id_area'] . "'";
    $exec_query = mysql_query($query);
    while ($row = mysql_fetch_array($exec_query)) {
        if ($row["estado"] == 1) {
            echo $row["area"];
        } else {
            echo "Area deshabilitada";
        }
    }
}
?>
                                </b></td>
                            <td><b>
<?php
echo $dni;
?>
                                </b></td>
                        </tr>
                    </tbody>
                </table>

            </div>
            <!-- End Header -->

<?php
$query = "SELECT * FROM tb_perfil WHERE id = '" . $perfil . "'";
//echo $query;
$exec_query = mysql_query($query);
while ($row = mysql_fetch_array($exec_query)) {
    $estado = $row["estado"];
}
?>

    <?php
    if ($estado == 1) {
    ?>

    <!-- Begin Faux Columns -->
    <div id="faux">

        <!-- Begin Left Column -->
        <div id="leftcolumn">
        <table>
        <?php
        switch ($perfil) {
            case (1):
                ?>
                <tr>
                    <th scope="row" class="column1"><a href="#" onclick="sms_libre()">&nbsp;SMS Libre</a></th>
                </tr><tr>
                    <th scope="row" class="column1"><a href="#" onclick="sms_individual()">&nbsp;SMS Individual</a></th>	
                </tr><tr>
                    <th scope="row" class="column1"><a href="#">&nbsp;SMS Grupal</a></th>	
                </tr><tr>
                    <th scope="row" class="column1"><a href="#" >&nbsp;Mantenimientos</a></th>
                </tr>
                <?php
                break;
                ?>
                <?php
                case (2):
                case (3):
                    ?>
                    <tr>
                        <th scope="row" class="column1"><a href="#" onclick="sms_individual()">&nbsp;SMS Individual</a></th>	
                    </tr><tr>
                        <th scope="row" class="column1"><a href="#" >&nbsp;SMS Grupal</a></th>	
                    </tr>
                <?php
                break;
        }
        ?>
            </table>

            </div>
            <!-- End Left Column -->

            <!-- Begin Right Column -->
            <div id="rightcolumn">



                <div id="clear">Seleccione opcion.</div>

            </div>
            <!-- End Right Column -->

            <div id="clear"></div>

        </div>	   
                <!-- End Faux Columns --> 

    <?php
} else {
    echo "<div class='div0' style='color:#FF0000;'><b>Perfil Deshabilitado. Comunicarse con el administrador.</b></div>";
}
?>

            <!-- Begin Footer -->
            <div id="footer">

                <table>
                    <tr>
                        <td style="align:center"><b>TDP - Planificacion y Soluciones Informaticas</b></td>
                    </tr>
                </table>	

            </div>
            <!-- End Footer -->

        </div>
        <!-- End Wrapper -->
    </body>
</html>

<?php ?>
