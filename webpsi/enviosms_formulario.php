<?php
include ("conexion.php");
session_start();

if (isset($_SESSION["usuario"]) && isset($_SESSION["persona"]) && isset($_SESSION["grupo"]) && isset($_SESSION["online"])) {
?>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>SMS Libre</title>

<link href="estilos.css" rel="stylesheet" type="text/css" media="all"/>

<script type="text/javascript" src="jquery-1.7.2.min.js"></script>

<script type="text/javascript">

$(document).ready(function() {
	iniciar();
});

function iniciar()
{
	$("#countdown").val(140);
	$("#celular").val("");
	$("#mensaje").val("");
}


function enviar()
{
	var celular = $("#celular").val();
	var mensaje = $("#mensaje").val();
	
	var modo = $("#modo").val();
	var grupo = $("#grupo").val();
	
	if (celular.length<9)
	{
		alert("Numero celular no valido. Debe tener 9 digitos.");
		return;
	}

	if (mensaje.length<4)
	{
		alert("Mensaje debe tener minimo 4 letras.");
		return;
	}
	
	
	$.ajax({
	  type: "POST",
	  url: "enviar_individual_ajax.php",
	  data: { 
		enviar_sms: 1,
		celular: celular,
		mensaje: mensaje,
		modo: modo
	  }
	}).done(function( msg ) {
		var res = msg.split("|");
		r = $.trim(res[1])
		//alert(r);
		if (r == "1") {
			$("#div_res").html("Se envio el mensaje correctamente.");
			iniciar();
		}
		else
			$("#div_res").html("Ocurrio un error, no se pudo enviar el mensaje.");
	});
}

function limitText() {
	var limitField = document.getElementById("mensaje");
	var limitCount = document.getElementById("countdown");
	var limitNum = 140;
	
	if (limitField.value.length > limitNum) {
		limitField.value = limitField.value.substring(0, limitNum);
	} else {
		limitCount.value = limitNum - limitField.value.length;
	}
}


</script>

</head>
    
<body bgcolor='#DBE3FF'>

<br/>

<?php
	$modo = $_POST["modo"];
	echo "<input type='hidden' value='".$modo."' id='modo' />";
?>

<div id="id0" class="div0" >
<table>
	<thead>
	<tr class="odd">
		<?php
			switch($modo){	
				case (1):
		?>
			<th scope="col" abbr="Home" colspan="4">&nbsp;Envio de SMS Libres</th>
		<?php
			break;
			case (2):
		?>
			<th scope="col" abbr="Home" colspan="4">&nbsp;Envio de SMS Individuales</th>
		<?php
			break;
		}
		?>	
	</tr>	
	</thead>

<tbody>
<tr>
	<?php
		switch($modo){	
			case (1):
	?>
		<th scope="row" class="column1">Numero Celular:</th>
		<td><input name="celular" id="celular" type="text" maxlength="10" /></td>
	<?php
			break;
			case (2):
	?>
		<!--
		<th scope="row" class="column1">Grupo:</th>
		<td><select name="grupo" id="grupo">
			<option value = '0'>TODOS</option>
		<?php 
			// $query = "SELECT * FROM tb_grupo ORDER BY grupo";
			// $exec_query = mysql_query($query);
			// while($row = mysql_fetch_array($exec_query)){
				// echo "<option value='".$row['id']."'>".$row['grupo']."</option>";
			// }
		?>
		</select></td>
		</tr><tr>
		-->
		<th scope="row" class="column1">Numero Celular:</th>
		<td><select name="celular" id="celular">
		<?php 
			$query = "SELECT a.nombre as nomb, a.apellido as apel, b.contacto as cont FROM tb_persona a, tb_persona_contacto b WHERE a.id = b.id_persona AND id_tipo_contacto='1' AND defecto = '1' ORDER BY a.nombre";
			$exec_query = mysql_query($query);
			while($row = mysql_fetch_array($exec_query)){
				echo "<option value='".$row['cont']."'>".$row['nomb']." ".$row['apel']." (".$row['cont'].")</option>";
			}
		?>
		</select></td>		
	<?php
			break;
		}
	?>
</tr>
<tr>
	<th scope="row" class="column1">Mensaje SMS:</th>
	<td><textarea id="mensaje" name="mensaje" class="caja_texto2"  onKeyDown="limitText();" 
onKeyUp="limitText();"></textarea></td>

</tr>
<tr>
<td colspan="2" class="td_center">
	<font size="1">(Max caracteres: 140)<br>
	Tienes <input readonly type="text" name="countdown" id="countdown" size="3" value="140" style="width:40px; font-size: 10px;"> caracteres por escribir.</font>

</td>
</tr>
<tr>
	<td colspan="2" class="td_center"><input name="enviar" type="button" value="Enviar SMS" onclick="enviar()"/></td>
</tr>
</tbody>
</table>
</div>

<div id="div_res" class="div0"></div>

</body>
</html>

<?php
}
?>