USE psi;
SELECT stc.`created_at` , IF(stu.`actividad_id` = 1, "Averia", "Provision") AS actividad, stc.`tipo_operacion`, stc.`id_solicitud_tecnica`, stc.`clase_servicio`, stc.`codigo_contrata`, stc.`cod_motivo_generacion`, stc.`tipo_requerimiento` ,
IF (
    (SELECT IF(stc.clase_servicio ="26" OR stc.`clase_servicio` = "27" OR stc.`clase_servicio` = "33", "premiummov1", NULL) AS bucket FROM webpsi_coc.`zona_premium_catv`  AS zpc WHERE zpc.nodo = stc.cod_nodo AND zpc.troba = stc.cod_troba AND zpc.estado = 1)
    IS NULL, 
    IF(
	(stc.`clase_servicio` = "26" OR stc.`clase_servicio` = "27" OR stc.`clase_servicio` = "33") AND (stc.`cod_troba` = "LL" OR stc.`cod_troba` = "LM" OR stc.`cod_troba`= "SP"),
	"PENDMOV1", "OTRO QUIEBRE"
    ), 
    "PREM_PENDMOV1"
) AS premium
-- IFNULL((SELECT zpc.zona AS bucket FROM webpsi_coc.`zona_premium_catv`  AS zpc WHERE zpc.nodo = stc.cod_nodo AND zpc.troba = stc.cod_troba AND zpc.estado = 1),  "no") As premium
FROM solicitud_tecnica_cms AS stc
	LEFT JOIN solicitud_tecnica_ultimo AS stu ON stu.solicitud_tecnica_id = stc.solicitud_tecnica_id
	WHERE stu.`quiebre_id` IS NULL
	AND stu.estado_st = 1
GROUP BY stu.solicitud_tecnica_id
ORDER BY stc.`created_at` ASC;