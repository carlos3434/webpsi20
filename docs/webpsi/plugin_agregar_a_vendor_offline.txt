 =================================
| AGREGAR PAQUETE|PLUGIN A VENDOR OFF-LINE
 =================================
 NOTA: Para saber en que archivo se debe generar las rutas debe fijarse en otro proyecto las rutas generadas,
 en la siguiente ruta: "vendor/composer"
 donde se haya instalado el plugin mediante "composer require" o "composer update <>"
 
 NOTA: Puede que haya paquetes que sean mas complejas a la hora de generar sus rutas
 
1. Copiar el paquete a usar a vendor/
	.:EJEMPLO:.
	vendor/yajra
	
2. editar composer.js en la seccion 
	"autoload": { .... }

3. Agregar si es necesario, se agrega en autoload_classmap.php:
	"classmap": [ ... ]
	.:EJEMPLO:.
	"classmap": [
		...
		"vendor/yajra"
	],
	
4. Agregar si es necesario, se agrega en autoload_namespaces.php:
	"psr-0": { ... }
	.:EJEMPLO:.
	"psr-0": {
		...
		"yajra\\Datatables\\":"vendor/yajra/laravel-datatables-oracle/src"
	}
	
5. Agregar si es necesario, se agrega en autoload_ps4.php:
	"psr-4": { ... }
	.:EJEMPLO:.
	"psr-4": {
		...
		"Intervention\\Image\\": [
			"vendor/intervention/image/src/Intervention/Image",
			"vendor/intervention/imagecache/src/Intervention/Image"
		]
	},
	
6. Ejecutar el comando: 
	composer dump-autoload

 ======
|FUENTE
 ======
Basado en, https://jtreminio.com/2012/10/composer-namespaces-in-5-minutes/