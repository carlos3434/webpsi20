Provision

-- Llenamos el campo actividad_tipo_id

-- Registros ALTAS-CATV actividad_tipo_id=1

UPDATE webpsi_coc.tmp_provision SET actividad_tipo_id=1 WHERE origen='pen_prov_catv' AND quiebre<>'PENDMOV1' AND tipo_actuacion='ALTA';

-- Registros ALTAS-M1 actividad_tipo_id=3

UPDATE webpsi_coc.tmp_provision SET actividad_tipo_id=3 WHERE origen='pen_prov_catv' AND quiebre='PENDMOV1' AND tipo_actuacion='ALTA';

-- Registros RUTINAS-CATV-AT actividad_tipo_id=7

UPDATE webpsi_coc.tmp_provision SET actividad_tipo_id=7 WHERE origen='pen_prov_catv' AND quiebre<>'PENDMOV1' AND tipo_actuacion<>'ALTA' AND tipo_motivo LIKE 'AT|%';

-- Registros RUTINAS-CATV-MT actividad_tipo_id=8

UPDATE webpsi_coc.tmp_provision SET actividad_tipo_id=8 WHERE origen='pen_prov_catv' AND quiebre<>'PENDMOV1' AND tipo_actuacion<>'ALTA' AND tipo_motivo LIKE 'MT|%';

-- Registros RUTINAS-CATV-VA actividad_tipo_id=9

UPDATE webpsi_coc.tmp_provision SET actividad_tipo_id=9 WHERE origen='pen_prov_catv' AND quiebre<>'PENDMOV1' AND tipo_actuacion<>'ALTA' AND tipo_motivo LIKE 'VA|%';

 

Averias

-- Registros AVERIAS-CATV actividad_tipo_id=4

UPDATE webpsi_coc.averias_criticos_final SET actividad_tipo_id=4 WHERE tipo_averia='aver-catv-pais' AND quiebre<>'PENDMOV1';

-- Registros AVERIAS-M1 actividad_tipo_id=6

UPDATE webpsi_coc.averias_criticos_final SET actividad_tipo_id=6  WHERE tipo_averia='aver-catv-pais' AND quiebre='PENDMOV1';