==========================================================================
			REAGENDAMIENTO NO PROGRAMADO
==========================================================================

PROVISIÓN
- si se cambia de estado a "No realizada"
 - si es "CLIENTE ASENTE"
   - si es del tipo SLA 
     - si tiene menos de 3 reangendamientos por no realizada
	->>> la actividad se vuelve a reagendar como no programada

AVERIA
- Si se cambia de estado a "Completar"
   - Si es Submotivo "CASA CERRADA"
     - Si es del Tipo SLA
	- Si tiene menos de 3 reangendamientos por "CASA CERRADA"
	   ->>> la actividad se vuelve a reagendar como no programada

