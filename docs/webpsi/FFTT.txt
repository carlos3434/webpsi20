
==============================
Logica de Facilidades Tecnicas
==============================

1.- Las FFTT son elementos para ayudar a sectorizar y realizar el trabajo del 
    tecnico, entre estos elementos tenemos a las siguientes segun jerarquia:

    - Zonal (p)
        - Mdf (p)
            - Armario (p)
                - Terminal flexible (.) tabla 7/bas
                    - Borne (.)
            - Cable (p)
                - Terminal directa (.) tabla 6/adsl
                    - Borne (.)
        - Nodo (p)
            - Troba (p)
                - Amplificador (.)
                    - Tap (.) tabla 5 / catv
                        - Borne (.)

    de los leementos presentados se pueden armar poligonos en el mapa o puntos
    especificos.
    - (p) => poligono
    - (.) => punto

2.- Deacuerdo al tipo de actividad se obtienen las facilidades tecnicas
    
    adsl y basica (cobre)   => MDF
    catv (HFC)              => Nodo


