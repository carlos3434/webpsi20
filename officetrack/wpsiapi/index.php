<?php
require_once '../integracion/class/class.Error.php';
require_once '../integracion/class/class.Utils.php';
require_once '../integracion/class/class.Conexion.php';
require_once '../integracion/class/class.CurlNovus.php';

$Error = new Error();
$Utils = new Utils();
$CurlNovus = new CurlNovus();
$Conexion = new Conexion();

$fo = fopen("../02_wpsi_api.txt", "w+");
fwrite($fo, serialize($_POST));
fclose($fo);

if (isset($_POST["hashfw"]))
{
    if ($_POST["hashfw"] == md5("10.226.666.969.753" . date("Ymd")))
    {
        /**
         * Validar distancia de inicio
         * Solo para "Pendiente M1"
         * Config:
         * 
         * quiebre_id       = 12
         * distancia_max    = 300m
         */
        if ($_POST["method"]=='valida_distancia_inicio')
        {   
            $gestion_id = $_POST["gid"];
            $actu = $CurlNovus->obtener_actu("$gestion_id");
            
            $fo = fopen("../03.1_wpsi_api.txt", "w+");
            fwrite($fo, $actu);
            fclose($fo);
            
            $data = json_decode($actu);
            
            if ($data->rst==1 and $data->datos->quiebre_id==12)
            {
                $dist = $CurlNovus->calcular_distancia($_POST);
                $fo = fopen("../05_wpsi_api.txt", "w+");
                fwrite($fo, $dist);
                fclose($fo);
                
                $valida = json_decode($dist);
                
                //Se reenvia orden al tecnico
                if ($valida->rst == 1 and $valida->distancia > 300)
                {
                    $fo = fopen("../06_wpsi_api.txt", "w+");
                    fwrite($fo, "Reenviar orden");
                    fclose($fo);
                }
            }
        }
    }
}
