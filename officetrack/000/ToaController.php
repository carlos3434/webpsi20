<?php
use Ofsc\Activity;

class ToaController extends BaseController
{

    public function __construct()
    {
        $this->beforeFilter('auth'); // bloqueo de acceso
    }

    /**
     * Listar registro de actividades con estado 1
     * POST actividad/listar
     *
     * @return Response
     */
    public function postValidagestion()
    {
        if (Request::ajax()) {
            $permiso=Toa::ValidaPermiso();
            return Response::json(
                array(
                    'rst'=>1,
                    'permiso'=>$permiso
                )
            );
        }
    }
    
    public function postGetimage(){
        $activity = new Activity();
        $response = $activity->getFile(
            Input::get('aid'), Input::get('label')
        );
        
        $img = "<img src=\"img/no_image_256x256.png\">";
        
        if ($response->data->result_code == 0)
        {
            foreach ($response->data->file_data as $val) {
                $img = "<img src=\"data:image/jpeg;base64,"
                        . base64_encode($val) ."\" />";
            }
        }
        
        return Response::json(
            array(
                'rst'=>1,
                'img'=>$img
            )
        );
    }

}
