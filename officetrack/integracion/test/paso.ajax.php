
<?php

require_once '../class/class.Conexion.php';
$Conexion = new Conexion();

$mysql = $Conexion->conectar();

if ($_POST["id"]) {

    $id = $_POST["id"];

    //Paso uno
    $sql = "SELECT * FROM webpsi_officetrack.paso_uno WHERE task_id=" . $id;
    $query = $mysql->query($sql);

    $data = $query->fetchAll(PDO::FETCH_ASSOC);

    if (!empty($data))
    {
        echo "<hr><h2>PASO 1</h2>";
        foreach ($data as $key => $val) {
            echo "<h3>Tarea</h3>";
            echo "<p>{$val['task_id']}</p>";

            echo "<h3>XY</h3>";
            echo "<p>{$val['y']}, {$val['x']}</p>";

            echo "<h3>Casa</h3>";
            echo "<p><img src=\"data:image/jpg;base64,{$val['casa_img1']}\" /></p>";
            echo "<p>Tarea</p>";
        }
    }
    
    //Paso dos
    $sql = "SELECT * FROM webpsi_officetrack.paso_dos WHERE task_id=" . $id;
    $query = $mysql->query($sql);

    $data = $query->fetchAll(PDO::FETCH_ASSOC);

    if (!empty($data)) 
    {
        echo "<hr><h2>PASO 2</h2>";
        foreach ($data as $key => $val) {
            echo "<h3>Tarea</h3>";
            echo "<p>{$val['task_id']}</p>";

            echo "<h3>Motivo</h3>";
            echo "<p>{$val['motivo']}</p>";
            
            echo "<h3>Observacion</h3>";
            echo "<p>{$val['observacion']}</p>";
            
            echo "<h3>TAP IMG1</h3>";
            echo "<p><img src=\"data:image/jpg;base64,{$val['tap_img1']}\" /></p>";
            
            echo "<h3>TAP IMG2</h3>";
            echo "<p><img src=\"data:image/jpg;base64,{$val['tap_img2']}\" /></p>";
            
            echo "<h3>TAP IMG3</h3>";
            echo "<p><img src=\"data:image/jpg;base64,{$val['tap_img3']}\" /></p>";
            
            echo "<h3>MODEM IMG1</h3>";
            echo "<p><img src=\"data:image/jpg;base64,{$val['modem_img1']}\" /></p>";
            
            echo "<h3>MODEM IMG2</h3>";
            echo "<p><img src=\"data:image/jpg;base64,{$val['modem_img2']}\" /></p>";
            
            echo "<h3>MODEM IMG3</h3>";
            echo "<p><img src=\"data:image/jpg;base64,{$val['modem_img3']}\" /></p>";
            
            echo "<h3>TV IMG1</h3>";
            echo "<p><img src=\"data:image/jpg;base64,{$val['tv_img1']}\" /></p>";
            
            echo "<h3>TV IMG2</h3>";
            echo "<p><img src=\"data:image/jpg;base64,{$val['tv_img2']}\" /></p>";
            
            echo "<h3>TV IMG3</h3>";
            echo "<p><img src=\"data:image/jpg;base64,{$val['tv_img3']}\" /></p>";
            
            echo "<h3>PROBLEMA IMG1</h3>";
            echo "<p><img src=\"data:image/jpg;base64,{$val['problema_img1']}\" /></p>";
            
            echo "<h3>PROBLEMA IMG2</h3>";
            echo "<p><img src=\"data:image/jpg;base64,{$val['problema_img2']}\" /></p>";
            
            echo "<h3>PROBLEMA IMG3</h3>";
            echo "<p><img src=\"data:image/jpg;base64,{$val['problema_img3']}\" /></p>";
        }
    }
    
    //Paso tres
    $sql = "SELECT * FROM webpsi_officetrack.paso_tres WHERE task_id=" . $id;
    $query = $mysql->query($sql);

    $data = $query->fetchAll(PDO::FETCH_ASSOC);

    if (!empty($data)) 
    {
        echo "<hr><h2>PASO 3</h2>";
        foreach ($data as $key => $val) {
            echo "<h3>Tarea</h3>";
            echo "<p>{$val['task_id']}</p>";

            echo "<h3>estado</h3>";
            echo "<p>{$val['estado']}</p>";
            
            echo "<h3>observacion</h3>";
            echo "<p>{$val['observacion']}</p>";

            echo "<h3>Firma</h3>";
            echo "<p><img src=\"data:image/jpg;base64,{$val['firma_img']}\" /></p>";
            
            echo "<h3>FINAL IMG1</h3>";
            echo "<p><img src=\"data:image/jpg;base64,{$val['final_img1']}\" /></p>";
            
            echo "<h3>FINAL IMG2</h3>";
            echo "<p><img src=\"data:image/jpg;base64,{$val['final_img2']}\" /></p>";
            
            echo "<h3>FINAL IMG3</h3>";
            echo "<p><img src=\"data:image/jpg;base64,{$val['final_img3']}\" /></p>";
        }
    }
    
}
