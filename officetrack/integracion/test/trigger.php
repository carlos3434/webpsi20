<?php
//-12.109166, -77.016398 -> TDP Surquillo
//-12.111901, -77.028632 -> VES (12.4 Km)
/**
 * Pruebas:
 * TEST0001 - 69706
 * TEST0002 - 69714
 * TEST0003 - 69716
 * 
 * Tec. 666, 101010
 */
$array = array(
    'UserName'=>''
    ,'Password'=>''
    ,'Operation'=>'AutoSelect'
    //,'TaskTypeCode'=>'Provision'
    //,'Categories'=>'Provision'
    ,'TaskNumber'=>'2000'
    ,'EmployeeNumber'=>'666'
    ,'DueDateAsYYYYMMDDHHMMSS'=>'20150924210717'
    ,'Duration'=>'2'
    ,'Notes'=>''
    ,'Description'=>'Agenda: 2015-07-10 10:00-12:00'
    ,'Status'=>'NewTask'
    ,'CustomerName'=>'2000-666-123456'
    , 'Location' => array(
        'East' => '-77.016398', 
        'North' => '-12.109166', 
        'Address' => 'San Felipe 1130'
    )
    ,'Data1'=>'2015-07-10 10:00-12:00'
    ,'Data2'=>'29341430'
    ,'Data3'=>'2015-07-10 10:00-12:00'
    ,'Data4'=>'SOTO CAYSAHUANA JUSTA'
    ,'Data5'=>'JR RIO MAJES 0, Piso:   Int:   Mzn: F Lt: 17'
    ,'Data6'=>'Averia - AT|A422'
    ,'Data7'=>'20729456'
    ,'Data8'=>'CG|R013|09|06|04'
    ,'Data9'=>''
    ,'Data10'=>'1426372'
    ,'Data11'=>'CG'
    ,'Data12'=>'N'
    ,'Data13'=>'ESTANDAR'
    ,'Data14'=>''
    ,'Data15'=>'LIM'
    ,'Data16'=>''
    ,'Data17'=>'DIGITALIZACION'
    ,'Data18'=>'LEJANO'
    ,'Data19'=>'SAN JUAN DE LURIGANCHO'
    ,'Data20'=>''
    ,'Data21'=>'1426372'
    ,'Data22'=>'PAI'
    ,'Data23'=>'http://psiweb.ddns.net:2230/webpsi/modulos/public/rutaTecnico/?cc=LA3900%26tareas=0_0_14969'
    ,'Data24'=>'http://psiweb.ddns.net:2230/webpsi/modulos/public/rutaTecnico/?cc=LA3900'
    ,'Data25'=>'LARI'
    ,'Data26'=>'14969'
    ,'Data27'=>'Agendado con Técnico'
    ,'Data28'=>'ATENDER 2-4PM'
    ,'Data29'=>'MOVISTAR SPEEDY 4M'
    ,'Data30'=>'Componente 1\nComponente 2\nComponente 3'
    ,'Options'=>'SendNotificationToMobile'
    ,'AdditionalInfoXml'=>'Jmx0Oz94bWwgdmVyc2lvbj0mcXVvdDsxLjAmcXVvdDsgZW5jb2Rpbmc9JnF1b3Q7dXRmLTE2JnF1b3Q7PyZndDsmbHQ7QXJyYXlPZk9iamVjdEFkZGl0aW9uYWxJbmZvIHhtbG5zOnhzaSA9ICZxdW90O2h0dHA6Ly93d3cudzMub3JnLzIwMDEvWE1MU2NoZW1hLWluc3RhbmNlJnF1b3Q7IHhtbG5zOnhzZCA9JnF1b3Q7aHR0cDovL3d3dy53My5vcmcvMjAwMS9YTUxTY2hlbWEmcXVvdDsmZ3Q7Jmx0O09iamVjdEFkZGl0aW9uYWxJbmZvJmd0OyZsdDtOYW1lJmd0O0FkZGl0aW9uYWwgSW5mbyAxJmx0Oy9OYW1lJmd0OyZsdDtJdGVtcyZndDsmbHQ7VGV4dFZhbHVlUGFpciZndDsmbHQ7VGV4dCZndDsxLjEmbHQ7L1RleHQmZ3Q7Jmx0O1ZhbHVlJmd0OzEuMSBEYXRhJmx0Oy9WYWx1ZSZndDsmbHQ7L1RleHRWYWx1ZVBhaXImZ3Q7Jmx0O1RleHRWYWx1ZVBhaXImZ3Q7Jmx0O1RleHQmZ3Q7MS4yJmx0Oy9UZXh0Jmd0OyZsdDtWYWx1ZSZndDsxLjIgRGF0YSZsdDsvVmFsdWUmZ3Q7Jmx0Oy9UZXh0VmFsdWVQYWlyJmd0OyZsdDsvSXRlbXMmZ3Q7Jmx0Oy9PYmplY3RBZGRpdGlvbmFsSW5mbyZndDsmbHQ7T2JqZWN0QWRkaXRpb25hbEluZm8mZ3Q7Jmx0O05hbWUmZ3Q7RGF0b3MgMDImbHQ7L05hbWUmZ3Q7Jmx0O0l0ZW1zJmd0OyZsdDtUZXh0VmFsdWVQYWlyJmd0OyZsdDtUZXh0Jmd0OzIuMSZsdDsvVGV4dCZndDsmbHQ7VmFsdWUmZ3Q7Mi4xIERhdGEmbHQ7L1ZhbHVlJmd0OyZsdDsvVGV4dFZhbHVlUGFpciZndDsmbHQ7VGV4dFZhbHVlUGFpciZndDsmbHQ7VGV4dCZndDtDb2RpZ28gMDImbHQ7L1RleHQmZ3Q7Jmx0O1ZhbHVlJmd0O0FsZ28gZGUgRGF0YSZsdDsvVmFsdWUmZ3Q7Jmx0Oy9UZXh0VmFsdWVQYWlyJmd0OyZsdDtUZXh0VmFsdWVQYWlyJmd0OyZsdDtUZXh0Jmd0O0FsZ28mbHQ7L1RleHQmZ3Q7Jmx0O1ZhbHVlJmd0O1BydWViYSBkZSBwcnVlYmEgdGV4dG8geSBtYXMgdGV4dG8geSBtYXMgdGV4dG8gZGUgZW52aW8gaGFjaWEgT1QmbHQ7L1ZhbHVlJmd0OyZsdDsvVGV4dFZhbHVlUGFpciZndDsmbHQ7L0l0ZW1zJmd0OyZsdDsvT2JqZWN0QWRkaXRpb25hbEluZm8mZ3Q7Jmx0Oy9BcnJheU9mT2JqZWN0QWRkaXRpb25hbEluZm8mZ3Q7'
    //,'MaximalRadiusForEntries'=>0.3
    //,'ProhibitEntriesOutsideRadius'=>'1'    
//    ,'AdditionalInfoXml'=>'&lt;?xml version=&quot;1.0&quot; encoding=&quot;utf-16&quot;?&gt;&lt;ArrayOfObjectAdditionalInfo xmlns:xsi = &quot;http://www.w3.org/2001/XMLSchema-instance&quot; xmlns:xsd = &quot;http://www.w3.org/2001/XMLSchema&quot;&gt;&lt;ObjectAdditionalInfo&gt;&lt;Name&gt;Additional info 1&lt;/Name&gt;&lt;Items&gt;&lt;TextValuePair&gt;&lt;Text&gt;Customer Name&lt;/Text&gt;&lt;Value&gt;Data&lt;/Value&gt;&lt;/TextValuePair&gt;&lt;TextValuePair&gt;&lt;Text&gt;Customer Number&lt;/Text&gt;&lt;Value&gt;123&lt;/Value&gt;&lt;/TextValuePair&gt;&lt;TextValuePair&gt;&lt;Text&gt;More Info 1&lt;/Text&gt;&lt;Value&gt;Data 1&lt;/Value&gt;&lt;/TextValuePair&gt;&lt;TextValuePair&gt;&lt;Text&gt;More Info 2&lt;/Text&gt;&lt;Value&gt;Data 2&lt;/Value&gt;&lt;/TextValuePair&gt;&lt;TextValuePair&gt;&lt;Text&gt;More Info 3&lt;/Text&gt;&lt;Value&gt;Data 3&lt;/Value&gt;&lt;/TextValuePair&gt;&lt;TextValuePair&gt;&lt;Text&gt;More Info 4&lt;/Text&gt;&lt;Value&gt;Data 4&lt;/Value&gt;&lt;/TextValuePair&gt;&lt;TextValuePair&gt;&lt;Text&gt;More Info 5&lt;/Text&gt;&lt;Value&gt;Data 5&lt;/Value&gt;&lt;/TextValuePair&gt;&lt;TextValuePair&gt;&lt;Text&gt;More Info 6&lt;/Text&gt;&lt;Value&gt;Data 6&lt;/Value&gt;&lt;/TextValuePair&gt;&lt;/Items&gt;&lt;/ObjectAdditionalInfo&gt;&lt;ObjectAdditionalInfo&gt;&lt;Name&gt;Additional info 2&lt;/Name&gt;&lt;Items&gt;&lt;TextValuePair&gt;&lt;Text&gt;Question 1?&lt;/Text&gt;&lt;Value&gt;Answer 1&lt;/Value&gt;&lt;/TextValuePair&gt;&lt;TextValuePair&gt;&lt;Text&gt;Question 2?&lt;/Text&gt;&lt;Value&gt;Answer 2&lt;/Value&gt;&lt;/TextValuePair&gt;&lt;TextValuePair&gt;&lt;Text&gt;Question 3?&lt;/Text&gt;&lt;Value&gt;Answer 3&lt;/Value&gt;&lt;/TextValuePair&gt;&lt;TextValuePair&gt;&lt;Text&gt;Question 4?&lt;/Text&gt;&lt;Value&gt;Answer 4&lt;/Value&gt;&lt;/TextValuePair&gt;&lt;/Items&gt;&lt;/ObjectAdditionalInfo&gt;&lt;ObjectAdditionalInfo&gt;&lt;Name&gt;Additional info 3&lt;/Name&gt;&lt;Items&gt;&lt;TextValuePair&gt;&lt;Text&gt;Field Name1&lt;/Text&gt;&lt;Value&gt;Value 1&lt;/Value&gt;&lt;/TextValuePair&gt;&lt;TextValuePair&gt;&lt;Text&gt;Field Name2&lt;/Text&gt;&lt;Value&gt;Value 2&lt;/Value&gt;&lt;/TextValuePair&gt;&lt;TextValuePair&gt;&lt;Text&gt;Field Name3&lt;/Text&gt;&lt;Value&gt;Value 3&lt;/Value&gt;&lt;/TextValuePair&gt;&lt;TextValuePair&gt;&lt;Text&gt;Field Name4&lt;/Text&gt;&lt;Value&gt;Value 4&lt;/Value&gt;&lt;/TextValuePair&gt;&lt;TextValuePair&gt;&lt;Text&gt;Field Name5&lt;/Text&gt;&lt;Value&gt;Value 5&lt;/Value&gt;&lt;/TextValuePair&gt;&lt;/Items&gt;&lt;/ObjectAdditionalInfo&gt;&lt;/ArrayOfObjectAdditionalInfo&gt;'
);
$json = json_encode($array);
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
        <script src="jquery-1.11.1.min.js"></script>
        <script>
            $(document).ready(function (){
                $("#send").click(function (){
                    $.ajax({
                        type: "POST",
                        url: "http://190.234.74.6/test/integracion/office_track_test.php",
                        data: 'cadena=<?php echo $json;?>',
                        dataType: 'json',
                        error: function(datos) {
                            console.log(datos);
                        },
                        success: function(datos) {
                            console.log(datos);
                        }
                    });
                });
                
            });
            
        </script>
    </head>
    <body>

        <input type="button" name="send" id="send" value="Send" />

    </body>
</html>
