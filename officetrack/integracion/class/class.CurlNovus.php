<?php
class CurlNovus 
{
    private $_base = "http://psiweb.ddns.net";
    private $_acceso="\$PSI20\$";
    private $_clave="\$1st3m@\$";
    
    public function obtener_actu($gid){
        
        try {
            $url     = $this->_base . ":7020/api/obteneractu";
            $hashg   = hash('sha256', $this->_acceso . $this->_clave . $gid);
            $postData=array(
                'gestion_id'    => "$gid",
                'hashg'         => "$hashg"
            );

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_HEADER, false);
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt(
                    $ch, 
                    CURLOPT_POSTFIELDS, 
                    http_build_query($postData)
                    );
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            //Retorno  
            $result = curl_exec($ch);
            curl_close($ch);
            //$fo = fopen("../03.4_wpsi_api.txt", "w+");
            //fwrite($fo, $result);
            //fclose($fo);
            return $result;
        } catch (Exception $exc) {
            
        }
        
    }
    //activacion mensaje 12/04
    public function notificar_noregistro($phone, $msj){
        try {
            $url     = $this->_base . ":7020/api/getvalidacion";
            $hashg   = hash('sha256', $this->_acceso . $this->_clave  );
            $postData=array(
                'telefonoOrigen'    => $phone,
                'mensaje'           =>$msj,
                'hashg'         => "$hashg"
            );

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_HEADER, false);
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt(
                    $ch, 
                    CURLOPT_POSTFIELDS, 
                    http_build_query($postData)
                    );
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            //Retorno  
            $result = curl_exec($ch);
            curl_close($ch);
             return $result;
            //var_dump( $result);
        } catch (Exception $exc) {
            return $exc->getMessage();
        }
        
    }
    
    public function calcular_distancia($postData){
        $url="http://psiweb.ddns.net:7020/api/distanciaactu";
        $gid = $postData["gid"];
        $hashg   = hash('sha256', $this->_acceso . $this->_clave . $gid);
        $postData["hashg"] = $hashg;
        $postData["gestion_id"] = $gid;
        
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HEADER, false);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt(
                $ch, 
                CURLOPT_POSTFIELDS, 
                http_build_query($postData)
                );
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        //Retorno  
        $result = curl_exec($ch);
        curl_close($ch);
        
        return $result;
    }
    
    public function calcular_visitas($gestion_id){
        $url="http://psiweb.ddns.net:7020/api/estadovisitas";
        
        $hashg   = hash('sha256', $this->_acceso . $this->_clave . $gestion_id);
        $postData["hashg"] = $hashg;
        $postData["gestion_id"] = $gestion_id;
        
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HEADER, false);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt(
                $ch, 
                CURLOPT_POSTFIELDS, 
                http_build_query($postData)
                );
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        //Retorno  
        $result = curl_exec($ch);
        curl_close($ch);
        
        return $result;
    }
    
    public function reasignar_trabajo($gestion_id){
        $url="http://psiweb.ddns.net:7020/api/reenviarot";
        
        $hashg   = hash('sha256', $this->_acceso . $this->_clave . $gid);
        $postData["hashg"] = $hashg;
        $postData["gestion_id"] = $gestion_id;
        
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HEADER, false);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt(
                $ch, 
                CURLOPT_POSTFIELDS, 
                http_build_query($postData)
                );
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        //Retorno  
        $result = curl_exec($ch);
        curl_close($ch);
        
        return $result;
    }

    public function registrar_location($data){
        $url=$this->_base.":7020/location/envio";
   
        $company = $data['company'];
        $device = $data['device'];
        $x = $data['x'];
        $y = $data['y'];
        $time = $data['time'];


        $postData=array(
            'company'=>"$company",
            'device'=>"$device",
            'x'=>"$x",
            'y'=>"$y",
            'time'=>"$time"
            );

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HEADER, false);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt(
                $ch, 
                CURLOPT_POSTFIELDS, 
                http_build_query($postData)
                );
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        //Retorno  
        $result = curl_exec($ch);
        curl_close($ch);
        
        return $result;
    }
}