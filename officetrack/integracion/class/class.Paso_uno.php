<?php
class Paso_uno
{
    private $_mysql;
    private $_db = "webpsi_officetrack";
    private $_table = "paso_uno";
    
    public function registrar($mysql, $task_id="", $x="", $y="", $observacion="", $img=array())
    {
        $this->_mysql = $mysql;
        
        try {
            $Conexion = new Conexion();
            $this->_mysql = $Conexion->conectar();
            
            //Iniciar transaccion
            $this->_mysql->beginTransaction();
            
            //Registrar error
            $date = date("Y-m-d H:i:s");
            $sql = "INSERT INTO $this->_db.$this->_table 
                    (
                        task_id, x, y, observacion, casa_img1, casa_img2, casa_img3
                    ) 
                    VALUES 
                    (
                       ?, ?, ?, ?, ?, ?, ?
                    )";

            $valArray = array($task_id, $x, $y, $observacion, $img[1], $img[2], $img[3]);
            $stmt = $this->_mysql->prepare($sql);
            $stmt->execute($valArray);
            
            $this->_mysql->commit();
            $result["estado"] = true;
            $result["msg"] = "Paso uno registrado correctamente";
            return $result;
        } catch (PDOException $error) {            
            //Rollback
            $this->_mysql->rollback();
            $result["estado"] = false;
            $result["msg"] = $error->getMessage();
            $fo = fopen("errorPasoUno.txt", "w+");
            fwrite($fo, serialize($result));
            fclose($fo);
            //Registrar error
            /*
            require_once './class.Error.php';
            $Error = new Error();
            
            $Error->registrar(
                $this->_mysql,
                "err_" . $paso_id, 
                $exc->getMessage(), 
                $exc->getFile() . "(" . $exc->getLine() . ")"
            );*/
            
            return $result;
        }
    }
    
}