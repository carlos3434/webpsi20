<?php
class Catalogo_decos
{
    private $_mysql;
    private $_db = "webpsi_officetrack";
    private $_table = "catalogo_decos";
    
    public $gestion_id;
    public $carnet;
    public $serie;
    public $tarjeta;
    public $cliente;
    public $fecha_registro;
    public $activo;
    
    public function registrar($mysql)
    {
        try {
            $Conexion = new Conexion();
            $this->_mysql = $Conexion->conectar();
            
            //Iniciar transaccion
            $this->_mysql->beginTransaction();
            
            //Registrar decos
            $sql = "INSERT INTO $this->_db.$this->_table 
                    (
                        gestion_id, carnet, serie, tarjeta, 
                        cliente, fecha_registro, accion, activo
                    ) 
                    VALUES 
                    (
                       ?, ?, ?, ?, ?, ?, ?, ?
                    )";

            $valArray = array(
                $this->gestion_id,
                $this->carnet,
                $this->serie,
                $this->tarjeta,
                $this->cliente,
                $this->fecha_registro,
                $this->accion,
                $this->activo
            );
            $stmt = $this->_mysql->prepare($sql);
            $stmt->execute($valArray);
            
            $this->_mysql->commit();
            $result["estado"] = true;
            $result["msg"] = "Deco registrado correctamente";
            return $result;
        } catch (PDOException $error) {            
            //Rollback
            $this->_mysql->rollback();
            $result["estado"] = false;
            $result["msg"] = $error->getMessage();
            $fo = fopen("errorDecos.txt", "w+");
            fwrite($fo, serialize($result));
            fclose($fo);
            
            return $result;
        }
    }
    
}