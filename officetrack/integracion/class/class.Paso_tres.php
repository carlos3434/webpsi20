<?php
class Paso_tres
{
    private $_mysql;
    private $_db = "webpsi_officetrack";
    private $_table = "paso_tres";
    
    public function registrar($mysql, $TaskNumber, $estado, 
                        $observacion, $tvadicional, 
                        $final_img=array(), $firma_img, $boleta_img)
    {
        $this->_mysql = $mysql;
        
        try {
            $Conexion = new Conexion();
            $this->_mysql = $Conexion->conectar();
            
            //Iniciar transaccion
            $this->_mysql->beginTransaction();
            
            //Registrar error
            $date = date("Y-m-d H:i:s");
            $sql = "INSERT INTO $this->_db.$this->_table 
                    (
                        task_id, estado, estado_codigo,
                        observacion, tvadicional, final_img1,
                        final_img2, final_img3,
                        firma_img, boleta_img,
                        fecha_registro
                    ) 
                    VALUES 
                    (
                       ?, ?, ?,
                       ?, ?, ?,
                       ?, ?,
                       ?, ?,
                       ?
                    )";
            
            $estadon=$estado;
            $estadoc="";
            
            $estadoarray=explode("||",$estado);
            if( count($estadoarray)>1 ){
                $estadoc=$estadoarray[0];
                $estadon=$estadoarray[1];
            }
            
            $valArray = array(
                    $TaskNumber, $estadon, $estadoc,
                    $observacion, $tvadicional, $final_img[1],
                    $final_img[2], $final_img[3],
                    $firma_img, $boleta_img,
                    $date
                );
            $stmt = $this->_mysql->prepare($sql);
            $stmt->execute($valArray);
            
            $reporte["id"] = $this->_mysql->lastInsertId();
            
            $this->_mysql->commit();
            $result["estado"] = true;
            $result["msg"] = "Paso tres registrado correctamente";
            $result["data"] = $reporte;
            return $result;
        } catch (PDOException $error) {
            //Rollback
            $this->_mysql->rollback();
            $result["estado"] = false;
            $result["msg"] = $error->getMessage();
            $fo = fopen("errorPasoTres.txt", "w+");
            fwrite($fo, serialize($result));
            fclose($fo);
            //Registrar error
            /*
            require_once './class.Error.php';
            $Error = new Error();
            
            $Error->registrar(
                $this->_mysql,
                "err_" . $paso_id, 
                $exc->getMessage(), 
                $exc->getFile() . "(" . $exc->getLine() . ")"
            );*/
            
            return $result;
        }
    }
}