<?php
class WsEnvio
{
    private $_mysql;
    private $_db = "webpsi_officetrack";
    private $_table = "wsenvios";
    
    public function registrar($mysql, $trama, $response)
    {
        $this->_mysql = $mysql;
        
        try {
            
            //Iniciar transaccion
            $this->_mysql->beginTransaction();
            
            //Registrar error
            $fecha = date("Y-m-d H:i:s");
            $sql = "INSERT INTO $this->_db.$this->_table 
                    (
                        fecha, trama, response
                    ) 
                    VALUES 
                    (
                       ?, ?, ?
                    )";

            $valArray = array($fecha, $trama, $response);
            $stmt = $this->_mysql->prepare($sql);
            $stmt->execute($valArray);
            
            $this->_mysql->commit();
            $result["estado"] = true;
            $result["msg"] = "Trama enviada hacia OT";
            return $result;
        } catch (PDOException $error) {            
            //Rollback
            $this->_mysql->rollback();
            $result["estado"] = false;
            $result["msg"] = $error->getMessage();
            $fo = fopen("errorWsEnvios.txt", "w+");
            fwrite($fo, serialize($result));
            fclose($fo);
            //Registrar error
            /*
            require_once './class.Error.php';
            $Error = new Error();
            
            $Error->registrar(
                $this->_mysql,
                "err_" . $paso_id, 
                $exc->getMessage(), 
                $exc->getFile() . "(" . $exc->getLine() . ")"
            );*/
            
            return $result;
        }
    }
    
}