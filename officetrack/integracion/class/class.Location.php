<?php

class Location
{

    private $_db = "webpsi_officetrack";
    private $_table = "locations";
    private $_data;
    
    protected $_LastX;
    protected $_LastY;
    protected $_MobileNumber;
    protected $_EmployeeNum;
    protected $_LastBattery;
    protected $_TimeStamp;

    public function getLocations($db, $codArray, $numArray, $date) {
        try {
            //Iniciar transaccion
            $db->beginTransaction();
            $sql = "SELECT 
                        LastX x, LastY y, MobileNumber, 
                        EmployeeNum, LastBattery Battery, TimeStamp, 
                        MAX(
                            CONVERT(
                                    CONCAT(
                                            TRIM(SUBSTR(TimeStamp, 7, 4)),
                                            '-',	 
                                            TRIM(SUBSTR(TimeStamp, 4, 2)), 
                                            '-',
                                            TRIM(SUBSTR(TimeStamp, 1, 2)),
                                            ' ',
                                            TRIM(SUBSTR(TimeStamp, 11)),
                                            ':00'
                                    ), DATETIME
                            )
                        ) tmax 
                    FROM 
                        $this->_db.$this->_table
                    WHERE 
                        TimeStamp <> ''";
            
            if (!empty($codArray))
            {
                $sql .= "AND (" 
                            . $this->employeeList("EmployeeNum", $codArray)
                            . ")";
                $this->_data = $codArray;
            }
            
            if (!empty($numArray))
            {
                $sql .= " AND (" 
                            . $this->employeeList("MobileNumber", $numArray)
                            . ")";
                $this->_data = $numArray;
            }
            
            if ( $date !== "" )
            {
                $sql .= " AND TimeStamp LIKE '$date%'";
            }
            
            //Eliminar a Moty y JR (temporal)
            $sql .= " AND !(EmployeeNum = 89745 OR EmployeeNum = 564856) ";
            
            //Agrupar por empleado
            $sql .= " GROUP BY EmployeeNum";
            
            $bind = $db->prepare($sql);
            $bind->execute($this->_data);
            
            $reporte = array();
            while ($data = $bind->fetch(PDO::FETCH_ASSOC)) {
                $reporte[] = $data;
            }            
            
            $db->commit();
            $result["estado"] = true;
            $result["msg"] = "Datos recuperados";
            $result["data"] = $reporte;
            return $result;
        } catch (PDOException $error) {
            //var_dump($error);
            $db->rollback();
            $result["estado"] = false;
            $result["msg"] = $error->getMessage();
            return $result;
        }
    }
    
    private function employeeList($field, $array)
    {
        $sql = "";
        
        foreach ($array as $val)
        {
            $sql .= " OR $field=?";
        }
        return substr($sql, 4);
    }
    
    public function getPath($db, $date, $code){
        try {
            //Iniciar transaccion
            $db->beginTransaction();
            $sql = "SELECT 
                        LastX X, LastY Y, MobileNumber, 
                        EmployeeNum, LastBattery Battery, 
                        CONVERT(
                            CONCAT(
                                TRIM(SUBSTR(TIMESTAMP, 7, 4)),
                                '-',	 
                                TRIM(SUBSTR(TIMESTAMP, 4, 2)), 
                                '-',
                                TRIM(SUBSTR(TIMESTAMP, 1, 2)),
                                ' ',
                                TRIM(SUBSTR(TIMESTAMP, 11)),
                                ':00'
                            ), DATETIME
                        ) t                        
                    FROM 
                        $this->_db.$this->_table 
                    WHERE 
                        TIMESTAMP LIKE CONCAT(?, '%') AND EmployeeNum=? 
                    ORDER BY t DESC";
            
            
            $bind = $db->prepare($sql);
            $bind->execute( array($date, $code) );
            
            $reporte = array();
            while ($data = $bind->fetch(PDO::FETCH_ASSOC)) {
                $reporte[] = $data;
            }            
            
            $db->commit();
            $result["estado"] = true;
            $result["msg"] = "Datos recuperados";
            $result["data"] = $reporte;
            return $result;
        } catch (PDOException $error) {
            //var_dump($error);
            $db->rollback();
            $result["estado"] = false;
            $result["msg"] = $error->getMessage();
            return $result;
        }
    }

}
