<?php
class Error
{    
    private $_table = "errores";
    private $_mysql;
    
    public function registrar($mysql, $codigo, $mensaje, $archivo)
    {  
        $this->_mysql = $mysql;
            
        try {
            
            //Iniciar transaccion
            $this->_mysql->beginTransaction();
            
            //Registrar error
            $date = date("Y-m-d H:i:s");
            $sql = "INSERT INTO webpsi_officetrack.$this->_table 
                    (
                        codigo, mensaje, archivo, fecha
                    ) 
                    VALUES 
                    (
                       ?, ?, ?, ?
                    )";

            $valArray = array($codigo, $mensaje, $archivo, $date);
            $stmt = $this->_mysql->prepare($sql);
            $stmt->execute($valArray);
            
            $this->_mysql->commit();
            $result["estado"] = true;
            $result["msg"] = "Proyecto registrado correctamente";
        } catch (PDOException $error) {
            $this->_mysql->rollback();
            $result["estado"] = false;
            $result["msg"] = $error->getMessage();
        }
    }
}