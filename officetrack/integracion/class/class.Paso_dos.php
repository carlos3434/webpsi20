<?php
class Paso_dos
{
    private $_mysql;
    private $_db = "webpsi_officetrack";
    private $_table = "paso_dos";
    
    public function registrar($mysql, $TaskNumber, $tap_img=array(), 
                        $modem_img=array(), $tv_img=array(),
                        $problema_img=array(), $motivo, $observacion="123")
    {
        $this->_mysql = $mysql;
        
        try {
            $Conexion = new Conexion();
            $this->_mysql = $Conexion->conectar();
            
            //Iniciar transaccion
            $this->_mysql->beginTransaction();
            
            //Registrar error
            $date = date("Y-m-d H:i:s");
            $sql = "INSERT INTO $this->_db.$this->_table 
                    (
                        task_id, tap_img1, tap_img2, tap_img3,
                        modem_img1, modem_img2, modem_img3,
                        tv_img1, tv_img2, tv_img3,
                        problema_img1, problema_img2, problema_img3,
                        motivo, observacion
                    ) 
                    VALUES 
                    (
                       ?, ?, ?, ?,
                       ?, ?, ?,
                       ?, ?, ?,
                       ?, ?, ?,
                       ?, ?
                    )";

            $valArray = array(
                $TaskNumber,
                $tap_img[1],
                $tap_img[2],
                $tap_img[3],
                $modem_img[1],
                $modem_img[2],
                $modem_img[3],
                $tv_img[1],
                $tv_img[2],
                $tv_img[3],
                $problema_img[1],
                $problema_img[2],
                $problema_img[3],
                $motivo,
                $observacion
            );
            $stmt = $this->_mysql->prepare($sql);
            $stmt->execute($valArray);
            
            $this->_mysql->commit();
            $result["estado"] = true;
            $result["msg"] = "Paso dos registrado correctamente";
            return $result;
        } catch (PDOException $error) {
            //Rollback
            $this->_mysql->rollback();
            $result["estado"] = false;
            $result["msg"] = $error->getMessage();
            $fo = fopen("errorPasoDos.txt", "w+");
            fwrite($fo, serialize($result));
            fclose($fo);
            //Registrar error
            /*
            require_once './class.Error.php';
            $Error = new Error();
            
            $Error->registrar(
                $this->_mysql,
                "err_" . $paso_id, 
                $exc->getMessage(), 
                $exc->getFile() . "(" . $exc->getLine() . ")"
            );*/
            
            return $result;
        }
    }
}