<?php
class Tarea
{
    private $_mysql;
    private $_db = "webpsi_officetrack";
    private $_table = "tareas";
    
    public function registrar($mysql, $task_id, $cod_tecnico, 
            $paso, $fec_recepcion, $cliente, $fec_agenda)
    {
        $this->_mysql = $mysql;
        
        try {
            $Conexion = new Conexion();
            $this->_mysql = $Conexion->conectar();
            
            //Iniciar transaccion
            $this->_mysql->beginTransaction();
            
            //Registrar error
            $sql = "INSERT INTO $this->_db.$this->_table 
                    (
                        task_id, cod_tecnico, 
                        paso, fecha_recepcion, 
                        cliente, fecha_agenda
                    ) 
                    VALUES 
                    (
                       ?, ?, 
                       ?, ?, 
                       ?, ?
                    )";

            $valArray = array(
                    $task_id, $cod_tecnico, 
                    $paso, $fec_recepcion, 
                    $cliente, $fec_agenda
                );
            $stmt = $this->_mysql->prepare($sql);
            $stmt->execute($valArray);
            
            //Ultimo ID registrado
            $id = $this->_mysql->lastInsertId();
            
            $this->_mysql->commit();
            $result["estado"] = true;
            $result["msg"] = "Tarea registrada correctamente";
            $result["id"] = $id;
            return $result;
        } catch (PDOException $error) {
            $this->_mysql->rollback();
            $result["estado"] = false;
            $result["msg"] = $error->getMessage();
            return $result;
        }
    }
    
}