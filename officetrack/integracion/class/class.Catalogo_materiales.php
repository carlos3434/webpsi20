<?php
class Catalogo_materiales
{
    private $_mysql;
    private $_db = "webpsi_officetrack";
    private $_table = "catalogo_materiales";
    
    public $id;
    public $material;
    
    public function buscar($mysql)
    {    
        try {
            $Conexion = new Conexion();
            $this->_mysql = $Conexion->conectar();
            
            //Iniciar transaccion
            $this->_mysql->beginTransaction();
            
            $sql = "SELECT 
                        id, material
                    FROM
                        $this->_db.$this->_table
                    WHERE
                        id IS NOT NULL ";

            $valArray = array();
            
            if ( $this->id != "" )
            {
                $sql .= " AND id = ?";
                $valArray[] = $this->id;
            }
            if ( $this->material != "" )
            {
                $sql .= " AND material = ?";
                $valArray[] = $this->material;
            }
            
            $stmt = $this->_mysql->prepare($sql);
            $stmt->execute($valArray);
            
            $reporte = array();
            while ($data = $stmt->fetch(PDO::FETCH_ASSOC)) {
                $reporte[] = $data;
            }            
            
            $this->_mysql->commit();
            $result["estado"] = true;
            $result["msg"] = "Datos recuperados";
            $result["data"] = $reporte;
            return $result;
        } catch (PDOException $error) {            
            //Rollback
            $this->_mysql->rollback();
            $result["estado"] = false;
            $result["msg"] = $error->getMessage();            
            return $result;
        }
    }
    
    
}