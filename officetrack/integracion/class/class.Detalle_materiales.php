<?php
class Detalle_materiales
{
    private $_mysql;
    private $_db = "webpsi_officetrack";
    private $_table = "detalle_materiales";
    
    public $paso_tres_id;
    public $material_id;
    public $utilizado;
    public $stock;
    public $fecha_registro;
    public $serie;
    
    public function registrar($mysql)
    {        
        try {
            $Conexion = new Conexion();
            $this->_mysql = $Conexion->conectar();
            
            //Iniciar transaccion
            $this->_mysql->beginTransaction();
            
            //Registrar decos
            $sql = "INSERT INTO $this->_db.$this->_table 
                    (
                        paso_tres_id, material_id, 
                        utilizado, stock, fecha_registro,
                        serie
                    ) 
                    VALUES 
                    (
                       ?, ?, ?, ?, ?, ?
                    )";

            $valArray = array(
                $this->paso_tres_id,
                $this->material_id,
                $this->utilizado,
                $this->stock,
                $this->fecha_registro,
                $this->serie
            );
            $stmt = $this->_mysql->prepare($sql);
            $stmt->execute($valArray);
            
            $this->_mysql->commit();
            $result["estado"] = true;
            $result["msg"] = "Material registrado correctamente";
            return $result;
        } catch (PDOException $error) {            
            //Rollback
            $this->_mysql->rollback();
            $result["estado"] = false;
            $result["msg"] = $error->getMessage();
            
            return $result;
        }
    }
    
}