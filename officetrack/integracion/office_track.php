<?php
// Allow from any origin
if (isset($_SERVER['HTTP_ORIGIN'])) {
    header('Access-Control-Allow-Origin: *');
    header('Access-Control-Allow-Credentials: true');
    header('Access-Control-Max-Age: 86400');    // cache for 1 day
}

// Access-Control headers are received during OPTIONS requests
if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {

    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
    {
        header("Access-Control-Allow-Methods: POST");
    }

    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
    {
        $server = $_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS'];
        header("Access-Control-Allow-Headers: $server");
    }
    exit(0);
}

require_once './ot.config.php';
require_once './class/class.Utils.php';
require_once './class/class.OfficeTrack.php';
//Almacenar trama enviada hacia OT
require_once './class/class.Conexion.php';
require_once './class/class.WsEnvio.php';

$Utils = new Utils();
$OfficeTrack = new OfficeTrack();
$WsEnvio = new WsEnvio();

//Existe la variable "cadena"
$json = str_replace("&", "%26", $_POST["cadena"]);
if (isset($_POST["cadena"]) and ! empty($json)) 
{
    try {
        //Validar cadena json
        $object = json_decode($json);
        if (is_object($object)) 
        {
            //Cadena json OK, agregar usuario y clave
            $object->UserName = base64_decode($otAccess["UserName"]);
            $object->Password = base64_decode($otAccess["Password"]);
                        
            //Convertir a XMl
            $xml = $Utils->arrayToXml(
                    $object, 
                    '<CreateOrUpdateTaskRequest></CreateOrUpdateTaskRequest>'
            );

            $OfficeTrack->set_wsdl($otWsdl['tareas']['WSDL']);
            $tareas = $OfficeTrack->get_client();

            $result = $tareas->CreateOrUpdateTask(array('Request' => $xml));
            $otRes = $result->CreateOrUpdateTaskResult;
            echo $otRes;
            //$fo = fopen("/var/www/test/result_registro", "w+");
            //fwrite($fo, date("Y-m-d H:i:s") . $result->CreateOrUpdateTaskResult);
            //fclose($fo);
            //Almacenar trama enviada hacia OT
            $Conexion = new Conexion();
            $db = $Conexion->conectar();
            
            $WsEnvio->registrar($db, $json, $result->CreateOrUpdateTaskResult);
            
        } else {
            //Cadena KO
            throw new Exception("Cadena Json no valida");
        }
    } catch (Exception $exc) {
        $fo = fopen("/var/www/test/error_registro", "w+");
        fwrite($fo, date("Y-m-d H:i:s") . $exc->getMessage());
        fclose($fo);
        /*
        echo $exc->getMessage();
        
        require_once './class/class.Error.php';
        $Error = new Error();
        $Error->registrar(
                "err0001", 
                $exc->getMessage(), 
                $exc->getFile() . "(" . $exc->getLine() . ")"
            );
        */
    }
}







