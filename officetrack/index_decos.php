<?php
require_once './integracion/class/class.Error.php';
require_once './integracion/class/class.Utils.php';
require_once './integracion/class/class.Conexion.php';

require_once './integracion/class/class.Tarea.php';
require_once './integracion/class/class.Paso_uno.php';
require_once './integracion/class/class.Paso_dos.php';
require_once './integracion/class/class.Paso_tres.php';
require_once './integracion/class/class.Catalogo_decos.php';
require_once './integracion/class/class.Catalogo_materiales.php';
require_once './integracion/class/class.Detalle_materiales.php';
require_once './integracion/class/class.Asistencia_tecnico.php';

$Error = new Error();
$Utils = new Utils();
$Tarea = new Tarea();
$Conexion = new Conexion();
$Asistencia = new Asistencia_tecnico();

if (isset($_POST["forms"]) and ! empty($_POST))
{
    $xml = trim($_POST["forms"]);
    
    //Cadena vacia
    try {
        if ($xml === "") {
            throw new Exception("Cadena vac&iacute;a.");
        }
    } catch (Exception $exc) {
        //Registrar en DB lo capturado
        //echo $exc->getTraceAsString();
    }

    //XML valido
    try {
        if ($Utils->is_valid_xml($xml) === false) {
            throw new Exception("Cadena XML no valida");
        }
    } catch (Exception $exc) {
        //Registrar en DB lo capturado
        //echo $exc->getTraceAsString();
    }
    
    //Response de la tarea enviada
    echo "_OK_";

    //Conexion y registro a DB
    $mysql = $Conexion->conectar();
    
    $fileContents = str_replace(array(" "), array("+"), $xml);

    //Captura de datos
    $simpleXml = simplexml_load_string($xml);
    
    /**
     * Entradas y tipos
     * 
     * Asistencia:
     * Ingreso     21
     * Salida      22
     * Vacaciones 100
     * Enfermedad 102
     */
    $EventType = $simpleXml->EventType;
    $EntryType = $simpleXml->EntryType;
    $entryArray = array(21, 22, 100, 102);
    
    //Ubicacion
    $x = $simpleXml->EntryLocation->X;
    $y = $simpleXml->EntryLocation->Y;
    
    $EmployeeNumber     = $simpleXml->Employee->EmployeeNumber;
    
    $TaskNumber         = $simpleXml->Task->TaskNumber;
    $Status             = $simpleXml->Task->Status;
    $Description        = $simpleXml->Task->Description;
    $CustomerName       = $simpleXml->Task->CustomerName;
    $Data1              = $simpleXml->Task->Data1;
    $Data2              = $simpleXml->Task->Data2;
    $Data3              = $simpleXml->Task->Data3;
    $Data4              = $simpleXml->Task->Data4;
    $Data6              = $simpleXml->Task->Data6;
    $Data7              = $simpleXml->Task->Data7;
    $Data8              = $simpleXml->Task->Data8;
    $Data10             = $simpleXml->Task->Data10;
    $Data11             = $simpleXml->Task->Data11;
    $Data12             = $simpleXml->Task->Data12;
    $Data13             = $simpleXml->Task->Data13;
    $Data14             = $simpleXml->Task->Data14;
    $Data15             = $simpleXml->Task->Data15;
    $Data16             = $simpleXml->Task->Data16;
    $Data17             = $simpleXml->Task->Data17;
    $Data18             = $simpleXml->Task->Data18;
    $Data19             = $simpleXml->Task->Data19;
    $Data20             = $simpleXml->Task->Data20;
    $Data21             = $simpleXml->Task->Data21;
    $Data22             = $simpleXml->Task->Data22;
    $Data23             = $simpleXml->Task->Data23;
    $Data24             = $simpleXml->Task->Data24;
    $Data25             = $simpleXml->Task->Data25;
    $Data26             = $simpleXml->Task->Data26;
    $Data27             = $simpleXml->Task->Data27;
    $Data28             = $simpleXml->Task->Data28;
    $StartDate          = $simpleXml->Task->StartDate;
    $StartDateAge       = $simpleXml->Task->StartDateAge;
    $StartDateFromEpoch = $simpleXml->Task->StartDateFromEpoch;
    $DueDate            = $simpleXml->Task->DueDate;
    $DueDateAge         = $simpleXml->Task->DueDateAge;
    $DueDateFromEpoch   = $simpleXml->Task->DueDateFromEpoch;
    
    //Registrar solo si existen datos de formulario
    if ( isset( $simpleXml->Form->Name ) )
    {
        $paso = trim($simpleXml->Form->Name);
        $paso_id = substr($paso, 0, 5);
        
        try {
            //Iniciar transaccion
            $mysql->beginTransaction();
            
            //Registrar datos de la tarea
            $date = date("Y-m-d H:i:s");
            
            $saveTask = $Tarea->registrar(
                    $mysql, $TaskNumber, $EmployeeNumber, 
                    $paso, $date, 
                    $Data4, $Description);
            
            if ( $saveTask["estado"] === false ) {
                throw new Exception("Error al registrar tarea");
            }
            
            //Resultado de registro
            $doSave = array();
            
            //Registrar datos Paso 1
            if ( $paso_id === '0001-' )
            {
                $Paso_uno = new Paso_uno();
                $casa_img = array(1=>"", 2=>"", 3=>"");
                
                if ( isset( $simpleXml->Files->File ) )
                {
                    $n = 1;
                    foreach ($simpleXml->Files->File as $obj) {
                        $casa_img[$n] = $obj->Data;
                        $n++;
                    }
                }
                
                $doSave = $Paso_uno->registrar($mysql, 
                        $saveTask["id"], $x, $y, $casa_img);
                
                //Agregar movimiento tecnico en sitio
                $sql = "SELECT webpsi_criticos.GenerarInicio($TaskNumber)";
                $mysql->query($sql);
            }
            
            //Registrar datos Paso 2
            if ( $paso_id === '0002-' )
            {
                $Paso_dos = new Paso_dos();
                
                //Datos
                $motivo= "";
                $observaciones = "";
                
                if ($simpleXml->Form->Fields->Field->Id == 'Motivo Problema')
                {
                    $motivo = $simpleXml->Form->Fields->Field->Value;
                }
                
                if ($simpleXml->Form->Fields->Field->Id == 'Observaciones')
                {
                    $observaciones = $simpleXml->Form->Fields->Field->Value;
                }
                
                //Imagenes
                $tap_img    = array(1=>"", 2=>"", 3=>"");
                $modem_img  = array(1=>"", 2=>"", 3=>"");
                $tv_img     = array(1=>"", 2=>"", 3=>"");
                $prob_img   = array(1=>"", 2=>"", 3=>"");
                
                if ( isset( $simpleXml->Files->File ) )
                {
                    $nt = 1;
                    $nm = 1;
                    $nv = 1;
                    $np = 1;
                    foreach ($simpleXml->Files->File as $obj) {
                        
                        if ( $obj->Id == "Tap" )
                        {
                            $tap_img[$nt] = $obj->Data;
                            $nt++;
                        }
                        
                        if ( $obj->Id == "Conexion Modem" )
                        {
                            $modem_img[$nm] = $obj->Data;
                            $nm++;
                        }
                        
                        if ( $obj->Id == "Conexion TV" )
                        {
                            $tv_img[$nv] = $obj->Data;
                            $nv++;
                        }
                        
                        if ( $obj->Id == "Problema" )
                        {
                            $prob_img[$np] = $obj->Data;
                            $np++;
                        }
                        
                    }
                }
                
                $doSave = $Paso_dos->registrar($mysql,
                        $saveTask["id"],
                        $tap_img,
                        $modem_img,
                        $tv_img,
                        $prob_img,
                        $motivo,
                        $observaciones
                    );
            }
            
            //Registrar Activacion_Decos
            if ( $paso === 'Activacion_Decos' )
            {
                $Catalogo_decos = new Catalogo_decos();
                                
                foreach ($simpleXml->Form->Fields->Field as $key=>$val)
                {
                    //Serie y Tarjeta
                    $serie = "";
                    $tarjeta = "";
                    if (substr(trim($val->Id), 0, 10) == 'Serie Deco') {
                        $serie = $val->Value;
                    }

                    if (substr(trim($val->Id), 0, 13) == 'Serie tarjeta') {
                        $tarjeta = $val->Value;
                    }
                    
                    //Datos para el registro
                    $Catalogo_decos->gestion_id = $TaskNumber;
                    $Catalogo_decos->carnet     = $EmployeeNumber;
                    $Catalogo_decos->serie      = $serie;
                    $Catalogo_decos->tarjeta    = $tarjeta;
                    
                    //Registrar Deco
                    $doSave = $Catalogo_decos->registrar($mysql);
                }
            }
            
            //Registrar datos Paso 3
            if ( $paso_id === '0003-' )
            {
                $Paso_tres = new Paso_tres();
                $Catalogo_materiales = new Catalogo_materiales();
                $Detalle_materiales = new Detalle_materiales();
                                
                //Datos
                $estado = "";
                $observaciones = "";
                $materiales = array();
                                
                foreach ($simpleXml->Form->Fields->Field as $key=>$val) {
                    if ($val->Id == 'Estado') {
                        $estado = $val->Value;
                    } elseif ($val->Id == 'Observaciones') {
                        $observaciones = $val->Value;
                    } else {
                        $arrMat = explode("_", $val->Id);
                        $material = $arrMat[0];
                        $material_id = 0;
                        $Catalogo_materiales->material = $material;
                        $materialArray = $Catalogo_materiales->buscar($mysql);
                        
                        foreach ($materialArray as $key=>$val) {
                            $material_id = $val["id"];
                        }
                        
                        $materiales[$material_id][strtolower($arrMat[1])] = $val->Value;
                    }
                }
                
                $final_img = array(1=>"", 2=>"", 3=>"");
                $firma_img = "";
                
                if ( isset( $simpleXml->Files->File ) )
                {
                    $n = 1;
                    foreach ($simpleXml->Files->File as $obj) {
                        
                        if ( trim($obj->Id) == "Trabajo Final" )
                        {
                            $final_img[$n] = $obj->Data;
                            $n++;
                        }
                        
                        if ( trim($obj->Id) == "Firma Cliente" )
                        {
                            $firma_img = $obj->Data;
                        }
                    }
                }
                
                $doSave = $Paso_tres->registrar($mysql,
                        $saveTask["id"],
                        $estado,
                        $observaciones,
                        $final_img,
                        $firma_img
                    );
                $idPasoTres = 0;
                if ( !empty($doSave) ) {
                    $idPasoTres = $doSave["data"]["id"];
                }
                
                //Registro de materiales
                if ( !empty($materiales) ) {
                    $fecha_registro = date("Y-m-d H:i:s");
                    foreach ($materiales as $key=>$val) {
                        //Asignar valores
                        $Detalle_materiales->paso_tres_id = $idPasoTres;
                        $Detalle_materiales->material_id = $key;
                        $Detalle_materiales->utilizado = $val["utilizado"];
                        $Detalle_materiales->stock = $val["stock"];
                        $Detalle_materiales->fecha_registro = $fecha_registro;
                        
                        //Registrar
                        $regmat = $Detalle_materiales->registrar($mysql);
                    }
                }
                
                //Agregar movimiento tecnico en sitio
                $sql = "SELECT webpsi_criticos.GenerarCierre($TaskNumber)";
                $mysql->query($sql);
            }
            
            //Validar correcto grabado del paso (1, 2, o 3)
            try {
                if ( $doSave["estado"] === false )
                {
                    throw new Exception( $doSave["msg"] );
                }
            } catch (Exception $exc) {
                //Deshacer grabado
                $mysql->rollback();
                
                //Registrar error
                $Error->registrar(
                    $mysql,
                    "err_" . $paso_id, 
                    $exc->getMessage(), 
                    $exc->getFile() . "(" . $exc->getLine() . ")"
                );
            }

            $mysql->commit();
            $result["estado"] = true;
            $result["msg"] = "Proyecto registrado correctamente";
        } catch (PDOException $error) {
            $mysql->rollback();
            $result["estado"] = false;
            $result["msg"] = $error->getMessage();
            
            //Registrar error
            $Error->registrar(
                $mysql,
                "err_", 
                $exc->getMessage(), 
                $exc->getFile() . "(" . $exc->getLine() . ")"
            );
        }        
        
    }
    
    //Registrar Asistencia y entradas
    if ($EventType==1 and in_array($EntryType, $entryArray)) {
        $Asistencia->id_entrada = $EntryType;
        $Asistencia->fecha_asistencia = date("Y-m-d H:i:s");
        $Asistencia->direccion = $simpleXml->EntryLocation->Address;
        $Asistencia->coor_x = $x;
        $Asistencia->coor_y = $y;
        $Asistencia->descripcion = $simpleXml->Data;
        $Asistencia->nombre_tecnico = $simpleXml->Employee->FirstName;
        $Asistencia->numero_tecnico = $simpleXml->Employee->EmployeeNumber;
        
        $Asistencia->registrar($mysql);
    }
    
    //if ($TaskNumber == 2011) {
    //    $fo = fopen($TaskNumber . "_test_" . date("dmYHis") . ".txt", "w+");
    //    fwrite($fo, $xml);
    //    fclose($fo);
    //}
    
} else {
    echo "Nothing to do";
}

