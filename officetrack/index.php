<?php
require_once './integracion/class/class.Error.php';
require_once './integracion/class/class.Utils.php';
require_once './integracion/class/class.Conexion.php';

require_once './integracion/class/class.Tarea.php';
require_once './integracion/class/class.Paso_uno.php';
require_once './integracion/class/class.Paso_dos.php';
require_once './integracion/class/class.Paso_tres.php';
require_once './integracion/class/class.Catalogo_decos.php';
require_once './integracion/class/class.Catalogo_materiales.php';
require_once './integracion/class/class.Detalle_materiales.php';
require_once './integracion/class/class.Asistencia_tecnico.php';
require_once './integracion/class/class.CambioDireccion.php';
require_once './integracion/class/class.Validacion_activa.php';


$Error = new Error();
$Utils = new Utils();
$Tarea = new Tarea();
$Conexion = new Conexion();
$Asistencia = new Asistencia_tecnico();
/*
if (isset($_POST))
{
    $fo = fopen("_form_deco_" . date("dmYHis") . ".txt", "w+");
    fwrite($fo, serialize($_POST));
    fclose($fo);
}*/
    
if (isset($_POST["forms"]) and ! empty($_POST))
{
    //Conexion y registro a DB
    $mysql = $Conexion->conectar();

    $xml = trim($_POST["forms"]);

    
    /*$sql = "INSERT INTO webpsi_officetrack.tareas_ot "
        ." (tareas) "
        ." values (".$xml.") ";

    $mysql->query($sql);*/
    
    //Cadena vacia
    try {
        if ($xml === "") {
            throw new Exception("Cadena vac&iacute;a.");
        }
    } catch (Exception $exc) {
        //Registrar en DB lo capturado
        //echo $exc->getTraceAsString();
    }

    //XML valido
    try {
        if ($Utils->is_valid_xml($xml) === false) {
            throw new Exception("Cadena XML no valida");
        }
    } catch (Exception $exc) {
        //Registrar en DB lo capturado
        //echo $exc->getTraceAsString();
    }
    
    //Response de la tarea enviada
    echo "_OK_";

    
    $fileContents = str_replace(array(" "), array("+"), $xml);

    //Captura de datos
    $simpleXml = simplexml_load_string($xml);

   
    /**
     * Entradas y tipos
     * 
     * Asistencia:
     * Ingreso     21
     * Salida      22
     * Vacaciones 100
     * Enfermedad 102
     */
    $EventType = $simpleXml->EventType;
    $EntryType = $simpleXml->EntryType;
    $entryArray = array(21, 22, 100, 102);
    
    //Ubicacion
    $x = $simpleXml->EntryLocation->X;
    $y = $simpleXml->EntryLocation->Y;
    
    $EmployeeNumber     = $simpleXml->Employee->EmployeeNumber;
    
    $TaskNumber         = $simpleXml->Task->TaskNumber;
    $Status             = $simpleXml->Task->Status;
    $Description        = $simpleXml->Task->Description;
    $CustomerName       = $simpleXml->Task->CustomerName;
    $Data1              = $simpleXml->Task->Data1;
    $Data2              = $simpleXml->Task->Data2;
    $Data3              = $simpleXml->Task->Data3;
    $Data4              = $simpleXml->Task->Data4;
    $Data6              = $simpleXml->Task->Data6;
    $Data7              = $simpleXml->Task->Data7;
    $Data8              = $simpleXml->Task->Data8;
    $Data10             = $simpleXml->Task->Data10;
    $Data11             = $simpleXml->Task->Data11;
    $Data12             = $simpleXml->Task->Data12;
    $Data13             = $simpleXml->Task->Data13;
    $Data14             = $simpleXml->Task->Data14;
    $Data15             = $simpleXml->Task->Data15;
    $Data16             = $simpleXml->Task->Data16;
    $Data17             = $simpleXml->Task->Data17;
    $Data18             = $simpleXml->Task->Data18;
    $Data19             = $simpleXml->Task->Data19;
    $Data20             = $simpleXml->Task->Data20;
    $Data21             = $simpleXml->Task->Data21;
    $Data22             = $simpleXml->Task->Data22;
    $Data23             = $simpleXml->Task->Data23;
    $Data24             = $simpleXml->Task->Data24;
    $Data25             = $simpleXml->Task->Data25;
    $Data26             = $simpleXml->Task->Data26;
    $Data27             = $simpleXml->Task->Data27;
    $Data28             = $simpleXml->Task->Data28;
    $StartDate          = $simpleXml->Task->StartDate;
    $StartDateAge       = $simpleXml->Task->StartDateAge;
    $StartDateFromEpoch = $simpleXml->Task->StartDateFromEpoch;
    $DueDate            = $simpleXml->Task->DueDate;
    $DueDateAge         = $simpleXml->Task->DueDateAge;
    $DueDateFromEpoch   = $simpleXml->Task->DueDateFromEpoch;

    //if($simpleXml->Employee->EmployeeNumber == "800318"){
    if($simpleXml->Employee->EmployeeNumber == "666"){
        $fo = fopen("prueba_offic.txt", "w+");
        fwrite($fo, $xml . "\r\n");
        fwrite($fo, serialize($xml) . "\r\n");
        fclose($fo);
    }

    //Registrar solo si existen datos de formulario
    if ( isset( $simpleXml->Form->Name ) )
    {
        $paso = trim($simpleXml->Form->Name);
        $paso_id = substr($paso, 0, 5);
        
        try {
            //Iniciar transaccion
            $mysql->beginTransaction();
            
            //Registrar datos de la tarea
            $date = date("Y-m-d H:i:s");
            
            $saveTask = array();
            
            if ( trim($TaskNumber) != "" )
            {

                $fo = fopen("form_ot.txt", "w+");
                fwrite($fo, $xml . "\r\n");
                fwrite($fo, serialize($xml) . "\r\n");
                fclose($fo);

                $saveTask = $Tarea->registrar(
                        $mysql, $TaskNumber, $EmployeeNumber, 
                        $paso, $date, 
                        $Data4, $Description);

                if ( $saveTask["estado"] === false ) {
                    throw new Exception("Error al registrar tarea");
                }
            }
            
            //Resultado de registro
            $doSave = array();
            
            /**
             * Registrar datos Paso 1:
             * 
             * 0001 => Comun
             * 0009 => "Inicio Devoluciones"
             */
            if ( $paso_id === '0001-' or $paso_id === '0009-')
            {

                $fo = fopen("paso_uno.txt", "w+");
                fwrite($fo, $xml . "\r\n");
                fwrite($fo, serialize($xml) . "\r\n");
                fclose($fo);
                $Paso_uno = new Paso_uno();
                $casa_img = array(1=>"", 2=>"", 3=>"");
                
                if ( isset( $simpleXml->Files->File ) )
                {
                    $n = 1;
                    foreach ($simpleXml->Files->File as $obj) {
                        $casa_img[$n] = $obj->Data;
                        $n++;
                    }
                }
                
                $observacion = "";
                
                $doSave = $Paso_uno->registrar($mysql, 
                        $saveTask["id"], $x, $y, $observacion, $casa_img);
                
                try {
                    //Agregar movimiento tecnico en sitio
                    $sql = "SELECT psi.GenerarInicio($TaskNumber, '$x', '$y', '$date')";
                    $mysql->query($sql);
                } catch (PDOException  $e) {
                    $fo = fopen("00_error_inicio_db.txt", "w+");
                    fwrite($fo, $e . "\r\n");
                    fwrite($fo, serialize($e) . "\r\n");
                    fclose($fo);
                }

                //Generar imagenes
                $nimg = 1;
                foreach ($casa_img as $index=>$val) {
                    if (trim($val) != '')
                    {
                        $Utils->save_img_curl(
                                $val, 
                                $TaskNumber, 
                                $saveTask["id"], 
                                $nimg,
                                $index,
                                $mysql
                            );
                        //$nimg++;
                        //insert into imagenes_tareas
                        //(tarea_id, imagen_tipo_id, nombre)
                        //values ($TaskNumber , 8 , $val)
                        $nombre = "p0".$nimg."/g".$TaskNumber."/i".$saveTask["id"]."_".($index).".jpg";
                        
                        $sql = "INSERT INTO webpsi_officetrack.imagenes_tareas "
                             ." (tarea_id, imagen_tipo_id, nombre, fecha_creacion) "
                             ." values (".$saveTask["id"].", '8', '$nombre', now()) ";
                        $mysql->query($sql);
                        
                    }
                }
                
                //Validar distancia, solo para "Pendientes M1"
                /*if ($TaskNumber == 69706)
                {
                    try {
                        $url="http://psiweb.ddns.net/test/wpsiapi/";
                        $hash = md5("10.226.666.969.753" . date("Ymd"));
                        $postData=array(
                             'hashfw'    => "$hash",
                            'gid'       => "$TaskNumber",
                            'method'    => "valida_distancia_inicio",
                            'x'         => "$x",
                            'y'         => "$y"
                        );

                        $ch = curl_init();
                        curl_setopt($ch, CURLOPT_URL, $url);
                        curl_setopt($ch, CURLOPT_HEADER, false);
                        curl_setopt($ch, CURLOPT_POST, true);
                        curl_setopt(
                                $ch, 
                                CURLOPT_POSTFIELDS, 
                                http_build_query($postData)
                                );
                        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                        //Retorno  
                        $result = curl_exec($ch);
                        curl_close($ch);
                        
                        $fo = fopen("01_wpsi_api.txt", "w+");
                        fwrite($fo, $result . $TaskNumber);
                        fclose($fo);
                    } catch (Exception $exc) {
                        //echo $exc->getTraceAsString();
                    }
                }*/
                            
            }
            
            //Registrar datos Paso 2
            if ( $paso_id === '0002-' )
            {
                $fo = fopen("paso_dos.txt", "w+");
                fwrite($fo, $xml . "\r\n");
                fwrite($fo, serialize($xml) . "\r\n");
                fclose($fo);
                $Paso_dos = new Paso_dos();
                
                //Datos
                $motivo= "";
                $observaciones = "";
                
                if ($simpleXml->Form->Fields->Field->Id == 'Motivo Problema')
                {
                    $motivo = $simpleXml->Form->Fields->Field->Value;
                }
                
                if ($simpleXml->Form->Fields->Field->Id == 'Observaciones')
                {
                    $observaciones = $simpleXml->Form->Fields->Field->Value;
                }
                
                //Imagenes
                $tap_img    = array(1=>"", 2=>"", 3=>"");
                $modem_img  = array(1=>"", 2=>"", 3=>"");
                $tv_img     = array(1=>"", 2=>"", 3=>"");
                $prob_img   = array(1=>"", 2=>"", 3=>"");
                
                if ( isset( $simpleXml->Files->File ) )
                {
                    $nt = 1;
                    $nm = 1;
                    $nv = 1;
                    $np = 1;
                    foreach ($simpleXml->Files->File as $obj) {
                        
                        if ( $obj->Id == "Tap" )
                        {
                            $tap_img[$nt] = $obj->Data;
                            $nt++;
                        }
                        
                        if ( $obj->Id == "Conexion Modem" )
                        {
                            $modem_img[$nm] = $obj->Data;
                            $nm++;
                        }
                        
                        if ( $obj->Id == "Conexion TV" )
                        {
                            $tv_img[$nv] = $obj->Data;
                            $nv++;
                        }
                        
                        if ( $obj->Id == "Problema" )
                        {
                            $prob_img[$np] = $obj->Data;
                            $np++;
                        }
                        
                    }
                }
                
                $doSave = $Paso_dos->registrar($mysql,
                        $saveTask["id"],
                        $tap_img,
                        $modem_img,
                        $tv_img,
                        $prob_img,
                        $motivo,
                        $observaciones
                    );
                
                //Generar imagenes paso 2
                $nimg = 2;
                foreach ($tap_img as $index=>$val) {
                    if (trim($val) != '')
                    {
                        $Utils->save_img_curl(
                                $val, 
                                $TaskNumber, 
                                $saveTask["id"], 
                                $nimg,
                                $index,
                                $mysql
                            );
                        //$nimg++;
                        //insert into imagenes_tareas
                        //(tarea_id, imagen_tipo_id, nombre)
                        //values ()
                        $nombre = "p0".$nimg."/g".$TaskNumber."/i".$saveTask["id"]."_".($index).".jpg";
                        
                        $sql = "INSERT INTO webpsi_officetrack.imagenes_tareas "
                             ." (tarea_id, imagen_tipo_id, nombre, fecha_creacion) "
                             ." values (".$saveTask["id"].", '2', '$nombre', now()) ";
                        $mysql->query($sql);
                        
                    }
                }
                
                foreach ($modem_img as $index=>$val) {
                    if (trim($val) != '')
                    {
                        $Utils->save_img_curl(
                                $val, 
                                $TaskNumber, 
                                $saveTask["id"], 
                                $nimg,
                                $index,
                                $mysql
                            );
                        //$nimg++;
                        //insert into imagenes_tareas
                        //(tarea_id, imagen_tipo_id, nombre)
                        //values ()
                        $nombre = "p0".$nimg."/g".$TaskNumber."/i".$saveTask["id"]."_".($index).".jpg";
                        
                        $sql = "INSERT INTO webpsi_officetrack.imagenes_tareas "
                             ." (tarea_id, imagen_tipo_id, nombre, fecha_creacion) "
                             ." values (".$saveTask["id"].", '3', '$nombre', now()) ";
                        $mysql->query($sql);
                    }
                }
                
                foreach ($tv_img as $index=>$val) {
                    if (trim($val) != '')
                    {
                        $Utils->save_img_curl(
                                $val, 
                                $TaskNumber, 
                                $saveTask["id"], 
                                $nimg,
                                $index,
                                $mysql
                            );
                        //$nimg++;
                        //insert into imagenes_tareas
                        //(tarea_id, imagen_tipo_id, nombre)
                        //values ()
                        $nombre = "p0".$nimg."/g".$TaskNumber."/i".$saveTask["id"]."_".($index).".jpg";
                        
                        $sql = "INSERT INTO webpsi_officetrack.imagenes_tareas "
                             ." (tarea_id, imagen_tipo_id, nombre, fecha_creacion) "
                             ." values (".$saveTask["id"].", '4', '$nombre', now()) ";
                        $mysql->query($sql);
                        
                    }
                }
                
                foreach ($prob_img as $index=>$val) {
                    if (trim($val) != '')
                    {
                        $Utils->save_img_curl(
                                $val, 
                                $TaskNumber, 
                                $saveTask["id"], 
                                $nimg,
                                $index,
                                $mysql
                            );
                        //$nimg++;
                        //insert into imagenes_tareas
                        //(tarea_id, imagen_tipo_id, nombre)
                        //values ()
                        $nombre = "p0".$nimg."/g".$TaskNumber."/i".$saveTask["id"]."_".($index).".jpg";
                        
                        $sql = "INSERT INTO webpsi_officetrack.imagenes_tareas "
                             ." (tarea_id, imagen_tipo_id, nombre, fecha_creacion) "
                             ." values (".$saveTask["id"].", '5', '$nombre', now()) ";
                        $mysql->query($sql);
                    }
                }
            }
            
            //Registrar Activacion_Decos
            if ( $paso == 'Activacion_Decos(interno)' )
            {
                 
                $fecha_registro = date("Y-m-d H:i:s");
                $Catalogo_decos = new Catalogo_decos();
                
                //Serie y Tarjeta
                $serie = "";
                $tarjeta = "";
                
                $parImpar = 0;
                foreach ($simpleXml->Form->Fields->Field as $key=>$val)
                {
                    
                    if (substr(trim(strtolower($val->Id)), 0, 10) == 'serie deco') {
                        $serie = $val->Value;
                    }

                    if (substr(trim(strtolower($val->Id)), 0, 13) == 'serie tarjeta') {
                        $tarjeta = $val->Value;
                    }
                    
                    $parImpar++;
                    
                    //Datos para el registro
                    $Catalogo_decos->gestion_id     = $TaskNumber;
                    $Catalogo_decos->carnet         = $EmployeeNumber;
                    $Catalogo_decos->serie          = $serie;
                    $Catalogo_decos->tarjeta        = $tarjeta;
                    $Catalogo_decos->cliente        = 0;
                    $Catalogo_decos->fecha_registro = $fecha_registro;
                    $Catalogo_decos->accion         = "activacion";
                    $Catalogo_decos->activo         = 0;
                    
                    //Registrar Deco
                    if ($parImpar%2===0) {
                        $saveDeco = $Catalogo_decos->registrar($mysql);
                        $serie = "";
                        $tarjeta = "";
                    }
                    
                }
                
                /*//Datos para el registro
                $Catalogo_decos->gestion_id     = $TaskNumber;
                $Catalogo_decos->carnet         = $EmployeeNumber;
                $Catalogo_decos->serie          = $serie;
                $Catalogo_decos->tarjeta        = $tarjeta;
                $Catalogo_decos->fecha_registro = $fecha_registro;
                $Catalogo_decos->activo         = 0;

                //Registrar Deco
                $saveDeco = $Catalogo_decos->registrar($mysql);*/
            }

            // nuevo formulario
            if ( $paso == 'Activacion_Decos' ) {
                $xml = trim($_POST["forms"]);
                $fos = fopen("test_activacionxml.txt", "w+");
                fwrite($fos, $xml . "\r\n");
                fclose($fos);

                if($simpleXml->Form->Fields->Field[0]->Id=='cod_cliente') {
                    $codcliente=$simpleXml->Form->Fields->Field[0]->Value; 
                }
                if($simpleXml->Form->Fields->Field[1]->Id=='requerimiento') {
                    $codactu=$simpleXml->Form->Fields->Field[1]->Value;
                }

                if($codactu!='') {
                $Validacion_activa = new Validacion_activa();
                $Validacion_activa->codactu = $codactu;
                $codactuArray = $Validacion_activa->buscar($mysql);

                //$fo = fopen("test_activacionesp.txt", "w+");
                //fwrite($fo, "Imprimiendo...." . "\r\n");
                foreach ($codactuArray["data"] as $dbIndex=>$dbField) {
                            $estado_ofsc_id = $dbField["estado_ofsc_id"];
                            $carnet_tmp = $dbField["carnet_tmp"];
                            $gestion_id=$dbField["gestion_id"];
                            /*fwrite($fo, "in-> reque:" . $codactu . "\r\n");
                            fwrite($fo, "in-> carnet:" . $EmployeeNumber . "\r\n");
                            fwrite($fo, "estado:" . $estado_ofsc_id. "\r\n");
                            fwrite($fo, "carnet:" . $carnet_tmp . "\r\n");
                            fwrite($fo, "gestion:" . $gestion_id . "\r\n");*/
                 }
                

                if ($estado_ofsc_id=='2' && $carnet_tmp==$EmployeeNumber) {
                    //insertar catalogo decos
                    $fecha_registro = date("Y-m-d H:i:s");
                    $Catalogo_decos = new Catalogo_decos();
                    //Serie y Tarjeta
                    $serie = "";
                    $tarjeta = "";
                    $parImpar = 0;

                    foreach ($simpleXml->Form->Fields->Field as $key=>$val) {
                        
                        if (substr(trim(strtolower($val->Id)), 0, 10) == 'serie_deco') {
                            $serie = $val->Value;
                        }
    
                        if (substr(trim(strtolower($val->Id)), 0, 13) == 'serie_tarjeta') {
                            $tarjeta = $val->Value;
                        }
                        
                        /*Datos para el registro*/
                        $Catalogo_decos->gestion_id     = $gestion_id;
                        $Catalogo_decos->carnet         = $EmployeeNumber;
                        $Catalogo_decos->serie          = $serie;
                        $Catalogo_decos->tarjeta        = $tarjeta;
                        $Catalogo_decos->cliente        = $codcliente;
                        $Catalogo_decos->fecha_registro = $fecha_registro;
                        $Catalogo_decos->accion         = "activacion";
                        $Catalogo_decos->activo         = 0;

                        $parImpar++;

                        //Registrar Deco
                        if ($parImpar%2===0  && $parImpar>3) {
                            //fwrite($fo, "serie ".$parImpar.': '  . $serie . "\r\n");
                            //fwrite($fo, "tarjeta ".$parImpar.': ' .  $tarjeta . "\r\n");
                            $saveDeco = $Catalogo_decos->registrar($mysql);
                            $serie = "";
                            $tarjeta = "";
                        }

                    }
                    //fclose($fo);
                } 
                else {
                     $validacion_activa = new Validacion_activa();
                     $validacion_activa->employee = $EmployeeNumber;
                     $phoneArray = $validacion_activa->consultarphone($mysql);
                     foreach ($phoneArray["data"] as $dbIndex=>$dbField) {
                            $nombre = $dbField["ape_paterno"];
                            $phone = $dbField["celular"];
                     }
                     if ($estado_ofsc_id!='2') {
                        $msj="Tecnico: ". $nombre . ", el requerimiento: ".$codactu. 
                         " no está Iniciado ";
                     } else {
                        $msj="Tecnico: ". $nombre . ", usted no tiene permisos ".
                        "para el requerimiento: ".$codactu ;
                     } 

                       require_once './integracion/class/class.CurlNovus.php';
                       $CurlNovus = new CurlNovus();
                       $notificarStr = $CurlNovus->notificar_noregistro($phone, $msj);
                       $notificarObj = json_decode($notificarStr);
                       $rst=$notificarObj->rst;
                       if($rst==1) {
                       $fose = fopen("test_activacionerror.txt", "w+");
                               fwrite($fose, $notificarObj->msj . "\r\n");
                               fclose($fose);
                       }
                }
              } else { 
                        $validacion_activa = new Validacion_activa();
                        $validacion_activa->employee = $EmployeeNumber;
                        $phoneArray = $validacion_activa->consultarphone($mysql);
                        foreach ($phoneArray["data"] as $dbIndex=>$dbField) {
                               $nombre = $dbField["ape_paterno"];
                               $phone = $dbField["celular"];
                        }

                      $msj="Tecnico: ". $nombre . ", ". 
                           "No se encontraron registros, verifique los ".
                           "datos ingresados e intente nuevamente.";

                       require_once './integracion/class/class.CurlNovus.php';
                       $CurlNovus = new CurlNovus();
                       $notificarStr = $CurlNovus->notificar_noregistro($phone, $msj);
                       $notificarObj = json_decode($notificarStr);
                       $rst=$notificarObj->rst;
                       if($rst==1) {
                       $fose = fopen("test_activacionerror.txt", "w+");
                               fwrite($fose, $notificarObj->msj . "\r\n");
                               fclose($fose);
                       }
              }
            }
            
            /**
             * Activación o refresh de Deco
             */
            if ( $paso == 'Refresh_Deco' )
            {                
                $fo = fopen("prueba_offic.txt", "w+");
                fwrite($fo, $xml . "\r\n");
                fwrite($fo, serialize($xml) . "\r\n");
                fclose($fo);
                $decoArray = array();
                $fecha_registro = date("Y-m-d H:i:s");
                $Catalogo_decos = new Catalogo_decos();
                
                foreach ($simpleXml->Form->Fields->Field as $key=>$val)
                {
                    
                    if (substr(trim(strtolower($val->Id)), -5) == 'tarea') {
                        $decoArray["tarea"] = $val->Value;
                    }
                    
                    if (trim(strtolower($val->Id)) == 'accion') {
                        $decoArray["accion"] = $val->Value;
                    }
                    
                    if (substr(trim(strtolower($val->Id)), 0, 10) == 'serie deco') {
                        $decoArray["deco"][] = $val->Value;
                    }
                    
                }
                
                //Registro
                if ( !empty($decoArray) and count($decoArray) > 0 )
                {
                    $Catalogo_decos->gestion_id     = $decoArray["tarea"];
                    $Catalogo_decos->carnet         = $EmployeeNumber;
                    $Catalogo_decos->tarjeta        = "";
                    $Catalogo_decos->cliente        = "";
                    $Catalogo_decos->fecha_registro = $fecha_registro;
                    $Catalogo_decos->accion         
                            = strtolower(trim($decoArray["accion"]));
                    $Catalogo_decos->activo         = 0;
                    
                    foreach ($decoArray["deco"] as $val) {
                        $Catalogo_decos->serie = $val;
                        $saveDeco = $Catalogo_decos->registrar($mysql);
                    }
                    
                }
            }
            
            /**
             * Refresh de Deco por codigo cliente
             */
            if ( $paso == 'Refresh Cliente' )
            {
                $decoArray = array();
                $fecha_registro = date("Y-m-d H:i:s");
                $Catalogo_decos = new Catalogo_decos();
                $fo = fopen("01_refresh.txt", "w+");fwrite($fo, $xml);fclose($fo);
                $codigo_cliente = "";
                foreach ($simpleXml->Form->Fields->Field as $key=>$val)
                {    
                    if (trim($val->Id) == 'Codigo de Cliente CMS') {
                        $codigo_cliente = $val->Value;
                    }
                }
                
                //Registro
                if ( $codigo_cliente != '' )
                {
                    $Catalogo_decos->gestion_id     = 0;
                    $Catalogo_decos->carnet         = $EmployeeNumber;
                    $Catalogo_decos->serie          = 0;
                    $Catalogo_decos->tarjeta        = 0;
                    $Catalogo_decos->cliente        = $codigo_cliente;
                    $Catalogo_decos->fecha_registro = $fecha_registro;
                    $Catalogo_decos->accion         = 'refresh';
                    $Catalogo_decos->activo         = 0;
                    
                    $saveDeco = $Catalogo_decos->registrar($mysql);
                    
                }
            }
            
            //Registrar datos Paso 3
            if ( $paso_id === '0003-' )
            {
                if($simpleXml->Employee->EmployeeNumber == "666"){
                    $fo = fopen("paso_tres.txt", "w+");
                    fwrite($fo, $xml . "\r\n");
                    fwrite($fo, serialize($xml) . "\r\n");
                    fclose($fo);
                }

                $Paso_tres = new Paso_tres();
                $Catalogo_materiales = new Catalogo_materiales();
                $Detalle_materiales = new Detalle_materiales();
                                
                //Datos
                $estado = "";
                $observaciones = "";
                $tvadicional = "";
                $materiales = array();
                
                //$fo = fopen("material_prueba.txt", "w+");
                foreach ($simpleXml->Form->Fields->Field as $key=>$val) {
                    if ($val->Id == 'Estado') {
                        $estado = $val->Value;
                    } elseif ($val->Id == 'Observaciones') {
                        $observaciones = $val->Value;
                    } else {
                        
                        //fwrite($fo, "Header:" . $val->Id . "\r\n");
                        $arrMat = explode("_", $val->Id);
                        $material = $arrMat[0];
                        $material_id = 0;
                        $Catalogo_materiales->material = $material;
                        $materialArray = $Catalogo_materiales->buscar($mysql);
                        
                        foreach ($materialArray["data"] as $dbIndex=>$dbField) {
                            $material_id = $dbField["id"];
                            fwrite($fo, "Id:" . $material_id . "\r\n");
                        }
                        
                        if ($material_id != 0){
                            $materiales[$material_id][strtolower($arrMat[1])] = $val->Value;
                        }
                    }
                }
                //fclose($fo);
                
                $final_img = array(1=>"", 2=>"", 3=>"");
                $firma_img = "";
                $boleta_img = "";
                
                if ( isset( $simpleXml->Files->File ) )
                {
                    $n = 1;
                    foreach ($simpleXml->Files->File as $obj) {
                        
                        if ( trim($obj->Id) == "Trabajo Final" )
                        {
                            $final_img[$n] = $obj->Data;
                            $n++;
                        }
                        
                        if ( trim($obj->Id) == "Firma Cliente" )
                        {
                            $firma_img = $obj->Data;
                        }
                    }
                }
                
                $doSave = $Paso_tres->registrar($mysql,
                        $saveTask["id"],
                        $estado,
                        $observaciones,
                        $tvadicional,
                        $final_img,
                        $firma_img,
                        $boleta_img
                    );
                
                $idPasoTres = 0;
                if ( !empty($doSave) ) {
                    $idPasoTres = $doSave["data"]["id"];
                }
                
                //Registro de materiales
                if ( !empty($materiales) ) {
                    $fecha_registro = date("Y-m-d H:i:s");
                    foreach ($materiales as $matId=>$val) {
                        //Asignar valores
                        if (trim($val["utilizado"]) == "") {
                            $val["utilizado"] = 0;
                        }
                        if (trim($val["stock"]) == "") {
                            $val["stock"] = 0;
                        }
                        
                        $Detalle_materiales->paso_tres_id = $idPasoTres;
                        $Detalle_materiales->material_id = $matId;
                        $Detalle_materiales->utilizado = $val["utilizado"];
                        $Detalle_materiales->stock = $val["stock"];
                        $Detalle_materiales->fecha_registro = $fecha_registro;
                        $Detalle_materiales->serie = '';
                        
                        //Registrar
                        $regmat = $Detalle_materiales->registrar($mysql);
                    }
                }
                
                //Agregar movimiento tecnico en sitio
                $sql = "SELECT psi.GenerarCierre($TaskNumber, '$estado', '$fecha_registro')";
                $mysql->query($sql);
                
                //Generar imagenes paso 3
                $nimg = 3;
                foreach ($final_img as $index=>$val) {
                    if (trim($val) != '')
                    {
                        $Utils->save_img_curl(
                                $val, 
                                $TaskNumber, 
                                $saveTask["id"], 
                                $nimg,
                                $index,
                                $mysql
                            );
                        //$nimg++;
                        //insert into imagenes_tareas
                        //(tarea_id, imagen_tipo_id, nombre)
                        //values ()
                        $nombre = "p0".$nimg."/g".$TaskNumber."/i".$saveTask["id"]."_".($index).".jpg";
                        
                        $sql = "INSERT INTO webpsi_officetrack.imagenes_tareas "
                             ." (tarea_id, imagen_tipo_id, nombre, fecha_creacion) "
                             ." values (".$saveTask["id"].", '6', '$nombre', now()) ";
                        $mysql->query($sql);
                    }
                }
                
                if (trim($firma_img) != '')
                {
                    $Utils->save_img_curl(
                            $firma_img, 
                            $TaskNumber, 
                            $saveTask["id"], 
                            $nimg,
                            '1',
                            $mysql
                        );
                    //$nimg++;
                    //insert into imagenes_tareas
                    //(tarea_id, imagen_tipo_id, nombre)
                    //values ()
                    $nombre = "p0".$nimg."/g".$TaskNumber."/i".$saveTask["id"]."_1.jpg";
                        
                        $sql = "INSERT INTO webpsi_officetrack.imagenes_tareas "
                             ." (tarea_id, imagen_tipo_id, nombre, fecha_creacion) "
                             ." values (".$saveTask["id"].", '7', '$nombre', now()) ";
                        $mysql->query($sql);
                }
            }
            
            
            //Registrar Inicio - PEX
            if ( $paso_id === '0004-' )
            {

                $xml = trim($_POST["forms"]);


                $Paso_uno = new Paso_uno();
                $casa_img = array(1=>"", 2=>"", 3=>"");
                
                if ( isset( $simpleXml->Files->File ) )
                {
                    $n = 1;
                    foreach ($simpleXml->Files->File as $obj) {
                        $casa_img[$n] = $obj->Data;
                        $n++;
                    }
                }
                
                //Datos
                $problema= "";
                
                if ($simpleXml->Form->Fields->Field->Id == 'Problema')
                {
                    $problema = $simpleXml->Form->Fields->Field->Value;
                }
                
                $doSave = $Paso_uno->registrar($mysql, 
                        $saveTask["id"], $x, $y, $problema, $casa_img);
                
                //Agregar movimiento tecnico en sitio
                $sql = "SELECT psi.GenerarInicio($TaskNumber, '$x', '$y', '$date')";
                $mysql->query($sql);
            }
            
            //Registrar Cierre - PEX
            if ( $paso_id === '0005-' )
            {
                $Paso_tres = new Paso_tres();
                                
                //Datos
                $estado = "";
                $observaciones = "";
                $tvadicional = "";
                
                foreach ($simpleXml->Form->Fields->Field as $key=>$val) {
                    if ($val->Id == 'Estado') {
                        $estado = $val->Value;
                    } elseif ($val->Id == 'Observaciones') {
                        $observaciones = $val->Value;
                    }
                }
                
                $final_img = array(1=>"", 2=>"", 3=>"");
                $firma_img = "";
                $boleta_img = "";
                
                if ( isset( $simpleXml->Files->File ) )
                {
                    $n = 1;
                    foreach ($simpleXml->Files->File as $obj) {
                        
                        if ( trim($obj->Id) == "Foto Trabajo Final" )
                        {
                            $final_img[$n] = $obj->Data;
                            $n++;
                        }
                        
                        if ( trim($obj->Id) == "Firma Cliente" )
                        {
                            $firma_img = $obj->Data;
                        }
                    }
                }
                
                $doSave = $Paso_tres->registrar($mysql,
                        $saveTask["id"],
                        $estado,
                        $observaciones,
                        $tvadicional,
                        $final_img,
                        $firma_img,
                        $boleta_img
                    );
                
                //Agregar movimiento tecnico en sitio
                $sql = "SELECT psi.GenerarCierre($TaskNumber, '$estado', '$date')";
                
                $mysql->query($sql);
            }
            
            /**
             * Formulario cambio de direccion
             * Devoluciones.
             */
            if ( $paso_id === '0008-' )
            {
                //data
                $ubicacion      = "";
                $direccion      = "";
                $referencia     = "";
                $nodo           = "";
                $troba          = "";
                $tap            = "";
                $amplificador   = "";
                $nuevo_x        = "";
                $nuevo_y        = "";
                
                $carnet = $simpleXml->Employee->EmployeeNumber;
                    
                $actualiza_ubicacion = false;
                foreach ($simpleXml->Form->Fields->Field as $key=>$val) {
                    $id = $val->Id;
                    $value = $val->Value;
                    
                    if ($id == 'actualiza_ubicacion' and $value == 'si') 
                    {
                        $actualiza_ubicacion = true;
                    }
                    
                    if ($id == 'ubicacion') {
                        $ubicacion = $val->Value;
                        $ubicacionArray = explode(",", trim($ubicacion));
                        
                        if (isset($ubicacionArray[0]))
                        {
                            $nuevo_x = $ubicacionArray[0];
                        }
                        
                        if (isset($ubicacionArray[1]))
                        {
                            $nuevo_x = $ubicacionArray[1];
                        }
                    }
                    
                    if ($id == 'direccion') {
                        $direccion = $val->Value;
                    }
                    
                    if ($id == 'referencia') {
                        $referencia = $val->Value;
                    }
                    
                    if ($id == 'nodo') {
                        $nodo = $val->Value;
                    }
                    
                    if ($id == 'troba') {
                        $troba = $val->Value;
                    }
                    
                    if ($id == 'tap') {
                        $tap = $val->Value;
                    }
                    
                    if ($id == 'amplificador') {
                        $amplificador = $val->Value;
                    }
                }
                
                //Actualizar xy de direccion y reenviar al tecnico
                if ($actualiza_ubicacion) 
                {
                    //1. Updates tablas y guardar histórico
                    /*$CambioDireccion = new CambioDireccion();
                    $upd = $CambioDireccion->registrar(
                            $TaskNumber, 
                            $carnet, 
                            $nuevo_x, 
                            $nuevo_y, 
                            $direccion, 
                            $referencia
                        );*/
                    
                    //2. Reenvío al técnico
                    
                }
            }
                        
            //Formulario cierre devoluciones
            if ( $paso_id === '0011-' )
            {
                $Paso_tres = new Paso_tres();
                $Catalogo_materiales = new Catalogo_materiales();
                $Detalle_materiales = new Detalle_materiales();
                
                //Datos
                $estado = "";
                $observaciones = "";
                $tvadicional = "";
                $materiales = array();
             
                foreach ($simpleXml->Form->Fields->Field as $key=>$val) {
                    if ($val->Id == 'motivocierre') {
                        $estado = $val->Value;
                    } elseif ($val->Id == 'observaciones') {
                        $observaciones = $val->Value;
                    } elseif ($val->Id == 'TVadicional') {
                        $tvadicional = $val->Value;
                    } else {
                        
                        $arrMat = explode("_", $val->Id);
                        $material = $arrMat[0];
                        $material_id = 0;
                        $Catalogo_materiales->material = $material;
                        $materialArray = $Catalogo_materiales->buscar($mysql);
                        
                        foreach ($materialArray["data"] as $dbIndex=>$dbField) {
                            $material_id = $dbField["id"];
                        }
                        
                        if ($material_id != 0){
                            $materiales[$material_id][strtolower($arrMat[1])] = (string) $val->Value;
                        }
                        $materiales[$material_id]['serie'] = '';
                        
                        //Caso de cable modem
                        if ($arrMat[1]=='modem') {
                            $materiales[$material_id]['utilizado'] = 1;
                            $materiales[$material_id]['stock'] = '';
                            $materiales[$material_id]['serie'] = (string) $val->Value;
                        }
                        
                    }
                    
                }
                
                $final_img = array(1=>"", 2=>"", 3=>"");
                $firma_img = "";
                $boleta_img = "";
                
                if ( isset( $simpleXml->Files->File ) )
                {
                    $n = 1;
                    
                    foreach ($simpleXml->Files->File as $obj) {
                        
                        if ( trim($obj->Id) == "Trabajo Final" )
                        {
                            $final_img[$n] = $obj->Data;
                            $n++;
                        }
                        //Boleta
                        if ( trim($obj->Id) == "Boleta" )
                        {
                            $boleta_img = $obj->Data;
                        }
                        
                        if ( trim($obj->Id) == "Firma Cliente" )
                        {
                            $firma_img = $obj->Data;
                        }
                        
                    }
                }
                
                $doSave = $Paso_tres->registrar($mysql,
                        $saveTask["id"],
                        $estado,
                        $observaciones,
                        $tvadicional,
                        $final_img,
                        $firma_img,
                        $boleta_img
                    );
                
                $idPasoTres = 0;
                if ( !empty($doSave) ) {
                    $idPasoTres = $doSave["data"]["id"];
                }
                                
                //Registro de materiales
                if ( !empty($materiales) ) {
                    $fecha_registro = date("Y-m-d H:i:s");
                    foreach ($materiales as $matId=>$val) {
                        //Asignar valores
                        if (trim($val["utilizado"]) == "") {
                            $val["utilizado"] = 0;
                        }
                        if (trim($val["stock"]) == "") {
                            $val["stock"] = 0;
                        }
                        
                        $Detalle_materiales->paso_tres_id = $idPasoTres;
                        $Detalle_materiales->material_id = $matId;
                        $Detalle_materiales->utilizado = $val["utilizado"];
                        $Detalle_materiales->stock = $val["stock"];
                        $Detalle_materiales->fecha_registro = $fecha_registro;
                        $Detalle_materiales->serie = $val["serie"];
                        
                        //Registrar
                        $regmat = $Detalle_materiales->registrar($mysql);
                    }
                    
                }
                
                //Agregar movimiento pre-liquidar
                $estadoarray=explode("||",$estado);
                $sql = "SELECT psi.GenerarCierre($TaskNumber, '$estadoarray[1]', '$fecha_registro')";
                $mysql->query($sql);
                
                //Generar imagenes paso 3
                $nimg = 3;
                $indice = 1;
                foreach ($final_img as $index=>$val) {
                    if (trim($val) != '')
                    {
                        $Utils->save_img_curl(
                                $val, 
                                $TaskNumber, 
                                $saveTask["id"], 
                                $nimg,
                                $indice,
                                $mysql
                            );
                        
                        $nombre = "p0".$nimg."/g".$TaskNumber."/i".$saveTask["id"]."_".($indice).".jpg";
                        
                        $sql = "INSERT INTO webpsi_officetrack.imagenes_tareas "
                             ." (tarea_id, imagen_tipo_id, nombre, fecha_creacion) "
                             ." values (".$saveTask["id"].", '6', '$nombre', now()) ";
                        $mysql->query($sql);
                        
                        $indice++;
                    }
                }
                
                if (trim($firma_img) != '')
                {
                    $Utils->save_img_curl(
                            $firma_img, 
                            $TaskNumber, 
                            $saveTask["id"], 
                            $nimg,
                            $indice,
                            $mysql
                        );
                    
                    $nombre = "p0".$nimg."/g".$TaskNumber."/i".$saveTask["id"]."_$indice.jpg";
                        
                    $sql = "INSERT INTO webpsi_officetrack.imagenes_tareas "
                         ." (tarea_id, imagen_tipo_id, nombre, fecha_creacion) "
                         ." values (".$saveTask["id"].", '7', '$nombre', now()) ";
                    $mysql->query($sql);

                    $indice++;
                }
                
                if (trim($boleta_img) != '')
                {
                    $Utils->save_img_curl(
                            $boleta_img, 
                            $TaskNumber, 
                            $saveTask["id"], 
                            $nimg,
                            $indice,
                            $mysql
                        );
                    
                    $nombre = "p0".$nimg."/g".$TaskNumber."/i".$saveTask["id"]."_$indice.jpg";

                    $sql = "INSERT INTO webpsi_officetrack.imagenes_tareas "
                         ." (tarea_id, imagen_tipo_id, nombre, fecha_creacion) "
                         ." values (".$saveTask["id"].", '9', '$nombre', now()) ";
                    $mysql->query($sql);
                }
                
                //Cantidad de visitas y reenvio
                /**
                 * Validar devolucion por cliente ausente.
                 */
                $strPos = false;
                if (isset($estadoarray[1])) {
                    $strPos = strpos(strtolower($estado), 'ausente');
                }
                //$strPos = strpos(strtoupper($estado), 'DT02');
                if ($strPos !== false)
                {   
                    require_once './integracion/class/class.CurlNovus.php';
                    $CurlNovus = new CurlNovus();
                    $visitaStr = $CurlNovus->calcular_visitas("$TaskNumber");
                    $visitaObj = json_decode($visitaStr);
                    
                    //Datos de la gestion de la actuacion
                    $taskStr = $CurlNovus->obtener_actu("$TaskNumber");
                    $taskObj = json_decode($taskStr);
                    
                    //Reenviar al tecnico
                    if ($visitaObj->rst == 1 and $visitaObj->cantidad > 0)
                    {
                        //Agregar movimiento "Agendado con tecnico"
                        $sql = "SELECT psi.GenerarReasignacion($TaskNumber)";
                        $mysql->query($sql);
                        
                        $reenviarStr = $CurlNovus->reasignar_trabajo("$TaskNumber");
                        
                    } else if ($visitaObj->rst == 1 and $visitaObj->cantidad == 0) {
                        //invocar funcion de jorge, para cambiar de empresa -> retenciones
                        //act movimiento gestinoes detalles, ult mov.
                        
                        //Para ciertos quiebres cambia a empresa "RETENCIONES"
                        $quiebreArray = array(
                            12,
                        );
                        $quiebreId = $taskObj->datos->quiebre_id;
                        
                        if (array_search($quiebreId, $quiebreArray)!== false) {
                            $sql = "SELECT psi.ActEmpresa($TaskNumber)";
                            $mysql->query($sql);
                        }
                        
                    }
                    
                }
                
                /**
                 * Para "Cliente NO DESEA" (DT06)
                 * pasa a RETENCIONES
                 */
                if (isset($estadoarray[0]) and $estadoarray[0]=='DT06') {
                    $sql = "SELECT psi.ActEmpresa($TaskNumber)";
                    $mysql->query($sql);
                }
                
            }            
            
            //Validar correcto grabado del paso (1, 2, o 3)
            try {
                if ( $doSave["estado"] === false )
                {
                    throw new Exception( $doSave["msg"] );
                }
                
            } catch (Exception $exc) {
                //Deshacer grabado
                $mysql->rollback();
                
                //Registrar error
                $Error->registrar(
                    $mysql,
                    "err_" . $paso_id, 
                    $exc->getMessage(), 
                    $exc->getFile() . "(" . $exc->getLine() . ")"
                );
                
            }

            $mysql->commit();
            $result["estado"] = true;
            $result["msg"] = "Proyecto registrado correctamente";
        } catch (PDOException $error) {
            $mysql->rollback();
            $result["estado"] = false;
            $result["msg"] = $error->getMessage();
            
            //Registrar error
            $Error->registrar(
                $mysql,
                "err_", 
                $exc->getMessage(), 
                $exc->getFile() . "(" . $exc->getLine() . ")"
            );
        }        
        
    }
    $Asistencia->numero_tecnico = $simpleXml->Employee->EmployeeNumber;

    //Registrar Asistencia y entradas
    if ($EventType==1 and in_array($EntryType, $entryArray)) {

        try {            
        

            $fechaAsistencia = $simpleXml->EntryDate;
            $dia = substr($fechaAsistencia, 0,2);
            $mes = substr($fechaAsistencia, 2,2);
            $año = substr($fechaAsistencia, 4,4);

            $hora = substr($fechaAsistencia, 8,2);
            $min = substr($fechaAsistencia, 10,2);
            $seg = substr($fechaAsistencia, 12,2);

            $fecha =  $año."-".$mes."-".$dia." ".$hora.":".$min.":".$seg;

            $Asistencia->id_entrada = $EntryType;
            $Asistencia->fecha_asistencia = $fecha;
            $Asistencia->direccion = $simpleXml->EntryLocation->Address;
            $Asistencia->coor_x = $x;
            $Asistencia->coor_y = $y;
            $Asistencia->descripcion = $simpleXml->Data;
            $Asistencia->nombre_tecnico = $simpleXml->Employee->FirstName;
            $Asistencia->numero_tecnico = $simpleXml->Employee->EmployeeNumber;
            
            $mensaje = $Asistencia->registrar($mysql);
            $fo = fopen("errorAsistencia.txt", "w+");
            fwrite($fo, $mensaje . "\r\n");
            fwrite($fo, serialize($mensaje) . "\r\n");
            fclose($fo);

        } catch (Exception $e) {
            $fo = fopen("errorAsistencia.txt", "w+");
            fwrite($fo, $e . "\r\n");
            fwrite($fo, serialize($e) . "\r\n");
            fclose($fo);
        }
/*
        if((strlen(trim($x)) > 0) && (strlen(trim($y)) > 0)){
            require_once './integracion/class/class.CurlNovus.php';
            $CurlNovus = new CurlNovus();
            $dt = new DateTime();
            $array = array(
                    'company' => 'telefonica-pe.test',
                    'device' => $simpleXml->Employee->EmployeeNumber,
                    'x' => (double)$x,
                    'y' => (double)$y,
                    'time' => $dt->format('Y-m-d H:i:s')
                );
            $request = $CurlNovus->registrar_location($array);
            $request .= 'x:'.$x.' y:'.$y.' number:'.$simpleXml->Employee->EmployeeNumber.' time:'.$dt->format('Y-m-d H:i:s').' EntryType:'.$EntryType;
            $fo = fopen("test_tony.txt", "w+");
            fwrite($fo, $request . "\r\n");
            fwrite($fo, serialize($request) . "\r\n");
            fclose($fo);
        } else {
            $text = 'coordenadas vacias.'.$x.' '.$y;
            $fo = fopen("error_tony.txt", "w+");
            fwrite($fo, $text . "\r\n");
            fwrite($fo, serialize($text) . "\r\n");
            fclose($fo);
        }       
*/
    }
    
    if ($TaskNumber == 2000) {
        $fo = fopen($TaskNumber . "_form_deco_" . date("dmYHis") . ".txt", "w+");
        fwrite($fo, $xml);
        fclose($fo);
    }
    
} else {
    echo "Nothing to do";
}

