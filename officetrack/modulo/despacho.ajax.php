<?php
require_once '../integracion/class/class.Conexion.php';
require_once '../integracion/class/class.Location.php';
$Conexion = new Conexion();
$Location = new Location();

$db = $Conexion->conectar();

if ( isset( $_POST["codePath"] ) ) 
{
    $code = $_POST["codePath"];
    $date = date("d.m.Y");
    $path = $Location->getPath($db, $date, $code);
    
    echo json_encode($path);
}