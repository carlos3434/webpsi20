<?php
require_once '../integracion/class/class.Conexion.php';
require_once '../integracion/class/class.Location.php';
$Conexion = new Conexion();
$Location = new Location();

$db = $Conexion->conectar();

$fecha = date("d.m.Y");
$xy = $Location->getLocations(
        $db, 
        $codArray = array(), 
        $numArray = array(),
        $fecha
    );

if ($xy["estado"] === true) {
    $coord = json_encode($xy["data"]);
} else {
    die("Error al recuperar localizaciones.");
}

//$path = $Location->getPath($db, $fecha, "9048677");

?>

<!DOCTYPE html>
<html>
    <head>
        <title>Simple Map</title>
        <meta name="viewport" content="initial-scale=1.0, user-scalable=no">
        <meta charset="utf-8">
        <style>
            html, body, #map-canvas {
                height: 100%;
                margin: 0px;
                padding: 0px
            }
        </style>
        <script src="https://maps.googleapis.com/maps/api/js?v=3.exp"></script>
        <script src="http://code.jquery.com/jquery-1.11.1.min.js"></script>
        <script>
            var x;
            var y;
            var map;
            var marker;
            var bounds;
            var coord;
            var myLatlng;
            var infocontent;
            var infowindow;
            var tecData = [];

            function initialize() {
                var mapOptions = {
                    zoom: 14,
                    center: new google.maps.LatLng(-12.109461, -77.015492)
                };
                map = new google.maps.Map(document.getElementById('map-canvas'),
                        mapOptions);
                //marcadores (tecnicos)
                coord = <?php echo $coord; ?>;
                
                //Inicializar infowindow
                infowindow = new google.maps.InfoWindow({
                    content: "Loading..."
                });

                loadMarker(map, coord);

                map.fitBounds(bounds);
            }

            function loadMarker(map, coord) {
                
                $.each(coord, function() {
                    //Ubicacion
                    x = Number(this.x.replace(",", "."));
                    y = Number(this.y.replace(",", "."));
                    myLatlng = new google.maps.LatLng(y, x);

                    bounds.extend(myLatlng);

                    //Marcador
                    marker = new google.maps.Marker({
                        position: myLatlng,
                        map: map,
                        title: this.EmployeeNum
                    });
                    
                    infocontent = "<table>"
                                  + "<tr>"
                                  + "    <td rowspan=\"4\"><img src=\"imgtec/tecnico.png\"></td>"
                                  + "    <td></td>"
                                  + "</tr>"
                                  + "<tr>"
                                  + "    <td>" + this.EmployeeNum + "</td>"
                                  + "</tr>"
                                  + "<tr>"
                                  + "    <td>" + this.MobileNumber + "</td>"
                                  + "</tr>"
                                  + "<tr>"
                                  + "    <td><a href=\"javascript:void(0)\" onClick=\"doPath('" + this.EmployeeNum + "')\">Ruta</a></td>"
                                  + "</tr>"
                                  + "</table>";
                    
                    

                    google.maps.event.addListener(marker,'click', (
                            function(marker,infocontent,infowindow){ 
                        return function() {
                            infowindow.setContent(infocontent);
                            infowindow.open(map,marker);
                        };
                    })(marker,infocontent,infowindow)); 
                    
                });
            }
            
            function doPath(code){
                //Get coords by address                
                $.ajax({
                    type: "POST",
                    url: "despacho.ajax.php",
                    data: "codePath=" + code,
                    dataType: 'json',
                    error: function(data) {
                        console.log(data);
                    },
                    success: function(data) {
                        
                        if (data.estado===true)
                        {
                            var n = 1;
                            $.each(data.data, function(){
                                x = Number(this.X.replace(",", "."));
                                y = Number(this.Y.replace(",", "."));
                                myLatlng = new google.maps.LatLng(y, x);

                                bounds.extend(myLatlng);

                                //Marcador
                                marker = new google.maps.Marker({
                                    position: myLatlng,
                                    map: map,
                                    title: this.EmployeeNum,
                                    icon: "http://chart.apis.google.com/chart" 
                                           + "?chst=d_map_pin_letter&chld=" + n + "|00FF00",
                                    draggable:true
                                });
                                n++;
                            });
                        } else {
                            console.log("Error");
                        }
                    }
                });
            }

            $(document).ready(function() {
                //Mostrar mapa
                google.maps.event.addDomListener(window, 'load', initialize);
                //bounds
                bounds = new google.maps.LatLngBounds();
            });



        </script>
    </head>
    <body>
        <div id="map-canvas"></div>
    </body>
</html>